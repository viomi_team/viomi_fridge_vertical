package com.viomi.fridge.vertical.web.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amap.api.location.AMapLocationClient;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.miot.api.MiotManager;
import com.miot.common.people.People;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.model.entity.QRCodeBase;
import com.viomi.fridge.vertical.administration.model.preference.ManagePreference;
import com.viomi.fridge.vertical.administration.model.repository.ManageRepository;
import com.viomi.fridge.vertical.administration.view.dialog.LoginQRCodeDialog;
import com.viomi.fridge.vertical.common.base.BaseFragment;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.scope.ActivityScoped;
import com.viomi.fridge.vertical.common.util.FileUtil;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;
import com.viomi.fridge.vertical.web.X5WebView;
import com.viomi.fridge.vertical.web.activity.BrowserActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import rx.Observable;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * 云米商城 Fragment
 * Created by William on 2018/3/5.
 */
@ActivityScoped
public class MallFragment extends BaseFragment implements X5WebView.OnWebViewListener {
    private static final String TAG = MallFragment.class.getSimpleName();
    private CompositeSubscription mCompositeSubscription;
    private Subscription mRemoveSubscription;// WebView 移除计时
    private AMapLocationClient mLocationClient;// 高德定位
    private VMallJavaScriptInterface mJavaScriptInterface;// JS 交互接口
    private boolean mIsRemove = true, mIsError = false;// 是否已经移除
    private Context mContext;
    private String mUrl;
    private X5WebView mWebView;// 浏览器
    private LoginQRCodeDialog mLoginQRCodeDialog;// 登录对话框

    @BindView(R.id.mall_browser)
    ViewGroup mViewGroup;// 浏览器

    @BindView(R.id.mall_progress)
    ProgressBar mProgressBar;// 加载进度条

    @BindView(R.id.mall_loading_text)
    TextView mTextView;

    @BindView(R.id.mall_loading_gif)
    SimpleDraweeView mSimpleDraweeView;// 动画

    @BindView(R.id.mall_loading_layout)
    LinearLayout mLinearLayout;// 加载布局

    @BindView(R.id.mall_network_fail_layout)
    RelativeLayout mRelativeLayout;// 网络错误提示

    @Inject
    public MallFragment() {

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (mContext != null) {
            if (isVisibleToUser) {
                stopRemoveTimer();
                if (mIsRemove) {
                    initWebView();
                    mIsRemove = false;
                }
            } else {
                startRemoveTimer();
            }
        }
    }

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.fragment_mall;
        mCompositeSubscription = new CompositeSubscription();
        Subscription subscription = RxBus.getInstance().subscribe(busEvent -> {
            switch (busEvent.getMsgId()) {
                case BusEvent.MSG_LOGIN_SUCCESS: // 登录成功
                    if (mWebView != null) mWebView.reload();
                    break;
                case BusEvent.MSG_LOGOUT_SUCCESS: // 注销成功
                    if (mWebView != null) mWebView.reload();
                    break;
                case BusEvent.MSG_SERVER_CHANGE: // 服务器改变
                    removeWebView();
                    break;
            }
        });
        mCompositeSubscription.add(subscription);
    }


    @Override
    protected void initWithOnCreateView() {
        mContext = getActivity();
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setUri(Uri.parse("android.resource://" + mContext.getPackageName() + "/" + com.viomi.widget.R.drawable.icon_loading))
                .setAutoPlayAnimations(false)
                .build();
        mSimpleDraweeView.setController(controller);
    }

    private void initWebView() {
        mWebView = new X5WebView(mContext, null);
        mViewGroup.addView(mWebView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));

        if (mSimpleDraweeView.getController() != null && mSimpleDraweeView.getController().getAnimatable() != null) {
            mSimpleDraweeView.getController().getAnimatable().start();
        }
        mTextView.setVisibility(View.VISIBLE);

        startAssistLocation();

        mJavaScriptInterface = new VMallJavaScriptInterface();
        mWebView.addJavascriptInterface(mJavaScriptInterface, "H5ToNative");
        mWebView.setOnPageFinishListener(this);

        if (!FridgePreference.getInstance().getModel().equals(AppConstants.MODEL_JD)) {
            if (ManagePreference.getInstance().getDebug()) mUrl = AppConstants.URL_VMALL_DEBUG;
            else mUrl = AppConstants.URL_VMALL_RELEASE;
        } else { // 京东定制款
            String mac = ToolUtil.getMiIdentification().getMac();
            mac = mac.replace(":", "");
            mUrl = "https://coupon.m.jd.com/union?&mtm_source=kepler-m&mtm_subsource=ac498242c1ac40b2b21554de3511c146" +
                    "&mopenbp5=" + mac +
                    "&returl=https%3A%2F%2Fpro.m.jd.com%2Fmall%2Factive%2F4P9a2T9osR9JvtzHVaYTPvsecRtg%2Findex.html";
        }
        mWebView.loadUrl(mUrl);
        String call1 = "javascript:NativeToH5.onWebPageBack()";
        mWebView.loadUrl(call1);
    }

    @Override
    public void onDestroyView() {
        removeWebView();
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
        mContext = null;
        if (mLoginQRCodeDialog != null && mLoginQRCodeDialog.isAdded()) {
            mLoginQRCodeDialog.dismiss();
            mLoginQRCodeDialog = null;
        }
        super.onDestroyView();
    }

    @OnClick(R.id.mall_error_retry)
    public void retry() { // 重试
        mWebView.reload();
    }

    /**
     * 启动 H5 辅助定位
     */
    private void startAssistLocation() {
        if (mLocationClient == null) {
            mLocationClient = new AMapLocationClient(FridgeApplication.getContext());
        }
        mLocationClient.startAssistantLocation();
    }

    /**
     * 开始 WebView 移除计时
     */
    private void startRemoveTimer() {
        mRemoveSubscription = Observable.timer(30, TimeUnit.SECONDS)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(aLong -> removeWebView(), throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(mRemoveSubscription);
    }

    /**
     * 停止 WebView 移除计时
     */
    private void stopRemoveTimer() {
        if (mRemoveSubscription != null && !mRemoveSubscription.isUnsubscribed()) {
            mRemoveSubscription.unsubscribe();
            mRemoveSubscription = null;
        }
    }

    /**
     * 移除 WebView
     */
    private void removeWebView() {
        if (mWebView != null) {
            mWebView.getSettings().setJavaScriptEnabled(false);
            mWebView.stopLoading();
            mViewGroup.removeView(mWebView);
            mWebView.removeAllViews();
            mWebView.destroy();
            mWebView = null;
        }
        if (mLinearLayout != null) mLinearLayout.setVisibility(View.VISIBLE);
        if (mViewGroup != null) mViewGroup.setVisibility(View.GONE);
        if (mRelativeLayout != null) mRelativeLayout.setVisibility(View.GONE);
        mJavaScriptInterface = null;
        stopRemoveTimer();
        if (mLocationClient != null) {
            mLocationClient.stopAssistantLocation();
            mLocationClient.onDestroy();
        }
        mIsRemove = true;
    }

    @Override
    public void onReceivedTitle(String title) {
        if (mWebView.getX5WebViewExtension() == null && title.equals("找不到网页")) { // 系统内核
            mIsError = true;
            mRelativeLayout.setVisibility(View.VISIBLE);
            mViewGroup.setVisibility(View.GONE);
        }
    }

    @Override
    public void onProgressChanged(int progress) {
        if (mLinearLayout.getVisibility() == View.VISIBLE) {
            if (progress >= 80) {
                if (mSimpleDraweeView.getController() != null && mSimpleDraweeView.getController().getAnimatable() != null) {
                    mSimpleDraweeView.getController().getAnimatable().stop();
                }
                DraweeController controller = Fresco.newDraweeControllerBuilder()
                        .setUri(Uri.parse("android.resource://" + mContext.getPackageName() + "/" + com.viomi.widget.R.drawable.icon_loading))
                        .setAutoPlayAnimations(false)
                        .build();
                mSimpleDraweeView.setController(controller);
                mTextView.setVisibility(View.GONE);
                mLinearLayout.setVisibility(View.GONE);
                mViewGroup.setVisibility(View.VISIBLE);
            }
        } else {
            mProgressBar.setVisibility(View.VISIBLE);
            mProgressBar.setProgress(progress);
            // 优化用户体验
            if (progress >= 80) mProgressBar.setVisibility(View.GONE);
            if (progress >= 30 && !mIsError) {
                if (mRelativeLayout.getVisibility() == View.VISIBLE)
                    mRelativeLayout.setVisibility(View.GONE);
                if (mViewGroup.getVisibility() == View.GONE) mViewGroup.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onPageStarted(String url) {
        mIsError = false;
        if (!url.contains("mi-ae.net")) {
            mWebView.stopLoading();
            mWebView.loadUrl(mUrl);
        }
    }

    @Override
    public void onPageFinished(String url) {
        mUrl = url;
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onReceivedError() {
        if (mWebView.getX5WebViewExtension() == null) { // 系统内核
            mIsError = true;
            mRelativeLayout.setVisibility(View.VISIBLE);
            mViewGroup.setVisibility(View.GONE);
        }
    }

    private class VMallJavaScriptInterface {
        /**
         * 解决 JS 轮播与 ViewPager 冲突
         */
        @JavascriptInterface
        public void isH5ViewPagerScroll(boolean enable) {
            logUtil.d(TAG, "isH5ViewPagerScroll:" + enable);
            if (enable) mWebView.requestDisallowInterceptTouchEvent(true);
            else mWebView.requestDisallowInterceptTouchEvent(false);
        }

        /**
         * 清除帐号信息
         */
        @JavascriptInterface
        public void onClearAcount() {
            logUtil.d(TAG, "onClearAcount");
            ManageRepository.getInstance().logout()
                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                    .subscribe(aBoolean -> {
                        if (aBoolean) {
                            ToolUtil.saveObject(FridgeApplication.getContext(), AppConstants.USER_INFO_FILE, null);
                            mWebView.reload();
                        }
                    });
        }

        /**
         * 打开新 H5 页面
         *
         * @param title 新页面标题
         * @param url   新页面链接
         */
        @JavascriptInterface
        public void onWebPageJump(String title, String url) {
            logUtil.d(TAG, "onWebPageJump");
            if (getActivity() != null) {
                getActivity().runOnUiThread(() -> {
                    Intent intent = new Intent(getActivity(), BrowserActivity.class);
                    intent.putExtra("url", url);
                    startActivity(intent);
                });
            }
        }

        /**
         * 关闭当前页面，返回上一页面
         */
        @JavascriptInterface
        public void onWebPageReturn() {
            logUtil.d(TAG, "onWebPageReturn");
//            runOnUiThread(MallWebActivity.this::finish);
        }

        /**
         * 跳转到登陆页面
         */
        @JavascriptInterface
        public void onLoginPageJump() {
            logUtil.d(TAG, "onLoginPageJump");
            if (mLoginQRCodeDialog == null) mLoginQRCodeDialog = new LoginQRCodeDialog();
            if (mLoginQRCodeDialog.isAdded()) return;
            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            if (getActivity() != null)
                getActivity().runOnUiThread(() -> mLoginQRCodeDialog.show(fragmentTransaction, TAG));
        }

        /**
         * 获取城市名称
         */
        @JavascriptInterface
        public String getCityName() {
            logUtil.d(TAG, "getCityName");
            return FridgePreference.getInstance().getCity();
        }

        /**
         * 获取城市编码
         */
        @JavascriptInterface
        public String getCityCode() {
            logUtil.d(TAG, "getCityCode");
            return FridgePreference.getInstance().getCityCode();
        }

        /**
         * 获取用户信息
         * return 未登录，返回 null;已登陆返回 json 字符串
         */
        @JavascriptInterface
        public String getUserInfo() {
            logUtil.d(TAG, "getUserInfo");
            QRCodeBase qrCodeBase = (QRCodeBase) FileUtil.getObject(FridgeApplication.getContext(), AppConstants.USER_INFO_FILE);
            if (qrCodeBase == null) {
                return null;
            }
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("account", qrCodeBase.getLoginQRCode().getUserInfo().getAccount());
                jsonObject.put("userCode", qrCodeBase.getLoginQRCode().getUserInfo().getUserCode());
                jsonObject.put("token", qrCodeBase.getLoginQRCode().getUserInfo().getToken());
                jsonObject.put("cid", qrCodeBase.getLoginQRCode().getUserInfo().getCid());
                People mPeople = MiotManager.getPeople();
                if (mPeople != null && !TextUtils.isEmpty(mPeople.getUserId())) {
                    jsonObject.put("miid", mPeople.getUserId());
                }
                return jsonObject.toString();
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }

        // 编辑器关闭时调用，隐藏系统底部导航栏
        @JavascriptInterface
        public void hideBottomNavigation() {
            logUtil.d(TAG, "hideBottomNavigation");
//            runOnUiThread(() -> {
//                getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//                getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
//                int uiFlags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
//                        | View.SYSTEM_UI_FLAG_FULLSCREEN; // hide status bar
//
//                if (Build.VERSION.SDK_INT >= 19) {
//                    uiFlags |= 0x00001000;    //SYSTEM_UI_FLAG_IMMERSIVE_STICKY: hide navigation bars - compatibility: building API level is lower thatn 19, use magic number directly for higher API target level
//                } else {
//                    uiFlags |= View.SYSTEM_UI_FLAG_LOW_PROFILE;
//                }
//                try {
//                    getWindow().getDecorView().setSystemUiVisibility(uiFlags);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                logUtil.d(TAG, "hideBottomNavigation");
//            });
        }

        // 有数据更新是清除本地缓存，H5 调用
        @JavascriptInterface
        public void clearNativeCache() {
            logUtil.d(TAG, "clearNativeCache");
            if (getActivity() != null)
                getActivity().runOnUiThread(() -> mWebView.clearCache(true));
        }

        // H5 加载失败后调用
        @JavascriptInterface
        public void loadFail() {
            logUtil.d(TAG, "loadFail");
            if (getActivity() != null) {
                getActivity().runOnUiThread(() -> mRelativeLayout.setVisibility(View.VISIBLE));
            }
        }

        // 是否显示商城标题栏
        @JavascriptInterface
        public boolean isH5TilteBarShow() {
            return true;
        }
    }
}
