package com.viomi.fridge.vertical.fridgecontrol.defined.action;

import com.viomi.fridge.vertical.fridgecontrol.defined.ViomiDefined;
import com.viomi.fridge.vertical.fridgecontrol.defined.property.RCSetTemp;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetRCSetTemp extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setRCSetTemp.toActionType();

    public SetRCSetTemp() {
        super(TYPE);

        super.addArgument(RCSetTemp.TYPE.toString());
    }
}