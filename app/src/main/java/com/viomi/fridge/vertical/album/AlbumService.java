package com.viomi.fridge.vertical.album;

import android.app.Service;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.view.WindowManager;

import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.album.activity.AlbumActivity;
import com.viomi.fridge.vertical.album.response.RequestLoginHandler;
import com.viomi.fridge.vertical.album.response.RequestPermission;
import com.viomi.fridge.vertical.album.response.RequestUploadHandler;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.widget.dialog.BaseAlertDialog;
import com.yanzhenjie.andserver.AndServer;
import com.yanzhenjie.andserver.Server;
import com.yanzhenjie.andserver.website.AssetsWebsite;

import rx.Observable;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * 电子相册监听上传 Service
 * Created by William on 2018/1/19.
 */
public class AlbumService extends Service implements RequestUploadHandler.onReceiveListener {
    private static final String TAG = AlbumService.class.getSimpleName();
    private Server mServer;
    private CompositeSubscription mCompositeSubscription;
    private BaseAlertDialog mDialog;

    @Override
    public void onCreate() {
        super.onCreate();
        AssetManager assetManager = getAssets();

        AndServer andServer = new AndServer.Build()
                .port(8080) // 默认是 8080，Android 平台允许的端口号都可以
                .timeout(10 * 1000) // 默认超时 10 * 1000 毫秒
                .registerHandler("login", new RequestLoginHandler())
                .registerHandler("prepare_connect", new RequestPermission())
                .registerHandler("upload_file", new RequestUploadHandler(this)) // 上传图片 API
                .website(new AssetsWebsite(assetManager, "")) // 部署网站
                .listener(mListener)
                .build();

        mServer = andServer.createServer();
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startServer();
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.cancel();
            mDialog = null;
        }
        stopServer();
    }

    /**
     * 启动服务器
     */
    private void startServer() {
        if (mServer != null) {
            if (mServer.isRunning()) {
                logUtil.d(TAG, "电子相册服务器已经运行");
            } else {
                logUtil.d(TAG, "电子相册服务器开始启动");
                mServer.start();
            }
        }
    }

    /**
     * 停止服务器
     */
    private void stopServer() {
        if (mServer != null) {
            logUtil.e(TAG, "电子相册服务器终止");
            mServer.stop();
        }
    }

    /**
     * 服务器状态监听
     */
    private Server.Listener mListener = new Server.Listener() {
        @Override
        public void onStarted() {
            logUtil.d(TAG, "服务器启动成功");
        }

        @Override
        public void onStopped() {
            logUtil.d(TAG, "服务器已停止");
        }

        @Override
        public void onError(Exception e) {
            logUtil.e(TAG, "服务器启动发生错误");
        }
    };

    @Override
    public void onReceiveFinish() {
        logUtil.d(TAG, "onReceiveFinish");
        Subscription subscription = Observable.just(true)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(aBoolean -> {
                    RxBus.getInstance().post(BusEvent.MSG_ALBUM_UPDATE);
                    // 电子相册 Activity 是否正在运行
                    if (ToolUtil.isActivityRunning(FridgeApplication.getContext(), "com.viomi.fridge.vertical.album.activity.AlbumActivity"))
                        return;
                    if (mDialog != null && mDialog.isShowing()) return;
                    mDialog = new BaseAlertDialog(FridgeApplication.getContext(), FridgeApplication.getContext().getResources().getString(R.string.album_dialog_message),
                            FridgeApplication.getContext().getResources().getString(R.string.album_next_time),
                            FridgeApplication.getContext().getResources().getString(R.string.album_go));
                    mDialog.setOnLeftClickListener(mDialog::dismiss);
                    mDialog.setOnRightClickListener(() -> {
                        Intent intent = new Intent(AlbumService.this, AlbumActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        mDialog.dismiss();
                    });
                    if (mDialog.getWindow() == null) return;
                    mDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                    mDialog.show();
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }
}