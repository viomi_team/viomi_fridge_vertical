package com.viomi.fridge.vertical.fridgecontrol.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.entity.ChangeableScene;
import com.viomi.fridge.vertical.fridgecontrol.view.adapter.SceneAdapter;
import com.viomi.widget.switchbutton.SwitchButton2;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * 仓室设置 View
 * Created by William on 2018/2/2.
 */
public class RoomSettingView extends LinearLayout implements SeekBar.OnSeekBarChangeListener, CompoundButton.OnCheckedChangeListener,
        AdapterView.OnItemClickListener {
    private Unbinder unbinder;
    private Context mContext;
    private int mMinTemp, mMaxTemp;// 最低设置温度，最高设置温度
    private boolean isIgnore = false, isSetting = false;// 仓室开关
    private int mCurTemp;// 当前温度
    private String mModel;// 设备 Model
    private List<ChangeableScene> mList;// 变温室场景集合
    private SceneAdapter mAdapter;// 变温室场景适配器
    private OnSwitchChangedListener onSwitchChangedListener;
    private OnTempChangedListener onTempChangedListener;
    private OnSceneMoreClickListener onSceneMoreClickListener;
    private OnSceneSetListener onSceneSetListener;

    @BindView(R.id.room_setting_icon)
    ImageView mTypeImageView;// 仓室图标
    @BindView(R.id.room_setting_decrease)
    ImageView mDecreaseImageView;// 减
    @BindView(R.id.room_setting_increase)
    ImageView mIncreaseImageView;// 增

    @BindView(R.id.room_setting_type)
    TextView mTypeTextView;// 仓室类型
    @BindView(R.id.room_setting_temp)
    TextView mTempTextView;// 温度
    @BindView(R.id.room_setting_scene_name)
    TextView mDefinedTextView;// 自定义
    @BindView(R.id.room_setting_scene_tip)
    TextView mTipTextView;// 场景提示文字

    @BindView(R.id.room_setting_seek_bar)
    SeekBar mSeekBar;// 温度滑动条

    @BindView(R.id.room_setting_switch)
    SwitchButton2 mSwitchButton;// 仓室开关

    @BindView(R.id.room_setting_scene_layout)
    RelativeLayout mRelativeLayout;// 场景布局

    @BindView(R.id.room_setting_scene_list)
    RecyclerView mRecyclerView;// 场景 list

    public RoomSettingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    public RoomSettingView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoomSettingView(Context context) {
        this(context, null);
    }

    private void initView(Context context, AttributeSet attrs) {
        mContext = context;
        View view = LayoutInflater.from(context).inflate(R.layout.fridge_room_setting_view, this);
        unbinder = ButterKnife.bind(this, view);
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.RoomSettingView);
        String name = typedArray.getString(R.styleable.RoomSettingView_room_name);// 仓室名称
        mModel = FridgePreference.getInstance().getModel();// 冰箱 Model
        if (name == null) name = "";
        // 仓室对应图标
        if (name.equals(mContext.getResources().getString(R.string.fridge_room_cold))) {
            switch (mModel) {
                case AppConstants.MODEL_X2: // 双鹿 446
                case AppConstants.MODEL_X3: // 美菱 462
                    mTypeImageView.setImageResource(R.drawable.icon_2_doors_room_cold);
                    break;
                case AppConstants.MODEL_X4: // 雪祺 450
                case AppConstants.MODEL_JD: // 雪祺 450 京东定制
                    mTypeImageView.setImageResource(R.drawable.icon_opposite_doors_room_right);
                    break;
                case AppConstants.MODEL_X5: // 美菱 521
                    mTypeImageView.setImageResource(R.drawable.icon_cross_doors_room_cold);
                    break;
                default:
                    mTypeImageView.setImageResource(R.drawable.icon_3_doors_room_cold);
                    break;
            }
        } else if (name.equals(mContext.getResources().getString(R.string.fridge_room_changeable))) {
            if (mModel.equals(AppConstants.MODEL_X5)) {
                mTypeImageView.setImageResource(R.drawable.icon_cross_doors_room_changeable);
            } else {
                mTypeImageView.setImageResource(R.drawable.icon_3_doors_room_changeable);
            }
        } else if (name.equals(mContext.getResources().getString(R.string.fridge_room_freezing))) {
            switch (mModel) {
                case AppConstants.MODEL_X2: // 双鹿 446
                case AppConstants.MODEL_X3: // 美菱 462
                    mTypeImageView.setImageResource(R.drawable.icon_2_doors_room_freeze);
                    break;
                case AppConstants.MODEL_X4: // 雪祺 450
                case AppConstants.MODEL_JD: // 雪祺 450 京东定制
                    mTypeImageView.setImageResource(R.drawable.icon_opposite_doors_room_left);
                    break;
                case AppConstants.MODEL_X5: // 美菱 521
                    mTypeImageView.setImageResource(R.drawable.icon_cross_doors_room_freeze);
                    break;
                default:
                    mTypeImageView.setImageResource(R.drawable.icon_3_doors_room_freezing);
                    break;
            }
        }
        mTypeTextView.setText(name);// 仓室名称
        typedArray.recycle();

        mTempTextView.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/DINCond-Medium.otf"));// 字体
        if (name.equals(getContext().getResources().getString(R.string.fridge_room_cold))) { // 冷藏室
            mMinTemp = 2;
            mMaxTemp = 8;
            mTipTextView.setVisibility(View.GONE);
            mRelativeLayout.setVisibility(View.GONE);
            mSwitchButton.setOnCheckedChangeListener(this);
        } else if (name.equals(getContext().getResources().getString(R.string.fridge_room_changeable))) { // 变温室
            mMinTemp = -18;
            switch (mModel) {
                case AppConstants.MODEL_X2: // 双鹿 428
                    mMaxTemp = -3;
                    break;
                case AppConstants.MODEL_X5: // 美菱 521
                    mMaxTemp = 5;
                    break;
                default:
                    mMaxTemp = 8;
                    break;
            }
            mSwitchButton.setOnCheckedChangeListener(this);
        } else { // 冷冻室
            switch (mModel) {
                case AppConstants.MODEL_X2: // 双鹿 428
                case AppConstants.MODEL_X3: // 美菱 462
                case AppConstants.MODEL_X5: // 美菱 521
                    mMinTemp = -24;
                    break;
                case AppConstants.MODEL_X4: // 雪祺 450
                case AppConstants.MODEL_JD: // 雪祺 450 京东定制
                    mMinTemp = -23;
                    break;
                default:
                    mMinTemp = -25;
                    break;
            }
            if (mModel.equals(AppConstants.MODEL_X2) || mModel.equals(AppConstants.MODEL_X3) || mModel.equals(AppConstants.MODEL_X5))
                mMaxTemp = -16;
            else mMaxTemp = -15;
            mTipTextView.setVisibility(View.GONE);
            mRelativeLayout.setVisibility(View.GONE);
            mSwitchButton.setVisibility(View.INVISIBLE);
        }
        mSeekBar.setMax(mMaxTemp - mMinTemp);// 设置 SeekBar 范围
        mSeekBar.setOnSeekBarChangeListener(this);
    }

    /**
     * 设置数据
     */
    public void setupData(boolean enable, int temp) {
        if (isSetting) return;
        if (temp < mMinTemp) temp = mMinTemp;
        else if (temp > mMaxTemp && !mModel.equals(AppConstants.MODEL_X5)) temp = mMaxTemp;
        mCurTemp = temp;
        mSeekBar.setProgress(mCurTemp - mMinTemp);// SeekBar 进度
        setUi(enable);
    }

    /**
     * 根据仓室开关调整 UI
     */
    protected void setUi(boolean enable) {
        if (enable) { // 开
            mDecreaseImageView.setEnabled(true);
            mIncreaseImageView.setEnabled(true);
            mSeekBar.setEnabled(true);
            mTempTextView.setText(String.valueOf(mCurTemp));
        } else { // 关
            mDecreaseImageView.setEnabled(false);
            mIncreaseImageView.setEnabled(false);
            mSeekBar.setEnabled(false);
            mTempTextView.setText(getContext().getResources().getString(R.string.fridge_off));
        }
        isIgnore = true;
        mSwitchButton.setChecked(enable);
        isIgnore = false;
    }

    /**
     * 刷新显示场景
     */
    public void refreshScene(List<ChangeableScene> list) {
        mList = new ArrayList<>();
        mList.addAll(list);
        mAdapter = new SceneAdapter(mList, 0);
        String name = FridgePreference.getInstance().getScene();
        if (name.equals("")) {
            mAdapter.setSelectedPosition(-1);
            mTipTextView.setText("");
            mDefinedTextView.setSelected(true);
        } else {
            mTipTextView.setText("");
            for (int i = 0; i < mList.size(); i++) {
                if (mList.get(i).getScene().equals(name)) {
                    mTipTextView.setText(mList.get(i).getTip());
                    mAdapter.setSelectedPosition(i);
                    mDefinedTextView.setSelected(false);
                    break;
                }
            }
        }
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(this);
    }

    @OnClick(R.id.room_setting_decrease)
    public void tempDecrease() { // 加
        if (mCurTemp > mMinTemp) {
            mCurTemp = mCurTemp - 1;
            mSeekBar.setProgress(mSeekBar.getProgress() - 1);
            mTempTextView.setText(String.valueOf(mCurTemp));
            if (onTempChangedListener != null) onTempChangedListener.onTempChange(mCurTemp);
        }
    }

    @OnClick(R.id.room_setting_increase)
    public void tempIncrease() { // 减
        if (mCurTemp < mMaxTemp) {
            mCurTemp = mCurTemp + 1;
            mSeekBar.setProgress(mSeekBar.getProgress() + 1);
            mTempTextView.setText(String.valueOf(mCurTemp));
            if (onTempChangedListener != null) onTempChangedListener.onTempChange(mCurTemp);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        unbinder.unbind();
        if (mAdapter != null) mAdapter = null;
        if (mList != null) {
            mList.clear();
            mList = null;
        }
        if (mContext != null) mContext = null;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        mCurTemp = progress + mMinTemp;
        mTempTextView.setText(String.valueOf(mCurTemp));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        isSetting = true;
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        isSetting = false;
        if (onTempChangedListener != null) onTempChangedListener.onTempChange(mCurTemp);
    }

    @OnClick(R.id.room_setting_scene_more)
    public void scenesMore() {
        if (onSceneMoreClickListener != null) onSceneMoreClickListener.onSceneMoreClick();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (onSceneSetListener != null && mSwitchButton.isChecked()) {
            onSceneSetListener.onSceneSet(mList.get(position));
            mTipTextView.setText(mList.get(position).getTip());
            mAdapter.setSelectedPosition(position);
            mAdapter.notifyDataSetChanged();
            mDefinedTextView.setSelected(false);
        }
    }

    public interface OnSwitchChangedListener {
        void onSwitchChange(boolean enable);
    }

    public void setOnSwitchChangedListener(OnSwitchChangedListener onSwitchChangedListener) {
        this.onSwitchChangedListener = onSwitchChangedListener;
    }

    public interface OnTempChangedListener {
        void onTempChange(int temp);
    }

    public void setOnTempChangedListener(OnTempChangedListener onTempChangedListener) {
        this.onTempChangedListener = onTempChangedListener;
    }

    public interface OnSceneMoreClickListener {
        void onSceneMoreClick();
    }

    public void setOnSceneMoreClickListener(OnSceneMoreClickListener onSceneMoreClickListener) {
        this.onSceneMoreClickListener = onSceneMoreClickListener;
    }

    public interface OnSceneSetListener {
        void onSceneSet(ChangeableScene scene);
    }

    public void setOnSceneSetListener(OnSceneSetListener onSceneSetListener) {
        this.onSceneSetListener = onSceneSetListener;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isIgnore) return;
        if (onSwitchChangedListener != null) onSwitchChangedListener.onSwitchChange(isChecked);
        setUi(isChecked);
    }
}