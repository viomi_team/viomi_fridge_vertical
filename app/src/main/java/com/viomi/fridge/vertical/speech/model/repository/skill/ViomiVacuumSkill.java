package com.viomi.fridge.vertical.speech.model.repository.skill;

import android.content.Context;

import com.miot.common.abstractdevice.AbstractDevice;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.administration.model.entity.QRCodeBase;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.iot.model.repository.SweepingRobotRepository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 云米扫地机技能
 * Created by William on 2018/8/30.
 */
public class ViomiVacuumSkill {
    private static final String TAG = ViomiVacuumSkill.class.getSimpleName();

    public static void handle(Context context, String data, AbstractDevice device) {
        String content;// 语音播报内容
        QRCodeBase qrCodeBase = (QRCodeBase) ToolUtil.getFileObject(context, AppConstants.USER_INFO_FILE);
        if (qrCodeBase == null) {
            content = "请先登录云米账号";
            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
            return;
        }
        if (device == null) {
            content = "没有连接到扫地机";
            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
            return;
        }

        try {
            JSONObject jsonObject = new JSONObject(data);
            JSONObject nlu = jsonObject.optJSONObject("nlu");
            if (nlu == null) return;
            JSONObject semantics = nlu.optJSONObject("semantics");
            if (semantics == null) return;
            JSONObject request = semantics.optJSONObject("request");
            if (request == null) return;
            JSONArray slots = request.optJSONArray("slots");
            if (slots == null || slots.length() <= 0) return;

            String value = "";// 意图
            for (int i = 0; i < slots.length(); i++) {
                JSONObject jsonItem = slots.optJSONObject(i);
                String name = jsonItem.optString("name");
                if (name.equals("intent")) {
                    value = jsonItem.optString("value");
                    break;
                }
            }

            switch (value) {
                case "设置工作模式":
                    String mode = "";
                    for (int i = 0; i < slots.length(); i++) {
                        JSONObject itemJson = slots.optJSONObject(i);
                        String name = itemJson.optString("name");
                        if ("工作模式".equals(name)) {
                            mode = itemJson.optString("value");
                            break;
                        }
                    }
                    switch (mode) {
                        case "自动清扫":
                            SweepingRobotRepository.setMode(0, device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "设为" + mode + "模式";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "拖地":
                            SweepingRobotRepository.setMode(1, device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "设为" + mode + "模式";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "重点清扫":
                            SweepingRobotRepository.setMode(2, device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "设为" + mode + "模式";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "回充":
                            SweepingRobotRepository.setMode(3, device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "设为" + mode + "模式";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                    }
                    break;
                case "扫地机启动":
                    SweepingRobotRepository.setPower(1, device.getDeviceId())
                            .subscribeOn(Schedulers.io())
                            .onTerminateDetach()
                            .flatMap(rpcResult -> SweepingRobotRepository.setMode(0, device.getDeviceId()))
                            .observeOn(AndroidSchedulers.mainThread())
                            .onTerminateDetach()
                            .subscribe(rpcResult -> {
                            }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                    content = "正在为你开启" + device.getName();
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "扫地机关闭":
                    SweepingRobotRepository.setPower(0, device.getDeviceId())
                            .compose(RxSchedulerUtil.SchedulersTransformer1())
                            .subscribe(rpcResult -> {
                            }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                    content = "正在为你关闭" + device.getName();
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
            }
        } catch (JSONException e) {
            logUtil.e(TAG, e.toString());
            e.printStackTrace();
        }
    }
}
