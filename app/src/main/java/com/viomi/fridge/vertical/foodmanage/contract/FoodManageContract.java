package com.viomi.fridge.vertical.foodmanage.contract;

import com.viomi.fridge.vertical.common.callback.BasePresenter;
import com.viomi.fridge.vertical.foodmanage.model.entity.FoodDetail;

/**
 * 食材管理主页 Contract
 * Created by William on 2018/2/27.
 */
public interface FoodManageContract {
    interface View {
        void dismissFoodDialog();// 隐藏食材对话框
    }

    interface Presenter extends BasePresenter<View> {
        void saveFood(FoodDetail foodDetail);// 保存食材

        void deleteImage(String filePath);// 删除食材绘图

        void editFood(FoodDetail foodDetail, String id);// 编辑单个食材
    }
}