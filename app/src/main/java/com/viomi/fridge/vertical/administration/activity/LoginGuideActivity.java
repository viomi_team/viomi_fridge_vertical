package com.viomi.fridge.vertical.administration.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseActivity;
import com.viomi.fridge.vertical.common.util.ToolUtil;

import butterknife.BindView;

/**
 * 登录指引 Activity
 * Created by William on 2018/1/30.
 */
public class LoginGuideActivity extends BaseActivity {

    @BindView(R.id.login_guide_img)
    ImageView mImageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        layoutId = R.layout.activity_login_guide;
        mTitle = getResources().getString(R.string.login_guide_title);
        super.onCreate(savedInstanceState);
        mImageView.setImageDrawable(ToolUtil.readBitmap(this, R.drawable.img_login_guide));
    }
}
