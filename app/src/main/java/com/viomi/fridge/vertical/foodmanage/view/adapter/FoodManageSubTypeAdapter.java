package com.viomi.fridge.vertical.foodmanage.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseRecyclerViewAdapter;
import com.viomi.fridge.vertical.foodmanage.model.entity.DefinedFood;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 食材管理种类子类适配器
 * Created by William on 2018/2/26.
 */
public class FoodManageSubTypeAdapter extends BaseRecyclerViewAdapter<FoodManageSubTypeAdapter.SubTypeViewHolder> {
    private List<DefinedFood> mList;
    private String mSelected;

    public FoodManageSubTypeAdapter(List<DefinedFood> list, String selected) {
        mList = list;
        mSelected = selected;
        if (mList == null) mList = new ArrayList<>();
    }

    @Override
    public FoodManageSubTypeAdapter.SubTypeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_holder_food_manage_type, parent, false);
        return new SubTypeViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(FoodManageSubTypeAdapter.SubTypeViewHolder holder, int position) {
        DefinedFood food = mList.get(position);
        holder.textView.setText(food.getName());
        if (food.getName().equals(mSelected)) holder.textView.setSelected(true);
        else holder.textView.setSelected(false);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class SubTypeViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.holder_food_manage_type)
        TextView textView;

        SubTypeViewHolder(View itemView, FoodManageSubTypeAdapter adapter) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> adapter.onItemHolderClick(this, 1000));
        }
    }
}