package com.viomi.fridge.vertical.iot.view.dialog;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.model.preference.ManagePreference;
import com.viomi.fridge.vertical.common.base.CommonDialog;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;
import com.viomi.fridge.vertical.iot.contract.WaterPurifierContract;
import com.viomi.fridge.vertical.iot.model.http.entity.CSeriesPurifierProp;
import com.viomi.fridge.vertical.iot.model.http.entity.MiPurifierProp;
import com.viomi.fridge.vertical.iot.model.http.entity.SSeriesPurifierProp;
import com.viomi.fridge.vertical.iot.model.http.entity.VSeriesPurifierProp;
import com.viomi.fridge.vertical.iot.model.http.entity.WaterPurifierFilter;
import com.viomi.fridge.vertical.iot.model.http.entity.X3PurifierProp;
import com.viomi.fridge.vertical.iot.model.http.entity.X5PurifierProp;
import com.viomi.fridge.vertical.iot.presenter.WaterPurifierPresenter;
import com.viomi.fridge.vertical.iot.view.adapter.WaterPurifierFilterAdapter;
import com.viomi.fridge.vertical.web.activity.BrowserActivity;
import com.viomi.widget.CircleProgressView;
import com.viomi.widget.waveview.WaveHelper;
import com.viomi.widget.waveview.WaveView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * 净水器设备 Dialog
 * Created by William on 2018/2/5.
 */
public class WaterPurifierDialog extends CommonDialog implements WaterPurifierContract.View, AdapterView.OnItemClickListener {
    private static final String TAG = WaterPurifierDialog.class.getSimpleName();
    private String FILTER_URL;// 滤芯购买链接
    public final String PARAM_NAME = "name";// 设备名称
    public final String PARAM_DID = "did";// 设备 id
    public final String PARAM_MODEL = "model";// 设备 model
    private WaterPurifierFilterAdapter mAdapter;
    private List<WaterPurifierFilter> mList;
    private WaveHelper mWaveHelper;
    private int mMinTemp = 40, mMaxTemp = 90;// 最低和最高设定出水温度
    private int mSmallMin = 120, mMiddleMin, mBigMin, mBigMax;
    private String mModel;
    private boolean mIsTempSetting, mIsSmallSetting, mIsMiddleSetting, mIsBigSetting;// 是否在设定
    @BindString(R.string.iot_value_default)
    String mDefaultValue;
    private WaterPurifierContract.Presenter mPresenter;

    @BindView(R.id.water_purifier_filter)
    RecyclerView mRecyclerView;// 滤芯

    @BindView(R.id.water_purifier_wave)
    WaveView mWaveView;

    @BindView(R.id.water_purifier_data_layout)
    LinearLayout mDataLinearLayout;// 净水器数据布局
    @BindView(R.id.water_purifier_filter_layout)
    LinearLayout mFilterLinearLayout;// 滤芯详情布局
    @BindView(R.id.water_purifier_second_layout)
    LinearLayout mSecondLinearLayout;// 第二列
    @BindView(R.id.water_purifier_third_layout)
    LinearLayout mThirdLinearLayout;// 第三列
    @BindView(R.id.water_purifier_flow_middle_title)
    LinearLayout mMiddleLinearLayout;// 中杯设定
    @BindView(R.id.water_purifier_temp_content)
    LinearLayout mTempSettingLinearLayout;// 出水温度展开内容
    @BindView(R.id.water_purifier_flow_content)
    LinearLayout mFlowSettingLinearLayout;// 出水流量展开内容

    @BindView(R.id.water_purifier_flow_middle_content)
    RelativeLayout mMiddleRelativeLayout;// 中杯设定

    @BindView(R.id.water_purifier_temp_setting)
    CardView mTempSettingCardView;// 出水温度设定布局
    @BindView(R.id.water_purifier_flow_setting)
    CardView mFlowSettingCardView;// 出水流量设定布局

    @BindView(R.id.water_purifier_filter_one)
    CircleProgressView mOneCircleProgressView;// 第一个滤芯
    @BindView(R.id.water_purifier_filter_two)
    CircleProgressView mTwoCircleProgressView;// 第二个滤芯
    @BindView(R.id.water_purifier_filter_three)
    CircleProgressView mThreeCircleProgressView;// 第三个滤芯
    @BindView(R.id.water_purifier_filter_four)
    CircleProgressView mFourCircleProgressView;// 第四个滤芯

    @BindView(R.id.water_purifier_name)
    TextView mNameTextView;// 设备名称
    @BindView(R.id.water_purifier_tds_value)
    TextView mTDSTextView;// TDS 值
    @BindView(R.id.water_purifier_temp)
    TextView mWaterTempTextView;// 自来水温
    @BindView(R.id.water_purifier_uv)
    TextView mUVTextView;// UV杀菌
    @BindView(R.id.water_purifier_first)
    TextView mFirstTextView;// 第三列第一个标题
    @BindView(R.id.water_purifier_first_value)
    TextView mFirstValueTextView;// 第三列第一个标题值
    @BindView(R.id.water_purifier_second)
    TextView mSecTextView;// 第三列第二个标题
    @BindView(R.id.water_purifier_second_value)
    TextView mSecValueTextView;// 第三列第二个标题
    @BindView(R.id.water_purifier_filter_title)
    TextView mFilterTitleTextView;// 滤芯名称
    @BindView(R.id.water_purifier_filter_desc1)
    TextView mFilterDesc1TextView;// 滤芯描述
    @BindView(R.id.water_purifier_filter_desc2)
    TextView mFilterDesc2TextView;// 滤芯描述
    @BindView(R.id.water_purifier_filter_desc3)
    TextView mFilterDesc3TextView;// 滤芯描述
    @BindView(R.id.water_purifier_filter_remain)
    TextView mFilterRemainTextView;// 滤芯剩余
    @BindView(R.id.water_purifier_filter_per)
    TextView mFilterPerTextView;// 滤芯百分比
    @BindView(R.id.water_purifier_temp_value)
    TextView mTempTextView;// 设定出水温度
    @BindView(R.id.water_purifier_temp_tip)
    TextView mTempTipTextView;// 出水温度提示
    @BindView(R.id.water_purifier_temp_min)
    TextView mTempMinTextView;// 最低设定温度
    @BindView(R.id.water_purifier_temp_max)
    TextView mTempMaxTextView;// 最高设定温度
    @BindView(R.id.water_purifier_flow_value)
    TextView mFlowTextView;// 出水流量
    @BindView(R.id.water_purifier_flow_small_value)
    TextView mSmallTextView;// 小杯流量
    @BindView(R.id.water_purifier_flow_small_min)
    TextView mSmallMinTextView;// 小杯最低流量
    @BindView(R.id.water_purifier_flow_small_max)
    TextView mSmallMaxTextView;// 小杯最高流量
    @BindView(R.id.water_purifier_flow_middle_value)
    TextView mMiddleTextView;// 中杯流量
    @BindView(R.id.water_purifier_flow_middle_min)
    TextView mMiddleMinTextView;// 中杯最低流量
    @BindView(R.id.water_purifier_flow_middle_max)
    TextView mMiddleMaxTextView;// 中杯最高流量
    @BindView(R.id.water_purifier_flow_big_value)
    TextView mBigTextView;// 大杯流量
    @BindView(R.id.water_purifier_flow_big_min)
    TextView mBigMinTextView;// 大杯最低流量
    @BindView(R.id.water_purifier_flow_big_max)
    TextView mBigMaxTextView;// 大杯最高流量
    @BindView(R.id.water_purifier_filter_buy)
    TextView mBuyTextView;// 立即购买

    @BindView(R.id.water_purifier_temp_bar)
    SeekBar mTempSeekBar;// 出水温度设定
    @BindView(R.id.water_purifier_flow_small_bar)
    SeekBar mSmallSeekBar;// 小杯流量设定
    @BindView(R.id.water_purifier_flow_middle_bar)
    SeekBar mMiddleSeekBar;// 中杯流量设定
    @BindView(R.id.water_purifier_flow_big_bar)
    SeekBar mBigSeekBar;// 大杯流量设定

    @BindView(R.id.water_purifier_line)
    View mLineView;

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.dialog_water_purifier;
    }

    @Override
    protected void initWithOnCreateDialog(View view) {
        mPresenter = new WaterPurifierPresenter();
        mNameTextView.setText(getArguments() == null ? "" : getArguments().getString(PARAM_NAME));
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 4));
        mList = new ArrayList<>();
        mAdapter = new WaterPurifierFilterAdapter(mList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(this);
        mModel = getArguments() == null ? "" : getArguments().getString(PARAM_MODEL);

        mWaveView.setBorder(0, 0);
        mWaveView.setWaveColor(
                Color.parseColor("#FFFFFF"),
                Color.parseColor("#FFFFFF"));
        mWaveHelper = new WaveHelper(mWaveView);

        mTempSettingLinearLayout.setVisibility(View.GONE);
        mFlowSettingLinearLayout.setVisibility(View.GONE);

        mTempTextView.setText(mDefaultValue);
        mFlowTextView.setText(mDefaultValue);
        mTempMinTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_value), mMinTemp));
        mTempMaxTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_value), mMaxTemp));
        mTempSeekBar.setMax(mMaxTemp - mMinTemp);
        mTempSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mTempTipTextView.setText(switchTempTip(progress + mMinTemp));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mIsTempSetting = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mPresenter.setWaterTemp(seekBar.getProgress() + mMinTemp);
            }
        });

        mSmallSeekBar.setMax(300 - mSmallMin);
        mSmallMinTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_flow_value), mSmallMin));
        mSmallMaxTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_flow_value), 300));
        mSmallSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mSmallTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_flow_value), progress + mSmallMin));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mIsSmallSetting = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mPresenter.setWaterFlow(seekBar.getProgress() + mSmallMin, 0);
            }
        });

        mMiddleSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mMiddleTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_flow_value), progress + mMiddleMin));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mIsMiddleSetting = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mPresenter.setWaterFlow(seekBar.getProgress() + mMiddleMin, 1);
            }
        });

        mBigSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mBigTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_flow_value), progress + mBigMin));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mIsBigSetting = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (mLineView.getVisibility() == View.GONE)
                    mPresenter.setWaterFlow(seekBar.getProgress() + mBigMin, 1);
                else mPresenter.setWaterFlow(seekBar.getProgress() + mBigMin, 2);
            }
        });
        if (ManagePreference.getInstance().getDebug()) {
            FILTER_URL = AppConstants.URL_VMALL_DEBUG + "/index.html#/detail/";
        } else FILTER_URL = AppConstants.URL_VMALL_RELEASE + "/index.html#/detail/";
        if (FridgePreference.getInstance().getModel().equals(AppConstants.MODEL_JD))
            mBuyTextView.setVisibility(View.GONE);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null && dialog.getWindow() != null) {
            dialog.getWindow().setLayout(820, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.setCanceledOnTouchOutside(true);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe(this);
        mPresenter.switchModel(mModel, getArguments() == null ? "" : getArguments().getString(PARAM_DID));
        mWaveHelper.start();
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.unSubscribe();
        mWaveHelper.cancel();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mPresenter != null) mPresenter = null;
        if (mAdapter != null) mAdapter = null;
        if (mList != null) {
            mList.clear();
            mList = null;
        }
    }

    @Override
    public void showMiUi() {
        mSecondLinearLayout.setVisibility(View.GONE);
        mThirdLinearLayout.setVisibility(View.GONE);
        mTempSettingCardView.setVisibility(View.GONE);
        mFlowSettingCardView.setVisibility(View.GONE);
        mTDSTextView.setText(mDefaultValue);
        mPresenter.initFourFilters(mList);
        mAdapter.notifyDataSetChanged();
        mOneCircleProgressView.setText("1");
        mTwoCircleProgressView.setText("2");
        mThreeCircleProgressView.setText("3");
        mFourCircleProgressView.setText("4");
    }

    @Override
    public void showVSeriesUi() {
        mTempSettingCardView.setVisibility(View.GONE);
        mFlowSettingCardView.setVisibility(View.GONE);
        mTDSTextView.setText(mDefaultValue);
        mWaterTempTextView.setText(mDefaultValue);
        mUVTextView.setText(mDefaultValue);
        mFirstTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_water_gage));
        mFirstValueTextView.setText(mDefaultValue);
        mSecTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_water_switch));
        mSecValueTextView.setText(mDefaultValue);
        mPresenter.initFourFilters(mList);
        mAdapter.notifyDataSetChanged();
        mOneCircleProgressView.setText("1");
        mTwoCircleProgressView.setText("2");
        mThreeCircleProgressView.setText("3");
        mFourCircleProgressView.setText("4");
    }

    @Override
    public void showSSeriesUi() {
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        mTempSettingCardView.setVisibility(View.GONE);
        mFlowSettingCardView.setVisibility(View.GONE);
        mSecTextView.setVisibility(View.INVISIBLE);
        mSecValueTextView.setVisibility(View.INVISIBLE);
        mTDSTextView.setText(mDefaultValue);
        mUVTextView.setText(mDefaultValue);
        mWaterTempTextView.setText(mDefaultValue);
        mFirstTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_water_rationing));
        mFirstValueTextView.setText(mDefaultValue);
        mPresenter.initThreeFilters(mList);
        mAdapter.notifyDataSetChanged();
        mOneCircleProgressView.setVisibility(View.GONE);
        mTwoCircleProgressView.setText("1");
        mThreeCircleProgressView.setText("2");
        mFourCircleProgressView.setText("3");
    }

    @Override
    public void showCSeriesUi() {
        mThirdLinearLayout.setVisibility(View.GONE);
        mTempSettingCardView.setVisibility(View.GONE);
        mFlowSettingCardView.setVisibility(View.GONE);
        mTDSTextView.setText(mDefaultValue);
        mWaterTempTextView.setText(mDefaultValue);
        mUVTextView.setText(mDefaultValue);
        mPresenter.initFourFilters(mList);
        mAdapter.notifyDataSetChanged();
        mOneCircleProgressView.setText("1");
        mTwoCircleProgressView.setText("2");
        mThreeCircleProgressView.setText("3");
        mFourCircleProgressView.setText("4");
    }

    @Override
    public void showX3Ui() {
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        mSecTextView.setVisibility(View.INVISIBLE);
        mSecValueTextView.setVisibility(View.INVISIBLE);
        mLineView.setVisibility(View.GONE);
        mMiddleLinearLayout.setVisibility(View.GONE);
        mMiddleRelativeLayout.setVisibility(View.GONE);
        mTDSTextView.setText(mDefaultValue);
        mWaterTempTextView.setText(mDefaultValue);
        mUVTextView.setText(mDefaultValue);
        mFirstTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_water_rationing));
        mFirstValueTextView.setText(mDefaultValue);
        mBigMin = 310;
        mBigMax = 1000;
        mBigMinTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_flow_value), mBigMin));
        mBigMaxTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_flow_value), mBigMax));
        mBigSeekBar.setMax(mBigMax - mBigMin);
        mPresenter.initThreeFilters(mList);
        mAdapter.notifyDataSetChanged();
        mOneCircleProgressView.setVisibility(View.GONE);
        mTwoCircleProgressView.setText("1");
        mThreeCircleProgressView.setText("2");
        mFourCircleProgressView.setText("3");
    }

    @Override
    public void showX5Ui() {
        mThirdLinearLayout.setVisibility(View.GONE);
        mTDSTextView.setText(mDefaultValue);
        mWaterTempTextView.setText(mDefaultValue);
        mUVTextView.setText(mDefaultValue);
        mMiddleMin = 310;
        mMiddleMinTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_flow_value), mMiddleMin));
        mMiddleMaxTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_flow_value), 450));
        mMiddleSeekBar.setMax(450 - mMiddleMin);
        mBigMin = 460;
        mBigMax = 2000;
        mBigMinTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_flow_value), mBigMin));
        mBigMaxTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_flow_value), mBigMax));
        mBigSeekBar.setMax(mBigMax - mBigMin);
        mPresenter.initFourFilters(mList);
        mAdapter.notifyDataSetChanged();
        mOneCircleProgressView.setText("1");
        mTwoCircleProgressView.setText("2");
        mThreeCircleProgressView.setText("3");
        mFourCircleProgressView.setText("4");
    }

    @Override
    public void refreshMiUi(MiPurifierProp prop) {
        mTDSTextView.setText(String.valueOf(prop.getTds_out()));
        mList.get(0).setPercent(prop.getF1_Life());
        mList.get(1).setPercent(prop.getF2_Life());
        mList.get(2).setPercent(prop.getF3_Life());
        mList.get(3).setPercent(prop.getF4_Life());
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void refreshVSeriesUi(VSeriesPurifierProp prop) {
        mTDSTextView.setText(String.valueOf(prop.getTds_out()));
        mWaterTempTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_value), prop.getTemperature()));
        mUVTextView.setText(prop.getUv_state());
        mFirstValueTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_water_gage_value), prop.getPress() / 1000.0));
        mSecValueTextView.setText(prop.getElecval_state());
        mList.get(0).setPercent(prop.getF1_Life());
        mList.get(1).setPercent(prop.getF2_Life());
        mList.get(2).setPercent(prop.getF3_Life());
        mList.get(3).setPercent(prop.getF4_Life());
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void refreshSSeriesUi(SSeriesPurifierProp prop) {
        mTDSTextView.setText(String.valueOf(prop.getTds_out()));
        mWaterTempTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_value), prop.getTemperature()));
        mUVTextView.setText(prop.getUv_state());
        mFirstValueTextView.setText(prop.getOpenStatus());
        mList.get(0).setPercent(prop.getF1_Life());
        mList.get(1).setPercent(prop.getF2_Life());
        mList.get(2).setPercent(prop.getF3_Life());
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void refreshCSeriesUi(CSeriesPurifierProp prop) {
        mTDSTextView.setText(String.valueOf(prop.getTds_out()));
        mWaterTempTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_value), prop.getTemperature()));
        mUVTextView.setText(prop.getUv_state());
        mList.get(0).setPercent(prop.getF1_Life());
        mList.get(1).setPercent(prop.getF2_Life());
        mList.get(2).setPercent(prop.getF3_Life());
        mList.get(3).setPercent(prop.getF4_Life());
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void refreshX3Ui(X3PurifierProp prop) {
        mTDSTextView.setText(String.valueOf(prop.getTds_out()));
        mWaterTempTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_value), prop.getTemperature()));
        mUVTextView.setText(prop.getUv_state());
        mFirstValueTextView.setText(prop.getOpenStatus());
        mList.get(0).setPercent(prop.getF1_Life());
        mList.get(1).setPercent(prop.getF2_Life());
        mList.get(2).setPercent(prop.getF3_Life());
        mAdapter.notifyDataSetChanged();
        mTempTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_value), prop.getSetup_tempe()));
        mFlowTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_flow_value), prop.getSetup_flow()));
        if (!mIsTempSetting) {
            mMinTemp = prop.getMin_set_tempe();
            mTempSeekBar.setMax(mMaxTemp - mMinTemp);
            mTempMinTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_value), mMinTemp));
            mTempSeekBar.setProgress(prop.getCustom_tempe1() - mMinTemp);
        }
        if (!mIsSmallSetting) {
            mSmallTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_flow_value), prop.getCustom_flow0()));
            mSmallSeekBar.setProgress(prop.getCustom_flow0() - mSmallMin);
        }
        if (!mIsBigSetting) {
            mBigTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_flow_value), prop.getCustom_flow1()));
            mBigSeekBar.setProgress(prop.getCustom_flow1() - mBigMin);
        }
    }

    @Override
    public void refreshX5Ui(X5PurifierProp prop) {
        mTDSTextView.setText(String.valueOf(prop.getTds_out()));
        mWaterTempTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_value), prop.getTemperature()));
        mUVTextView.setText(prop.getUv_state());
        mList.get(0).setPercent(prop.getF1_Life());
        mList.get(1).setPercent(prop.getF2_Life());
        mList.get(2).setPercent(prop.getF3_Life());
        mList.get(3).setPercent(prop.getF4_Life());
        mAdapter.notifyDataSetChanged();
        mTempTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_value), prop.getSetup_tempe()));
        mFlowTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_flow_value), prop.getSetup_flow()));
        if (!mIsTempSetting) mTempSeekBar.setProgress(prop.getCustom_tempe1() - mMinTemp);
        if (!mIsSmallSetting) {
            mSmallTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_flow_value), prop.getCustom_flow0()));
            mSmallSeekBar.setProgress(prop.getCustom_flow0() - mSmallMin);
        }
        if (!mIsMiddleSetting) {
            mMiddleTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_flow_value), prop.getCustom_flow1()));
            mMiddleSeekBar.setProgress(prop.getCustom_flow1() - mMiddleMin);
        }
        if (!mIsBigSetting) {
            mBigTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_flow_value), prop.getCustom_flow2()));
            mBigSeekBar.setProgress(prop.getCustom_flow2() - mBigMin);
        }
    }

    @Override
    public void setIsTempSetting() {
        this.mIsTempSetting = false;
    }

    @Override
    public void setIsSmallSetting() {
        this.mIsSmallSetting = false;
    }

    @Override
    public void setIsMiddleSetting() {
        if (mLineView.getVisibility() == View.GONE) this.mIsBigSetting = false;
        else this.mIsMiddleSetting = false;
    }

    @Override
    public void setIsBigSetting() {
        this.mIsBigSetting = false;
    }

    @OnClick(R.id.water_purifier_filter_close)
    public void filterClose() {
        mDataLinearLayout.setVisibility(View.VISIBLE);
        mFilterLinearLayout.setVisibility(View.GONE);
        mDataLinearLayout.setAnimation(AnimationUtils.makeInAnimation(FridgeApplication.getContext(), true));
        mFilterLinearLayout.setAnimation(AnimationUtils.makeOutAnimation(FridgeApplication.getContext(), true));
    }

    @OnClick(R.id.water_purifier_filter_buy)
    public void filterBuy() {
        int skuId = 0;
        if (mFilterTitleTextView.getText().toString().equals(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_filter_pp))) {
            skuId = 24;
        } else if (mFilterTitleTextView.getText().toString().equals(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_filter_front))) {
            skuId = 16;
        } else if (mFilterTitleTextView.getText().toString().equals(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_filter_ro))) {
            switch (mModel) {
                case AppConstants.YUNMI_WATERPURI_V2:
                    skuId = 19;
                    break;
                case AppConstants.YUNMI_WATERPURI_S2:
                    skuId = 20;
                    break;
                case AppConstants.YUNMI_WATERPURI_X3:
                case AppConstants.YUNMI_WATERPURI_S1:
                    skuId = 30;
                    break;
                default:
                    skuId = 18;
                    break;
            }
        } else if (mFilterTitleTextView.getText().toString().equals(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_filter_behind))) {
            skuId = 17;
        } else if (mFilterTitleTextView.getText().toString().equals(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_filter_intelligence))) {
            skuId = 21;
        } else if (mFilterTitleTextView.getText().toString().equals(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_filter_pure_tank))) {
            skuId = -1;
        }
        String url = FILTER_URL + skuId + "?isFromNative=1";
        logUtil.d(TAG, url);
        Intent intent = new Intent(getActivity(), BrowserActivity.class);
        intent.putExtra("url", url);
        startActivity(intent);
    }

    @OnClick(R.id.water_purifier_temp_layout)
    public void tempSetting() {
        if (mTempSettingLinearLayout.getVisibility() == View.VISIBLE) {
            mTempSettingLinearLayout.setVisibility(View.GONE);
            mTempTextView.setSelected(false);
        } else {
            mTempSettingLinearLayout.setVisibility(View.VISIBLE);
            mTempTextView.setSelected(true);
        }
    }

    @OnClick(R.id.water_purifier_flow_layout)
    public void flowSetting() {
        if (mFlowSettingLinearLayout.getVisibility() == View.VISIBLE) {
            mFlowSettingLinearLayout.setVisibility(View.GONE);
            mFlowTextView.setSelected(false);
        } else {
            mFlowSettingLinearLayout.setVisibility(View.VISIBLE);
            mFlowTextView.setSelected(true);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mDataLinearLayout.setVisibility(View.GONE);
        mFilterLinearLayout.setVisibility(View.VISIBLE);
        mDataLinearLayout.setAnimation(AnimationUtils.makeOutAnimation(FridgeApplication.getContext(), false));
        mFilterLinearLayout.setAnimation(AnimationUtils.makeInAnimation(FridgeApplication.getContext(), false));
        String str = mList.get(position).getPercent() < 0 ? mDefaultValue : mList.get(position).getPercent() + "%";
        mFilterPerTextView.setText(str);
        mFilterTitleTextView.setText(mList.get(position).getName());
        mFilterDesc1TextView.setVisibility(View.VISIBLE);
        mOneCircleProgressView.setProgress(0);
        mTwoCircleProgressView.setProgress(0);
        mThreeCircleProgressView.setProgress(0);
        mFourCircleProgressView.setProgress(0);
        if (mList.get(position).getName().equals(getResources().getString(R.string.iot_water_purifier_filter_pp))) {
            mFilterDesc3TextView.setVisibility(View.GONE);
            mFilterDesc1TextView.setText(getResources().getString(R.string.iot_water_purifier_filter_pp_desc1));
            mFilterDesc2TextView.setText(getResources().getString(R.string.iot_water_purifier_filter_pp_desc2));
            mFilterRemainTextView.setText(getResources().getString(R.string.iot_water_purifier_filter_pp_remain));
            mOneCircleProgressView.setProgress(mList.get(position).getPercent());
        } else if (mList.get(position).getName().equals(getResources().getString(R.string.iot_water_purifier_filter_front))) {
            mFilterDesc3TextView.setVisibility(View.GONE);
            mFilterDesc1TextView.setText(getResources().getString(R.string.iot_water_purifier_filter_front_desc1));
            mFilterDesc2TextView.setText(getResources().getString(R.string.iot_water_purifier_filter_front_desc2));
            mFilterRemainTextView.setText(getResources().getString(R.string.iot_water_purifier_filter_front_remain));
            mTwoCircleProgressView.setProgress(mList.get(position).getPercent());
        } else if (mList.get(position).getName().equals(getResources().getString(R.string.iot_water_purifier_filter_ro))) {
            mFilterDesc3TextView.setVisibility(View.GONE);
            mFilterDesc1TextView.setVisibility(View.GONE);
            mFilterDesc2TextView.setText(getResources().getString(R.string.iot_water_purifier_filter_ro_desc3));
            mFilterRemainTextView.setText(getResources().getString(R.string.iot_water_purifier_filter_ro_remain));
            mThreeCircleProgressView.setProgress(mList.get(position).getPercent());
        } else if (mList.get(position).getName().equals(getResources().getString(R.string.iot_water_purifier_filter_behind))) {
            mFilterDesc3TextView.setVisibility(View.GONE);
            mFilterDesc1TextView.setText(getResources().getString(R.string.iot_water_purifier_filter_behind_desc1));
            mFilterDesc2TextView.setText(getResources().getString(R.string.iot_water_purifier_filter_behind_desc2));
            mFilterRemainTextView.setText(getResources().getString(R.string.iot_water_purifier_filter_behind_remain));
            mFourCircleProgressView.setProgress(mList.get(position).getPercent());
        } else if (mList.get(position).getName().equals(getResources().getString(R.string.iot_water_purifier_filter_intelligence))) {
            mFilterDesc1TextView.setText(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_filter_intelligence_desc2));
            mFilterDesc3TextView.setText(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_filter_intelligence_desc3));
            mFilterDesc2TextView.setText(getResources().getString(R.string.iot_water_purifier_filter_intelligence_desc4));
            mFilterRemainTextView.setText(getResources().getString(R.string.iot_water_purifier_filter_intelligence_remain));
            mTwoCircleProgressView.setProgress(mList.get(position).getPercent());
        } else if (mList.get(position).getName().equals(getResources().getString(R.string.iot_water_purifier_filter_pure_tank))) {
            mFilterDesc3TextView.setVisibility(View.GONE);
            mFilterDesc1TextView.setText(getResources().getString(R.string.iot_water_purifier_filter_pure_tank_desc1));
            mFilterDesc2TextView.setText(getResources().getString(R.string.iot_water_purifier_filter_pure_tank_desc2));
            mFilterRemainTextView.setText(getResources().getString(R.string.iot_water_purifier_filter_pure_tank_remain));
            mFourCircleProgressView.setProgress(mList.get(position).getPercent());
        }
    }

    /**
     * 根据温度显示对应提示
     */
    private String switchTempTip(int temp) {
        String str;
        switch (temp) {
            case 40:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_tip_40);
                break;
            case 50:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_tip_50);
                break;
            case 60:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_tip_60);
                break;
            case 75:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_tip_75);
                break;
            case 80:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_tip_80);
                break;
            case 85:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_tip_85);
                break;
            case 90:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_tip_90);
                break;
            default:
                str = String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_value), temp);
                break;
        }
        return str;
    }
}