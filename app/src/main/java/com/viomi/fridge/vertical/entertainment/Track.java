package com.viomi.fridge.vertical.entertainment;

public class Track {

    private String title;
    private String artist;
    private String album;
    private String url;
    private String img;
    private String lrcUrl;
    private  boolean isMusic;

    public Track() {
    }

    public Track(String title, String artist, String album, String url, String img, String lrcUrl) {
        this.title = title;
        this.artist = artist;
        this.album = album;
        this.url = url;
        this.img = img;
        this.lrcUrl = lrcUrl;
    }

    public Track(String title, String artist, String album, String url, String img, String lrcUrl, boolean isMusic) {
        this.title = title;
        this.artist = artist;
        this.album = album;
        this.url = url;
        this.img = img;
        this.lrcUrl = lrcUrl;
        this.isMusic = isMusic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getLrcUrl() {
        return lrcUrl;
    }

    public void setLrcUrl(String lrcUrl) {
        this.lrcUrl = lrcUrl;
    }

    public boolean isMusic() {
        return isMusic;
    }

    public void setMusic(boolean music) {
        isMusic = music;
    }
}
