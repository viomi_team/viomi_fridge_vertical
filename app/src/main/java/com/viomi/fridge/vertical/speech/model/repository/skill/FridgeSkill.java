package com.viomi.fridge.vertical.speech.model.repository.skill;

import android.content.Context;
import android.content.Intent;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.callback.AppCallback;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;
import com.viomi.fridge.vertical.fridgecontrol.model.repository.FridgeRepository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 冰箱控制技能
 * Created by William on 2018/8/23.
 */
public class FridgeSkill {
    private static final String TAG = FridgeSkill.class.getSimpleName();

    public static void handle(Context context, String data) {
        try {
            JSONObject jsonObject = new JSONObject(data);
            JSONObject nlu = jsonObject.optJSONObject("nlu");
            if (nlu == null) return;
            JSONObject semantics = nlu.optJSONObject("semantics");
            if (semantics == null) return;
            JSONObject request = semantics.optJSONObject("request");
            if (request == null) return;
            JSONArray slots = request.optJSONArray("slots");
            if (slots == null || slots.length() <= 0) return;
            String value = "";
            for (int i = 0; i < slots.length(); i++) {
                JSONObject jsonItem = slots.optJSONObject(i);
                String name = jsonItem.optString("name");
                if (name.equals("intent")) {
                    value = jsonItem.optString("value");
                    break;
                }
            }

            String content;
            Intent intent;

            switch (value) {
                case "设置冷藏室温度":
                    if (!FridgeRepository.getInstance().getRCSet().equals("on")) {
                        content = "冷藏室处于关闭状态，请先打开冷藏室";
                    } else {
                        String rcTemp = "0";
                        for (int i = 0; i < slots.length(); i++) {
                            JSONObject jsonItem = slots.optJSONObject(i);
                            String name = jsonItem.optString("name");
                            if (name.equals("整数")) {
                                rcTemp = jsonItem.optString("value");
                                break;
                            }
                        }
                        if (Integer.parseInt(rcTemp) < 2 || Integer.parseInt(rcTemp) > 8) {
                            content = "冷藏室只能设置为2度至8度";
                        } else {
                            FridgeRepository.getInstance().setRCRoomTemp(Integer.parseInt(rcTemp), null);
                            content = "正在为你把冷藏室温度调整为" + rcTemp + "度";
                        }
                    }
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "设置变温室温度":
                    if (!ToolUtil.getDeviceModel().equals(AppConstants.MODEL_X5)) {
                        content = "没有变温室哦。";
                    } else if (!FridgeRepository.getInstance().getCCSet().equals("on")) {
                        content = "变温室处于关闭状态，请先打开变温室";
                    } else {
                        String ccTemp = "0";
                        for (int i = 0; i < slots.length(); i++) {
                            JSONObject jsonItem = slots.optJSONObject(i);
                            String name = jsonItem.optString("name");
                            if (name.equals("整数")) {
                                ccTemp = jsonItem.optString("value");
                                break;
                            }
                        }
                        if (Integer.parseInt(ccTemp) < -18 || Integer.parseInt(ccTemp) > 5) {
                            content = "变温室只能设置为零下十八度至五度";
                        } else {
                            AppCallback<String> callback = new AppCallback<String>() {
                                @Override
                                public void onSuccess(String data) {
                                    FridgePreference.getInstance().saveScene("");
                                    RxBus.getInstance().post(BusEvent.MSG_CHANGEABLE_SCENE_UPDATE);
                                }

                                @Override
                                public void onFail(int errorCode, String msg) {

                                }
                            };
                            FridgeRepository.getInstance().setCCRoomTemp(Integer.parseInt(ccTemp), callback);
                            content = "正在为你把变温室温度调整为" + ccTemp + "度";
                        }
                    }
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "设置冷冻室温度":
                    String fcTemp = "0";
                    for (int i = 0; i < slots.length(); i++) {
                        JSONObject jsonItem = slots.optJSONObject(i);
                        String name = jsonItem.optString("name");
                        if (name.equals("整数")) {
                            fcTemp = jsonItem.optString("value");
                            break;
                        }
                    }
                    if (ToolUtil.getDeviceModel().equals(AppConstants.MODEL_X4)) {
                        if (Integer.parseInt(fcTemp) < -23 || Integer.parseInt(fcTemp) > -15) {
                            content = "冷冻室只能设置为零下二十三度至零下十五度";
                        } else {
                            FridgeRepository.getInstance().setFCRoomTemp(Integer.parseInt(fcTemp), null);
                            content = "正在为你把冷冻室温度调整为" + fcTemp + "度";
                        }
                    } else {
                        if (Integer.parseInt(fcTemp) < -24 || Integer.parseInt(fcTemp) > -16) {
                            content = "冷冻室只能设置为零下二十四度至零下十六度";
                        } else {
                            FridgeRepository.getInstance().setFCRoomTemp(Integer.parseInt(fcTemp), null);
                            content = "正在为你把冷冻室温度调整为" + fcTemp + "度";
                        }
                    }
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "设置变温室模式":
                    if (!ToolUtil.getDeviceModel().equals(AppConstants.MODEL_X5)) {
                        content = "没有变温室哦。";
                        RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    } else if (!FridgeRepository.getInstance().getCCSet().equals("on")) {
                        content = "变温室处于关闭状态，请先打开变温室";
                        RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    } else {
                        String scene = "";
                        for (int i = 0; i < slots.length(); i++) {
                            JSONObject jsonItem = slots.optJSONObject(i);
                            String name = jsonItem.optString("name");
                            if (name.equals("变温室模式")) {
                                scene = jsonItem.optString("value");
                                break;
                            }
                        }
                        switch (scene) {
                            case "鱼":
                            case "茶叶":
                            case "冰啤":
                            case "面膜":
                            case "解冻":
                            case "软冷冻":
                            case "干货":
                            case "雪糕":
                            case "鸡蛋":
                            case "冷藏":
                            case "冷冻":
                            case "肉类":
                            case "蔬菜":
                            case "水果":
                            case "剩菜":
                            case "鲜啤":
                            case "母乳":
                                String str = scene;
                                AppCallback<String> callback = new AppCallback<String>() {
                                    @Override
                                    public void onSuccess(String data) {
                                        FridgePreference.getInstance().saveScene(str);
                                        RxBus.getInstance().post(BusEvent.MSG_CHANGEABLE_SCENE_UPDATE);
                                    }

                                    @Override
                                    public void onFail(int errorCode, String msg) {

                                    }
                                };
                                FridgeRepository.getInstance().setCCRoomTemp(switchTemp(scene), callback);
                                content = "正在为你把变温室模式调整为" + scene;
                                RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                                break;
                        }
                    }
                    break;
                case "设置冰箱模式":
                    String mode = "";
                    for (int i = 0; i < slots.length(); i++) {
                        JSONObject jsonItem = slots.optJSONObject(i);
                        String name = jsonItem.optString("name");
                        if (name.equals("冰箱模式")) {
                            mode = jsonItem.optString("value");
                            break;
                        }
                    }
                    switch (mode) {
                        case "智能模式":
                            if (FridgeRepository.getInstance().getMode().equals("smart")) {
                                content = "已经处于智能模式";
                            } else {
                                FridgeRepository.getInstance().setMode("smart", null);
                                content = "正在为你把冰箱模式设置为" + mode;
                            }
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "假日模式":
                            if (FridgeRepository.getInstance().getMode().equals("holiday")) {
                                content = "已经处于假日模式";
                            } else {
                                FridgeRepository.getInstance().setMode("holiday", null);
                                content = "正在为你把冰箱模式设置为" + mode;
                            }
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "速冷模式":
                            if (ToolUtil.getDeviceModel().equals(AppConstants.MODEL_X5)) {
                                content = "没有速冷功能哦";
                            } else if (FridgeRepository.getInstance().getSmartCool().equals("on")) {
                                content = "速冷已经处于开启状态";
                            } else {
                                FridgeRepository.getInstance().setSmartCool(true, null);
                                content = "正在为你开启速冷";
                            }
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "速冻模式":
                            if (FridgeRepository.getInstance().getSmartFreeze().equals("on")) {
                                content = "速冻已经处于开启状态";
                            } else {
                                FridgeRepository.getInstance().setSmartFreeze(true, null);
                                content = "正在为你开启速冻";
                            }
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                    }
                    break;
                case "关闭冰箱模式":
                    String mode2 = "";
                    for (int i = 0; i < slots.length(); i++) {
                        JSONObject jsonItem = slots.optJSONObject(i);
                        String name = jsonItem.optString("name");
                        if (name.equals("冰箱模式")) {
                            mode2 = jsonItem.optString("value");
                            break;
                        }
                    }
                    switch (mode2) {
                        case "速冷模式":
                            if (ToolUtil.getDeviceModel().equals(AppConstants.MODEL_X5)) {
                                content = "没有速冷功能哦";
                            } else if (FridgeRepository.getInstance().getSmartCool().equals("off")) {
                                content = "速冷已经处于关闭状态";
                            } else {
                                FridgeRepository.getInstance().setSmartCool(false, null);
                                content = "正在为你关闭速冷";
                            }
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "速冻模式":
                            if (FridgeRepository.getInstance().getSmartFreeze().equals("off")) {
                                content = "速冻已经处于关闭状态";
                            } else {
                                FridgeRepository.getInstance().setSmartFreeze(false, null);
                                content = "正在为你关闭速冻";
                            }
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                    }
                    break;
                case "查询冷藏室温度":
                    if (!FridgeRepository.getInstance().getRCSet().equals("on")) {
                        content = "冷藏室处于关闭状态，请先打开冷藏室";
                    } else {
                        content = "当前冷藏室温度为" + FridgeRepository.getInstance().getRCSetTemp() + "度";
                    }
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "查询变温室温度":
                    if (!ToolUtil.getDeviceModel().equals(AppConstants.MODEL_X5)) {
                        content = "没有变温室哦。";
                    } else if (!FridgeRepository.getInstance().getCCSet().equals("on")) {
                        content = "变温室处于关闭状态，请先打开变温室";
                    } else {
                        content = "当前变温室温度为" + FridgeRepository.getInstance().getCCSetTemp() + "度";
                    }
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "查询冷冻室温度":
                    content = "当前冷冻室温度为" + FridgeRepository.getInstance().getFCSetTemp() + "度";
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "查询变温室模式":
                    if (!ToolUtil.getDeviceModel().equals(AppConstants.MODEL_X5)) {
                        content = "没有变温室哦。";
                    } else {
                        if (FridgePreference.getInstance().getScene().equals("")) {
                            content = "变温室暂未设置任何模式";
                        } else {
                            content = "当前变温室模式为" + FridgePreference.getInstance().getScene();
                        }
                    }
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "查询冰箱模式":
                    switch (FridgeRepository.getInstance().getMode()) {
                        case "smart":
                            content = "当前冰箱处于智能模式";
                            break;
                        case "holiday":
                            content = "当前冰箱处于假日模式";
                            break;
                        default:
                            content = "当前冰箱处于手动模式";
                            break;
                    }
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "设置冷藏变温区模式":
                    if (!ToolUtil.getDeviceModel().equals(AppConstants.MODEL_X5)) {
                        content = "没有冷藏变温区哦。";
                        RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    } else {
                        String ccMode = "";
                        for (int i = 0; i < slots.length(); i++) {
                            JSONObject jsonItem = slots.optJSONObject(i);
                            String name = jsonItem.optString("name");
                            if (name.equals("冷藏变温区模式")) {
                                ccMode = jsonItem.optString("value");
                                break;
                            }
                        }
                        switch (ccMode) {
                            case "鲜果":
                                if (FridgeRepository.getInstance().isFreshFruit()) {
                                    content = "冷藏变温区已经处于鲜果模式";
                                } else {
                                    FridgeRepository.getInstance().setFreshFruit(true, null);
                                    content = "正在为你把冷藏变温区设置为鲜果模式";
                                }
                                RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                                break;
                            case "零度保鲜":
                                if (FridgeRepository.getInstance().isRetainFresh()) {
                                    content = "冷藏变温区已经处于零度保鲜模式";
                                } else {
                                    FridgeRepository.getInstance().setRetainFresh(true, null);
                                    content = "正在为你把冷藏变温区设置为零度保鲜模式";
                                }
                                RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                                break;
                            case "冰镇":
                                if (FridgeRepository.getInstance().isIced()) {
                                    content = "冷藏变温区已经处于冰镇模式";
                                } else {
                                    FridgeRepository.getInstance().setIced(true, null);
                                    content = "正在为你把冷藏变温区设置为冰镇模式";
                                }
                                RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                                break;
                        }
                    }
                    break;
                case "关闭冷藏变温区模式":
                    if (!ToolUtil.getDeviceModel().equals(AppConstants.MODEL_X5)) {
                        content = "没有冷藏变温区哦。";
                        RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    } else {
                        String ccMode = "";
                        for (int i = 0; i < slots.length(); i++) {
                            JSONObject jsonItem = slots.optJSONObject(i);
                            String name = jsonItem.optString("name");
                            if (name.equals("冷藏变温区模式")) {
                                ccMode = jsonItem.optString("value");
                                break;
                            }
                        }
                        switch (ccMode) {
                            case "鲜果":
                                if (!FridgeRepository.getInstance().isFreshFruit()) {
                                    content = "冷藏变温区没有处于鲜果模式";
                                } else {
                                    FridgeRepository.getInstance().setFreshFruit(false, null);
                                    content = "正在为你关闭冷藏变温区的鲜果模式";
                                }
                                RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                                break;
                            case "零度保鲜":
                                if (!FridgeRepository.getInstance().isRetainFresh()) {
                                    content = "冷藏变温区没有处于零度保鲜模式";
                                } else {
                                    FridgeRepository.getInstance().setRetainFresh(false, null);
                                    content = "正在为你关闭冷藏变温区的零度保鲜模式";
                                }
                                RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                                break;
                            case "冰镇":
                                if (!FridgeRepository.getInstance().isIced()) {
                                    content = "冷藏变温区已经处于冰镇模式";
                                } else {
                                    FridgeRepository.getInstance().setIced(false, null);
                                    content = "正在为你关闭冷藏变温区的冰镇模式";
                                }
                                RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                                break;
                        }
                    }
                    break;
                case "查询冷藏变温区模式":
                    if (!ToolUtil.getDeviceModel().equals(AppConstants.MODEL_X5)) {
                        content = "没有冷藏变温区哦。";
                        RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    } else {
                        if (FridgeRepository.getInstance().isFreshFruit()) content = "冷藏变温区处于鲜果模式";
                        else if (FridgeRepository.getInstance().isRetainFresh())
                            content = "冷藏变温区处于零度保鲜模式";
                        else if (FridgeRepository.getInstance().isIced()) content = "冷藏变温区处于冰镇模式";
                        else content = "冷藏变温区处于关闭状态";
                        RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    }
                    break;
                case "接听电话":
                    intent = new Intent();
                    intent.setAction("com.action.audiocall.accept");
                    intent.putExtra("package", "com.viomi.fridge.vertical");
                    context.sendBroadcast(intent);
                    break;
                case "挂断电话":
                    intent = new Intent();
                    intent.setAction("com.action.audiocall.reject");
                    intent.putExtra("package", "com.viomi.fridge.vertical");
                    context.sendBroadcast(intent);
                    break;
            }
        } catch (JSONException e) {
            logUtil.e(TAG, e.toString());
            e.printStackTrace();
        }
    }

    private static int switchTemp(String name) {
        int[] tempArray = FridgeApplication.getContext().getResources().getIntArray(R.array.changeable_scene_temp);
        int temp = 0;
        switch (name) {
            case "鱼":
                temp = tempArray[0];
                break;
            case "茶叶":
                temp = tempArray[1];
                break;
            case "冰啤":
                temp = tempArray[2];
                break;
            case "面膜":
                temp = tempArray[3];
                break;
            case "解冻":
                temp = tempArray[4];
                break;
            case "软冷冻":
                temp = tempArray[5];
                break;
            case "干货":
                temp = tempArray[6];
                break;
            case "雪糕":
                temp = tempArray[7];
                break;
            case "鸡蛋":
                temp = tempArray[8];
                break;
            case "冷藏":
                temp = tempArray[9];
                break;
            case "冷冻":
                temp = tempArray[10];
                break;
            case "肉类":
                temp = tempArray[11];
                break;
            case "蔬菜":
                temp = tempArray[12];
                break;
            case "水果":
                temp = tempArray[13];
                break;
            case "剩菜":
                temp = tempArray[14];
                break;
            case "鲜啤":
                temp = tempArray[15];
                break;
            case "母乳":
                temp = tempArray[16];
                break;
        }
        return temp;
    }
}
