package com.viomi.fridge.vertical.fridgecontrol.model.serial.parser;


import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;
import com.viomi.fridge.vertical.fridgecontrol.model.serial.entity.DeviceParams;
import com.viomi.fridge.vertical.fridgecontrol.util.SerialDataUtil;

/**
 * 串口发送数据解析
 * Created by William on 2018/1/9.
 */
public class SerialWriteParser {
    private static final String TAG = SerialWriteParser.class.getSimpleName();

    /**
     * 发送数据前进行解析
     *
     * @param params:   冰箱数据实例
     * @param data:     解析后数据（数组每一个数均是 8 位 byte）
     * @param isModify: 是否查询状态（适用于 462 和 521）
     */
    public static boolean parse(DeviceParams params, int[] data, String deviceModel, boolean isModify) {
        if (params == null) {
            logUtil.e(TAG, "send params is null!");
            return false;
        }
        if (data == null) {
            logUtil.e(TAG, "send data is null!");
            return false;
        }
        // 开始解析
        int i = 0;// 数组下标
        int mode, function;
        switch (deviceModel) {
            case AppConstants.MODEL_X1:
                data[i] = AppConstants.X1_SERIAL_DATA_START;// 开始位

                i++;
                data[i] = params.getMode();// 冰箱工作模式

                if (params.isQuick_cold()) { // 速冷
                    data[i] = AppConstants.MODE_QUICK_COLD;
                }
                if (params.isQuick_freeze()) {   // 速冻
                    data[i] = AppConstants.MODE_QUICK_FREEZE;
                }
                i++;
                data[i] = 0;// 环温（没用到，传 0 即可）

                i++;
                // 冷藏室设置温度
                if (params.isCold_switch()) { // 开启
                    data[i] = params.getCold_temp_set() + 50;
                } else { // 关闭
                    data[i] = 0;
                }
                i++;
                // 变温室设置温度
                if (params.isChangeable_switch()) { // 开启
                    data[i] = params.getChangeable_temp_set() + 50;
                } else { // 关闭
                    data[i] = 0;
                }
                i++;
                data[i] = params.getFreezing_temp_set() + 50;// 冷冻室设置温度

                i++;
                // 强制启动和化霜
                int action = 0;
                action = rcfForcedStartParse(params.isRcf_forced(), action);
                action = forcedFrostParse(params.isFc_forced_frost(), action);
                data[i] = action;
                i++;
                // 诊断功能 1 (冷冻风机，变温风门电加热，变温风门，冷藏风门电加热，冷藏风门，冷冻电加热，压缩机)，暂无用到
                data[i] = 0;
                i++;
                // 诊断功能 2 (冷冻时间，硬件自检功能，商检功能，缩时功能，一键净化，冷藏照明灯，门装饰灯，诊断功能开启)
                int diagnose2 = 0;
                diagnose2 = oneKeyCleanParse(params.isOne_key_clean(), diagnose2);// 一键净化

                diagnose2 = inspectionParse(params.isCommodity_inspection(), diagnose2);// 商检

                diagnose2 = timeCutParse(params.isTime_cut(), diagnose2);// 缩时

                data[i] = diagnose2;
                i++;
                // 预留 1
                int reserve1 = 0;
                data[i] = reserve1 | 0x01;
                i++;
                // 预留 2
                data[i] = 0;
                i++;
                // check_sum
                data[i] = SerialDataUtil.getCheckSum(data, i);
                return true;

            case AppConstants.MODEL_X2: // 双鹿 446
                // 帧头（固定为：0x55 0x55）
                data[i] = AppConstants.X2_SERIAL_DATA_START;
                i++;
                data[i] = AppConstants.X2_SERIAL_DATA_START;
                // 数据类型（0xAA -- 常规控制数据）
                i++;
                data[i] = (byte) 0xAA;
                // 预留
                i++;
                data[i] = 0;
                // 预留
                i++;
                data[i] = 0;
                // 预留
                i++;
                data[i] = 0;
                // 预留
                i++;
                data[i] = 0;
                // 冷藏室设置温度（设置值 * 2 + 80（范围 84~96，代表 2~8℃））
                i++;
                data[i] = params.getCold_temp_set() * 2 + 80;
                // 冷冻室设置温度（设置值 * 2 + 80（范围 32~50，代表 -24~-15℃））
                i++;
                data[i] = params.getFreezing_temp_set() * 2 + 80;
                // 变温室设置温度（设置值 * 2 + 80（范围 44~74，代表 -18~-3℃））
                i++;
//                data[i] = params.getChangeable_temp_set() * 2 + 80;
                // 预留
                i++;
                data[i] = 0;
                // 环境温度（传感器数值 * 2 + 80（范围 80~180，代表 0~50℃））
                i++;
                data[i] = 0;
                // 设置功能模式
                i++;
                mode = 0;
                if (params.isCold_switch()) { // 冷藏室开关
                    mode = mode & 0xFE;
                } else {
                    mode = mode | 0x01;
                }
//                if (params.isChangeable_switch()) { // 变温室开关
//                    mode = mode & 0xF7;
//                } else {
//                    mode = mode | 0x08;
//                }
                if (params.getMode() == AppConstants.MODE_SMART) { // 智能模式
                    mode = mode | 0x04;
                } else {
                    mode = mode & 0xFB;
                }
                if (params.isQuick_cold()) { // 速冷
                    mode = mode | 0x10;
                } else {
                    mode = mode & 0xEF;
                }
                if (params.isQuick_freeze()) { // 速冻
                    mode = mode | 0x02;
                } else {
                    mode = mode & 0xFD;
                }
                data[i] = mode;
                // 设置特殊功能模式
                i++;
                int frost = 0;
                if (params.isFc_forced_frost()) { // 强制化霜设置
                    frost = frost | 0x01;
                } else {
                    frost = frost & 0xFE;
                }
                data[i] = frost;
                // 预留
                i++;
                data[i] = 0;
                // 预留
                i++;
                data[i] = 0;
                // 校验和
                i++;
                data[i] = SerialDataUtil.getCheckSum(data, 2, i - 2);
                // 帧尾（固定为：0xAA 0xAA）
                i++;
                data[i] = AppConstants.X2_SERIAL_DATA_END;
                i++;
                data[i] = AppConstants.X2_SERIAL_DATA_END;
                return true;

            case AppConstants.MODEL_X3: // 美菱 462
                // 通信起始命令  DATA0
                data[i] = AppConstants.X3_SERIAL_DATA_START_0;
                // 通信起始命令  DATA1
                i++;
                data[i] = AppConstants.X3_SERIAL_DATA_START_1;
                // 操作指令（详见协议） DATA2
                i++;
                // 指令类型
                if (isModify) data[i] = AppConstants.X3_OPERATE_TYPE_TEMP;// 修改：各间室设定温度值
                else data[i] = AppConstants.X3_OPERATE_TYPE_CHECK_STATUS;// 查询冰箱运行状态
                // 冷藏室设定温度值整数位（温度设定值整数位 + 100） DATA3
                i++;
                data[i] = params.getCold_temp_set() + 100;// 冷藏室设置温度
                // 变温室设定温度值整数位（温度设定值整数位 + 100） DATA4
                i++;
                data[i] = 0;// 变温室设置温度
                // 冷冻室设定温度值整数位（温度设定值整数位 + 100） DATA5
                i++;
                data[i] = params.getFreezing_temp_set() + 100;// 冷冻室设置温度
                // 冷藏室设定温度值小数位（温度设定值小数位 * 10 + 10） DATA6
                i++;
                data[i] = 10;
                // 变温室设定温度值小数位（温度设定值小数位 * 10 + 10） DATA7
                i++;
                data[i] = 10;
                // 冷冻室设定温度值小数位（温度设定值小数位 * 10 + 10） DATA8
                i++;
                data[i] = 10;
                // 设定运行模式  DATA9
                i++;
                mode = 0;
                if (params.getMode() == AppConstants.MODE_SMART) { // 智能模式
                    mode = mode | 0x01;
                } else {
                    mode = mode & 0xF;
                }
                if (params.getMode() == AppConstants.MODE_HOLIDAY) { // 假日模式
                    mode = mode | 0x04;
                } else {
                    mode = mode & 0xFB;
                }
                if (!params.isCold_switch()) { // 冷藏室开关
                    mode = mode | 0x02;
                } else {
                    mode = mode & 0xFD;
                }
                if (params.isQuick_freeze()) { // 速冻
                    mode = mode | 0x10;
                } else {
                    mode = mode & 0xEF;
                }
                if (params.isQuick_cold()) { // 速冷
                    mode = mode | 0x20;
                } else {
                    mode = mode & 0xDF;
                }
                data[i] = mode;// 模式选择
                // 功能字节  DATA10
                i++;
                function = 0;
                if (params.isFlipping_beam_reset_mode()) {
                    function = function | 0x02;
                } else {
                    function = function & 0xFD;
                }
                data[i] = function;
                // 维修运行模式（操作指令等于“0x7”时有效） DATA11
                i++;
                data[i] = 0;
                // 维修运行模式（操作指令等于“0x7”时有效） DATA12
                i++;
                data[i] = 0;
                // 保留  DATA13
                i++;
                data[i] = 0;
                // 保留  DATA14
                i++;
                data[i] = 0;
                // 维修运行模式：变频档位（范围：0～99） DATA15
                i++;
                data[i] = 0;
                // 通信故障  DATA16
                i++;
                data[i] = 0;
                // 保留  DATA17
                i++;
                data[i] = 0;
                // 保留  DATA18
                i++;
                data[i] = 0;
                // 保留  DATA19
                i++;
                data[i] = 0;
                // 保留  DATA20
                i++;
                data[i] = 0;
                // 保留  DATA21
                i++;
                data[i] = 0;
                // 保留  DATA22
                i++;
                data[i] = 0;
                // DATA23
                i++;
                data[i] = SerialDataUtil.getCheckSum(data, i);
                return true;

            case AppConstants.MODEL_X4: // 雪祺 450
            case AppConstants.MODEL_JD: // 雪祺 450 京东定制
                data[i] = AppConstants.X4_SERIAL_DATA_START;// 开始位

                i++;
                data[i] = params.getCold_temp_set() + 50;// 冷藏室温度

                i++;
                data[i] = params.getFreezing_temp_set() + 50;// 冷冻室温度

                i++;
                int bit3 = 0;
                if (params.isQuick_freeze()) { // 速冻
                    bit3 |= 0x01;
                } else {
                    bit3 &= 0xFE;
                }
                if (params.isFc_forced_frost()) { // 强制化霜
                    bit3 |= 0x02;
                } else {
                    bit3 &= 0xFD;
                }
                data[i] = bit3;

                i++;
                int bit4 = 0;
                if (params.getMode() == AppConstants.MODE_HOLIDAY) { // 假日模式
                    bit4 |= 0x01;
                } else {
                    bit4 &= 0xFE;
                }
                if (params.isQuick_cold()) { // 速冷模式
                    bit4 |= 0x02;
                } else {
                    bit4 &= 0xFD;
                }
                if (params.isCold_switch()) { // 冷藏关闭
                    bit4 &= 0xFB;
                } else {
                    bit4 |= 0x08;
                }
                data[i] = bit4;

                i++;
                data[i] = FridgePreference.getInstance().getRunTime();// 压缩机累计运行时间

                i++;
                i++;
                // check_sum
                data[i] = SerialDataUtil.getCheckSum(data, i);
                return true;

            case AppConstants.MODEL_X5: // 美菱 521
                // 通信起始命令  DATA0
                data[i] = AppConstants.X5_SERIAL_DATA_START_0;
                // 通信起始命令  DATA1
                i++;
                data[i] = AppConstants.X5_SERIAL_DATA_START_1;
                // 操作指令 DATA2
                i++;
                // 指令类型
                if (isModify) data[i] = AppConstants.X5_OPERATE_TYPE_TEMP;// 修改：各间室设定温度值
                else data[i] = AppConstants.X5_OPERATE_TYPE_CHECK_STATUS;// 查询冰箱运行状态
                // 冷藏室设定温度值整数位（温度设定值整数位 + 100） DATA3
                i++;
                data[i] = params.getCold_temp_set() + 100;// 冷藏室设置温度
                // 变温室设定温度值整数位（温度设定值整数位 + 100） DATA4
                i++;
                data[i] = params.getChangeable_temp_set() + 100;// 变温室设置温度
                // 冷冻室设定温度值整数位（温度设定值整数位 + 100） DATA5
                i++;
                data[i] = params.getFreezing_temp_set() + 100;// 冷冻室设置温度
                // 冷藏室设定温度值小数位（温度设定值小数位 * 10 + 10） DATA6
                i++;
                data[i] = 10;
                // 变温室设定温度值小数位（温度设定值小数位 * 10 + 10） DATA7
                i++;
                data[i] = 10;
                // 冷冻室设定温度值小数位（温度设定值小数位 * 10 + 10） DATA8
                i++;
                data[i] = 10;
                // 设定运行模式  DATA9
                i++;
                mode = 0;
                if (params.getMode() == AppConstants.MODE_SMART) { // 智能模式
                    mode = mode | 0x01;
                } else {
                    mode = mode & 0xF;
                }
                if (params.getMode() == AppConstants.MODE_HOLIDAY) { // 假日模式
                    mode = mode | 0x04;
                } else {
                    mode = mode & 0xFB;
                }
                if (!params.isCold_switch()) { // 冷藏室开关
                    mode = mode | 0x02;
                } else {
                    mode = mode & 0xFD;
                }
                if (!params.isChangeable_switch()) { // 变温室开关
                    mode = mode | 0x08;
                } else {
                    mode = mode & 0xF7;
                }
                if (params.isQuick_freeze()) { // 速冻
                    mode = mode | 0x10;
                } else {
                    mode = mode & 0xEF;
                }
                if (params.isOne_key_clean()) { // 一键净化
                    mode = mode | 0x80;
                } else {
                    mode = mode & 0x7F;
                }
                data[i] = mode;// 模式选择
                // 功能字节  DATA10
                i++;
                function = 0;
                if (params.isFlipping_beam_reset_mode()) { // 翻转梁加热器关闭模式
                    function = function | 0x02;
                } else {
                    function = function & 0xFD;
                }
                if (params.isFresh_fruit()) { // 鲜果
                    function = function | 0x08;
                } else {
                    function = function & 0xF7;
                }
                if (params.isRetain_fresh()) { // 0 度保鲜
                    function = function | 0x10;
                } else {
                    function = function & 0xEF;
                }
                if (params.isIced()) { // 冰镇
                    function = function | 0x20;
                } else {
                    function = function & 0xDF;
                }
                data[i] = function;
                // 维修运行模式（操作指令等于“0x7”时有效） DATA11
                i++;
                data[i] = 0;
                // 维修运行模式（操作指令等于“0x7”时有效） DATA12
                i++;
                data[i] = 0;
                // 保留  DATA13
                i++;
                data[i] = 0;
                // 保留  DATA14
                i++;
                data[i] = 0;
                // 维修运行模式：变频档位（范围：0～99） DATA15
                i++;
                data[i] = 0;
                // 通信故障  DATA16
                i++;
                data[i] = 0;
                // 保留  DATA17
                i++;
                data[i] = 0;
                // 保留  DATA18
                i++;
                data[i] = 0;
                // 保留  DATA19
                i++;
                data[i] = 0;
                // 保留  DATA20
                i++;
                data[i] = 0;
                // 保留  DATA21
                i++;
                data[i] = 0;
                // 保留  DATA22
                i++;
                data[i] = 0;
                // DATA23
                i++;
                data[i] = SerialDataUtil.getCheckSum(data, i);
                return true;

            default:
                logUtil.e(TAG, "model,send data,info null !");
                return false;
        }
    }

    /**
     * 一键净化
     *
     * @param enable: 是否开启一键净化
     * @param data:   字节数据
     */
    private static int oneKeyCleanParse(boolean enable, int data) {
        if (enable) return data | 0x08;
        else return data & 0xF7;
    }

    /**
     * 商检
     *
     * @param enable: 是否启动商检
     * @param data:   字节数据
     */
    private static int inspectionParse(boolean enable, int data) {
        if (enable) return data | 0x20;
        else return data & 0xCF;
    }

    /**
     * 缩时
     *
     * @param enable: 是否开启缩时功能
     * @param data:   字节数据
     */
    private static int timeCutParse(boolean enable, int data) {
        if (enable) return data | 0x10;
        else return data & 0xEF;
    }

    /**
     * 强制启动
     *
     * @param enable: 是否强制启动
     * @param data:   字节数据
     */
    private static int rcfForcedStartParse(boolean enable, int data) {
        if (enable) return data | 0x01;
        else return data & 0xFE;
    }

    /**
     * 强制化霜
     *
     * @param enable: 是否强制化霜
     * @param data:   字节数据
     */
    private static int forcedFrostParse(boolean enable, int data) {
        if (enable) return data | 0x02;
        else return data & 0xFD;
    }
}