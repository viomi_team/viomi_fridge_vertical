package com.viomi.fridge.vertical.fridgecontrol.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseRecyclerViewAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 冰箱故障列表适配器
 * Created by William on 2018/7/3.
 */
public class ErrorAdapter extends BaseRecyclerViewAdapter<ErrorAdapter.ErrorHolder> {
    private String[] mArray;

    public ErrorAdapter(String[] array) {
        this.mArray = array;
    }

    @Override
    public ErrorHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_holder_device_error, parent, false);
        return new ErrorHolder(view);
    }

    @Override
    public void onBindViewHolder(ErrorHolder holder, int position) {
        holder.textView.setText(mArray[position]);
    }

    @Override
    public int getItemCount() {
        return mArray.length;
    }

    class ErrorHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.holder_device_error)
        TextView textView;

        ErrorHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
