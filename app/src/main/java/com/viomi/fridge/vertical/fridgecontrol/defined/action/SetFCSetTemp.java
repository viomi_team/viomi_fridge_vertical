package com.viomi.fridge.vertical.fridgecontrol.defined.action;

import com.viomi.fridge.vertical.fridgecontrol.defined.ViomiDefined;
import com.viomi.fridge.vertical.fridgecontrol.defined.property.FCSetTemp;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetFCSetTemp extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setFCSetTemp.toActionType();

    public SetFCSetTemp() {
        super(TYPE);

        super.addArgument(FCSetTemp.TYPE.toString());
    }
}