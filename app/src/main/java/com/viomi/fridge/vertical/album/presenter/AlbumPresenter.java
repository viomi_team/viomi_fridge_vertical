package com.viomi.fridge.vertical.album.presenter;

import android.content.Context;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.album.contract.AlbumContract;
import com.viomi.fridge.vertical.album.model.AlbumRepository;
import com.viomi.fridge.vertical.album.model.entity.AlbumEntity;
import com.viomi.fridge.vertical.album.model.preference.AlbumPreference;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.ToastUtil;
import com.viomi.fridge.vertical.common.util.logUtil;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * 电子相册加载 Presenter
 * Created by William on 2018/1/19.
 */
public class AlbumPresenter implements AlbumContract.Presenter {
    private final static String TAG = AlbumPresenter.class.getSimpleName();
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;

    @Nullable
    private AlbumContract.View mView;

    @Inject
    AlbumPresenter(Context context) {
        mContext = context;
    }

    @Override
    public void subscribe(AlbumContract.View view) {
        mView = view;
        mCompositeSubscription = new CompositeSubscription();
        loadCache();// 读取缓存
    }

    @Override
    public void unSubscribe() {
        mView = null;
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
    }

    @Override
    public void loadCache() {
        boolean enable = AlbumPreference.getInstance().getSwitch();
        String time = AlbumPreference.getInstance().getTime();
        if (mView != null) mView.initWithCache(enable, time);
    }

    @Override
    public void loadAlbum(List<AlbumEntity> list) {
        Subscription subscription = AlbumRepository.getInstance().getAlbum()
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(albumEntities -> {
                    if (mView != null && albumEntities != null) {
                        int size = list.size();
                        list.clear();
                        mView.notifyDataRemoved(size);
                        list.addAll(albumEntities);
                        mView.notifyDataInserted(list.size());
                        mView.updateAlbum();
                    }
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void delete(List<AlbumEntity> list) {
        Subscription subscription = AlbumRepository.getInstance().deleteAlbum(list)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(aBoolean -> {
                    if (aBoolean && mView != null) {
                        mView.notifyDataChanged();
                        mView.updateAlbum();
                        RxBus.getInstance().post(BusEvent.MSG_ALBUM_UPDATE);
                    } else
                        ToastUtil.showCenter(mContext, mContext.getResources().getString(R.string.album_delete_fail_tip));
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void cacheSwitch(boolean enable) {
        Subscription subscription = Observable.just(enable)
                .observeOn(Schedulers.io())
                .onTerminateDetach()
                .subscribe(aBoolean -> AlbumPreference.getInstance().saveSwitch(enable), throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void cacheTime(String time) {
        Subscription subscription = Observable.just(time)
                .observeOn(Schedulers.io())
                .onTerminateDetach()
                .subscribe(s -> AlbumPreference.getInstance().saveTime(time), throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void cacheScreenSaver(List<AlbumEntity> list) {
        Subscription subscription = Observable.just(list)
                .compose(RxSchedulerUtil.SchedulersTransformer2())
                .onTerminateDetach()
                .map(list1 -> {
                    List<AlbumEntity> list2 = new ArrayList<>();
                    for (AlbumEntity entity : list1) {
                        if (entity.isSelected()) list2.add(entity);
                    }
                    return list2;
                })
                .onTerminateDetach()
                .subscribe(list1 -> {
                    if (list1 != null && list1.size() > 0)
                        AlbumPreference.getInstance().saveAlbumEntityList(list1);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public boolean isChosen(List<AlbumEntity> list) {
        boolean isChosen = false;
        for (AlbumEntity entity : list) {
            if (entity.isSelected()) {
                isChosen = true;
                break;
            }
        }
        return isChosen;
    }
}
