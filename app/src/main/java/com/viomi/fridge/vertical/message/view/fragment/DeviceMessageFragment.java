package com.viomi.fridge.vertical.message.view.fragment;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseFragment;
import com.viomi.fridge.vertical.common.scope.ActivityScoped;
import com.viomi.fridge.vertical.message.entity.DeviceInfoMessage;
import com.viomi.fridge.vertical.message.manager.InfoManager;
import com.viomi.fridge.vertical.message.view.adapter.DeviceMessageAdapter;
import com.viomi.fridge.vertical.web.activity.BrowserActivity;
import com.viomi.widget.dialog.BaseAlertDialog;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * 消息中心 - 设备消息
 * 包括食物过期和滤芯过期提醒
 * Created by nanquan on 2018/2/6.
 */
@ActivityScoped
public class DeviceMessageFragment extends BaseFragment {
    private List<DeviceInfoMessage> mList = new ArrayList<>();

    @BindView(R.id.rv_message)
    RecyclerView mRecyclerView;// 消息列表

    @BindView(R.id.ll_message_empty)
    LinearLayout mLinearLayout;// 消息为空布局

    @Inject
    public DeviceMessageFragment() {

    }

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.fragment_message;
    }

    @Override
    protected void initWithOnCreateView() {
        initRecyclerView();
        initData();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mList != null) {
            mList.clear();
            mList = null;
        }
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        DeviceMessageAdapter messageAdapter = new DeviceMessageAdapter(getActivity(), mList);
        mRecyclerView.setAdapter(messageAdapter);
        messageAdapter.setOnItemClickListener(new DeviceMessageAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                DeviceInfoMessage message = mList.get(position);
                if (message != null && !TextUtils.isEmpty(message.getLinkUrl())) {
                    Intent intent = new Intent(getActivity(), BrowserActivity.class);
                    intent.putExtra(AppConstants.WEB_URL, message.getLinkUrl());
                    startActivity(intent);
                }
            }

            @Override
            public void onItemDeleteClick(int position) {
                if (getActivity() != null) {
                    BaseAlertDialog dialog = new BaseAlertDialog(getActivity(), FridgeApplication.getContext().getResources().getString(R.string.message_delete_tip), FridgeApplication.getContext().getResources().getString(R.string.cancel), FridgeApplication.getContext().getResources().getString(R.string.confirm));
                    dialog.setOnLeftClickListener(dialog::dismiss);
                    dialog.setOnRightClickListener(() -> {
                        DeviceInfoMessage message = mList.get(position);
                        if (message != null) {
                            DeviceInfoMessage info = InfoManager.getInstance().getDeviceInfoById(message.getInfoId());
                            InfoManager.getInstance().deleteDeviceInfo(info);
                            mList.remove(position);
                            notifyData();
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
            }
        });
    }

    private void initData() {
        List<DeviceInfoMessage> cacheList = InfoManager.getInstance().getDeviceInfoRecord();
        if (cacheList != null) {
            mList.addAll(cacheList);
        }
        notifyData();
    }

    private void notifyData() {
        mRecyclerView.getAdapter().notifyDataSetChanged();
        if (mList.size() == 0) {
            mRecyclerView.setVisibility(View.GONE);
            mLinearLayout.setVisibility(View.VISIBLE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            mLinearLayout.setVisibility(View.GONE);
        }
    }
}