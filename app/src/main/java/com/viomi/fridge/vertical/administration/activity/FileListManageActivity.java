package com.viomi.fridge.vertical.administration.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.model.entity.FileInfo;
import com.viomi.fridge.vertical.administration.view.adapter.FileListAdapter;
import com.viomi.fridge.vertical.administration.view.dialog.CleanSuccessDialog;
import com.viomi.fridge.vertical.administration.view.dialog.ClearingDialog;
import com.viomi.fridge.vertical.common.base.BaseActivity;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.FileOperationHelper;
import com.viomi.fridge.vertical.common.util.FileUtil;
import com.viomi.fridge.vertical.common.util.SDCardUtils;
import com.viomi.widget.dialog.BaseAlertDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 文件管理 Activity
 */
public class FileListManageActivity extends BaseActivity implements FileOperationHelper.IOperationProgressListener {
    private FileUtil.FileType mFileType;// 文件类型
    private long mClearSize;
    private FileListAdapter mFileListAdapter;// 文件列表适配器
    private List<FileInfo> mFileList;// 总文件集合
    private ArrayList<FileInfo> mSelectFiles;// 选中文件集合
    private FileOperationHelper mFileOperationHelper;
    private ClearingDialog mClearingDialog;// 正在清理 Dialog
    private CleanSuccessDialog mCleanSuccessDialog;// 清理完成 Dialog

    @BindView(R.id.rv_file)
    RecyclerView mRecyclerView;// 文件列表

    @BindView(R.id.img_select_all)
    ImageView mSelectAllImageView;// 全选图标

    @BindView(R.id.tv_clear_info)
    TextView mClearTextView;// 清理信息

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        layoutId = R.layout.activity_file_list_manage;
        super.onCreate(savedInstanceState);
        mFileType = (FileUtil.FileType) getIntent().getSerializableExtra("fileType");
        mFileOperationHelper = new FileOperationHelper(this, FridgeApplication.getContext());
        initTitle();
        initRecyclerView();
        initDialog();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RxBus.getInstance().post(BusEvent.MSG_FILE_DELETE);
        if (mFileList != null) {
            mFileList.clear();
            mFileList = null;
        }
        if (mSelectFiles != null) {
            mSelectFiles.clear();
            mSelectFiles = null;
        }
        if (mFileListAdapter != null) mFileListAdapter = null;
        if (mFileOperationHelper != null) mFileOperationHelper = null;
        if (mClearingDialog != null && mClearingDialog.isShowing()) {
            mClearingDialog.cancel();
            mClearingDialog = null;
        }
        if (mCleanSuccessDialog != null && mCleanSuccessDialog.isShowing()) {
            mCleanSuccessDialog.cancel();
            mCleanSuccessDialog = null;
        }
    }

    private void initTitle() {
        String title = "";
        if (mFileType == FileUtil.FileType.FILE_VIDEO) {
            title = getResources().getString(R.string.management_file_video);
        } else if (mFileType == FileUtil.FileType.FILE_PIC) {
            title = getResources().getString(R.string.management_file_picture);
        } else if (mFileType == FileUtil.FileType.FILE_MUSIC) {
            title = getResources().getString(R.string.management_file_music);
        }
        if (mTitleTextView != null) mTitleTextView.setText(title);
    }

    private void initRecyclerView() {
        mFileList = new ArrayList<>();
        mSelectFiles = new ArrayList<>();
        mFileListAdapter = new FileListAdapter(this, mFileList);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 6));
        mRecyclerView.setAdapter(mFileListAdapter);
        mFileList.addAll(FileUtil.getFileInfoList(FridgeApplication.getContext(), mFileType));
        mFileListAdapter.notifyDataSetChanged();
        mFileListAdapter.setOnItemClickListener(position -> {
            mFileList.get(position).setSelected(!mFileList.get(position).isSelected());
            if (mFileList.get(position).isSelected()) {
                mClearSize += mFileList.get(position).getFileSize();
            } else {
                mClearSize -= mFileList.get(position).getFileSize();
            }
            showClearInfo();
            mFileListAdapter.notifyItemChanged(position);
            mSelectAllImageView.setSelected(isSelectAll());
        });
    }

    private void initDialog() {
        mClearingDialog = new ClearingDialog(this);
        mCleanSuccessDialog = new CleanSuccessDialog(this);
    }

    @OnClick(R.id.tv_select_all)
    public void onSelectAllClick() { // 全选
        mClearSize = 0;
        mSelectAllImageView.setSelected(!mSelectAllImageView.isSelected());
        for (FileInfo file : mFileList) {
            file.setSelected(mSelectAllImageView.isSelected());
            if (mSelectAllImageView.isSelected()) {
                mClearSize += file.getFileSize();
            }
        }
        showClearInfo();
        mFileListAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.tv_clear)
    public void onClearClick() { // 清理
        BaseAlertDialog dialog = new BaseAlertDialog(this, getResources().getString(R.string.management_clear_tip), getResources().getString(R.string.no), getResources().getString(R.string.yes));
        dialog.setOnLeftClickListener(dialog::dismiss);
        dialog.setOnRightClickListener(() -> {
            mClearingDialog.show();
            mSelectFiles.clear();
            // 删除文件
            for (FileInfo file : mFileList) {
                if (file.isSelected()) {
                    mSelectFiles.add(file);
                }
            }
            mFileOperationHelper.Delete(mSelectFiles);
            dialog.dismiss();
        });
        dialog.show();
    }

    private boolean isSelectAll() {
        boolean selectAll = true;
        for (FileInfo file : mFileList) {
            if (!file.isSelected()) {
                selectAll = false;
                break;
            }
        }
        return selectAll;
    }

    private void showClearInfo() {
        mClearTextView.setText(mClearSize > 0 ? "可节省 " + SDCardUtils.bit2KbMGB(mClearSize)
                + "，清理后可用 " + SDCardUtils.bit2KbMGB(SDCardUtils.getSDCardFreeSizeBit() + mClearSize) : "");
    }

    /**
     * 文件删除完成
     */
    @Override
    public void onFinish() {
        for (FileInfo selectFile : mSelectFiles) {
            for (FileInfo file : mFileList) {
                if (selectFile.getFileId() == file.getFileId()) {
                    mFileList.remove(file);
                    break;
                }
            }
        }
        mClearTextView.setVisibility(View.INVISIBLE);
        mFileListAdapter.notifyDataSetChanged();
        mClearingDialog.cancel();
        mCleanSuccessDialog.show();
        mCleanSuccessDialog.setInfo("恭喜您，节省 " + SDCardUtils.bit2KbMGB(mClearSize) + " 空间");
        mCleanSuccessDialog.setOnCancelListener(dialog -> {
            if (mFileList.size() == 0) finish();
        });
    }

    @Override
    public void onFileChanged(String path) {
        if (TextUtils.isEmpty(path)) return;
        final File file = new File(path);
        final Intent intent;
        if (file.isDirectory()) {
            intent = new Intent("action.intent.action_file_update_broadcast");
        } else {
            intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            intent.setData(Uri.fromFile(new File(path)));
        }
        sendBroadcast(intent);
    }
}
