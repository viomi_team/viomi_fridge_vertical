package com.viomi.fridge.vertical.fridgecontrol.defined.action;

import com.viomi.fridge.vertical.fridgecontrol.defined.ViomiDefined;
import com.viomi.fridge.vertical.fridgecontrol.defined.property.CoolBeverage;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetCoolBeverage extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setCoolBeverage.toActionType();

    public SetCoolBeverage() {
        super(TYPE);

        super.addArgument(CoolBeverage.TYPE.toString());
    }
}