package com.viomi.fridge.vertical.message.entity;

import java.io.Serializable;

/**
 * 消息推送设置
 * Created by nanquan on 2016/9/7.
 */

public class PushSetting implements Serializable {

    private boolean activityEnable = true;
    private boolean mallEnable = true;
    private boolean deviceEnable = true;
    private String mallAlias;
    private String deviceAlias;
    private boolean pushEnable = true;
    private int noPushStartTime = 22;
    private int noPushEndTime = 8;

    public boolean isActivityEnable() {
        return activityEnable;
    }

    public void setActivityEnable(boolean activityEnable) {
        this.activityEnable = activityEnable;
    }

    public boolean isMallEnable() {
        return mallEnable;
    }

    public void setMallEnable(boolean mallEnable) {
        this.mallEnable = mallEnable;
    }

    public boolean isDeviceEnable() {
        return deviceEnable;
    }

    public void setDeviceEnable(boolean deviceEnable) {
        this.deviceEnable = deviceEnable;
    }

    public String getMallAlias() {
        return mallAlias;
    }

    public void setMallAlias(String mallAlias) {
        this.mallAlias = mallAlias;
    }

    public String getDeviceAlias() {
        return deviceAlias;
    }

    public void setDeviceAlias(String deviceAlias) {
        this.deviceAlias = deviceAlias;
    }

    public boolean isPushEnable() {
        return pushEnable;
    }

    public void setPushEnable(boolean pushEnable) {
        this.pushEnable = pushEnable;
    }

    public int getNoPushStartTime() {
        return noPushStartTime;
    }

    public void setNoPushStartTime(int noPushStartTime) {
        this.noPushStartTime = noPushStartTime;
    }

    public int getNoPushEndTime() {
        return noPushEndTime;
    }

    public void setNoPushEndTime(int noPushEndTime) {
        this.noPushEndTime = noPushEndTime;
    }
}
