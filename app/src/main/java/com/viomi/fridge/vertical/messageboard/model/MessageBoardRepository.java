package com.viomi.fridge.vertical.messageboard.model;

import android.graphics.Bitmap;
import android.os.Environment;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.messageboard.model.entity.MessageBoardEntity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import rx.Observable;

/**
 * 留言板相关 Api
 * Created by William on 2018/2/27.
 */
public class MessageBoardRepository {
    private static final String TAG = MessageBoardRepository.class.getSimpleName();
    private static MessageBoardRepository mInstance;

    public static MessageBoardRepository getInstance() {
        if (mInstance == null) {
            synchronized (MessageBoardRepository.class) {
                if (mInstance == null) {
                    mInstance = new MessageBoardRepository();
                }
            }
        }
        return mInstance;
    }

    /**
     * 保存留言版绘图
     *
     * @param bitmap:        绘图 bitmap
     * @param compress:压缩百分比 1 - 100
     */
    public Observable<Boolean> saveImage(Bitmap bitmap, int compress) {
        return Observable.create(subscriber -> {
            String path = Environment.getExternalStorageDirectory().toString() + "/" + AppConstants.PATH + AppConstants.MESSAGE_BOARD_PATH;// 路径
            File dir = new File(path);// 保存文件夹
            if (!dir.exists()) { // 若不存在文件夹先创建
                if (!dir.mkdirs()) {
                    subscriber.onNext(false);
                    subscriber.onCompleted();
                    return;
                }
            }
            File file = new File(path, System.currentTimeMillis() + ".png");
            try {
                if (!file.createNewFile()) { // 先创建文件
                    subscriber.onNext(false);
                    subscriber.onCompleted();
                    return;
                }
                FileOutputStream outputStream = new FileOutputStream(file);
                // 开始保存
                if (compress >= 1 && compress <= 100)
                    bitmap.compress(Bitmap.CompressFormat.PNG, compress, outputStream);
                else bitmap.compress(Bitmap.CompressFormat.PNG, 80, outputStream);
                outputStream.flush();
                outputStream.getFD().sync();// 掉电保护
                outputStream.close();
                bitmap.recycle();
                subscriber.onNext(true);
                subscriber.onCompleted();
                logUtil.d(TAG, "save success");
            } catch (IOException e) {
                e.printStackTrace();
                subscriber.onNext(false);
                subscriber.onCompleted();
            }
        });
    }

    /**
     * 获取留言板图片列表
     */
    public Observable<List<MessageBoardEntity>> getMessageBoardList() {
        return Observable.create(subscriber -> {
            File dir = new File(Environment.getExternalStorageDirectory(), AppConstants.PATH + AppConstants.MESSAGE_BOARD_PATH);// 留言板路径
            if (!dir.exists()) { // 无留言
                logUtil.e(TAG, "message board directory is not exit");
                subscriber.onNext(null);
                subscriber.onCompleted();
                return;
            }
            File[] files = null;
            if (dir.isDirectory()) {
                files = dir.listFiles((dir1, name) -> name.endsWith(".jpg") || name.endsWith(".png") || name.endsWith(".jpeg"));
            }
            if (files == null || files.length == 0) { // 没有图片
                subscriber.onNext(null);
                subscriber.onCompleted();
                return;
            }
            List<MessageBoardEntity> list = new ArrayList<>();
            for (File file : files) {
                MessageBoardEntity entity = new MessageBoardEntity();
                entity.setFile(file);
                entity.setSelected(false);
                list.add(entity);
            }
            Collections.reverse(list);
            subscriber.onNext(list);
            subscriber.onCompleted();
        });
    }

    /**
     * 删除选中的留言板
     */
    public Observable<Boolean> deleteList(List<MessageBoardEntity> list) {
        return Observable.create(subscriber -> {
            if (list != null && list.size() > 0) {
                Iterator<MessageBoardEntity> iterator = list.iterator();
                while (iterator.hasNext()) {
                    MessageBoardEntity entity = iterator.next();
                    if (entity.isSelected()) {
                        if (entity.getFile().exists() && entity.getFile().delete())
                            iterator.remove();
                    }
                }
                subscriber.onNext(true);
                subscriber.onCompleted();
            } else {
                subscriber.onNext(false);
                subscriber.onCompleted();
            }
        });
    }
}