package com.viomi.fridge.vertical.iot.view.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.iot.model.http.entity.DeviceTypeEntity;
import com.viomi.fridge.vertical.iot.util.DeviceIconUtil;
import com.viomi.widget.sphere3d.TagsAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * 设备分类适配器
 * Created by William on 16/3/4.
 */
public class DeviceTypeAdapter extends TagsAdapter {
    private List<DeviceTypeEntity> mList;
    private Context mContext;

    public DeviceTypeAdapter(Context context, List<DeviceTypeEntity> list) {
        mList = list;
        mContext = context;
        if (mList == null) mList = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public View getView(Context context, int position, ViewGroup parent) {
        DeviceTypeEntity entity = mList.get(position);
        View view;
        if (entity.getType().equals(mContext.getResources().getString(R.string.iot_device_fridge))) { // 冰箱
            view = LayoutInflater.from(context).inflate(R.layout.view_holder_device_type_2, parent, false);
            // 图标
            ImageView imageView = view.findViewById(R.id.holder_device_type_2_icon);
            imageView.setImageResource(DeviceIconUtil.switchIconWithPosition(position));
            // 设备分类
            TextView textView = view.findViewById(R.id.holder_device_type_2_name);
            textView.setText(entity.getType());
            // 背景
            LinearLayout linearLayout = view.findViewById(R.id.holder_device_type_2_bg);
            linearLayout.setBackgroundResource(DeviceIconUtil.switchDeviceBg(position));
            // 数据
            TextView coldTextView = view.findViewById(R.id.holder_device_type_fridge_cold);
            TextView changeableTextView = view.findViewById(R.id.holder_device_type_fridge_changeable);
            TextView freezingTextView = view.findViewById(R.id.holder_device_type_fridge_freezing);
            LinearLayout linearLayout1 = view.findViewById(R.id.holder_device_type_changeable_layout);
            View lineView = view.findViewById(R.id.holder_device_type_line);
            Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/DINCond-Medium.otf");
            coldTextView.setTypeface(typeface);
            changeableTextView.setTypeface(typeface);
            freezingTextView.setTypeface(typeface);
            if (entity.getData() != null) { // 数据不为空
                String[] data = entity.getData().split(",");
                if (data.length >= 2) {
                    if (data.length == 2) {
                        linearLayout1.setVisibility(View.GONE);
                        lineView.setVisibility(View.GONE);
                        coldTextView.setText(data[0]);
                        freezingTextView.setText(data[1]);
                    } else {
                        coldTextView.setText(data[0]);
                        changeableTextView.setText(data[1]);
                        freezingTextView.setText(data[2]);
                    }
                }
            }
        } else {
            view = LayoutInflater.from(mContext).inflate(R.layout.view_holder_device_type_1, parent, false);
            // 图标
            ImageView imageView = view.findViewById(R.id.holder_device_type_1_icon);
            imageView.setImageResource(DeviceIconUtil.switchIconWithPosition(position));
            // 设备分类
            TextView typeTextView = view.findViewById(R.id.holder_device_type_1_name);
            typeTextView.setText(entity.getType());
            // 背景
            LinearLayout linearLayout = view.findViewById(R.id.holder_device_type_1_bg);
            if (entity.getList().size() > 0 && !entity.isExist()) { // 离线
                linearLayout.setBackgroundResource(R.drawable.icon_device_circle_gray);
            } else {
                linearLayout.setBackgroundResource(DeviceIconUtil.switchDeviceBg(position));
            }
            // 显示数据
            TextView dataTextView = view.findViewById(R.id.holder_device_type_1_data);
            if (entity.getList().size() > 0 && entity.isExist()) { // 在线
                dataTextView.setText(entity.getData() == null ? FridgeApplication.getContext().getResources().getString(R.string.iot_load_data_getting) : entity.getData());
            } else if (entity.getList().size() > 0 && !entity.isExist()) { // 离线
                dataTextView.setText(mContext.getResources().getString(R.string.iot_device_type_offline));
            } else if (entity.getList().size() == 0 && entity.isOnSale()) { // 已上架
                dataTextView.setText(mContext.getResources().getString(R.string.iot_buy_confirm));
            } else {
                dataTextView.setText(mContext.getResources().getString(R.string.iot_please_look_forward));
            }
        }
        return view;
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public int getPopularity(int position) {
        return position % 5;
    }

    @Override
    public void onThemeColorChanged(View view, float alpha) {
        view.setAlpha(alpha - 0.2f);
    }
}