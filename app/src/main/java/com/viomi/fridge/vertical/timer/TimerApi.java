package com.viomi.fridge.vertical.timer;

import android.content.Context;
import android.content.Intent;

import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.timer.activity.TimerActivity;

/**
 * Created by admin on 2018/3/2.
 */

public class TimerApi {
    private static TimerApi timerApi;

    private TimerApi() {
    }

    public static TimerApi getInstance() {
        if (timerApi == null) {
            synchronized (TimerApi.class) {
                if (timerApi == null) {
                    timerApi = new TimerApi();
                }
            }
        }
        return timerApi;
    }

    public static void startMode(Context context,int mode) {
        Intent intent = new Intent(context, TimerActivity.class);
        intent.putExtra("time", -1);
        intent.putExtra("mode", mode);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }


    public static void startTime(Context context,int minute) {
        Intent intent = new Intent(context, TimerActivity.class);
        intent.putExtra("time", minute);
        intent.putExtra("mode", -1);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    //取消
    public static void cancel() {
        RxBus.getInstance().post(BusEvent.MSG_TIMER_SPEECH_STOP);
    }
}
