package com.viomi.fridge.vertical.cookbook.module;

import com.viomi.fridge.vertical.common.scope.FragmentScoped;
import com.viomi.fridge.vertical.cookbook.view.fragment.CookCategoryFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * 菜谱分类 Module
 */
@Module
public abstract class CookCategoryModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract CookCategoryFragment cookCategoryFragment();// 分类 Fragment
}