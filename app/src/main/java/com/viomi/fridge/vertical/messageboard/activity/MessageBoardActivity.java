package com.viomi.fridge.vertical.messageboard.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.album.activity.AlbumActivity;
import com.viomi.fridge.vertical.common.base.BaseActivity;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.messageboard.contract.MessageBoardContract;
import com.viomi.fridge.vertical.messageboard.model.entity.MessageBoardEntity;
import com.viomi.fridge.vertical.messageboard.view.dialog.EraserEditDialog;
import com.viomi.fridge.vertical.messageboard.view.dialog.PenEditDialog;
import com.viomi.fridge.vertical.messageboard.view.dialog.TextEditDialog;
import com.viomi.widget.dialog.BaseAlertDialog;
import com.viomi.widget.sketchview.SketchData;
import com.viomi.widget.sketchview.SketchView;
import com.viomi.widget.sketchview.StrokeRecord;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static com.viomi.widget.sketchview.StrokeRecord.STROKE_TYPE_DRAW;
import static com.viomi.widget.sketchview.StrokeRecord.STROKE_TYPE_ERASER;

/**
 * 留言板 Activity
 * Created by William on 2018/3/30.
 */
public class MessageBoardActivity extends BaseActivity implements SketchView.OnDrawChangedListener, SketchView.TextWindowCallback,
        PenEditDialog.OnPenEditDismissListener, EraserEditDialog.OnEraserEditDismissListener, MessageBoardContract.View,
        SketchView.TextEditCallback {
    private static final String TAG = MessageBoardActivity.class.getSimpleName();
    private String mPath = Environment.getExternalStorageDirectory().toString() + "/" + AppConstants.PATH + AppConstants.CAMERA_CACHE;
    private List<SketchData> mList;// 画板步骤记录
    private File mCameraFile, mCropFile;// 拍照，裁剪暂存
    private EraserEditDialog mEraserEditDialog;// 橡皮擦编辑 Dialog
    private PenEditDialog mPenEditDialog;// 画笔编辑 Dialog
    private TextEditDialog mTextEditDialog;// 文字编辑 Dialog

    @Inject
    MessageBoardContract.Presenter mPresenter;

    @BindView(R.id.message_board_drawing)
    SketchView mSketchView;// 画板

    @BindView(R.id.title_bar_all)
    LinearLayout mAllLinearLayout;// 全部留言

    @BindView(R.id.message_board_delete)
    ImageView mDeleteImageView;// 删除
    @BindView(R.id.message_board_undo)
    ImageView mUndoImageView;// 撤销
    @BindView(R.id.message_board_redo)
    ImageView mRedoImageView;// 恢复
    @BindView(R.id.message_board_save)
    ImageView mSaveImageView;// 保存
    @BindView(R.id.message_board_pen)
    ImageView mPenImageView;// 画笔
    @BindView(R.id.message_board_eraser)
    ImageView mEraserImageView;// 橡皮擦
    @BindView(R.id.message_board_text)
    ImageView mTextImageView;// 添加文字
    @BindView(R.id.message_board_photo)
    ImageView mPhotoImageView;// 添加图片
    @BindView(R.id.message_board_camera)
    ImageView mCameraImageView;// 拍照

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        layoutId = R.layout.activity_message_board;
        mTitle = getResources().getString(R.string.home_message_board);
        super.onCreate(savedInstanceState);
        mPresenter.subscribe(this);// 订阅
        mPenImageView.setImageResource(R.drawable.icon_message_board_pen_active);
        mList = new ArrayList<>();
        SketchData sketchData = new SketchData();
        mList.add(sketchData);
        mEraserEditDialog = new EraserEditDialog();
        mPenEditDialog = new PenEditDialog();
        mTextEditDialog = new TextEditDialog();
        mSketchView.setSketchData(sketchData);
        mSketchView.setOnDrawChangedListener(this);// 绘制监听
        mSketchView.setTextEditCallback(this);// 文字编辑监听
        mSketchView.setTextWindowCallback(this);// 添加文字监听
        mPenEditDialog.setOnPenEditDismissListener(this);// 画笔编辑框关闭监听
        mEraserEditDialog.setOnEraserEditDismissListener(this);// 橡皮擦编辑关闭监听
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mPresenter != null) mPresenter.load();// 读取全部留言
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RxBus.getInstance().post(BusEvent.MSG_DELETE_CAMERA_CACHE);
        if (mSketchView != null) mSketchView.erase(false);// 删除所有绘图
        if (mList != null) {
            mList.clear();
            mList = null;
        }
        if (mPresenter != null) {
            mPresenter.unSubscribe();// 取消订阅
            mPresenter = null;
        }
        if (mEraserEditDialog != null && mEraserEditDialog.isAdded()) {
            mEraserEditDialog.dismiss();
            mEraserEditDialog = null;
        }
        if (mPenEditDialog != null && mPenEditDialog.isAdded()) {
            mPenEditDialog.dismiss();
            mPenEditDialog = null;
        }
        if (mTextEditDialog != null && mTextEditDialog.isAdded()) {
            mTextEditDialog.dismiss();
            mTextEditDialog = null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstants.CODE_CHOSE_ALBUM && resultCode == AppConstants.CODE_CHOSE_ALBUM_SUCCESS) {
            String path = data.getStringExtra(AppConstants.CHOSE_ALBUM_RESULT);
            logUtil.d(TAG, path);
            if (path == null || path.equals("")) return;
            mSketchView.addPhotoByPath(path);
            mSketchView.setEditMode(SketchView.EDIT_PHOTO);
        } else if (requestCode == AppConstants.CODE_CAMERA && resultCode == RESULT_OK) {
            Intent intent = new Intent("com.android.camera.action.CROP");
            Uri inputUri, cropUri;
            mCropFile = new File(mPath, System.currentTimeMillis() + ".jpg");// 裁剪保存图片
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) { // Android N 以上
                inputUri = FileProvider.getUriForFile(FridgeApplication.getContext(), "com.viomi.fridge.vertical.fileprovider", mCameraFile);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            } else {
                inputUri = Uri.fromFile(mCameraFile);
            }
            cropUri = Uri.fromFile(mCropFile);
            intent.setDataAndType(inputUri, "image/*");
            intent.putExtra("crop", "true");// 可裁剪
            intent.putExtra("scale", true);// 支持缩放
            intent.putExtra(MediaStore.EXTRA_OUTPUT, cropUri);// 将裁剪的结果输出到指定的 Uri
            intent.putExtra("return-data", false);// 若为 true 则表示返回数据
            intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());// 裁剪成的图片的格式
            intent.putExtra("noFaceDetection", false);// 去除默认的人脸识别，否则和剪裁匡重叠
            startActivityForResult(intent, AppConstants.CODE_CAMERA_CROP);
        } else if (requestCode == AppConstants.CODE_CAMERA_CROP && resultCode == RESULT_OK) {
            String path = mCropFile.getAbsolutePath();
            if (path.equals("")) return;
            mSketchView.addPhotoByPath(path);
            mSketchView.setEditMode(SketchView.EDIT_PHOTO);
        }
    }

    @Override
    public void onDrawChanged() {
        // 撤销
        if (mSketchView.getEditMode() == SketchView.EDIT_STROKE && mSketchView.getStrokeRecordCount() > 0) {
            mUndoImageView.setEnabled(true);
            mUndoImageView.setAlpha(1f);
        } else {
            mUndoImageView.setEnabled(false);
            mUndoImageView.setAlpha(0.2f);
        }
        // 恢复
        if (mSketchView.getEditMode() == SketchView.EDIT_STROKE && mSketchView.getRedoCount() > 0) {
            mRedoImageView.setEnabled(true);
            mRedoImageView.setAlpha(1f);
        } else {
            mRedoImageView.setEnabled(false);
            mRedoImageView.setAlpha(0.2f);
        }
        // 绘图记录
        if (mSketchView.getRecordCount() > 0) {
            mDeleteImageView.setEnabled(true);
//            mSaveImageView.setEnabled(true);
//            mSaveImageView.setAlpha(1f);
            mDeleteImageView.setAlpha(1f);
        } else {
            mDeleteImageView.setEnabled(false);
//            mSaveImageView.setEnabled(false);
//            mSaveImageView.setAlpha(0.2f);
            mDeleteImageView.setAlpha(0.2f);
        }
    }

    @OnClick(R.id.message_board_pen)
    public void pen() { // 画笔
        if (mSketchView.getStrokeType() == STROKE_TYPE_DRAW && mSketchView.getEditMode() == SketchView.EDIT_STROKE) {
            if (mPenEditDialog.isAdded()) return;
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            mPenEditDialog.show(transaction, TAG);
        } else {
            mSketchView.setStrokeType(STROKE_TYPE_DRAW);
            mSketchView.setEditMode(SketchView.EDIT_STROKE);
            mPenImageView.setImageResource(R.drawable.icon_message_board_pen_active);
            mEraserImageView.setImageResource(R.drawable.icon_message_board_eraser_normal);
            mTextImageView.setImageResource(R.drawable.icon_message_board_text_normal);
            mPhotoImageView.setImageResource(R.drawable.icon_message_board_photo_normal);
            mCameraImageView.setImageResource(R.drawable.icon_message_board_camera_normal);
        }
    }

    @OnClick(R.id.message_board_eraser)
    public void eraser() { // 橡皮擦
        if (mSketchView.getStrokeType() == STROKE_TYPE_ERASER && mSketchView.getEditMode() == SketchView.EDIT_STROKE) {
            if (mEraserEditDialog.isAdded()) return;
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            mEraserEditDialog.show(transaction, TAG);
        } else {
            mSketchView.setStrokeType(STROKE_TYPE_ERASER);
            mSketchView.setEditMode(SketchView.EDIT_STROKE);
            mPenImageView.setImageResource(R.drawable.icon_message_board_pen_normal);
            mEraserImageView.setImageResource(R.drawable.icon_message_board_eraser_active);
            mTextImageView.setImageResource(R.drawable.icon_message_board_text_normal);
            mPhotoImageView.setImageResource(R.drawable.icon_message_board_photo_normal);
            mCameraImageView.setImageResource(R.drawable.icon_message_board_camera_normal);
        }
    }

    @OnClick(R.id.message_board_text)
    public void text() { // 文字
        if (mTextEditDialog.isAdded()) return;
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putString("text", "");
        mTextEditDialog.setArguments(bundle);
        mTextEditDialog.show(transaction, TAG);
        mTextEditDialog.setOnDismissListener(content -> {
            if (content != null && !content.trim().equals("")) {
                mSketchView.addText(content);
            }
        });
        if (mSketchView.getEditMode() != SketchView.EDIT_TEXT) {
            mSketchView.setEditMode(SketchView.EDIT_TEXT);
            mPenImageView.setImageResource(R.drawable.icon_message_board_pen_normal);
            mEraserImageView.setImageResource(R.drawable.icon_message_board_eraser_normal);
            mTextImageView.setImageResource(R.drawable.icon_message_board_text_active);
            mPhotoImageView.setImageResource(R.drawable.icon_message_board_photo_normal);
            mCameraImageView.setImageResource(R.drawable.icon_message_board_camera_normal);
        }
    }

    @OnClick(R.id.message_board_photo)
    public void photo() { // 图片
        Intent intent = new Intent(MessageBoardActivity.this, AlbumActivity.class);
        intent.putExtra(AppConstants.CHOSE_ALBUM, true);
        startActivityForResult(intent, AppConstants.CODE_CHOSE_ALBUM);
        mPhotoImageView.setImageResource(R.drawable.icon_message_board_photo_active);
        mCameraImageView.setImageResource(R.drawable.icon_message_board_camera_normal);
        if (mSketchView.getEditMode() != SketchView.EDIT_PHOTO) {
            mSketchView.setEditMode(SketchView.EDIT_PHOTO);
            mPenImageView.setImageResource(R.drawable.icon_message_board_pen_normal);
            mEraserImageView.setImageResource(R.drawable.icon_message_board_eraser_normal);
            mTextImageView.setImageResource(R.drawable.icon_message_board_text_normal);
        }
    }

    @OnClick(R.id.message_board_camera)
    public void camera() { // 拍照
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);// 启动系统相机
        if (intent.resolveActivity(getPackageManager()) != null) { // 判断是否有相机应用
            File path = new File(mPath);// 路径
            if (!path.exists()) logUtil.d(TAG, "create cache path:" + path.mkdirs());
            mCameraFile = new File(mPath, System.currentTimeMillis() + ".jpg");// 拍照保存文件
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) { // 7.0及以上
                Uri uri = FileProvider.getUriForFile(FridgeApplication.getContext(), "com.viomi.fridge.vertical.fileprovider", mCameraFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            } else {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mCameraFile));
            }
            startActivityForResult(intent, AppConstants.CODE_CAMERA);
        }
        mCameraImageView.setImageResource(R.drawable.icon_message_board_camera_active);
        mPhotoImageView.setImageResource(R.drawable.icon_message_board_photo_normal);
        if (mSketchView.getEditMode() != SketchView.EDIT_PHOTO) {
            mSketchView.setEditMode(SketchView.EDIT_PHOTO);
            mPenImageView.setImageResource(R.drawable.icon_message_board_pen_normal);
            mEraserImageView.setImageResource(R.drawable.icon_message_board_eraser_normal);
            mTextImageView.setImageResource(R.drawable.icon_message_board_text_normal);
        }
    }

    @OnClick(R.id.message_board_undo)
    public void undo() { // 撤销
        mSketchView.undo();
    }

    @OnClick(R.id.message_board_redo)
    public void redo() { // 恢复
        mSketchView.redo();
    }

    @OnClick(R.id.message_board_delete)
    public void delete() { // 删除
        if (mSketchView.getRecordCount() > 0) {
            BaseAlertDialog dialog = new BaseAlertDialog(this, getResources().getString(R.string.message_board_delete_tip),
                    getResources().getString(R.string.cancel), getResources().getString(R.string.confirm));
            dialog.setOnLeftClickListener(dialog::dismiss);
            dialog.setOnRightClickListener(() -> {
                mSketchView.erase(true);// 删除所有绘图
                dialog.dismiss();
            });
            dialog.show();
        }
    }

    @OnClick(R.id.message_board_save)
    public void save() { // 保存
        mPresenter.save(mSketchView);
    }

    @OnClick(R.id.title_bar_all)
    public void enterHistory() { // 进入全部留言
        Intent intent = new Intent(MessageBoardActivity.this, MessageBoardListActivity.class);
        startActivity(intent);
    }

    @Override
    public void onPenEditDismiss(int color, int alpha, int size) {
        mSketchView.setStrokeColor(color);// 画笔颜色
        mSketchView.setStrokeAlpha(alpha);// 画笔透明度
        mSketchView.setSize(size, STROKE_TYPE_DRAW);// 画笔大小
    }

    @Override
    public void onEraserEditDismiss(int size) {
        mSketchView.setSize(size, STROKE_TYPE_ERASER);// 橡皮擦大小
    }

    @Override
    public void saveFinish() { // 保存成功
        RxBus.getInstance().post(BusEvent.MSG_MESSAGE_BOARD_UPDATE);// 留言板更新
        finish();
    }

    @Override
    public void setTitleBarUi(List<MessageBoardEntity> list) {
        if (list == null || list.size() == 0) mAllLinearLayout.setVisibility(View.GONE);
        else mAllLinearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onText(View view, StrokeRecord record) { // 添加文字

    }

    @Override
    public void onTextEdit(String text) {
        if (mTextEditDialog.isAdded()) return;
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putString("text", text);
        mTextEditDialog.setArguments(bundle);
        mTextEditDialog.show(transaction, TAG);
        mTextEditDialog.setOnDismissListener(content -> {
            if (content != null && !content.trim().equals("") && !content.equals(text)) {
                mSketchView.updateCurText(content);
            }
        });
    }
}