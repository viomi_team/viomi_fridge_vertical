package com.viomi.fridge.vertical.speech.contract;

import com.viomi.fridge.vertical.common.callback.BasePresenter;

/**
 * 语音助手设置 Contract
 * Created by William on 2018/1/31.
 */
public interface SpeechManageContract {
    interface View {
        void showSetting(boolean enable);// 显示设置
    }

    interface Presenter extends BasePresenter<View> {
        void loadSetting();// 加载设置

        void cacheSwitch(boolean enable);// 缓存设置
    }
}
