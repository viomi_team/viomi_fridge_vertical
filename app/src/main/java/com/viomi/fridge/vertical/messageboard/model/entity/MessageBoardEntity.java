package com.viomi.fridge.vertical.messageboard.model.entity;

import java.io.File;

/**
 * 留言板相关信息
 * Created by William on 2018/4/11.
 */
public class MessageBoardEntity {
    private File file;// 图片文件
    private boolean selected = false;// 是否被选中

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}