package com.viomi.fridge.vertical.administration.view.fragment;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.model.preference.ManagePreference;
import com.viomi.fridge.vertical.administration.view.adapter.ScreenTimeAdapter;
import com.viomi.fridge.vertical.common.base.BaseFragment;
import com.viomi.fridge.vertical.common.scope.ActivityScoped;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.widget.popupwindow.CommonPopupWindow;
import com.viomi.widget.switchbutton.SwitchButton;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 屏幕与声音 Fragment
 * Created by William on 2018/1/31.
 */
@ActivityScoped
public class MediaFragment extends BaseFragment implements CommonPopupWindow.ViewInterface {
    private static final String TAG = MediaFragment.class.getSimpleName();
    private List<String> mStringList;// 屏保时间集合
    private List<Integer> mIntegerList;// 屏保时间集合
    private CommonPopupWindow mPopupWindow;// 时间选择 PopupWindow
    private AudioManager mAudioManager;
    private MediaPlayer mMediaPlayer;
    private NotificationManager mNotificationManager;

    @BindView(R.id.media_human_sensor_switch)
    SwitchButton mSwitchButton;// 人感开关

    @BindView(R.id.media_bright_seek)
    SeekBar mBrightSeekBar;// 亮度调节
    @BindView(R.id.media_audio_seek)
    SeekBar mAudioSeekBar;// 音量调节

    @BindView(R.id.media_screen_time)
    TextView mTextView;// 屏保时间

    @Inject
    public MediaFragment() {

    }

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.fragment_media;
    }

    @Override
    protected void initWithOnCreateView() {
        initHumanSensorButton();
        initBrightness();
        initAudio();
        initScreenSleep();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        stopPlay();
    }

    /**
     * 初始化人体传感器按钮
     */
    private void initHumanSensorButton() {
        boolean enable = ManagePreference.getInstance().getHumanSensorState();
        mSwitchButton.setOn(enable);
        mSwitchButton.setOnSwitchStateChangeListener(isOn -> {
            ToolUtil.setHumanSensorState(isOn);
            mSwitchButton.setOn(isOn, true);
        });
    }

    /**
     * 初始化屏幕亮度
     */
    private void initBrightness() {
        int brightness = 0;
        try {
            brightness = Settings.System.getInt(FridgeApplication.getContext().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            logUtil.e(TAG, e.getMessage());
            e.printStackTrace();
        }
        mBrightSeekBar.setProgress(brightness);
        mBrightSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Uri uri = Settings.System.getUriFor(Settings.System.SCREEN_BRIGHTNESS);
                Settings.System.putInt(FridgeApplication.getContext().getContentResolver(),
                        Settings.System.SCREEN_BRIGHTNESS, progress);
                FridgeApplication.getContext().getContentResolver().notifyChange(uri, null);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    /**
     * 初始化音量
     */
    private void initAudio() {
        int systemVolume = 0, musicVolume = 0, systemMaxVolume = 0, musicMaxVolume = 0;
        boolean flag;
        mAudioManager = (AudioManager) FridgeApplication.getContext().getSystemService(Context.AUDIO_SERVICE);
        mNotificationManager = (NotificationManager) FridgeApplication.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        if (mAudioManager != null) {
            systemVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_SYSTEM);
            musicVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            systemMaxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM);
            musicMaxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            mAudioSeekBar.setMax(systemMaxVolume <= musicMaxVolume ? systemMaxVolume : musicMaxVolume);
        }
        flag = systemMaxVolume <= musicMaxVolume;
        int ratio = systemMaxVolume <= musicMaxVolume ? musicMaxVolume / systemMaxVolume : systemMaxVolume / musicMaxVolume;
        mAudioSeekBar.setProgress(systemMaxVolume <= musicMaxVolume ? systemVolume : musicVolume);
        mAudioSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N
                        && mNotificationManager != null && !mNotificationManager.isNotificationPolicyAccessGranted()) {
                    Intent intent = new Intent(android.provider.Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);
                    startActivity(intent);
                    return;
                }
                if (flag) {
                    mAudioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, progress, 0);
                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress * ratio, 0);
                } else {
                    mAudioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, progress * ratio, 0);
                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
                }

                if (mMediaPlayer != null && mMediaPlayer.isPlaying()) return;
                startPlay();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (mMediaPlayer != null && mMediaPlayer.isPlaying()) mMediaPlayer.stop();
            }
        });

        mAudioManager = (AudioManager) FridgeApplication.getContext().getSystemService(Context.AUDIO_SERVICE);
    }

    /**
     * 初始化待机时间
     */
    private void initScreenSleep() {
        mStringList = new ArrayList<>();
        mIntegerList = new ArrayList<>();
        mStringList.add(FridgeApplication.getContext().getResources().getString(R.string.management_media_15_second));
        mStringList.add(FridgeApplication.getContext().getResources().getString(R.string.management_media_30_second));
        mStringList.add(FridgeApplication.getContext().getResources().getString(R.string.management_media_1_minute));
        mStringList.add(FridgeApplication.getContext().getResources().getString(R.string.management_media_2_minute));
        mStringList.add(FridgeApplication.getContext().getResources().getString(R.string.management_media_5_minute));
        mStringList.add(FridgeApplication.getContext().getResources().getString(R.string.management_media_10_minute));
        mStringList.add(FridgeApplication.getContext().getResources().getString(R.string.management_media_30_minute));
        mIntegerList.add(15 * 1000);
        mIntegerList.add(30 * 1000);
        mIntegerList.add(60 * 1000);
        mIntegerList.add(2 * 60 * 1000);
        mIntegerList.add(5 * 60 * 1000);
        mIntegerList.add(10 * 60 * 1000);
        mIntegerList.add(30 * 60 * 1000);

        try {
            int screenTime = Settings.System.getInt(FridgeApplication.getContext().getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT);
            if (mIntegerList.contains(screenTime)) {
                mTextView.setText(mStringList.get(mIntegerList.indexOf(screenTime)));
            } else {
                mTextView.setText(mStringList.get(4));
            }
        } catch (Settings.SettingNotFoundException e) {
            logUtil.e(TAG, e.getMessage());
            e.printStackTrace();
        }
    }

    @OnClick(R.id.media_screen_time)
    public void choseScreenTime() {
        if (mPopupWindow != null && mPopupWindow.isShowing()) return;
        mPopupWindow = new CommonPopupWindow.Builder(getActivity())
                .setView(R.layout.popup_window_screen_time)
                .setOutsideTouchable(true)
                .setWidthAndHeight(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                .setViewOnclickListener(this)
                .create();
        mPopupWindow.showAsDropDown(mTextView, Math.abs(mTextView.getMeasuredWidth() / 2 - mPopupWindow.getWidth() / 2),
                -mTextView.getMeasuredHeight());
    }

    /**
     * 播放音乐
     */
    private void startPlay() {
        mMediaPlayer = MediaPlayer.create(FridgeApplication.getContext(), R.raw.ok2);
        if (mAudioManager != null) {
            if (mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC) != 0) {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                try {
                    mMediaPlayer.prepare();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mMediaPlayer.setLooping(true);
                mMediaPlayer.start();
            }
        }
    }

    private void stopPlay() {
        if (mMediaPlayer != null) {
            if (mMediaPlayer.isPlaying()) mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        if (mAudioManager != null) mAudioManager = null;
    }

    @Override
    public void getChildView(View view, int layoutResId) {
        RecyclerView recyclerView = view.findViewById(R.id.screen_time_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        ScreenTimeAdapter adapter = new ScreenTimeAdapter(mStringList);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener((parent, view1, position, id) -> {
            mTextView.setText(mStringList.get(position));
            Settings.System.putInt(FridgeApplication.getContext().getContentResolver(),
                    Settings.System.SCREEN_OFF_TIMEOUT, mIntegerList.get(position));
            mPopupWindow.dismiss();
        });
    }
}
