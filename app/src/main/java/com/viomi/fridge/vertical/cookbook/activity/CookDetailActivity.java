package com.viomi.fridge.vertical.cookbook.activity;

import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseActivity;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.ImageLoadUtil;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.cookbook.model.CookMenuDetail;
import com.viomi.fridge.vertical.cookbook.model.CookMenuMethod;
import com.viomi.fridge.vertical.cookbook.model.CookMenuRecipe;
import com.viomi.fridge.vertical.cookbook.view.adapter.CookMethodAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import rx.Subscription;

/**
 * 菜谱详情 Activity
 * Created by nanquan on 2018/2/27.
 */
public class CookDetailActivity extends BaseActivity {
    private static final String TAG = CookDetailActivity.class.getSimpleName();
    private int mStep = 0;// 步骤
    private List<String> mList;
    private Subscription mSubscription;
    private CookMenuDetail mCookMenuDetail;// 详情数据
    private String mSummary = "";// 介绍
    private boolean mIsMain = false;

    @BindView(R.id.sl_cook_menu)
    NestedScrollView mNestedScrollView;// 滚动

    @BindView(R.id.iv_cook_img)
    ImageView mImageView;// 菜式图片

    @BindView(R.id.tv_cook_intro)
    TextView mSummaryTextView;// 菜式介绍
    @BindView(R.id.tv_cook_intro2)
    TextView mSummary2TextView;// 菜式介绍
    @BindView(R.id.tv_cook_name)
    TextView mNameTextView;// 菜式名称
    @BindView(R.id.tv_cook_material)
    TextView mIngredientTextView;// 材料
    @BindView(R.id.tv_cook_menu_title)
    TextView mMethodTitleTextView;// 步骤标题

    @BindView(R.id.tl_cook_material)
    TableLayout mTableLayout;

    @BindView(R.id.rv_cook_method)
    RecyclerView mRecyclerView;// 制作步骤

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        layoutId = R.layout.activity_cook_detail;
        mTitle = getResources().getString(R.string.cookbook_health_title);
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mCookMenuDetail = (CookMenuDetail) bundle.getSerializable("cookMenuDetail");
            mIsMain = bundle.getBoolean("isMain", false);
        }
        initView();

        mSubscription = RxBus.getInstance().subscribe(busEvent -> {
            switch (busEvent.getMsgId()) {
                case BusEvent.MSG_NEXT_STEP: // 下一步
                    if (mStep + 1 > mList.size()) {
                        RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, "已到最后一步");
                    } else {
                        mStep++;
                        RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, mList.get(mStep - 1));
                    }
                    break;
                case BusEvent.MSG_LAST_STEP: // 上一步
                    if (mStep - 1 <= 0) return;
                    else {
                        mStep--;
                        RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, mList.get(mStep - 1));
                    }
                    break;
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
        if (mList != null) {
            mList.clear();
            mList = null;
        }
        if (mCookMenuDetail != null) mCookMenuDetail = null;
        if (mIsMain) {
            if (ToolUtil.checkApkExist(FridgeApplication.getContext(), "com.unilife.fridge.yunmi.recipe")) {
                try {
                    ToolUtil.startOtherApp(FridgeApplication.getContext(), "com.unilife.fridge.yunmi.recipe", false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void initView() {
        mList = new ArrayList<>();
        if (mCookMenuDetail == null) {
            finish();
            return;
        }
        CookMenuRecipe cookMenuRecipe = mCookMenuDetail.getRecipe();
        if (cookMenuRecipe == null) {
            finish();
            return;
        }
        // 菜式图片
        if (cookMenuRecipe.getImg() != null && !TextUtils.isEmpty(cookMenuRecipe.getImg())) {
            ImageLoadUtil.getInstance().loadUrl(cookMenuRecipe.getImg(), mImageView);
        } else {
            ImageLoadUtil.getInstance().loadResource(R.drawable.cook_menu_default, mImageView);
        }
        // 菜式名字
        mNameTextView.setText(mCookMenuDetail.getName() == null ? "" : mCookMenuDetail.getName());
        // 用料
        if (cookMenuRecipe.getIngredients() != null && !TextUtils.isEmpty(cookMenuRecipe.getIngredients())) {
            Gson gson = new Gson();
            List<String> ingredientList = gson.fromJson(cookMenuRecipe.getIngredients(),
                    new TypeToken<List<String>>() {
                    }.getType());
            StringBuilder ingredients = new StringBuilder();
            ingredients.append(ingredientList.get(0));
            for (int i = 1; i < ingredientList.size(); i++) {
                ingredients.append("\n").append(ingredientList.get(i));
            }
            mIngredientTextView.setText(ingredients);
        } else if (cookMenuRecipe.getIngredients2() != null && !TextUtils.isEmpty(cookMenuRecipe.getIngredients2())) {
            mIngredientTextView.setText(cookMenuRecipe.getIngredients2());
        } else {
            mTableLayout.setVisibility(View.GONE);
        }
        // 描述
        if (cookMenuRecipe.getSumary() != null && !TextUtils.isEmpty(cookMenuRecipe.getSumary())) {
            logUtil.d(TAG, cookMenuRecipe.getSumary());
            if (cookMenuRecipe.getSumary().length() > 260) {
                mSummary = cookMenuRecipe.getSumary();
                String str = cookMenuRecipe.getSumary().substring(0, 260);
                str = str + "..." + getResources().getString(R.string.cookbook_look_more);
                str = str.replace(" ", "");
                mSummaryTextView.setText(str);
                SpannableStringBuilder spannable = new SpannableStringBuilder(str);
                int startIndex = str.indexOf(getResources().getString(R.string.cookbook_look_more));
                int endIndex = startIndex + getResources().getString(R.string.cookbook_look_more).length();
                // 文字点击
                spannable.setSpan(new MoreTextClick(), startIndex, endIndex
                        , Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                // 一定要记得设置，不然点击不生效
                mSummaryTextView.setMovementMethod(LinkMovementMethod.getInstance());
                mSummaryTextView.setText(spannable);
            } else {
                mSummaryTextView.setText(cookMenuRecipe.getSumary());
            }
        }
        // 步骤标题
        if (cookMenuRecipe.getTitle() != null && !TextUtils.isEmpty(cookMenuRecipe.getTitle())) {
            mMethodTitleTextView.setText(cookMenuRecipe.getTitle());
        }
        // 设置步骤
        if (cookMenuRecipe.getMethod() != null && !TextUtils.isEmpty(cookMenuRecipe.getMethod())) {
            Gson gson = new Gson();
            List<CookMenuMethod> methodList = gson.fromJson(cookMenuRecipe.getMethod(),
                    new TypeToken<List<CookMenuMethod>>() {
                    }.getType());
            LinearLayoutManager layoutManager = new LinearLayoutManager(this) {
                @Override
                public boolean canScrollVertically() {
                    return false;
                }
            };
            layoutManager.setAutoMeasureEnabled(true);
            mRecyclerView.setLayoutManager(layoutManager);
            CookMethodAdapter methodAdapter = new CookMethodAdapter(this, methodList);
            mRecyclerView.setAdapter(methodAdapter);
            for (int i = 0; i < methodList.size(); i++) mList.add(methodList.get(i).getStep());
        } else if (cookMenuRecipe.getList() != null) {
            LinearLayoutManager layoutManager = new LinearLayoutManager(this) {
                @Override
                public boolean canScrollVertically() {
                    return false;
                }
            };
            layoutManager.setAutoMeasureEnabled(true);
            mRecyclerView.setLayoutManager(layoutManager);
            CookMethodAdapter methodAdapter = new CookMethodAdapter(this, cookMenuRecipe.getList());
            mRecyclerView.setAdapter(methodAdapter);
            for (int i = 0; i < cookMenuRecipe.getList().size(); i++)
                mList.add(cookMenuRecipe.getList().get(i).getStep());
        }
        mNestedScrollView.postDelayed(() -> mNestedScrollView.scrollTo(0, 0), 100);
    }

    /**
     * 获取 TextView 每行内容
     */
    private String getTextLine() {
        Layout layout = mSummaryTextView.getLayout();
        StringBuilder result = new StringBuilder();
        String text = layout.getText().toString();
        for (int i = 0; i < 9; i++) {
            int start = layout.getLineStart(i);
            int end = layout.getLineEnd(i);
            result.append(text.substring(start, end));
        }
        logUtil.d(TAG, result.toString());
        return result.toString();
    }

    private class MoreTextClick extends ClickableSpan {

        @Override
        public void onClick(View widget) {
            // 在此处理点击事件
            mSummaryTextView.setText(mSummary);
            if (mSummaryTextView.getLayout().getLineCount() <= 9) return;
            mSummaryTextView.setText(getTextLine());
            mSummary2TextView.setVisibility(View.VISIBLE);
            String str = mSummary.replace(mSummaryTextView.getText().toString(), "");
            logUtil.d(TAG, str);
            str = str + getResources().getString(R.string.cookbook_pack_up);
            str = str.replace(" ", "");
            mSummary2TextView.setText(str);
            SpannableStringBuilder spannable = new SpannableStringBuilder(str);
            int startIndex = str.indexOf(getResources().getString(R.string.cookbook_pack_up));
            int endIndex = startIndex + getResources().getString(R.string.cookbook_pack_up).length();
            // 文字点击
            spannable.setSpan(new CloseTextClick(), startIndex, endIndex
                    , Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            // 一定要记得设置，不然点击不生效
            mSummary2TextView.setMovementMethod(LinkMovementMethod.getInstance());
            mSummary2TextView.setText(spannable);
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            ds.setColor(0xFF44D4A0);
        }
    }

    private class CloseTextClick extends ClickableSpan {

        @Override
        public void onClick(View widget) {
            // 在此处理点击事件
            mSummary2TextView.setVisibility(View.GONE);
            String str = mSummary.substring(0, 260);
            str = str + "..." + getResources().getString(R.string.cookbook_look_more);
            str = str.replace(" ", "");
            mSummaryTextView.setText(str);
            SpannableStringBuilder spannable = new SpannableStringBuilder(str);
            int startIndex = str.indexOf(getResources().getString(R.string.cookbook_look_more));
            int endIndex = startIndex + getResources().getString(R.string.cookbook_look_more).length();
            // 文字点击
            spannable.setSpan(new MoreTextClick(), startIndex, endIndex
                    , Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            // 一定要记得设置，不然点击不生效
            mSummaryTextView.setMovementMethod(LinkMovementMethod.getInstance());
            mSummaryTextView.setText(spannable);
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            ds.setColor(0xFF44D4A0);
        }
    }
}
