package com.viomi.fridge.vertical.iot.model.repository;

import com.viomi.fridge.vertical.administration.model.preference.LoginPreference;
import com.viomi.fridge.vertical.common.http.ApiClient;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.iot.model.http.entity.CameraStatus;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by hailang on 2018/5/15 0015.
 */

public class LockReposity {

    public static Observable<String> turnOnCamera(String devID, String GwID, String type) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("devID", devID);
            jsonObject.put("gwID", GwID);
            jsonObject.put("type", type);
            jsonObject.put("method", "openCamera");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url = ApiClient.STORE_DEBUG_HOST + "/wlink/action/execute?token=" + LoginPreference.getInstance().getUserToken();
        logUtil.d("LockReposity", "turnOnCamera:" + url + jsonObject.toString());
        return ApiClient.getInstance().getApiService().wlinkActionExecute(
                url
                , ApiClient.getInstance().getRequestBody(jsonObject));

    }

    public static Observable<String> getWlinkDevices(String token) {

        return ApiClient.getInstance().getApiService().wlinkDevices(token);
    }

    public static Observable<String> getWlinkDeviceChildren(String token) {

        return ApiClient.getInstance().getApiService().wlinkDeviceChildren(token);
    }

    public static Observable<CameraStatus> getCameraStatus(String devID, String type) {

        return ApiClient.getInstance().getApiService().getCameraStatus(
                LoginPreference.getInstance().getUserToken()
                , devID
                , type);
    }

    public static Observable<String> turnOffCamera(String devID, String GwID, String type) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("devID", devID);
            jsonObject.put("gwID", GwID);
            jsonObject.put("type", type);
            jsonObject.put("method", "closeCamera");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url = ApiClient.STORE_DEBUG_HOST + "/wlink/action/execute?token=" + LoginPreference.getInstance().getUserToken();
        return ApiClient.getInstance().getApiService().wlinkActionExecute(
                url,
                ApiClient.getInstance().getRequestBody(jsonObject));
    }

    public static Observable<String> doorUnlock(String devID, String GwID, String pass, String type) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("devID", devID);
            jsonObject.put("gwID", GwID);
            jsonObject.put("type", type);
            jsonObject.put("method", "unLock");
            jsonObject.put("parameter", pass);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url = ApiClient.STORE_DEBUG_HOST + "/wlink/action/execute?token=" + LoginPreference.getInstance().getUserToken();
        logUtil.d("LockReposity", "doorUnlock:" + url + jsonObject.toString());
        return ApiClient.getInstance().getApiService().wlinkActionExecute(
                url,
                ApiClient.getInstance().getRequestBody(jsonObject));
    }
}
