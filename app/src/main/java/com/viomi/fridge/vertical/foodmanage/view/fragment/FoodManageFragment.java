package com.viomi.fridge.vertical.foodmanage.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.foodmanage.contract.FoodFragmentContract;
import com.viomi.fridge.vertical.foodmanage.model.entity.FoodDetail;
import com.viomi.fridge.vertical.foodmanage.presenter.FoodFragmentPresenter;
import com.viomi.fridge.vertical.foodmanage.view.adapter.FoodManageAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import rx.Subscription;

/**
 * 食材管理 Fragment
 * Created by William on 2018/1/9.
 */
public class FoodManageFragment extends Fragment implements FoodFragmentContract.View, AdapterView.OnItemClickListener {
    private static final String TAG = FoodManageFragment.class.getSimpleName();
    private static final String PARAM = "type";
    private int mType;// 仓室类型
    private Context mContext;
    private Unbinder mUnBinder;
    private List<FoodDetail> mList;// 食材信息集合
    private FoodManageAdapter mAdapter;// 食材管理适配器
    private OnFoodEditListener onFoodEditListener;
    private FoodFragmentContract.Presenter mPresenter;
    private Subscription mSubscription;

    @BindView(R.id.food_manage_list)
    RecyclerView mRecyclerView;// 食材管理列表

    @BindView(R.id.food_manage_default)
    ImageView mImageView;// 默认图片

    public static FoodManageFragment newInstance(int type) {
        Bundle bundle = new Bundle();
        FoodManageFragment fragment = new FoodManageFragment();
        bundle.putInt(PARAM, type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) mType = getArguments().getInt(PARAM);
        mList = new ArrayList<>();
        mPresenter = new FoodFragmentPresenter();
        mPresenter.subscribe(this);// 订阅
        mSubscription = RxBus.getInstance().subscribe(busEvent -> {
            switch (busEvent.getMsgId()) {
                case BusEvent.MSG_FOOD_UPDATE: // 食材更新
                    List<FoodDetail> list = (List<FoodDetail>) busEvent.getMsgObject();
                    if (mAdapter != null && mAdapter.isEditMode()) return;
                    if (mPresenter != null) {
                        mPresenter.loadFood(list, mList, mType);
                    }
                    break;
                case BusEvent.MSG_FOOD_MANAGE_EDIT_CHANGE:// 编辑状态改变
                    if (mAdapter.isEditMode()) { // 取消编辑状态
                        mAdapter.setIsEditMode(false);
                        for (FoodDetail detail : mList) {
                            detail.setSelected(false);
                        }
                        mAdapter.notifyDataSetChanged();
                    } else { // 编辑状态
                        if (busEvent.getMsgObject() == null) return;
                        int type = (int) busEvent.getMsgObject();
                        if (type != mType) return;
                        mAdapter.setIsEditMode(true);
                        mAdapter.notifyDataSetChanged();
                    }
                    break;
                case BusEvent.MSG_FOOD_MANAGE_DELETE: // 删除食材
                    int type = (int) busEvent.getMsgObject();
                    if (type != mType) return;
                    if (mPresenter != null) mPresenter.deleteFood(mList);
                    mAdapter.setIsEditMode(false);
                    break;
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_food_manage, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        mContext = getActivity();
        initView();
        return view;
    }

    private void initView() {
        mAdapter = new FoodManageAdapter(mContext, mList);
        mAdapter.setOnItemClickListener(this);
        mRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 4));
        mRecyclerView.setAdapter(mAdapter);

        if (mType == 0) {
            RxBus.getInstance().post(BusEvent.MSG_START_FOOD_UPDATE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mPresenter != null) {
            mPresenter.unSubscribe();
            mPresenter = null;
        }
        if (mUnBinder != null) mUnBinder.unbind();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
        if (mAdapter != null) mAdapter = null;
        if (mList != null) {
            mList.clear();
            mList = null;
        }
        if (mContext != null) mContext = null;
    }

    @Override
    public void refreshList(List<FoodDetail> list) {
        logUtil.d(TAG, "type:" + mType + "," + list.size() + "");
        if (mList.size() == 0) {
            mRecyclerView.setVisibility(View.GONE);
            mImageView.setVisibility(View.VISIBLE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            mImageView.setVisibility(View.GONE);
        }
    }

    @Override
    public void notifyDataChanged() {
        if (getActivity() != null)
            getActivity().runOnUiThread(() -> {
                if (mAdapter != null) mAdapter.notifyDataSetChanged();
            });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (mAdapter.isEditMode()) {
            mList.get(position).setSelected(!mList.get(position).isSelected());
            mAdapter.notifyItemChanged(position);
        } else {
            FoodDetail foodDetail = mList.get(position);
            if (onFoodEditListener != null) onFoodEditListener.onFoodEdit(foodDetail);
        }
    }

    public interface OnFoodEditListener {
        void onFoodEdit(FoodDetail detail);
    }

    public void setOnFoodEditListener(OnFoodEditListener onFoodEditListener) {
        this.onFoodEditListener = onFoodEditListener;
    }
}