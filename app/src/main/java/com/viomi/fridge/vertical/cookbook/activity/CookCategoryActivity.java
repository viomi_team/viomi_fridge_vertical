package com.viomi.fridge.vertical.cookbook.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseActivity;
import com.viomi.fridge.vertical.cookbook.model.CookMenuCategory;
import com.viomi.fridge.vertical.cookbook.view.adapter.CookCategoryPagerAdapter;

import butterknife.BindView;

/**
 * 菜谱分类页面 Activity
 * Created by nanquan on 2018/2/26.
 */
public class CookCategoryActivity extends BaseActivity {
    private CookMenuCategory mCookMenuCategory;

    @BindView(R.id.tl_cook_category)
    TabLayout mTabLayout;

    @BindView(R.id.vp_cook_category)
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        layoutId = R.layout.activity_cook_category;
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mCookMenuCategory = (CookMenuCategory) bundle.getSerializable("cookMenuCategory");
            mTitle = bundle.getString("title");
        }
        super.onCreate(savedInstanceState);
        initViewPager();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCookMenuCategory != null) mCookMenuCategory = null;
    }

    private void initViewPager() {
        mViewPager.setAdapter(new CookCategoryPagerAdapter(getSupportFragmentManager(), mCookMenuCategory.getChilds()));
        mTabLayout.setupWithViewPager(mViewPager);
    }
}