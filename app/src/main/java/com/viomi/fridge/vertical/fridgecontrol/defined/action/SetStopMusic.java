package com.viomi.fridge.vertical.fridgecontrol.defined.action;

import com.viomi.fridge.vertical.fridgecontrol.defined.ViomiDefined;
import com.viomi.fridge.vertical.fridgecontrol.defined.property.StopMusic;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetStopMusic extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setStopMusic.toActionType();

    public SetStopMusic() {
        super(TYPE);

        super.addArgument(StopMusic.TYPE.toString());
    }
}