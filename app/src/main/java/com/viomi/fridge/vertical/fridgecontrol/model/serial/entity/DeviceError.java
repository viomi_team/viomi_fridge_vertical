package com.viomi.fridge.vertical.fridgecontrol.model.serial.entity;

import java.io.Serializable;

/**
 * 设备故障信息
 * Created by William on 2018/1/17.
 */
public class DeviceError implements Serializable {
    private boolean error_communication = true;// 通信故障
    private boolean error_rc_sensor;// 冷藏室传感器故障
    private boolean error_cc_sensor;// 变温室传感器故障
    private boolean error_fc_sensor;// 冷冻室传感器故障
    private boolean error_rc_defrost_sensor;// 冷藏化霜传感器故障
    private boolean error_fc_defrost_sensor;// 冷冻化霜传感器故障
    private boolean error_indoor_sensor;// 环境温度传感器故障
    private boolean error_fan_door;// 风门故障
    private boolean error_rc_fan;// 冷藏风扇故障
    private boolean error_cc_fan;// 冷凝风扇故障
    private boolean error_fc_fan;// 冷冻风扇故障
    private boolean rc_door_open_alarm;// 冷藏室门开报警
    private boolean cc_door_open_alarm;// 变温室门开报警
    private boolean fc_door_open_alarm;// 冷冻室门开报警
    private boolean error_defrost;// 化霜不良报警
    private boolean error_cc_defrost_sensor;// 变温化霜传感器故障
    private boolean error_humidity_sensor;// 湿度传感器故障
    private boolean error_humidity_temp_sensor;// 湿度传感器温度故障
    private boolean error_rc_cc_sensor;// 冷藏变温传感器故障

    public boolean isError_communication() {
        return error_communication;
    }

    public void setError_communication(boolean error_communication) {
        this.error_communication = error_communication;
    }

    public boolean isError_rc_sensor() {
        return error_rc_sensor;
    }

    public void setError_rc_sensor(boolean error_rc_sensor) {
        this.error_rc_sensor = error_rc_sensor;
    }

    public boolean isError_cc_sensor() {
        return error_cc_sensor;
    }

    public void setError_cc_sensor(boolean error_cc_sensor) {
        this.error_cc_sensor = error_cc_sensor;
    }

    public boolean isError_fc_sensor() {
        return error_fc_sensor;
    }

    public void setError_fc_sensor(boolean error_fc_sensor) {
        this.error_fc_sensor = error_fc_sensor;
    }

    public boolean isError_rc_defrost_sensor() {
        return error_rc_defrost_sensor;
    }

    public void setError_rc_defrost_sensor(boolean error_rc_defrost_sensor) {
        this.error_rc_defrost_sensor = error_rc_defrost_sensor;
    }

    public boolean isError_fc_defrost_sensor() {
        return error_fc_defrost_sensor;
    }

    public void setError_fc_defrost_sensor(boolean error_fc_defrost_sensor) {
        this.error_fc_defrost_sensor = error_fc_defrost_sensor;
    }

    public boolean isError_indoor_sensor() {
        return error_indoor_sensor;
    }

    public void setError_indoor_sensor(boolean error_indoor_sensor) {
        this.error_indoor_sensor = error_indoor_sensor;
    }

    public boolean isError_fan_door() {
        return error_fan_door;
    }

    public void setError_fan_door(boolean error_fan_door) {
        this.error_fan_door = error_fan_door;
    }

    public boolean isError_rc_fan() {
        return error_rc_fan;
    }

    public void setError_rc_fan(boolean error_rc_fan) {
        this.error_rc_fan = error_rc_fan;
    }

    public boolean isError_cc_fan() {
        return error_cc_fan;
    }

    public void setError_cc_fan(boolean error_cc_fan) {
        this.error_cc_fan = error_cc_fan;
    }

    public boolean isError_fc_fan() {
        return error_fc_fan;
    }

    public void setError_fc_fan(boolean error_fc_fan) {
        this.error_fc_fan = error_fc_fan;
    }

    public boolean isRc_door_open_alarm() {
        return rc_door_open_alarm;
    }

    public void setRc_door_open_alarm(boolean rc_door_open_alarm) {
        this.rc_door_open_alarm = rc_door_open_alarm;
    }

    public boolean isCc_door_open_alarm() {
        return cc_door_open_alarm;
    }

    public void setCc_door_open_alarm(boolean cc_door_open_alarm) {
        this.cc_door_open_alarm = cc_door_open_alarm;
    }

    public boolean isFc_door_open_alarm() {
        return fc_door_open_alarm;
    }

    public void setFc_door_open_alarm(boolean fc_door_open_alarm) {
        this.fc_door_open_alarm = fc_door_open_alarm;
    }

    public boolean isError_defrost() {
        return error_defrost;
    }

    public void setError_defrost(boolean error_defrost) {
        this.error_defrost = error_defrost;
    }

    public boolean isError_cc_defrost_sensor() {
        return error_cc_defrost_sensor;
    }

    public void setError_cc_defrost_sensor(boolean error_cc_defrost_sensor) {
        this.error_cc_defrost_sensor = error_cc_defrost_sensor;
    }

    public boolean isError_humidity_sensor() {
        return error_humidity_sensor;
    }

    public void setError_humidity_sensor(boolean error_humidity_sensor) {
        this.error_humidity_sensor = error_humidity_sensor;
    }

    public boolean isError_humidity_temp_sensor() {
        return error_humidity_temp_sensor;
    }

    public void setError_humidity_temp_sensor(boolean error_humidity_temp_sensor) {
        this.error_humidity_temp_sensor = error_humidity_temp_sensor;
    }

    public boolean isError_rc_cc_sensor() {
        return error_rc_cc_sensor;
    }

    public void setError_rc_cc_sensor(boolean error_rc_cc_sensor) {
        this.error_rc_cc_sensor = error_rc_cc_sensor;
    }
}