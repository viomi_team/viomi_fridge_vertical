package com.viomi.fridge.vertical.album.model;

import android.net.Uri;
import android.os.Environment;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.album.model.entity.AlbumEntity;
import com.viomi.fridge.vertical.common.util.logUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import rx.Observable;

/**
 * 电子相册读取实现
 * Created by William on 2018/1/20.
 */
public class AlbumRepository {
    private static final String TAG = AlbumRepository.class.getSimpleName();
    private static AlbumRepository mInstance;

    public static AlbumRepository getInstance() {
        if (mInstance == null) {
            synchronized (AlbumRepository.class) {
                if (mInstance == null) {
                    mInstance = new AlbumRepository();
                }
            }
        }
        return mInstance;
    }

    /**
     * 读取相册
     */
    public Observable<List<AlbumEntity>> getAlbum() {
        return Observable.create(subscriber -> {
            File dir = new File(Environment.getExternalStorageDirectory(), AppConstants.PATH + AppConstants.ALBUM_SAVE_PATH);// 相册路径
            if (!dir.exists()) { // 无接受过图片
                logUtil.e(TAG, "album directory is not exit");
                subscriber.onNext(null);
                subscriber.onCompleted();
                return;
            }
            File[] files = null;
            if (dir.isDirectory()) {
                files = dir.listFiles((dir1, name) -> name.endsWith(".jpg") || name.endsWith(".png") || name.endsWith(".jpeg") ||
                        name.endsWith(".JPG") || name.endsWith(".PNG") || name.endsWith(".JPEG"));
            }
            if (files == null || files.length == 0) { // 没有图片
                subscriber.onNext(null);
                subscriber.onCompleted();
                return;
            }
            List<AlbumEntity> list = new ArrayList<>();
            for (File file : files) {
                AlbumEntity entity = new AlbumEntity();
                entity.setFile(file);
                entity.setSelected(false);
                entity.setLastModified(file.lastModified());
                list.add(entity);
            }
            Collections.sort(list, (file1, file2) -> {
                if (file1.getLastModified() > file2.getLastModified()) {
                    return -1;
                } else {
                    return 1;
                }
            });
            subscriber.onNext(list);
            subscriber.onCompleted();
        });
    }

    /**
     * 首页 Banner 获取相册
     */
    public Observable<List<Uri>> getAlbumFromBanner() {
        return Observable.create(subscriber -> {
            File dir = new File(Environment.getExternalStorageDirectory(), AppConstants.PATH + AppConstants.ALBUM_SAVE_PATH);
            List<Uri> list = new ArrayList<>();
            if (dir.exists()) { // 目录存在
                List<File> files = Arrays.asList(dir.listFiles((dir1, name) -> name.endsWith(".jpg") || name.endsWith(".png") || name.endsWith(".jpeg") ||
                        name.endsWith(".JPG") || name.endsWith(".PNG") || name.endsWith(".JPEG")));
                Collections.sort(files, (file1, file2) -> {
                    if (file1.lastModified() > file2.lastModified()) {
                        return -1;
                    } else {
                        return 1;
                    }
                });
                int length = files.size() >= 4 ? 4 : files.size();
                for (int i = 0; i < length; i++) {
                    File file = files.get(i);
                    list.add(Uri.fromFile(file));
                }
            }
            if (list.size() == 0) { // 目录无相册
                list.add(Uri.parse("android.resource://" + FridgeApplication.getContext().getPackageName() + "/" + R.drawable.img_album_default_1));
                list.add(Uri.parse("android.resource://" + FridgeApplication.getContext().getPackageName() + "/" + R.drawable.img_album_default_2));
                list.add(Uri.parse("android.resource://" + FridgeApplication.getContext().getPackageName() + "/" + R.drawable.img_album_default_3));
                list.add(Uri.parse("android.resource://" + FridgeApplication.getContext().getPackageName() + "/" + R.drawable.img_album_default_4));
            }
            subscriber.onNext(list);
            subscriber.onCompleted();
        });
    }

    /**
     * 删除相册
     */
    public Observable<Boolean> deleteAlbum(List<AlbumEntity> list) {
        return Observable.create(subscriber -> {
            if (list != null && list.size() > 0) {
                Iterator<AlbumEntity> iterator = list.iterator();
                while (iterator.hasNext()) {
                    AlbumEntity entity = iterator.next();
                    if (entity.isSelected()) {
                        if (entity.getFile().exists() && entity.getFile().delete())
                            iterator.remove();
                    }
                }
                subscriber.onNext(true);
                subscriber.onCompleted();
            } else {
                subscriber.onNext(false);
                subscriber.onCompleted();
            }
        });
    }
}
