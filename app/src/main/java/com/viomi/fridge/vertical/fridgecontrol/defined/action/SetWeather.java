package com.viomi.fridge.vertical.fridgecontrol.defined.action;

import com.viomi.fridge.vertical.fridgecontrol.defined.ViomiDefined;
import com.viomi.fridge.vertical.fridgecontrol.defined.property.Weather;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetWeather extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setWeather.toActionType();

    public SetWeather() {
        super(TYPE);

        super.addArgument(Weather.TYPE.toString());
    }
}