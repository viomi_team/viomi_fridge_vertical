package com.viomi.fridge.vertical.common.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PixelFormat;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.tencent.smtt.sdk.QbSdk;
import com.tencent.smtt.sdk.TbsListener;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.model.preference.ManagePreference;
import com.viomi.fridge.vertical.administration.model.repository.ManageRepository;
import com.viomi.fridge.vertical.album.activity.ScreenSaverActivity;
import com.viomi.fridge.vertical.album.model.entity.AlbumEntity;
import com.viomi.fridge.vertical.album.model.preference.AlbumPreference;
import com.viomi.fridge.vertical.common.http.ApiClient;
import com.viomi.fridge.vertical.common.http.ProgressListener;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.StatisticsUtil;
import com.viomi.fridge.vertical.common.util.ToastUtil;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.foodmanage.model.FoodManageRepository;
import com.viomi.fridge.vertical.foodmanage.model.entity.FoodDetail;
import com.viomi.fridge.vertical.fridgecontrol.activity.OpenAlarmActivity;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;
import com.viomi.fridge.vertical.fridgecontrol.model.repository.MiotRepository;
import com.viomi.fridge.vertical.fridgecontrol.model.serial.manager.SerialManager;
import com.viomi.fridge.vertical.home.activity.WarnAlarmActivity;
import com.viomi.fridge.vertical.iot.activity.LockVideoCallActivity;
import com.viomi.fridge.vertical.message.entity.DeviceInfoMessage;
import com.viomi.fridge.vertical.message.entity.PushMsg;
import com.viomi.fridge.vertical.message.manager.InfoManager;
import com.viomi.fridge.vertical.message.manager.PushManager;
import com.viomi.fridge.vertical.message.view.activity.MessageCenterActivity;
import com.viomi.fridge.vertical.speech.model.preference.SpeechPreference;
import com.viomi.widget.dialog.BaseAlertDialog;
import com.viomi.widget.dialog.DownLoadDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import cn.com.viomi.ailib.api.VioAI;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * 后台处理 Service
 * Created by William on 2017/12/29.
 */
public class BackgroundService extends Service {
    private static final String TAG = BackgroundService.class.getSimpleName();
    private WindowManager mWindowManager;
    private ServiceHandler mHandler;
    private CompositeSubscription mCompositeSubscription;// 统一管理订阅
    private Subscription mFoodSubscription;// 食材管理消息订阅
    private Subscription mTimeSubscription;// 1 分钟倒计时
    private Subscription mScreenSaverSubscription;// 屏保计时
    private boolean mIsShowing;// 消息对话框是否在显示
    private boolean mIsDownloading = false;// 是否正在下载
    private List<FoodDetail> mList;// 食材信息集合
    private View mMessageDialog;// 消息对话框
    private TextView mMessageTextView;// 消息内容
    private final static int WHAT_SHOW_MESSAGE_DIALOG = 0;// 显示消息对话框
    private final static int WHAT_CANCEL_MESSAGE_DIALOG = 1;// 隐藏消息对话框
    private final static int WHAT_DOWNLOAD_APP = 2;// 推送升级
    private final static int WHAT_NETWORK_CHANGE = 3;// 网络变化
    private final static int WHAT_SET_TIME = 4;// 时间设置
    private final static int WHAT_REFRESH_TIME = 5;// 刷新时间
    private int mMinute = 0;// 分钟计时
    private boolean mIsPauseScreenSaver = false;
    private int mHeartBeatCount = 1;// 心跳计时
    private Timer mTimer2;
    private TimerTask mTimeTask2;

    @Override
    public void onCreate() {
        super.onCreate();
        logUtil.d(TAG, "onCreate");
        mWindowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        mHandler = new ServiceHandler(this);
        mList = new ArrayList<>();
        mCompositeSubscription = new CompositeSubscription();
        x5KernelDownload();// X5 内核下载
        SerialManager.getInstance().open();// 打开串口
        screenSaverStart();// 屏保计时
        deleteApk();// 删除安装包
        if (!FridgePreference.getInstance().getModel().equals(AppConstants.MODEL_JD))
            checkUpdate(false);// 检查 app 更新
        loadFoodData();// 读取食材信息
        initMessageDialog();// 消息对话框初始化
        initSubscription();// RxBus 初始化
        registerReceiver();// 广播注册
        initTimer();// 计时任务
        openUploadTimer();
        PushManager.getInstance().init();// 友盟推送初始化
        heartbeat();// 心跳包
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) { // 监听网络变化
            netWorkChange();
        }
        setHumanSensorState();// 重新设置人感
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        logUtil.d(TAG, "onStartCommand");
        return START_REDELIVER_INTENT;
    }

    @Override
    public IBinder onBind(Intent intent) {
        logUtil.d(TAG, "onBind");
        return null;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        logUtil.e(TAG, "onUnbind");
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        logUtil.e(TAG, "onDestroy");
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
        SerialManager.getInstance().close();// 关闭串口
        PushManager.getInstance().close();// 友盟推送关闭
        unregisterReceiver(mReceiver);// 注销广播
        super.onDestroy();
    }

    @SuppressLint("InflateParams")
    private void initMessageDialog() { // 消息弹窗初始化
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater == null) return;
        mMessageDialog = inflater.inflate(R.layout.dialog_message_notification, null);
        mMessageTextView = mMessageDialog.findViewById(R.id.message_notification_text);
        mMessageDialog.setOnClickListener(view -> {
            if (mHandler == null) return;
            mHandler.sendEmptyMessage(WHAT_CANCEL_MESSAGE_DIALOG);
            Intent intent = new Intent(getApplicationContext(), MessageCenterActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        });
    }

    /**
     * X5 内核下载
     */
    private void x5KernelDownload() {
        if (!ManagePreference.getInstance().getX5KernelState())
            QbSdk.reset(FridgeApplication.getContext());
        // 搜集本地 tbs 内核信息并上报服务器，服务器返回结果决定使用哪个内核。
        QbSdk.PreInitCallback cb = new QbSdk.PreInitCallback() {

            @Override
            public void onViewInitFinished(boolean arg0) {
                // x5 內核初始化完成的回调，为 true 表示 x5 内核加载成功，否则表示 x5 内核加载失败，会自动切换到系统内核。
                logUtil.d(TAG, " onViewInitFinished is " + arg0);
                ManagePreference.getInstance().saveX5KernelState(arg0);
            }

            @Override
            public void onCoreInitFinished() {

            }
        };
        //x5 内核初始化接口
        QbSdk.initX5Environment(getApplicationContext(), cb);
        QbSdk.setTbsListener(new TbsListener() {
            @Override
            public void onDownloadFinish(int i) {
                logUtil.d(TAG, " onDownloadFinish");
            }

            @Override
            public void onInstallFinish(int i) {
                logUtil.d(TAG, " onInstallFinish");
            }

            @Override
            public void onDownloadProgress(int i) {
                logUtil.d(TAG, " onDownloadProgress：" + i);
            }
        });
    }

    /**
     * 网络变化监听（适用 Android N）
     */
    private void netWorkChange() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) return;
        connectivityManager.requestNetwork(new NetworkRequest.Builder().build(), new ConnectivityManager.NetworkCallback() {
            @Override
            public void onAvailable(Network network) {
                super.onAvailable(network);
                if (mHandler != null)
                    Message.obtain(mHandler, WHAT_NETWORK_CHANGE).sendToTarget();
            }
        });
    }

    /**
     * 根据缓存重新设置人感
     */
    private void setHumanSensorState() {
        boolean enable = ManagePreference.getInstance().getHumanSensorState();
        ToolUtil.setHumanSensorState(enable);
    }

    /**
     * 每分钟计时器
     */
    private void initTimer() {
        Subscription timerSubscription = Observable.interval(0, 60, TimeUnit.SECONDS)
                .onBackpressureDrop()
                .compose(RxSchedulerUtil.SchedulersTransformer5())
                .onTerminateDetach()
                .subscribe(aLong -> {
                    logUtil.d(TAG, "MSG_TIME_MINUTE+++");
                    RxBus.getInstance().post(BusEvent.MSG_TIME_MINUTE);
                }, throwable -> logUtil.e(TAG, "MSG_TIME_MINUTE error! " + throwable.getMessage()));
        mCompositeSubscription.add(timerSubscription);
    }

    /**
     * 心跳
     */
    private void heartbeat() {
        Subscription heartbeatSubscription = Observable.interval(0, 10, TimeUnit.SECONDS)
                .onBackpressureDrop()
                .compose(RxSchedulerUtil.SchedulersTransformer5())
                .onTerminateDetach()
                .subscribe(aLong -> {
                    mHeartBeatCount++;
                    logUtil.d(TAG, "heartbeat=" + mHeartBeatCount);
                    Intent intent = new Intent();
                    intent.setAction("com.unilife.fridge.action.dameon.heartbeat");
                    intent.putExtra("package", "com.viomi.fridge.vertical");
                    intent.putExtra("beats", mHeartBeatCount);
                    sendBroadcast(intent);
                }, throwable -> logUtil.e(TAG, "heartbeat error! " + throwable.getMessage()));
        mCompositeSubscription.add(heartbeatSubscription);
    }

    /**
     * RxBus 消息管理
     */
    private void initSubscription() {
        Subscription subscription = RxBus.getInstance()
                .subscribe(event -> {
                    Intent intent;
                    logUtil.d(TAG, "event.getMsgId:" + event.getMsgId());
                    switch (event.getMsgId()) {
                        case BusEvent.MSG_PUSH_MESSAGE: // 消息推送
                            PushMsg pushMsg = (PushMsg) event.getMsgObject();
                            if (pushMsg != null && pushMsg.type != null) {
                                if (PushManager.getInstance().isInAcceptTime()) {
                                    switch (pushMsg.type) {
                                        case PushMsg.PUSH_MESSAGE_TYPE_ABNORMAL: // 设备异常消息
                                        case PushMsg.PUSH_MESSAGE_TYPE_FILTER: // 滤芯消息
                                        case PushMsg.PUSH_MESSAGE_TYPE_FOOD: // 食材管理消息
                                            if (PushManager.getInstance().isDevicePushEnable()) {
                                                if (mHandler != null) {
                                                    Message.obtain(mHandler, WHAT_SHOW_MESSAGE_DIALOG, pushMsg).sendToTarget();
                                                }
                                            }
                                            break;
                                        case PushMsg.PUSH_MESSAGE_TYPE_ACTIVITY: // 活动
                                            if (PushManager.getInstance().isActivityPushEnable()) {
                                                if (mHandler != null) {
                                                    Message.obtain(mHandler, WHAT_SHOW_MESSAGE_DIALOG, pushMsg).sendToTarget();
                                                }
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                }
                                if (pushMsg.type.equals(PushMsg.PUSH_MESSAGE_TYPE_UPDATE)) { // 推送升级
                                    if (pushMsg.link == null || (!pushMsg.link.startsWith("http"))) {
                                        logUtil.e(TAG, "update pushMsg link error!pushMsg.link=" + pushMsg.link);
                                        return;
                                    }
                                    if (mHandler != null) {
                                        Message.obtain(mHandler, WHAT_DOWNLOAD_APP, pushMsg).sendToTarget();
                                    }
                                }
                                if (pushMsg.type.equals(PushMsg.PUSH_MESSAGE_TYPE_WULIAN)) { // 物联
                                    //{"type":"wulian""data":{"did":"2343c","event":"bell","parameter":{"camId":"cmicxxxx";}}
                                    logUtil.e(TAG, "wulian pushMsg:" + pushMsg.toString());
                                    try {
                                        Intent intent1 = new Intent(getApplicationContext(), LockVideoCallActivity.class);
                                        intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        if ("bell".equals(new JSONObject(pushMsg.custom).optString("event"))) {
                                            intent1.putExtra("info", pushMsg.custom);
                                            startActivity(intent1);
                                        } else if ("bell".equals(new JSONObject(pushMsg.data).optString("event"))) {
                                            intent1.putExtra("info", pushMsg.data);
                                            startActivity(intent1);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            break;
                        case BusEvent.MSG_OPEN_MESSAGE_CENTER: // 打开消息中心
                            mHandler.sendEmptyMessage(WHAT_CANCEL_MESSAGE_DIALOG);
                            break;
                        case BusEvent.MSG_WARN_ALARM: // 报警
                            int type = (int) event.getMsgObject();
                            intent = new Intent(getApplicationContext(), WarnAlarmActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("type", type);
                            startActivity(intent);
                            break;
                        case BusEvent.LOCK_RING: // 门铃
                            logUtil.d("BackgroundService", "LOCK_RING  LockVideoCallActivity");
                            intent = new Intent(getApplicationContext(), LockVideoCallActivity.class);
                            intent.putExtra("info", (String) event.getMsgObject());
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            break;
                        case BusEvent.MSG_START_FOOD_UPDATE: // 食材信息更新
                            loadFoodData();
                            break;
                        case BusEvent.MSG_OPEN_ALARM_DISPLAY: // 冰箱门开报警
                            if (!ToolUtil.isActivityRunning(FridgeApplication.getContext(),
                                    "com.viomi.fridge.vertical.fridgecontrol.activity.OpenAlarmActivity")) {
                                countdown();
                            }
                            break;
                        case BusEvent.MSG_OPEN_ALARM_DISMISS: // 冰箱门开报警复位
                            if (mTimeSubscription != null) {
                                mTimeSubscription.unsubscribe();
                                mTimeSubscription = null;
                            }
                            break;
                        case BusEvent.MSG_SCREEN_SAVER_UPDATE: // 屏保
                            screenSaverStart();
                            break;
                        case BusEvent.MSG_STOP_SCREEN_TIMER: // 暂停屏保计时
                            mIsPauseScreenSaver = true;
                            break;
                        case BusEvent.MSG_START_SCREEN_TIMER: // 开始屏保计时
                            mIsPauseScreenSaver = false;
                            RxBus.getInstance().post(BusEvent.MSG_SCREEN_SAVER_UPDATE);// 重置屏保时间
                            break;
                        case BusEvent.MSG_DELETE_CAMERA_CACHE: // 删除拍照缓存
                            deleteCameraCache();
                            break;
                        case BusEvent.MSG_TIME_MINUTE: // 每分钟监听
                            mMinute++;
                            loadFoodData();// 读取食材信息
                            // 计算滤芯寿命和启动时间
                            if (mMinute >= 60) {
                                mMinute = 0;
                                FridgePreference.getInstance().saveUsedFilterLife(FridgePreference.getInstance().getUsedFilterLife() + 1);
                                FridgePreference.getInstance().saveStartHours(FridgePreference.getInstance().getStartHours() + 1);
                                // 滤芯即将过期
                                if (MiotRepository.getInstance().FILTER_LIFE_BASE - FridgePreference.getInstance().getUsedFilterLife() <= 336) {
                                    if (!FridgePreference.getInstance().getFilterLowFlag()) {
                                        MiotRepository.getInstance().sendFilterLifeLow();
                                        FridgePreference.getInstance().setFilterLowFlag(true);
                                        PushMsg pushMsg1 = new PushMsg();
                                        pushMsg1.title = FridgeApplication.getContext().getString(R.string.message_filter_warn);
                                        pushMsg1.content = FridgeApplication.getContext().getString(R.string.message_filter_low);
                                        pushMsg1.type = PushMsg.PUSH_MESSAGE_TYPE_FILTER;
                                        PushManager.getInstance().pushNotificationToStatusBar(pushMsg1);

                                        DeviceInfoMessage deviceInfoMessage = new DeviceInfoMessage();
                                        deviceInfoMessage.setTitle(FridgeApplication.getContext().getString(R.string.message_filter_warn));
                                        deviceInfoMessage.setContent(FridgeApplication.getContext().getString(R.string.message_filter_low));
                                        deviceInfoMessage.setInfoId(InfoManager.getInstance().getLastDeviceInfoId() + 1);
                                        deviceInfoMessage.setTopic(PushMsg.PUSH_MESSAGE_TYPE_FILTER);
                                        deviceInfoMessage.setTime(System.currentTimeMillis());
                                        deviceInfoMessage.setDelete(false);
                                        deviceInfoMessage.setRead(false);
                                        InfoManager.getInstance().addDeviceInfoRecord(deviceInfoMessage);
                                    }
                                }
                                // 滤芯过期
                                if (MiotRepository.getInstance().FILTER_LIFE_BASE - FridgePreference.getInstance().getUsedFilterLife() <= 0) {
                                    if (!FridgePreference.getInstance().getFilterOutOfDateFlag()) {
                                        MiotRepository.getInstance().sendFilterLifeEnd();
                                        FridgePreference.getInstance().setFilterOutOfDateFlag(true);
                                        PushMsg pushMsg2 = new PushMsg();
                                        pushMsg2.title = FridgeApplication.getContext().getString(R.string.message_filter_warn);
                                        pushMsg2.content = FridgeApplication.getContext().getString(R.string.message_filter_out_of_date);
                                        pushMsg2.type = PushMsg.PUSH_MESSAGE_TYPE_FILTER;
                                        PushManager.getInstance().pushNotificationToStatusBar(pushMsg2);

                                        DeviceInfoMessage deviceInfoMessage = new DeviceInfoMessage();
                                        deviceInfoMessage.setTitle(FridgeApplication.getContext().getString(R.string.message_filter_warn));
                                        deviceInfoMessage.setContent(FridgeApplication.getContext().getString(R.string.message_filter_out_of_date));
                                        deviceInfoMessage.setInfoId(InfoManager.getInstance().getLastDeviceInfoId() + 1);
                                        deviceInfoMessage.setTopic(PushMsg.PUSH_MESSAGE_TYPE_FILTER);
                                        deviceInfoMessage.setTime(System.currentTimeMillis());
                                        deviceInfoMessage.setDelete(false);
                                        deviceInfoMessage.setRead(false);
                                        InfoManager.getInstance().addDeviceInfoRecord(deviceInfoMessage);
                                    }
                                }
                            }
                            // 每天整点自动检查 App 更新
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HHmm", Locale.getDefault());
                            String timeStr = simpleDateFormat.format(new Date());
                            int hour = Integer.parseInt(timeStr);
                            if (hour == 0) {
                                if (!FridgePreference.getInstance().getModel().equals(AppConstants.MODEL_JD))
                                    checkUpdate(true);
                                x5KernelDownload();
                            }
                            break;
                    }
                });
        mCompositeSubscription.add(subscription);
    }

    /**
     * 读取食材信息
     */
    private void loadFoodData() {
        if (mFoodSubscription != null) {
            mFoodSubscription.unsubscribe();
            mFoodSubscription = null;
        }
        mFoodSubscription = FoodManageRepository.getInstance().loadFood()
                .compose(RxSchedulerUtil.SchedulersTransformer3())
                .onTerminateDetach()
                .subscribe(foodDetails -> {
                    if (foodDetails == null) foodDetails = new ArrayList<>();
                    mList.clear();
                    mList.addAll(foodDetails);
                    calculate(mList);// 计算过期食材
                    RxBus.getInstance().post(BusEvent.MSG_FOOD_UPDATE, mList);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(mFoodSubscription);
    }

    /**
     * 检查 App 更新
     */
    private void checkUpdate(boolean isAuto) {
        String packageName = ManagePreference.getInstance().getDebug() ? getPackageName() + ".debug" : getPackageName();
        String model = FridgePreference.getInstance().getModel();// 读取冰箱 Model
        String channel = AppConstants.CHANNEL_VIOMI;
        if (AppConstants.MODEL_JD.equals(model)) {
            channel = AppConstants.CHANNEL_JINGDONG;
        }
        Subscription subscription = ApiClient.getInstance().getApiService().checkAppUpdate(packageName, channel)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(appUpdateResult -> {
                    if (appUpdateResult.getData().get(0).getCode() > ToolUtil.getVersionCode(this)) {
                        ManagePreference.getInstance().saveAppUpdate(true);
                        if (isAuto) downLoadApk(appUpdateResult.getData().get(0).getUrl());
                    } else ManagePreference.getInstance().saveAppUpdate(false);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    /**
     * 下载安装包
     */
    private void downLoadApk(String url) {
        logUtil.d(TAG, "downLoadApk =" + url);
        if (mIsDownloading) return;
        mIsDownloading = true;
        logUtil.d(TAG, "startDownload");
        // 更新对话框
        DownLoadDialog downLoadDialog = new DownLoadDialog(getApplicationContext(), getApplicationContext().getResources().getString(R.string.update_app),
                getApplicationContext().getResources().getString(R.string.update_cancel), getApplicationContext().getResources().getString(R.string.management_update_finish));
        // 退出升级对话框
        BaseAlertDialog alertDialog = new BaseAlertDialog(getApplicationContext(), getApplicationContext().getResources().getString(R.string.management_update_quit),
                getApplicationContext().getResources().getString(R.string.management_update_quit_confirm), getApplicationContext().getResources().getString(R.string.management_update_quit_cancel));

        ProgressListener progressListener = (currentBytes, contentLength, isFinish) -> {
            logUtil.d(TAG, "download:" + currentBytes + "");
            if (downLoadDialog.isShowing())
                downLoadDialog.setProgressBar(currentBytes, contentLength);
        };
        if (downLoadDialog.getWindow() != null) {
            downLoadDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            downLoadDialog.show();
        }

        ApiClient.getInstance().setProgressListener(progressListener);
        Subscription subscription = ApiClient.getInstance().getApiService().download(url)
                .subscribeOn(Schedulers.newThread())
                .onTerminateDetach()
                .flatMap(responseBody -> ManageRepository.getInstance().saveApk(url, responseBody))
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(s -> {
                    mIsDownloading = false;
                    if (s == null) {
                        ToastUtil.showCenter(getApplicationContext(), getApplicationContext().getResources().getString(R.string.management_update_fail_1));
                        ApiClient.getInstance().setProgressListener(null);
                        if (alertDialog.isShowing()) alertDialog.dismiss();
                        if (downLoadDialog.isShowing()) downLoadDialog.dismiss();
                    } else {
                        // 开始静默安装
                        logUtil.d(TAG, "SILENCE_INSTALL!");
                        Intent mIntent = new Intent();
                        mIntent.setAction("android.intent.action.SILENCE_INSTALL");
                        mIntent.putExtra("apkPath", s);
                        sendBroadcast(mIntent);
                    }
                }, throwable -> {
                    mIsDownloading = false;
                    ToastUtil.showCenter(getApplicationContext(), getApplicationContext().getResources().getString(R.string.management_update_fail_1));
                    ApiClient.getInstance().setProgressListener(null);
                    if (alertDialog.isShowing()) alertDialog.dismiss();
                    if (downLoadDialog.isShowing()) downLoadDialog.dismiss();
                    logUtil.e(TAG, throwable.getMessage());
                });
        mCompositeSubscription.add(subscription);

        alertDialog.setOnLeftClickListener(() -> {
            alertDialog.dismiss();
            downLoadDialog.dismiss();
            // 取消下载
            subscription.unsubscribe();
            mIsDownloading = false;
            ApiClient.getInstance().setProgressListener(null);
        });
        alertDialog.setOnRightClickListener(alertDialog::dismiss);
        downLoadDialog.setOnCancelClickListener(() -> {
            if (alertDialog.getWindow() != null) {
                alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                alertDialog.show();
            }
        });
    }

    /**
     * 删除安装包
     */
    private void deleteApk() {
        Subscription subscription = Observable.just(true)
                .compose(RxSchedulerUtil.SchedulersTransformer2())
                .onTerminateDetach()
                .subscribe(aBoolean -> {
                    File dir = new File(Environment.getExternalStorageDirectory(), AppConstants.PATH);// Apk 路径
                    if (!dir.exists()) return;
                    File[] files = null;
                    if (dir.isDirectory())
                        files = dir.listFiles((dir1, name) -> name.endsWith(".apk"));
                    if (files == null || files.length == 0) return;
                    for (File file : files) {
                        if (file.exists()) logUtil.d(TAG, file.delete() + "");
                    }
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    /**
     * 屏保计时
     */
    public void screenSaverStart() {
        if (mScreenSaverSubscription != null) {
            mScreenSaverSubscription.unsubscribe();
            mScreenSaverSubscription = null;
        }
        boolean enable = AlbumPreference.getInstance().getSwitch();// 读取屏保开关
        if (!enable) return;
        // 屏保时间
        String time = AlbumPreference.getInstance().getTime();
        boolean isMin = time.contains(FridgeApplication.getContext().getResources().getString(R.string.timer_minute));// 是否分钟计时
        int t;
        if (isMin)
            t = Integer.valueOf(AlbumPreference.getInstance().getTime().replace(FridgeApplication.getContext().getResources().getString(R.string.album_minute), ""));
        else
            t = Integer.valueOf(AlbumPreference.getInstance().getTime().replace(FridgeApplication.getContext().getResources().getString(R.string.album_second), ""));
        logUtil.d(TAG, "screenSaverStart");
        mScreenSaverSubscription = Observable.interval(0, 1, TimeUnit.SECONDS)
                .onBackpressureDrop()
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(aLong -> {
                    logUtil.d(TAG, "time:" + aLong);
                    if (isMin) { // 分钟计时
                        if ((t * 60) == aLong) {
                            showScreenSaver();
                        }
                    } else if (t == aLong) { // 秒计时
                        showScreenSaver();
                    }
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(mScreenSaverSubscription);
    }

    /**
     * 屏保跳转
     */
    private void showScreenSaver() {
        mScreenSaverSubscription.unsubscribe();
        mScreenSaverSubscription = null;
        if (mIsPauseScreenSaver) return;
        // 语音对话框运行
        if (ToolUtil.isServiceWork(FridgeApplication.getContext(), "com.viomi.fridge.vertical.speech.service.SpeechService"))
            return;
        // App 是否在前台运行
        if (ToolUtil.getAppStatus(FridgeApplication.getContext(), "com.viomi.fridge.vertical") != 1)
            return;
        List<AlbumEntity> list = AlbumPreference.getInstance().getAlbumEntityList();
        if (list == null || list.size() == 0) return;
        Iterator<AlbumEntity> iterator = list.iterator();
        while (iterator.hasNext()) {
            AlbumEntity entity = iterator.next();
            if (!entity.getFile().exists()) {
                iterator.remove();
            }
        }
        logUtil.d(TAG, list.size() + "");
        if (list.size() == 0) return;
        Intent intent = new Intent(getApplicationContext(), ScreenSaverActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    /**
     * 注册广播
     */
    private void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);// 网络变化监听
        }
        intentFilter.addAction("com.action.audiocall.callstatus.result");// 蓝牙电话广播
        registerReceiver(mReceiver, intentFilter);
    }

    private WindowManager.LayoutParams getWindowLayoutParams(int gravity, int width, int height) {
        WindowManager.LayoutParams wmParams = new WindowManager.LayoutParams();
        wmParams.format = PixelFormat.TRANSPARENT;
        wmParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM;
        wmParams.x = 0;
        wmParams.y = 0;
        wmParams.gravity = gravity;
        wmParams.width = width;
        wmParams.height = height;
        wmParams.type = WindowManager.LayoutParams.TYPE_PHONE;
        return wmParams;
    }

    /**
     * 计算过期食材
     */
    private void calculate(List<FoodDetail> list) {
        Subscription subscription = FoodManageRepository.getInstance().filterOutDate(list)
                .compose(RxSchedulerUtil.SchedulersTransformer6())
                .onTerminateDetach()
                .subscribe(list1 -> FoodManageRepository.getInstance().setPushFlag(list1),
                        throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    /**
     * 1 分钟倒计时（门开报警）
     */
    private void countdown() {
        if (mTimeSubscription != null && !mTimeSubscription.isUnsubscribed()) return;
        logUtil.d(TAG, "countdown");
        mTimeSubscription = Observable.timer(60, TimeUnit.SECONDS)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(aLong -> {
                    Intent intent = new Intent(getApplicationContext(), OpenAlarmActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    if (mTimeSubscription != null) {
                        mTimeSubscription.unsubscribe();
                        mTimeSubscription = null;
                    }
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(mTimeSubscription);
    }

    /**
     * 删除拍照缓存
     */
    private void deleteCameraCache() {
        Subscription subscription = FoodManageRepository.getInstance().deleteCache()
                .compose(RxSchedulerUtil.SchedulersTransformer2())
                .onTerminateDetach()
                .subscribe(aBoolean -> logUtil.d(TAG, "delete cache result:" + aBoolean),
                        throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    private void openUploadTimer() {
        if (mTimer2 != null) {
            mTimer2.cancel();
            mTimer2 = null;
        }
        if (mTimeTask2 != null) {
            mTimeTask2.cancel();
            mTimeTask2 = null;
        }
        mTimer2 = new Timer();
        mTimeTask2 = new TimerTask() {
            @Override
            public void run() {
                String Path = StatisticsUtil.LOG_FILE_PATH;
                File file = new File(Path);
                if (file.exists() && file.length() > 5 * 1048576) {//文件存在，大小大于5M进行上传
                    //TODO LZW
                    //OkHttpUtils.getInstance().uploadFile(StatisticsUtil.UPLOAD_EVENT_FILE, file, new CallBack() {
                    //    @Override
                    //    public void onSuccess() {
                    //        FileUtil.deleteChildFile(file);
                    //    }
                    //
                    //    @Override
                    //    public void onFailure(String tips, String exception) {
                    //        Log.e(TAG,"uploadFile fail!msg="+exception);
                    //    }
                    //});
                }
            }
        };
        mTimer2.schedule(mTimeTask2, 1000, 1000 * 60 * 30);//每30分钟执行一次
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() != null) {
                logUtil.d(TAG, intent.getAction());
                switch (intent.getAction()) {
                    case Intent.ACTION_TIME_TICK: // 系统分钟广播
                        RxBus.getInstance().post(BusEvent.MSG_TIME_MINUTE);
                        break;
                    case "com.action.audiocall.callstatus.result": // 蓝牙电话广播
                        if (intent.getExtras() == null) return;
                        String result = intent.getExtras().getString("callstatus", "");
                        if (result.equals("true")) { // 接听
                            logUtil.d(TAG, "phone answer");
                            if (SpeechPreference.getInstance().isAuth()) VioAI.disableWakeup();
                        } else if (result.equals("false")) { // 挂断
                            logUtil.d(TAG, "phone hang up");
                            if (SpeechPreference.getInstance().isAuth()) VioAI.enableWakeup();
                        }
                        break;
                    case ConnectivityManager.CONNECTIVITY_ACTION: // 网络变化
                        if (mHandler != null)
                            Message.obtain(mHandler, WHAT_NETWORK_CHANGE).sendToTarget();
                        break;
                }
            }
        }
    };

    private static class ServiceHandler extends Handler {
        WeakReference<BackgroundService> weakReference;

        ServiceHandler(BackgroundService service) {
            this.weakReference = new WeakReference<>(service);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (this.weakReference != null) {
                BackgroundService service = this.weakReference.get();
                if (service != null) {
                    switch (msg.what) {
                        case WHAT_SHOW_MESSAGE_DIALOG: // 显示新消息弹窗
                            PushMsg pushMsg = (PushMsg) msg.obj;
                            if (!service.mIsShowing &&
                                    !ToolUtil.isActivityRunning(FridgeApplication.getContext(), "com.viomi.fridge.vertical.message.view.activity.MessageCenterActivity")) {
                                service.mWindowManager.addView(service.mMessageDialog,
                                        service.getWindowLayoutParams(Gravity.TOP,
                                                WindowManager.LayoutParams.MATCH_PARENT,
                                                WindowManager.LayoutParams.WRAP_CONTENT));
                                if (pushMsg != null && pushMsg.title != null && service.mMessageTextView != null) {
                                    service.mMessageTextView.setText(pushMsg.title);
                                }
                                service.mIsShowing = true;
                                // 显示 10 秒
                                if (service.mHandler == null) return;
                                service.mHandler.sendEmptyMessageDelayed(WHAT_CANCEL_MESSAGE_DIALOG,
                                        10000);
                            }
                            break;
                        case WHAT_CANCEL_MESSAGE_DIALOG: // 隐藏新消息弹窗
                            if (service.mIsShowing) {
                                service.mWindowManager.removeView(service.mMessageDialog);
                                service.mIsShowing = false;
                            }
                            break;
                        case WHAT_DOWNLOAD_APP: // 推送升级
                            PushMsg pushMsg1 = (PushMsg) msg.obj;
                            service.downLoadApk(pushMsg1.link);
                            break;
                        case WHAT_NETWORK_CHANGE: // 网络变化（修正联网后时间不准问题）
                            ConnectivityManager connectivityManager = (ConnectivityManager) FridgeApplication.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                            if (connectivityManager == null) return;
                            NetworkInfo wifiNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                            if (!wifiNetInfo.isConnected()) return;
                            Observable.just(true)
                                    .compose(RxSchedulerUtil.SchedulersTransformer2())
                                    .subscribe(aBoolean -> {
                                        URL url;
                                        try {
                                            url = new URL("http://www.baidu.com");
                                            URLConnection connection = url.openConnection();
                                            connection.connect();
                                            long time = connection.getDate();// 网络时间
                                            if (service.mHandler != null) {
                                                Message.obtain(service.mHandler, WHAT_SET_TIME, time).sendToTarget();
                                            }
                                        } catch (IOException e) {
                                            logUtil.e(TAG, e.getMessage());
                                            e.printStackTrace();
                                        }
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            break;
                        case WHAT_SET_TIME: // 设置时间
                            long time = (long) msg.obj;
                            logUtil.d(TAG, "network time：" + time);
                            logUtil.d(TAG, "system time：" + System.currentTimeMillis());
                            if (System.currentTimeMillis() >= time) {
                                if (service.mHandler != null)
                                    service.mHandler.sendEmptyMessage(WHAT_REFRESH_TIME);
                                return;
                            }
                            Observable.just(time)
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(aLong -> {
                                        Intent intent = new Intent();
                                        intent.setAction("android.intent.action.USER_SET_TIME");
                                        intent.putExtra("time", time);
                                        service.sendBroadcast(intent);
                                        if (service.mHandler != null) {
                                            service.mHandler.sendEmptyMessageDelayed(WHAT_REFRESH_TIME, 5000);
                                        }
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            break;
                        case WHAT_REFRESH_TIME:
                            RxBus.getInstance().post(BusEvent.MSG_TIME_MINUTE);
                            break;
                    }
                }
            }
        }
    }
}
