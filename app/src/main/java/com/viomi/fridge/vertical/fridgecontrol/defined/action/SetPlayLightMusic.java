package com.viomi.fridge.vertical.fridgecontrol.defined.action;

import com.viomi.fridge.vertical.fridgecontrol.defined.ViomiDefined;
import com.viomi.fridge.vertical.fridgecontrol.defined.property.PlayLightMusic;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetPlayLightMusic extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setPlayLightMusic.toActionType();

    public SetPlayLightMusic() {
        super(TYPE);

        super.addArgument(PlayLightMusic.TYPE.toString());
    }
}