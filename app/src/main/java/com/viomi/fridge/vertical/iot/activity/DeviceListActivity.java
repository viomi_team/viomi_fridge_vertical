package com.viomi.fridge.vertical.iot.activity;


import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.miot.common.abstractdevice.AbstractDevice;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseHandlerActivity;
import com.viomi.fridge.vertical.common.base.RecyclerViewLinearDivider;
import com.viomi.fridge.vertical.common.util.ToastUtil;
import com.viomi.fridge.vertical.iot.contract.IotContract;
import com.viomi.fridge.vertical.iot.model.http.entity.DeviceInfo;
import com.viomi.fridge.vertical.iot.view.adapter.DeviceAdapter;
import com.viomi.fridge.vertical.iot.view.dialog.HeatKettleDialog;
import com.viomi.fridge.vertical.iot.view.dialog.PLMachineDialog;
import com.viomi.fridge.vertical.iot.view.dialog.RangeHoodDialog;
import com.viomi.fridge.vertical.iot.view.dialog.WaterPurifierDialog;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 互联网家设备列表 Activity
 * Created by William on 2018/2/3.
 */

public class DeviceListActivity extends BaseHandlerActivity implements IotContract.View, AdapterView.OnItemClickListener {
    private static final String TAG = DeviceListActivity.class.getSimpleName();
    private List<DeviceInfo> mList = new ArrayList<>();
    private DeviceAdapter mAdapter;
    //    private List<CameraInfo> mCameraList;// 绿联摄像头设备集合
    private boolean isIPCLogin = false, isEntering = true;// IPC 登录标志
    private int mCount = 0;// IPC 登录失败重试次数
    public static final int IPC_LOGIN_SUCCESS = 1;// 摄像头登录成功
    public static final int IPC_LOGIN_FAIL = 2;// 摄像头登录失败
    public static final int IPC_GET_DEVICE_SUCCESS = 3;// 摄像头列表获取成功
    public static final int IPC_GET_DEVICE_FAIL = 4;// 摄像头列表获取失败
    private RangeHoodDialog mRangeHoodDialog;// 烟机 Dialog
    private WaterPurifierDialog mWaterPurifierDialog;// 净水器 Dialog
    private HeatKettleDialog mHeatKettleDialog;// 即热饮水吧 Dialog
    private PLMachineDialog mPLMachineDialog;// 管线机 Dialog
//    private CameraDialog mCameraDialog;// 摄像头 Dialog

    @Inject
    IotContract.Presenter mPresenter;

    @BindView(R.id.iot_no_content)
    ImageView mImageView;// 提示图片

    @BindView(R.id.title_bar_right)
    TextView mRightTextView;// 刷新

    @BindView(R.id.title_bar_right_progress)
    ProgressBar mProgressBar;// 进度条

    @BindView(R.id.iot_device_list)
    RecyclerView mRecyclerView;// 设备列表

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        layoutId = R.layout.activity_device_list;
        mTitle = getResources().getString(R.string.home_internet_of_things);
        super.onCreate(savedInstanceState);
        mProgressBar.setVisibility(View.VISIBLE);
        mRightTextView.setText(getResources().getString(R.string.iot_refresh));
        mPresenter.subscribe(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new RecyclerViewLinearDivider(this, LinearLayoutManager.VERTICAL, 20,
                getResources().getColor(android.R.color.transparent)));
        mAdapter = new DeviceAdapter(mList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(this);

        // IPC SDK 初始化
//        SDKInstance.init(this);
//        SDKInstance.getInstance().enableDebugLog(true);// 调试日志
//        new CameraLoginTask(mHandler).execute("13790455320", "zxc138ASD139");
//        mCameraList = new ArrayList<>();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.unSubscribe();
//        SDKInstance.getInstance().logout();// 退出登录
    }

    @OnClick(R.id.title_bar_right)
    public void refresh() {
        mPresenter.loadMiDeviceList();
        mRightTextView.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);
//        if (!isEntering && isIPCLogin) new GetCameraListTask(mHandler).execute();// 获取摄像头列表
    }

    @Override
    public void showMiDeviceList(List<AbstractDevice> list) {
        mRightTextView.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
        if (list.size() == 0) return;
        if (mImageView.getVisibility() == View.VISIBLE) mImageView.setVisibility(View.GONE);
        if (mRecyclerView.getVisibility() == View.GONE) mRecyclerView.setVisibility(View.VISIBLE);
        mList.clear();
        for (AbstractDevice abstractDevice : list) {
            DeviceInfo deviceInfo = new DeviceInfo();
            deviceInfo.setAbstractDevice(abstractDevice);
            mList.add(deviceInfo);
        }
        // 绿联摄像头
//        if (mCameraList.size() != 0) {
//            for (int i = 0; i < mCameraList.size(); i++) {
//                DeviceInfo info = new DeviceInfo();
//                info.setCameraInfo(mCameraList.get(i));
//                mList.add(info);
//            }
//        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void showRefreshFail() {
        mRightTextView.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
        ToastUtil.showCenter(FridgeApplication.getContext(), getResources().getString(R.string.iot_fail_tip));
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        DeviceInfo deviceInfo = mList.get(position);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        Bundle bundle = new Bundle();
        if (deviceInfo.getAbstractDevice() != null) { // 小米 IOT 设备
            if (deviceInfo.getAbstractDevice().isOnline()) { // 在线
                switch (deviceInfo.getAbstractDevice().getDeviceModel()) {
                    case AppConstants.YUNMI_WATERPURI_V1:
                    case AppConstants.YUNMI_WATERPURI_V2:
                    case AppConstants.YUNMI_WATERPURI_S1:
                    case AppConstants.YUNMI_WATERPURI_S2:
                    case AppConstants.YUNMI_WATERPURI_C1:
                    case AppConstants.YUNMI_WATERPURI_C2:
                    case AppConstants.YUNMI_WATERPURI_X3:
                    case AppConstants.YUNMI_WATERPURI_X5:
                    case AppConstants.YUNMI_WATERPURIFIER_V1:
                    case AppConstants.YUNMI_WATERPURIFIER_V2:
                    case AppConstants.YUNMI_WATERPURIFIER_V3:
                    case AppConstants.YUNMI_WATERPURI_LX2:
                    case AppConstants.YUNMI_WATERPURI_LX3:
                        if (mWaterPurifierDialog == null)
                            mWaterPurifierDialog = new WaterPurifierDialog();
                        if (!mWaterPurifierDialog.isAdded()) {
                            bundle.putString(mWaterPurifierDialog.PARAM_NAME, deviceInfo.getAbstractDevice().getName());
                            bundle.putString(mWaterPurifierDialog.PARAM_DID, deviceInfo.getAbstractDevice().getDeviceId());
                            bundle.putString(mWaterPurifierDialog.PARAM_MODEL, deviceInfo.getAbstractDevice().getDeviceModel());
                            mWaterPurifierDialog.setArguments(bundle);
                            mWaterPurifierDialog.show(ft, TAG);
                        }
                        break;
                    case AppConstants.YUNMI_KETTLE_R1:
                        if (mHeatKettleDialog == null) mHeatKettleDialog = new HeatKettleDialog();
                        if (!mHeatKettleDialog.isAdded()) {
                            bundle.putString(mHeatKettleDialog.PARAM_NAME, deviceInfo.getAbstractDevice().getName());
                            bundle.putString(mHeatKettleDialog.PARAM_DID, deviceInfo.getAbstractDevice().getDeviceId());
                            mHeatKettleDialog.setArguments(bundle);
                            mHeatKettleDialog.show(ft, TAG);
                        }
                        break;
                    case AppConstants.YUNMI_PLMACHINE_MG2:
                        if (mPLMachineDialog == null) mPLMachineDialog = new PLMachineDialog();
                        if (!mPLMachineDialog.isAdded()) {
                            bundle.putString(mPLMachineDialog.PARAM_NAME, deviceInfo.getAbstractDevice().getName());
                            bundle.putString(mPLMachineDialog.PARAM_DID, deviceInfo.getAbstractDevice().getDeviceId());
                            mPLMachineDialog.setArguments(bundle);
                            mPLMachineDialog.show(ft, TAG);
                        }
                        break;
                    case AppConstants.VIOMI_HOOD_A5:
                    case AppConstants.VIOMI_HOOD_A6:
                    case AppConstants.VIOMI_HOOD_A4:
                    case AppConstants.VIOMI_HOOD_A7:
                    case AppConstants.VIOMI_HOOD_C1:
                    case AppConstants.VIOMI_HOOD_H1:
                    case AppConstants.VIOMI_HOOD_H2:
                        if (mRangeHoodDialog == null) mRangeHoodDialog = new RangeHoodDialog();
                        if (!mRangeHoodDialog.isAdded()) {
                            bundle.putString(mRangeHoodDialog.PARAM_NAME, deviceInfo.getAbstractDevice().getName());
                            bundle.putString(mRangeHoodDialog.PARAM_DID, deviceInfo.getAbstractDevice().getDeviceId());
                            mRangeHoodDialog.setArguments(bundle);
                            mRangeHoodDialog.show(ft, TAG);
                        }
                        break;
                }
            } else {
                ToastUtil.showCenter(FridgeApplication.getContext(), getResources().getString(R.string.iot_device_offline_tip));
            }
        }
//        else if (deviceInfo.getCameraInfo() != null) { // 绿联摄像头
//            if (deviceInfo.getCameraInfo().isOnline()) {
//                bundle.putString(mCameraDialog.PARAM_DID, deviceInfo.getCameraInfo().getSrcId());
//                mCameraDialog.setArguments(bundle);
//                mCameraDialog.show(ft, TAG);
//            } else {
//                ToastUtil.showCenter(DeviceListActivity.this, getResources().getString(R.string.iot_device_offline_tip));
//            }
//        }
    }

    @Override
    protected void handleMessage(Message msg) {
        super.handleMessage(msg);
        switch (msg.what) {
            case IPC_LOGIN_SUCCESS:
                isIPCLogin = true;
//                new GetCameraListTask(mHandler).execute();// 获取摄像头设备列表
                break;
            case IPC_LOGIN_FAIL:
                isIPCLogin = false;
                if (mCount < 4) {
//                    SDKInstance.getInstance().logout();
                    mCount++;
//                    new CameraLoginTask(mHandler).execute("13790455320", "zxc138ASD139");// 重新登录
                } else {
                    isEntering = false;
                    mCount = 0;
                }
                break;
            case IPC_GET_DEVICE_SUCCESS:
                isEntering = false;
//                mCameraList.clear();
//                mCameraList.addAll(CameraListManager.getInstance().getCameraList());
//                for (int i = 0; i < mCameraList.size(); i++) {
//                    DeviceInfo info = new DeviceInfo();
//                    info.setCameraInfo(mCameraList.get(i));
//                    mList.add(info);
//                }
//                mAdapter.notifyDataSetChanged();
//                if (mCameraList.size() > 0) {
//                    if (mImageView.getVisibility() == View.VISIBLE)
//                        mImageView.setVisibility(View.GONE);
//                    if (mRecyclerView.getVisibility() == View.GONE)
//                        mRecyclerView.setVisibility(View.VISIBLE);
//                }
                break;
            case IPC_GET_DEVICE_FAIL:
                isEntering = false;
                break;
        }
    }
}
