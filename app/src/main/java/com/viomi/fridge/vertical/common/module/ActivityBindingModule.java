package com.viomi.fridge.vertical.common.module;


import com.viomi.fridge.vertical.administration.activity.FileListManageActivity;
import com.viomi.fridge.vertical.administration.activity.LoginGuideActivity;
import com.viomi.fridge.vertical.administration.activity.ManageActivity;
import com.viomi.fridge.vertical.administration.module.ManageModule;
import com.viomi.fridge.vertical.album.activity.AlbumActivity;
import com.viomi.fridge.vertical.album.activity.AlbumPreviewActivity;
import com.viomi.fridge.vertical.album.activity.ScreenSaverActivity;
import com.viomi.fridge.vertical.album.module.AlbumModule;
import com.viomi.fridge.vertical.common.scope.ActivityScoped;
import com.viomi.fridge.vertical.cookbook.activity.CookCategoryActivity;
import com.viomi.fridge.vertical.cookbook.activity.CookDetailActivity;
import com.viomi.fridge.vertical.cookbook.activity.CookMainActivity;
import com.viomi.fridge.vertical.cookbook.activity.CookSearchActivity;
import com.viomi.fridge.vertical.cookbook.module.CookCategoryModule;
import com.viomi.fridge.vertical.entertainment.MusicPlayActivity;
import com.viomi.fridge.vertical.foodmanage.activity.FoodManageActivity;
import com.viomi.fridge.vertical.foodmanage.module.FoodManageModule;
import com.viomi.fridge.vertical.fridgecontrol.activity.FactoryTestActivity;
import com.viomi.fridge.vertical.fridgecontrol.activity.OpenAlarmActivity;
import com.viomi.fridge.vertical.home.activity.HomeActivity;
import com.viomi.fridge.vertical.home.module.HomeModule;
import com.viomi.fridge.vertical.iot.activity.DeviceGalleryActivity;
import com.viomi.fridge.vertical.iot.activity.DeviceListActivity;
import com.viomi.fridge.vertical.iot.activity.LockPassWordActivity;
import com.viomi.fridge.vertical.iot.activity.LockVideoCallActivity;
import com.viomi.fridge.vertical.iot.module.IotModule;
import com.viomi.fridge.vertical.iot.module.LockModule;
import com.viomi.fridge.vertical.message.module.MessageCenterModule;
import com.viomi.fridge.vertical.message.view.activity.MessageCenterActivity;
import com.viomi.fridge.vertical.messageboard.activity.MessageBoardActivity;
import com.viomi.fridge.vertical.messageboard.activity.MessageBoardListActivity;
import com.viomi.fridge.vertical.messageboard.module.MessageBoardListModule;
import com.viomi.fridge.vertical.messageboard.module.MessageBoardModule;
import com.viomi.fridge.vertical.timer.activity.TimerActivity;
import com.viomi.fridge.vertical.web.activity.BrowserActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * 继承 BaseActivity 需在此类注册
 * Created by nanquan on 2018/1/6.
 */
@Module
public abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = HomeModule.class)
    abstract HomeActivity homeActivity();// 首页

    @ActivityScoped
    @ContributesAndroidInjector
    abstract BrowserActivity commonWebActivity();// 浏览器

    @ActivityScoped
    @ContributesAndroidInjector(modules = AlbumModule.class)
    abstract AlbumActivity albumActivity();// 电子相册

    @ActivityScoped
    @ContributesAndroidInjector()
    abstract AlbumPreviewActivity albumPreviewActivity();// 电子相册预览

    @ActivityScoped
    @ContributesAndroidInjector()
    abstract ScreenSaverActivity screenSaverActivity();// 屏保

    @ActivityScoped
    @ContributesAndroidInjector
    abstract TimerActivity timerActivity();// 定时器

    @ActivityScoped
    @ContributesAndroidInjector(modules = ManageModule.class)
    abstract ManageActivity manageActivity();// 管理中心

    @ActivityScoped
    @ContributesAndroidInjector
    abstract FactoryTestActivity factoryTestActivity();// 工厂测试

    @ActivityScoped
    @ContributesAndroidInjector
    abstract LoginGuideActivity loginGuideActivity();// 登录指引

    @ActivityScoped
    @ContributesAndroidInjector(modules = IotModule.class)
    abstract DeviceListActivity deviceListActivity();// 互联网家

    @ActivityScoped
    @ContributesAndroidInjector
    abstract FileListManageActivity fileListManageActivity();// 文件管理

    @ActivityScoped
    @ContributesAndroidInjector(modules = MessageCenterModule.class)
    abstract MessageCenterActivity messageCenterActivity();// 消息中心

    @ActivityScoped
    @ContributesAndroidInjector
    abstract CookMainActivity cookMenuActivity();// 菜谱首页

    @ActivityScoped
    @ContributesAndroidInjector(modules = CookCategoryModule.class)
    abstract CookCategoryActivity cookCategoryActivity();// 菜谱分类

    @ActivityScoped
    @ContributesAndroidInjector
    abstract CookSearchActivity cookSearchActivity();// 菜谱搜索

    @ActivityScoped
    @ContributesAndroidInjector
    abstract CookDetailActivity cookDetailActivity();// 菜谱详情

    @ActivityScoped
    @ContributesAndroidInjector(modules = FoodManageModule.class)
    abstract FoodManageActivity foodManageActivity();// 食材管理

    @ActivityScoped
    @ContributesAndroidInjector
    abstract MusicPlayActivity musicPlayActivity();// 音乐播放

    @ActivityScoped
    @ContributesAndroidInjector
    abstract OpenAlarmActivity openAlarmActivity();// 门开报警

    @ActivityScoped
    @ContributesAndroidInjector(modules = MessageBoardModule.class)
    abstract MessageBoardActivity messageBoardActivity();// 留言版

    @ActivityScoped
    @ContributesAndroidInjector(modules = MessageBoardListModule.class)
    abstract MessageBoardListActivity messageBoardListActivity();// 留言板列表

    @ActivityScoped
    @ContributesAndroidInjector(modules = LockModule.class)
    abstract LockVideoCallActivity lockVideoCallActivity();// 门锁对话

    @ActivityScoped
    @ContributesAndroidInjector(modules = LockModule.class)
    abstract LockPassWordActivity lockPassWordActivity();// 门锁对话

    @ActivityScoped
    @ContributesAndroidInjector
    abstract DeviceGalleryActivity deviceGalleryActivity();// 设备画廊
}
