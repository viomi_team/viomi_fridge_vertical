package com.viomi.fridge.vertical.message.view.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.message.view.adapter.MessageSettingPagerAdapter;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatDialogFragment;

/**
 * 消息中心设置 Dialog
 * Created by nanquan on 2018/2/7.
 */
public class MessageSettingDialog extends DaggerAppCompatDialogFragment {
    private ViewPager mViewPager;

    @Inject
    MainMessageSettingFragment mMainFragment;

    @Inject
    TimeMessageSettingFragment mTimeFragment;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return LayoutInflater.from(getActivity()).inflate(R.layout.dialog_message_setting, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        initViewPager(view);
        initMainDialog();
        initTimeDialog();
    }

    private void initViewPager(View view) {
        mViewPager = view.findViewById(R.id.vp_message_setting);
        MessageSettingPagerAdapter pagerAdapter = new MessageSettingPagerAdapter(getChildFragmentManager(), mMainFragment, mTimeFragment);
        mViewPager.setAdapter(pagerAdapter);
    }

    private void initMainDialog() {
        mMainFragment.setOnClickListener(new MainMessageSettingFragment.OnClickListener() {
            @Override
            public void onBackgroundClick() {
                dismiss();
            }

            @Override
            public void onTimeItemClick() {
                mViewPager.setCurrentItem(1);
            }
        });
    }

    private void initTimeDialog() {
        mTimeFragment.setOnClickListener(new TimeMessageSettingFragment.OnClickListener() {
            @Override
            public void onBackgroundClick() {
                mViewPager.setCurrentItem(0);
            }

            @Override
            public void onConfirmClick(int beginHour, int endHour) {
                mMainFragment.setTimeText(beginHour, endHour);
                mViewPager.setCurrentItem(0);
            }
        });
    }
}