package com.viomi.fridge.vertical.home.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;

/**
 * 思必驰天气 Json
 * Created by William on 2018/8/15.
 */
public class WindEntity implements Serializable {
    @JSONField(name = "deg")
    private String deg;
    @JSONField(name = "dir")
    private String dir;
    @JSONField(name = "sc")
    private String sc;
    @JSONField(name = "spd")
    private String spd;

    public String getDeg() {
        return deg;
    }

    public void setDeg(String deg) {
        this.deg = deg;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public String getSc() {
        return sc;
    }

    public void setSc(String sc) {
        this.sc = sc;
    }

    public String getSpd() {
        return spd;
    }

    public void setSpd(String spd) {
        this.spd = spd;
    }
}
