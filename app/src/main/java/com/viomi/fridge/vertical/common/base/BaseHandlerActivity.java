package com.viomi.fridge.vertical.common.base;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;

import java.lang.ref.WeakReference;

/**
 * 带 Handler 全局 Activity
 * Created by William on 2017/12/29.
 */
@SuppressLint("Registered")
public class BaseHandlerActivity extends BaseActivity {
    protected Handler mHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new BaseHandler(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
    }

    protected void handleMessage(Message msg) {

    }

    private static class BaseHandler extends Handler {
        WeakReference<BaseHandlerActivity> weakReference;

        private BaseHandler(BaseHandlerActivity activity) {
            this.weakReference = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (this.weakReference != null) {
                BaseHandlerActivity activity = this.weakReference.get();
                if (activity != null && !activity.isFinishing()) {
                    activity.handleMessage(msg);
                }
            }
        }
    }
}
