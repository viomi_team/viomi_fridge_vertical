package com.viomi.fridge.vertical.home.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;

/**
 * 思必驰天气 Json
 * Created by William on 2018/8/15.
 */
public class CondEntity implements Serializable {
    @JSONField(name = "code_d")
    private String code_d;
    @JSONField(name = "code_n")
    private String code_n;
    @JSONField(name = "txt_d")
    private String txt_d;
    @JSONField(name = "txt_n")
    private String txt_n;
    @JSONField(name = "code")
    private String code;
    @JSONField(name = "txt")
    private String txt;

    public String getCode_d() {
        return code_d;
    }

    public void setCode_d(String code_d) {
        this.code_d = code_d;
    }

    public String getCode_n() {
        return code_n;
    }

    public void setCode_n(String code_n) {
        this.code_n = code_n;
    }

    public String getTxt_d() {
        return txt_d;
    }

    public void setTxt_d(String txt_d) {
        this.txt_d = txt_d;
    }

    public String getTxt_n() {
        return txt_n;
    }

    public void setTxt_n(String txt_n) {
        this.txt_n = txt_n;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTxt() {
        return txt;
    }

    public void setTxt(String txt) {
        this.txt = txt;
    }
}
