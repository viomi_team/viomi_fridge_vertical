package com.viomi.fridge.vertical.iot.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseRecyclerViewAdapter;
import com.viomi.fridge.vertical.iot.model.http.entity.WaterPurifierFilter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 净水器滤芯适配器
 * Created by William on 2018/2/5.
 */
public class WaterPurifierFilterAdapter extends BaseRecyclerViewAdapter<WaterPurifierFilterAdapter.FilterHolder> {
    private List<WaterPurifierFilter> mList;

    public WaterPurifierFilterAdapter(List<WaterPurifierFilter> list) {
        this.mList = list;
        if (mList == null) mList = new ArrayList<>();
    }

    @Override
    public FilterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_holder_water_purifier_filter, parent, false);
        return new FilterHolder(view, this);
    }

    @Override
    public void onBindViewHolder(FilterHolder holder, int position) {
        WaterPurifierFilter waterPurifierFilter = mList.get(position);
        holder.nameTextView.setText(waterPurifierFilter.getName());
        holder.mDescTextView.setText(waterPurifierFilter.getDesc());
        String str;
        if (waterPurifierFilter.getPercent() < 0 || waterPurifierFilter.getPercent() > 100)
            str = "--";
        else str = waterPurifierFilter.getPercent() + "%";
        holder.mPerTextView.setText(str);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class FilterHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.water_purifier_filter_first)
        TextView nameTextView;
        @BindView(R.id.water_purifier_filter_first_desc)
        TextView mDescTextView;
        @BindView(R.id.water_purifier_filter_first_percent)
        TextView mPerTextView;

        FilterHolder(View itemView, WaterPurifierFilterAdapter adapter) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> adapter.onItemHolderClick(this,1000));
        }
    }
}