package com.viomi.fridge.vertical.speech.model.entity;

import java.io.Serializable;

/**
 * Created by young2 on 2017/5/26.
 */

public class TalkInfoEntity implements Serializable {
    public static final int TYPE_LISTEN = 0;
    public static final int TYPE_TALK = 1;
    public String msg;
    public int talkType;// 对话的类似，接听或者说
}
