package com.viomi.fridge.vertical.cookbook.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.view.dialog.LoadingDialog;
import com.viomi.fridge.vertical.common.base.BaseFragment;
import com.viomi.fridge.vertical.common.http.ApiClient;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.cookbook.activity.CookDetailActivity;
import com.viomi.fridge.vertical.cookbook.model.CookMenuDetail;
import com.viomi.fridge.vertical.cookbook.model.CookMenuMethod;
import com.viomi.fridge.vertical.cookbook.model.CookMenuSearch;
import com.viomi.fridge.vertical.cookbook.view.adapter.CookMenuListAdapter;

import java.util.ArrayList;
import java.util.List;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * 菜品分类 Fragment
 * Created by nanquan on 2018/2/27.
 */
public class CookCategoryFragment extends BaseFragment {
    private static final String TAG = CookCategoryFragment.class.getSimpleName();
    private CompositeSubscription mCompositeSubscription;
    private String mCtgId;
    private List<CookMenuDetail> mList;// 集合
    private CookMenuListAdapter mAdapter;
    private int mCurrentPage = 1;// 当前页数
    private int mCount = 0;// 无数据字数
    private boolean mIsRepeat = true;// 是否重复获取
    private LoadingDialog mLoadingDialog;// 加载对话框

    public static CookCategoryFragment getInstance(String ctgId) {
        CookCategoryFragment fragment = new CookCategoryFragment();
        Bundle bundle = new Bundle();
        bundle.putString("ctgId", ctgId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.fragment_cook_category;
    }

    @Override
    protected void initWithOnCreateView() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            mCtgId = bundle.getString("ctgId");
        }
        mLoadingDialog = new LoadingDialog(getActivity());
        mCompositeSubscription = new CompositeSubscription();
        initRecyclerView();
        mCurrentPage = 1;
        mCount = 0;
        mIsRepeat = true;
        getData();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
        if (mList != null) {
            mList.clear();
            mList = null;
        }
        if (mAdapter != null) mAdapter = null;
        if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
            mLoadingDialog.cancel();
            mLoadingDialog = null;
        }
    }

    private void initRecyclerView() {
        mList = new ArrayList<>();
        mAdapter = new CookMenuListAdapter(getActivity(), mList);
        mAdapter.setOnItemClickListener(position -> {
            Intent intent = new Intent(getActivity(), CookDetailActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("cookMenuDetail", mList.get(position));
            intent.putExtras(bundle);
            startActivity(intent);
        });
        RecyclerView recyclerView = mRootView.findViewById(R.id.rv_cook_category);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE: {
                        // 滑动到底部,加载更多
                        if (!recyclerView.canScrollVertically(1)) {
                            mCurrentPage++;
                            mIsRepeat = false;
                            getData();
                        }
                    }
                }
            }
        });
    }

    private void getData() {
        if (TextUtils.isEmpty(mCtgId)) return;
        if (mLoadingDialog != null && !mLoadingDialog.isShowing()) mLoadingDialog.show();
        Subscription subscription = ApiClient.getInstance().getApiService().getCookSearch(mCtgId, null, mCurrentPage, 20)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(cookMenuSearchMobResult -> {
                    CookMenuSearch cookMenuSearch = cookMenuSearchMobResult.getResult();
                    mList.addAll(filterNoImage(cookMenuSearch.getList()));
                    mAdapter.notifyDataSetChanged();
                    if (mIsRepeat && mCount < 3 && mList.size() < 8) {
                        mCount++;
                        mCurrentPage++;
                        getData();
                        return;
                    }
                    mIsRepeat = false;
                    if (mLoadingDialog != null && mLoadingDialog.isShowing())
                        mLoadingDialog.dismiss();
                }, throwable -> {
                    if (mLoadingDialog != null && mLoadingDialog.isShowing())
                        mLoadingDialog.dismiss();
                    logUtil.e(TAG, throwable.getMessage());
                });
        mCompositeSubscription.add(subscription);
    }

    /**
     * 过滤无图菜谱
     */
    private List<CookMenuDetail> filterNoImage(List<CookMenuDetail> list) {
        List<CookMenuDetail> list1 = new ArrayList<>();
        for (CookMenuDetail detail : list) {
            if (detail == null ||
                    detail.getRecipe() == null ||
                    detail.getRecipe().getImg() == null ||
                    TextUtils.isEmpty(detail.getRecipe().getImg()) ||
                    detail.getRecipe().getMethod() == null ||
                    TextUtils.isEmpty(detail.getRecipe().getMethod())) {
                logUtil.d(TAG, "detail is null");
            } else {
                Gson gson = new Gson();
                List<CookMenuMethod> methodList = gson.fromJson(detail.getRecipe().getMethod(),
                        new TypeToken<List<CookMenuMethod>>() {
                        }.getType());
                boolean isReturn = false;
                for (CookMenuMethod cookMenuMethod : methodList) {
                    if (cookMenuMethod.getImg() == null || cookMenuMethod.getImg().equals("")
                            || cookMenuMethod.getStep() == null || cookMenuMethod.getStep().equals("")) {
                        isReturn = true;
                        break;
                    }
                }
                if (!isReturn) list1.add(detail);
            }
        }
        return list1;
    }
}