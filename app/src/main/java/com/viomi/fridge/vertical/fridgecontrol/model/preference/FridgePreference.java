package com.viomi.fridge.vertical.fridgecontrol.model.preference;

import android.content.Context;
import android.content.SharedPreferences;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.entity.ChangeableScene;
import com.viomi.fridge.vertical.fridgecontrol.model.serial.entity.DeviceParams;

import java.util.ArrayList;
import java.util.List;

/**
 * 冰箱控制相关数据缓存
 * Created by William on 2018/2/3.
 */
public class FridgePreference {
    private static FridgePreference mInstance;
    private static final String name = "fridgeControl";
    private static final String DATA_SEND = "data_send";// 串口发送数据
    private static final String DATA_READ = "data_read";// 串口读取数据
    private static final String MODEL = "model";// 冰箱 model
    private static final String COLD_TEMP = "cold_temp";// 冷藏室设置温度
    private static final String CHANGEABLE_TEMP = "change_temp";// 变温室设置温度
    private static final String FREEZING_TEMP = "freezing_temp";// 冷冻室设置温度
    private static final String FILTER_LIFE_USED = "filter_life_used";// 滤芯已使用时间
    private static final String OUTDOOR_TEMP = "outdoor_temp";// 室外温度
    private static final String START_HOURS = "start_hours";// 冰箱启动天数
    private static final String SCENE_LIST = "scene_list";// 选择场景集合
    private static final String SCENE_NAME = "scene";// 选择场景
    private static final String CITY = "city";// 城市
    private static final String CITY_CODE = "city_code";// 城市编码
    private static final String WEATHER_REPORT = "weather_report";// 天气预报
    private static final String INSPECTION_FLAG = "inspection_flag";// 商检标志
    private static final String RUN_TIME = "run_time";// 压缩机累计运行时间
    private static final String LOW_FILTER_FLAG = "low_filter_flag";// 滤芯即将过期推送
    private static final String OUT_DATE_FILTER_FLAG = "out_date_filter_flag";// 滤芯过期推送
    private static final String AIR_QUALITY = "air_quality";// 空气质量
    private static final String PM_25 = "pm_25";// PM 2.5
    private static final String LATITUDE = "latitude";// 纬度
    private static final String LONGITUDE = "longitude";// 经度

    public static FridgePreference getInstance() {
        if (mInstance == null) {
            synchronized (FridgePreference.class) {
                if (mInstance == null) {
                    mInstance = new FridgePreference();
                }
            }
        }
        return mInstance;
    }

    private SharedPreferences getSharedPreferences() {
        return FridgeApplication.getInstance().getSharedPreferences(
                name, Context.MODE_PRIVATE);
    }

    /**
     * 缓存冰箱串口发送数据
     */
    public void saveFridgeSetData(DeviceParams params) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(DATA_SEND, JSONObject.toJSONString(params));
        editor.apply();
    }

    /**
     * 读取缓存冰箱串口发送数据
     */
    public DeviceParams getFridgeSetData() {
        DeviceParams params = null;
        SharedPreferences sharedPreferences = getSharedPreferences();
        String json = sharedPreferences.getString(DATA_SEND, null);
        if (json != null) {
            params = JSON.parseObject(json, new TypeReference<DeviceParams>() {
            });
        }
        return params;
    }

    /**
     * 缓存冰箱串口读取数据
     */
    public void saveFridgeGetData(DeviceParams params) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(DATA_READ, JSONObject.toJSONString(params));
        editor.apply();
    }

    /**
     * 读取缓存冰箱串口读取数据
     */
    public DeviceParams getFridgeGetData() {
        DeviceParams params = null;
        SharedPreferences sharedPreferences = getSharedPreferences();
        String json = sharedPreferences.getString(DATA_READ, null);
        if (json != null) {
            params = JSON.parseObject(json, new TypeReference<DeviceParams>() {
            });
        }
        return params;
    }

    /**
     * 缓存冰箱 model
     */
    public void saveModel(String model) {
        if (model == null || model.equals("")) model = AppConstants.MODEL_X2;
        cacheString(MODEL, model);
    }

    /**
     * 获取缓存冰箱 model
     */
    public String getModel() {
        return getSharedPreferences().getString(MODEL, AppConstants.MODEL_X2);
    }

    /**
     * 缓存冷藏室设置温度
     */
    public void saveColdTemp(int temp) {
        temp = temp == AppConstants.ROOM_CLOSED_TEMP ? AppConstants.COLD_TEMP_DEFAULT : temp;
        cacheInt(COLD_TEMP, temp);
    }

    /**
     * 读取缓存冷藏室设置温度
     */
    public int getColdTemp() {
        return getSharedPreferences().getInt(COLD_TEMP, AppConstants.COLD_TEMP_DEFAULT);
    }

    /**
     * 缓存变温室设置温度
     */
    public void saveChangeableColdTemp(int temp) {
        temp = temp == AppConstants.ROOM_CLOSED_TEMP ? AppConstants.CHANGEABLE_TEMP_DEFAULT : temp;
        cacheInt(CHANGEABLE_TEMP, temp);
    }

    /**
     * 读取缓存变温室设置温度
     */
    public int getChangeableTemp() {
        return getSharedPreferences().getInt(CHANGEABLE_TEMP, AppConstants.CHANGEABLE_TEMP_DEFAULT);
    }

    /**
     * 缓存冷冻室设置温度
     */
    public void saveFreezingTemp(int temp) {
        temp = temp == AppConstants.ROOM_CLOSED_TEMP ? AppConstants.FREEZING_TEMP_DEFAULT : temp;
        cacheInt(FREEZING_TEMP, temp);
    }

    /**
     * 读取缓存冷冻室设置温度
     */
    public int getFreezingTemp() {
        return getSharedPreferences().getInt(FREEZING_TEMP, AppConstants.FREEZING_TEMP_DEFAULT);
    }

    /**
     * 缓存滤芯已使用时间（单位: 小时）
     */
    public String saveUsedFilterLife(int FilterLife) {
        FilterLife = FilterLife < 0 ? 0 : FilterLife;
        cacheInt(FILTER_LIFE_USED, FilterLife);
        return null;
    }

    /**
     * 读取缓存滤芯已使用时间（单位: 小时）
     */
    public int getUsedFilterLife() {
        return getSharedPreferences().getInt(FILTER_LIFE_USED, 0);
    }

    /**
     * 缓存冰箱所在城市编码
     */
    public void saveCityCode(String code) {
        code = code == null ? "" : code;
        cacheString(CITY_CODE, code);
    }

    /**
     * 读取冰箱所在城市编码
     */
    public String getCityCode() {
        return getSharedPreferences().getString(CITY_CODE, "");
    }

    /**
     * 缓存冰箱所在城市
     */
    public void saveCity(String city) {
        city = city == null ? "" : city;
        cacheString(CITY, city);
    }

    /**
     * 读取冰箱缓存城市
     */
    public String getCity() {
        return getSharedPreferences().getString(CITY, "");
    }

    /**
     * 缓存纬度
     */
    public void saveLatitude(String latitude) {
        cacheString(LATITUDE, latitude);
    }

    /**
     * 读取纬度
     */
    public String getLatitude() {
        return getSharedPreferences().getString(LATITUDE, "");
    }

    /**
     * 缓存经度
     */
    public void saveLongitude(String longitude) {
        cacheString(LONGITUDE, longitude);
    }

    /**
     * 读取经度
     */
    public String getLongitude() {
        return getSharedPreferences().getString(LONGITUDE, "");
    }

    /**
     * 缓存天气预报
     */
    public void saveWeatherReport(String report) {
        report = report == null ? "" : report;
        cacheString(WEATHER_REPORT, report);
    }

    /**
     * 读取缓存天气预报
     */
    public String getWeatherReport() {
        return getSharedPreferences().getString(WEATHER_REPORT, "");
    }

    /**
     * 缓存空气质量
     */
    public void saveAirQuality(String air) {
        air = air == null ? "" : air;
        cacheString(AIR_QUALITY, air);
    }

    /**
     * 读取缓存空气质量
     */
    public String getAirQuality() {
        return getSharedPreferences().getString(AIR_QUALITY, "");
    }

    /**
     * 缓存 PM 2.5
     */
    public void savePM25(String pm25) {
        pm25 = pm25 == null ? "" : pm25;
        cacheString(PM_25, pm25);
    }

    /**
     * 读取缓存 PM 2.5
     */
    public String getPM25() {
        return getSharedPreferences().getString(PM_25, "");
    }

    /**
     * 缓存室外温度
     */
    public void saveOutdoorTemp(String temp) {
        temp = temp == null ? "" : temp;
        cacheString(OUTDOOR_TEMP, temp);
    }

    /**
     * 读取缓存室外温度
     */
    public String getOutdoorTemp() {
        return getSharedPreferences().getString(OUTDOOR_TEMP, "");
    }

    /**
     * 缓存启动时间（单位：小时）
     */
    public void saveStartHours(int hour) {
        cacheInt(START_HOURS, hour);
    }

    /**
     * 读取缓存启动时间（单位：小时）
     */
    public int getStartHours() {
        return getSharedPreferences().getInt(START_HOURS, 0);
    }

    /**
     * 缓存压缩机累计运行时间
     */
    public void saveRunTime(int time) {
        cacheInt(RUN_TIME, time);
    }

    /**
     * 读取缓存压缩机累计运行时间
     */
    public int getRunTime() {
        return getSharedPreferences().getInt(RUN_TIME, 0);
    }

    /**
     * 缓存变温室选择场景集合
     */
    public String saveSceneList(List<ChangeableScene> list) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(SCENE_LIST, json);
        editor.apply();
        return null;
    }

    /**
     * 获取缓存变温室场景集合
     */
    public List<ChangeableScene> getSceneList() {
        List<ChangeableScene> list = new ArrayList<>();
        SharedPreferences sharedPreferences = getSharedPreferences();
        String json = sharedPreferences.getString(SCENE_LIST, null);
        if (json != null) {
            Gson gson = new Gson();
            list = gson.fromJson(json, new TypeToken<List<ChangeableScene>>() {
            }.getType());
        }
        return list;
    }

    /**
     * 缓存选择场景
     */
    public void saveScene(String name) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(SCENE_NAME, name);
        editor.apply();
    }

    /**
     * 获取缓存选择场景
     */
    public String getScene() {
        return getSharedPreferences().getString(SCENE_NAME, "");
    }

    /**
     * 缓存商检标志
     */
    public void saveInspectionFlag(boolean enable) {
        cacheBoolean(INSPECTION_FLAG, enable);
    }

    /**
     * 读取商检标志
     */
    public boolean getInspectionFlag() {
        return getSharedPreferences().getBoolean(INSPECTION_FLAG, false);
    }

    /**
     * 缓存滤芯即将过期标志
     */
    public void setFilterLowFlag(boolean flag) {
        cacheBoolean(LOW_FILTER_FLAG, flag);
    }

    /**
     * 读取缓存滤芯即将过期标志
     */
    public boolean getFilterLowFlag() {
        return getSharedPreferences().getBoolean(LOW_FILTER_FLAG, false);
    }

    /**
     * 缓存滤芯过期标志
     */
    public void setFilterOutOfDateFlag(boolean flag) {
        cacheBoolean(OUT_DATE_FILTER_FLAG, flag);
    }

    /**
     * 读取缓存滤芯过期标志
     */
    public boolean getFilterOutOfDateFlag() {
        return getSharedPreferences().getBoolean(OUT_DATE_FILTER_FLAG, true);
    }

    /**
     * 缓存 Int 类型数据
     */
    private void cacheInt(String key, int value) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    /**
     * 缓存 String 类型数据
     */
    private void cacheString(String key, String value) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    /**
     * 缓存 Boolean 类型数据
     */
    private void cacheBoolean(String key, boolean value) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }
}