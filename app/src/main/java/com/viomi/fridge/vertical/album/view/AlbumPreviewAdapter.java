package com.viomi.fridge.vertical.album.view;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeController;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.widget.zoomableview.DoubleTapGestureListener;
import com.viomi.widget.zoomableview.ZoomableDraweeView;

import java.io.File;
import java.util.ArrayList;

/**
 * 电子相册预览 Adapter
 * Created by William on 2018/3/7.
 */
public class AlbumPreviewAdapter extends PagerAdapter {
    private static final String TAG = AlbumPreviewAdapter.class.getSimpleName();
    private Context mContext;
    private ArrayList<String> mPaths;// 目录集合
    private int mWidth, mHeight;// 屏幕宽高

    public AlbumPreviewAdapter(Context context, ArrayList<String> paths, int width, int height) {
        mContext = context;
        mPaths = paths;
        mWidth = width;
        mHeight = height;
        if (mPaths == null) mPaths = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return mPaths != null ? mPaths.size() : 0;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.view_holder_album_preview, container, false);
        ZoomableDraweeView zoomableDraweeView = view.findViewById(R.id.holder_album_preview);
        // 允许缩放时切换
        zoomableDraweeView.setAllowTouchInterceptionWhileZoomed(true);
        // 长按
        zoomableDraweeView.setIsLongpressEnabled(false);
        // 双击击放大或缩小
        zoomableDraweeView.setTapListener(new DoubleTapGestureListener(zoomableDraweeView));
        // 先采样
        int[] size = ToolUtil.getImageSize(mPaths.get(position));// 获取图片宽高
        int imageWidth = size[0];// 图片高度
        int imageHeight = size[1];// 图片宽度
        int sampleHeight;// 采样高度
        int sampleWidth;// 采样宽度
        logUtil.d(TAG, "height:" + imageHeight + ",width:" + imageWidth);
        if (imageHeight >= imageWidth) { // 竖图
            if (imageHeight > mHeight) sampleHeight = mHeight;
            else sampleHeight = imageHeight;
            sampleHeight = (int) (sampleHeight * 0.8);
            sampleWidth = (int) ((float) (imageWidth * sampleHeight) / (float) imageHeight);
            sampleWidth = (int) (sampleWidth * 0.8);
        } else { // 横图
            if (imageWidth > mWidth) sampleWidth = mWidth;
            else sampleWidth = imageWidth;
            sampleWidth = (int) (sampleWidth * 0.8);
            sampleHeight = (int) ((float) (imageHeight * sampleWidth) / (float) imageWidth);
            sampleHeight = (int) (sampleHeight * 0.8);
        }
        logUtil.d(TAG, "height:" + sampleHeight + ",width:" + sampleWidth);

        ImageRequest request = ImageRequestBuilder
                .newBuilderWithSource(Uri.fromFile(new File(mPaths.get(position))))
                .setResizeOptions(new ResizeOptions(sampleWidth, sampleHeight))
                .build();
        PipelineDraweeController controller = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                .setOldController(zoomableDraweeView.getController())
                .setImageRequest(request)
                .build();
        // 加载图片
        zoomableDraweeView.setController(controller);
        container.addView(view);
        view.requestLayout();
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        View view = (View) object;
        ZoomableDraweeView zoomableDraweeView = view.findViewById(R.id.holder_album_preview);
        zoomableDraweeView.setController(null);
        container.removeView(view);
    }
}