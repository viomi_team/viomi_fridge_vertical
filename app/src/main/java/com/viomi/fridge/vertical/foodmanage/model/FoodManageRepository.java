package com.viomi.fridge.vertical.foodmanage.model;

import android.os.Environment;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.foodmanage.model.entity.FoodDetail;
import com.viomi.fridge.vertical.fridgecontrol.model.repository.MiotRepository;
import com.viomi.fridge.vertical.message.entity.DeviceInfoMessage;
import com.viomi.fridge.vertical.message.entity.PushMsg;
import com.viomi.fridge.vertical.message.manager.InfoManager;
import com.viomi.fridge.vertical.message.manager.PushManager;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import rx.Observable;

/**
 * 食材管理相关数据操作
 * Created by William on 2018/1/10.
 */
public class FoodManageRepository {
    private static final String TAG = FoodManageRepository.class.getSimpleName();
    private static FoodManageRepository mInstance;
    private final String XML_PATH = Environment.getExternalStorageDirectory().toString() + "/" + AppConstants.PATH
            + AppConstants.FOOD_MANAGE_PATH + AppConstants.FOOD_MANAGE_SAVE_FILE;// 食材信息保存文件

    public static FoodManageRepository getInstance() {
        if (mInstance == null) {
            synchronized (FoodManageRepository.class) {
                if (mInstance == null) {
                    mInstance = new FoodManageRepository();
                }
            }
        }
        return mInstance;
    }

    /**
     * 删除食材图片
     *
     * @param path: 图片路径
     */
    public Observable<Boolean> deleteImage(String path) {
        return Observable.create(subscriber -> {
            File file = new File(path);
            if (file.exists()) subscriber.onNext(file.delete());
            else subscriber.onNext(true);
            subscriber.onCompleted();
        });
    }

    /**
     * 删除拍照缓存
     */
    public Observable<Boolean> deleteCache() {
        return Observable.create(subscriber -> {
            File file = new File(Environment.getExternalStorageDirectory().toString() + "/" + AppConstants.PATH + AppConstants.CAMERA_CACHE);
            if (file.exists() && file.isDirectory()) {
                File[] files = file.listFiles();
                for (File file1 : files) {
                    if (file1.exists())
                        logUtil.d(TAG, file.getName() + "delete " + file1.delete());
                }
                subscriber.onNext(file.delete());
            } else subscriber.onNext(true);
            subscriber.onCompleted();
        });
    }

    /**
     * 保存食材信息
     */
    public Observable<Boolean> saveFood(FoodDetail detail) {
        Observable<Boolean> createObservable = createXml(detail);
        Observable<Boolean> modifyObservable = modifyXml(detail);
        File file = new File(XML_PATH);
        if (file.exists()) return modifyObservable;
        else return createObservable;
    }

    /**
     * 重新创建 XML 文件
     */
    private Observable<Boolean> createXml(FoodDetail detail) {
        return Observable.create(subscriber -> {
            try {
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document doc = builder.newDocument();

                // 创建根节点
                Element eleRoot = doc.createElement("root");
                eleRoot.setAttribute("author", "viomi");
                eleRoot.setAttribute("date", new Date().toString());
                doc.appendChild(eleRoot);

                // 创建食材根节点
                Element eleFood = doc.createElement("food");
                eleRoot.appendChild(eleFood);

                // 食材存放类型节点
                Element eleType = doc.createElement("type");
                Node nodeType = doc.createTextNode(detail.getRoomType());
                eleType.appendChild(nodeType);
                eleFood.appendChild(eleType);

                // 食材名称节点
                Element eleName = doc.createElement("name");
                Node nodeName = doc.createTextNode(detail.getName());
                eleName.appendChild(nodeName);
                eleFood.appendChild(eleName);

                // 食材添加时间节点
                Element eleTime = doc.createElement("time");
                Node nodeTime = doc.createTextNode(detail.getAddTime());
                eleTime.appendChild(nodeTime);
                eleFood.appendChild(eleTime);

                // 食材绘图保存路径
                Element elePath = doc.createElement("path");
                Node nodePath = doc.createTextNode(detail.getImgPath());
                elePath.appendChild(nodePath);
                eleFood.appendChild(elePath);

                // 食材到期时间
                Element eleDeadLine = doc.createElement("deadline");
                Node nodeDeadLine = doc.createTextNode(detail.getDeadLine());
                eleDeadLine.appendChild(nodeDeadLine);
                eleFood.appendChild(eleDeadLine);

                // 默认未推送
                Element elePush = doc.createElement("push");
                Node nodePush = doc.createTextNode(detail.getIsPush());
                elePush.appendChild(nodePush);
                eleFood.appendChild(elePush);

                // 食材分类
                Element eleFoodType = doc.createElement("foodType");
                Node nodeFoodType = doc.createTextNode(detail.getFoodType());
                eleFoodType.appendChild(nodeFoodType);
                eleFood.appendChild(eleFoodType);

                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();
                DOMSource domSource = new DOMSource(doc);
                transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");// 编码方式
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");// 是否缩进

                File dir = new File(Environment.getExternalStorageDirectory().toString() + "/" + AppConstants.PATH + AppConstants.FOOD_MANAGE_PATH);
                boolean isExist = dir.exists() || dir.mkdirs();
                if (isExist) {
                    File file = new File(XML_PATH);
                    boolean isCreate = file.createNewFile();
                    if (isCreate) {
                        FileOutputStream fileOutputStream = new FileOutputStream(file);
                        PrintWriter printWriter = new PrintWriter(fileOutputStream);
                        StreamResult streamResult = new StreamResult(printWriter);
                        transformer.transform(domSource, streamResult);
                        fileOutputStream.flush();
                        fileOutputStream.getFD().sync();// 掉电保护
                        fileOutputStream.close();
                        subscriber.onNext(true);
                    } else subscriber.onNext(false);
                } else subscriber.onNext(false);
                subscriber.onCompleted();
            } catch (TransformerFactoryConfigurationError | Exception e) {
                e.printStackTrace();
                logUtil.e(TAG, e.toString());
                subscriber.onNext(false);
                subscriber.onCompleted();
            }
        });
    }

    /**
     * 在原 XML 文件上添加
     */
    private Observable<Boolean> modifyXml(FoodDetail detail) {
        return Observable.create(subscriber -> {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            try {
                DocumentBuilder db = dbFactory.newDocumentBuilder();
                Document doc = db.parse(new File(XML_PATH));
                Element root = (Element) doc.getFirstChild();

                // 创建食材根节点
                Element eleFood = doc.createElement("food");
                root.appendChild(eleFood);

                // 食材存放类型节点
                Element eleType = doc.createElement("type");
                Node nodeType = doc.createTextNode(detail.getRoomType());
                eleType.appendChild(nodeType);
                eleFood.appendChild(eleType);

                // 食材名称节点
                Element eleName = doc.createElement("name");
                Node nodeName = doc.createTextNode(detail.getName());
                eleName.appendChild(nodeName);
                eleFood.appendChild(eleName);

                // 食材添加时间节点
                Element eleTime = doc.createElement("time");
                Node nodeTime = doc.createTextNode(detail.getAddTime());
                eleTime.appendChild(nodeTime);
                eleFood.appendChild(eleTime);

                // 食材绘图保存路径
                Element elePath = doc.createElement("path");
                Node nodePath = doc.createTextNode(detail.getImgPath());
                elePath.appendChild(nodePath);
                eleFood.appendChild(elePath);

                // 食材到期时间
                Element eleDeadLine = doc.createElement("deadline");
                Node nodeDeadLine = doc.createTextNode(detail.getDeadLine());
                eleDeadLine.appendChild(nodeDeadLine);
                eleFood.appendChild(eleDeadLine);

                // 默认未推送
                Element elePush = doc.createElement("push");
                Node nodePush = doc.createTextNode(detail.getIsPush());
                elePush.appendChild(nodePush);
                eleFood.appendChild(elePush);

                // 食材分类
                Element eleFoodType = doc.createElement("foodType");
                Node nodeFoodType = doc.createTextNode(detail.getFoodType());
                eleFoodType.appendChild(nodeFoodType);
                eleFood.appendChild(eleFoodType);

                // 因为 dom 是将一棵树整个拷进内存操作, 上面操作的只是内存中的 dom, 并没有存到硬盘中去。
                // 下面就是将内存中的 dom 树存入硬盘中
                // 第一步，用 TransformerFactory 的工厂方法得到一个 tfFactory 对象
                TransformerFactory tfFactory = TransformerFactory.newInstance();
                // 第二步，用 tfFactory 对象的 newTransformer 得到一个 Transformer 对象 tf
                Transformer tf = tfFactory.newTransformer();
                // 第三步，用 tf 对象的 transform 方法保存
                tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");    // 编码方式
                tf.setOutputProperty(OutputKeys.INDENT, "yes");    // 是否缩进
                FileOutputStream fileOutputStream = new FileOutputStream(new File(XML_PATH));
                PrintWriter printWriter = new PrintWriter(fileOutputStream);
                StreamResult streamResult = new StreamResult(printWriter);
                tf.transform(new DOMSource(doc), streamResult);
                fileOutputStream.flush();
                fileOutputStream.getFD().sync();// 掉电保护
                fileOutputStream.close();
                subscriber.onNext(true);
                subscriber.onCompleted();
            } catch (ParserConfigurationException | TransformerException | IOException | SAXException e) {
                e.printStackTrace();
                logUtil.e(TAG, e.toString());
                logUtil.d(TAG, new File(XML_PATH).delete() + "");
                subscriber.onNext(false);
                subscriber.onCompleted();
            }
        });
    }

    /**
     * 批量删除食材
     */
    public Observable<Boolean> delete(List<FoodDetail> list) {
        return Observable.create(subscriber -> {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            try {
                DocumentBuilder db = dbFactory.newDocumentBuilder();
                Document dom = db.parse(new File(XML_PATH));
                Element root = (Element) dom.getFirstChild();
                NodeList foodList = root.getElementsByTagName("food");

                for (int j = 0; j < list.size(); j++) {
                    if (list.get(j).isSelected()) {
                        // 删除文件中数据
                        for (int i = 0; i < foodList.getLength(); i++) {
                            Element userUpdate = (Element) foodList.item(i);
                            // 获取 food 节点下的所有子节点
                            NodeList childNodes = userUpdate.getChildNodes();
                            Node node = childNodes.item(5); // 获取唯一标识
                            Element childNode = (Element) node;
                            if (childNode.getFirstChild().getTextContent().equals(list.get(j).getAddTime())) {
                                Node pathNode = childNodes.item(7); // 获取食材图片路径
                                Element pathChild = (Element) pathNode;
                                File file = new File(pathChild.getFirstChild().getTextContent());
                                if (file.exists()) {
                                    boolean isSuccess = file.delete();
                                    logUtil.d(TAG, "delete img " + isSuccess);
                                }
                                root.removeChild(foodList.item(i));
                                break;
                            }
                        }
                        list.remove(j);
                        j = j - 1;
                    }
                }
                // 保存数据
                TransformerFactory tfFactory = TransformerFactory.newInstance();
                Transformer tf = tfFactory.newTransformer();
                tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");    // 编码方式
                tf.setOutputProperty(OutputKeys.INDENT, "yes");    // 是否缩进
                FileOutputStream fileOutputStream = new FileOutputStream(new File(XML_PATH));
                PrintWriter printWriter = new PrintWriter(fileOutputStream);
                StreamResult streamResult = new StreamResult(printWriter);
                tf.transform(new DOMSource(dom), streamResult);
                fileOutputStream.flush();
                fileOutputStream.getFD().sync();// 掉电保护
                fileOutputStream.close();
                subscriber.onNext(true);
                subscriber.onCompleted();
            } catch (ParserConfigurationException | TransformerException | IOException | SAXException e) {
                e.printStackTrace();
                logUtil.e(TAG, e.toString());
                logUtil.d(TAG, new File(XML_PATH).delete() + "");
                subscriber.onNext(false);
                subscriber.onCompleted();
            }
        });
    }

    /**
     * 修改单个食材信息
     */
    public Observable<Boolean> updateFood(FoodDetail foodDetail, String id) {
        return Observable.create(subscriber -> {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            try {
                DocumentBuilder db = dbFactory.newDocumentBuilder();
                Document dom = db.parse(new File(XML_PATH));
                Element root = (Element) dom.getFirstChild();
                NodeList foodList = root.getElementsByTagName("food");
                for (int i = 0; i < foodList.getLength(); i++) {
                    Element userUpdate = (Element) foodList.item(i);

                    // 获取 food 节点下的所有子节点
                    NodeList childNodes = userUpdate.getChildNodes();
                    Node node = childNodes.item(5);
                    Element childNode = (Element) node;

                    if (childNode.getFirstChild().getTextContent().equals(id)) {
                        Node node0 = childNodes.item(1);
                        Element childNode0 = (Element) node0;
                        childNode0.setTextContent(foodDetail.getRoomType());

                        Node node1 = childNodes.item(3);
                        Element childNode1 = (Element) node1;
                        childNode1.setTextContent(foodDetail.getName());

                        Node node2 = childNodes.item(9);
                        Element childNode2 = (Element) node2;
                        childNode2.setTextContent(foodDetail.getDeadLine());

                        Node node3 = childNodes.item(5);
                        Element childNode3 = (Element) node3;
                        childNode3.setTextContent(String.valueOf(System.currentTimeMillis()));

                        Node node4 = childNodes.item(11);
                        Element childNode4 = (Element) node4;
                        childNode4.setTextContent("false");

                        Node node5 = childNodes.item(13);
                        Element childNode5 = (Element) node5;
                        childNode5.setTextContent(foodDetail.getFoodType());

                        Node node6 = childNodes.item(7);
                        Element childNode6 = (Element) node6;
                        childNode6.setTextContent(foodDetail.getImgPath());
                        break;
                    }
                }
                // 从内存写入硬盘
                TransformerFactory tfFactory = TransformerFactory.newInstance();
                Transformer tf = tfFactory.newTransformer();
                tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");    // 编码方式
                tf.setOutputProperty(OutputKeys.INDENT, "yes");    // 是否缩进
                FileOutputStream fileOutputStream = new FileOutputStream(new File(XML_PATH));
                PrintWriter printWriter = new PrintWriter(fileOutputStream);
                StreamResult streamResult = new StreamResult(printWriter);
                tf.transform(new DOMSource(dom), streamResult);
                fileOutputStream.flush();
                fileOutputStream.getFD().sync();// 掉电保护
                fileOutputStream.close();
                subscriber.onNext(true);
                subscriber.onCompleted();
            } catch (ParserConfigurationException | IOException | SAXException | TransformerException e) {
                e.printStackTrace();
                logUtil.e(TAG, e.toString());
                logUtil.d(TAG, new File(XML_PATH).delete() + "");
                subscriber.onNext(false);
                subscriber.onCompleted();
            }
        });
    }

    /**
     * 读取食材信息
     */
    public Observable<List<FoodDetail>> loadFood() {
        return Observable.create(subscriber -> {
            List<FoodDetail> list = new ArrayList<>();
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            try {
                DocumentBuilder builder = factory.newDocumentBuilder();
                if (!new File(XML_PATH).exists()) {
                    subscriber.onNext(null);
                    return;
                }
                FileInputStream fileIS = new FileInputStream(XML_PATH);
                Document dom = builder.parse(fileIS);
                dom.normalize();

                Element root = dom.getDocumentElement();
                NodeList items = root.getElementsByTagName("food");// 查找所有 food 节点

                for (int i = 0; i < items.getLength(); i++) {
                    FoodDetail data = new FoodDetail();
                    // 得到第一个 food 节点
                    Element personNode = (Element) items.item(i);
                    // 获取 food 节点下的所有子节点
                    NodeList childNodes = personNode.getChildNodes();

                    for (int j = 0; j < childNodes.getLength(); j++) {
                        Node node = childNodes.item(j); // 判断是否为元素类型
                        if (node.getNodeType() == Node.ELEMENT_NODE) {
                            Element childNode = (Element) node;
                            if ("type".equals(childNode.getNodeName())) { // 仓室类型
                                data.setRoomType(childNode.getFirstChild().getNodeValue());
                            } else if ("name".equals(childNode.getNodeName())) { // 食材名称
                                data.setName(childNode.getFirstChild().getNodeValue());
                            } else if ("time".equals(childNode.getNodeName())) { // 添加时间
                                data.setAddTime(childNode.getFirstChild().getNodeValue());
                            } else if ("path".equals(childNode.getNodeName())) { // 图片保存路径
                                data.setImgPath(childNode.getFirstChild().getNodeValue());
                            } else if ("deadline".equals(childNode.getNodeName())) { // 到期时间
                                data.setDeadLine(childNode.getFirstChild().getNodeValue());
                            } else if ("push".equals(childNode.getNodeName())) { // 是否已推送
                                data.setIsPush(childNode.getFirstChild().getNodeValue());
                            } else if ("foodType".equals(childNode.getNodeName())) { // 食材分类
                                data.setFoodType(childNode.getFirstChild().getNodeValue());
                            }
                        }
                    }
                    list.add(data);
                }
                fileIS.close();
                Collections.reverse(list);
                subscriber.onNext(list);
                subscriber.onCompleted();
            } catch (Exception e) {
                e.printStackTrace();
                logUtil.e(TAG, e.toString());
                logUtil.d(TAG, new File(XML_PATH).delete() + "");
                subscriber.onNext(null);
                subscriber.onCompleted();
            }
        });
    }

    /**
     * 设置过期食材推送标志为 true
     */
    public void setPushFlag(List<FoodDetail> foodDetails) {
        if (!new File(XML_PATH).exists()) return;
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbFactory.newDocumentBuilder();
            Document dom = db.parse(new File(XML_PATH));
            Element root = (Element) dom.getFirstChild();
            NodeList foodList = root.getElementsByTagName("food");

            for (int j = 0; j < foodDetails.size(); j++) {
                // 删除文件中数据
                for (int i = 0; i < foodList.getLength(); i++) {
                    Element userUpdate = (Element) foodList.item(i);
                    // 获取 food 节点下的所有子节点
                    NodeList childNodes = userUpdate.getChildNodes();
                    Node node = childNodes.item(5); // 获取唯一标识
                    Element childNode = (Element) node;
                    if (childNode.getFirstChild().getTextContent().equals(foodDetails.get(j).getAddTime())) {
                        Node node1 = childNodes.item(11);
                        Element childNode1 = (Element) node1;
                        childNode1.setTextContent("true");
                        break;
                    }
                }
            }
            // 保存数据
            TransformerFactory tfFactory = TransformerFactory.newInstance();
            Transformer tf = tfFactory.newTransformer();
            tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");    // 编码方式
            tf.setOutputProperty(OutputKeys.INDENT, "yes");    // 是否缩进
            FileOutputStream fileOutputStream = new FileOutputStream(new File(XML_PATH));
            PrintWriter printWriter = new PrintWriter(fileOutputStream);
            StreamResult streamResult = new StreamResult(printWriter);
            tf.transform(new DOMSource(dom), streamResult);
            fileOutputStream.flush();
            fileOutputStream.getFD().sync();// 掉电保护
            fileOutputStream.close();
        } catch (ParserConfigurationException | TransformerException | IOException | SAXException e) {
            e.printStackTrace();
            logUtil.e(TAG, e.toString());
            logUtil.d(TAG, new File(XML_PATH).delete() + "");
        }
    }

    /**
     * 过滤食材过期集合
     */
    public Observable<List<FoodDetail>> filterOutDate(List<FoodDetail> list) {
        List<FoodDetail> outDateList = new ArrayList<>();
        return Observable.create(subscriber -> {
            long current = System.currentTimeMillis();
            for (int i = 0; i < list.size(); i++) {
                FoodDetail data = list.get(i);
                long deadline = Long.valueOf(data.getDeadLine());
                if (current > deadline && data.getIsPush().equals("false")) {
                    outDateList.add(data);
                    outDatePush(data.getName(), Long.valueOf(data.getAddTime()) / 1000,
                            (int) (Long.valueOf(data.getDeadLine()) / 1000), Integer.valueOf(data.getRoomType()));// 发送过期推送
                }
            }
            subscriber.onNext(outDateList);
            subscriber.onCompleted();
        });
    }

    /**
     * 食材过期推送
     *
     * @param name：食材名称
     * @param startTime：开始时间（秒）
     * @param endTime：过期时间（秒）
     * @param room：0.冷藏室,1.变温室,2.冷冻室,3.冷藏变温区
     */
    private void outDatePush(String name, long startTime, int endTime, int room) {
        MiotRepository.getInstance().sendFoodExpire(name, startTime, endTime, room);
        DeviceInfoMessage deviceInfoMessage = new DeviceInfoMessage();
        String roomStr = "";
        if (room == 0) {
            roomStr = FridgeApplication.getContext().getString(R.string.food_management_room_cold);
        } else if (room == 1) {
            roomStr = FridgeApplication.getContext().getString(R.string.food_management_room_changeable);
        } else if (room == 2) {
            roomStr = FridgeApplication.getContext().getString(R.string.food_management_room_freezing);
        } else if (room == 3) {
            roomStr = FridgeApplication.getContext().getString(R.string.food_management_room_cc);
        }
        deviceInfoMessage.setTitle(String.format(FridgeApplication.getContext().getString(R.string.food_management_food_out_date_desc), roomStr));
        deviceInfoMessage.setContent(name + FridgeApplication.getContext().getString(R.string.food_management_food_out_date_title));
        deviceInfoMessage.setInfoId(InfoManager.getInstance().getLastDeviceInfoId() + 1);
        deviceInfoMessage.setTopic(PushMsg.PUSH_MESSAGE_TYPE_FOOD);
        deviceInfoMessage.setTime(System.currentTimeMillis());
        deviceInfoMessage.setDelete(false);
        deviceInfoMessage.setRead(false);
        InfoManager.getInstance().addDeviceInfoRecord(deviceInfoMessage);

        PushMsg pushMsg = new PushMsg();
        pushMsg.title = name + FridgeApplication.getContext().getString(R.string.food_management_food_out_date_title);
        pushMsg.content = FridgeApplication.getContext().getString(R.string.food_management_food_out_date);
        pushMsg.type = PushMsg.PUSH_MESSAGE_TYPE_FOOD;
        PushManager.getInstance().pushNotificationToStatusBar(pushMsg);
    }
}