package com.viomi.fridge.vertical.administration.view.dialog;

import android.content.Context;
import android.widget.TextView;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseDialog;

/**
 * 清理缓存成功提示
 * Created by nanquan on 2018/1/15.
 */
public class CleanSuccessDialog extends BaseDialog {
    private TextView tvInfo;

    public CleanSuccessDialog(Context context) {
        super(context, R.style.Dialog);
        setContentView(R.layout.dialog_clean_success);
        setCancelable(true);
    }

    @Override
    public void initView() {
        tvInfo = findViewById(R.id.tv_clean_info);
    }

    public void setInfo(String info) {
        tvInfo.setText(info);
    }
}