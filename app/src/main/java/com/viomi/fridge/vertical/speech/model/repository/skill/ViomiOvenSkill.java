package com.viomi.fridge.vertical.speech.model.repository.skill;

import android.content.Context;

import com.miot.common.abstractdevice.AbstractDevice;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.administration.model.entity.QRCodeBase;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.iot.model.repository.OvenRepository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 云米蒸烤箱技能
 * Created by William on 2018/8/30.
 */
public class ViomiOvenSkill {
    private static final String TAG = ViomiOvenSkill.class.getSimpleName();

    public static void handle(Context context, String data, AbstractDevice device) {
        String content;
        QRCodeBase qrCodeBase = (QRCodeBase) ToolUtil.getFileObject(context, AppConstants.USER_INFO_FILE);
        if (qrCodeBase == null) {
            content = "请先登录云米账号";
            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
            return;
        }
        if (device == null) {
            content = "没有连接到蒸烤箱";
            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
            return;
        }

        try {
            JSONObject jsonObject = new JSONObject(data);
            JSONObject semantics = jsonObject.optJSONObject("nlu").optJSONObject("semantics");
            if (semantics == null) return;
            JSONArray slots = semantics.optJSONObject("request").optJSONArray("slots");
            if (slots == null || slots.length() <= 0) return;

            String value = "";
            for (int i = 0; i < slots.length(); i++) {
                JSONObject jsonItem = slots.optJSONObject(i);
                String name = jsonItem.optString("name");
                if (name.equals("intent")) {
                    value = jsonItem.optString("value");
                    break;
                }
            }

            switch (value) {
                case "蒸烤启动":
                    OvenRepository.setPauseStatus(0, device.getDeviceId());
                    content = "正在为你开启" + device.getName();
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "蒸烤暂停":
                    OvenRepository.setPauseStatus(1, device.getDeviceId());
                    content = "正在为你暂停" + device.getName();
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "蒸烤关闭":
                    OvenRepository.setPauseStatus(2, device.getDeviceId());
                    content = "正在为你关闭" + device.getName();
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
            }
        } catch (JSONException e) {
            logUtil.e(TAG, e.toString());
            e.printStackTrace();
        }
    }
}
