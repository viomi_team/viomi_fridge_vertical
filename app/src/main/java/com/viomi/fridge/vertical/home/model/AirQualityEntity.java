package com.viomi.fridge.vertical.home.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.List;

/**
 * 思必驰天气 Json
 * Created by William on 2018/8/13.
 */
public class AirQualityEntity implements Serializable {
    @JSONField(name = "city")
    private AirQualityDetailEntity city;

    public AirQualityDetailEntity getCity() {
        return city;
    }

    public void setCity(AirQualityDetailEntity city) {
        this.city = city;
    }
}
