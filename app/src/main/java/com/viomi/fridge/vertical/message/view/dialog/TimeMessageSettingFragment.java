package com.viomi.fridge.vertical.message.view.dialog;

import android.graphics.Color;
import android.graphics.Typeface;
import android.widget.TextView;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseFragment;
import com.viomi.fridge.vertical.common.scope.ActivityScoped;
import com.viomi.fridge.vertical.message.manager.PushManager;
import com.viomi.widget.wheelview.adapter.ArrayWheelAdapter;
import com.viomi.widget.wheelview.widget.WheelView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 消息设置弹窗 - 设置勿扰时间
 * Created by nanquan on 2018/2/7.
 */
@ActivityScoped
public class TimeMessageSettingFragment extends BaseFragment {
    private PushManager mPushManager;
    private TimeMessageSettingFragment.OnClickListener mOnClickListener;
    private int mCurrentBeginTime;// 当前开始时间
    private int mCurrentEndTime;// 当前结束时间

    @BindView(R.id.tv_message_setting_time)
    TextView mTimeTextView;// 时间

    @BindView(R.id.wv_message_setting_start_hour)
    WheelView<String> mStartTimeWheelView;// 开始时间
    @BindView(R.id.wv_message_setting_end_hour)
    WheelView<String> mEndTimeWheelView;// 结束时间

    @Inject
    public TimeMessageSettingFragment() {

    }

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.dialog_message_setting_time;
    }

    @Override
    protected void initWithOnCreateView() {
        mPushManager = PushManager.getInstance();
        initTypeFace();
        initWheelView();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mPushManager != null) mPushManager = null;
        if (mOnClickListener != null) mOnClickListener = null;
    }

    private void initTypeFace() {
        if (getContext() == null) return;
        mTimeTextView.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/DINCond-Medium.otf"));
    }

    private void initWheelView() {
        // 设置样式
        WheelView.WheelViewStyle style = new WheelView.WheelViewStyle();
        style.backgroundColor = Color.WHITE;
        style.selectedTextColor = getResources().getColor(R.color.color_light_green);
        style.textColor = getResources().getColor(R.color.color_66);
        style.textSize = 50;
        style.selectedTextSize = 70;
        style.itemPadding = 0;
        style.itemMargin = 0;
        style.itemHeight = 100;
        setWheelViewStyle(mStartTimeWheelView, style);
        setWheelViewStyle(mEndTimeWheelView, style);
        // 设置时间
        mStartTimeWheelView.setSelection(mPushManager.getNoPushStartTime());
        mEndTimeWheelView.setSelection(mPushManager.getNoPushEndTime());
        // 监听选中
        mStartTimeWheelView.setOnWheelItemSelectedListener((position, s) -> {
            mCurrentBeginTime = Integer.parseInt(s);
            refreshTitleTime();
        });
        mEndTimeWheelView.setOnWheelItemSelectedListener((position, s) -> {
            mCurrentEndTime = Integer.parseInt(s);
            refreshTitleTime();
        });
    }

    private void setWheelViewStyle(WheelView<String> wheelView, WheelView.WheelViewStyle style) {
        if (getContext() == null) {
            return;
        }
        wheelView.setWheelData(createHours());
        wheelView.setWheelAdapter(new ArrayWheelAdapter<>(getContext()));
        wheelView.setSkin(WheelView.Skin.None);
        wheelView.setLoop(true);
        wheelView.setWheelSize(5);
        wheelView.setStyle(style);
    }

    private void refreshTitleTime() {
        mTimeTextView.setText(String.valueOf(mCurrentBeginTime + ":00-" + mCurrentEndTime + ":00"));
    }

    private ArrayList<String> createHours() {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < 24; i++) {
            if (i < 10) {
                list.add("0" + i);
            } else {
                list.add(String.valueOf(i));
            }
        }
        return list;
    }

    @OnClick(R.id.fl_message_setting_container)
    public void OnBackgroundClick() { // 背景点击
        if (mOnClickListener != null) {
            mOnClickListener.onBackgroundClick();
        }
    }

    @OnClick(R.id.tv_confirm)
    public void onConfirmClick() { // 确定
        if (mOnClickListener != null) {
            mPushManager.setNoPushTime(mCurrentBeginTime, mCurrentEndTime);
            mOnClickListener.onConfirmClick(mCurrentBeginTime, mCurrentEndTime);
        }
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.mOnClickListener = onClickListener;
    }

    public interface OnClickListener {

        void onBackgroundClick();

        void onConfirmClick(int beginHour, int endHour);
    }
}