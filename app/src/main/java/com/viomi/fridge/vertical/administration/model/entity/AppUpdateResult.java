package com.viomi.fridge.vertical.administration.model.entity;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;


/**
 * App 检查更新
 * Created by William on 2018/2/22.
 */
public class AppUpdateResult {
    @JSONField(name = "total")
    private int total;// 总数

    @JSONField(name = "data")
    private List<Data> data;

    public void setTotal(int total) {
        this.total = total;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public int getTotal() {
        return total;
    }

    public List<Data> getData() {
        return data;
    }

    public class Data {
        @JSONField(name = "id")
        private int id;

        @JSONField(name = "packageName")
        private String packageName;// 包名

        @JSONField(name = "code")
        private int code;

        @JSONField(name = "channel")
        private String channel;// 渠道

        @JSONField(name = "detail")
        private String detail;// 更新说明

        @JSONField(name = "url")
        private String url;// Apk 下载链接

        @JSONField(name = "createtime")
        private String createtime;// 创建时间

        public void setId(int id) {
            this.id = id;
        }

        public void setPackageName(String packageName) {
            this.packageName = packageName;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public void setChannel(String channel) {
            this.channel = channel;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public int getId() {
            return id;
        }

        public String getPackageName() {
            return packageName;
        }

        public int getCode() {
            return code;
        }

        public String getChannel() {
            return channel;
        }

        public String getDetail() {
            return detail;
        }

        public String getUrl() {
            return url;
        }

        public String getCreatetime() {
            return createtime;
        }
    }
}