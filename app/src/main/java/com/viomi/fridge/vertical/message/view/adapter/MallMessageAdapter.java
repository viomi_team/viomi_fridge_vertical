package com.viomi.fridge.vertical.message.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.util.ImageLoadUtil;
import com.viomi.fridge.vertical.message.entity.MallMessage;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * 活动消息列表适配器
 * Created by nanquan on 2018/2/6.
 */
public class MallMessageAdapter extends RecyclerView.Adapter<MallMessageAdapter.MessageViewHolder> {
    private Context mContext;
    private List<MallMessage> mList;
    private SimpleDateFormat mSimpleDateFormat;
    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {

        void onItemClick(int position);

        void onItemDeleteClick(int position);
    }

    public MallMessageAdapter(Context context, List<MallMessage> list) {
        this.mContext = context;
        this.mList = list;
        mSimpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm", Locale.getDefault());
    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(mContext).inflate(R.layout.item_message_activity, parent, false);
        return new MessageViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position) {
        MallMessage message = mList.get(position);
        // 线条
        holder.vTopLine.setVisibility(position == 0 ? View.INVISIBLE : View.VISIBLE);
        holder.vBottomLine.setVisibility(position == mList.size() - 1 ? View.INVISIBLE : View.VISIBLE);
        // 图片
        if (!TextUtils.isEmpty(message.getImgurl())) {
            ImageLoadUtil.getInstance().loadUrl(message.getImgurl(), holder.ivPic);
            holder.ivPic.setVisibility(View.VISIBLE);
        } else {
            holder.ivPic.setVisibility(View.GONE);
        }
        // 标题
        if (!TextUtils.isEmpty(message.getTitle())) {
            holder.tvTitle.setText(message.getTitle());
            holder.tvTitle.setVisibility(View.VISIBLE);
        } else {
            holder.tvTitle.setVisibility(View.GONE);
        }
        // 内容
        if (!TextUtils.isEmpty(message.getContent())) {
            holder.tvContent.setText(message.getContent());
            holder.tvContent.setVisibility(View.VISIBLE);
        } else {
            holder.tvContent.setVisibility(View.GONE);
        }
        // 时间
        if (message.getTime() > 0) {
            holder.tvDate.setText(mSimpleDateFormat.format(new Date(message.getTime())));
            holder.tvDate.setVisibility(View.VISIBLE);
        } else {
            holder.tvDate.setVisibility(View.GONE);
        }
        // 点击事件
        if (mOnItemClickListener != null) {
            holder.llContent.setOnClickListener(v -> mOnItemClickListener.onItemClick(position));
            holder.ivDelete.setOnClickListener(v -> mOnItemClickListener.onItemDeleteClick(position));
        }

        if (message.getAccessurl() == null) {
            holder.msgDetailView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    class MessageViewHolder extends RecyclerView.ViewHolder {
        View vTopLine;
        View vBottomLine;
        LinearLayout llContent;
        ImageView ivPic;
        TextView tvTitle;
        TextView tvContent;
        ImageView ivDelete;
        TextView tvDate;
        TextView msgDetailView;

        MessageViewHolder(View itemView) {
            super(itemView);
            vTopLine = itemView.findViewById(R.id.v_message_line_top);
            vBottomLine = itemView.findViewById(R.id.v_message_line_bottom);
            llContent = itemView.findViewById(R.id.ll_message_content);
            ivPic = itemView.findViewById(R.id.iv_message_pic);
            tvTitle = itemView.findViewById(R.id.tv_message_title);
            tvContent = itemView.findViewById(R.id.tv_message_content);
            ivDelete = itemView.findViewById(R.id.iv_message_delete);
            msgDetailView = itemView.findViewById(R.id.message_detail);
            tvDate = itemView.findViewById(R.id.tv_message_date);
        }
    }
}