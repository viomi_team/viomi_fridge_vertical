package com.viomi.fridge.vertical.speech.model.repository.skill;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.cookbook.activity.CookDetailActivity;
import com.viomi.fridge.vertical.cookbook.model.CookMenuDetail;
import com.viomi.fridge.vertical.cookbook.model.CookMenuMethod;
import com.viomi.fridge.vertical.cookbook.model.CookMenuRecipe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * AITEK 菜谱技能
 * Created by William on 2018/8/17.
 */
public class CookBookSkill {
    private static final String TAG = CookBookSkill.class.getSimpleName();

    public static void handle(Context context, String data) {
        try {
            JSONObject jsonObject = new JSONObject(data);
            JSONObject extra = jsonObject.optJSONObject("extra");
            if (extra == null) return;
            JSONArray list = extra.optJSONArray("list");
            if (list == null || list.length() <= 0) return;
            int index = 0;// new Random().nextInt(list.length())
            JSONObject itemJson = list.optJSONObject(index);
            if (itemJson == null) return;
            CookMenuDetail detail = new CookMenuDetail();
            CookMenuRecipe recipe = new CookMenuRecipe();
            recipe.setImg(itemJson.optString("pic"));// 图片
            detail.setName(itemJson.optString("name"));// 菜名
            recipe.setSumary(itemJson.optString("content"));// 介绍
            JSONArray jsonArray = itemJson.optJSONArray("material");
            StringBuilder stringBuilder = new StringBuilder();
            if (jsonArray != null) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject material = jsonArray.optJSONObject(i);
                    stringBuilder.append(material.optString("amount")).append(material.optString("mname"));
                    if (i != jsonArray.length() - 1) stringBuilder.append("，");
                }
            }
            recipe.setIngredients2(stringBuilder.toString());// 用料
            recipe.setTitle(FridgeApplication.getContext().getResources().getString(R.string.cookbook_cooking_step));// 步骤名称
            recipe.setList(new ArrayList<>());
            JSONArray process = itemJson.optJSONArray("process");
            if (process != null) {
                for (int i = 0; i < process.length(); i++) {
                    JSONObject jsonProcess = process.optJSONObject(i);
                    CookMenuMethod cookMenuMethod = new CookMenuMethod();
                    cookMenuMethod.setImg(jsonProcess.optString("pic"));
                    cookMenuMethod.setStep(jsonProcess.optString("pcontent"));
                    cookMenuMethod.setPosition(i + 1);
                    recipe.getList().add(cookMenuMethod);
                }
            }
            detail.setRecipe(recipe);
            Intent intent = new Intent(context, CookDetailActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("cookMenuDetail", detail);
            intent.putExtras(bundle);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (JSONException e) {
            logUtil.e(TAG, e.toString());
            e.printStackTrace();
        }
    }
}
