package com.viomi.fridge.vertical.administration.view.dialog;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.view.adapter.WifiAdapter;
import com.viomi.fridge.vertical.common.base.CommonDialog;
import com.viomi.widget.switchbutton.SwitchButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

import static android.content.Context.WIFI_SERVICE;
import static com.inuker.bluetooth.library.utils.BluetoothUtils.registerReceiver;
import static com.inuker.bluetooth.library.utils.BluetoothUtils.unregisterReceiver;

/**
 * WIFI 设置页面
 * Created by nanquan on 2018/1/9.
 */
public class WifiSettingDialog extends CommonDialog {
    private WifiAdapter mAdapter;
    private WifiManager mWifiManager;
    private List<ScanResult> scanList;
    private long lastNotifyTime;
    private static final int MSG_WHAT_CLOSE_WIFI = 100;

    @BindView(R.id.sb_wifi)
    SwitchButton sbWifi;

    @BindView(R.id.rv_wifi)
    RecyclerView rvWifi;

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.dialog_wifi_setting;
    }

    @Override
    protected void initWithOnCreateDialog(View view) {
        mWifiManager = (WifiManager) FridgeApplication.getContext().getApplicationContext().getSystemService(WIFI_SERVICE);
        initView();
    }

    @Override
    public void onStart() {
        super.onStart();
        // 设置背景
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setBackgroundDrawableResource(android.R.color.transparent);
            WindowManager.LayoutParams windowParams = window.getAttributes();
            windowParams.dimAmount = 0.5f;
            window.setAttributes(windowParams);
        }
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == MSG_WHAT_CLOSE_WIFI) {
                scanList.clear();
                mAdapter.notifyDataSetChanged();
            }
        }
    };

    private void initView() {
        if (mWifiManager == null) {
            return;
        }
        scanList = new ArrayList<>();
        sbWifi.setOn(mWifiManager.isWifiEnabled());
        sbWifi.setOnSwitchStateChangeListener(isOn -> {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    mWifiManager.setWifiEnabled(isOn);
                    if (!isOn) {
                        if (mHandler != null) {
                            mHandler.sendEmptyMessage(MSG_WHAT_CLOSE_WIFI);
                        }
                    }
                }
            }).start();

        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity()) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        layoutManager.setAutoMeasureEnabled(true);
        rvWifi.setLayoutManager(layoutManager);
        mAdapter = new WifiAdapter(getActivity(), scanList, mWifiManager);
        rvWifi.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(this::launchWifiConnecter);
    }

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        registerReceiver(mReceiver1, filter);

        IntentFilter filter2 = new IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        registerReceiver(mReceiver2, filter2);

        mWifiManager.startScan();
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(mReceiver1);
        unregisterReceiver(mReceiver2);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mAdapter != null) mAdapter = null;
        if (mWifiManager != null) mWifiManager = null;
        if (scanList != null) {
            scanList.clear();
            scanList = null;
        }
    }

    private BroadcastReceiver mReceiver1 = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (WifiManager.SCAN_RESULTS_AVAILABLE_ACTION.equals(action)) {
                // 避免频繁刷新
                long currentTime = System.currentTimeMillis();
                if (currentTime - lastNotifyTime > 5000) {
                    lastNotifyTime = currentTime;
                    List<ScanResult> sResults = mWifiManager.getScanResults();
                    Map<String, ScanResult> sMap = new HashMap<>();
                    for (ScanResult result : sResults) {
                        if (result.SSID != null && !result.SSID.isEmpty()) {
                            // 去重
                            if (sMap.containsKey(result.SSID)) {
                                WifiInfo info = mWifiManager.getConnectionInfo();
                                boolean isCurrentNetwork_WifiInfo = info != null &&
                                        android.text.TextUtils.equals(info.getSSID(), "\"" + result.SSID + "\"") &&
                                        android.text.TextUtils.equals(info.getBSSID(), result.BSSID);
                                // 不覆盖已连接
                                if (!(isWiFiActive(context) && isCurrentNetwork_WifiInfo)) {
                                    continue;
                                }
                            }
                            sMap.put(result.SSID, result);
                        }
                    }
                    scanList.clear();
                    scanList.addAll(sMap.values());
                    // 已连接和正在连接放在最前
                    for (int i = 0; i < scanList.size(); i++) {
                        if (mWifiManager.getConnectionInfo() != null &&
                                android.text.TextUtils.equals(mWifiManager.getConnectionInfo().getSSID(), "\"" + scanList.get(i).SSID + "\"") &&
                                android.text.TextUtils.equals(mWifiManager.getConnectionInfo().getBSSID(), scanList.get(i).BSSID)) {
                            if (i != 0) Collections.swap(scanList, i, 0);
                            break;
                        }
                    }
                    if (sbWifi.isOn()) mAdapter.notifyDataSetChanged();
                    mWifiManager.startScan();
                }
            }
        }
    };

    private BroadcastReceiver mReceiver2 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (WifiManager.NETWORK_STATE_CHANGED_ACTION.equals(intent.getAction())) {
                if (mAdapter != null && sbWifi.isOn()) {
                    mAdapter.notifyDataSetChanged();
                }
            }
        }
    };

    private void launchWifiConnecter(final ScanResult hotspot) {
        final Intent intent = new Intent("com.farproc.wifi.connecter.action.CONNECT_OR_EDIT");
        intent.putExtra("EXTRA_HOTSPOT", hotspot);
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException ignored) {
        }
    }

    public static boolean isWiFiActive(Context context) {
        WifiManager mWifiManager = (WifiManager) context.getApplicationContext()
                .getSystemService(WIFI_SERVICE);
        if (mWifiManager == null) {
            return false;
        }
        WifiInfo wifiInfo = mWifiManager.getConnectionInfo();
        int ipAddress = wifiInfo == null ? 0 : wifiInfo.getIpAddress();
        return mWifiManager.isWifiEnabled() && ipAddress != 0;
    }
}