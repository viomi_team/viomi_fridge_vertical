package com.viomi.fridge.vertical.cookbook.view.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.viomi.fridge.vertical.cookbook.model.CookMenuCategory;
import com.viomi.fridge.vertical.cookbook.view.fragment.CookCategoryFragment;

import java.util.List;

/**
 * 菜品 ViewPager 适配器
 * Created by nanquan on 2018/2/27.
 */
public class CookCategoryPagerAdapter extends FragmentPagerAdapter {
    private List<CookMenuCategory> categoryList;

    public CookCategoryPagerAdapter(FragmentManager fm, List<CookMenuCategory> categoryList) {
        super(fm);
        this.categoryList = categoryList;
    }

    @Override
    public Fragment getItem(int position) {
        return CookCategoryFragment.getInstance(categoryList.get(position).getCategoryInfo().getCtgId());
    }

    @Override
    public int getCount() {
        return categoryList.size() - 1;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return categoryList.get(position).getCategoryInfo().getName();
    }
}