package com.viomi.fridge.vertical.speech.model.entity;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;

/**
 * 思必驰媒体资源 Json
 * Created by William on 2018/8/14.
 */
public class MediaContentEntity implements Serializable {
    @JSONField(name = "linkUrl")
    private String linkUrl;
    @JSONField(name = "title")
    private String title;
    @JSONField(name = "subTitle")
    private String subTitle;
    @JSONField(name = "imageUrl")
    private String imageUrl;
    @JSONField(name = "label")
    private String label;

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
