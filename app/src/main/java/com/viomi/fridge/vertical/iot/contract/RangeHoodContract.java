package com.viomi.fridge.vertical.iot.contract;

import com.viomi.fridge.vertical.common.callback.BasePresenter;
import com.viomi.fridge.vertical.iot.model.http.entity.RangeHoodData;
import com.viomi.fridge.vertical.iot.model.http.entity.RangeHoodProp;

/**
 * 烟机 Contract
 * Created by William on 2018/2/21.
 */
public interface RangeHoodContract {
    interface View {
        void refreshUi(RangeHoodProp prop);// 刷新 Ui

        void refreshUi(RangeHoodData data);// 刷新统计数据

        void setIsSetting();
    }

    interface Presenter extends BasePresenter<View> {
        void getProp(String did);// GetProp 请求

        void getUserData(String did);// 获取统计数据

        void setPower(String param, String did);// 电源开关设置

        void setWind(String param, String did);// 风速设置

        void setLight(String param, String did);// 灯光设置
    }
}
