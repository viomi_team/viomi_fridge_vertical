package com.viomi.fridge.vertical.administration.view.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.activity.FileListManageActivity;
import com.viomi.fridge.vertical.administration.model.entity.FileInfo;
import com.viomi.fridge.vertical.administration.view.dialog.CleanSuccessDialog;
import com.viomi.fridge.vertical.administration.view.dialog.ClearingDialog;
import com.viomi.fridge.vertical.common.base.BaseFragment;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.scope.ActivityScoped;
import com.viomi.fridge.vertical.common.util.FileUtil;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.SDCardUtils;
import com.viomi.fridge.vertical.common.util.ToastUtil;
import com.viomi.fridge.vertical.common.util.logUtil;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import rx.Observable;
import rx.Subscription;

import static com.inuker.bluetooth.library.utils.BluetoothUtils.sendBroadcast;

/**
 * 文件管理 Fragment
 * Created by nanquan on 2018/2/5.
 */
@ActivityScoped
public class FileFragment extends BaseFragment {
    private static final String TAG = FileFragment.class.getSimpleName();
    private static final String TENCENT_PATH = "/storage/sdcard0/Android/data/com.tencent.qqlive/files"; // 腾讯视频缓存目录
    private static final String XIAMI_PATH = "/storage/sdcard0/Android/data/fm.xiami.main/files"; // 虾米音乐缓存目录
    private Subscription mSubscription;
    private ClearingDialog mClearingDialog;// 正在清理 Dialog
    private CleanSuccessDialog mCleanSuccessDialog;// 清理完成 Dialog
    private long mVideoSize;// 视频占用大小
    private long mPictureSize;// 图片占用大小
    private long mMusicSize;// 音乐占用大小
    private long mFreeMemory;// 剩余空间
    private boolean mIsCleanFinish = false;// 是否清理完成

    @BindView(R.id.ll_manage_memory)
    LinearLayout mMemoryLinearLayout;// 空间布局

    @BindView(R.id.tv_manage_free_memory)
    TextView mFreeMemoryTextView;// 剩余空间
    @BindView(R.id.tv_manage_all_memory)
    TextView mAllMemoryTextView;// 总空间
    @BindView(R.id.tv_manage_video_size)
    TextView mVideoTextView;// 视频
    @BindView(R.id.tv_manage_picture_size)
    TextView mPictureTextView;// 图片
    @BindView(R.id.tv_manage_music_size)
    TextView mMusicTextView;// 音乐

    @Inject
    public FileFragment() {

    }

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.fragment_file;

        mSubscription = RxBus.getInstance().subscribe(busEvent -> {
            switch (busEvent.getMsgId()) {
                case BusEvent.MSG_FILE_DELETE:
                    initSize();
                    break;
            }
        });
    }

    @Override
    protected void initWithOnCreateView() {
        initSize();
        initDialog();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
        if (mClearingDialog != null && mClearingDialog.isShowing()) {
            mClearingDialog.cancel();
            mClearingDialog = null;
        }
        if (mCleanSuccessDialog != null && mCleanSuccessDialog.isShowing()) {
            mCleanSuccessDialog.cancel();
            mCleanSuccessDialog = null;
        }
    }

    public void initSize() {
        long totalMemory = SDCardUtils.getSDCardTotalSize();// 内存卡总空间
        mFreeMemory = SDCardUtils.getSDCardFreeSizeBit();// 内存卡剩余空间
        if (mFreeMemory / 1024 / 1024 > totalMemory * 0.6) {
            mMemoryLinearLayout.setBackgroundResource(R.drawable.bg_manage_gradient_green);
        } else if (mFreeMemory / 1024 / 1024 > totalMemory * 0.2) {
            mMemoryLinearLayout.setBackgroundResource(R.drawable.bg_manage_gradient_yellow);
        } else {
            mMemoryLinearLayout.setBackgroundResource(R.drawable.bg_manage_gradient_red);
        }
        mFreeMemoryTextView.setText(SDCardUtils.bit2KbMGB(mFreeMemory));
        mAllMemoryTextView.setText(String.valueOf(FridgeApplication.getContext().getResources().getString(R.string.management_clear_file_all) + SDCardUtils.bit2KbMGB(totalMemory * 1024 * 1024)));

        mVideoSize = FileUtil.queryFileSize(FridgeApplication.getContext(), FileUtil.FileType.FILE_VIDEO);
        mPictureSize = FileUtil.queryFileSize(FridgeApplication.getContext(), FileUtil.FileType.FILE_PIC);
        mMusicSize = FileUtil.queryFileSize(FridgeApplication.getContext(), FileUtil.FileType.FILE_MUSIC);
        mVideoTextView.setText(SDCardUtils.bit2KbMGB(mVideoSize));
        mPictureTextView.setText(SDCardUtils.bit2KbMGB(mPictureSize));
        mMusicTextView.setText(SDCardUtils.bit2KbMGB(mMusicSize));
    }

    private void initDialog() {
        mClearingDialog = new ClearingDialog(getActivity());
        mCleanSuccessDialog = new CleanSuccessDialog(getActivity());
    }

    @OnClick(R.id.ll_manage_video)
    public void onVideoClick() { // 视频
        if (mVideoSize > 0 && getActivity() != null) {
            Intent intent = new Intent(getActivity(), FileListManageActivity.class);
            intent.putExtra("fileType", FileUtil.FileType.FILE_VIDEO);
            getActivity().startActivityForResult(intent, 0);
        }
    }

    @OnClick(R.id.ll_manage_picture)
    public void onPictureClick() { // 图片
        if (mPictureSize > 0 && getActivity() != null) {
            Intent intent = new Intent(getActivity(), FileListManageActivity.class);
            intent.putExtra("fileType", FileUtil.FileType.FILE_PIC);
            getActivity().startActivityForResult(intent, 0);
        }
    }

    @OnClick(R.id.ll_manage_music)
    public void onMusicClick() { // 音乐
        if (mMusicSize > 0 && getActivity() != null) {
            Intent intent = new Intent(getActivity(), FileListManageActivity.class);
            intent.putExtra("fileType", FileUtil.FileType.FILE_MUSIC);
            getActivity().startActivityForResult(intent, 0);
        }
    }

    @OnClick(R.id.tv_manage_clear_cache)
    public void onClearCacheClick() { // 清理缓存
        if (mIsCleanFinish) {
            ToastUtil.showCenter(FridgeApplication.getContext(), FridgeApplication.getContext().getResources().getString(R.string.management_clear_no_content_tip));
            return;
        }
        if (mClearingDialog != null) mClearingDialog.show();
        Observable.create((Observable.OnSubscribe<Boolean>) subscriber -> {
            // 清除垃圾文件
            List<FileInfo> mTempFiles = FileUtil.getFileInfoList(FridgeApplication.getContext(), FileUtil.FileType.FILE_GARBAGE);
            for (FileInfo temp : mTempFiles) {
                FileUtil.deleteAll(new File(temp.getAbsolutePath()));
            }
            // 清除缓存
            FileUtil.cleanAppsCache(FridgeApplication.getContext());
            // 清除腾讯缓存视频和虾米的缓存
            FileUtil.deleteChildFile(new File(TENCENT_PATH));
            FileUtil.deleteChildFile(new File(XIAMI_PATH));
            // 通知系统重新进行媒体扫描
            Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            intent.setData(Uri.fromFile(new File(Environment.getExternalStorageDirectory().getAbsolutePath())));
            sendBroadcast(intent);
            subscriber.onNext(true);
        })
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(isDone -> {
                    long preFreeMemory = mFreeMemory;
                    initSize();
                    if (mFreeMemory - preFreeMemory > 1024) {
                        if (mClearingDialog != null) mClearingDialog.cancel();
                        mCleanSuccessDialog.show();
                        mCleanSuccessDialog.setInfo(String.format(FridgeApplication.getContext().getResources().getString(R.string.management_clear_finish_tip), SDCardUtils.bit2KbMGB(mFreeMemory - preFreeMemory)));
                        mIsCleanFinish = true;
                    } else {
                        if (mClearingDialog != null) mClearingDialog.cancel();
                        ToastUtil.showCenter(FridgeApplication.getContext(), FridgeApplication.getContext().getResources().getString(R.string.management_clear_no_content_tip));
                    }
                }, throwable -> {
                    if (mClearingDialog != null) mClearingDialog.cancel();
                    logUtil.e(TAG, throwable.getMessage());
                });
    }
}
