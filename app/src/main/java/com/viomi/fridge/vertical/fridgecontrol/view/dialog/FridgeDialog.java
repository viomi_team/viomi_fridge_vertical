package com.viomi.fridge.vertical.fridgecontrol.view.dialog;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.model.preference.ManagePreference;
import com.viomi.fridge.vertical.common.base.CommonDialog;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.ToastUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.fridgecontrol.contract.FridgeContract;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.entity.ChangeableScene;
import com.viomi.fridge.vertical.fridgecontrol.model.repository.FridgeRepository;
import com.viomi.fridge.vertical.fridgecontrol.model.repository.MiotRepository;
import com.viomi.fridge.vertical.fridgecontrol.model.serial.entity.DeviceError;
import com.viomi.fridge.vertical.fridgecontrol.model.serial.entity.DeviceParams;
import com.viomi.fridge.vertical.fridgecontrol.model.serial.manager.SerialManager;
import com.viomi.fridge.vertical.fridgecontrol.presenter.FridgePresenter;
import com.viomi.fridge.vertical.fridgecontrol.view.RoomSettingView;
import com.viomi.fridge.vertical.fridgecontrol.view.RoomView;
import com.viomi.fridge.vertical.fridgecontrol.view.adapter.ErrorAdapter;
import com.viomi.fridge.vertical.fridgecontrol.view.adapter.SceneAdapter;
import com.viomi.fridge.vertical.web.activity.BrowserActivity;
import com.viomi.widget.ProgressWheel;
import com.viomi.widget.dialog.BaseAlertDialog;
import com.viomi.widget.snowingview.SnowingSurfaceView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;
import rx.Observable;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * 冰箱相关控制 Dialog
 * Created by William on 2018/2/1.
 */
public class FridgeDialog extends CommonDialog implements FridgeContract.View, RoomSettingView.OnSceneMoreClickListener,
        RoomSettingView.OnSceneSetListener, View.OnClickListener {
    private static final String TAG = FridgeDialog.class.getSimpleName();
    private int mMode = AppConstants.MODE_NULL;// 工作模式
    private String mModel;// 设备 Model
    private int mMinute = 0;// 分钟计时
    private boolean mIsSetting = false, mIsPlay = false;// 在设置时停止刷新 Ui，动画是否播放
    private List<ChangeableScene> mAllList, mDisplayList, mBackupList;// 所有，显示场景，备用场景集合
    private SceneAdapter mDisplayAdapter, mBackupAdapter;// 显示场景，备选场景适配器
    private CompositeSubscription mCompositeSubscription;// 统一管理消息订阅
    private Subscription mSubscription;// 一键净化动画
    private RoomView mColdRoomView;// 冷藏室
    private RoomView mCCRoomView;// 冷藏变温区
    private RoomView mChangeableRoomView;// 变温室
    private RoomView mFreezingRoomView;// 冷冻室
    private TextView mCCTempTextView;// 冷藏变温区温度
    private TextView mFruitTextView;// 鲜果
    private TextView mFreshTextView;// 0度保鲜
    private TextView mIcedTextView;// 冰镇
    private FridgeContract.Presenter mPresenter;

    @BindView(R.id.fridge_setting_line1)
    View mLineView1;// 分割线
    @BindView(R.id.fridge_setting_line2)
    View mLineView2;// 分割线
    @BindView(R.id.fridge_setting_line_center)
    View mLineViewCenter;// 分割线

    @BindView(R.id.fridge_setting_cold_changeable)
    ViewStub mViewStub;// 冷藏变温区

    @BindView(R.id.fridge_filter_layout)
    LinearLayout mFilterLayout;// 滤芯状态 icon 布局
    @BindView(R.id.fridge_filter_detail_layout)
    LinearLayout mFilterLinearLayout;// 滤芯状态布局
    @BindView(R.id.fridge_quick_cold)
    LinearLayout mQuickColdLinearLayout;// 速冷布局
    @BindView(R.id.fridge_clean)
    LinearLayout mCleanLinearLayout;// 一键净化布局

    @BindView(R.id.fridge_mode)
    ImageView mBigImageView;// 中间大 icon
    @BindView(R.id.fridge_wave_1)
    ImageView mWave1ImageView;// 一键净化动画
    @BindView(R.id.fridge_wave_2)
    ImageView mWave2ImageView;// 一键净化动画
    @BindView(R.id.fridge_wave_3)
    ImageView mWave3ImageView;// 一键净化动画

    @BindView(R.id.fridge_setting_cold)
    RoomSettingView mColdRoomSettingView;// 冷藏室设置
    @BindView(R.id.fridge_setting_changeable)
    RoomSettingView mChangeableRoomSettingView;// 变温室设置
    @BindView(R.id.fridge_setting_freezing)
    RoomSettingView mFreezingRoomSettingView;// 冷冻室设置

    @BindView(R.id.fridge_mode_intelligence_icon)
    ImageView mIntelligenceImageView;// 智能模式图标
    @BindView(R.id.fridge_mode_holiday_icon)
    ImageView mHolidayImageView;// 假日模式图标
    @BindView(R.id.fridge_quick_cold_icon)
    ImageView mQuickColdImageView;// 速冷图标
    @BindView(R.id.fridge_quick_freezing_icon)
    ImageView mQuickFreezingImageView;// 速冻图标
    @BindView(R.id.fridge_clean_icon)
    ImageView mCleanImageView;// 一键净化

    @BindView(R.id.fridge_clean_text)
    TextView mCleanTextView;// 一键净化文字
    @BindView(R.id.fridge_tip)
    TextView mTipTextView;// 提示文字
    @BindView(R.id.fridge_error)
    TextView mErrorTextView;// 故障
    @BindView(R.id.fridge_filter_least)
    TextView mFilterTextView;// 详情滤芯剩余
    @BindView(R.id.fridge_filter_buy)
    TextView mBuyTextView;// 购买滤芯

    @BindView(R.id.fridge_right_bg_layout)
    FrameLayout mFrameLayout;// 背景布局

    @BindView(R.id.fridge_scene_setting)
    RelativeLayout mSceneRelativeLayout;// 场景选择

    @BindView(R.id.fridge_filter_progress)
    ProgressWheel mProgressWheel;// 滤芯剩余

    @BindView(R.id.fridge_setting_layout)
    RelativeLayout mControlRelativeLayout;// 相关控制布局

    @BindView(R.id.fridge_snow_view)
    SnowingSurfaceView mSnowingSurfaceView;// 下雪 View

    @BindView(R.id.fridge_scene_display_list)
    RecyclerView mDisplayRecyclerView;// 场景显示区
    @BindView(R.id.fridge_scene_backup_list)
    RecyclerView mBackupRecyclerView;// 场景备选区
    @BindView(R.id.fridge_error_list)
    RecyclerView mErrorRecyclerView;// 故障列表

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.dialog_fridge;
    }

    @Override
    protected void initWithOnCreateDialog(View view) {
        mModel = FridgePreference.getInstance().getModel();// 冰箱 model
        mPresenter = new FridgePresenter(FridgeApplication.getContext());
        RxBus.getInstance().post(BusEvent.MSG_ALBUM_STOP_STROLL);// 首页相册停止滚动
        mIsSetting = false;// 复位
        // 根据 Model 设置 Ui
        ViewStub viewStub;
        View layoutView = null;
        switch (mModel) {
            case AppConstants.MODEL_X2: // 双鹿 446
                viewStub = view.findViewById(R.id.fridge_vertical_layout);
                layoutView = viewStub.inflate();
                mColdRoomView = layoutView.findViewById(R.id.fridge_vertical_room_cold);
                mChangeableRoomView = layoutView.findViewById(R.id.fridge_vertical_room_changeable);
                mFreezingRoomView = layoutView.findViewById(R.id.fridge_vertical_room_freezing);
                mLineView1.setVisibility(View.GONE);
                mLineView2.setVisibility(View.GONE);
                mLineViewCenter.setVisibility(View.VISIBLE);
                mChangeableRoomSettingView.setVisibility(View.INVISIBLE);
                mCleanLinearLayout.setVisibility(View.GONE);
                break;
            case AppConstants.MODEL_X3: // 美菱 462
                viewStub = view.findViewById(R.id.fridge_vertical_layout);
                layoutView = viewStub.inflate();
                mColdRoomView = layoutView.findViewById(R.id.fridge_vertical_room_cold);
                mChangeableRoomView = layoutView.findViewById(R.id.fridge_vertical_room_changeable);
                mFreezingRoomView = layoutView.findViewById(R.id.fridge_vertical_room_freezing);
                mLineView1.setVisibility(View.GONE);
                mLineView2.setVisibility(View.GONE);
                mLineViewCenter.setVisibility(View.VISIBLE);
                mChangeableRoomSettingView.setVisibility(View.INVISIBLE);
                mCleanLinearLayout.setVisibility(View.GONE);
                break;
            case AppConstants.MODEL_X4: // 雪祺 450
            case AppConstants.MODEL_JD: // 雪祺 450 京东定制
                viewStub = view.findViewById(R.id.fridge_opposite_layout);
                layoutView = viewStub.inflate();
                mColdRoomView = layoutView.findViewById(R.id.fridge_opposite_room_cold);
                mFreezingRoomView = layoutView.findViewById(R.id.fridge_opposite_room_freezing);
                mLineView1.setVisibility(View.GONE);
                mLineView2.setVisibility(View.GONE);
                mLineViewCenter.setVisibility(View.VISIBLE);
                mChangeableRoomSettingView.setVisibility(View.INVISIBLE);
                mCleanLinearLayout.setVisibility(View.GONE);
                mColdRoomView.setOppositeLayout();
                mFreezingRoomView.setOppositeLayout();
                break;
            case AppConstants.MODEL_X5: // 美菱 521
                viewStub = view.findViewById(R.id.fridge_cross_layout);
                View ccView = mViewStub.inflate();
                mCCTempTextView = ccView.findViewById(R.id.room_cc_setting_temp);
                mFruitTextView = ccView.findViewById(R.id.room_cc_fresh_fruit);
                mFreshTextView = ccView.findViewById(R.id.room_cc_retain_freshness);
                mIcedTextView = ccView.findViewById(R.id.room_cc_iced);
                layoutView = viewStub.inflate();
                mColdRoomView = layoutView.findViewById(R.id.fridge_cross_room_cold);
                mCCRoomView = layoutView.findViewById(R.id.fridge_cross_room_cold_changeable);
                mChangeableRoomView = layoutView.findViewById(R.id.fridge_cross_room_changeable);
                mFreezingRoomView = layoutView.findViewById(R.id.fridge_cross_room_freezing);
                mColdRoomView.setIconLayout();
                mCCRoomView.setIconLayout();
                mChangeableRoomView.setIconLayout();
                mFreezingRoomView.setIconLayout();
                mCleanLinearLayout.setVisibility(View.GONE);
                mQuickColdLinearLayout.setVisibility(View.GONE);
                mFruitTextView.setOnClickListener(v -> { // 鲜果
                    if (mPresenter.isInspectionRunning()
                        //   || mPresenter.isCommunicationError()
                            )
                        return;
                    mFruitTextView.setSelected(!mFruitTextView.isSelected());
                    if (mFruitTextView.isSelected()) {
                        mCCRoomView.setTempIconVisible(2);
                        mFreshTextView.setSelected(false);
                        mIcedTextView.setSelected(false);
                    }
                    mPresenter.setFreshFruit(mFruitTextView.isSelected());
                });
                mFreshTextView.setOnClickListener(v -> { // 0 度保鲜
                    if (mPresenter.isInspectionRunning()
                        // || mPresenter.isCommunicationError()
                            )
                        return;
                    mFreshTextView.setSelected(!mFreshTextView.isSelected());
                    if (mFreshTextView.isSelected()) {
                        mCCRoomView.setTempIconVisible(2);
                        mFruitTextView.setSelected(false);
                        mIcedTextView.setSelected(false);
                    }
                    mPresenter.setRetainFresh(mFreshTextView.isSelected());
                });
                mIcedTextView.setOnClickListener(v -> { // 冰镇
                    if (mPresenter.isInspectionRunning()
                        //     || mPresenter.isCommunicationError()
                            )
                        return;
                    mIcedTextView.setSelected(!mIcedTextView.isSelected());
                    if (mIcedTextView.isSelected()) {
                        mCCRoomView.setTempIconVisible(2);
                        mFruitTextView.setSelected(false);
                        mFreshTextView.setSelected(false);
                    }
                    mPresenter.setIced(mIcedTextView.isSelected());
                });
                mCCTempTextView.setTypeface(Typeface.createFromAsset(FridgeApplication.getContext().getAssets(), "fonts/DINCond-Medium.otf"));
                initChangeableScene();
                break;
        }
        if (layoutView != null) {
            layoutView.setVisibility(View.VISIBLE);
            layoutView.setOnClickListener(this);
        }

        mColdRoomSettingView.setOnSwitchChangedListener(enable -> { // 设置冷藏室开关
            if (!mPresenter.isInspectionRunning()
                //&& !mPresenter.isCommunicationError()
                    ) {
                mPresenter.setColdSwitch(enable);
            }
        });
        mChangeableRoomSettingView.setOnSwitchChangedListener(enable -> { // 设置变温室开关
            if (!mPresenter.isInspectionRunning()
                //  && !mPresenter.isCommunicationError()
                    )
                mPresenter.setChangeableSwitch(enable);
        });
        mColdRoomSettingView.setOnTempChangedListener(temp -> { // 设置冷藏室温度
            if (!mPresenter.isInspectionRunning()
                // && !mPresenter.isCommunicationError()
                    ) {
                mPresenter.setColdTemp(temp);
                mColdRoomView.setTempIconVisible(1);
                mFreezingRoomView.setModeIconGone();
            }
        });
        mChangeableRoomSettingView.setOnTempChangedListener(temp -> { // 设置变温室温度
            if (!mPresenter.isInspectionRunning()
                //&& !mPresenter.isCommunicationError()
                    ) {
                mPresenter.setChangeableTemp(temp);
                mChangeableRoomView.setTempIconVisible(2);
            }
        });
        mFreezingRoomSettingView.setOnTempChangedListener(temp -> { // 设置冷冻室温度
            if (!mPresenter.isInspectionRunning()
                // && !mPresenter.isCommunicationError()
                    ) {
                mPresenter.setFreezingTemp(temp);
                mFreezingRoomView.setTempIconVisible(2);
                mColdRoomView.setModeIconGone();
            }
        });
        mChangeableRoomSettingView.setOnSceneMoreClickListener(this);
        mChangeableRoomSettingView.setOnSceneSetListener(this);
        mDisplayRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 6));
        mBackupRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 6));
        mErrorRecyclerView.setLayoutManager(new LinearLayoutManager(FridgeApplication.getContext()));
        mDisplayRecyclerView.setAdapter(mDisplayAdapter);
        mBackupRecyclerView.setAdapter(mBackupAdapter);
        refreshFilter();// 读取滤芯时间

        mCompositeSubscription = new CompositeSubscription();
        Subscription subscription = RxBus.getInstance().subscribe(busEvent -> {
            switch (busEvent.getMsgId()) {
                case BusEvent.MSG_TIME_MINUTE:
                    if (mMinute == 59 && getActivity() != null) { // 一小时刷新滤芯状态一次
                        mMinute = 0;
                        getActivity().runOnUiThread(this::refreshFilter);
                    } else mMinute++;
                    break;
                case BusEvent.MSG_CHANGEABLE_SCENE_UPDATE: // 变温室场景更新
                    mPresenter.loadDisplayScene(mAllList, mDisplayList, mBackupList);
                    break;
            }
        });
        mCompositeSubscription.add(subscription);

        if (mModel.equals(AppConstants.MODEL_JD)) mBuyTextView.setVisibility(View.GONE);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null && dialog.getWindow() != null) {
            dialog.getWindow().setLayout((int) FridgeApplication.getContext().getResources().getDimension(R.dimen.px_x_1000), ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe(this);// 订阅
        mSnowingSurfaceView.startFall();// 雪花 View 动画开始
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.unSubscribe();// 取消订阅
        mSnowingSurfaceView.stopFall();// 雪花 View 动画停止
    }

    @Override
    public void onDestroyView() {
        stopWaveAnimation();// 一键净化动画停止
        super.onDestroyView();
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();// 统一取消订阅
            mCompositeSubscription = null;
        }
        if (mPresenter != null) mPresenter = null;
        if (mDisplayAdapter != null) mDisplayAdapter = null;
        if (mBackupAdapter != null) mBackupAdapter = null;
        if (mAllList != null) {
            mAllList.clear();
            mAllList = null;
        }
        if (mBackupList != null) {
            mBackupList.clear();
            mBackupList = null;
        }
        if (mDisplayList != null) {
            mDisplayList.clear();
            mDisplayList = null;
        }
        RxBus.getInstance().post(BusEvent.MSG_START_STROLL);// 首页相册开始滚动
    }

    @OnClick(R.id.fridge_back)
    public void back() { // 隐藏冰箱控制页
        mControlRelativeLayout.setVisibility(View.GONE);
        mControlRelativeLayout.setAnimation(AnimationUtils.makeOutAnimation(FridgeApplication.getContext(), false));
    }

    @OnClick(R.id.fridge_close)
    public void close() { // 关闭
        dismiss();
    }

    private void initChangeableScene() { // 初始化变温室场景
        mAllList = new ArrayList<>();
        mDisplayList = new ArrayList<>();
        mBackupList = new ArrayList<>();
        mDisplayAdapter = new SceneAdapter(mDisplayList, 2);
        mBackupAdapter = new SceneAdapter(mBackupList, 1);

        mDisplayAdapter.setOnItemClickListener((parent, view1, position, id) -> { // 场景显示区点击
            if (mDisplayList.size() <= 2) {
                ToastUtil.showCenter(FridgeApplication.getContext(), FridgeApplication.getContext().getResources().getString(R.string.fridge_least_than_tip));
                return;
            }
            mBackupList.add(mDisplayList.get(position));
            mDisplayList.remove(position);
            mDisplayAdapter.notifyItemRemoved(position);
            mDisplayAdapter.notifyItemRangeChanged(position, mDisplayList.size() - position);
            mBackupAdapter.notifyItemInserted(mBackupList.size() - 1);
            mBackupAdapter.notifyItemRangeChanged(mBackupList.size() - 1, 1);
        });

        mBackupAdapter.setOnItemClickListener((parent, view12, position, id) -> { // 场景备选区点击
            if (mDisplayList.size() >= 6) {
                ToastUtil.showCenter(FridgeApplication.getContext(), FridgeApplication.getContext().getResources().getString(R.string.fridge_more_than_tip));
                return;
            }
            mDisplayList.add(mBackupList.get(position));
            mBackupList.remove(position);
            mBackupAdapter.notifyItemRemoved(position);
            mBackupAdapter.notifyItemRangeChanged(position, mBackupList.size() - position);
            mDisplayAdapter.notifyItemInserted(mDisplayList.size() - 1);
            mDisplayAdapter.notifyItemRangeChanged(mDisplayList.size() - 1, 1);
        });
        mPresenter.loadDisplayScene(mAllList, mDisplayList, mBackupList);// 读取展示场景列表
    }

    @OnClick(R.id.fridge_filter_layout)
    public void showFilter() { // 显示滤芯
        mFrameLayout.setVisibility(View.VISIBLE);
        mFilterLinearLayout.setVisibility(View.VISIBLE);
        mFilterLinearLayout.setAnimation(AnimationUtils.makeInAnimation(FridgeApplication.getContext(), false));
    }

    @OnClick(R.id.fridge_right_bg_layout)
    public void hideRightLayout() { // 隐藏右边布局
        Animation animation = AnimationUtils.makeOutAnimation(FridgeApplication.getContext(), true);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mFrameLayout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        if (mFilterLinearLayout.getVisibility() == View.VISIBLE) {
            mFilterLinearLayout.setVisibility(View.GONE);
            mFilterLinearLayout.setAnimation(animation);
        } else if (mErrorRecyclerView.getVisibility() == View.VISIBLE) {
            mErrorRecyclerView.setVisibility(View.GONE);
            mErrorRecyclerView.setAnimation(animation);
        } else if (mSceneRelativeLayout.getVisibility() == View.VISIBLE) {
            mSceneRelativeLayout.setVisibility(View.GONE);
            mSceneRelativeLayout.setAnimation(animation);
            mPresenter.loadDisplayScene(mAllList, mDisplayList, mBackupList);
        }
    }

    @OnClick(R.id.fridge_filter_reset)
    public void resetFilter() { // 重置滤芯
        if (isRepeatedClick()) return;
        if (getActivity() != null) {
            BaseAlertDialog dialog = new BaseAlertDialog(getActivity(), FridgeApplication.getContext().getResources().getString(R.string.fridge_filter_reset_tip),
                    FridgeApplication.getContext().getResources().getString(R.string.cancel), FridgeApplication.getContext().getResources().getString(R.string.confirm));
            dialog.setOnLeftClickListener(dialog::dismiss);
            dialog.setOnRightClickListener(() -> Observable.just(FridgePreference.getInstance().saveUsedFilterLife(0))
                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                    .onTerminateDetach()
                    .subscribe(s -> {
                        FridgePreference.getInstance().setFilterLowFlag(false);
                        FridgePreference.getInstance().setFilterOutOfDateFlag(false);
                        refreshFilter();
                        dialog.dismiss();
                    }, throwable -> logUtil.e(TAG, throwable.getMessage())));
            dialog.show();
        }
    }

    @OnClick(R.id.fridge_filter_buy)
    public void buyFilter() { // 购买滤芯
        Intent intent = new Intent(getActivity(), BrowserActivity.class);
        String url;
        if (ManagePreference.getInstance().getDebug()) {
            url = AppConstants.URL_VMALL_DEBUG + "/#/detail/58";
        } else {
            url = AppConstants.URL_VMALL_RELEASE + "/#/detail/58";
        }
        intent.putExtra(AppConstants.WEB_URL, url);
        startActivity(intent);
    }

    @OnClick(R.id.fridge_mode_intelligence)
    public void setSmart() { // 智能模式
        if (mPresenter.isInspectionRunning()
            //|| mPresenter.isCommunicationError()
                ) return;
//        if ((mModel.equals(AppConstants.MODEL_X4) || mModel.equals(AppConstants.MODEL_JD)) && mPresenter.isExistError())
//            return;
        if (mMode == AppConstants.MODE_SMART) return;
        mMode = AppConstants.MODE_SMART;
        mIntelligenceImageView.setSelected(true);
        mHolidayImageView.setSelected(false);
        mQuickColdImageView.setSelected(false);
        mQuickFreezingImageView.setSelected(false);
        mBigImageView.setImageResource(R.drawable.icon_fridge_intelligence_desc);
        mColdRoomView.setModeIconVisible(1);
        mFreezingRoomView.setModeIconVisible(1);
        mPresenter.setMode(mMode, true);
    }

    @OnClick(R.id.fridge_mode_holiday)
    public void setHoliday() { // 假日模式
        if (mPresenter.isInspectionRunning()
            //  || mPresenter.isCommunicationError()
                ) return;
//        if ((mModel.equals(AppConstants.MODEL_X4) || mModel.equals(AppConstants.MODEL_JD)) && mPresenter.isExistError())
//            return;
        if (mMode == AppConstants.MODE_HOLIDAY) return;
        mMode = AppConstants.MODE_HOLIDAY;
        mIntelligenceImageView.setSelected(false);
        mQuickColdImageView.setSelected(false);
        mQuickFreezingImageView.setSelected(false);
        mHolidayImageView.setSelected(true);
        mBigImageView.setImageResource(R.drawable.icon_fridge_holiday_desc);
        mColdRoomView.setModeIconVisible(2);
        mFreezingRoomView.setModeIconVisible(2);
        mPresenter.setMode(mMode, true);
    }

    @OnClick(R.id.fridge_quick_cold)
    public void setQuickCold() { // 速冷
        if (mPresenter.isInspectionRunning()
            // || mPresenter.isCommunicationError()
                ) return;
//        if ((mModel.equals(AppConstants.MODEL_X4) || mModel.equals(AppConstants.MODEL_JD)) && mPresenter.isExistError())
//            return;
        mQuickColdImageView.setSelected(!mQuickColdImageView.isSelected());
        mPresenter.setMode(AppConstants.MODE_QUICK_COLD, mQuickColdImageView.isSelected());
        if (mQuickColdImageView.isSelected()) mColdRoomView.setTempIconVisible(1);
        else mColdRoomView.setTempIconGone();
    }

    @OnClick(R.id.fridge_quick_freezing)
    public void setQuickFreezing() { // 速冻
        if (mPresenter.isInspectionRunning()
            // || mPresenter.isCommunicationError()
                ) return;
//        if ((mModel.equals(AppConstants.MODEL_X4) || mModel.equals(AppConstants.MODEL_JD)) && mPresenter.isExistError())
//            return;
        mQuickFreezingImageView.setSelected(!mQuickFreezingImageView.isSelected());
        mPresenter.setMode(AppConstants.MODE_QUICK_FREEZE, mQuickFreezingImageView.isSelected());
        if (mQuickFreezingImageView.isSelected()) mFreezingRoomView.setTempIconVisible(2);
        else mFreezingRoomView.setTempIconGone();
    }

    @OnClick(R.id.fridge_clean)
    public void setOneKeyClean() {
        if (mPresenter.isInspectionRunning()
            // && mPresenter.isCommunicationError()
                ) return;
        if (mCleanImageView.isSelected()) {
            mCleanImageView.setSelected(false);
            mCleanTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.fridge_one_key_clean));
            stopWaveAnimation();
        } else {
            mCleanImageView.setSelected(true);
            mCleanTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.fridge_one_key_clean_doing));
            playWaveAnimation();
        }
        mPresenter.setOneKeyClean(mCleanImageView.isSelected());
    }

    @Override
    public void refreshTip(String tip) {
        mTipTextView.setText(tip);
    }

    @Override
    public void refreshUi(DeviceParams params) {
        logUtil.d(TAG, "start refreshUi");
        if (params == null) {
            logUtil.e(TAG, "refreshUi fail, params is null");
            return;
        }
        if (mIsSetting) return;
        mColdRoomView.setTemp(params.isCold_switch(), params.getCold_temp_set());// 冷藏室设置温度
        if (mChangeableRoomView != null)
            mChangeableRoomView.setTemp(params.isChangeable_switch(), params.getChangeable_temp_set());// 变温室设置温度
        mFreezingRoomView.setTemp(true, params.getFreezing_temp_set());// 冷冻室设置温度
        mColdRoomSettingView.setupData(params.isCold_switch(), params.getCold_temp_set());// 冷藏室设置
        if (mChangeableRoomSettingView != null)
            mChangeableRoomSettingView.setupData(params.isChangeable_switch(), params.getChangeable_temp_set());// 变温室设置
        mFreezingRoomSettingView.setupData(true, params.getFreezing_temp_set());// 冷冻室设置
        if (mModel.equals(AppConstants.MODEL_X4) || mModel.equals(AppConstants.MODEL_JD)) { // 雪祺 450
            if (!params.isCold_switch()) mQuickColdLinearLayout.setEnabled(false);
            else mQuickColdLinearLayout.setEnabled(true);
        }
        // 工作模式
        mMode = params.getMode();
        if (mMode == AppConstants.MODE_SMART) { // 智能模式
            mIntelligenceImageView.setSelected(true);
            mHolidayImageView.setSelected(false);
            mBigImageView.setImageResource(R.drawable.icon_fridge_intelligence_desc);
        } else if (mMode == AppConstants.MODE_HOLIDAY) { // 假日模式
            mIntelligenceImageView.setSelected(false);
            mHolidayImageView.setSelected(true);
            mBigImageView.setImageResource(R.drawable.icon_fridge_holiday_desc);
        } else {
            mIntelligenceImageView.setSelected(false);
            mHolidayImageView.setSelected(false);
            mBigImageView.setImageResource(R.drawable.icon_common_fridge);
        }
        if (mModel.equals(AppConstants.MODEL_X5)) { // 美菱 521
            mFruitTextView.setSelected(false);
            mFreshTextView.setSelected(false);
            mIcedTextView.setSelected(false);
            if (params.isCold_switch()) {
                mFruitTextView.setEnabled(true);
                mFreshTextView.setEnabled(true);
                mIcedTextView.setEnabled(true);
            } else {
                mFruitTextView.setEnabled(false);
                mFreshTextView.setEnabled(false);
                mIcedTextView.setEnabled(false);
            }
            if (params.isFresh_fruit()) { // 鲜果
                mFruitTextView.setSelected(true);
                mCCRoomView.setTemp(true, 5);
                mCCTempTextView.setText(String.valueOf(5));
            } else if (params.isRetain_fresh()) { // 0 度保鲜
                mFreshTextView.setSelected(true);
                mCCRoomView.setTemp(true, 0);
                mCCTempTextView.setText(String.valueOf(0));
            } else if (params.isIced()) { // 冰镇
                mIcedTextView.setSelected(true);
                mCCRoomView.setTemp(true, -1);
                mCCTempTextView.setText(String.valueOf(-1));
            } else {
                mCCRoomView.setTemp(false, -1);
                mCCTempTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.fridge_off));
            }
        }
        mQuickColdImageView.setSelected(params.isQuick_cold());// 速冷
        mQuickFreezingImageView.setSelected(params.isQuick_freeze());// 速冻
        // 一键净化
        if (FridgeRepository.getInstance().isOneKeyCleanRunning()) {
            mCleanImageView.setSelected(true);
            mCleanTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.fridge_one_key_clean_doing));
        } else {
            stopWaveAnimation();
            mCleanImageView.setSelected(false);
            mCleanTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.fridge_one_key_clean));
        }
        // 故障
        DeviceError deviceError = SerialManager.getInstance().getDeviceParamsGet().getDeviceError();
        String status = "";
        if (deviceError.isError_communication())
            status += FridgeApplication.getContext().getResources().getString(R.string.fridge_error_0) + ",";
        if (deviceError.isError_rc_sensor())
            status += FridgeApplication.getContext().getResources().getString(R.string.fridge_error_1) + ",";
        if (deviceError.isError_cc_sensor())
            status += FridgeApplication.getContext().getResources().getString(R.string.fridge_error_2) + ",";
        if (deviceError.isError_fc_sensor())
            status += FridgeApplication.getContext().getResources().getString(R.string.fridge_error_3) + ",";
        if (deviceError.isError_rc_defrost_sensor())
            status += FridgeApplication.getContext().getResources().getString(R.string.fridge_error_4) + ",";
        if (deviceError.isError_fc_defrost_sensor())
            status += FridgeApplication.getContext().getResources().getString(R.string.fridge_error_5) + ",";
        if (deviceError.isError_indoor_sensor())
            status += FridgeApplication.getContext().getResources().getString(R.string.fridge_error_6) + ",";
        if (deviceError.isError_defrost())
            status += FridgeApplication.getContext().getResources().getString(R.string.fridge_error_14) + ",";
        if (deviceError.isError_fan_door())
            status += FridgeApplication.getContext().getResources().getString(R.string.fridge_error_7) + ",";
        if (deviceError.isError_rc_fan())
            status += FridgeApplication.getContext().getResources().getString(R.string.fridge_error_8) + ",";
        if (deviceError.isError_cc_fan())
            status += FridgeApplication.getContext().getResources().getString(R.string.fridge_error_9) + ",";
        if (deviceError.isError_fc_fan())
            status += FridgeApplication.getContext().getResources().getString(R.string.fridge_error_10) + ",";
        if (deviceError.isError_rc_defrost_sensor())
            status += FridgeApplication.getContext().getResources().getString(R.string.fridge_error_21) + ",";
        if (deviceError.isError_humidity_sensor())
            status += FridgeApplication.getContext().getResources().getString(R.string.fridge_error_22) + ",";
        if (deviceError.isError_humidity_temp_sensor())
            status += FridgeApplication.getContext().getResources().getString(R.string.fridge_error_23) + ",";
        if (deviceError.isError_rc_cc_sensor())
            status += FridgeApplication.getContext().getResources().getString(R.string.fridge_error_24) + ",";
        if (status.length() == 0) mErrorTextView.setVisibility(View.GONE);
        else {
            mErrorTextView.setVisibility(View.VISIBLE);
            String[] array = status.split(",");
            if (array.length > 1) { // 多个故障
                mErrorTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.fridge_error_more));
                mErrorTextView.setEnabled(true);
                mErrorTextView.setOnClickListener(v -> {
                    mFrameLayout.setVisibility(View.VISIBLE);
                    mErrorRecyclerView.setVisibility(View.VISIBLE);
                    mErrorRecyclerView.setAnimation(AnimationUtils.makeInAnimation(FridgeApplication.getContext(), false));
                    ErrorAdapter adapter = new ErrorAdapter(array);
                    mErrorRecyclerView.setAdapter(adapter);
                });
            } else {
                status = status.substring(0, status.length() - 1);
                mErrorTextView.setText(status);
                mErrorTextView.setEnabled(false);
            }
        }
    }

    @Override
    public void refreshScene() {
        mDisplayAdapter.notifyDataSetChanged();
        mBackupAdapter.notifyDataSetChanged();
        mChangeableRoomSettingView.refreshScene(mDisplayList);
    }

    @Override
    public void setIsSetting(boolean enable) {
        mIsSetting = enable;
    }

    @Override
    public void onSceneMoreClick() {
        mFrameLayout.setVisibility(View.VISIBLE);
        mSceneRelativeLayout.setVisibility(View.VISIBLE);
        mSceneRelativeLayout.setAnimation(AnimationUtils.makeInAnimation(FridgeApplication.getContext(), false));
    }

    @Override
    public void onSceneSet(ChangeableScene name) {
        mChangeableRoomView.setTempIconVisible(2);
        mPresenter.setScene(mAllList, name);
    }

    @OnClick(R.id.fridge_scene_save)
    public void save() {
        mPresenter.saveDisplayScenes(mDisplayList);
        Animation animation = AnimationUtils.makeOutAnimation(FridgeApplication.getContext(), true);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mFrameLayout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mSceneRelativeLayout.setVisibility(View.GONE);
        mSceneRelativeLayout.setAnimation(animation);
    }

    /**
     * 滤芯状态刷新
     */
    private void refreshFilter() {
        int progress = (int) (FridgeRepository.getInstance().getFilterLifeUsedTime() * 1.0 / MiotRepository.getInstance().FILTER_LIFE_BASE * 100 + 0.5);
        progress = 100 - progress;
        if (progress < 0) progress = 0;
        if (progress == 0) mProgressWheel.setRimColor(0xFFFF7070);
        else if (progress <= 10) {
            mProgressWheel.setBarColor(0xFFFFD700);
            mProgressWheel.setRimColor(0xFF00BBA5);
        } else {
            mProgressWheel.setBarColor(0xFF98EF72);
            mProgressWheel.setRimColor(0xFF00BBA5);
        }
        mProgressWheel.setProgress(progress);
        String str = String.format(FridgeApplication.getContext().getResources().getString(R.string.fridge_filter_least), progress) + "%";
        mFilterTextView.setText(str);
    }

    /**
     * 播放一键净化动画
     */
    private void playWaveAnimation() {
        if (mSubscription != null) mSubscription.unsubscribe();
        mIsPlay = true;
        mSubscription = Observable.timer(20, TimeUnit.SECONDS)
                .onTerminateDetach()
                .subscribe(aLong -> mIsPlay = false, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(mSubscription);
        mWave1ImageView.setVisibility(View.VISIBLE);
        mWave2ImageView.setVisibility(View.VISIBLE);
        mWave3ImageView.setVisibility(View.VISIBLE);
        Animation animation_up = AnimationUtils.loadAnimation(FridgeApplication.getContext(), R.anim.wave_up);
        Animation animation_down = AnimationUtils.loadAnimation(FridgeApplication.getContext(), R.anim.wave_down);
        mWave1ImageView.startAnimation(animation_up);
        mWave2ImageView.startAnimation(animation_up);
        mWave3ImageView.startAnimation(animation_up);
        animation_up.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (!mIsPlay) {
                    stopWaveAnimation();
                    return;
                }
                mWave1ImageView.startAnimation(animation_down);
                mWave2ImageView.startAnimation(animation_down);
                mWave3ImageView.startAnimation(animation_down);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        animation_down.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (!mIsPlay) {
                    stopWaveAnimation();
                    return;
                }
                mWave1ImageView.startAnimation(animation_up);
                mWave2ImageView.startAnimation(animation_up);
                mWave3ImageView.startAnimation(animation_up);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 停止播放一键净化动画
     */
    private void stopWaveAnimation() {
        mIsPlay = false;
        mWave1ImageView.clearAnimation();
        mWave2ImageView.clearAnimation();
        mWave3ImageView.clearAnimation();
        mWave1ImageView.setVisibility(View.GONE);
        mWave2ImageView.setVisibility(View.GONE);
        mWave3ImageView.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        if (mControlRelativeLayout.getVisibility() != View.VISIBLE) {
            mControlRelativeLayout.setVisibility(View.VISIBLE);
            mControlRelativeLayout.setAnimation(AnimationUtils.makeInAnimation(FridgeApplication.getContext(), true));
        }
    }
}