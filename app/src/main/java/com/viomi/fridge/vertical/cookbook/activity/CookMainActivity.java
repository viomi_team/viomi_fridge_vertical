package com.viomi.fridge.vertical.cookbook.activity;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseActivity;
import com.viomi.fridge.vertical.common.http.ApiClient;
import com.viomi.fridge.vertical.common.util.ImageLoadUtil;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.ToastUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.cookbook.model.CookMenuCategory;
import com.viomi.fridge.vertical.cookbook.model.CookMenuDetail;
import com.viomi.fridge.vertical.cookbook.model.CookMenuMethod;
import com.viomi.fridge.vertical.cookbook.model.CookMenuRecipe;
import com.viomi.fridge.vertical.cookbook.model.CookMenuSearch;
import com.viomi.widget.AutoScrollPageView2;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import butterknife.BindView;
import butterknife.OnClick;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * 菜谱首页 Activity
 * Created by nanquan on 2018/2/26.
 */
public class CookMainActivity extends BaseActivity {
    private static final String TAG = CookMainActivity.class.getSimpleName();
    private CookMenuCategory mCookMenuCategory;// 菜谱分类
    private List<CookMenuDetail> mRecommendList = new ArrayList<>();
    private ObjectAnimator mRotationAnimator;
    private boolean mIsRefreshing;// 是否正在刷新
    private CompositeSubscription mCompositeSubscription;

    @BindView(R.id.cook_main_banner)
    AutoScrollPageView2 mAutoScrollPageView;// 轮播

    @BindView(R.id.title_bar_right_image)
    ImageView mSearchImageView;// 搜索
    @BindView(R.id.cook_main_refresh)
    ImageView mRefreshImageView;// 刷新

    @BindView(R.id.fl_cook_recommend_1)
    FrameLayout mRecommend1FrameLayout;
    @BindView(R.id.fl_cook_recommend_2)
    FrameLayout mRecommend2FrameLayout;
    @BindView(R.id.fl_cook_recommend_3)
    FrameLayout mRecommend3FrameLayout;
    @BindView(R.id.fl_cook_recommend_4)
    FrameLayout mRecommend4FrameLayout;
    @BindView(R.id.fl_cook_recommend_5)
    FrameLayout mRecommend5FrameLayout;
    @BindView(R.id.fl_cook_recommend_6)
    FrameLayout mRecommend6FrameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        layoutId = R.layout.activity_cook_main;
        mTitle = getResources().getString(R.string.home_cookbook);
        super.onCreate(savedInstanceState);
        initTitle();
        initBanner();
        initAnimator();
        refreshRecommends();
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
        if (mRecommendList != null) {
            mRecommendList.clear();
            mRecommendList = null;
        }
        if (mCookMenuCategory != null) mCookMenuCategory = null;
    }

    private void initTitle() {
        mSearchImageView.setImageResource(R.drawable.icon_search);
        mSearchImageView.setVisibility(View.VISIBLE);
    }

    private void initBanner() {
        List<Uri> list = new ArrayList<>();
        list.add(Uri.parse("android.resource://" +
                FridgeApplication.getContext().getPackageName() + "/" + R.drawable.cook_banner_1));
        list.add(Uri.parse("android.resource://" +
                FridgeApplication.getContext().getPackageName() + "/" + R.drawable.cook_banner_2));
        list.add(Uri.parse("android.resource://" +
                FridgeApplication.getContext().getPackageName() + "/" + R.drawable.cook_banner_3));
        list.add(Uri.parse("android.resource://" +
                FridgeApplication.getContext().getPackageName() + "/" + R.drawable.cook_banner_1));
        list.add(Uri.parse("android.resource://" +
                FridgeApplication.getContext().getPackageName() + "/" + R.drawable.cook_banner_2));
        list.add(Uri.parse("android.resource://" +
                FridgeApplication.getContext().getPackageName() + "/" + R.drawable.cook_banner_3));
        mAutoScrollPageView.setViewUris(list);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        ViewPager viewPager = mAutoScrollPageView.getPager();

        ViewGroup.LayoutParams params = viewPager.getLayoutParams();
        params.width = (int) (metrics.widthPixels * 0.8);
        viewPager.setLayoutParams(params);
        viewPager.setPageMargin((int) FridgeApplication.getContext().getResources().getDimension(R.dimen.px_x_40));
        viewPager.setClipChildren(false);
    }

    private void initAnimator() {
        mRotationAnimator = ObjectAnimator.ofFloat(mRefreshImageView, "rotation", 0.0f, 360.0f);
        mRotationAnimator.setDuration(1000);
        mRotationAnimator.setRepeatCount(-1);
        mRotationAnimator.setInterpolator(new LinearInterpolator());
    }

    /**
     * 刷新推荐菜谱
     */
    private void refreshRecommends() {
        if (mIsRefreshing) return;
        startRefresh();
        if (mCookMenuCategory == null) {
            getCategory();
            return;
        }
        mRecommendList.clear();
        getRecommends();
    }

    private void getCategory() {
        // 获取分类数据
        Subscription subscription = ApiClient.getInstance().getApiService().getCookCategory()
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(cookMenuCategoryMobResult -> {
                    endRefresh();
                    mCookMenuCategory = cookMenuCategoryMobResult.getResult();
                    refreshRecommends();
                }, throwable -> {
                    endRefresh();
                    ToastUtil.showCenter(FridgeApplication.getContext(), getResources().getString(R.string.net_error));
                    logUtil.e(TAG, throwable.getMessage());
                });
        if (mCompositeSubscription == null) mCompositeSubscription = new CompositeSubscription();
        mCompositeSubscription.add(subscription);
    }

    /**
     * 获取随机推荐菜谱
     */
    private void getRecommends() {
        // 获取随机菜品二级分类
        CookMenuCategory cookMenuCategory1 = mCookMenuCategory.getChilds().get(0);
        Random random = new Random();
        int position = random.nextInt(cookMenuCategory1.getChilds().size());
        CookMenuCategory cookMenuCategory2 = cookMenuCategory1.getChilds().get(position);// 随机菜品 id

        Subscription subscription = ApiClient.getInstance().getApiService().getCookSearch(cookMenuCategory2.getCategoryInfo().getCtgId(),
                null, 1, 1)
                .onTerminateDetach()
                .flatMap(result -> {
                    // 获取当前分类菜谱信息
                    CookMenuSearch cookMenuSearch = result.getResult();
                    if (cookMenuSearch == null) return null;
                    // 菜谱 id 规则：分类 id（10 位数）+ 菜谱编号（10 位数，不重复）
                    // 根据最后一条菜谱 id 和当前分类的菜谱数量，生成随机的菜谱 id
                    int total = cookMenuSearch.getTotal();
                    String lastMenuId = cookMenuSearch.getList().get(0).getMenuId();
                    String ctgId = lastMenuId.substring(0, 10);
                    String menuNum = lastMenuId.substring(10, 20);
                    // 获取随机菜谱 id
                    int randomMenuNum = Integer.parseInt(menuNum) - random.nextInt(total);
                    String randomMenuId = ctgId +
                            String.format(Locale.getDefault(), "%010d", randomMenuNum);
                    // 获取菜谱详情
                    return ApiClient.getInstance().getApiService().getCookDetail(randomMenuId);
                })
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(cookMenuDetailMobResult -> {
                    CookMenuDetail cookMenuDetail = cookMenuDetailMobResult.getResult();
                    if (cookMenuDetail == null ||
                            cookMenuDetail.getRecipe() == null ||
                            cookMenuDetail.getRecipe().getImg() == null ||
                            TextUtils.isEmpty(cookMenuDetail.getRecipe().getImg()) ||
                            cookMenuDetail.getRecipe().getMethod() == null ||
                            TextUtils.isEmpty(cookMenuDetail.getRecipe().getMethod())) {
                        // 重新获取
                        getRecommends();
                        return;
                    } else { // 排除缺图菜谱
                        Gson gson = new Gson();
                        List<CookMenuMethod> methodList = gson.fromJson(cookMenuDetail.getRecipe().getMethod(),
                                new TypeToken<List<CookMenuMethod>>() {
                                }.getType());
                        boolean isReturn = false;
                        for (CookMenuMethod cookMenuMethod : methodList) {
                            if (cookMenuMethod.getImg() == null || cookMenuMethod.getImg().equals("")
                                    || cookMenuMethod.getStep() == null || cookMenuMethod.getStep().equals("")) {
                                isReturn = true;
                                break;
                            }
                        }
                        if (isReturn) {
                            getRecommends();// 重新获取
                            return;
                        }
                    }
                    mRecommendList.add(cookMenuDetail);
                    if (mRecommendList.size() < 6) getRecommends();
                    else showRecommends(mRecommendList);
                }, throwable -> {
                    logUtil.e(TAG, throwable.getMessage());
                    endRefresh();
                });
        if (mCompositeSubscription == null) mCompositeSubscription = new CompositeSubscription();
        mCompositeSubscription.add(subscription);
    }

    private void showRecommends(List<CookMenuDetail> list) {
        endRefresh();
        showRecommend(mRecommend1FrameLayout, list.get(0));
        showRecommend(mRecommend2FrameLayout, list.get(1));
        showRecommend(mRecommend3FrameLayout, list.get(2));
        showRecommend(mRecommend4FrameLayout, list.get(3));
        showRecommend(mRecommend5FrameLayout, list.get(4));
        showRecommend(mRecommend6FrameLayout, list.get(5));
    }

    private void showRecommend(View itemView, CookMenuDetail cookMenuDetail) {
        itemView.setTag(cookMenuDetail);

        ImageView ivImg = itemView.findViewById(R.id.iv_cook_menu_img);
        TextView tvCategory = itemView.findViewById(R.id.tv_cook_menu_category);
        TextView tvName = itemView.findViewById(R.id.tv_cook_menu_name);
        TextView tvMaterial = itemView.findViewById(R.id.tv_cook_menu_material);
        TextView tvIntro = itemView.findViewById(R.id.tv_cook_menu_intro);

        CookMenuRecipe cookMenuRecipe = cookMenuDetail.getRecipe();
        // 设置图片
        if (!TextUtils.isEmpty(cookMenuRecipe.getImg())) {
            ImageLoadUtil.getInstance().loadUrl(cookMenuRecipe.getImg(), ivImg);
        }
        // 设置类别标签
        if (!TextUtils.isEmpty(cookMenuDetail.getCtgTitles())) {
            String category = cookMenuDetail.getCtgTitles();
            if (category.contains(",")) {
                int dividePosition = category.indexOf(",");
                category = category.substring(0, dividePosition);
            }
            tvCategory.setText(category);
        }
        // 设置名字
        tvName.setText(cookMenuDetail.getName());
        // 设置食材
        if (!TextUtils.isEmpty(cookMenuRecipe.getIngredients())) {
            Gson gson = new Gson();
            List<String> ingredientList = gson.fromJson(cookMenuRecipe.getIngredients(),
                    new TypeToken<List<String>>() {
                    }.getType());
            tvMaterial.setText(ingredientList.get(0));
        }
        // 设置描述
        if (!TextUtils.isEmpty(cookMenuRecipe.getSumary())) {
            tvIntro.setText(cookMenuRecipe.getSumary());
        }
    }

    private void startRefresh() {
        mRotationAnimator.start();
        mIsRefreshing = true;
    }

    private void endRefresh() {
        mRotationAnimator.end();
        mIsRefreshing = false;
    }

    @OnClick(R.id.title_bar_right_image)
    public void onSearchClick() { // 搜索菜谱
        Intent intent = new Intent(CookMainActivity.this, CookSearchActivity.class);
        startActivity(intent);
    }

    @OnClick({R.id.cook_main_category_1, R.id.cook_main_category_2, R.id.cook_main_category_3, R.id.cook_main_category_4, R.id.tv_cook_category_5})
    public void onCategoryClick(View view) { // 菜品点击
        if (mCookMenuCategory == null) return;
        CookMenuCategory childCookMenuCategory = null;
        String title = "";
        List<CookMenuCategory> categoryList = mCookMenuCategory.getChilds();
        switch (view.getId()) {
            case R.id.cook_main_category_1:
                childCookMenuCategory = categoryList.get(0);
                title = getResources().getString(R.string.cookbook_menu_type);
                break;
            case R.id.cook_main_category_2:
                childCookMenuCategory = categoryList.get(1);
                title = getResources().getString(R.string.cookbook_technology_type);
                break;
            case R.id.cook_main_category_3:
                childCookMenuCategory = categoryList.get(2);
                title = getResources().getString(R.string.cookbook_type_type);
                break;
            case R.id.cook_main_category_4:
                childCookMenuCategory = categoryList.get(3);
                title = getResources().getString(R.string.cookbook_people_type);
                break;
            case R.id.tv_cook_category_5:
                childCookMenuCategory = categoryList.get(4);
                title = getResources().getString(R.string.cookbook_function_type);
                break;
        }
        Intent intent = new Intent(CookMainActivity.this, CookCategoryActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("cookMenuCategory", childCookMenuCategory);
        bundle.putString("title", title);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @OnClick(R.id.ll_cook_refresh)
    public void onRefreshClick() { // 刷新点击
        refreshRecommends();
    }

    @OnClick({R.id.fl_cook_recommend_1, R.id.fl_cook_recommend_2, R.id.fl_cook_recommend_3, R.id.fl_cook_recommend_4, R.id.fl_cook_recommend_5, R.id.fl_cook_recommend_6})
    public void onRecommendClick(View view) { // 推荐菜式点击
        CookMenuDetail cookMenuDetail = (CookMenuDetail) view.getTag();
        if (cookMenuDetail != null) {
            Intent intent = new Intent(CookMainActivity.this, CookDetailActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("cookMenuDetail", cookMenuDetail);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }
}