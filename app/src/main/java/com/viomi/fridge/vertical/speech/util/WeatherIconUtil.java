package com.viomi.fridge.vertical.speech.util;

import com.viomi.fridge.vertical.R;


/**
 * 讯飞天气对应图标
 * Created by viomi on 2017/3/6.
 */
public class WeatherIconUtil {

    public static int setWeatherIcon(String type) {
        int drawableId;
        switch (type) {
            case "暴雪":
                drawableId = R.drawable.icon_weather_snow_3;
                break;
            case "暴雨":
                drawableId = R.drawable.icon_weather_rain_6;
                break;
            case "暴雨到大暴雨":
                drawableId = R.drawable.icon_weather_rain_5;
                break;
            case "大暴雨":
                drawableId = R.drawable.icon_weather_rain_1;
                break;
            case "大暴雨到特大暴雨":
                drawableId = R.drawable.icon_weather_rain_4;
                break;
            case "大到暴雪":
                drawableId = R.drawable.icon_weather_snow_3;
                break;
            case "大到暴雨":
                drawableId = R.drawable.icon_weather_rain_2;
                break;
            case "大雪":
                drawableId = R.drawable.icon_weather_snow_2;
                break;
            case "大雨":
                drawableId = R.drawable.icon_weather_rain_3;
                break;
            case "冻雨":
                drawableId = R.drawable.icon_weather_hail;
                break;
            case "多云":
                drawableId = R.drawable.icon_weather_sunny_cloudy;
                break;
            case "浮尘":
                drawableId = R.drawable.icon_weather_sand_storm_1;
                break;
            case "雷阵雨":
                drawableId = R.drawable.icon_weather_thundershower;
                break;
            case "雷阵雨伴有冰雹":
                drawableId = R.drawable.icon_weather_thundershower_hail;
                break;
            case "霾":
                drawableId = R.drawable.icon_weather_haze;
                break;
            case "强沙尘暴":
                drawableId = R.drawable.icon_weather_sand_storm_2;
                break;
            case "晴":
                drawableId = R.drawable.icon_weather_sunny;
                break;
            case "沙尘暴":
                drawableId = R.drawable.icon_weather_sand_storm_1;
                break;
            case "特大暴雨":
                drawableId = R.drawable.icon_weather_rain_6;
                break;
            case "雾":
                drawableId = R.drawable.icon_weather_fog;
                break;
            case "小到中雪":
                drawableId = R.drawable.icon_weather_snow_1;
                break;
            case "小到中雨":
                drawableId = R.drawable.icon_weather_rain_2;
                break;
            case "小雪":
                drawableId = R.drawable.icon_weather_snow_1;
                break;
            case "小雨":
                drawableId = R.drawable.icon_weather_rain_1;
                break;
            case "扬沙":
                drawableId = R.drawable.icon_weather_sand_storm_1;
                break;
            case "阴":
                drawableId = R.drawable.icon_weather_cloudy;
                break;
            case "雨夹雪": {
                drawableId = R.drawable.icon_weather_rain_and_snow;
                break;
            }
            case "阵雪":
                drawableId = R.drawable.icon_weather_snow_1;
                break;
            case "阵雨":
                drawableId = R.drawable.icon_weather_rain_1;
                break;
            case "中到大雪":
                drawableId = R.drawable.icon_weather_snow_2;
                break;
            case "中到大雨":
                drawableId = R.drawable.icon_weather_rain_3;
                break;
            case "中雪":
                drawableId = R.drawable.icon_weather_snow_1;
                break;
            case "中雨":
                drawableId = R.drawable.icon_weather_rain_2;
                break;
            case "无":
                drawableId = R.drawable.icon_weather_cloudy;
                break;
            default:
                if (type.contains("小雪")) drawableId = R.drawable.icon_weather_snow_1;
                else if (type.contains("中雪")) drawableId = R.drawable.icon_weather_snow_1;
                else if (type.contains("大雪")) drawableId = R.drawable.icon_weather_snow_2;
                else if (type.contains("暴雪")) drawableId = R.drawable.icon_weather_snow_3;
                else if (type.contains("雪")) drawableId = R.drawable.icon_weather_snow_1;
                else if (type.contains("小雨")) drawableId = R.drawable.icon_weather_rain_1;
                else if (type.contains("中雨")) drawableId = R.drawable.icon_weather_rain_2;
                else if (type.contains("大雨")) drawableId = R.drawable.icon_weather_rain_3;
                else if (type.contains("暴雨")) drawableId = R.drawable.icon_weather_rain_6;
                else if (type.contains("雨")) drawableId = R.drawable.icon_weather_rain_1;
                else if (type.contains("冰雹")) drawableId = R.drawable.icon_weather_hail;
                else if (type.contains("尘")) drawableId = R.drawable.icon_weather_sand_storm_1;
                else if (type.contains("霾")) drawableId = R.drawable.icon_weather_haze;
                else if (type.contains("雾")) drawableId = R.drawable.icon_weather_fog;
                else if (type.contains("阴")) drawableId = R.drawable.icon_weather_cloudy;
                else if (type.contains("云")) drawableId = R.drawable.icon_weather_sunny_cloudy;
                else drawableId = R.drawable.icon_weather_cloudy;
                break;
        }
        return drawableId;
    }
}