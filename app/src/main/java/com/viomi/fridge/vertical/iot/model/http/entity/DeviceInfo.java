package com.viomi.fridge.vertical.iot.model.http.entity;

import com.miot.common.abstractdevice.AbstractDevice;


/**
 * Iot 设备信息
 * Created by William on 2018/2/3.
 */

public class DeviceInfo {
    private AbstractDevice abstractDevice = null;// 小米 IOT 设备
//    private CameraInfo cameraInfo = null;   // 绿联 IPC 设备信息

    public AbstractDevice getAbstractDevice() {
        return abstractDevice;
    }

    public void setAbstractDevice(AbstractDevice abstractDevice) {
        this.abstractDevice = abstractDevice;
    }

//    public CameraInfo getCameraInfo() {
//        return cameraInfo;
//    }
//
//    public void setCameraInfo(CameraInfo cameraInfo) {
//        this.cameraInfo = cameraInfo;
//    }
}
