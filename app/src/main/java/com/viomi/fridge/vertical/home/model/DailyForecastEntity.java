package com.viomi.fridge.vertical.home.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;

/**
 * 思必驰天气 Json
 * Created by William on 2018/8/15.
 */
public class DailyForecastEntity implements Serializable {
    @JSONField(name = "cond")
    private CondEntity cond;
    @JSONField(name = "date")
    private String date;
    @JSONField(name = "hum")
    private String hum;
    @JSONField(name = "pcpn")
    private String pcpn;
    @JSONField(name = "pop")
    private String pop;
    @JSONField(name = "pres")
    private String pres;
    @JSONField(name = "tmp")
    private TempRangeEntity tmp;
    @JSONField(name = "uv")
    private String uv;
    @JSONField(name = "vis")
    private String vis;
    @JSONField(name = "wind")
    private WindEntity wind;

    public CondEntity getCond() {
        return cond;
    }

    public void setCond(CondEntity cond) {
        this.cond = cond;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHum() {
        return hum;
    }

    public void setHum(String hum) {
        this.hum = hum;
    }

    public String getPcpn() {
        return pcpn;
    }

    public void setPcpn(String pcpn) {
        this.pcpn = pcpn;
    }

    public String getPop() {
        return pop;
    }

    public void setPop(String pop) {
        this.pop = pop;
    }

    public String getPres() {
        return pres;
    }

    public void setPres(String pres) {
        this.pres = pres;
    }

    public TempRangeEntity getTmp() {
        return tmp;
    }

    public void setTmp(TempRangeEntity tmp) {
        this.tmp = tmp;
    }

    public String getUv() {
        return uv;
    }

    public void setUv(String uv) {
        this.uv = uv;
    }

    public String getVis() {
        return vis;
    }

    public void setVis(String vis) {
        this.vis = vis;
    }

    public WindEntity getWind() {
        return wind;
    }

    public void setWind(WindEntity wind) {
        this.wind = wind;
    }
}
