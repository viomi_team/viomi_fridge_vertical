package com.viomi.fridge.vertical.speech.model.repository.skill;

import android.content.Context;

import com.miot.common.abstractdevice.AbstractDevice;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.administration.model.entity.QRCodeBase;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.iot.model.http.entity.WashingMachineProp;
import com.viomi.fridge.vertical.iot.model.repository.WashingMachineRepository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import rx.schedulers.Schedulers;

/**
 * 云米洗衣机技能
 * Created by William on 2018/8/30.
 */
public class ViomiWasherSkill {
    private static final String TAG = ViomiWasherSkill.class.getSimpleName();

    public static void handle(Context context, String data, AbstractDevice device) {
        String content;// 语音播放内容
        QRCodeBase qrCodeBase = (QRCodeBase) ToolUtil.getFileObject(context, AppConstants.USER_INFO_FILE);
        if (qrCodeBase == null) {
            content = "请先登录云米账号";
            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
            return;
        }
        if (device == null) {
            content = "没有连接到洗衣机";
            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
            return;
        }

        try {
            JSONObject jsonObject = new JSONObject(data);
            JSONObject nlu = jsonObject.optJSONObject("nlu");
            if (nlu == null) return;
            JSONObject semantics = nlu.optJSONObject("semantics");
            if (semantics == null) return;
            JSONObject request = semantics.optJSONObject("request");
            if (request == null) return;
            JSONArray slots = request.optJSONArray("slots");
            if (slots == null || slots.length() <= 0) return;

            String value = "";// 意图
            for (int i = 0; i < slots.length(); i++) {
                JSONObject jsonItem = slots.optJSONObject(i);
                String name = jsonItem.optString("name");
                if (name.equals("intent")) {
                    value = jsonItem.optString("value");
                    break;
                }
            }

            switch (value) {
                case "洗衣机启动":
                    switch (device.getDeviceModel()) {
                        case AppConstants.VIOMI_WASHER_U1:
                        case AppConstants.VIOMI_WASHER_U3:
                        case AppConstants.VIOMI_WASHER_U5:
                            WashingMachineRepository.getProp(device.getDeviceId())
                                    .subscribeOn(Schedulers.io())
                                    .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                                    .onTerminateDetach()
                                    .flatMap(rpcResult -> {
                                        WashingMachineProp prop = new WashingMachineProp(rpcResult.getList());
                                        return WashingMachineRepository.setWashProgram(prop.getProgram(), device.getDeviceId());
                                    })
                                    .onTerminateDetach()
                                    .flatMap(rpcResult -> WashingMachineRepository.setWashAction(1, device.getDeviceId()))
                                    .onTerminateDetach()
                                    .subscribe(rpcResult1 -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            break;
                        case AppConstants.VIOMI_WASHER_U2:
                        case AppConstants.VIOMI_WASHER_U4:
                            WashingMachineRepository.setWashAction(2, device.getDeviceId())
                                    .subscribeOn(Schedulers.io())
                                    .onTerminateDetach()
                                    .flatMap(rpcResult -> WashingMachineRepository.setWashAction(1, device.getDeviceId()))
                                    .onTerminateDetach()
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            break;
                    }
                    content = "正在为你启动" + device.getName();
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "洗衣机暂停":
                    WashingMachineRepository.setWashAction(0, device.getDeviceId())
                            .subscribeOn(Schedulers.io())
                            .onTerminateDetach()
                            .subscribe(rpcResult -> {
                            }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                    content = "正在为你暂停" + device.getName();
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "洗衣机关闭":
                    switch (device.getDeviceModel()) {
                        case AppConstants.VIOMI_WASHER_U1:
                        case AppConstants.VIOMI_WASHER_U3:
                        case AppConstants.VIOMI_WASHER_U5:
                            WashingMachineRepository.setWashAction(2, device.getDeviceId())
                                    .subscribeOn(Schedulers.io())
                                    .onTerminateDetach()
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            break;
                        case AppConstants.VIOMI_WASHER_U2:
                        case AppConstants.VIOMI_WASHER_U4:
                            WashingMachineRepository.setWashAction(3, device.getDeviceId())
                                    .subscribeOn(Schedulers.io())
                                    .onTerminateDetach()
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            break;
                    }
                    content = "正在为你关闭" + device.getName();
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "设置水温":
                    int value_temp = -1;
                    for (int i = 0; i < slots.length(); i++) {
                        JSONObject itemJson = slots.optJSONObject(i);
                        String name = itemJson.optString("name");
                        if ("整数".equals(name)) {
                            value_temp = itemJson.optInt("value");
                            break;
                        }
                    }
                    if (!(value_temp == 30 || value_temp == 40 || value_temp == 60 || value_temp == 90)) {
                        content = "洗衣机水温只能设为30度，40度，60度，90度";
                        RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    } else {
                        WashingMachineRepository.setWashTemp(value_temp, device.getDeviceId())
                                .compose(RxSchedulerUtil.SchedulersTransformer1())
                                .subscribe(rpcResult -> {
                                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                        content = "正在为你把" + device.getName() + "水温设置为" + value_temp + "度";
                        RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    }
                    break;
                case "设置漂洗次数":
                    int count = -1;
                    for (int i = 0; i < slots.length(); i++) {
                        JSONObject itemJson = slots.optJSONObject(i);
                        String name = itemJson.optString("name");
                        if ("整数".equals(name)) {
                            count = itemJson.optInt("value");
                            break;
                        }
                    }
                    if (count > 5 || count < 1) {
                        content = "洗衣机漂洗次数只能设为1至5次";
                        RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    } else {
                        WashingMachineRepository.setRinseTime(count, device.getDeviceId())
                                .compose(RxSchedulerUtil.SchedulersTransformer1())
                                .subscribe(rpcResult -> {
                                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                        content = "正在为你把" + device.getName() + "漂洗次数设置为" + count + "次";
                        RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    }
                    break;
                case "设置脱水转速":
                    String speed = "";
                    for (int i = 0; i < slots.length(); i++) {
                        JSONObject itemJson = slots.optJSONObject(i);
                        String name = itemJson.optString("name");
                        if ("脱水转速".equals(name)) {
                            speed = itemJson.optString("value");
                            break;
                        }
                    }
                    switch (speed) {
                        case "不脱水":
                            WashingMachineRepository.setSpinLevel("none", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "脱水强度设置为" + speed;
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "柔脱水":
                            WashingMachineRepository.setSpinLevel("gentle", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "脱水强度设置为" + speed;
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "轻脱水":
                            WashingMachineRepository.setSpinLevel("mild", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "脱水强度设置为" + speed;
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "中脱水":
                            WashingMachineRepository.setSpinLevel("middle", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "脱水强度设置为" + speed;
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "强脱水":
                            WashingMachineRepository.setSpinLevel("strong", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "脱水强度设置为" + speed;
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                    }
                    break;
                case "设置洗涤模式":
                    String mode = "";
                    for (int i = 0; i < slots.length(); i++) {
                        JSONObject itemJson = slots.optJSONObject(i);
                        String name = itemJson.optString("name");
                        if ("洗涤模式".equals(name)) {
                            mode = itemJson.optString("value");
                            break;
                        }
                    }
                    switch (mode) {
                        case "黄金洗":
                            WashingMachineRepository.setWashProgram("goldenwash", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "洗衣模式设置为黄金洗";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "洁筒洗":
                            WashingMachineRepository.setWashProgram("drumclean", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "洗衣模式设置为洁筒洗";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "单脱水":
                            WashingMachineRepository.setWashProgram("spin", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "洗衣模式设置为单脱水";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "除菌螨":
                            WashingMachineRepository.setWashProgram("antibacterial", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "洗衣模式设置为除菌螨";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "超快洗":
                            WashingMachineRepository.setWashProgram("super_quick", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "洗衣模式设置为超快洗";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "棉织物":
                            WashingMachineRepository.setWashProgram("cottons", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "洗衣模式设置为棉织物";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "衬衣":
                            WashingMachineRepository.setWashProgram("shirts", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "洗衣模式设置为衬衣";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "童衣洗":
                            WashingMachineRepository.setWashProgram("child_cloth", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "洗衣模式设置为童衣洗";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "牛仔":
                            WashingMachineRepository.setWashProgram("jeans", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "洗衣模式设置为牛仔";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "羊毛":
                            WashingMachineRepository.setWashProgram("wool", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "洗衣模式设置为羊毛";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "羽绒服":
                            WashingMachineRepository.setWashProgram("down", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "洗衣模式设置为羽绒服";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "雪纺":
                            WashingMachineRepository.setWashProgram("chiffon", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "洗衣模式设置为雪纺";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "运动服":
                            WashingMachineRepository.setWashProgram("outdoor", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "洗衣模式设置为运动服";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "精细织物":
                            WashingMachineRepository.setWashProgram("delicates", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "洗衣模式设置为精细织物";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "内衣":
                            WashingMachineRepository.setWashProgram("underwears", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "洗衣模式设置为内衣";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "漂脱":
                            WashingMachineRepository.setWashProgram("rinse_spin", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "洗衣模式设置为漂脱";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "自定义":
                            WashingMachineRepository.setWashProgram("My Time", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "洗衣模式设置为自定义";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "棉节能":
                            WashingMachineRepository.setWashProgram("cotton Eco", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "洗衣模式设置为棉节能";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                    }
                    break;
                case "查询剩余时间":
                    WashingMachineRepository.getProp(device.getDeviceId())
                            .compose(RxSchedulerUtil.SchedulersTransformer1())
                            .onTerminateDetach()
                            .subscribe(rpcResult -> {
                                WashingMachineProp prop = new WashingMachineProp(rpcResult.getList());
                                if (prop.getWash_process() == 0) {
                                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, device.getName() + "还没开始洗衣服呢。");
                                } else if (prop.getWash_process() == 5) {
                                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, device.getName() + "已经洗完衣服了。");
                                } else if (prop.getWash_process() == 6) {
                                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, device.getName() + "停机了哦。");
                                } else if (prop.getWash_process() == 7) {
                                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, device.getName() + "还没开机哦。");
                                } else {
                                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, device.getName() + "还有"
                                            + prop.getRemain_time() + "分钟洗完衣服。");
                                }
                            }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                    break;
            }
        } catch (JSONException e) {
            logUtil.e(TAG, e.toString());
            e.printStackTrace();
        }
    }
}
