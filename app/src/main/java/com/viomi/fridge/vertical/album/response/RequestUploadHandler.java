package com.viomi.fridge.vertical.album.response;

import android.os.Environment;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.album.AlbumService;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.yanzhenjie.andserver.RequestHandler;
import com.yanzhenjie.andserver.upload.HttpFileUpload;
import com.yanzhenjie.andserver.upload.HttpUploadContext;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HttpContext;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * 上传文件 Request
 */

public class RequestUploadHandler implements RequestHandler {
    private static final String TAG = RequestUploadHandler.class.getSimpleName();
    private onReceiveListener mOnReceiveListener;

    public RequestUploadHandler(AlbumService service) {
        mOnReceiveListener = service;
    }

    @Override
    public void handle(HttpRequest request, HttpResponse response, HttpContext context) throws HttpException, IOException {
        if (!HttpFileUpload.isMultipartContentWithPost(request)) {
            response(403, "You must upload file.", response);
        } else {
            String did = ToolUtil.getMiIdentification().getDeviceId() + "3331";
            StringBuilder result = new StringBuilder();
            try {
                MessageDigest messageDigest = MessageDigest.getInstance("md5");
                byte[] digest = messageDigest.digest(did.getBytes("UTF-8"));
                for (byte b : digest) {
                    result.append(String.format("%02x", b));
                }
            } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            Header[] allHeaders = request.getAllHeaders();
            boolean permission = false;

            for (Header allHeader : allHeaders) {
                String name = allHeader.getName();
                String value = allHeader.getValue();
                if ("key".equals(name) && result.toString().equals(value)) {
                    permission = true;
                }
            }

            if (permission) {
                File saveDirectory = new File(Environment.getExternalStorageDirectory(), AppConstants.PATH + AppConstants.ALBUM_SAVE_PATH);
                if (!saveDirectory.exists()) {
                    boolean isSuccess = saveDirectory.mkdirs();
                    logUtil.d(TAG, AppConstants.ALBUM_SAVE_PATH + " path create " + isSuccess);
                }

                if (saveDirectory.isDirectory()) {
                    try {
                        saveFile(request, saveDirectory);
                        response(200, "ok", response);
                        logUtil.d(TAG, "receive file finish");
                        if (mOnReceiveListener != null) mOnReceiveListener.onReceiveFinish();
                    } catch (Exception e) {
                        e.printStackTrace();
                        response(500, "Save the file when the error occurs.", response);
                    }
                } else {
                    response(500, "The server can not save the file.", response);
                }
            } else {
                response(403, "No permission.", response);
                throw (new HttpException("No permission."));
            }
        }
    }

    /**
     * 保存客户端上传的文件
     *
     * @param request:       请求
     * @param saveDirectory: 保存目录
     */
    private void saveFile(HttpRequest request, File saveDirectory) throws Exception {
        FileItemFactory factory = new DiskFileItemFactory(1024 * 1024, saveDirectory);
        HttpFileUpload fileUpload = new HttpFileUpload(factory);

        fileUpload.setProgressListener((l, l1, i) -> logUtil.d(TAG, "-----download-----" + l));

        List<FileItem> fileItems = fileUpload.parseRequest(new HttpUploadContext((HttpEntityEnclosingRequest) request));

        for (FileItem fileItem : fileItems) {
            if (!fileItem.isFormField()) { // File param.
                File uploadedFile = new File(saveDirectory, fileItem.getName());
                // 把流写到文件上。
                fileItem.write(uploadedFile);
            } else { // General param.
                String key = fileItem.getName();
                String value = fileItem.getString();
                logUtil.d(TAG, "key:" + key + ",value:" + value);
            }
        }
    }

    /**
     * 返回给客户端信息
     *
     * @param responseCode: 返回码
     * @param message:      返回信息
     * @param response:     回应
     */
    private void response(int responseCode, String message, HttpResponse response) throws IOException {
        response.setStatusCode(responseCode);
        response.setEntity(new StringEntity(message, "utf-8"));
    }

    public interface onReceiveListener {
        void onReceiveFinish();
    }
}
