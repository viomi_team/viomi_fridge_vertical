package com.viomi.fridge.vertical.administration.view.dialog;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.activity.LoginGuideActivity;
import com.viomi.fridge.vertical.administration.contract.LoginQRCodeContract;
import com.viomi.fridge.vertical.administration.model.entity.QRCodeLogin;
import com.viomi.fridge.vertical.administration.presenter.LoginQRCodePresenter;
import com.viomi.fridge.vertical.common.base.CommonDialog;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.PreferenceHelper;
import com.viomi.fridge.vertical.common.util.ToastUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;
import com.viomi.fridge.vertical.fridgecontrol.model.repository.MiotRepository;
import com.viomi.fridge.vertical.fridgecontrol.model.serial.entity.DeviceParams;
import com.viomi.fridge.vertical.fridgecontrol.model.serial.manager.SerialManager;
import com.viomi.fridge.vertical.message.manager.PushManager;
import com.viomi.widget.dialog.BaseProgressDialog;

import butterknife.BindView;
import butterknife.OnClick;
import rx.Subscription;

/**
 * 登录二维码生成 Dialog
 * Created by William on 2018/1/26.
 */
public class LoginQRCodeDialog extends CommonDialog implements LoginQRCodeContract.View {
    private static final String TAG = LoginQRCodeDialog.class.getSimpleName();
    private BaseProgressDialog mDialog;// 加载对话框
    private LoginQRCodeContract.Presenter mPresenter;
    private Subscription mSubscription;
    private boolean mIsBind;

    @BindView(R.id.login_qr_layout)
    LinearLayout mQRCodeLinearLayout;// 生成二维码布局
    @BindView(R.id.login_download_qr_layout)
    LinearLayout mDownloadLinearLayout;// 下载 app 二维码布局

    @BindView(R.id.login_qr_code)
    SimpleDraweeView mSimpleDraweeView;// 登录二维码

    @BindView(R.id.login_retry)
    TextView mRetryTextView;// 重新获取
    @BindView(R.id.login_app)
    TextView mAppTextView;// 使用 app 名称
    @BindView(R.id.login_message)
    TextView mMessageTextView;// 提示文字
    @BindView(R.id.login_guide)
    TextView mGuideTextView;// 如何使用
    @BindView(R.id.login_download_app)
    TextView mDownloadTextView;// 下载

    @BindView(R.id.login_progressbar)
    ProgressBar mProgressBar;// 进度

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.dialog_login_qr_code;
    }

    @Override
    protected void initWithOnCreateDialog(View view) {
        mPresenter = new LoginQRCodePresenter(FridgeApplication.getContext());
        mPresenter.subscribe(this);// 订阅
        if (getArguments() != null) {
            mIsBind = getArguments().getBoolean("miBind", false);
            if (!mIsBind) mPresenter.loadQRCode();
            else {
                mPresenter.loadMiQRCode();
                mAppTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.management_login_mi_app));
                mMessageTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.management_login_bind));
                mGuideTextView.setVisibility(View.GONE);
                mDownloadTextView.setVisibility(View.GONE);
            }
        } else {
            mIsBind = false;
            mPresenter.loadQRCode();
        }

        mSubscription = RxBus.getInstance().subscribe(busEvent -> {
            switch (busEvent.getMsgId()) {
                case BusEvent.MSG_MI_BIND: // 米家绑定成功
                    // 绑定成功，上报属性
                    DeviceParams params = SerialManager.getInstance().getDeviceParamsSet();
                    MiotRepository.getInstance().sendPropertyMode(params.getMode());
                    MiotRepository.getInstance().sendPropertyRCRoomEnable(params.isCold_switch());
                    MiotRepository.getInstance().sendPropertyRCSetTemp(params.getCold_temp_set());
                    if (FridgePreference.getInstance().getModel().equals(AppConstants.MODEL_X5)) {
                        MiotRepository.getInstance().sendPropertyCCRoomEnable(params.isChangeable_switch());
                        MiotRepository.getInstance().sendPropertyCCSetTemp(params.getChangeable_temp_set());
                    }
                    MiotRepository.getInstance().sendPropertyFZSetTemp(params.getFreezing_temp_set());
                    if (getActivity() != null) getActivity().runOnUiThread(this::dismiss);
                    break;
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mPresenter != null) {
            mPresenter.unSubscribe();// 取消订阅
            mPresenter = null;
        }
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.cancel();
            mDialog = null;
        }
    }

    @Override
    public void showQRCode(QRCodeLogin loginQRCode) { // 显示二维码
        mProgressBar.setVisibility(View.GONE);
        mSimpleDraweeView.setVisibility(View.VISIBLE);
        // 生成二维码
        ImageRequest request = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(loginQRCode.getResult()))
                .setResizeOptions(new ResizeOptions((int) FridgeApplication.getContext().getResources().getDimension(R.dimen.px_x_220), (int) FridgeApplication.getContext().getResources().getDimension(R.dimen.px_y_220)))
                .build();
        PipelineDraweeController controller = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                .setOldController(mSimpleDraweeView.getController())
                .setImageRequest(request)
                .build();
        mSimpleDraweeView.setController(controller);
        mPresenter.checkStatus();// 检查登录状态
    }

    @Override
    public void showMiQRCode(Bitmap bitmap) {
        mProgressBar.setVisibility(View.GONE);
        mSimpleDraweeView.setVisibility(View.VISIBLE);
        // 生成二维码
        if (bitmap == null) return;
        mSimpleDraweeView.setImageBitmap(bitmap);
    }

    @Override
    public void showRetry() { // 显示重试
        mSimpleDraweeView.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.GONE);
        mRetryTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoading() { // 显示 Loading
        if (getActivity() != null) {
            mDialog = new BaseProgressDialog(getActivity(), FridgeApplication.getContext().getResources().getString(R.string.management_login_in));
            mDialog.show();
        }
    }

    @Override
    public void loginFail(String message) { // 登录失败
        logUtil.e(TAG, message);
        ToastUtil.showCenter(FridgeApplication.getContext(), message);
        if (mDialog != null && mDialog.isShowing()) mDialog.dismiss();
        if (mPresenter != null) mPresenter.loadQRCode();// 重新加载二维码
    }

    @Override
    public void loginSuccess() { // 登录成功
        logUtil.d(TAG, "loginSuccess");
        if (mDialog != null && mDialog.isShowing()) mDialog.dismiss();
        ToastUtil.showCenter(FridgeApplication.getContext(), FridgeApplication.getContext().getResources().getString(R.string.management_login_success));
        RxBus.getInstance().post(BusEvent.MSG_LOGIN_SUCCESS);
        // 登录成功，上报属性
        DeviceParams params = SerialManager.getInstance().getDeviceParamsSet();
        MiotRepository.getInstance().sendPropertyMode(params.getMode());
        MiotRepository.getInstance().sendPropertyRCRoomEnable(params.isCold_switch());
        MiotRepository.getInstance().sendPropertyRCSetTemp(params.getCold_temp_set());
        if (FridgePreference.getInstance().getModel().equals(AppConstants.MODEL_X5)) {
            MiotRepository.getInstance().sendPropertyCCRoomEnable(params.isChangeable_switch());
            MiotRepository.getInstance().sendPropertyCCSetTemp(params.getChangeable_temp_set());
        }
        MiotRepository.getInstance().sendPropertyFZSetTemp(params.getFreezing_temp_set());
        // 设置推送
        PushManager.getInstance().setMallPushEnable(new PreferenceHelper(FridgeApplication.getContext()).getPushSetting().isMallEnable());
        dismiss();
    }

    @OnClick(R.id.login_retry)
    public void retry() { // 重试
        mProgressBar.setVisibility(View.VISIBLE);
        mRetryTextView.setVisibility(View.GONE);
        if (mPresenter != null) {
            if (mIsBind) mPresenter.loadMiQRCode();
            else mPresenter.loadQRCode();
        }
    }

    @OnClick(R.id.login_download_app)
    public void showDownload() { // 进入下载云米商城 Ui
        mQRCodeLinearLayout.setVisibility(View.GONE);
        mDownloadLinearLayout.setVisibility(View.VISIBLE);
        mQRCodeLinearLayout.setAnimation(AnimationUtils.makeOutAnimation(FridgeApplication.getContext(), false));
        mDownloadLinearLayout.setAnimation(AnimationUtils.makeInAnimation(FridgeApplication.getContext(), false));
    }

    @OnClick(R.id.login_back)
    public void hideDownload() { // 进入二维码登录 Ui
        mQRCodeLinearLayout.setVisibility(View.VISIBLE);
        mDownloadLinearLayout.setVisibility(View.GONE);
        mQRCodeLinearLayout.setAnimation(AnimationUtils.makeInAnimation(FridgeApplication.getContext(), true));
        mDownloadLinearLayout.setAnimation(AnimationUtils.makeOutAnimation(FridgeApplication.getContext(), true));
    }

    @OnClick(R.id.login_guide)
    public void guide() { // 进入登录指引
        Intent intent = new Intent(getActivity(), LoginGuideActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.login_qr_code)
    public void refresh() { // 点击刷新二维码
        mProgressBar.setVisibility(View.VISIBLE);
        mRetryTextView.setVisibility(View.GONE);
        mPresenter.unSubscribe();
        mPresenter.subscribe(this);
        if (mIsBind) mPresenter.loadMiQRCode();
        else mPresenter.loadQRCode();
    }
}
