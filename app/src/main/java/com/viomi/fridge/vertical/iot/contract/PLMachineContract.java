package com.viomi.fridge.vertical.iot.contract;

import com.viomi.fridge.vertical.common.callback.BasePresenter;
import com.viomi.fridge.vertical.iot.model.http.entity.PLMachineProp;

/**
 * 管线机 Contract
 * Created by William on 2018/2/8.
 */
public interface PLMachineContract {
    interface View {
        void refreshUi(PLMachineProp prop);// 刷新 Ui

        void setIsSetting();
    }

    interface Presenter extends BasePresenter<View> {
        void getProp(String did);// GetProp 请求

        void setTemp(String did, int temp);// 设置温水键温度
    }
}
