package com.viomi.fridge.vertical.home.presenter;


import android.content.Context;
import android.text.TextUtils;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.viomi.fridge.vertical.album.model.AlbumRepository;
import com.viomi.fridge.vertical.common.http.ApiClient;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.cookbook.model.CookMenuCategory;
import com.viomi.fridge.vertical.cookbook.model.CookMenuDetail;
import com.viomi.fridge.vertical.cookbook.model.CookMenuMethod;
import com.viomi.fridge.vertical.cookbook.model.CookMenuSearch;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;
import com.viomi.fridge.vertical.fridgecontrol.model.repository.FridgeRepository;
import com.viomi.fridge.vertical.home.contract.MainContract;
import com.viomi.fridge.vertical.home.model.WeatherDetailEntity;
import com.viomi.fridge.vertical.messageboard.model.MessageBoardRepository;

import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;
import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * 首页 Presenter
 * Created by William on 2018/1/25.
 */
public class MainPresenter implements MainContract.Presenter, AMapLocationListener {
    private static final String TAG = MainPresenter.class.getSimpleName();
    private CompositeSubscription mCompositeSubscription;
    private CookMenuCategory mCookMenuCategory;// 菜谱分类
    private Context mContext;
    private boolean mIsLocated = false;// 是否已定位
    private AMapLocationClient mLocationClient;// 声明 LocationClient 对象
    private int mCount = 0;// 重试次数

    @Nullable
    private MainContract.View mView;

    @Inject
    MainPresenter(Context context) {
        this.mContext = context;
    }

    @Override
    public void subscribe(MainContract.View view) {
        this.mView = view;
        mCompositeSubscription = new CompositeSubscription();
        loadAlbum();// 读取相册
        loadFridge();// 读取设备数据
        location();// 定位
        loadMessageBoard();// 读取留言版
        loadCookBook();// 读取菜谱
    }

    @Override
    public void unSubscribe() {
        mLocationClient.stopLocation();// 停止定位
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
        mView = null;
    }

    @Override
    public void loadAlbum() {
        Subscription subscription = AlbumRepository.getInstance().getAlbumFromBanner()
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(list -> {
                    if (mView != null) mView.displayAlbum(list);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void loadMessageBoard() {
        Subscription subscription = MessageBoardRepository.getInstance().getMessageBoardList()
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(list -> {
                    if (mView != null) mView.refreshMessageBoard(list);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void loadCookBook() {
        Subscription subscription = Observable.interval(0, 5, TimeUnit.MINUTES)
                .onBackpressureDrop()
                .compose(RxSchedulerUtil.SchedulersTransformer2())
                .onTerminateDetach()
                .subscribe(aLong -> {
                    mCount = 0;
                    if (mCookMenuCategory == null) loadCookbookCategory();
                    else loadCookbookRecommend();
                }, throwable -> {
                    logUtil.e(TAG, throwable.getMessage());
                    retryLoadCookBook();
                });
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void loadCookbookCategory() {
        Subscription subscription = ApiClient.getInstance().getApiService().getCookCategory()
                .compose(RxSchedulerUtil.SchedulersTransformer2())
                .onTerminateDetach()
                .subscribe(cookMenuCategoryMobResult -> {
                    mCookMenuCategory = cookMenuCategoryMobResult.getResult();
                    loadCookbookRecommend();
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void loadCookbookRecommend() {
        CookMenuCategory cookMenuCategory = mCookMenuCategory.getChilds().get(0);
        Random random = new Random();
        int position = random.nextInt(cookMenuCategory.getChilds().size());
        CookMenuCategory cookMenuCategory2 = cookMenuCategory.getChilds().get(position);
        Subscription subscription = ApiClient.getInstance().getApiService().getCookSearch(cookMenuCategory2.getCategoryInfo().getCtgId(),
                null, 1, 1)
                .onTerminateDetach()
                .flatMap(cookMenuSearchMobResult -> {
                    // 获取当前分类菜谱信息
                    CookMenuSearch cookMenuSearch = cookMenuSearchMobResult.getResult();
                    if (cookMenuSearch == null) {
                        return null;
                    }
                    // 菜谱 id 规则：分类 id（10 位数）+ 菜谱编号（10 位数，不重复）
                    // 根据最后一条菜谱id和当前分类的菜谱数量，生成随机的菜谱id
                    int total = cookMenuSearch.getTotal();
                    String lastMenuId = cookMenuSearch.getList().get(0).getMenuId();
                    String ctgId = lastMenuId.substring(0, 10);
                    String menuNum = lastMenuId.substring(10, 20);
                    // 获取随机菜谱id
                    int randomMenuNum = Integer.parseInt(menuNum) - random.nextInt(total);
                    String randomMenuId = ctgId +
                            String.format(Locale.getDefault(), "%010d", randomMenuNum);
                    // 获取菜谱详情
                    return ApiClient.getInstance().getApiService().getCookDetail(randomMenuId);
                })
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(cookMenuDetailMobResult -> {
                    CookMenuDetail cookMenuDetail = cookMenuDetailMobResult.getResult();
                    if (cookMenuDetail == null ||
                            cookMenuDetail.getRecipe() == null ||
                            cookMenuDetail.getRecipe().getImg() == null ||
                            TextUtils.isEmpty(cookMenuDetail.getRecipe().getImg()) ||
                            cookMenuDetail.getRecipe().getMethod() == null ||
                            TextUtils.isEmpty(cookMenuDetail.getRecipe().getMethod())) {
                        if (mCount <= 5) {
                            loadCookbookRecommend();// 重新获取
                            mCount++;
                        }
                    } else { // 筛选缺图
                        Gson gson = new Gson();
                        List<CookMenuMethod> methodList = gson.fromJson(cookMenuDetail.getRecipe().getMethod(),
                                new TypeToken<List<CookMenuMethod>>() {
                                }.getType());
                        boolean isReturn = false;
                        for (CookMenuMethod cookMenuMethod : methodList) {
                            if (cookMenuMethod.getImg() == null || cookMenuMethod.getImg().equals("")
                                    || cookMenuMethod.getStep() == null || cookMenuMethod.getStep().equals("")) {
                                isReturn = true;
                                break;
                            }
                        }
                        if (isReturn) {
                            if (mCount <= 5) {
                                loadCookbookRecommend();// 重新获取
                                mCount++;
                            }
                        } else {
                            if (mView != null) mView.refreshCookbook(cookMenuDetail);
                        }
                    }
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void loadFridge() {
        Subscription subscription = Observable.interval(0, 3, TimeUnit.SECONDS)
                .onBackpressureDrop() // 背压处理
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .map(aLong -> FridgeRepository.getInstance().getComSendData())
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(params -> {
                    if (mView != null && params != null) mView.refreshFridgeUi(params);
                }, throwable -> {
                    logUtil.e(TAG, throwable.getMessage());
                    retryLoadFridge();
                });
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void location() {
        mLocationClient = new AMapLocationClient(mContext);
        // 初始化定位参数
        AMapLocationClientOption locationOption = new AMapLocationClientOption();
        // 设置定位监听
        mLocationClient.setLocationListener(this);
        // 设置定位模式为高精度模式，Battery_Saving 为低功耗模式，Device_Sensors 是仅设备模式
        locationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
        // 设置定位间隔,单位毫秒,默认为 2000 ms
        locationOption.setInterval(2000);
        // 设置定位参数
        mLocationClient.setLocationOption(locationOption);
        // 此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
        // 注意设置合适的定位时间的间隔（最小间隔支持为 1000ms），并且在合适时间调用 stopLocation() 方法来取消定位请求
        // 在定位结束后，在合适的生命周期调用 onDestroy() 方法
        // 在单次定位情况下，定位无论成功与否，都无需调用 stopLocation() 方法移除请求，定位 sdk 内部会移除
        // 启动定位
        mLocationClient.startLocation();
    }

    @Override
    public void loadWeather(String cityName) {
        Subscription subscription = ApiClient.getInstance().getApiService().getWeather(cityName)
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .filter(weatherEntity -> weatherEntity != null && weatherEntity.isSuccess())
                .map(weatherEntity -> {
                    WeatherDetailEntity entity = weatherEntity.getData();
                    FridgePreference.getInstance().saveOutdoorTemp(entity.getNow().getTmp());// 缓存室外温度
                    // 缓存天气预报
                    StringBuilder report = new StringBuilder();
                    report.append(cityName).append("，");
                    if (entity.getDaily_forecast().get(0).getCond().getTxt_d().equals(entity.getDaily_forecast().get(0).getCond().getTxt_n())) {
                        report.append(entity.getDaily_forecast().get(0).getCond().getTxt_d());
                    } else
                        report.append(entity.getDaily_forecast().get(0).getCond().getTxt_d()).append("转")
                                .append(entity.getDaily_forecast().get(0).getCond().getTxt_n());
                    report.append(",").append(entity.getDaily_forecast().get(0).getTmp().getMin().replace("℃", "")).append("℃~")
                            .append(entity.getDaily_forecast().get(0).getTmp().getMax().replace("℃", "")).append("℃，")
                            .append(entity.getDaily_forecast().get(0).getWind().getDir()).append(entity.getDaily_forecast().get(0).getWind().getSc()).append("级，")
                            .append("空气质量：").append(entity.getAqi().getCity().getQlty()).append("，Pm2.5：").append(entity.getAqi().getCity().getPm25());
                    FridgePreference.getInstance().saveWeatherReport(report.toString());// 缓存天气预报
                    FridgePreference.getInstance().saveAirQuality(entity.getAqi().getCity().getQlty());// 空气质量
                    FridgePreference.getInstance().savePM25(String.valueOf(entity.getAqi().getCity().getPm25()));// PM2.5
                    return entity;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(entity -> {
                    if (entity != null && mView != null) {
                        mIsLocated = true;
                        mView.refreshWeather(entity);
                    }
                }, throwable -> {
                    mIsLocated = false;
                    logUtil.e(TAG, throwable.getMessage());
                });
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        String cityName = aMapLocation.getCity();// 城市名称
        String cityCode = aMapLocation.getCityCode();// 城市编码
        if (cityName == null || cityName.equals("")) return;
        FridgePreference.getInstance().saveCity(cityName);// 缓存城市名称
        FridgePreference.getInstance().saveCityCode(cityCode);// 缓存城市编码
        FridgePreference.getInstance().saveLatitude(String.valueOf(aMapLocation.getLatitude()));// 纬度
        FridgePreference.getInstance().saveLongitude(String.valueOf(aMapLocation.getLongitude()));// 经度
        if (mView != null) mView.refreshCity(cityName);
        if (!mIsLocated) loadWeather(cityName);
    }

    /**
     * 读取冰箱数据异常重试
     */
    private void retryLoadFridge() {
        Subscription subscription = Observable.timer(3, TimeUnit.SECONDS)
                .compose(RxSchedulerUtil.SchedulersTransformer2())
                .onTerminateDetach()
                .subscribe(aLong -> loadFridge(), throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    /**
     * 读取菜谱数据异常重试
     */
    private void retryLoadCookBook() {
        Subscription subscription = Observable.timer(5, TimeUnit.MINUTES)
                .compose(RxSchedulerUtil.SchedulersTransformer2())
                .onTerminateDetach()
                .subscribe(aLong -> loadCookBook(), throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }
}
