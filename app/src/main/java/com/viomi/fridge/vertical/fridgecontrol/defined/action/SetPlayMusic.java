package com.viomi.fridge.vertical.fridgecontrol.defined.action;

import com.viomi.fridge.vertical.fridgecontrol.defined.ViomiDefined;
import com.viomi.fridge.vertical.fridgecontrol.defined.property.PlayMusic;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetPlayMusic extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setPlayMusic.toActionType();

    public SetPlayMusic() {
        super(TYPE);

        super.addArgument(PlayMusic.TYPE.toString());
    }
}