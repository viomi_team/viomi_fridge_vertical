package com.viomi.fridge.vertical.fridgecontrol.presenter;

import android.content.Context;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.callback.AppCallback;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.ToastUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.fridgecontrol.contract.FridgeContract;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.entity.ChangeableScene;
import com.viomi.fridge.vertical.fridgecontrol.model.repository.FridgeRepository;
import com.viomi.fridge.vertical.fridgecontrol.model.serial.manager.SerialManager;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * 冰箱控制 Presenter
 * Created by William on 2018/2/2.
 */
public class FridgePresenter implements FridgeContract.Presenter {
    private static final String TAG = FridgePresenter.class.getSimpleName();
    private CompositeSubscription mCompositeSubscription;// 统一管理消息订阅
    private Subscription mTimerSubscription;// 每次设置停止刷新页面 1S
    private final String MODE_SMART = "smart";// 智能模式
    private final String MODE_HOLIDAY = "holiday";// 假日模式
    private Context mContext;

    @Nullable
    private FridgeContract.View mView;

    public FridgePresenter(Context context) {
        mContext = context;
    }

    @Override
    public void subscribe(FridgeContract.View view) {
        this.mView = view;
        if (mCompositeSubscription == null) mCompositeSubscription = new CompositeSubscription();
        loadTips();
        loadFridgeData();
    }

    @Override
    public void unSubscribe() {
        this.mView = null;
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
    }

    @Override
    public void loadTips() {
        Subscription subscription = Observable.interval(0, 10, TimeUnit.SECONDS)
                .onBackpressureDrop() // 背压处理
                .subscribeOn(Schedulers.computation())
                .onTerminateDetach()
                .map(aLong -> FridgeRepository.getInstance().randomTip())
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(s -> {
                    if (mView != null) mView.refreshTip(s);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void loadFridgeData() {
        Subscription subscription = Observable.interval(0, 3, TimeUnit.SECONDS)
                .onBackpressureDrop() // 背压处理
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .map(aLong -> FridgeRepository.getInstance().getComSendData())
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(s -> {
                    if (mView != null)
                        mView.refreshUi(FridgeRepository.getInstance().getComSendData());
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void loadDisplayScene(List<ChangeableScene> allList, List<ChangeableScene> displayList, List<ChangeableScene> backupList) {
        allList.clear();
        displayList.clear();
        backupList.clear();
        Subscription subscription = Observable.just(FridgePreference.getInstance().getSceneList())
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .onTerminateDetach()
                .map(list -> {
                    String[] nameArray = mContext.getResources().getStringArray(R.array.changeable_scene);
                    String[] tipArray = mContext.getResources().getStringArray(R.array.changeable_scene_tip);
                    int[] tempArray = mContext.getResources().getIntArray(R.array.changeable_scene_temp);
                    // 所有场景
                    for (int i = 0; i < nameArray.length; i++) {
                        ChangeableScene scene = new ChangeableScene();
                        scene.setScene(nameArray[i]);
                        scene.setTip(tipArray[i]);
                        scene.setTemp(tempArray[i]);
                        allList.add(scene);
                    }
                    // 显示场景
                    if (list.size() == 0) {
                        for (int i = 0; i < 6; i++) {
                            ChangeableScene scene = new ChangeableScene();
                            scene.setScene(allList.get(i).getScene());
                            scene.setTip(allList.get(i).getTip());
                            scene.setTemp(allList.get(i).getTemp());
                            displayList.add(scene);
                        }
                    } else displayList.addAll(list);
                    // 备选区
                    for (int i = 0; i < allList.size(); i++) {
                        if (!displayList.contains(allList.get(i))) {
                            backupList.add(allList.get(i));
                        }
                    }
                    return true;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(result -> {
                    if (mView != null) mView.refreshScene();
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        if (mCompositeSubscription == null) {
            mCompositeSubscription = new CompositeSubscription();
        }
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void setMode(int mode, boolean enable) {
        setIsSetting();
        Subscription subscription = Observable.just(mode)
                .observeOn(Schedulers.io())
                .onTerminateDetach()
                .subscribe(integer -> {
                    if (integer == AppConstants.MODE_SMART)
                        FridgeRepository.getInstance().setMode(MODE_SMART, null);
                    else if (integer == AppConstants.MODE_HOLIDAY)
                        FridgeRepository.getInstance().setMode(MODE_HOLIDAY, null);
                    else if (integer == AppConstants.MODE_QUICK_COLD)
                        FridgeRepository.getInstance().setSmartCool(enable, null);
                    else if (integer == AppConstants.MODE_QUICK_FREEZE)
                        FridgeRepository.getInstance().setSmartFreeze(enable, null);
                    else FridgeRepository.getInstance().setMode("none", null);
                    logUtil.d(TAG, "setMode " + mode);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void setOneKeyClean(boolean enable) {
        setIsSetting();
        Subscription subscription = Observable.just(enable)
                .observeOn(Schedulers.io())
                .onTerminateDetach()
                .subscribe(aBoolean -> {
                    FridgeRepository.getInstance().setOneKeyClean(enable);
                    logUtil.d(TAG, "setOneKeyClean " + enable);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void setColdSwitch(boolean enable) {
        setIsSetting();
        Subscription subscription = Observable.just(enable)
                .observeOn(Schedulers.io())
                .onTerminateDetach()
                .subscribe(aBoolean -> {
                    FridgeRepository.getInstance().setRCSet(enable, null);
                    logUtil.d(TAG, "setColdSwitch " + enable);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void setChangeableSwitch(boolean enable) {
        setIsSetting();
        Subscription subscription = Observable.just(enable)
                .observeOn(Schedulers.io())
                .onTerminateDetach()
                .subscribe(aBoolean -> {
                    FridgeRepository.getInstance().setCCSet(enable, null);
                    logUtil.d(TAG, "setChangeableSwitch " + enable);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void setColdTemp(int temp) {
        setIsSetting();
        Subscription subscription = Observable.just(temp)
                .observeOn(Schedulers.io())
                .onTerminateDetach()
                .subscribe(aBoolean -> {
                    FridgeRepository.getInstance().setRCRoomTemp(temp, null);
                    logUtil.d(TAG, "setColdTemp " + temp);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void setChangeableTemp(int temp) {
        setIsSetting();
        AppCallback<String> callback = new AppCallback<String>() {
            @Override
            public void onSuccess(String data) {
                FridgePreference.getInstance().saveScene("");
                RxBus.getInstance().post(BusEvent.MSG_CHANGEABLE_SCENE_UPDATE);
            }

            @Override
            public void onFail(int errorCode, String msg) {

            }
        };
        Subscription subscription = Observable.just(temp)
                .observeOn(Schedulers.io())
                .onTerminateDetach()
                .subscribe(aBoolean -> {
                    FridgeRepository.getInstance().setCCRoomTemp(temp, callback);
                    logUtil.d(TAG, "setChangeableTemp " + temp);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void setFreezingTemp(int temp) {
        setIsSetting();
        Subscription subscription = Observable.just(temp)
                .observeOn(Schedulers.io())
                .onTerminateDetach()
                .subscribe(aBoolean -> {
                    FridgeRepository.getInstance().setFCRoomTemp(temp, null);
                    logUtil.d(TAG, "setFreezingTemp " + temp);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void saveDisplayScenes(List<ChangeableScene> list) {
        Subscription subscription = Observable.just(FridgePreference.getInstance().saveSceneList(list))
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(s -> {
                    if (mView != null) mView.refreshScene();
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void setScene(List<ChangeableScene> list, ChangeableScene scene) {
        AppCallback<String> callback = new AppCallback<String>() {
            @Override
            public void onSuccess(String data) {
                FridgePreference.getInstance().saveScene(scene.getScene());
            }

            @Override
            public void onFail(int errorCode, String msg) {
                RxBus.getInstance().post(BusEvent.MSG_CHANGEABLE_SCENE_UPDATE);
            }
        };
        Subscription subscription = Observable.just(FridgeRepository.getInstance().setCCRoomTemp(switchTempWithScene(list, scene.getScene()), callback))
                .compose(RxSchedulerUtil.SchedulersTransformer2())
                .onTerminateDetach()
                .subscribe(aBoolean -> logUtil.d(TAG, "set scene " + aBoolean), throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void setFreshFruit(boolean enable) {
        setIsSetting();
        Subscription subscription = Observable.just(enable)
                .observeOn(Schedulers.io())
                .onTerminateDetach()
                .subscribe(aBoolean -> {
                    FridgeRepository.getInstance().setFreshFruit(enable, null);
                    logUtil.d(TAG, "setFreshFruit " + enable);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void setRetainFresh(boolean enable) {
        setIsSetting();
        Subscription subscription = Observable.just(enable)
                .observeOn(Schedulers.io())
                .onTerminateDetach()
                .subscribe(aBoolean -> {
                    FridgeRepository.getInstance().setRetainFresh(enable, null);
                    logUtil.d(TAG, "setRetainFresh " + enable);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void setIced(boolean enable) {
        setIsSetting();
        Subscription subscription = Observable.just(enable)
                .observeOn(Schedulers.io())
                .onTerminateDetach()
                .subscribe(aBoolean -> {
                    FridgeRepository.getInstance().setIced(enable, null);
                    logUtil.d(TAG, "setIced " + enable);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public boolean isInspectionRunning() {
        if (FridgeRepository.getInstance().isInspectionRunning()) {
            ToastUtil.showCenter(mContext, mContext.getResources().getString(R.string.fridge_commodity_inspection_running));
            return true;
        }
        return false;
    }

    /**
     * 是否通讯故障
     */
    @Override
    public boolean isCommunicationError() {
        if (SerialManager.getInstance().getDeviceParamsGet().getDeviceError().isError_communication()) {
            //     ToastUtil.showCenter(mContext, mContext.getResources().getString(R.string.fridge_communication_error_tip));
            return true;
        }
        return false;
    }

    @Override
    public boolean isExistError() {
        if (SerialManager.getInstance().getDeviceParamsGet().getError() != 0) {
            ToastUtil.showCenter(mContext, mContext.getResources().getString(R.string.fridge_error_tip));
            return true;
        }
        return false;
    }

    private void setIsSetting() {
        if (mView != null) mView.setIsSetting(true);
        if (mTimerSubscription != null) mTimerSubscription.unsubscribe();
        mTimerSubscription = Observable.timer(1, TimeUnit.SECONDS)
                .compose(RxSchedulerUtil.SchedulersTransformer4())
                .onTerminateDetach()
                .subscribe(aLong -> {
                    if (mView != null) mView.setIsSetting(false);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(mTimerSubscription);
    }

    private int switchTempWithScene(List<ChangeableScene> list, String name) {
        int temp = 0;
        for (int i = 0; i < list.size(); i++) {
            if (name.equals(list.get(i).getScene())) {
                temp = list.get(i).getTemp();
                break;
            }
        }
        return temp;
    }
}