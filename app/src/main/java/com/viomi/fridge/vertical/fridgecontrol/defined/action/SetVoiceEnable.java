package com.viomi.fridge.vertical.fridgecontrol.defined.action;

import com.viomi.fridge.vertical.fridgecontrol.defined.ViomiDefined;
import com.viomi.fridge.vertical.fridgecontrol.defined.property.VoiceEnable;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetVoiceEnable extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setVoiceEnable.toActionType();

    public SetVoiceEnable() {
        super(TYPE);

        super.addArgument(VoiceEnable.TYPE.toString());
    }
}