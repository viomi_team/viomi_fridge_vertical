package com.viomi.fridge.vertical.messageboard.module;

import com.viomi.fridge.vertical.common.scope.ActivityScoped;
import com.viomi.fridge.vertical.messageboard.contract.MessageBoardListContract;
import com.viomi.fridge.vertical.messageboard.presenter.MessageBoardListPresenter;

import dagger.Binds;
import dagger.Module;

/**
 * 留言板列表 Module
 * Created by William on 2018/4/11.
 */
@Module
public abstract class MessageBoardListModule {

    @ActivityScoped
    @Binds
    abstract MessageBoardListContract.Presenter messageBoardListPresenter(MessageBoardListPresenter presenter);
}