package com.viomi.fridge.vertical.fridgecontrol.defined.action;

import com.viomi.fridge.vertical.fridgecontrol.defined.ViomiDefined;
import com.viomi.fridge.vertical.fridgecontrol.defined.property.Mode;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetMode extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setMode.toActionType();

    public SetMode() {
        super(TYPE);

        super.addArgument(Mode.TYPE.toString());
    }
}