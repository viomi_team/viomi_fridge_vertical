package com.viomi.fridge.vertical.iot.model.http.entity;

/**
 * 滤芯信息
 * Created by William on 2018/2/5.
 */

public class WaterPurifierFilter {
    private String name;// 滤芯名称
    private String desc;// 滤芯描述
    private int percent;// 滤芯剩余

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }
}
