package com.viomi.fridge.vertical.speech.view.dialog;

import android.app.Dialog;
import android.view.View;
import android.widget.ListView;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.CommonDialog;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;
import com.viomi.fridge.vertical.speech.view.adapter.SpeechTipAdapter;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 语音指令指引 Dialog
 * Created by William on 2018/1/31.
 */
public class SpeechTipDialog extends CommonDialog {

    @BindView(R.id.speech_tip_list)
    ListView mListView;

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.dialog_speech_tip;
    }

    @Override
    protected void initWithOnCreateDialog(View view) {
        String model = FridgePreference.getInstance().getModel();
        String[] array;
        if (model.equals(AppConstants.MODEL_X5)) {
            array = FridgeApplication.getContext().getResources().getStringArray(R.array.speech_tip_list);
        } else {
            array = FridgeApplication.getContext().getResources().getStringArray(R.array.speech_tip_no_changeable_list);
        }
        List<String> list = Arrays.asList(array);
        SpeechTipAdapter adapter = new SpeechTipAdapter(getActivity(), list);
        mListView.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) dialog.setCanceledOnTouchOutside(false);
    }

    @OnClick(R.id.speech_tip_confirm)
    public void confirm() {
        dismiss();
    }
}