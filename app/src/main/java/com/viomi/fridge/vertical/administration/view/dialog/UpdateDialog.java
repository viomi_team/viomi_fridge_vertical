package com.viomi.fridge.vertical.administration.view.dialog;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.contract.UpdateContract;
import com.viomi.fridge.vertical.administration.model.entity.AppUpdateResult;
import com.viomi.fridge.vertical.administration.model.preference.ManagePreference;
import com.viomi.fridge.vertical.administration.presenter.UpdatePresenter;
import com.viomi.fridge.vertical.common.base.CommonDialog;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.ToastUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.widget.ProgressWheel;
import com.viomi.widget.dialog.BaseAlertDialog;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 版本升级 Dialog
 * Created by William on 2018/2/22.
 */
public class UpdateDialog extends CommonDialog implements UpdateContract.View {
    private static final String TAG = UpdateDialog.class.getSimpleName();
    public final String VERSION = "version";
    private String mUrl;// App 下载链接
    private int mVersion;// 版本号
    private boolean mIsDowning = false;// 是否正在下载
    private UpdateContract.Presenter mPresenter;

    @BindView(R.id.update_tip)
    TextView mTipTextView;// 提示文字
    @BindView(R.id.update_old_version)
    TextView mCurrentVersionTextView;// 当前版本
    @BindView(R.id.update_new_version)
    TextView mNewVersionTextView;// 最新版本
    @BindView(R.id.update_description)
    TextView mDescTextView;// 描述
    @BindView(R.id.update_button)
    TextView mUpdateTextView;// 升级

    @BindView(R.id.update_status_view)
    ImageView mTipImageView;// 提示图标

    @BindView(R.id.update_content_layout)
    LinearLayout mUpdateLinearLayout;// 更新布局

    @BindView(R.id.update_progress_bar)
    ProgressWheel mProgressWheel;// 更新进度条

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.dialog_update;
    }

    @Override
    protected void initWithOnCreateDialog(View view) {
        mPresenter = new UpdatePresenter(FridgeApplication.getContext());
        mPresenter.subscribe(this);// 订阅
        mVersion = getArguments() == null ? 0 : getArguments().getInt(VERSION);
        if (mPresenter != null) mPresenter.checkAppUpdate();// 检查更新
        mProgressWheel.setProgress(0);
        mIsDowning = false;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().setCanceledOnTouchOutside(false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mPresenter != null) {
            mPresenter.unSubscribe();// 取消订阅
            mPresenter = null;
        }
    }

    @OnClick(R.id.update_close)
    public void close() { // 关闭
        if (mIsDowning) {
            if (getActivity() != null) {
                BaseAlertDialog dialog = new BaseAlertDialog(getActivity(), FridgeApplication.getContext().getResources().getString(R.string.management_update_quit),
                        FridgeApplication.getContext().getResources().getString(R.string.management_update_quit_confirm), FridgeApplication.getContext().getResources().getString(R.string.management_update_quit_cancel));
                dialog.setOnLeftClickListener(() -> {
                    dialog.dismiss();
                    dismiss();
                });
                dialog.setOnRightClickListener(dialog::dismiss);
                dialog.show();
            }
        } else dismiss();
    }

    @Override
    public void refreshUi(AppUpdateResult result) {
        mTipTextView.setVisibility(View.GONE);
        mUpdateLinearLayout.setVisibility(View.VISIBLE);
        if (result.getData().size() == 0 || result.getData().get(0).getCode() == 0 || result.getData().get(0).getCode() <= mVersion) {
            mCurrentVersionTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.management_update_current_version_value), mVersion));
            mDescTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.management_update_up_to_date_tip));
            ManagePreference.getInstance().saveAppUpdate(false);
        } else {
            mCurrentVersionTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.management_update_current_version_value), mVersion));
            mNewVersionTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.management_update_up_to_date_version_value), result.getData().get(0).getCode()));
            String str = FridgeApplication.getContext().getResources().getString(R.string.management_update_statement) + result.getData().get(0).getDetail();
            mDescTextView.setText(str);
            mUpdateTextView.setVisibility(View.VISIBLE);
            mUrl = result.getData().get(0).getUrl();
            ManagePreference.getInstance().saveAppUpdate(true);
        }
        RxBus.getInstance().post(BusEvent.MSG_VERSION_UPDATE);
    }

    @Override
    public void refreshProgress(int progress) {
        logUtil.d(TAG, progress + "");
        if (progress < 0) return;
        mProgressWheel.setProgress(progress);
    }

    @Override
    public void install(String path) {
        if (path == null) {
            mIsDowning = false;
            mUpdateTextView.setVisibility(View.VISIBLE);
            mTipImageView.setVisibility(View.VISIBLE);
            mProgressWheel.setVisibility(View.GONE);
            mProgressWheel.setProgress(0);
            ToastUtil.showCenter(FridgeApplication.getContext(), FridgeApplication.getContext().getResources().getString(R.string.management_update_fail));
        } else {
            mTipTextView.setVisibility(View.VISIBLE);
            mUpdateLinearLayout.setVisibility(View.GONE);
            mTipTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.management_update_finish));
            // 开始静默安装
            Intent mIntent = new Intent();
            mIntent.setAction("android.intent.action.SILENCE_INSTALL");
            mIntent.putExtra("apkPath", path);
            if (getActivity() != null) getActivity().sendBroadcast(mIntent);
        }
    }

    @OnClick(R.id.update_button)
    public void download() { // 下载
        mTipImageView.setVisibility(View.GONE);
        mProgressWheel.setVisibility(View.VISIBLE);
        mPresenter.downloadApp(mUrl);
        mUpdateTextView.setVisibility(View.GONE);
        mIsDowning = true;
    }
}
