package com.viomi.fridge.vertical.speech.view.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.FragmentTransaction;
import android.widget.TextView;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseFragment;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.scope.ActivityScoped;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.speech.contract.SpeechManageContract;
import com.viomi.fridge.vertical.speech.model.preference.SpeechPreference;
import com.viomi.fridge.vertical.speech.presenter.SpeechManagePresenter;
import com.viomi.fridge.vertical.speech.view.dialog.SpeechTipDialog;
import com.viomi.widget.switchbutton.SwitchButton;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import cn.com.viomi.ailib.api.VioAI;

/**
 * 语音助手设置 Fragment
 * Created by William on 2018/1/31.
 */
@ActivityScoped
public class SpeechFragment extends BaseFragment implements SpeechManageContract.View {
    private static final String TAG = SpeechFragment.class.getSimpleName();
    private SpeechTipDialog mSpeechTipDialog;// 语音指引 Dialog
    private SpeechManageContract.Presenter mPresenter;

    @BindView(R.id.manage_voice_switch)
    SwitchButton mSwitchButton;// 语音开关

    @BindView(R.id.manage_void_status)
    TextView mTextView;// 语音状态

    @Inject
    public SpeechFragment() {

    }

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.fragment_speech;
    }

    @Override
    protected void initWithOnCreateView() {
        mPresenter = new SpeechManagePresenter();
        mPresenter.subscribe(this);// 订阅

        mSwitchButton.setOnSwitchStateChangeListener(isOn -> {
            mPresenter.cacheSwitch(isOn);
            if (isOn) {
                if (SpeechPreference.getInstance().isAuth()) VioAI.enableWakeup();
            } else {
                if (SpeechPreference.getInstance().isAuth()) VioAI.disableWakeup();
            }
            mTextView.setText(isOn ? getResources().getString(R.string.speech_opened) :
                    getResources().getString(R.string.speech_closed));
        });
        registerReceiver();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mPresenter != null) {
            mPresenter.unSubscribe();// 取消订阅
            mPresenter = null;
        }
        if (mSpeechTipDialog != null && mSpeechTipDialog.isAdded()) {
            mSpeechTipDialog.dismiss();
            mSpeechTipDialog = null;
        }
        if (getActivity() != null) getActivity().unregisterReceiver(mReceiver);
    }

    @Override
    public void showSetting(boolean enable) {
        logUtil.d(TAG, "showSetting");
        mSwitchButton.setOn(enable, true);
        mTextView.setText(enable ? getResources().getString(R.string.speech_opened) : getResources().getString(R.string.speech_closed));
    }

    @OnClick(R.id.manage_wake_up_manually)
    public void wakeupManually() { // 手动唤醒
        if (isRepeatedClick()) return;
        if (SpeechPreference.getInstance().isAuth()) {
            VioAI.getInstance().wakeupByHand("");
            RxBus.getInstance().post(BusEvent.MSG_WAKE_UP_MANUAL, "有什么可以帮你");
        }
    }

    @OnClick(R.id.manage_more_instruction)
    public void showInstruction() { // 查看指令
        if (mSpeechTipDialog == null) mSpeechTipDialog = new SpeechTipDialog();
        if (mSpeechTipDialog.isAdded()) return;
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        mSpeechTipDialog.show(ft, TAG);
    }

    private void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(AppConstants.ACTION_ENABLE_SPEECH);
        intentFilter.addAction(AppConstants.ACTION_DISABLE_SPEECH);
        if (getActivity() != null) {
            getActivity().registerReceiver(mReceiver, intentFilter);
        }
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            logUtil.d(TAG, "onReceive,action=" + intent.getAction());
            if (intent.getAction() != null) {
                switch (intent.getAction()) {
                    case AppConstants.ACTION_ENABLE_SPEECH:
                        showSetting(true);
                        break;
                    case AppConstants.ACTION_DISABLE_SPEECH:
                        showSetting(false);
                        break;
                }
            }
        }
    };
}
