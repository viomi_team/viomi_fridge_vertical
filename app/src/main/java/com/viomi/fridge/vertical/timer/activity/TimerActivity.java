package com.viomi.fridge.vertical.timer.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseActivity;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.PreferenceHelper;
import com.viomi.fridge.vertical.common.util.ToastUtil;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.timer.service.TimerService;
import com.viomi.fridge.vertical.timer.view.dialog.SelectMusicDialog;
import com.viomi.widget.wheelview.adapter.ArrayWheelAdapter;
import com.viomi.widget.wheelview.widget.WheelView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import rx.Subscription;

/**
 * 计时器 Activity
 * Created by nanquan on 2018/1/23.
 */
public class TimerActivity extends BaseActivity implements SelectMusicDialog.OnMusicClickListener, SelectMusicDialog.OnConfirmClickListener {
    private static final String TAG = TimerActivity.class.getSimpleName();
    private int mSelectHour;// 选择小时
    private int mSelectMinute;// 选择分钟
    private View mSelectCookTab;// 选择类型
    private SelectMusicDialog mMusicDialog;// 选择音乐 Dialog
    private Subscription mSubscription;
    private int mCurrentDuration;  // 当前计时
    private TimerState currentTimerState = TimerState.IDLE;  // 当前状态

    @BindView(R.id.wv_hour)
    WheelView<String> mHourWheelView;// 小时
    @BindView(R.id.wv_minute)
    WheelView<String> mMinuteWheelView;// 分钟

    @BindView(R.id.layout_cook_tab)
    View layoutCookTab;

    @BindView(R.id.tv_timing)
    TextView mTimingTextView;// 计时时间
    @BindView(R.id.timer_select_music)
    TextView mSelectMusicTextView;// 选择音乐

    @BindView(R.id.btn_right)
    Button mRightButton;

    @BindView(R.id.rl_cook_tab1)
    RelativeLayout rl_cook_tab1;
    @BindView(R.id.rl_cook_tab2)
    RelativeLayout rl_cook_tab2;
    @BindView(R.id.rl_cook_tab3)
    RelativeLayout rl_cook_tab3;
    @BindView(R.id.rl_cook_tab4)
    RelativeLayout rl_cook_tab4;
    @BindView(R.id.rl_cook_tab5)
    RelativeLayout rl_cook_tab5;
    @BindView(R.id.rl_cook_tab6)
    RelativeLayout rl_cook_tab6;

    public enum TimerState {
        IDLE, TIMING, PAUSE
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        layoutId = R.layout.activity_timer;
        mTitle = getResources().getString(R.string.timer_title);
        super.onCreate(savedInstanceState);
        initWheelView();
        initTypeFace();
        initDialog();
        initSubscription();
        autoStart(null);
        // 判断定时器服务是否在运行
        if (!ToolUtil.isServiceWork(TimerActivity.this, "com.viomi.fridge.vertical.timer.service.TimerService")) {
            logUtil.d(TAG, "restart service");
            startService(new Intent(this, TimerService.class));
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        autoStart(intent);
    }

    private void initWheelView() {
        // 样式
        WheelView.WheelViewStyle style = new WheelView.WheelViewStyle();
        style.backgroundColor = getResources().getColor(R.color.color_theme);
        style.selectedTextColor = Color.WHITE;
        style.textColor = Color.WHITE;
        style.textSize = 60;
        style.selectedTextSize = 120;
        style.itemPadding = 0;
        style.itemMargin = 0;
        style.itemHeight = 120;
        // 小时
        mHourWheelView.setWheelData(createHours());
        mHourWheelView.setWheelAdapter(new ArrayWheelAdapter<>(this));
        mHourWheelView.setSkin(WheelView.Skin.None);
        mHourWheelView.setLoop(true);
        mHourWheelView.setWheelSize(5);
        mHourWheelView.setStyle(style);
        // 分钟
        mMinuteWheelView.setWheelData(createMinutes());
        mMinuteWheelView.setWheelAdapter(new ArrayWheelAdapter<>(this));
        mMinuteWheelView.setSkin(WheelView.Skin.None);
        mMinuteWheelView.setLoop(true);
        mMinuteWheelView.setWheelSize(5);
        mMinuteWheelView.setStyle(style);
        // 监听选中
        mHourWheelView.setOnWheelItemSelectedListener((position, s) -> mSelectHour = Integer.parseInt(s));
        mMinuteWheelView.setOnWheelItemSelectedListener((position, s) -> mSelectMinute = Integer.parseInt(s));
        // 选择铃声
        PreferenceHelper preferenceHelper = new PreferenceHelper(this);
        int position = preferenceHelper.getSelectedTimeoutRing();
        String str = getResources().getString(R.string.timer_bell) + (position + 1);
        mSelectMusicTextView.setText(str);
    }

    private ArrayList<String> createHours() {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < 24; i++) {
            if (i < 10) {
                list.add("0" + i);
            } else {
                list.add(String.valueOf(i));
            }
        }
        return list;
    }

    private ArrayList<String> createMinutes() {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < 60; i++) {
            if (i < 10) {
                list.add("0" + i);
            } else {
                list.add(String.valueOf(i));
            }
        }
        return list;
    }

    private void initTypeFace() {
        mTimingTextView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/DINCond-Medium.otf"));
    }

    public void initDialog() {
        mMusicDialog = new SelectMusicDialog(this);
        mMusicDialog.setOnMusicClickListener(this);
        mMusicDialog.setOnConfirmClickListener(this);
    }

    private void initSubscription() {
        mSubscription = RxBus.getInstance()
                .subscribe(event -> {
                    switch (event.getMsgId()) {
                        case BusEvent.MSG_TIMER_TIMING: {
                            int duration = (int) event.getMsgObject();
                            setTiming(duration);
                            if (duration == 0) {
                                refreshTimerState(TimerState.IDLE);
                            }
                        }
                        break;
                        case BusEvent.MSG_TIMER_SPEECH_STOP:
                            runOnUiThread(() -> refreshTimerState(TimerState.IDLE));
                            break;
                    }
                });
    }

    private void setTiming(int duration) {
        if (duration != 0 && currentTimerState != TimerState.TIMING) {
            refreshTimerState(TimerState.TIMING);
        }
        mCurrentDuration = duration;
        String hour = String.valueOf(duration / 3600);
        if (hour.length() == 1) {
            hour = "0" + hour;
        }
        String minute = String.valueOf((duration % 3600) / 60);
        if (minute.length() == 1) {
            minute = "0" + minute;
        }
        String second = String.valueOf(duration % 60);
        if (second.length() == 1) {
            second = "0" + second;
        }
        String timing = hour + ":" + minute + ":" + second;
        mTimingTextView.setText(timing);
    }

    private void autoStart(Intent intent) {
        if (intent == null) intent = getIntent();
        int voiceSetTime = intent.getIntExtra("time", -1);// 语音输入时间
        int voiceSetMode = intent.getIntExtra("mode", -1);// 语音输入计时类型
        if (voiceSetTime == -1 && voiceSetMode == -1) return;
        if (voiceSetMode != -1) {
            if (mSelectCookTab != null) {
                mSelectCookTab.setSelected(false);
            }
            switch (voiceSetMode) {
                case 1:
                    // 15 min
                    mHourWheelView.setSelection(0);
                    mMinuteWheelView.setSelection(15);
                    voiceSetTime = 15;
                    mSelectCookTab = rl_cook_tab1;
                    break;
                case 2:
                    // 30 min
                    mHourWheelView.setSelection(0);
                    mMinuteWheelView.setSelection(30);
                    voiceSetTime = 30;
                    mSelectCookTab = rl_cook_tab2;
                    break;
                case 3:
                    // 90 min
                    mHourWheelView.setSelection(1);
                    mMinuteWheelView.setSelection(30);
                    voiceSetTime = 90;
                    mSelectCookTab = rl_cook_tab3;
                    break;
                case 4:
                    // 45 min
                    mHourWheelView.setSelection(0);
                    mMinuteWheelView.setSelection(45);
                    voiceSetTime = 45;
                    mSelectCookTab = rl_cook_tab4;
                    break;
                case 5:
                    // 20 min
                    mHourWheelView.setSelection(0);
                    mMinuteWheelView.setSelection(20);
                    voiceSetTime = 20;
                    mSelectCookTab = rl_cook_tab5;
                    break;
                case 6:
                    // 20 min
                    mHourWheelView.setSelection(0);
                    mMinuteWheelView.setSelection(20);
                    voiceSetTime = 20;
                    mSelectCookTab = rl_cook_tab6;
                    break;
                default:
                    break;
            }
            if (mSelectCookTab != null) {
                mSelectCookTab.setSelected(true);
            }
        }
        int duration = voiceSetTime * 60;
        setTiming(duration);
        RxBus.getInstance().post(BusEvent.MSG_TIMER_START, duration - 1);
        refreshTimerState(TimerState.TIMING);
    }

    @OnClick({R.id.rl_cook_tab1, R.id.rl_cook_tab2, R.id.rl_cook_tab3, R.id.rl_cook_tab4, R.id.rl_cook_tab5, R.id.rl_cook_tab6})
    public void onCookTabClick(View view) {
        if (view == mSelectCookTab) return;
        if (mSelectCookTab != null) mSelectCookTab.setSelected(false);
        mSelectCookTab = view;
        view.setSelected(true);
        switch (view.getId()) {
            case R.id.rl_cook_tab1: {
                // 15min
                mHourWheelView.setSelection(0);
                mMinuteWheelView.setSelection(15);
            }
            break;
            case R.id.rl_cook_tab2: {
                // 30min
                mHourWheelView.setSelection(0);
                mMinuteWheelView.setSelection(30);
            }
            break;
            case R.id.rl_cook_tab3: {
                // 90min
                mHourWheelView.setSelection(1);
                mMinuteWheelView.setSelection(30);
            }
            break;
            case R.id.rl_cook_tab4: {
                // 45min
                mHourWheelView.setSelection(0);
                mMinuteWheelView.setSelection(45);
            }
            break;
            case R.id.rl_cook_tab5:
            case R.id.rl_cook_tab6: {
                // 20min
                mHourWheelView.setSelection(0);
                mMinuteWheelView.setSelection(20);
            }
            break;
        }
    }

    @OnClick(R.id.ll_select_music)
    public void onSelectMusicClick() { // 音乐选择
        if (mMusicDialog != null) mMusicDialog.show();
    }

    @Override
    public void onMusicClick(int position) {
        RxBus.getInstance().post(BusEvent.MSG_TIMER_PLAY_MUSIC, position);
    }

    @Override
    public void onConfirmClick(int position) {
        String str = getResources().getString(R.string.timer_bell) + (position + 1);
        mSelectMusicTextView.setText(str);
    }

    @OnClick(R.id.btn_left)
    public void onLeftButtonClick() {
        // 结束计时
        if (currentTimerState == TimerState.IDLE) {
            ToastUtil.showDefinedCenter(FridgeApplication.getContext(), getResources().getString(R.string.home_cancel_tip));
            return;
        }
        RxBus.getInstance().post(BusEvent.MSG_TIMER_STOP);
        refreshTimerState(TimerState.IDLE);
    }

    @OnClick(R.id.btn_right)
    public void onRightButtonClick() {
        switch (currentTimerState) {
            case IDLE: {
                // 开始计时
                int duration = (mSelectHour * 60 + mSelectMinute) * 60;
                if (duration <= 0) {
                    ToastUtil.showDefinedCenter(FridgeApplication.getContext(), getResources().getString(R.string.timer_please_set_time));
                    return;
                }
                if (mSelectMusicTextView.getText().toString().equals(getResources().getString(R.string.timer_select_timer_music))) {
                    ToastUtil.showDefinedCenter(FridgeApplication.getContext(), getResources().getString(R.string.timer_stop_music));
                    return;
                }
                setTiming(duration);
                RxBus.getInstance().post(BusEvent.MSG_TIMER_START, duration - 1);
                refreshTimerState(TimerState.TIMING);
            }
            break;
            case PAUSE: {
                // 继续计时
                RxBus.getInstance().post(BusEvent.MSG_TIMER_START, mCurrentDuration - 1);
                refreshTimerState(TimerState.TIMING);
            }
            break;
            case TIMING: {
                // 暂停计时
                RxBus.getInstance().post(BusEvent.MSG_TIMER_STOP);
                refreshTimerState(TimerState.PAUSE);
            }
            break;
        }
    }

    private void refreshTimerState(TimerState timerState) {
        if (currentTimerState == timerState) return;
        currentTimerState = timerState;
        switch (timerState) {
            case IDLE: {
                mRightButton.setBackground(getResources().getDrawable(R.drawable.btn_bg_light_green));
                mRightButton.setText(getResources().getString(R.string.timer_start_count));
                layoutCookTab.setVisibility(View.VISIBLE);
                mTimingTextView.setVisibility(View.GONE);
                setTiming(0);
            }
            break;
            case PAUSE: {
                mRightButton.setBackground(getResources().getDrawable(R.drawable.btn_bg_light_green));
                mRightButton.setText(getResources().getString(R.string.timer_continue));
                layoutCookTab.setVisibility(View.INVISIBLE);
                mTimingTextView.setVisibility(View.VISIBLE);
            }
            break;
            case TIMING: {
                mRightButton.setBackground(getResources().getDrawable(R.drawable.btn_bg_light_red));
                mRightButton.setText(getResources().getString(R.string.timer_pause));
                layoutCookTab.setVisibility(View.INVISIBLE);
                mTimingTextView.setVisibility(View.VISIBLE);
            }
            break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
        if (mMusicDialog != null && mMusicDialog.isShowing()) {
            mMusicDialog.cancel();
            mMusicDialog = null;
        }
    }
}
