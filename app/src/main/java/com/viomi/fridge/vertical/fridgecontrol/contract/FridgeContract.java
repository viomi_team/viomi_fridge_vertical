package com.viomi.fridge.vertical.fridgecontrol.contract;

import com.viomi.fridge.vertical.common.callback.BasePresenter;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.entity.ChangeableScene;
import com.viomi.fridge.vertical.fridgecontrol.model.serial.entity.DeviceParams;

import java.util.List;

/**
 * 冰箱控制 Contract
 * Created by William on 2018/2/2.
 */
public interface FridgeContract {
    interface View {
        void refreshTip(String tip);// 更新提示

        void refreshUi(DeviceParams params);// 刷新 Ui

        void refreshScene();// 刷新场景

        void setIsSetting(boolean enable);
    }

    interface Presenter extends BasePresenter<View> {
        void loadTips();// 随机生成提示文字

        void loadFridgeData();// 读取冰箱数据

        void loadDisplayScene(List<ChangeableScene> allList, List<ChangeableScene> displayList, List<ChangeableScene> backupList);// 读取展示场景

        void setMode(int mode, boolean enable);// 工作模式设置

        void setOneKeyClean(boolean enable);// 一键净化设置

        void setColdSwitch(boolean enable);// 冷藏室开关

        void setChangeableSwitch(boolean enable);// 变温室开关

        void setColdTemp(int temp);// 设置冷藏室温度

        void setChangeableTemp(int temp);// 设置变温室温度

        void setFreezingTemp(int temp);// 设置冷冻室温度

        void saveDisplayScenes(List<ChangeableScene> list);// 保存场景显示区

        void setScene(List<ChangeableScene> list, ChangeableScene scene);// 场景设置

        void setFreshFruit(boolean enable);// 鲜果

        void setRetainFresh(boolean enable);// 0 ℃保鲜

        void setIced(boolean enable);// 冰镇

        boolean isInspectionRunning();// 是否正在商检

        boolean isCommunicationError();// 是否通讯故障

        boolean isExistError();// 是否存在故障
    }
}