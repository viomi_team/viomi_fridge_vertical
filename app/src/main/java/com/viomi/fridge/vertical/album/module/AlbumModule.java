package com.viomi.fridge.vertical.album.module;

import com.viomi.fridge.vertical.album.contract.AlbumContract;
import com.viomi.fridge.vertical.album.presenter.AlbumPresenter;
import com.viomi.fridge.vertical.common.scope.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * 电子相册 Module
 * Created by William on 2018/1/20.
 */
@Module
public abstract class AlbumModule {

    @ActivityScoped
    @Binds
    abstract AlbumContract.Presenter albumPresenter(AlbumPresenter presenter);
}
