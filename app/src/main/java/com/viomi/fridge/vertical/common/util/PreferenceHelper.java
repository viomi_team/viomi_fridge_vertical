package com.viomi.fridge.vertical.common.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Base64;

import com.viomi.fridge.vertical.message.entity.PushSetting;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * SharedPreference 帮助类
 * Created by nanquan on 2018/1/25.
 */
public class PreferenceHelper {
    private static final String TAG = "PreferenceHelper";
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;
    private static Map<String, SharedPreferences> mPreferenceMap;

    private static String PREFERENCE_NAME = "viomi.preference"; // 默认文件名

    /**
     * 使用默认文件访问
     *
     * @param context the context
     */
    public PreferenceHelper(Context context) {
        this(context, PREFERENCE_NAME);
    }

    /**
     * 指定文件访问
     *
     * @param context        the context
     * @param preferenceName 文件名称
     */
    public PreferenceHelper(Context context, String preferenceName) {
        if (mSharedPreferences == null) {
            mPreferenceMap = new HashMap<>();
        }
        if (preferenceName != null && !preferenceName.isEmpty()) {
            PREFERENCE_NAME = preferenceName;
        }
        if (!mPreferenceMap.containsKey(preferenceName)) {
            SharedPreferences preferences = context.getSharedPreferences(
                    PREFERENCE_NAME, Context.MODE_PRIVATE);
            mPreferenceMap.put(preferenceName, preferences);
            mSharedPreferences = preferences;
        } else {
            mSharedPreferences = mPreferenceMap.get(preferenceName);
        }
        mEditor = mSharedPreferences.edit();
    }

    public String getString(String key) {
        return getString(key, null);
    }

    public String getString(String key, String defValue) {
        return mSharedPreferences.getString(key, defValue);
    }

    public boolean getBoolean(String key) {
        return getBoolean(key, false);
    }

    public boolean getBoolean(String key, boolean defValue) {
        return mSharedPreferences.getBoolean(key, defValue);
    }

    public Integer getInt(String key) {
        return getInt(key, 0);
    }

    public Integer getInt(String key, int defValue) {
        return mSharedPreferences.getInt(key, defValue);
    }

    protected Object getObject(String key) {
        return getObject(key, null);
    }

    protected Object getObject(String key, Object defObject) {
        if (mSharedPreferences.contains(key)) {
            String objectString = mSharedPreferences.getString(key, "");
            if (TextUtils.isEmpty(objectString)) {
                return null;
            }
            byte[] objBytes = Base64.decode(objectString.getBytes(), Base64.DEFAULT);
            ByteArrayInputStream bais = new ByteArrayInputStream(objBytes);
            ObjectInputStream ois = null;
            try {
                ois = new ObjectInputStream(bais);
                return ois.readObject();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    bais.close();
                    if (ois != null) {
                        ois.close();
                    }
                } catch (IOException e) {
                    logUtil.w(TAG, e.toString());
                }
            }
        }
        return defObject;
    }

    public void putString(String key, String value) {
        mEditor.putString(key, value);
        mEditor.commit();
    }

    public void putBoolean(String key, boolean value) {
        mEditor.putBoolean(key, value);
        mEditor.commit();
    }

    public void putInt(String key, int value) {
        mEditor.putInt(key, value);
        mEditor.commit();
    }

    public void putObject(String key, Object object) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
            String objectString = new String(Base64.encode(baos.toByteArray(), Base64.DEFAULT));
            mEditor.putString(key, objectString);
            mEditor.apply();
        } catch (IOException e) {
            logUtil.w(TAG, e.toString());
        } finally {
            try {
                baos.close();
                if (oos != null) {
                    oos.close();
                }
            } catch (IOException e) {
                logUtil.w(TAG, e.toString());
            }
        }
    }

    public void remove(String key) {
        mEditor.remove(key);
        mEditor.commit();
    }

    // 计时结束铃声
    private static final String PREFERENCE_SELECTED_TIMEOUT_RING = "preference_selected_timeout_ring";

    public void putSelectedTimeoutTing(int position) {
        putInt(PREFERENCE_SELECTED_TIMEOUT_RING, position);
    }

    public int getSelectedTimeoutRing() {
        return getInt(PREFERENCE_SELECTED_TIMEOUT_RING);
    }

    // 最后一条活动消息id
    private static final String PREFERENCE_LAST_ACTIVITY_MESSAGE_ID = "preference_last_activity_message_id";

    public void putLastActivityMessageId(String id) {
        putString(PREFERENCE_LAST_ACTIVITY_MESSAGE_ID, id);
    }

    public String getLastActivityMessageId() {
        return getString(PREFERENCE_LAST_ACTIVITY_MESSAGE_ID, "0");
    }

    // 最后一条商城消息id
    private static final String PREFERENCE_LAST_MALL_MESSAGE_ID = "preference_last_mall_message_id";

    public void putLastMallMessageId(String id) {
        putString(PREFERENCE_LAST_MALL_MESSAGE_ID, id);
    }

    public String getLastMallMessageId() {
        return getString(PREFERENCE_LAST_MALL_MESSAGE_ID, "0");
    }

    // 最后一条推送消息id
    private static final String PREFERENCE_PUSH_SETTING = "preference_push_setting";

    public void putPushSetting(PushSetting pushSetting) {
        putObject(PREFERENCE_PUSH_SETTING, pushSetting);
    }

    public PushSetting getPushSetting() {
        return (PushSetting) getObject(PREFERENCE_PUSH_SETTING, new PushSetting());
    }

    // 未读消息通知数量
    private static final String PREFERENCE_UNREAD_MESSAGE_NUM = "preference_unread_message_num";

    public void putUnreadMessageNum(int num) {
        putInt(PREFERENCE_UNREAD_MESSAGE_NUM, num);
    }

    public int getUnreadMessageNum() {
        return getInt(PREFERENCE_UNREAD_MESSAGE_NUM);
    }
}