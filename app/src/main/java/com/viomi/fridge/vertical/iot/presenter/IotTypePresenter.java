package com.viomi.fridge.vertical.iot.presenter;

import android.content.Context;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.model.preference.ManagePreference;
import com.viomi.fridge.vertical.administration.model.repository.ManageRepository;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.iot.contract.IotTypeContract;

import javax.annotation.Nullable;
import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * 互联网家设备分类 Presenter
 * Created by William on 2018/3/3.
 */
public class IotTypePresenter implements IotTypeContract.Presenter {
    private static final String TAG = IotTypePresenter.class.getSimpleName();
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;

    @Nullable
    private IotTypeContract.View mView;

    @Inject
    IotTypePresenter(Context context) {
        mContext = context;
    }

    @Override
    public void subscribe(IotTypeContract.View view) {
        this.mView = view;
        mCompositeSubscription = new CompositeSubscription();
        loadUserInfo();
    }

    @Override
    public void unSubscribe() {
        this.mView = null;
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
    }

    @Override
    public void loadUserInfo() {
        Subscription subscription = ManageRepository.getInstance().getUser(mContext)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(qrCodeBase -> {
                    if (mView != null) mView.showUserInfo(qrCodeBase);
                }, throwable -> {
                    logUtil.e(TAG, throwable.getMessage());
                    if (mView != null) mView.showUserInfo(null);
                });
        mCompositeSubscription.add(subscription);
    }

    @Override
    public String switchBuyUrl(String type) {
        String url;
        if (ManagePreference.getInstance().getDebug()) url = AppConstants.URL_VMALL_DEBUG;
        else url = AppConstants.URL_VMALL_RELEASE;
        if (type.equals(mContext.getResources().getString(R.string.iot_device_range_hood))) {
            url = url + "/index.html#/more/1/307/-1";
        } else if (type.equals(mContext.getResources().getString(R.string.iot_device_water_purifier))) {
            url = url + "/index.html#/more/1/308/-1";
        } else if (type.equals(mContext.getResources().getString(R.string.iot_device_washing_machine)) || type.equals(mContext.getResources().getString(R.string.iot_device_dish_washing)) || type.equals(mContext.getResources().getString(R.string.iot_device_heat_kettle)) ||
                type.equals(mContext.getResources().getString(R.string.iot_device_fan)) || type.equals(mContext.getResources().getString(R.string.iot_device_sweep_robot))) {
            url = url + "/index.html#/more/1/309/421";
        }
        return url;
    }
}
