package com.viomi.fridge.vertical.fridgecontrol.model.serial.entity;

import com.viomi.fridge.vertical.AppConstants;

import java.io.Serializable;

/**
 * 串口接收和发送串口数据
 * Created by William on 2018/1/5.
 */
public class DeviceParams implements Serializable {
    private int mode = AppConstants.MODE_SMART;// 工作模式
    private int cold_temp_set = AppConstants.COLD_TEMP_DEFAULT;// 冷藏室设置温度
    private int changeable_temp_set = AppConstants.CHANGEABLE_TEMP_DEFAULT;// 变温室设置温度
    private int freezing_temp_set = AppConstants.FREEZING_TEMP_DEFAULT;// 冷冻室设置温度
    private boolean cold_switch = true;// 冷藏室开关
    private boolean changeable_switch = true;// 变温室开关
    private int cold_temp_real = AppConstants.COLD_TEMP_DEFAULT;// 冷藏室实际温度
    private int changeable_temp_real = AppConstants.CHANGEABLE_TEMP_DEFAULT;// 变温室实际温度
    private int freezing_temp_real = AppConstants.FREEZING_TEMP_DEFAULT;// 冷冻室实际温度
    private int rc_evaporator_temp;// 蒸发器温度，冷藏化霜温度
    private int cc_evaporator_temp;// 蒸发器温度，变温化霜温度
    private int fc_evaporator_temp;// 蒸发器温度，冷冻化霜温度
    private boolean one_key_clean = false;// 一键净化
    private boolean rcf_forced = false;// 不强制启动
    private boolean rc_forced_frost = false;// 冷藏化霜测试
    private boolean fc_forced_frost = false;// 冷冻化霜测试
    private boolean commodity_inspection;// 商检状态
    private boolean time_cut;// 缩时
    private int indoor_temp;// 室内温度（环境温度）
    private int error;// 设备异常，一个位一个异常
    private DeviceError mDeviceError = new DeviceError();// 设备相关异常信息
    private int crc;// CRC 校验
    private boolean quick_cold;// 速冷
    private boolean quick_freeze;// 速冻
    private boolean iced_drink;// 冰饮
    private boolean fresh_fruit;// 鲜果
    private boolean retain_fresh;// 0 度保鲜
    private boolean iced;// 冰镇
    private boolean flipping_beam_status;// 翻转梁加热状态
    private boolean flipping_beam_reset_mode;// 翻转梁加热复位模式
    private boolean fc_evaporator_heat;// 冷冻化霜加热器状态
    private int compressor_run_time;// 压缩机累计运行时间
    private int data40;// 462，521 型号判断
    private int data41;// 462，521 型号判断

    public String toString(String model) {
        switch (model) {
            case AppConstants.MODEL_X2:  // 双鹿 446
                return "Mode = " + mode + ",RCSetTemp = " + cold_temp_set + ",FCSetTemp = " + freezing_temp_set
                        + ",RCSet = " + cold_switch + ",OneKeyClean = " + one_key_clean;
            case AppConstants.MODEL_X3:  // 美菱 462
                return "Mode = " + mode + ",RCSetTemp = " + cold_temp_set + ",FCSetTemp = " + freezing_temp_set +
                        ",RCSet = " + cold_switch + ",QuickCold = " + quick_cold + ",QuickFreeze = " + quick_freeze;
            case AppConstants.MODEL_X5:  // 美菱 521
                return "Mode = " + mode + ",RCSetTemp = " + cold_temp_set + ",CCSetTemp = " + changeable_temp_set +
                        ",FCSetTemp = " + freezing_temp_set + ",RCSet = " + cold_switch + ",CCSet = " + changeable_switch +
                        ",QuickFreeze = " + quick_freeze + ",OneKeyClean = " + one_key_clean + ",FreshFruit = " + fresh_fruit +
                        ",RetainFresh = " + retain_fresh + ",Iced = " + iced;
            default:
                return "Mode = " + mode + ",RCSetTemp = " + cold_temp_set + ",CCSetTemp = " + changeable_temp_set +
                        ",FCSetTemp = " + freezing_temp_set + ",RCSet = " + cold_switch + ",CCSet = " + changeable_switch +
                        ",OneKeyClean = " + one_key_clean;
        }
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public int getFreezing_temp_set() {
        return freezing_temp_set;
    }

    public void setFreezing_temp_set(int freezing_temp_set) {
        this.freezing_temp_set = freezing_temp_set;
    }

    public int getChangeable_temp_set() {
        return changeable_temp_set;
    }

    public void setChangeable_temp_set(int changeable_temp_set) {
        this.changeable_temp_set = changeable_temp_set;
    }

    public boolean isChangeable_switch() {
        return changeable_switch;
    }

    public void setChangeable_switch(boolean changeable_switch) {
        this.changeable_switch = changeable_switch;
    }

    public int getCold_temp_set() {
        return cold_temp_set;
    }

    public void setCold_temp_set(int cold_temp_set) {
        this.cold_temp_set = cold_temp_set;
    }

    public boolean isCold_switch() {
        return cold_switch;
    }

    public void setCold_switch(boolean cold_switch) {
        this.cold_switch = cold_switch;
    }

    public boolean isQuick_cold() {
        return quick_cold;
    }

    public void setQuick_cold(boolean quick_cold) {
        this.quick_cold = quick_cold;
    }

    public boolean isQuick_freeze() {
        return quick_freeze;
    }

    public void setQuick_freeze(boolean quick_freeze) {
        this.quick_freeze = quick_freeze;
    }

    public boolean isRcf_forced() {
        return rcf_forced;
    }

    public void setRcf_forced(boolean rcf_forced) {
        this.rcf_forced = rcf_forced;
    }

    public boolean isRc_forced_frost() {
        return rc_forced_frost;
    }

    public void setRc_forced_frost(boolean rc_forced_frost) {
        this.rc_forced_frost = rc_forced_frost;
    }

    public boolean isFc_forced_frost() {
        return fc_forced_frost;
    }

    public void setFc_forced_frost(boolean fc_forced_frost) {
        this.fc_forced_frost = fc_forced_frost;
    }

    public int getCold_temp_real() {
        return cold_temp_real;
    }

    public void setCold_temp_real(int cold_temp_real) {
        this.cold_temp_real = cold_temp_real;
    }

    public int getChangeable_temp_real() {
        return changeable_temp_real;
    }

    public void setChangeable_temp_real(int changeable_temp_real) {
        this.changeable_temp_real = changeable_temp_real;
    }

    public int getFreezing_temp_real() {
        return freezing_temp_real;
    }

    public void setFreezing_temp_real(int freezing_temp_real) {
        this.freezing_temp_real = freezing_temp_real;
    }

    public int getRc_evaporator_temp() {
        return rc_evaporator_temp;
    }

    public void setRc_evaporator_temp(int rc_evaporator_temp) {
        this.rc_evaporator_temp = rc_evaporator_temp;
    }

    public int getFc_evaporator_temp() {
        return fc_evaporator_temp;
    }

    public void setFc_evaporator_temp(int fc_evaporator_temp) {
        this.fc_evaporator_temp = fc_evaporator_temp;
    }

    public boolean isOne_key_clean() {
        return one_key_clean;
    }

    public void setOne_key_clean(boolean one_key_clean) {
        this.one_key_clean = one_key_clean;
    }

    public boolean isCommodity_inspection() {
        return commodity_inspection;
    }

    public void setCommodity_inspection(boolean commodity_inspection) {
        this.commodity_inspection = commodity_inspection;
    }

    public boolean isTime_cut() {
        return time_cut;
    }

    public void setTime_cut(boolean time_cut) {
        this.time_cut = time_cut;
    }

    public int getIndoor_temp() {
        return indoor_temp;
    }

    public void setIndoor_temp(int indoor_temp) {
        this.indoor_temp = indoor_temp;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public int getCrc() {
        return crc;
    }

    public void setCrc(int crc) {
        this.crc = crc;
    }

    public boolean isIced_drink() {
        return iced_drink;
    }

    public void setIced_drink(boolean iced_drink) {
        this.iced_drink = iced_drink;
    }

    public boolean isFlipping_beam_status() {
        return flipping_beam_status;
    }

    public void setFlipping_beam_status(boolean flipping_beam_status) {
        this.flipping_beam_status = flipping_beam_status;
    }

    public boolean isFlipping_beam_reset_mode() {
        return flipping_beam_reset_mode;
    }

    public void setFlipping_beam_reset_mode(boolean flipping_beam_reset_mode) {
        this.flipping_beam_reset_mode = flipping_beam_reset_mode;
    }

    public boolean isFc_evaporator_heat() {
        return fc_evaporator_heat;
    }

    public void setFc_evaporator_heat(boolean fc_evaporator_heat) {
        this.fc_evaporator_heat = fc_evaporator_heat;
    }

    public DeviceError getDeviceError() {
        return mDeviceError;
    }

    public void setErrorCode() {
        error = 0;
        if (mDeviceError.isError_communication()) error = error | 0x01;// 通信故障
        if (mDeviceError.isError_rc_sensor()) error = error | 0x02;// 冷藏室传感器故障
        if (mDeviceError.isError_cc_sensor()) error = error | 0x04;// 变温室传感器故障
        if (mDeviceError.isError_fc_sensor()) error = error | 0x08;// 冷冻室传感器故障
        if (mDeviceError.isError_rc_defrost_sensor()) error = error | 0x10;// 冷藏化霜传感器故障
        if (mDeviceError.isError_fc_defrost_sensor()) error = error | 0x20;// 冷冻化霜传感器故障
        if (mDeviceError.isError_indoor_sensor()) error = error | 0x40;// 环境温度传感器故障
        if (mDeviceError.isError_fan_door()) error = error | 0x80;// 风门故障
        if (mDeviceError.isError_rc_fan()) error = error | 0x0100;// 冷藏风扇故障
        if (mDeviceError.isError_cc_fan()) error = error | 0x0200;// 冷凝风扇故障
        if (mDeviceError.isError_fc_fan()) error = error | 0x0400;// 冷冻风扇故障
        if (mDeviceError.isError_defrost()) error = error | 0x4000;// 化霜不良报警
        if (mDeviceError.isError_cc_defrost_sensor()) error = error | 0x200000;// 变温化霜传感器故障
        if (mDeviceError.isError_humidity_sensor()) error = error | 0x400000;// 湿度传感器湿度故障
        if (mDeviceError.isError_humidity_temp_sensor()) error = error | 0x800000;// 湿度传感器温度故障
        if (mDeviceError.isError_rc_cc_sensor()) error = error | 0x1000000;// 冷藏变温传感器故障
    }

    public int getCompressor_run_time() {
        return compressor_run_time;
    }

    public void setCompressor_run_time(int compressor_run_time) {
        this.compressor_run_time = compressor_run_time;
    }

    public boolean isFresh_fruit() {
        return fresh_fruit;
    }

    public void setFresh_fruit(boolean fresh_fruit) {
        this.fresh_fruit = fresh_fruit;
    }

    public boolean isRetain_fresh() {
        return retain_fresh;
    }

    public void setRetain_fresh(boolean retain_fresh) {
        this.retain_fresh = retain_fresh;
    }

    public boolean isIced() {
        return iced;
    }

    public void setIced(boolean iced) {
        this.iced = iced;
    }

    public int getCc_evaporator_temp() {
        return cc_evaporator_temp;
    }

    public void setCc_evaporator_temp(int cc_evaporator_temp) {
        this.cc_evaporator_temp = cc_evaporator_temp;
    }

    public int getData40() {
        return data40;
    }

    public void setData40(int data40) {
        this.data40 = data40;
    }

    public int getData41() {
        return data41;
    }

    public void setData41(int data41) {
        this.data41 = data41;
    }
}