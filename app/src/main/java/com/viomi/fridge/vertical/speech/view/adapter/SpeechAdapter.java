package com.viomi.fridge.vertical.speech.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.speech.model.entity.TalkInfoEntity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 语音对话框适配器
 * Created by William on 2018/3/7.
 */
public class SpeechAdapter extends RecyclerView.Adapter<SpeechAdapter.SpeechHolder> {
    private List<TalkInfoEntity> mList;

    public SpeechAdapter(List<TalkInfoEntity> list) {
        mList = list;
        if (mList == null) mList = new ArrayList<>();
    }

    @Override
    public SpeechHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_holder_speech_item, parent, false);
        return new SpeechHolder(view);
    }

    @Override
    public void onBindViewHolder(SpeechHolder holder, int position) {
        TalkInfoEntity info = mList.get(position);
        if (info.talkType == TalkInfoEntity.TYPE_TALK) {
            holder.mFridgeRelativeLayout.setVisibility(View.VISIBLE);
            holder.mPeopleRelativeLayout.setVisibility(View.INVISIBLE);
            holder.mFridgeTextView.setText(info.msg);
        } else {
            holder.mFridgeRelativeLayout.setVisibility(View.INVISIBLE);
            holder.mPeopleRelativeLayout.setVisibility(View.VISIBLE);
            holder.mPeopleTextView.setText(info.msg);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class SpeechHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.holder_speech_fridge_layout)
        RelativeLayout mFridgeRelativeLayout;
        @BindView(R.id.holder_speech_people_layout)
        RelativeLayout mPeopleRelativeLayout;
        @BindView(R.id.holder_speech_fridge_text)
        TextView mFridgeTextView;
        @BindView(R.id.holder_speech_people_text)
        TextView mPeopleTextView;

        SpeechHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
