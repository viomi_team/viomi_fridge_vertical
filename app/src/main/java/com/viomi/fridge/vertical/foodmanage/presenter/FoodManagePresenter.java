package com.viomi.fridge.vertical.foodmanage.presenter;

import android.content.Context;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.ToastUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.foodmanage.contract.FoodManageContract;
import com.viomi.fridge.vertical.foodmanage.model.FoodManageRepository;
import com.viomi.fridge.vertical.foodmanage.model.entity.FoodDetail;

import javax.annotation.Nullable;
import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * 食材管理主页 Presenter
 * Created by William on 2018/2/27.
 */
public class FoodManagePresenter implements FoodManageContract.Presenter {
    private static final String TAG = FoodManagePresenter.class.getSimpleName();
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;

    @Nullable
    private FoodManageContract.View mView;

    @Inject
    FoodManagePresenter(Context context) {
        mContext = context;
    }

    @Override
    public void subscribe(FoodManageContract.View view) {
        mView = view;
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public void unSubscribe() {
        mView = null;
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
    }

    @Override
    public void saveFood(FoodDetail foodDetail) {
        Subscription subscription = FoodManageRepository.getInstance().saveFood(foodDetail)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(aBoolean -> {
                    if (aBoolean && mView != null) {
                        mView.dismissFoodDialog();
                        RxBus.getInstance().post(BusEvent.MSG_START_FOOD_UPDATE);
                    } else {
                        ToastUtil.showCenter(mContext, mContext.getResources().getString(R.string.food_management_save_food_fail));
                    }
                }, throwable -> {
                    ToastUtil.showCenter(mContext, mContext.getResources().getString(R.string.food_management_save_food_fail));
                    logUtil.e(TAG, throwable.getMessage());
                });
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void deleteImage(String filePath) {
        Subscription subscription = FoodManageRepository.getInstance().deleteImage(filePath)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(aBoolean -> {
                    if (aBoolean && mView != null) mView.dismissFoodDialog();
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void editFood(FoodDetail foodDetail, String id) {
        Subscription subscription = FoodManageRepository.getInstance().updateFood(foodDetail, id)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(aBoolean -> {
                    if (aBoolean && mView != null) {
                        mView.dismissFoodDialog();
                        RxBus.getInstance().post(BusEvent.MSG_START_FOOD_UPDATE);
                    } else {
                        ToastUtil.showCenter(mContext, mContext.getResources().getString(R.string.food_management_save_food_fail));
                    }
                }, throwable -> {
                    logUtil.e(TAG, throwable.getMessage());
                    ToastUtil.showCenter(mContext, mContext.getResources().getString(R.string.food_management_save_food_fail));
                });
        mCompositeSubscription.add(subscription);
    }
}