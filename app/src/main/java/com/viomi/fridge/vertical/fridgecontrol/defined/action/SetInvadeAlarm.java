package com.viomi.fridge.vertical.fridgecontrol.defined.action;

import com.viomi.fridge.vertical.fridgecontrol.defined.ViomiDefined;
import com.viomi.fridge.vertical.fridgecontrol.defined.property.InvadeAlarm;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetInvadeAlarm extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setInvadeAlarm.toActionType();

    public SetInvadeAlarm() {
        super(TYPE);

        super.addArgument(InvadeAlarm.TYPE.toString());
    }
}