package com.viomi.fridge.vertical.common.util;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.model.preference.ManagePreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.RandomAccessFile;

/**
 * 埋点统计类
 */

public class StatisticsUtil {
    private static final String TAG = "StatisticsUtil";
    //
    public static final String USER_ID_VIOMI = "viomi";
    public static final String USER_ID_XIAOMI = "xiaomi";
    //统计文件
    private static String filePath = "/sdcard/device/";//文件夹路径
    private static final String fileName = "temp.txt";
    public static final String LOG_FILE_PATH = "/sdcard/device/temp.txt";//文件夹路径
    //
    public static final String UPLOAD_EVENT_FILE = "https://ms.viomi.com.cn/acquisition/fridge/up-file";
    //食材管理
    public static final String EVENT_ENTER_FOODMANAGER = "enter_foodmanage";//进入食材
    public static final String EVENT_ADD_FOOD = "add_ingredients";//添加食材
    public static final String EVENT_DELETE_FOODMANAGER = "delete_ingredients";//删除食材
    public static final String EVENT_EXIT_FOODMANAGER = "exit_foodmanage";//退出食材
    //影音娱乐
    public static final String EVENT_ENTER_VIDEO = "enter_video";//进入视频
    public static final String EVENT_ENTER_MUSIC = "enter_music";//进入音乐
    //菜谱
    public static final String EVENT_ENTER_RECEIPE = "enter_recipe";//进入菜谱
    public static final String EVENT_ENTER_VOICE_RECEIPE = "enter_voice_recipe";
    public static final String EVENT_ENTER_RECEIPE_CATE = "enter_recipe_category";//进入菜系
    public static final String EVENT_SEARCH_RECEIPE = "search_recipe";//搜索的菜谱
    public static final String EVENT_WATCH_RECEIPE = "search_watch_recipe";//查看的菜谱
    public static final String EVENT_EXIT_RECEIPE = "exit_recipe";//退出菜谱
    //语音助手
    public static final String EVENT_ENTER_VOICE = "enter_voice_assistant";//打开语音助手
    public static final String EVENT_STATUS_VOICE = "voice_assistant_status";//语音功能是否开启
    public static final String EVENT_FIND_VOICE_CMD = "find_more_voice_cmd";//查看更多语音命令
    public static final String EVENT_WAKE_UP_VOICE = "wake_up_voice";//语音唤醒
    public static final String EVENT_EXIT_VOICE = "exit_voice_assistant";//关闭语音助手
    //商城
    public static final String EVENT_ENTER_MALL = "enter_mall";//进入商城
    public static final String EVENT_BROWSE_GOOD = "browse_good";//查看商品
    public static final String EVENT_ADD_SHOPPINGCAR = "add_shoppingcar";//加入购物车
    public static final String EVENT_PAY = "pay";//购买
    public static final String EVENT_PAY_STATUS = "pay_status";//支付状态
    public static final String EVENT_EXIT_MALL = "exit_mall";;//退出商城
    //互联网家
    public static final String EVENT_ENTER_UNION = "enter_connect_union";//进入智能互联
    public static final String EVENT_FIND_UNION = "find_device_info";//查看的设备
    public static final String EVENT_EXIT_UNION = "exit_connect_union";// 退出智能互联
    //冰箱
    public static final String EVENT_ENTER_FRIDGE = "enter_fridge";//进入冰箱
    public static final String EVENT_EXIT_FRIDGE = "exit_fridge";//退出冰箱
    //定时服务
    public static final String EVENT_ENTER_TIMER = "enter_timer";//进入定时器
    public static final String EVENT_EXIT_TIMER = "exit_timer";//退出定时器
    public static final String EVENT_START_TIMER = "start_timer";//开启定时器
    public static final String EVENT_CANCEL_TIMER = "cancel_timer";//取消定时器
    public static final String EVENT_WAKE_UP_TIMER = "timer_wake_up";//定时时间提醒
    //相册
    public static final String EVENT_ENTER_ALBUM = "enter_album";
    public static final String EVENT_RECEIVER_ALBUM = "receiver_album";
    public static final String EVENT_SET_SCREEN_ALBUM = "set_screen_album";
    public static final String EVENT_DELETE_ALBUM = "delete_album";
    public static final String EVENT_EXIT_ALBUM = "exit_album";
    //水质地图
    public static final String EVENT_ENTER_WATER_MAP = "enter_water_map";
    public static final String EVENT_EXIT_WATER_MAP = "exit_water_map";

    public static String getJson(Context context, String key, String value) {
        JSONObject object = new JSONObject();
        try {
            String appVersion = ToolUtil.getVersion();
            String systemVersion = android.os.Build.VERSION.RELEASE;
            long time = System.currentTimeMillis();
            String screen = ToolUtil.getScreenWidthHeight(context);
            String ip = ToolUtil.getIpAddress();
            String mac = ToolUtil.getMac();
            String mode = android.os.Build.MODEL;
            String did = ToolUtil.getImei();
            String name = context.getString(R.string.app_name);
            String ssid = ToolUtil.getSSid();

            long requestId = ManagePreference.getInstance().getStatisticsIdState();
            requestId++;
            ManagePreference.getInstance().saveStatisticsIdState(requestId);

            object.put("requestId", requestId);
            object.put("appVersion", appVersion);
            object.put("systemVersion", systemVersion);
            object.put("time", time);
            object.put("ip", ip);
            object.put("mac", mac);
            object.put("screen", screen);
            object.put("ssid", ssid);

            JSONObject obj = new JSONObject();
            obj.put("name", name);
            obj.put("did", did);
            obj.put("model", mode);
            obj.put("key", key);
            obj.put("value", value);
            object.put("data", obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object.toString();
    }

    /**
     * 将统计数据写入到文本文件中
     */
    public static void writeStatistics(Context context, String key,
                                       String value) {
        if (TextUtils.isEmpty(key)) {
            return;
        }
        if (TextUtils.isEmpty(value)) {
            value = "null";
        }

        String strcontent = StatisticsUtil.getJson(context, key, value);
        // 生成文件夹之后，再生成文件，不然会出错
        FileUtil.makeFilePath(filePath, fileName);// 生成文件
        String strFilePath = filePath + fileName;
        // 每次写入时，都换行写
        String strContent = strcontent + "\r\n";
        try {
            File file = new File(strFilePath);
            if (!file.exists()) {
                Log.d("TestFile", "Create the file:" + strFilePath);
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            RandomAccessFile raf = new RandomAccessFile(file, "rwd");
            raf.seek(file.length());
            raf.write(strContent.getBytes());
            raf.close();
        } catch (Exception e) {
            Log.e("error:", e + "");
        }
    }
}
