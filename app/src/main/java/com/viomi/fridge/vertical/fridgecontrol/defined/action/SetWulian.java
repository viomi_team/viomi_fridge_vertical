package com.viomi.fridge.vertical.fridgecontrol.defined.action;

import com.viomi.fridge.vertical.fridgecontrol.defined.ViomiDefined;
import com.viomi.fridge.vertical.fridgecontrol.defined.property.Wulian;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetWulian extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setWulian.toActionType();

    public SetWulian() {
        super(TYPE);

        super.addArgument(Wulian.TYPE.toString());
    }
}