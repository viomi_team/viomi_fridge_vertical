package com.viomi.fridge.vertical.foodmanage.view.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseRecyclerViewAdapter;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.foodmanage.model.entity.FoodDetail;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 食材管理适配器
 * Created by William on 2018/2/27.
 */
public class FoodManageAdapter extends BaseRecyclerViewAdapter<FoodManageAdapter.FoodViewHolder> {
    private List<FoodDetail> mList;
    private Context mContext;
    private boolean mIsEditMode = false;

    public FoodManageAdapter(Context context, List<FoodDetail> list) {
        mList = list;
        mContext = context;
        if (mList == null) mList = new ArrayList<>();
    }

    @Override
    public FoodViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_holder_food_management, parent, false);
        return new FoodViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(FoodViewHolder holder, int position) {
        FoodDetail detail = mList.get(position);
        logUtil.d("FoodManageAdapter", detail.getName());
        holder.textView.setText(detail.getName());// 食材名称
        // 食材图片
        int width = (int) FridgeApplication.getContext().getResources().getDimension(R.dimen.px_x_178), height = (int) FridgeApplication.getContext().getResources().getDimension(R.dimen.px_y_178);
        Uri uri;
        logUtil.d("===", detail.getImgPath());
        if (detail.getImgPath() == null || detail.getImgPath().equals("null")) {
            uri = Uri.parse("android.resource://" + FridgeApplication.getContext().getPackageName() + "/" + R.drawable.icon_food_manage_default);
        } else {
            uri = Uri.fromFile(new File(detail.getImgPath()));
        }
        ImageRequest request = ImageRequestBuilder
                .newBuilderWithSource(uri)
                .setResizeOptions(new ResizeOptions(width, height))
                .build();
        PipelineDraweeController controller = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                .setOldController(holder.simpleDraweeView.getController())
                .setImageRequest(request)
                .build();
        holder.simpleDraweeView.setController(controller);
        // 时间进度条
        int per;
        long deadLine = Long.valueOf(detail.getDeadLine());
        long current = System.currentTimeMillis();
        if (current > deadLine) {
            per = 0;
            holder.frameLayout.setVisibility(View.VISIBLE);
        } else {
            holder.frameLayout.setVisibility(View.GONE);
            // 计算进度
            long period = deadLine - current;
            int days = (int) (period / (24 * 60 * 60 * 1000));
            if (days >= 7) per = 100;   // 保质期 7 天以上
            else per = (int) ((days / 7.0) * 100);
            if (per == 0) per = 10;
        }
        holder.progressBar.setProgress(per);
        if (per > 0 && per <= 25) {
            holder.progressBar.setProgressDrawable(ContextCompat.getDrawable(mContext, R.drawable.food_progress_red));
        } else if (per > 25 && per <= 75) {
            holder.progressBar.setProgressDrawable(ContextCompat.getDrawable(mContext, R.drawable.food_progress_orange));
        } else {
            holder.progressBar.setProgressDrawable(ContextCompat.getDrawable(mContext, R.drawable.food_progress_green));
        }
        // 编辑模式
        if (mIsEditMode) {
            holder.mImageView.setVisibility(View.VISIBLE);
            if (detail.isSelected())
                holder.mImageView.setImageResource(R.drawable.icon_message_board_chosed);
            else holder.mImageView.setImageResource(R.drawable.icon_message_board_no_chose);
        } else {
            holder.mImageView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setIsEditMode(boolean isEditMode) {
        mIsEditMode = isEditMode;
    }

    public boolean isEditMode() {
        return mIsEditMode;
    }

    class FoodViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.holder_food_manage_img)
        SimpleDraweeView simpleDraweeView;
        @BindView(R.id.holder_food_progress)
        ProgressBar progressBar;
        @BindView(R.id.holder_food_name)
        TextView textView;
        @BindView(R.id.holder_food_out_of_date)
        FrameLayout frameLayout;
        @BindView(R.id.holder_food_chose)
        ImageView mImageView;

        FoodViewHolder(View itemView, FoodManageAdapter adapter) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> adapter.onItemHolderClick(this, 200));
        }
    }
}