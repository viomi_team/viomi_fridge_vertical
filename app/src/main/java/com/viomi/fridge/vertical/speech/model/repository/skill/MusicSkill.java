package com.viomi.fridge.vertical.speech.model.repository.skill;

import android.content.Context;
import android.content.Intent;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.speech.service.MusicService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 音乐技能
 * Created by William on 2018/8/14.
 */
public class MusicSkill {
    private static final String TAG = MusicSkill.class.getSimpleName();

    public static void handle(Context context, String data) {
        try {
            JSONObject jsonObject = new JSONObject(data);
            JSONArray content = jsonObject.optJSONArray("content");
            logUtil.d(TAG, data);
            if (content == null || content.length() <= 0) return;
            Intent intent = new Intent(context, MusicService.class);
            intent.putExtra(AppConstants.SPEECH_MEDIA_DATA, data);
            intent.putExtra(AppConstants.SPEECH_MEDIA_TYPE, AppConstants.SPEECH_MEDIA_MUSIC);
            context.startService(intent);
        } catch (JSONException e) {
            logUtil.e(TAG, e.toString());
            e.printStackTrace();
        }
    }
}
