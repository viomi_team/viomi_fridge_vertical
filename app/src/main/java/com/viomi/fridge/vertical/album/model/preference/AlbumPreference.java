package com.viomi.fridge.vertical.album.model.preference;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.album.model.entity.AlbumEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * 电子相册相关信息缓存
 * Created by William on 2018/1/22.
 */
public class AlbumPreference {
    private static final String name = "viomiAlbum";
    private static AlbumPreference mInstance;
    private static final String SCREEN_SAVER_SWITCH = "screen_saver_switch";// 屏保开关
    private static final String SCREEN_SAVER_TIME = "screen_saver_time";// 屏保时间
    private static final String SCREEN_SAVER_LIST = "screen_saver_list";// 屏保展示相册集合

    public static AlbumPreference getInstance() {
        if (mInstance == null) {
            synchronized (AlbumPreference.class) {
                if (mInstance == null) {
                    mInstance = new AlbumPreference();
                }
            }
        }
        return mInstance;
    }

    private SharedPreferences getSharedPreferences() {
        return FridgeApplication.getInstance().getSharedPreferences(
                name, Context.MODE_PRIVATE);
    }

    /**
     * 缓存屏保开关
     */
    public void saveSwitch(boolean value) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(SCREEN_SAVER_SWITCH, value);
        editor.apply();
    }

    /**
     * 获取屏保开关
     */
    public boolean getSwitch() {
        return getSharedPreferences().getBoolean(SCREEN_SAVER_SWITCH, false);
    }

    /**
     * 缓存屏保时间
     */
    public void saveTime(String value) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(SCREEN_SAVER_TIME, value);
        editor.apply();
    }

    /**
     * 获取屏保时间
     */
    public String getTime() {
        return getSharedPreferences().getString(SCREEN_SAVER_TIME, FridgeApplication.getContext().getResources().getString(R.string.album_1_minute));
    }

    /**
     * 缓存屏保展示的相册目录集合
     */
    public void saveAlbumEntityList(List<AlbumEntity> list) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(SCREEN_SAVER_LIST, json);
        editor.apply();
    }

    /**
     * 获取缓存屏保展示的相册目录集合
     */
    public List<AlbumEntity> getAlbumEntityList() {
        List<AlbumEntity> list = new ArrayList<>();
        SharedPreferences sharedPreferences = getSharedPreferences();
        String json = sharedPreferences.getString(SCREEN_SAVER_LIST, null);
        if (json != null) {
            Gson gson = new Gson();
            list = gson.fromJson(json, new TypeToken<List<AlbumEntity>>() {
            }.getType());
        }
        return list;
    }
}
