package com.viomi.fridge.vertical.iot.contract;

import com.viomi.fridge.vertical.administration.model.entity.QRCodeBase;
import com.viomi.fridge.vertical.common.callback.BasePresenter;
import com.viomi.fridge.vertical.iot.model.http.entity.DeviceTypeEntity;

import java.util.List;

/**
 * 设备分类 Contract
 * Created by William on 2018/3/2.
 */
public interface IotTypeContract {
    interface View {
        void showUserInfo(QRCodeBase qrCodeBase);// 显示用户信息
    }

    interface Presenter extends BasePresenter<View> {
        void loadUserInfo();// 读取用户信息

        String switchBuyUrl(String type);// 根据设备类型跳转相应购买链接
    }
}
