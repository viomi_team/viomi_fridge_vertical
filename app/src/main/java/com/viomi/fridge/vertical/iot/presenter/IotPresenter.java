package com.viomi.fridge.vertical.iot.presenter;

import android.content.Context;

import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.iot.contract.IotContract;
import com.viomi.fridge.vertical.iot.model.http.MiDeviceApi;

import javax.annotation.Nullable;
import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * 互联网家 Presenter
 * Created by William on 2018/2/3.
 */
public class IotPresenter implements IotContract.Presenter {
    private static final String TAG = IotPresenter.class.getSimpleName();
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;

    @Nullable
    private IotContract.View mView;

    @Inject
    IotPresenter(Context context) {
        mContext = context;
    }

    @Override
    public void subscribe(IotContract.View view) {
        this.mView = view;
        mCompositeSubscription = new CompositeSubscription();
        loadMiDeviceList();
    }

    @Override
    public void unSubscribe() {
        this.mView = null;
        mCompositeSubscription.clear();
    }

    @Override
    public void loadMiDeviceList() {
        Subscription subscription = MiDeviceApi.getDeviceList(mContext)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(list -> {
                    if (mView != null) mView.showMiDeviceList(list);
                }, throwable -> {
                    if (mView != null) mView.showRefreshFail();
                    logUtil.d(TAG, throwable.getMessage());
                });
        mCompositeSubscription.add(subscription);
    }
}