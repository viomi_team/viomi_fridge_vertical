package com.viomi.fridge.vertical.administration.view.fragment;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePickerView;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.view.dialog.BluetoothSettingDialog;
import com.viomi.fridge.vertical.administration.view.dialog.WifiSettingDialog;
import com.viomi.fridge.vertical.common.base.BaseFragment;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.scope.ActivityScoped;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.SDCardUtils;
import com.viomi.fridge.vertical.common.util.logUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import rx.Observable;
import rx.Subscription;

import static android.content.Context.WIFI_SERVICE;

/**
 * 通用设置 Fragment
 * Created by William on 2018/1/31.
 */
@ActivityScoped
public class SettingFragment extends BaseFragment {
    private static final String TAG = SettingFragment.class.getSimpleName();
    private Subscription mSubscription;// 消息订阅
    private BluetoothSettingDialog mBluetoothSettingDialog;// 蓝牙 Dialog
    private WifiSettingDialog mWifiSettingDialog;// Wifi Dialog

    @BindView(R.id.manage_setting_date)
    TextView mDateTextView;// 时间日期
    @BindView(R.id.manage_setting_used_storage)
    TextView mUsedStorageTextView;// 已用空间
    @BindView(R.id.manage_setting_all_storage)
    TextView mAllStorageTextView;// 总共空间
    @BindView(R.id.setting_wifi_status)
    TextView mWifiStatusTextView;// Wifi 连接状态
    @BindView(R.id.setting_bluetooth_status)
    TextView mBluetoothStatusTextView;// 蓝牙状态

    @BindView(R.id.manage_setting_storage)
    ProgressBar mStorageProgressBar;// 存储空间进度条

    @Inject
    public SettingFragment() {

    }

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.fragment_setting;
    }

    @Override
    protected void initWithOnCreateView() {
        showTime(System.currentTimeMillis());
        showStorage();

        mSubscription = RxBus.getInstance().subscribe(busEvent -> {
            switch (busEvent.getMsgId()) {
                case BusEvent.MSG_TIME_MINUTE: // 分钟监听
                    if (getActivity() != null) {
                        getActivity().runOnUiThread(() -> showTime(System.currentTimeMillis()));
                    }
                    break;
            }
        });
        checkWifiState();
        checkBluetoothState();
        registerReceiver();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
        if (mBluetoothSettingDialog != null && mBluetoothSettingDialog.isAdded()) {
            mBluetoothSettingDialog.dismiss();
            mBluetoothSettingDialog = null;
        }
        if (mWifiSettingDialog != null && mWifiSettingDialog.isAdded()) {
            mWifiSettingDialog.dismiss();
            mWifiSettingDialog = null;
        }
        if (getActivity() != null) {
            getActivity().unregisterReceiver(mReceiver);
        }
    }

    /**
     * 点击 Wifi 设置
     */
    @OnClick(R.id.manage_setting_wifi)
    public void onWifiClick() {
        if (mWifiSettingDialog == null) mWifiSettingDialog = new WifiSettingDialog();
        if (mWifiSettingDialog.isAdded()) return;
        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        mWifiSettingDialog.show(fragmentTransaction, TAG);
    }

    /**
     * 点击蓝牙设置
     */
    @OnClick(R.id.manage_setting_bluetooth)
    public void onBluetoothClick() {
        if (mBluetoothSettingDialog == null) mBluetoothSettingDialog = new BluetoothSettingDialog();
        if (mBluetoothSettingDialog.isAdded()) return;
        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        mBluetoothSettingDialog.show(fragmentTransaction, TAG);
    }

    /**
     * 点击日期设置
     */
    @OnClick(R.id.manage_setting_date_layout)
    public void onDateClick() {
        if (isRepeatedClick()) return;
        TimePickerView pvTime = new TimePickerView
                .Builder(getActivity(), (date, v) -> setSystemTime(date.getTime()))
                .setType(new boolean[]{true, true, true, true, true, false})
                .isDialog(true)
                .build();
        pvTime.show();
    }

    /**
     * 显示日期
     */
    private void showTime(long time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
        Date date = new Date(time);
        mDateTextView.setText(simpleDateFormat.format(date));
    }

    /**
     * 显示存储信息
     */
    private void showStorage() {
        float usedMemory = (SDCardUtils.getSDCardTotalSize() - SDCardUtils.getSDCardFreeSize());
        float allMemory = SDCardUtils.getSDCardTotalSize();
        String usedText = String.format(Locale.getDefault(),
                "%.2f", usedMemory / 1024f) + "GB";
        String allText = String.format(Locale.getDefault(),
                "%.2f", allMemory / 1024f) + "GB";
        mUsedStorageTextView.setText(usedText);
        mAllStorageTextView.setText(allText);
        int progress = (int) ((usedMemory / allMemory) * 100);
        mStorageProgressBar.setProgress(progress);
    }

    /**
     * 修改系统时间
     */
    private void setSystemTime(long time) {
        showTime(time);
        Intent intent = new Intent();
        intent.setAction("android.intent.action.USER_SET_TIME");
        intent.putExtra("time", time);
        if (getActivity() != null) getActivity().sendBroadcast(intent);
        Observable.timer(1, TimeUnit.SECONDS)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(aLong -> RxBus.getInstance().post(BusEvent.MSG_TIME_MINUTE), throwable -> logUtil.e(TAG, throwable.getMessage()));

    }

    private void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        intentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        if (getActivity() != null)
            getActivity().registerReceiver(mReceiver, intentFilter);
    }

    private void checkWifiState() {
        WifiManager wifiManager = (WifiManager) FridgeApplication.getContext().getApplicationContext().getSystemService(WIFI_SERVICE);
        if (wifiManager != null) {
            if (!wifiManager.isWifiEnabled())
                mWifiStatusTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.factory_test_heating_closed));
            else {
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                int ipAddress = wifiInfo == null ? 0 : wifiInfo.getIpAddress();
                if (ipAddress == 0) {
                    mWifiStatusTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.status_unconnected));
                } else {
                    String wifiId = wifiInfo.getSSID();
                    mWifiStatusTextView.setText(wifiId.substring(1, wifiId.length() - 1));
                }
            }
        }
    }

    private void checkBluetoothState() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter.isEnabled())
            mBluetoothStatusTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.speech_opened));
        else
            mBluetoothStatusTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.speech_closed));
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() != null) {
                logUtil.d(TAG, intent.getAction());
                switch (intent.getAction()) {
                    case WifiManager.WIFI_STATE_CHANGED_ACTION: // WiFi 状态
                    case ConnectivityManager.CONNECTIVITY_ACTION: // 网络变化
                        checkWifiState();
                        break;
                    case BluetoothAdapter.ACTION_STATE_CHANGED: // 蓝牙状态监听
                        checkBluetoothState();
                        break;
                }
            }
        }
    };
}
