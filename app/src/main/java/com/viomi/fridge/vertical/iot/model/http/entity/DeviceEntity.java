package com.viomi.fridge.vertical.iot.model.http.entity;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * Created by William on 2018/3/6.
 */

public class DeviceEntity {
    @JSONField(name = "devID")
    private String devID;
    @JSONField(name = "GwID")
    private String GwID;
    @JSONField(name = "type")
    private String type;
    @JSONField(name = "device")
    private String device;
    @JSONField(name = "status")
    private String status;

    public String getDevID() {
        return devID;
    }

    public void setDevID(String devID) {
        this.devID = devID;
    }

    public String getGwID() {
        return GwID;
    }

    public void setGwID(String gwID) {
        GwID = gwID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
