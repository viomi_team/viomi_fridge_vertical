package com.viomi.fridge.vertical.iot.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.miot.common.abstractdevice.AbstractDevice;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;
import com.viomi.fridge.vertical.fridgecontrol.model.serial.entity.DeviceParams;
import com.viomi.fridge.vertical.fridgecontrol.model.serial.manager.SerialManager;
import com.viomi.fridge.vertical.iot.model.http.MiDeviceApi;
import com.viomi.fridge.vertical.iot.model.http.entity.DeviceTypeEntity;
import com.viomi.fridge.vertical.iot.model.repository.DishWashingRepository;
import com.viomi.fridge.vertical.iot.model.repository.FanRepository;
import com.viomi.fridge.vertical.iot.model.repository.HeatKettleRepository;
import com.viomi.fridge.vertical.iot.model.repository.RangeHoodRepository;
import com.viomi.fridge.vertical.iot.model.repository.SweepingRobotRepository;
import com.viomi.fridge.vertical.iot.model.repository.WashingMachineRepository;
import com.viomi.fridge.vertical.iot.model.repository.WaterPurifierRepository;
import com.viomi.fridge.vertical.speech.model.repository.SkillRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * 互联网家 Iot Service
 * Created by William on 2018/8/29.
 */
public class IotService extends Service {
    private static final String TAG = IotService.class.getSimpleName();
    private Subscription mIotSubscription;
    private Subscription mRxBusSubscription;
    private CompositeSubscription mCompositeSubscription;
    private CompositeSubscription mHttpCompositeSubscription;
    private boolean mIsShowing = false;// 是否显示
    private IotBinder mBinder;
    private List<DeviceTypeEntity> mList;// 设备分类集合

    @Override
    public void onCreate() {
        super.onCreate();
        mCompositeSubscription = new CompositeSubscription();
        mHttpCompositeSubscription = new CompositeSubscription();
        mBinder = new IotBinder();
        mList = new ArrayList<>();
        initDeviceList();

        mRxBusSubscription = RxBus.getInstance().subscribe(busEvent -> {
            switch (busEvent.getMsgId()) {
                case BusEvent.MSG_IOT_SHOW: // 互联网家界面显示
                    mIsShowing = true;
                    loadDeviceList(mList);
                    break;
                case BusEvent.MSG_IOT_HIDE: // 互联网家界面隐藏
                    mIsShowing = false;
                    break;
                case BusEvent.MSG_IOT_STOP: // 停止刷新数据
                    if (mCompositeSubscription != null) mCompositeSubscription.clear();
                    if (mHttpCompositeSubscription != null) mHttpCompositeSubscription.clear();
                    break;
                case BusEvent.MSG_LOGIN_SUCCESS: // 登录成功
                    loadDeviceList(mList);
                    break;
                case BusEvent.MSG_SERVER_CHANGE: // 服务器环境变化
                case BusEvent.MSG_LOGOUT_SUCCESS: // 退出登录
                    for (DeviceTypeEntity entity : mList) {
                        entity.setPosition(0);
                        entity.setExist(false);
                        entity.setAsking(false);
                        entity.setData(null);
                        entity.getList().clear();
                    }
                    break;
            }
        });
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mRxBusSubscription != null) {
            mRxBusSubscription.unsubscribe();
            mRxBusSubscription = null;
        }
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
        if (mHttpCompositeSubscription != null) {
            mHttpCompositeSubscription.unsubscribe();
            mHttpCompositeSubscription = null;
        }
        if (mList != null) {
            mList.clear();
            mList = null;
        }
    }

    private void initDeviceList() {
        String[] types = FridgeApplication.getContext().getResources().getStringArray(R.array.device_type_list);
        String model = FridgePreference.getInstance().getModel();// 冰箱 Model
        for (int i = 0; i < types.length; i++) {
            DeviceTypeEntity entity = new DeviceTypeEntity();
            entity.setType(types[i]);
            if (model.equals(AppConstants.MODEL_JD)) entity.setOnSale(false);// 京东款屏蔽购买
            else isOnSale(i, entity);
            mList.add(entity);
        }
        // 冰箱数据
        DeviceParams params = SerialManager.getInstance().getDeviceParamsSet();
        String data = "";
        data = data + (params.isCold_switch() ? params.getCold_temp_set() : "--") + ",";
        if (model.equals(AppConstants.MODEL_X5)) {
            data = data + (params.isChangeable_switch() ? params.getChangeable_temp_set() : "--") + ",";
        }
        if (model.equals(AppConstants.MODEL_X3) || model.equals(AppConstants.MODEL_X5)) {
            data = data + (params.getFreezing_temp_set() < -24 ? -24 : params.getFreezing_temp_set());
        } else data = data + params.getFreezing_temp_set();
        mList.get(0).setData(data);
        SkillRepository.getInstance().setIotDevice(mList);
        loadDeviceList(mList);
    }

    private void loadDeviceList(List<DeviceTypeEntity> list) {
        if (mIotSubscription != null) {
            mIotSubscription.unsubscribe();
            mIotSubscription = null;
        }
        mIotSubscription = Observable.interval(0, 10, TimeUnit.SECONDS)
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .flatMap(aLong -> {
                    if (mHttpCompositeSubscription != null) mHttpCompositeSubscription.clear();
                    return MiDeviceApi.getDeviceList(FridgeApplication.getContext());
                })
                .subscribeOn(Schedulers.computation())
                .onTerminateDetach()
                .subscribe(list1 -> {
                    if (mList == null || mList.size() == 0) initDeviceList();
                    else classifyMiDevice(list1, list);
                }, throwable -> {
                    retry();
                    logUtil.e(TAG, throwable.getMessage());
                });
        mCompositeSubscription.add(mIotSubscription);
    }

    private void retry() {
        Subscription subscription = Observable.timer(10, TimeUnit.SECONDS)
                .compose(RxSchedulerUtil.SchedulersTransformer2())
                .onTerminateDetach()
                .subscribe(aLong -> loadDeviceList(mList), throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    /**
     * 根据 Model 进行分类
     *
     * @param miList: MiIot 设备集合
     * @param list:   分类集合
     */
    private void classifyMiDevice(List<AbstractDevice> miList, List<DeviceTypeEntity> list) {
        for (DeviceTypeEntity entity : list) { // 先清除
            entity.setPosition(0);
            entity.setExist(false);
            entity.setAsking(false);
            entity.setData(null);
            entity.getList().clear();
        }
        for (AbstractDevice device : miList) {
            switch (device.getDeviceModel()) {
                // 烟机
                case AppConstants.VIOMI_HOOD_A4:
                case AppConstants.VIOMI_HOOD_A5:
                case AppConstants.VIOMI_HOOD_A6:
                case AppConstants.VIOMI_HOOD_A7:
                case AppConstants.VIOMI_HOOD_C1:
                case AppConstants.VIOMI_HOOD_H1:
                case AppConstants.VIOMI_HOOD_H2:
                case AppConstants.VIOMI_HOOD_X2:
                    list.get(2).getList().add(device);// 分类添加
                    if (device.isOnline() && !list.get(2).isExist()) {
                        list.get(2).setExist(true);// 有在线设备
                        list.get(2).setPosition(list.get(2).getList().size() - 1);
                    }
                    if (mIsShowing && device.isOnline() && list.get(2).getData() == null && !list.get(2).isAsking()) { // 获取在线烟机数据
                        loadRangeHood(device, list);// 该分类无请求数据
                    }
                    break;
                // 净水器
                case AppConstants.YUNMI_WATERPURI_V1:
                case AppConstants.YUNMI_WATERPURI_V2:
                case AppConstants.YUNMI_WATERPURI_S1:
                case AppConstants.YUNMI_WATERPURI_S2:
                case AppConstants.YUNMI_WATERPURI_C1:
                case AppConstants.YUNMI_WATERPURI_C2:
                case AppConstants.YUNMI_WATERPURI_X3:
                case AppConstants.YUNMI_WATERPURI_X5:
                case AppConstants.YUNMI_WATERPURIFIER_V1:
                case AppConstants.YUNMI_WATERPURIFIER_V2:
                case AppConstants.YUNMI_WATERPURIFIER_V3:
                case AppConstants.YUNMI_WATERPURI_LX2:
                case AppConstants.YUNMI_WATERPURI_LX3:
                case AppConstants.YUNMI_WATERPURI_LX5:
                    list.get(3).getList().add(device);// 分类添加
                    if (device.isOnline() && !list.get(3).isExist()) {
                        list.get(3).setExist(true);// 有在线设备
                        list.get(3).setPosition(list.get(3).getList().size() - 1);
                    }
                    if (mIsShowing && device.isOnline() && list.get(3).getData() == null && !list.get(3).isAsking())
                        loadWaterPurifier(device, list);// 获取在线净水器数据
                    break;
                // 即热饮水吧
                case AppConstants.YUNMI_KETTLE_R1:
                case AppConstants.YUNMI_KETTLE_R2:
                case AppConstants.YUNMI_KETTLE_R3:
                    list.get(4).getList().add(device);// 分类添加
                    if (device.isOnline() && !list.get(4).isExist()) {
                        list.get(4).setExist(true);// 有在线设备
                        list.get(4).setPosition(list.get(4).getList().size() - 1);
                    }
                    if (mIsShowing && device.isOnline() && list.get(4).getData() == null && !list.get(4).isAsking())
                        loadHeatKettle(device, list);// 获取在线即热饮水吧数据
                    break;
                // 洗碗机
                case AppConstants.VIOMI_DISH_WASHER_V01:
                case AppConstants.VIOMI_DISH_WASHER_V02:
                    list.get(7).getList().add(device);// 分类添加
                    if (device.isOnline() && !list.get(7).isExist()) {
                        list.get(7).setExist(true);// 有在线设备
                        list.get(7).setPosition(list.get(7).getList().size() - 1);
                    }
                    if (mIsShowing && device.isOnline() && list.get(7).getData() == null && !list.get(7).isAsking())
                        loadDishWashing(device, list);// 获取在线洗碗机数据
                    break;
                // 洗衣机
                case AppConstants.VIOMI_WASHER_U1:
                case AppConstants.VIOMI_WASHER_U2:
                    list.get(8).getList().add(device);// 分类添加
                    if (device.isOnline() && !list.get(8).isExist()) {
                        list.get(8).setExist(true);// 有在线设备
                        list.get(8).setPosition(list.get(8).getList().size() - 1);
                    }
                    if (mIsShowing && device.isOnline() && list.get(8).getData() == null && !list.get(8).isAsking())
                        loadWashingMachine(device, list);// 获取在线洗衣机数据
                    break;
                // 风扇
                case AppConstants.VIOMI_FAN_V1:
                    list.get(11).getList().add(device);// 分类添加
                    if (device.isOnline() && !list.get(11).isExist()) {
                        list.get(11).setExist(true);// 有在线设备
                        list.get(11).setPosition(list.get(11).getList().size() - 1);
                    }
                    if (mIsShowing && device.isOnline() && list.get(11).getData() == null && !list.get(11).isAsking())
                        loadFan(device, list);// 获取在线风扇数据
                    break;
                // 扫地机器人
                case AppConstants.VIOMI_VACUUM_V1:
                    list.get(14).getList().add(device);// 分类添加
                    if (device.isOnline() && !list.get(14).isExist()) {
                        list.get(14).setExist(true);// 有在线设备
                        list.get(14).setPosition(list.get(14).getList().size() - 1);
                    }
                    if (mIsShowing && device.isOnline() && list.get(14).getData() == null && !list.get(14).isAsking())
                        loadSweepingRobot(device, list);// 获取在线扫地机器人数据
                    break;
                // 蒸烤箱
                case AppConstants.VIOMI_OVEN_V1:
                case AppConstants.VIOMI_OVEN_V2:
                    list.get(1).getList().add(device);// 分类添加
//                    if (device.isOnline() && !list.get(1).isExist()) {
//                        list.get(1).setExist(true);// 有在线设备
//                        list.get(1).setPosition(list.get(1).getList().size() - 1);
//                    }
                    // TODO 获取属性
                    break;
            }
        }
        // 冰箱数据
        DeviceParams params = SerialManager.getInstance().getDeviceParamsSet();
        String model = FridgePreference.getInstance().getModel();
        String data = "";
        data = data + (params.isCold_switch() ? params.getCold_temp_set() : "--") + ",";
        if (model.equals(AppConstants.MODEL_X5)) {
            data = data + (params.isChangeable_switch() ? params.getChangeable_temp_set() : "--") + ",";
        }
        if (model.equals(AppConstants.MODEL_X3) || model.equals(AppConstants.MODEL_X5)) {
            data = data + (params.getFreezing_temp_set() < -24 ? -24 : params.getFreezing_temp_set());
        } else data = data + params.getFreezing_temp_set();
        list.get(0).setData(data);
        SkillRepository.getInstance().setIotDevice(mList);
        Subscription subscription = Observable.just(list)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(list1 -> {
                    for (int i = 0; i < list1.size(); i++) {
                        if (!list1.get(i).isAsking() && mIsShowing)
                            RxBus.getInstance().post(BusEvent.MSG_IOT_ITEM_REFRESH, i);
                    }
                    if (mIsShowing) RxBus.getInstance().post(BusEvent.MSG_IOT_REFRESH);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    /**
     * 获取烟机工作状态
     */
    private void loadRangeHood(AbstractDevice device, List<DeviceTypeEntity> list) {
        list.get(2).setAsking(true);
        Subscription subscription = RangeHoodRepository.getProp(device.getDeviceId())
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (rpcResult.getCode() != 0 || rpcResult.getList().size() == 0) {
                        list.get(2).setData(FridgeApplication.getContext().getResources().getString(R.string.iot_load_data_fail));
                    } else {
                        list.get(2).setData(RangeHoodRepository.switchHoodStatus((int) rpcResult.getList().get(1), (int) rpcResult.getList().get(2)));
                    }
                    RxBus.getInstance().post(BusEvent.MSG_IOT_ITEM_REFRESH, 2);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mHttpCompositeSubscription.add(subscription);
    }

    /**
     * 获取净水器 TDS 值
     */
    private void loadWaterPurifier(AbstractDevice device, List<DeviceTypeEntity> list) {
        list.get(3).setAsking(true);
        Subscription subscription = WaterPurifierRepository.miGetProp(device.getDeviceId())
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (rpcResult.getCode() != 0 || rpcResult.getList().size() == 0) {
                        list.get(3).setData(FridgeApplication.getContext().getResources().getString(R.string.iot_load_data_fail));
                    } else {
                        list.get(3).setData(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_tds_text) + String.valueOf(rpcResult.getList().get(1)));
                    }
                    RxBus.getInstance().post(BusEvent.MSG_IOT_ITEM_REFRESH, 3);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mHttpCompositeSubscription.add(subscription);
    }

    /**
     * 获取即热饮水吧出水温度
     */
    private void loadHeatKettle(AbstractDevice device, List<DeviceTypeEntity> list) {
        list.get(4).setAsking(true);
        Subscription subscription = HeatKettleRepository.getProp(device.getDeviceId())
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (rpcResult.getCode() != 0 || rpcResult.getList().size() == 0) {
                        list.get(4).setData(FridgeApplication.getContext().getResources().getString(R.string.iot_load_data_fail));
                    } else {
                        list.get(4).setData(String.valueOf(rpcResult.getList().get(0)) + FridgeApplication.getContext().getResources().getString(R.string.fridge_temp_unit));
                    }
                    RxBus.getInstance().post(BusEvent.MSG_IOT_ITEM_REFRESH, 4);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mHttpCompositeSubscription.add(subscription);
    }

    /**
     * 获取洗碗机工作状态
     */
    private void loadDishWashing(AbstractDevice device, List<DeviceTypeEntity> list) {
        list.get(7).setAsking(true);
        Subscription subscription = DishWashingRepository.getProp(device.getDeviceId())
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (rpcResult.getCode() != 0 || rpcResult.getList().size() == 0) {
                        list.get(7).setData(FridgeApplication.getContext().getResources().getString(R.string.iot_load_data_fail));
                    } else {
                        list.get(7).setData(DishWashingRepository.switchWashProcess((int) rpcResult.getList().get(7), (int) rpcResult.getList().get(8), (int) rpcResult.getList().get(6)));
                    }
                    RxBus.getInstance().post(BusEvent.MSG_IOT_ITEM_REFRESH, 7);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mHttpCompositeSubscription.add(subscription);
    }

    /**
     * 获取洗衣机洗涤过程
     */
    private void loadWashingMachine(AbstractDevice device, List<DeviceTypeEntity> list) {
        list.get(8).setAsking(true);
        Subscription subscription = WashingMachineRepository.getProp(device.getDeviceId())
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (rpcResult.getCode() != 0 || rpcResult.getList().size() == 0) {
                        list.get(8).setData(FridgeApplication.getContext().getResources().getString(R.string.iot_load_data_fail));
                    } else {
                        list.get(8).setData(WashingMachineRepository.switchWashProcess((int) rpcResult.getList().get(1), (int) rpcResult.getList().get(2)));
                    }
                    RxBus.getInstance().post(BusEvent.MSG_IOT_ITEM_REFRESH, 8);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mHttpCompositeSubscription.add(subscription);
    }

    /**
     * 获取风扇工作模式
     */
    private void loadFan(AbstractDevice device, List<DeviceTypeEntity> list) {
        list.get(11).setAsking(true);
        Subscription subscription = FanRepository.getProp(device.getDeviceId())
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (rpcResult.getCode() != 0 || rpcResult.getList().size() == 0) {
                        list.get(11).setData(FridgeApplication.getContext().getResources().getString(R.string.iot_load_data_fail));
                    } else {
                        list.get(11).setData(FanRepository.switchWindMode((int) rpcResult.getList().get(0)));
                    }
                    RxBus.getInstance().post(BusEvent.MSG_IOT_ITEM_REFRESH, 11);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mHttpCompositeSubscription.add(subscription);
    }

    /**
     * 获取扫地机器人工作状态
     */
    private void loadSweepingRobot(AbstractDevice device, List<DeviceTypeEntity> list) {
        list.get(14).setAsking(true);
        Subscription subscription = SweepingRobotRepository.getProp(device.getDeviceId())
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (rpcResult.getCode() != 0 || rpcResult.getList().size() == 0) {
                        list.get(14).setData(FridgeApplication.getContext().getResources().getString(R.string.iot_load_data_fail));
                    } else {
                        list.get(14).setData(SweepingRobotRepository.switchRunState((int) rpcResult.getList().get(0), (int) rpcResult.getList().get(1)));
                    }
                    RxBus.getInstance().post(BusEvent.MSG_IOT_ITEM_REFRESH, 14);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mHttpCompositeSubscription.add(subscription);
    }

    public List<DeviceTypeEntity> getDeviceList() {
        return mList;
    }

    /**
     * 设备是否已上架
     */
    private void isOnSale(int type, DeviceTypeEntity entity) {
        switch (type) {
            case 1: // 蒸烤箱
            case 5: // 中央净水器
            case 6: // 中央软水机
            case 9: // 热水器
            case 10: // 空气净化器
            case 12: // 香熏机
            case 13: // 养生壶
                entity.setOnSale(false);
                break;
            case 2: // 烟灶
            case 3: // 净水器
            case 4: // 即热饮水吧
            case 7: // 洗碗机
            case 8: // 洗衣机
            case 11: // 风扇
            case 14: // 扫地机器人
                entity.setOnSale(true);
                break;
        }
    }

    public class IotBinder extends Binder {
        public IotService getService() {
            return IotService.this;
        }
    }
}
