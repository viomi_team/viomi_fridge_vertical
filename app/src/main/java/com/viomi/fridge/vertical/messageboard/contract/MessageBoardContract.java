package com.viomi.fridge.vertical.messageboard.contract;

import com.viomi.fridge.vertical.common.callback.BasePresenter;
import com.viomi.fridge.vertical.messageboard.model.entity.MessageBoardEntity;
import com.viomi.widget.sketchview.SketchView;

import java.util.List;

/**
 * 留言板 Contract
 * Created by William on 2018/4/10.
 */
public interface MessageBoardContract {
    interface View {
        void saveFinish();// 留言板保存成功

        void setTitleBarUi(List<MessageBoardEntity> list);// 根据留言记录设置标题栏
    }

    interface Presenter extends BasePresenter<View> {
        void save(SketchView sketchView);// 保存留言板图片

        void load();// 读取全部留言
    }
}