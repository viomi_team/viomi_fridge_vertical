package com.viomi.fridge.vertical.message.manager;

import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.message.entity.ActivityMessage;
import com.viomi.fridge.vertical.message.entity.ActivityMessageDao;
import com.viomi.fridge.vertical.message.entity.DeviceMessage;
import com.viomi.fridge.vertical.message.entity.DeviceMessageDao;
import com.viomi.fridge.vertical.message.entity.MallMessage;
import com.viomi.fridge.vertical.message.entity.MallMessageDao;

import java.util.List;

/**
 * 消息数据库管理类
 * Created by nanquan on 2018/2/7.
 */
public class MessageDaoManager {
    private final ActivityMessageDao mActivityMessageDao;
    private final MallMessageDao mMallMessageDao;
    private final DeviceMessageDao mDeviceMessageDao;

    public MessageDaoManager() {
        mActivityMessageDao = FridgeApplication.getInstance().getDaoSession().getActivityMessageDao();
        mMallMessageDao = FridgeApplication.getInstance().getDaoSession().getMallMessageDao();
        mDeviceMessageDao = FridgeApplication.getInstance().getDaoSession().getDeviceMessageDao();
    }

    public void saveActivityMessages(List<ActivityMessage> list) {
        mActivityMessageDao.insertOrReplaceInTx(list);
    }

    public List<ActivityMessage> getActivityMessages() {
        return mActivityMessageDao.queryBuilder()
                .orderDesc(ActivityMessageDao.Properties.Time)
                .build()
                .list();
    }

    public void deleteActivityMessage(String id) {
        mActivityMessageDao.deleteByKey(id);
    }

    public void saveMallMessages(List<MallMessage> list) {
        mMallMessageDao.insertOrReplaceInTx(list);
    }

    public List<MallMessage> getMallMessages() {
        return mMallMessageDao.queryBuilder()
                .orderDesc(MallMessageDao.Properties.Time)
                .build()
                .list();
    }

    public void deleteMallMessage(String id) {
        mMallMessageDao.deleteByKey(id);
    }

    public void saveDeviceMessages(List<DeviceMessage> list) {
        mDeviceMessageDao.insertOrReplaceInTx(list);
    }

    public void saveDeviceMessage(DeviceMessage message) {
        mDeviceMessageDao.insertOrReplace(message);
    }

    public List<DeviceMessage> getDeviceMessages() {
        return mDeviceMessageDao.queryBuilder()
                .orderDesc(DeviceMessageDao.Properties.Time)
                .build()
                .list();
    }

    public void deleteDeviceMessage(String id) {
        mDeviceMessageDao.deleteByKey(id);
    }
}