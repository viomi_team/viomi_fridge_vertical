package com.viomi.fridge.vertical.message.module;

import com.viomi.fridge.vertical.common.scope.FragmentScoped;
import com.viomi.fridge.vertical.message.view.dialog.MainMessageSettingFragment;
import com.viomi.fridge.vertical.message.view.dialog.MessageSettingDialog;
import com.viomi.fridge.vertical.message.view.dialog.TimeMessageSettingFragment;
import com.viomi.fridge.vertical.message.view.fragment.ActivityMessageFragment;
import com.viomi.fridge.vertical.message.view.fragment.DeviceMessageFragment;
import com.viomi.fridge.vertical.message.view.fragment.MallMessageFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * 消息中心 Module
 */

@Module
public abstract class MessageCenterModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract ActivityMessageFragment activityMessageFragment();

    @FragmentScoped
    @ContributesAndroidInjector
    abstract MallMessageFragment mallMessageFragment();

    @FragmentScoped
    @ContributesAndroidInjector
    abstract DeviceMessageFragment deviceMessageFragment();

    @FragmentScoped
    @ContributesAndroidInjector
    abstract MessageSettingDialog messageSettingDialog();

    @FragmentScoped
    @ContributesAndroidInjector
    abstract MainMessageSettingFragment mainMessageSettingFragment();

    @FragmentScoped
    @ContributesAndroidInjector
    abstract TimeMessageSettingFragment timeMessageSettingFragment();

}
