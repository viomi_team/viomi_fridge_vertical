package com.viomi.fridge.vertical.iot.model.http.entity;

import java.util.List;

/**
 * Created by hailang on 2018/5/21 0021.
 */

public class CameraStatus {


    /**
     * mobBaseRes : {"code":100,"desc":"处理成功","result":[{"devId":"9BE3600F004B1200","endpointNumber":1,"attributeId":32776,"attributeValue":"04"},{"devId":"9BE3600F004B1200","endpointNumber":1,"attributeId":32773,"attributeValue":"101000102000000"}]}
     */

    private MobBaseResBean mobBaseRes;

    public MobBaseResBean getMobBaseRes() {
        return mobBaseRes;
    }

    public void setMobBaseRes(MobBaseResBean mobBaseRes) {
        this.mobBaseRes = mobBaseRes;
    }

    public static class MobBaseResBean {
        /**
         * code : 100
         * desc : 处理成功
         * result : [{"devId":"9BE3600F004B1200","endpointNumber":1,"attributeId":32776,"attributeValue":"04"},{"devId":"9BE3600F004B1200","endpointNumber":1,"attributeId":32773,"attributeValue":"101000102000000"}]
         */

        private int code;
        private String desc;
        private List<ResultBean> result;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public List<ResultBean> getResult() {
            return result;
        }

        public void setResult(List<ResultBean> result) {
            this.result = result;
        }

        public static class ResultBean {
            /**
             * devId : 9BE3600F004B1200
             * endpointNumber : 1
             * attributeId : 32776
             * attributeValue : 04
             */

            private String devId;
            private int endpointNumber;
            private int attributeId;
            private String attributeValue;

            public String getDevId() {
                return devId;
            }

            public void setDevId(String devId) {
                this.devId = devId;
            }

            public int getEndpointNumber() {
                return endpointNumber;
            }

            public void setEndpointNumber(int endpointNumber) {
                this.endpointNumber = endpointNumber;
            }

            public int getAttributeId() {
                return attributeId;
            }

            public void setAttributeId(int attributeId) {
                this.attributeId = attributeId;
            }

            public String getAttributeValue() {
                return attributeValue;
            }

            public void setAttributeValue(String attributeValue) {
                this.attributeValue = attributeValue;
            }
        }
    }
}
