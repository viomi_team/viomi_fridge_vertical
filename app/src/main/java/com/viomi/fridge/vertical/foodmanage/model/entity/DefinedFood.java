package com.viomi.fridge.vertical.foodmanage.model.entity;

/**
 * 根据滤芯定义各食材保质期
 * Created by William on 2018/1/10.
 */
public class DefinedFood {
    private String name;// 名称
    private int coldNoFilterTime;// 无滤芯冷藏保存时间
    private int coldFilterTime;// 有滤芯冷藏保存时间
    private int freezingNoFilterTime;// 无滤芯冷冻保存时间
    private int freezingFilterTime;// 有滤芯冷冻保存时间

    public int getColdNoFilterTime() {
        return coldNoFilterTime;
    }

    public void setColdNoFilterTime(int coldNoFilterTime) {
        this.coldNoFilterTime = coldNoFilterTime;
    }

    public int getColdFilterTime() {
        return coldFilterTime;
    }

    public void setColdFilterTime(int coldFilterTime) {
        this.coldFilterTime = coldFilterTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFreezingNoFilterTime() {
        return freezingNoFilterTime;
    }

    public void setFreezingNoFilterTime(int freezingNoFilterTime) {
        this.freezingNoFilterTime = freezingNoFilterTime;
    }

    public int getFreezingFilterTime() {
        return freezingFilterTime;
    }

    public void setFreezingFilterTime(int freezingFilterTime) {
        this.freezingFilterTime = freezingFilterTime;
    }
}