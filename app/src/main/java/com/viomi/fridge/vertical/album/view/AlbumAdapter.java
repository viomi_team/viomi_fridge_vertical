package com.viomi.fridge.vertical.album.view;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.album.model.entity.AlbumEntity;
import com.viomi.fridge.vertical.common.base.BaseRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 电子相册适配器
 * Created by William on 2018/1/22.
 */
public class AlbumAdapter extends BaseRecyclerViewAdapter<AlbumAdapter.AlbumHolder> {
    private List<AlbumEntity> mList;// 相册集合
    private boolean mEditMode = false;// 是否编辑模式
    private int mSelectCount = 0;// 选中个数

    public AlbumAdapter(List<AlbumEntity> list) {
        this.mList = list;
        if (this.mList == null) mList = new ArrayList<>();
    }

    @Override
    public AlbumHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_holder_album, parent, false);
        return new AlbumHolder(view, this);
    }

    @Override
    public void onBindViewHolder(AlbumHolder holder, int position) {
        AlbumEntity entity = mList.get(position);
        if (mEditMode) { // 编辑模式
            holder.imageView.setVisibility(View.VISIBLE);
            if (entity.isSelected()) holder.imageView.setSelected(true);
            else holder.imageView.setSelected(false);
        } else holder.imageView.setVisibility(View.GONE);

        ImageRequest request = ImageRequestBuilder
                .newBuilderWithSource(Uri.fromFile(entity.getFile()))
                .setResizeOptions(new ResizeOptions((int) FridgeApplication.getContext().getResources().getDimension(R.dimen.px_x_248), (int) FridgeApplication.getContext().getResources().getDimension(R.dimen.px_y_248)))
                .build();
        PipelineDraweeController controller = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                .setOldController(holder.simpleDraweeView.getController())
                .setImageRequest(request)
                .build();
        holder.simpleDraweeView.setController(controller);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public boolean isEditMode() {
        return mEditMode;
    }

    public void setEditMode(boolean editMode) {
        mEditMode = editMode;
        if (!mEditMode) mSelectCount = 0;
    }

    public int getSelectCount() {
        return mSelectCount;
    }

    public void setSelectCount(int selectCount) {
        mSelectCount = selectCount;
    }

    class AlbumHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.holder_album_img)
        SimpleDraweeView simpleDraweeView;

        @BindView(R.id.holder_album_select)
        ImageView imageView;

        AlbumHolder(View itemView, AlbumAdapter adapter) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> adapter.onItemHolderClick(this, 200));
        }
    }
}