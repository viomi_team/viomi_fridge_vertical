package com.viomi.fridge.vertical.administration.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.contract.ManageContract;
import com.viomi.fridge.vertical.administration.model.entity.QRCodeBase;
import com.viomi.fridge.vertical.administration.model.preference.LoginPreference;
import com.viomi.fridge.vertical.administration.view.dialog.LoginQRCodeDialog;
import com.viomi.fridge.vertical.administration.view.fragment.AboutFragment;
import com.viomi.fridge.vertical.administration.view.fragment.FileFragment;
import com.viomi.fridge.vertical.administration.view.fragment.MediaFragment;
import com.viomi.fridge.vertical.administration.view.fragment.SettingFragment;
import com.viomi.fridge.vertical.album.activity.AlbumActivity;
import com.viomi.fridge.vertical.common.base.BaseActivity;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;
import com.viomi.fridge.vertical.speech.view.fragment.SpeechFragment;
import com.viomi.widget.dialog.BaseAlertDialog;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import rx.Subscription;

/**
 * 管理中心 Activity
 * Created by William on 2018/1/26.
 */
public class ManageActivity extends BaseActivity implements ManageContract.View {
    private static final String TAG = ManageActivity.class.getSimpleName();
    private boolean mIsLogin = false;// 是否已登录
    private Fragment mCurrentFragment;// 当前 Fragment
    private Subscription mSubscription;// 消息订阅
    private LoginQRCodeDialog mLoginQRCodeDialog;// 登录 Dialog

    @Inject
    ManageContract.Presenter mPresenter;

    @Inject
    SettingFragment mSettingFragment;// 通用设置 Fragment
    @Inject
    MediaFragment mMediaFragment;// 屏幕与声音 Fragment
    @Inject
    SpeechFragment mVoiceFragment;// 语音助手 Fragment
    @Inject
    AboutFragment mAboutFragment;// 关于软件 Fragment
    @Inject
    FileFragment mFileFragment;// 文件管理 Fragment

    @BindView(R.id.manage_login)
    TextView mLoginTextView;// 登录
    @BindView(R.id.manage_common)
    TextView mCommonTextView;// 通用设置
    @BindView(R.id.manage_media)
    TextView mMediaTextView;// 屏幕与声音
    @BindView(R.id.manage_voice)
    TextView mVoiceTextView;// 语音助手
    @BindView(R.id.manage_about)
    TextView mAboutTextView;// 关于软件
    @BindView(R.id.manage_file)
    TextView mFileTextView;// 文件管理
    @BindView(R.id.manage_user_name)
    TextView mUserNameTextView;// 用户名称
    @BindView(R.id.manage_account)
    TextView mAccountTextView;// 账号
    @BindView(R.id.manage_mi_login)
    TextView mMiBindTextView;// 米家绑定

    @BindView(R.id.manage_head_icon)
    SimpleDraweeView mSimpleDraweeView;// 头像

    @BindView(R.id.manage_default_icon)
    ImageView mDefaultImageView;// 默认图标

    @BindView(R.id.manage_line)
    View mLineView;// 分割线
    @BindView(R.id.manage_mi_line)
    View mMiLineView;// 分割线

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        layoutId = R.layout.activity_manage;
        mTitle = getResources().getString(R.string.management_login_title);
        super.onCreate(savedInstanceState);
        mPresenter.subscribe(this);// 订阅

        mSubscription = RxBus.getInstance().subscribe(busEvent -> {
            switch (busEvent.getMsgId()) {
                case BusEvent.MSG_LOGOUT_SUCCESS: // 注销登录成功
                    mMiBindTextView.setText(getResources().getString(R.string.management_mi_iot_no_bind));
                    mPresenter.loadUserInfo();
                    break;
                case BusEvent.MSG_LOGIN_SUCCESS: // 登录成功
                    mMiBindTextView.setText(getResources().getString(R.string.management_mi_iot_has_bind));
                    mPresenter.loadUserInfo();
                    break;
                case BusEvent.MSG_MI_BIND: // 米家绑定
                    runOnUiThread(() -> mMiBindTextView.setText(getResources().getString(R.string.management_mi_iot_has_bind)));
                    break;
                case BusEvent.MSG_MI_UNBIND: // 米家解绑
                    runOnUiThread(() -> mMiBindTextView.setText(getResources().getString(R.string.management_mi_iot_no_bind)));
                    break;
            }
        });

        if (FridgePreference.getInstance().getModel().equals(AppConstants.MODEL_JD)) { // 京东定制款取消语音功能
            mVoiceTextView.setVisibility(View.GONE);
            mLineView.setVisibility(View.GONE);
        }

        if (FridgePreference.getInstance().getModel().equals(AppConstants.MODEL_X4)) { // 雪琪 450
            mMiBindTextView.setVisibility(View.VISIBLE);
            mMiLineView.setVisibility(View.VISIBLE);
            if (LoginPreference.getInstance().getBindStatus()) {
                mMiBindTextView.setText(getResources().getString(R.string.management_mi_iot_has_bind));
            } else
                mMiBindTextView.setText(getResources().getString(R.string.management_mi_iot_no_bind));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) {
            mPresenter.unSubscribe();
            mPresenter = null;
        }
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
        if (mLoginQRCodeDialog != null && mLoginQRCodeDialog.isAdded()) {
            mLoginQRCodeDialog.dismiss();
            mLoginQRCodeDialog = null;
        }
    }

    @OnClick(R.id.manage_login)
    public void loginAndQuit() { // 登录或注销登录
        if (mLoginQRCodeDialog == null) mLoginQRCodeDialog = new LoginQRCodeDialog();
        if (mLoginQRCodeDialog.isAdded()) return;
        if (!mIsLogin) { // 未登录
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putBoolean("miBind", false);
            mLoginQRCodeDialog.setArguments(bundle);
            mLoginQRCodeDialog.show(fragmentTransaction, TAG);
        } else {
            BaseAlertDialog dialog = new BaseAlertDialog(ManageActivity.this, getResources().getString(R.string.management_logout_message),
                    getResources().getString(R.string.cancel), getResources().getString(R.string.confirm));
            dialog.show();
            dialog.setOnLeftClickListener(dialog::dismiss);
            dialog.setOnRightClickListener(() -> {
                dialog.dismiss();
                if (mPresenter != null) mPresenter.logout(1);// 退出登录
            });
        }
    }

    @OnClick(R.id.manage_mi_login)
    public void bindOrUnbind() { // 米家绑定或解绑
        if (mLoginQRCodeDialog == null) mLoginQRCodeDialog = new LoginQRCodeDialog();
        if (mLoginQRCodeDialog.isAdded()) return;
        if (!LoginPreference.getInstance().getBindStatus()) { // 未绑定
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putBoolean("miBind", true);
            mLoginQRCodeDialog.setArguments(bundle);
            mLoginQRCodeDialog.show(fragmentTransaction, TAG);
        } else {
            BaseAlertDialog dialog = new BaseAlertDialog(ManageActivity.this, getResources().getString(R.string.management_mi_iot_unbind_title),
                    getResources().getString(R.string.cancel), getResources().getString(R.string.confirm));
            dialog.show();
            dialog.setOnLeftClickListener(dialog::dismiss);
            dialog.setOnRightClickListener(() -> {
                dialog.dismiss();
                if (mPresenter != null) mPresenter.logout(2);// 解除绑定
            });
        }
    }

    @OnClick({R.id.manage_common, R.id.manage_media, R.id.manage_voice, R.id.manage_about, R.id.manage_file})
    public void itemClick(TextView textView) {
        mCommonTextView.setBackgroundColor(0xFFFFFFFF);
        mMediaTextView.setBackgroundColor(0xFFFFFFFF);
        mVoiceTextView.setBackgroundColor(0xFFFFFFFF);
        mAboutTextView.setBackgroundColor(0xFFFFFFFF);
        mFileTextView.setBackgroundColor(0xFFFFFFFF);
        textView.setBackgroundColor(0xFFF4F4F4);
        if (mDefaultImageView.getVisibility() != View.GONE)
            mDefaultImageView.setVisibility(View.GONE);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Fragment fragment = null;
        switch (textView.getId()) {
            case R.id.manage_common: // 通用设置
                fragment = mSettingFragment;
                break;
            case R.id.manage_media: // 屏幕与声音
                fragment = mMediaFragment;
                break;
            case R.id.manage_voice: // 语音助手
                fragment = mVoiceFragment;
                break;
            case R.id.manage_about: // 关于软件
                fragment = mAboutFragment;
                break;
            case R.id.manage_file: // 文件管理
                fragment = mFileFragment;
                break;
        }
        if (mCurrentFragment == fragment) return;
        if (mCurrentFragment != null)
            transaction.remove(mCurrentFragment).add(R.id.manage_fragment, fragment).commit();
        else transaction.add(R.id.manage_fragment, fragment).commit();
        mCurrentFragment = fragment;
    }

    @OnClick(R.id.manage_album)
    public void enterAlbum() { // 进入电子相册
        Intent intent = new Intent(ManageActivity.this, AlbumActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.manage_reboot)
    public void reboot() { // 重启系统
        if (isRepeatedClick()) return;
        BaseAlertDialog dialog = new BaseAlertDialog(ManageActivity.this, getResources().getString(R.string.management_reboot_tip),
                getResources().getString(R.string.cancel), getResources().getString(R.string.confirm));
        dialog.setOnLeftClickListener(dialog::dismiss);
        dialog.setOnRightClickListener(() -> {
            Intent intent = new Intent("com.gmt.fridge.action.dameon.reboot");
            sendBroadcast(intent);
            dialog.dismiss();
        });
        dialog.show();
    }

    @Override
    public void showUserInfo(QRCodeBase qrCodeBase) {
        Uri uri;
        if (qrCodeBase != null) { // 已登录
            mIsLogin = true;
            if (qrCodeBase.getLoginQRCode().getUserInfo().getHeadImg() == null || qrCodeBase.getLoginQRCode().getUserInfo().getHeadImg().equals("null") ||
                    qrCodeBase.getLoginQRCode().getUserInfo().getHeadImg().equals(""))
                uri = Uri.parse("android.resource://" + FridgeApplication.getContext().getPackageName() + "/" + R.drawable.icon_user_head_default);
            else uri = Uri.parse(qrCodeBase.getLoginQRCode().getUserInfo().getHeadImg());
            mLoginTextView.setText(getResources().getString(R.string.management_login_quit));
            mLoginTextView.setBackgroundResource(R.drawable.btn_bg_green_corner_59);
            mLoginTextView.setTextColor(0xFFFFFFFF);
            mUserNameTextView.setText(qrCodeBase.getLoginQRCode().getUserInfo().getNickname());
            mAccountTextView.setText(String.format(getResources().getString(R.string.management_account), qrCodeBase.getLoginQRCode().getUserInfo().getAccount()));
            mAccountTextView.setVisibility(View.VISIBLE);
        } else {
            mIsLogin = false;
            uri = Uri.parse("android.resource://" + FridgeApplication.getContext().getPackageName() + "/" + R.drawable.icon_user_head_default);
            mLoginTextView.setText(getResources().getString(R.string.management_login));
            mLoginTextView.setBackgroundResource(R.drawable.btn_rim_green_corner_59);
            mLoginTextView.setTextColor(0xFF44D4A0);
            mUserNameTextView.setText(getResources().getString(R.string.management_no_login));
            mAccountTextView.setVisibility(View.GONE);
        }
        ImageRequest request = ImageRequestBuilder
                .newBuilderWithSource(uri)
                .setResizeOptions(new ResizeOptions((int) getResources().getDimension(R.dimen.px_x_120), (int) getResources().getDimension(R.dimen.px_y_120)))
                .build();
        PipelineDraweeController controller = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                .setOldController(mSimpleDraweeView.getController())
                .setImageRequest(request)
                .build();
        mSimpleDraweeView.setController(controller);
    }
}
