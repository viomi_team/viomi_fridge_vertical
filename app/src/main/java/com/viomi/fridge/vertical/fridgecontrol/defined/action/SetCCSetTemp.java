package com.viomi.fridge.vertical.fridgecontrol.defined.action;

import com.viomi.fridge.vertical.fridgecontrol.defined.ViomiDefined;
import com.viomi.fridge.vertical.fridgecontrol.defined.property.CCSetTemp;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetCCSetTemp extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setCCSetTemp.toActionType();

    public SetCCSetTemp() {
        super(TYPE);

        super.addArgument(CCSetTemp.TYPE.toString());
    }
}