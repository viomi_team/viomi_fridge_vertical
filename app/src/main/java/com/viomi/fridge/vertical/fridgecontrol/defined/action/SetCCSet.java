package com.viomi.fridge.vertical.fridgecontrol.defined.action;

import com.viomi.fridge.vertical.fridgecontrol.defined.ViomiDefined;
import com.viomi.fridge.vertical.fridgecontrol.defined.property.CCSet;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetCCSet extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setCCSet.toActionType();

    public SetCCSet() {
        super(TYPE);

        super.addArgument(CCSet.TYPE.toString());
    }
}