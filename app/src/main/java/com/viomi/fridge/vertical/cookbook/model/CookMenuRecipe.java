package com.viomi.fridge.vertical.cookbook.model;

import java.io.Serializable;
import java.util.List;

/**
 * 食谱烹饪
 * Created by nanquan on 2018/2/12.
 */
public class CookMenuRecipe implements Serializable {
    private String img;
    private String ingredients;
    private String ingredients2;
    private String method;
    private String sumary;
    private String title;
    private List<CookMenuMethod> list;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getSumary() {
        return sumary;
    }

    public void setSumary(String summary) {
        this.sumary = summary;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIngredients2() {
        return ingredients2;
    }

    public void setIngredients2(String ingredients2) {
        this.ingredients2 = ingredients2;
    }

    public List<CookMenuMethod> getList() {
        return list;
    }

    public void setList(List<CookMenuMethod> list) {
        this.list = list;
    }
}