package com.viomi.fridge.vertical.home.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;

/**
 * 思必驰天气 Json
 * Created by William on 2018/8/15.
 */
public class AirQualityDetailEntity implements Serializable {
    @JSONField(name = "aqi")
    private String aqi;
    @JSONField(name = "qlty")
    private String qlty;
    @JSONField(name = "pm25")
    private String pm25;
    @JSONField(name = "pm10")
    private String pm10;
    @JSONField(name = "no2")
    private String no2;
    @JSONField(name = "so2")
    private String so2;
    @JSONField(name = "co")
    private String co;
    @JSONField(name = "o3")
    private String o3;

    public String getAqi() {
        return aqi;
    }

    public void setAqi(String aqi) {
        this.aqi = aqi;
    }

    public String getQlty() {
        return qlty;
    }

    public void setQlty(String qlty) {
        this.qlty = qlty;
    }

    public String getPm25() {
        return pm25;
    }

    public void setPm25(String pm25) {
        this.pm25 = pm25;
    }

    public String getPm10() {
        return pm10;
    }

    public void setPm10(String pm10) {
        this.pm10 = pm10;
    }

    public String getNo2() {
        return no2;
    }

    public void setNo2(String no2) {
        this.no2 = no2;
    }

    public String getSo2() {
        return so2;
    }

    public void setSo2(String so2) {
        this.so2 = so2;
    }

    public String getCo() {
        return co;
    }

    public void setCo(String co) {
        this.co = co;
    }

    public String getO3() {
        return o3;
    }

    public void setO3(String o3) {
        this.o3 = o3;
    }
}
