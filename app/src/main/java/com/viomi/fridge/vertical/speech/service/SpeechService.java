package com.viomi.fridge.vertical.speech.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.RecyclerViewLinearDivider;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.FileUtil;
import com.viomi.fridge.vertical.common.util.FrameAnimation;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.speech.model.entity.TalkInfoEntity;
import com.viomi.fridge.vertical.speech.model.preference.SpeechPreference;
import com.viomi.fridge.vertical.speech.model.repository.SkillRepository;
import com.viomi.fridge.vertical.speech.util.WakeAndLock;
import com.viomi.fridge.vertical.speech.view.adapter.SpeechAdapter;
import com.viomi.widget.dialog.DownLoadDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import cn.com.viomi.ailib.api.VioAI;
import cn.com.viomi.ailib.api.VioAIListener;
import cn.com.viomi.ailib.api.VioAIUpdateListener;
import cn.com.viomi.ailib.api.VioAIUpdater;
import cn.com.viomi.ailib.api.aispeechConfig;
import rx.Subscription;


/**
 * 语音服务 Service
 * Created by William on 2018/3/7.
 */
public class SpeechService extends Service {
    private static final String TAG = SpeechService.class.getSimpleName();
    private static final int MSG_SHOW_DIALOG = 1;// 显示 Dialog
    private static final int MSG_HIDE_DIALOG = 2;// 隐藏 Dialog
    private static final int MSG_CHAT_UPDATE = 3;// 对话更新
    private static final int MSG_SHOW_DOWNLOAD = 4;// 更新对话框显示
    private static final int MSG_HIDE_DOWNLOAD = 5;// 更新对话框隐藏
    private static final int MSG_UPDATE_DOWNLOAD = 6;// 更新对话框进度
    private static final int MSG_RETRY_AUTH = 7;// 授权重试
    private Subscription mSubscription;
    private TalkInfoEntity mTalkInfoEntity;
    private SpeechHandler mHandler;
    private WindowManager mWindowManager;
    private FrameAnimation mFrameAnimation;
    private RecyclerView mRecyclerView;// 对话列表
    private boolean mIsSuccess = false, mIsShowing = false, mIsSpeaking = false;// 授权是否成功，是否正在显示，是否正在说
    private View mView;// 对话框布局
    private SpeechAdapter mAdapter;// 语音对话适配器
    private List<TalkInfoEntity> mList;// 对话信息集合
    private DownLoadDialog mDownLoadDialog;// 下载对话框

    @Override
    public void onCreate() {
        super.onCreate();
        mHandler = new SpeechHandler(this);
        speechInit();// 语音初始化

        mSubscription = RxBus.getInstance().subscribe(busEvent -> {
            switch (busEvent.getMsgId()) {
                case BusEvent.MSG_SPEAK_CONTENT: // 语音合成
                    if (mIsSpeaking) VioAI.stopSpeak();
                    String content = (String) busEvent.getMsgObject();
                    VioAI.getInstance().speak(content);
                    break;
                case BusEvent.MSG_WAKE_UP_MANUAL: // 手动唤醒
                    if (!mIsSuccess) return;
                    String message = (String) busEvent.getMsgObject();
                    mTalkInfoEntity = new TalkInfoEntity();
                    mTalkInfoEntity.msg = message;
                    mTalkInfoEntity.talkType = TalkInfoEntity.TYPE_TALK;
                    if (mHandler != null) {
                        mIsShowing = true;
                        Message.obtain(mHandler, MSG_SHOW_DIALOG, mTalkInfoEntity).sendToTarget();
                    }
                    break;
            }
        });
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        VioAI.getInstance().release();
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
        if (mDownLoadDialog != null && mDownLoadDialog.isShowing()) mDownLoadDialog.dismiss();
        if (mIsShowing) hideChatDialog();
    }

    private void speechInit() {
        VioAI.setPlatform("aispeech");
        VioAI.addConfig(aispeechConfig.K_API_KEY, AppConstants.SPEECH_KEY);
        VioAI.addConfig(aispeechConfig.K_PRODUCT_ID, AppConstants.SPEECH_PRODUCT_ID);
        VioAI.addConfig(aispeechConfig.K_USER_ID, AppConstants.SPEECH_USER_ID);
        VioAI.addConfig(aispeechConfig.K_ALIAS_KEY, "test");// 产品分支
        File path = new File(Environment.getExternalStorageDirectory(), AppConstants.PATH);
        if (!path.exists()) logUtil.d(TAG, "create " + path.mkdirs());
        String filePath = Environment.getExternalStorageDirectory() + "/" + AppConstants.PATH + "speech.bin";
        try {
            FileUtil.copyFromAssets(filePath, "speech/speech.bin");
        } catch (IOException e) {
            logUtil.e(TAG, "copyFromAssets error!msg=" + e.getMessage());
            e.printStackTrace();
        }
        boolean result = FileUtil.isFileExist(filePath);
        logUtil.d(TAG, "wakeup file exist=" + result);
        if (result) {
            VioAI.addConfig(aispeechConfig.K_WAKEUP_BIN, filePath);
        }
        VioAI.init(this, vioAIListener);
    }

    /**
     * 显示语音对话框
     */
    private void showChatDialog(TalkInfoEntity talkInfoEntity) {
        if (mWindowManager == null)
            mWindowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater == null) return;
        mView = inflater.inflate(R.layout.dialog_speech, null);
        mRecyclerView = mView.findViewById(R.id.speech_chat_list);
        ImageView imageView = mView.findViewById(R.id.speech_chat_microphone);
        mView.findViewById(R.id.speech_layout).setOnClickListener(v -> {
            if (mHandler != null) Message.obtain(mHandler, MSG_HIDE_DIALOG).sendToTarget();
            VioAI.getInstance().stopDialog();// 结束对话
        });
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new RecyclerViewLinearDivider(this, LinearLayoutManager.VERTICAL, 60,
                getResources().getColor(android.R.color.transparent)));
        mList = new ArrayList<>();
        mList.add(talkInfoEntity);
        mAdapter = new SpeechAdapter(mList);
        mRecyclerView.setAdapter(mAdapter);
        // 添加 View
        if (mWindowManager != null) {
            mWindowManager.addView(mView, getWindowLayoutParams(Gravity.CENTER, WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT));
        }
        mFrameAnimation = new FrameAnimation(imageView, ToolUtil.getRes(FridgeApplication.getContext(), R.array.microphone_anim),
                40, true);
        SpeechPreference.getInstance().saveDialogStatus(true);
    }

    /**
     * 隐藏语音对话框
     */
    private void hideChatDialog() {
        if (mWindowManager != null && mView != null) mWindowManager.removeView(mView);
        if (mFrameAnimation != null) mFrameAnimation.release();
        if (mList != null) {
            mList.clear();
            if (mAdapter != null) {
                mAdapter.notifyDataSetChanged();
                mAdapter = null;
            }
            mList = null;
        }
        mView = null;
        SpeechPreference.getInstance().saveDialogStatus(false);
    }

    private WindowManager.LayoutParams getWindowLayoutParams(int gravity, int width, int height) {
        WindowManager.LayoutParams wmParams = new WindowManager.LayoutParams();
        wmParams.format = PixelFormat.TRANSPARENT;
        wmParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM;
        wmParams.x = 0;
        wmParams.y = 0;
        wmParams.gravity = gravity;
        wmParams.width = width;
        wmParams.height = height;
        wmParams.type = WindowManager.LayoutParams.TYPE_PHONE;
        return wmParams;
    }

    private VioAIListener vioAIListener = new VioAIListener() {
        @Override
        public void isAuthSuccess() {
            logUtil.d(TAG, "VioAIListener isAuthSuccess");
            mIsSuccess = true;
            VioAIUpdater.getInstance().checkResourceUpdate(vioAIUpdateListener);
        }

        @Override
        public void onAIInitComplete(boolean b) {
            logUtil.d(TAG, "VioAIListener onAIInitComplete：" + b);
            SpeechPreference.getInstance().saveAuthStatus(b);
            mIsSuccess = b;
            if (!b && mHandler != null) {
                VioAI.getInstance().release();
                mHandler.removeMessages(MSG_RETRY_AUTH);
                mHandler.sendEmptyMessageDelayed(MSG_RETRY_AUTH, 5000);
            }
            // 根据缓存设置是否允许唤醒
            if (SpeechPreference.getInstance().getSwitch()) VioAI.enableWakeup();
            else VioAI.disableWakeup();
            if (mHandler != null) Message.obtain(mHandler, MSG_HIDE_DOWNLOAD).sendToTarget();
        }

        @Override
        public void onAIInitError(int i, String s) {
            logUtil.e(TAG, "VioAIListener onAIInitError，" + "code：" + i + "，info：" + s);
            if (mHandler != null) {
                VioAI.getInstance().release();
                mHandler.removeMessages(MSG_RETRY_AUTH);
                mHandler.sendEmptyMessageDelayed(MSG_RETRY_AUTH, 5000);
            }
        }

        @Override
        public void onInitWakeupEngine(int i) {
            logUtil.d(TAG, "VioAIListener onInitWakeupEngine：" + i);
        }

        @Override
        public void onInitAsrEngine(int i) {
            logUtil.d(TAG, "VioAIListener onInitAsrEngine：" + i);
        }

        @Override
        public void onInitTTSEngine(int i) {
            logUtil.d(TAG, "VioAIListener onInitTTSEngine：" + i);
        }

        @Override
        public void onTTSBegining(String s) {
            logUtil.d(TAG, "VioAIListener onTTSBegining：" + s);
            mIsSpeaking = true;
            RxBus.getInstance().post(BusEvent.MSG_START_SPEAK);
        }

        @Override
        public void onTTSReceived(byte[] bytes) {

        }

        @Override
        public void onTTSEnd(String s, int i) {
            logUtil.d(TAG, "VioAIListener onTTSEnd：" + "，code：" + i + "，info：" + s);
            mIsSpeaking = false;
            RxBus.getInstance().post(BusEvent.MSG_STOP_SPEAK);
        }

        @Override
        public void onTTSError(String s) {
            logUtil.d(TAG, "VioAIListener onTTSError：" + s);
            mIsSpeaking = false;
            RxBus.getInstance().post(BusEvent.MSG_STOP_SPEAK);
        }

        @Override
        public void onWakeUp(String s) {
            logUtil.d(TAG, "VioAIListener onWakeUp：" + s);
            WakeAndLock wakeAndLock = new WakeAndLock();
            wakeAndLock.screenOn();
            Intent intent = new Intent();
            intent.setAction("action.voice.onWakeUp");
            intent.putExtra("package", "com.viomi.fridge.vertical");
            sendBroadcast(intent);
            String message = null;
            try {
                JSONObject jsonObject = new JSONObject(s);
                message = jsonObject.optString("greeting");
            } catch (JSONException e) {
                e.printStackTrace();
                logUtil.e(TAG, e.toString());
            }
            if (message == null) return;
            mTalkInfoEntity = new TalkInfoEntity();
            mTalkInfoEntity.msg = message;
            mTalkInfoEntity.talkType = TalkInfoEntity.TYPE_TALK;
        }

        @Override
        public void onAsrResult(int type, String result) {
            logUtil.e(TAG, "VioAIListener onAsrResult：type=" + type + "，result=" + result);
            if (result.equals(getResources().getString(R.string.app_name)) || !mIsShowing
                    || mHandler == null) return;
            if (result.length() > 0 && type == 1) {
                TalkInfoEntity talkInfoEntity = new TalkInfoEntity();
                talkInfoEntity.msg = result;
                talkInfoEntity.talkType = TalkInfoEntity.TYPE_LISTEN;
                Message.obtain(mHandler, MSG_CHAT_UPDATE, talkInfoEntity).sendToTarget();
            }
        }

        @Override
        public void onAsrSpeechOut(String s) {
            logUtil.e(TAG, "VioAIListener onAsrSpeechOut：" + s);
            if (!mIsShowing || mHandler == null || s.equals("")) return;
            if (s.length() > 0) { // 语音输入
                TalkInfoEntity talkInfoEntity = new TalkInfoEntity();
                talkInfoEntity.msg = s;
                talkInfoEntity.talkType = TalkInfoEntity.TYPE_TALK;
                Message.obtain(mHandler, MSG_CHAT_UPDATE, talkInfoEntity).sendToTarget();
            }
        }

        @Override
        public void onEndOfSpeech() {
            logUtil.e(TAG, "VioAIListener onEndOfSpeech");
        }

        @Override
        public void onNluSuccess(JSONObject jsonObject) {
            logUtil.d(TAG, "VioAIListener onNluSuccess：" + jsonObject.toString());
        }

        @Override
        public void onNluSuccess(String s) {
            logUtil.d(TAG, "VioAIListener onNluSuccess：" + s);
            SkillRepository.getInstance().handleNlp(SpeechService.this, s);
            SkillRepository.getInstance().handleNlu(SpeechService.this, s);
        }

        @Override
        public void onNluSuccess(String s, String s1) {
            logUtil.e(TAG, "VioAIListener onNluSuccess：" + "，message=" + s + "，data=" + s1);
            switch (s) {
                case "next.step": // 下一步
                    RxBus.getInstance().post(BusEvent.MSG_NEXT_STEP);
                    break;
                case "last.step": // 上一步
                    RxBus.getInstance().post(BusEvent.MSG_LAST_STEP);
                    break;
            }
        }

        @Override
        public void onNluError() {
            logUtil.e(TAG, "VioAIListener onNluError");
        }

        @Override
        public void onEventError(int i, String s) {

        }

        @Override
        public void onEventState(int i) {

        }

        @Override
        public void onStartDialog(String s) {
            logUtil.d(TAG, "VioAIListener onStartDialog：" + s);
            if (mHandler == null || mTalkInfoEntity == null) return;
            mIsShowing = true;
            Message.obtain(mHandler, MSG_SHOW_DIALOG, mTalkInfoEntity).sendToTarget();
        }

        @Override
        public void onEndDialog(String s) {
            logUtil.d(TAG, "VioAIListener onEndDialog：" + s);
            if (mHandler == null) return;
            mIsShowing = false;
            Message.obtain(mHandler, MSG_HIDE_DIALOG).sendToTarget();
        }

        @Override
        public void onAuthSuccess() {
            logUtil.d(TAG, "VioAIListener onAuthSuccess");
            VioAIUpdater.getInstance().checkResourceUpdate(vioAIUpdateListener);
        }

        @Override
        public void onStartListening() {

        }

        @Override
        public void onStopListening() {

        }

        @Override
        public void onAuthFailed(String s, String s1) {
            logUtil.e(TAG, "VioAIListener onAuthFailed: " + s + ", error:" + s1);
            if (mHandler != null) {
                VioAI.getInstance().release();
                mHandler.removeMessages(MSG_RETRY_AUTH);
                mHandler.sendEmptyMessageDelayed(MSG_RETRY_AUTH, 5000);
            }
        }
    };

    private VioAIUpdateListener vioAIUpdateListener = new VioAIUpdateListener() {
        @Override
        public void onUpdateFound(String s) {
            logUtil.d(TAG, "VioAIUpdateListener onUpdateFound：" + s);
            if (mHandler != null) Message.obtain(mHandler, MSG_SHOW_DOWNLOAD).sendToTarget();
        }

        @Override
        public void onUpdateFinish() {
            logUtil.d(TAG, "VioAIUpdateListener onUpdateFinish：");
            if (mHandler != null) Message.obtain(mHandler, MSG_HIDE_DOWNLOAD).sendToTarget();
        }

        @Override
        public void onDownloadProgress(float v) {
            logUtil.d(TAG, "VioAIUpdateListener onDownloadProgress：" + v);
            if (mHandler != null) {
                Message.obtain(mHandler, MSG_UPDATE_DOWNLOAD, v).sendToTarget();
            }
        }

        @Override
        public void onError(int i, String s) {
            logUtil.e(TAG, "VioAIUpdateListener onError，" + "code：" + i + "，info：" + s);
            if (mHandler != null) Message.obtain(mHandler, MSG_HIDE_DOWNLOAD).sendToTarget();
        }

        @Override
        public void onUpgrade(String s) {
            logUtil.d(TAG, "VioAIUpdateListener onUpgrade：" + s);
        }
    };

    private static class SpeechHandler extends Handler {
        WeakReference<SpeechService> weakReference;

        SpeechHandler(SpeechService service) {
            this.weakReference = new WeakReference<>(service);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (this.weakReference != null) {
                SpeechService service = this.weakReference.get();
                if (service != null) {
                    TalkInfoEntity talkInfoEntity;
                    switch (msg.what) {
                        case MSG_SHOW_DIALOG: // 显示 Dialog
                            talkInfoEntity = (TalkInfoEntity) msg.obj;
                            service.showChatDialog(talkInfoEntity);
                            break;
                        case MSG_HIDE_DIALOG: // 隐藏 Dialog
                            service.hideChatDialog();
                            break;
                        case MSG_CHAT_UPDATE: // 对话更新
                            talkInfoEntity = (TalkInfoEntity) msg.obj;
                            if (service.mList == null || service.mAdapter == null) return;
                            service.mList.add(talkInfoEntity);
                            service.mAdapter.notifyItemInserted(service.mList.size() - 1);
                            service.mAdapter.notifyItemRangeChanged(service.mList.size() - 1, 1);
                            service.mRecyclerView.scrollToPosition(service.mAdapter.getItemCount() - 1);
                            break;
                        case MSG_SHOW_DOWNLOAD: // 更新对话框显示
                            if (service.mDownLoadDialog != null && service.mDownLoadDialog.isShowing())
                                service.mDownLoadDialog.dismiss();
                            service.mDownLoadDialog = new DownLoadDialog(service.getApplicationContext(), service.getApplicationContext().getResources().getString(R.string.speech_update), null,
                                    service.getApplicationContext().getResources().getString(R.string.speech_update_finish));
                            if (service.mDownLoadDialog.getWindow() != null) {
                                service.mDownLoadDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                                service.mDownLoadDialog.show();
                            }
                            break;
                        case MSG_HIDE_DOWNLOAD: // 更新对话框隐藏
                            if (service.mDownLoadDialog != null && service.mDownLoadDialog.isShowing())
                                service.mDownLoadDialog.dismiss();
                            break;
                        case MSG_UPDATE_DOWNLOAD: // 更新对话框进度
                            float progress = (float) msg.obj;
                            if (service.mDownLoadDialog != null && service.mDownLoadDialog.isShowing())
                                service.mDownLoadDialog.setProgress((int) progress);
                            break;
                        case MSG_RETRY_AUTH: // 授权重试
                            service.speechInit();
                            break;
                    }
                }
            }
        }
    }
}
