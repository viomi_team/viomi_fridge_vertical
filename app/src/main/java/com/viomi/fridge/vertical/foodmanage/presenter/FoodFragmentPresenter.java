package com.viomi.fridge.vertical.foodmanage.presenter;

import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.ToastUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.foodmanage.contract.FoodFragmentContract;
import com.viomi.fridge.vertical.foodmanage.model.FoodManageRepository;
import com.viomi.fridge.vertical.foodmanage.model.entity.FoodDetail;

import java.util.List;

import javax.annotation.Nullable;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * 食材管理列表 Presenter
 * Created by William on 2018/2/28.
 */
public class FoodFragmentPresenter implements FoodFragmentContract.Presenter {
    private static final String TAG = FoodFragmentPresenter.class.getSimpleName();
    private CompositeSubscription mCompositeSubscription;
    @Nullable
    private FoodFragmentContract.View mView;

    public FoodFragmentPresenter() {

    }

    @Override
    public void subscribe(FoodFragmentContract.View view) {
        mView = view;
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public void unSubscribe() {
        mView = null;
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
    }

    @Override
    public void loadFood(List<FoodDetail> list, List<FoodDetail> list1, int type) {
        Subscription subscription = Observable.just(list)
                .subscribeOn(Schedulers.computation())
                .observeOn(Schedulers.computation())
                .onTerminateDetach()
                .map(list2 -> {
                    list1.clear();
                    if (mView != null) mView.notifyDataChanged();
                    for (FoodDetail foodDetail : list2) {
                        if (foodDetail.getRoomType().equals(String.valueOf(type)))
                            list1.add(foodDetail);
                    }
                    return list1;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(list3 -> {
                    if (mView != null) {
                        mView.notifyDataChanged();
                        mView.refreshList(list3);
                    }
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void deleteFood(List<FoodDetail> list) {
        Subscription subscription = Observable.just(list)
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .flatMap(list1 -> FoodManageRepository.getInstance().delete(list))
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(aBoolean -> {
                    if (aBoolean) {
                        if (mView != null) mView.notifyDataChanged();
                        RxBus.getInstance().post(BusEvent.MSG_START_FOOD_UPDATE);
                    } else
                        ToastUtil.showCenter(FridgeApplication.getContext(), FridgeApplication.getContext().getResources().getString(R.string.food_management_delete_fail_tip));
                }, throwable -> {
                    logUtil.e(TAG, throwable.getMessage());
                    ToastUtil.showCenter(FridgeApplication.getContext(), FridgeApplication.getContext().getResources().getString(R.string.food_management_delete_fail_tip));
                });
        mCompositeSubscription.add(subscription);
    }
}
