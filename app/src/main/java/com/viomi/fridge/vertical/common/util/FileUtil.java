package com.viomi.fridge.vertical.common.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageDataObserver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.os.RemoteException;
import android.provider.MediaStore;

import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.administration.model.entity.FileInfo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * 文件管理工具
 * Created by William on 2018/1/5.
 */
public class FileUtil {
    private static final String TAG = FileUtil.class.getSimpleName();
    private static final String SELECTION_INFO = MediaStore.Files.FileColumns.DATA + " NOT LIKE '%/storage/emulated/0/viomi/%'";// 过滤本应用的查找

    /**
     * 删除对象 f 下的所有文件和文件夹（含自身）
     *
     * @param f 文件路径
     */
    public static void deleteAll(File f) {
        if (!f.exists()) {
            logUtil.d(TAG, " the file is not exists");
            return;
        }
        // 文件
        if (f.isFile()) {
            if (f.delete())
                logUtil.d(TAG, "delete success");
        } else { // 文件夹
            // 获得当前文件夹下的所有子文件和子文件夹
            File f1[] = f.listFiles();
            // 循环处理每个对象
            int len = f1.length;
            for (int i = 0; i < len; i++) {
                // 递归调用，处理每个文件对象
                deleteAll(f1[i]);
            }
            // 删除当前文件夹
            if (f.delete())
                logUtil.d(TAG, "delete success");
        }
    }

    /**
     * f 为文件则删除自身，f 为目录则删除目录下的子文件
     *
     * @param f 文件路径
     */
    public static void deleteChildFile(File f) {
        if (!f.exists()) {
            return;
        }
        // 文件
        if (f.isFile()) {
            f.delete();
        } else { // 文件夹
            // 获得当前文件夹下的所有子文件和子文件夹
            File f1[] = f.listFiles();
            // 循环处理每个对象
            int len = f1.length;
            for (int i = 0; i < len; i++) {
                // 递归调用，处理每个文件对象
                deleteAll(f1[i]);
            }
        }
    }

    /**
     * 从本地（手机内部存储）读取保存的对象
     *
     * @param filename 文件名称
     */
    public static Object getObject(Context context, String filename) {
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = context.openFileInput(filename);
            ois = new ObjectInputStream(fis);
            return ois.readObject();
        } catch (Exception e) {
            logUtil.e(TAG, "getObject error,msg=" + e.getMessage());
            //e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (ois != null) {
                try {
                    ois.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    /**
     * 单位 bit
     * 支持音频、视频、图片
     */
    public static long queryFileSize(Context mContext, FileType type) {
        Uri uri;
        if (type == FileType.FILE_VIDEO)
            uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        else if (type == FileType.FILE_PIC)
            uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        else if (type == FileType.FILE_MUSIC)
            uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        else
            return -1;
        Cursor c = mContext.getContentResolver().query(uri,// 数据资源路径
                new String[]{"SUM(_size)"},// 查询的列  筛选、排序时用
                SELECTION_INFO,//null,// 查询的条件
                null/*条件填充值*/,
                null/*排序依据*/);
        if (c != null && c.moveToFirst()) {
            long size = c.getLong(0);
            logUtil.w(TAG, "the size is:" + size);
            c.close();
            return size;
        }
        return -1;
    }

    public static List<FileInfo> getFileInfoList(Context context, FileType type) {
        List<FileInfo> list = new ArrayList<>();
        Cursor cursor = null;
        if (type == FileType.FILE_MUSIC) {
            cursor = context.getContentResolver().query(
                    MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    new String[]{MediaStore.Files.FileColumns._ID, MediaStore.Files.FileColumns.DATA,
                            MediaStore.Files.FileColumns.SIZE, MediaStore.Files.FileColumns.DISPLAY_NAME,
                            MediaStore.Files.FileColumns.TITLE, MediaStore.Audio.Media.DURATION, MediaStore.Audio.Media.ALBUM_ID}, null,
                    null, null);
        } else if (type == FileType.FILE_VIDEO) {
            cursor = context.getContentResolver().query(
                    MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                    new String[]{MediaStore.Files.FileColumns._ID, MediaStore.Files.FileColumns.DATA,
                            MediaStore.Files.FileColumns.SIZE, MediaStore.Files.FileColumns.DISPLAY_NAME,
                            MediaStore.Files.FileColumns.TITLE, MediaStore.Video.Media.DURATION}, null,
                    null, null);
        } else if (type == FileType.FILE_PIC) {
            cursor = context.getContentResolver().query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    new String[]{MediaStore.Files.FileColumns._ID, MediaStore.Files.FileColumns.DATA,
                            MediaStore.Files.FileColumns.SIZE, MediaStore.Files.FileColumns.DISPLAY_NAME,
                            MediaStore.Files.FileColumns.TITLE}, SELECTION_INFO,//null,
                    null, null);
        } else if (type == FileType.FILE_APK) {
            String selection = MediaStore.Files.FileColumns.DATA + " LIKE '%.apk'";
            cursor = context.getContentResolver().query(
                    MediaStore.Files.getContentUri("external"),
                    new String[]{MediaStore.Files.FileColumns._ID, MediaStore.Files.FileColumns.DATA,
                            MediaStore.Files.FileColumns.SIZE, MediaStore.Files.FileColumns.DISPLAY_NAME,
                            MediaStore.Files.FileColumns.TITLE}, selection,
                    null, null);
        } else if (type == FileType.FILE_GARBAGE) {
            String selection = MediaStore.Files.FileColumns.DATA + " LIKE '%.tmp'"
                    + " OR " + MediaStore.Files.FileColumns.DATA + " LIKE '%.temp'"
                    + " OR " + MediaStore.Files.FileColumns.DATA + " LIKE '%.apk'"
                    + " OR " + MediaStore.Files.FileColumns.DATA + " LIKE '%.logUtil'";
            cursor = context.getContentResolver().query(
                    MediaStore.Files.getContentUri("external"),
                    new String[]{MediaStore.Files.FileColumns._ID, MediaStore.Files.FileColumns.DATA,
                            MediaStore.Files.FileColumns.SIZE, MediaStore.Files.FileColumns.DISPLAY_NAME,
                            MediaStore.Files.FileColumns.TITLE}, selection,
                    null, null);
        }
        while (cursor != null && cursor.moveToNext()) {
            FileInfo info = new FileInfo(type, cursor.getInt(cursor.getColumnIndex(MediaStore.Files.FileColumns._ID)),
                    cursor.getString(cursor.getColumnIndex(MediaStore.Files.FileColumns.TITLE)),
                    cursor.getString(cursor.getColumnIndex(MediaStore.Files.FileColumns.DATA)),
                    cursor.getLong(cursor.getColumnIndex(MediaStore.Files.FileColumns.SIZE)));
            if (type == FileType.FILE_VIDEO || type == FileType.FILE_MUSIC) {
                info.setTimeLong(cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION)));
            }
            if (type == FileType.FILE_MUSIC)
                info.setAlbum_id(cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID)));
            logUtil.d(TAG, " the item is:" + info.getAbsolutePath());
            list.add(info);
        }
        if (cursor != null)
            cursor.close();
        return list;
    }

    // 清理缓存
    public static boolean cleanAppsCache(Context mContext) {
        //
        File externalDir = mContext.getExternalCacheDir();
        if (externalDir == null) {
            return true;
        }

        PackageManager pm = mContext.getPackageManager();
        List<ApplicationInfo> installedPackages = pm.getInstalledApplications(PackageManager.GET_GIDS);
        for (ApplicationInfo info : installedPackages) {
            if (info.packageName.equals("com.viomi.fridge.vertical"))
                continue;
            //if (packageNameList.contains(info.packageName)) {
            String externalCacheDir = externalDir.getAbsolutePath()
                    .replace(mContext.getPackageName(), info.packageName);
            File externalCache = new File(externalCacheDir);
            if (externalCache.exists() && externalCache.isDirectory()) {
                FileUtil.deleteAll(new File(externalCacheDir));
                //FileUtil.deleteTarget(externalCacheDir);
            }
            //}
        }
        //
        final boolean[] isSuccess = {false};
        try {
            Method freeStorageAndNotify = pm.getClass()
                    .getMethod("freeStorageAndNotify", long.class, IPackageDataObserver.class);
            long freeStorageSize = Long.MAX_VALUE;

            freeStorageAndNotify.invoke(pm, freeStorageSize, new IPackageDataObserver.Stub() {

                @Override
                public void onRemoveCompleted(String packageName, boolean succeeded) throws RemoteException {
                    isSuccess[0] = succeeded;
                }
            });

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return isSuccess[0];
    }

    public enum FileType { // 文件类型
        FILE_MUSIC(0),//
        FILE_PIC(1),//
        FILE_VIDEO(2),//
        FILE_APK(3),
        FILE_DOC(4),
        FILE_ZIP(5),
        FILE_GARBAGE(6),// 清理缓存垃圾时用,清理 log/temp/apk 文件
        FILE_OTHER(7);//

        public int value;

        FileType(int value) {
            this.value = value;
        }
    }

    // 创建一个临时目录，用于复制临时文件，如 assets 目录下的离线资源文件
    public static String createTmpDir(Context context) {
        String sampleDir = "baiduTTS";
        String tmpDir = Environment.getExternalStorageDirectory().toString() + "/" + sampleDir;
        if (!FileUtil.makeDir(tmpDir)) {
            tmpDir = context.getExternalFilesDir(sampleDir).getAbsolutePath();
            if (!FileUtil.makeDir(sampleDir)) {
                throw new RuntimeException("create model resources dir failed :" + tmpDir);
            }
        }
        return tmpDir;
    }

    public static boolean makeDir(String dirPath) {
        File file = new File(dirPath);
        return file.exists() || file.mkdirs();
    }

    public static void copyFromAssets(String sdPath, String assetPath) throws IOException {
        File outFile = new File(sdPath);
        InputStream is;
        is = FridgeApplication.getContext().getAssets().open(assetPath);
        FileOutputStream fos = new FileOutputStream(outFile);
        byte[] buffer = new byte[1024];
        int byteCount;
        while ((byteCount = is.read(buffer)) != -1) {
            fos.write(buffer, 0, byteCount);
        }
        fos.flush();
        is.close();
        fos.getFD().sync();// 掉电保护
        fos.close();
    }

    public static boolean isFileExist(String filePath) {
        File file = new File(filePath);
        return file.exists();
    }

    /**
     * 生成文件
     */
    public static File makeFilePath(String filePath, String fileName) {
        File file = null;
        makeDir(filePath);// 生成文件夹
        try {
            file = new File(filePath + fileName);
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }
}
