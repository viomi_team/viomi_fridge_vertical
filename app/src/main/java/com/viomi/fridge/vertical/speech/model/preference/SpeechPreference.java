package com.viomi.fridge.vertical.speech.model.preference;

import android.content.Context;
import android.content.SharedPreferences;

import com.viomi.fridge.vertical.FridgeApplication;

/**
 * 语音助手相关缓存
 * Created by William on 2018/1/31.
 */
public class SpeechPreference {
    private static SpeechPreference mInstance;
    private static final String name = "speech";
    private static final String SPEECH_SWITCH = "switch";// 语音对话框
    private static final String KFC_FIRST = "kfc_first";// 是否首次进入 KFC
    private static final String DIALOG_STATUS = "dialog_status";// 语音对话框状态
    private static final String AUTH_SUCCESS = "auth_success";// 首次出厂需要先下载资源包，未下载完成不可调用语音方法

    public static SpeechPreference getInstance() {
        if (mInstance == null) {
            synchronized (SpeechPreference.class) {
                if (mInstance == null) {
                    mInstance = new SpeechPreference();
                }
            }
        }
        return mInstance;
    }

    private SharedPreferences getSharedPreferences() {
        return FridgeApplication.getInstance().getSharedPreferences(
                name, Context.MODE_PRIVATE);
    }

    /**
     * 缓存语音开关
     */
    public void saveSwitch(boolean enable) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(SPEECH_SWITCH, enable);
        editor.apply();
    }

    /**
     * 获取语音开关
     */
    public boolean getSwitch() {
        return getSharedPreferences().getBoolean(SPEECH_SWITCH, true);
    }

    /**
     * 缓存是否第一次语音进入KFC
     */
    public void saveKFCFirst(boolean enable) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(KFC_FIRST, enable);
        editor.apply();
    }

    /**
     * 获取是否第一次语音进入KFC
     */
    public boolean isKFCFirst() {
        return getSharedPreferences().getBoolean(KFC_FIRST, true);
    }

    /**
     * 缓存语音对话框状态
     */
    public void saveDialogStatus(boolean isShown) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(DIALOG_STATUS, isShown);
        editor.apply();
    }

    /**
     * 获取语音对话框状态
     */
    public boolean isDialogShown() {
        return getSharedPreferences().getBoolean(DIALOG_STATUS, false);
    }

    /**
     * 缓存资源包下载状态
     */
    public void saveAuthStatus(boolean isAuth) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(AUTH_SUCCESS, isAuth);
        editor.apply();
    }

    /**
     * 获取资源包下载状态
     */

    public boolean isAuth() {
        return getSharedPreferences().getBoolean(AUTH_SUCCESS, false);
    }
}
