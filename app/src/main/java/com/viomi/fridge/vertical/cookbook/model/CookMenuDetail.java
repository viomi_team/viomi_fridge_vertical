package com.viomi.fridge.vertical.cookbook.model;

import java.io.Serializable;
import java.util.List;

/**
 * 菜谱详情
 * Created by nanquan on 2018/2/12.
 */
public class CookMenuDetail implements Serializable{
    private String ctgTitles;
    private String menuId;
    private String name;
    private CookMenuRecipe recipe;
    private String thumbnail;
    private List<String> ctgIds;

    public String getCtgTitles() {
        return ctgTitles;
    }

    public void setCtgTitles(String ctgTitles) {
        this.ctgTitles = ctgTitles;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CookMenuRecipe getRecipe() {
        return recipe;
    }

    public void setRecipe(CookMenuRecipe recipe) {
        this.recipe = recipe;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public List<String> getCtgIds() {
        return ctgIds;
    }

    public void setCtgIds(List<String> ctgIds) {
        this.ctgIds = ctgIds;
    }
}