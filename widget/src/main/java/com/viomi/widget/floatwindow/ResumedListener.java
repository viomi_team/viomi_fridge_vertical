package com.viomi.widget.floatwindow;

/**
 * Created by yhao on 2017/12/30.
 * https://github.com/yhaolpz
 */

interface ResumedListener {
    void onResumed();
}
