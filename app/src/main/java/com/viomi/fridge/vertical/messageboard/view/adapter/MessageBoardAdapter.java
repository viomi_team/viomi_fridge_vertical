package com.viomi.fridge.vertical.messageboard.view.adapter;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseRecyclerViewAdapter;
import com.viomi.fridge.vertical.messageboard.model.entity.MessageBoardEntity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 留言板适配器
 * Created by William on 2018/4/11.
 */
public class MessageBoardAdapter extends BaseRecyclerViewAdapter<MessageBoardAdapter.BoardHolder> {
    private List<MessageBoardEntity> mList;
    private boolean mIsEdit = false;// 是否编辑

    public MessageBoardAdapter(List<MessageBoardEntity> list) {
        mList = list;
        if (mList == null) mList = new ArrayList<>();
    }

    @Override
    public BoardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_holder_message_board, parent, false);
        return new BoardHolder(view, this);
    }

    @Override
    public void onBindViewHolder(BoardHolder holder, int position) {
        MessageBoardEntity entity = mList.get(position);
        if (mIsEdit) { // 编辑模式
            holder.imageView.setVisibility(View.VISIBLE);
            if (entity.isSelected())
                holder.imageView.setImageResource(R.drawable.icon_message_board_chosed);
            else holder.imageView.setImageResource(R.drawable.icon_message_board_no_chose);
        } else {
            holder.imageView.setVisibility(View.GONE);
        }
        // 图片
        ImageRequest request = ImageRequestBuilder
                .newBuilderWithSource(Uri.fromFile(entity.getFile()))
                .setResizeOptions(new ResizeOptions((int) FridgeApplication.getContext().getResources().getDimension(R.dimen.px_x_320), (int) FridgeApplication.getContext().getResources().getDimension(R.dimen.px_y_538)))
                .build();
        PipelineDraweeController controller = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                .setOldController(holder.simpleDraweeView.getController())
                .setImageRequest(request)
                .build();
        holder.simpleDraweeView.setController(controller);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setEdit(boolean isEdit) {
        mIsEdit = isEdit;
    }

    public boolean isEdit() {
        return mIsEdit;
    }

    class BoardHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.holder_message_board_img)
        SimpleDraweeView simpleDraweeView;// 图片

        @BindView(R.id.holder_message_board_chose)
        ImageView imageView;// 选择

        BoardHolder(View itemView, MessageBoardAdapter adapter) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> adapter.onItemHolderClick(this, 200));
        }
    }
}