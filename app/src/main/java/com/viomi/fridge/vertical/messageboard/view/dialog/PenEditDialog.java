package com.viomi.fridge.vertical.messageboard.view.dialog;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.drawable.PaintDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.CommonDialog;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;

import butterknife.BindView;

/**
 * 画笔编辑 Dialog
 * Created by William on 2018/2/27.
 */
public class PenEditDialog extends CommonDialog {
    private static final String TAG = PenEditDialog.class.getSimpleName();
    private OnPenEditDismissListener onPenEditDismissListener;
    // 颜色条
    private static final int[] PICKER_BAR_COLORS = new int[]{Color.parseColor("#FF4343"), Color.parseColor("#FFB84A"),
            Color.parseColor("#FDF15A"), Color.parseColor("#D0FC6B"), Color.parseColor("#7CFA98"),
            Color.parseColor("#8AF8E1"), Color.parseColor("#5DCDFF"), Color.parseColor("#5385FF"),
            Color.parseColor("#B471FC"), Color.parseColor("#FF6FE7"), Color.parseColor("#FF5555")};
    private static final float[] PICKER_BAR_POSITIONS = new float[]{0f, 0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f, 0.7f, 0.8f, 0.9f, 1f};// 颜色变化
    private static final float[] SEEK_BAR_POSITIONS = new float[]{0f, 1f};// 明度变化
    private int[] mLightColors = new int[]{Color.parseColor("#000000"), Color.parseColor("#FFFFFF")};
    private int[] mSubColors = new int[]{Color.parseColor("#000000"), Color.parseColor("#FF4343")};
    private int[] mSubColors2 = new int[]{Color.parseColor("#FFFFFF"), Color.parseColor("#FF4343")};
    private int mColor = Color.parseColor("#FF4343");// 最终颜色
    private float mSubPosition = 1f;
    private int mAlpha = 255;// 透明度
    private int mSize = 6;// 画笔大小
    private int mColorProgress = 0, mLightProgress = 50, mAlphaProgress = 0, mSizeProgress = 15;

    @BindView(R.id.pen_edit_color)
    SeekBar mColorSeekBar;// 颜色选择
    @BindView(R.id.pen_edit_light)
    SeekBar mLightSeekBar;// 明度
    @BindView(R.id.pen_edit_alpha)
    SeekBar mAlphaSeekBar;// 透明度
    @BindView(R.id.pen_edit_size)
    SeekBar mSizeSeekBar;// 大小

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.dialog_pen_edit;
    }

    @Override
    protected void initWithOnCreateDialog(View view) {
        initListener();
        // 初始化颜色 SeekBar
        ShapeDrawable.ShaderFactory colorShaderFactory = new ShapeDrawable.ShaderFactory() {
            @Override
            public Shader resize(int width, int height) {
                return new LinearGradient(0, 0, width, height,
                        PICKER_BAR_COLORS, PICKER_BAR_POSITIONS, Shader.TileMode.REPEAT);
            }
        };
        PaintDrawable paintColor = new PaintDrawable();
        paintColor.setShape(new RectShape());
        paintColor.setCornerRadius(8);
        paintColor.setShaderFactory(colorShaderFactory);
        mColorSeekBar.setProgressDrawable(paintColor);
        mSubColors[1] = Color.parseColor("#FF4343");
        mSubColors2[1] = Color.parseColor("#FF4343");
        setLightBarBg();
        mColorSeekBar.setProgress(mColorProgress);// 颜色
        mLightSeekBar.setProgress(mLightProgress);// 明度
        mAlphaSeekBar.setProgress(mAlphaProgress);// 透明度
        mSizeSeekBar.setProgress(mSizeProgress);// 大小
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null && dialog.getWindow() != null) {
            dialog.getWindow().setLayout(820, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public void onDestroyView() {
        if (onPenEditDismissListener != null) {
            onPenEditDismissListener.onPenEditDismiss(mColor, mAlpha, mSize);
            logUtil.d(TAG, "onPenEditDismissListener");
        }
        super.onDestroyView();
    }

    private void initListener() {
        // 颜色选择
        mColorSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mColorProgress = progress;
                float radio = (float) progress / mColorSeekBar.getMax();
                int color = ToolUtil.getColor(radio, PICKER_BAR_COLORS, PICKER_BAR_POSITIONS, mAlpha);
                if (mLightProgress == 50) {
                    mColor = color;
                    mSubColors[1] = color;
                    mSubColors2[1] = color;
                } else if (mLightProgress < 50) {
                    mSubColors[1] = color;
                    mSubColors2[1] = color;
                    mColor = ToolUtil.getColor(mSubPosition, mSubColors, SEEK_BAR_POSITIONS, mAlpha);
                } else {
                    mSubColors[1] = color;
                    mSubColors2[1] = color;
                    mColor = ToolUtil.getColor(mSubPosition, mSubColors2, SEEK_BAR_POSITIONS, mAlpha);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        // 明度
        mLightSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mLightProgress = progress;
                float radio;
                if (progress <= 50) {
                    radio = (float) progress / (mLightSeekBar.getMax() / 2);
                    mColor = ToolUtil.getColor(radio, mSubColors, SEEK_BAR_POSITIONS, mAlpha);
                } else {
                    radio = (float) (mLightSeekBar.getMax() - progress) / (mLightSeekBar.getMax() / 2);
                    mColor = ToolUtil.getColor(radio, mSubColors2, SEEK_BAR_POSITIONS, mAlpha);
                }
                mSubPosition = radio;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        // 透明度
        mAlphaSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mAlphaProgress = progress;
                mAlpha = ((100 - progress) * 255) / 100;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        // 大小
        mSizeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mSizeProgress = progress;
                int calcProgress = progress > 1 ? progress : 1;
                mSize = Math.round((40 / 100f) * calcProgress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    /**
     * 设置明度 SeekBar 滑动条
     */
    private void setLightBarBg() {
        ShapeDrawable.ShaderFactory lightShaderFactory = new ShapeDrawable.ShaderFactory() {
            @Override
            public Shader resize(int width, int height) {
                return new LinearGradient(0, 0, width, height,
                        mLightColors, SEEK_BAR_POSITIONS, Shader.TileMode.REPEAT);
            }
        };
        PaintDrawable paintLight = new PaintDrawable();
        paintLight.setShape(new RectShape());
        paintLight.setCornerRadius(8);
        paintLight.setShaderFactory(lightShaderFactory);
        mLightSeekBar.setProgressDrawable(paintLight);
    }

//    private void setSubBarBg(final int color) {
//        Rect bounds = mLightSeekBar.getProgressDrawable().getBounds();
//        mSubColors[1] = color;
//        ShapeDrawable.ShaderFactory shaderFactory = new ShapeDrawable.ShaderFactory() {
//            @Override
//            public Shader resize(int width, int height) {
//                return new LinearGradient(0, 0, width, height,
//                        mSubColors, SEEK_BAR_POSITIONS, Shader.TileMode.REPEAT);
//            }
//        };
//
//        PaintDrawable paint = new PaintDrawable();
//        paint.setShape(new RectShape());
//        paint.setCornerRadius(12);
//        paint.setShaderFactory(shaderFactory);
//        mLightSeekBar.setProgressDrawable(paint);
//        mLightSeekBar.getProgressDrawable().setBounds(bounds);
//    }

    public interface OnPenEditDismissListener {
        void onPenEditDismiss(int color, int alpha, int size);
    }

    public void setOnPenEditDismissListener(OnPenEditDismissListener onPenEditDismissListener) {
        this.onPenEditDismissListener = onPenEditDismissListener;
    }
}