package com.viomi.fridge.vertical.administration.view.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothA2dp;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.model.entity.MyBluetoothDevice;
import com.viomi.fridge.vertical.administration.view.adapter.BluetoothListAdapter;
import com.viomi.fridge.vertical.common.base.CommonDialog;
import com.viomi.fridge.vertical.common.util.ToastUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.widget.switchbutton.SwitchButton;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import butterknife.BindView;

/**
 * 蓝牙对话框
 */
public class BluetoothSettingDialog extends CommonDialog {
    private static final String TAG = BluetoothSettingDialog.class.getSimpleName();
    private EditNameDialog mEditNameDialog;// 蓝牙名称编辑
    private LoadingDialog mLoadingDialog;// 加载对话框
    private BluetoothAdapter mBluetoothAdapter;// 蓝牙管理
    private BluetoothListAdapter mBondAdapter;// 绑定设备适配器
    private BluetoothListAdapter mScanAdapter;// 扫描设备适配器
    private List<MyBluetoothDevice> mPairDevices;// 绑定设备集合
    private List<MyBluetoothDevice> mScanDevices;// 扫描设备集合
    private boolean mIsSupportBlePhone = true;// 是否支持蓝牙电话
    private boolean mIsIgnore = false;

    private BluetoothDevice mConnectedSpeaker;// 当前连接的蓝牙音箱
    private BluetoothDevice mPreConnectSpeaker;// 准备连接的蓝牙音箱
    private String mConnectedPhoneAddr = "";// 当前连接的手机地址
    private String mPreConnectPhoneAddr = "";// 准备连接的手机地址

    // 蓝牙音频传输协议
    private BluetoothA2dp mA2dp;
    private BluetoothHeadset mHeadset;

    private static final String FIND_PHONE = "com.viomi.fridge.curPhone";// 查询当前连接的手机
    private static final String FIND_PHONE_RESULT = "com.viomi.fridge.curPhoneResult";// 查询结果
    private static final String CONNECT_TO_PHONE = "com.viomi.fridge.connectTo";// 与手机连接或断开连接
    private static final String PHONE_CONNECTION_STATE_CHANGED =
            "android.bluetooth.headsetclient.profile.action.CONNECTION_STATE_CHANGED";// 手机连接状态改变

    private static final int REQ_ENABLE_BT = 1000;

    private BroadcastReceiver mReceiver;

    @BindView(R.id.sb_bluetooth)
    SwitchButton mSwitchButton;// 蓝牙开关

    @BindView(R.id.tv_bluetooth_device_name)
    TextView mDeviceNameTextView;// 设备名称
    @BindView(R.id.tv_bluetooth_refresh)
    TextView mRefreshTextView;// 刷新

    @BindView(R.id.rv_bluetooth_paired)
    RecyclerView mPairedRecyclerView;// 已配对列表
    @BindView(R.id.rv_bluetooth_scan)
    RecyclerView mScanRecyclerView;// 扫描列表

    @BindView(R.id.ll_bluetooth_content)
    LinearLayout mContentLinearLayout;// 蓝牙设备列表布局

    @BindView(R.id.fl_bluetooth_bottom)
    FrameLayout mBottomFrameLayout;// 底部刷新布局

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.dialog_bluetooth_setting;
    }

    @Override
    protected void initWithOnCreateDialog(View view) {
        logUtil.i(TAG, "initWithOnCreateDialog");
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            ToastUtil.showCenter(FridgeApplication.getContext(), FridgeApplication.getContext().getResources().getString(R.string.management_bluetooth_no_support));
            dismiss();
            return;
        }
        initView();
        initDialog();
        initListener();
        initReceiver();
        initBluetooth();
    }

    @Override
    public void onStart() {
        super.onStart();
        // 设置背景
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setBackgroundDrawableResource(android.R.color.transparent);
            WindowManager.LayoutParams windowParams = window.getAttributes();
            windowParams.dimAmount = 0.5f;
            window.setAttributes(windowParams);
        }
    }

    public void initView() {
        mDeviceNameTextView.setText(mBluetoothAdapter.getName());// 蓝牙名称
        // 绑定设备列表
        LinearLayoutManager pairedLayoutManager = new LinearLayoutManager(getActivity()) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        pairedLayoutManager.setAutoMeasureEnabled(true);
        mPairedRecyclerView.setLayoutManager(pairedLayoutManager);
        mPairDevices = new ArrayList<>();
        mBondAdapter = new BluetoothListAdapter(getActivity(), mPairDevices);
        mPairedRecyclerView.setAdapter(mBondAdapter);
        // 扫描设备列表
        LinearLayoutManager scanLayoutManager = new LinearLayoutManager(getActivity()) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        scanLayoutManager.setAutoMeasureEnabled(true);
        mScanRecyclerView.setLayoutManager(scanLayoutManager);
        mScanDevices = new ArrayList<>();
        mScanAdapter = new BluetoothListAdapter(getActivity(), mScanDevices);
        mScanRecyclerView.setAdapter(mScanAdapter);
    }

    public void initDialog() {
        mEditNameDialog = new EditNameDialog(getActivity());
        mEditNameDialog.setEditNameDialogListener(info -> {
            if (mBluetoothAdapter.isDiscovering()) { // 先取消搜索
                mBluetoothAdapter.cancelDiscovery();
            }
            if (mBluetoothAdapter.setName(info)) { // 编辑蓝牙名称成功
                mDeviceNameTextView.setText(info);
            }
        });
        mLoadingDialog = new LoadingDialog(getActivity());
    }

    private void showLoading() {
        if (mLoadingDialog != null && !mLoadingDialog.isShowing()) mLoadingDialog.show();
    }

    private void hideLoading() {
        if (mLoadingDialog != null && mLoadingDialog.isShowing()) mLoadingDialog.cancel();
    }

    private void initListener() {
        // 蓝牙开关
        mSwitchButton.setOnSwitchStateChangeListener(isOn -> {
            if (mIsIgnore) return;
            if (isOn) {
                mBluetoothAdapter.enable();
                bluetoothEnableChanged(true);
                setDiscoverableTimeout(300);
//                Intent mIntent = new Intent(android.bluetooth.BluetoothAdapter.ACTION_REQUEST_ENABLE);
//                startActivityForResult(mIntent, REQ_ENABLE_BT);
            } else {
                new AlertDialog.Builder(getActivity()).setTitle(FridgeApplication.getContext().getResources().getString(R.string.management_bluetooth_close_tip))
                        .setMessage(FridgeApplication.getContext().getResources().getString(R.string.management_bluetooth_close_message))
                        .setNegativeButton(FridgeApplication.getContext().getResources().getString(R.string.cancel), (dialog, which) -> {
                            mIsIgnore = true;
                            mSwitchButton.setOn(true, true);
                            mIsIgnore = false;
                            dialog.dismiss();
                        })
                        .setCancelable(false)
                        .setPositiveButton(FridgeApplication.getContext().getResources().getString(R.string.confirm), (dialog, which) -> {
                            logUtil.d(TAG, "__________5");
                            bluetoothEnableChanged(false);
                            dialog.dismiss();
                            closeBluetoothDiscoverable();// 停止蓝牙搜索
                        }).show();
            }
        });
        // 编辑蓝牙名称
        mDeviceNameTextView.setOnClickListener(v -> mEditNameDialog.show(mBluetoothAdapter.getName()));
        // 刷新
        mRefreshTextView.setOnClickListener(v -> {
            if (mBluetoothAdapter.isDiscovering()) return;
            mRefreshTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.management_bluetooth_scanning));
            mScanDevices.clear();
            mScanAdapter.notifyDataSetChanged();
            mBluetoothAdapter.startDiscovery();
        });
        // 绑定列表
        mBondAdapter.setOnItemClickListener(myBluetoothDevice -> {
            if (mBluetoothAdapter.isDiscovering()) mBluetoothAdapter.cancelDiscovery();
            if (myBluetoothDevice.isConnected()) {
                new AlertDialog.Builder(getActivity()).setTitle(FridgeApplication.getContext().getResources().getString(R.string.management_bluetooth_close_single_tip))
                        .setMessage(FridgeApplication.getContext().getResources().getString(R.string.management_bluetooth_close_single))
                        .setCancelable(false)
                        .setNegativeButton(FridgeApplication.getContext().getResources().getString(R.string.cancel), (dialog, which) -> dialog.dismiss())
                        .setPositiveButton(FridgeApplication.getContext().getResources().getString(R.string.confirm), (dialog, which) -> {
                            if (myBluetoothDevice.getBluetoothDevice().getBluetoothClass().getMajorDeviceClass() == BluetoothClass.Device.Major.PHONE) {
                                switchConnectPhone(myBluetoothDevice.getBluetoothDevice().getAddress(), false);
                            } else {
                                try {
                                    if (mA2dp != null)
                                        mA2dp.getClass()
                                                .getMethod("disconnect", BluetoothDevice.class)
                                                .invoke(mA2dp, myBluetoothDevice.getBluetoothDevice());
                                    if (mHeadset != null)
                                        mHeadset.getClass()
                                                .getMethod("disconnect", BluetoothDevice.class)
                                                .invoke(mHeadset, myBluetoothDevice.getBluetoothDevice());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            dialog.dismiss();
                        }).show();
            } else { // 进行连接
                closeProfile();
                if (myBluetoothDevice.getBluetoothDevice().getBluetoothClass().getMajorDeviceClass() == BluetoothClass.Device.Major.PHONE) {
                    showLoading();
                    mPreConnectPhoneAddr = myBluetoothDevice.getBluetoothDevice().getAddress();
                    if (mConnectedPhoneAddr.length() > 0)
                        switchConnectPhone(mConnectedPhoneAddr, false);
                    else
                        switchConnectPhone(mPreConnectPhoneAddr, true);
                } else { // 连接耳机
                    showLoading();
                    mPreConnectSpeaker = myBluetoothDevice.getBluetoothDevice();
                    if (mConnectedPhoneAddr.length() > 0)// 断开 HFP 的手机连接
                        switchConnectPhone(mConnectedPhoneAddr, false);
                    connectProfile();
                }
            }
        });
        // 绑定列表
        mBondAdapter.setOnItemLongClickListener(myBluetoothDevice -> {
            if (mBluetoothAdapter.isDiscovering()) {
                mBluetoothAdapter.cancelDiscovery();
            }
            new AlertDialog.Builder(getActivity()).setTitle(FridgeApplication.getContext().getResources().getString(R.string.management_bluetooth_delete_tip))
                    .setMessage(FridgeApplication.getContext().getResources().getString(R.string.management_bluetooth_delete_message))
                    .setCancelable(false)
                    .setNegativeButton(FridgeApplication.getContext().getResources().getString(R.string.cancel), (dialog, which) -> dialog.dismiss())
                    .setPositiveButton(FridgeApplication.getContext().getResources().getString(R.string.confirm), (dialog, which) -> {
                        removeBond(myBluetoothDevice.getBluetoothDevice());
                        mRefreshTextView.performClick();
                        dialog.dismiss();
                    }).show();
        });
        // 扫描列表
        mScanAdapter.setOnItemClickListener(myBluetoothDevice -> {
            if (mBluetoothAdapter.isDiscovering()) mBluetoothAdapter.cancelDiscovery();
            showLoading();
            myBluetoothDevice.getBluetoothDevice().createBond();
        });
    }

    /**
     * 取消蓝牙配对
     *
     * @param device 蓝牙设备
     */
    public static void removeBond(BluetoothDevice device) {
        try {
            Method m = device.getClass().getMethod("removeBond", (Class[]) null);
            m.invoke(device, (Object[]) null);
        } catch (Exception e) {
            logUtil.e(TAG, e.getMessage());
        }
    }

    /**
     * 初始化广播
     */
    public void initReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        // 搜索到蓝牙设备，每搜到一个发送一条广播
        intentFilter.addAction(BluetoothDevice.ACTION_FOUND);
        // 搜索结束
        intentFilter.addAction(android.bluetooth.BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        // 配对状态改变
        intentFilter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        // 蓝牙设备连接
        intentFilter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        // 蓝牙设备断开连接
        intentFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        // 蓝牙设备提出断开连接请求，即将断开连接
        intentFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        // 打开、关闭蓝牙模块
        intentFilter.addAction(android.bluetooth.BluetoothAdapter.ACTION_STATE_CHANGED);
        // A2DP 连接状态改变
        intentFilter.addAction(BluetoothA2dp.ACTION_CONNECTION_STATE_CHANGED);
        // A2DP 播放状态改变
        intentFilter.addAction(BluetoothA2dp.ACTION_PLAYING_STATE_CHANGED);
        // 蓝牙音箱连接状态改变
        intentFilter.addAction(BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED);

        if (mIsSupportBlePhone) {
            // 手机连接状态改变
            intentFilter.addAction(PHONE_CONNECTION_STATE_CHANGED);
            // 查询手机结果
            intentFilter.addAction(FIND_PHONE_RESULT);
        }
        mReceiver = new BlueBroadcastReceiver();
        if (getActivity() != null) getActivity().registerReceiver(mReceiver, intentFilter);
    }

    private void initBluetooth() {
        bluetoothEnableChanged(mBluetoothAdapter.isEnabled());
        if (mBluetoothAdapter.isEnabled()) {
            setDiscoverableTimeout(300);
        }
    }

    /**
     * 蓝牙状态改变
     *
     * @param enable 蓝牙是否打开
     */
    public void bluetoothEnableChanged(boolean enable) {
        logUtil.i(TAG, "bluetoothEnableChanged");
        mSwitchButton.setOn(enable);
        mContentLinearLayout.setVisibility(enable ? View.VISIBLE : View.GONE);
        mBottomFrameLayout.setVisibility(enable ? View.VISIBLE : View.GONE);
        clearConnectDevice();
        if (enable) {
            if (mIsSupportBlePhone) findCurPhone();
            connectProfile();
            refreshBoundedDevices();
            mRefreshTextView.performClick();
        } else {
            mBluetoothAdapter.disable();
        }
    }


    /**
     * 刷新已配对设备列表
     */
    public void refreshBoundedDevices() {
        if (!mBluetoothAdapter.isEnabled()) return;
        mPairDevices.clear();
        Set<BluetoothDevice> devices = mBluetoothAdapter.getBondedDevices();
        if (devices != null && devices.size() > 0) {
            BluetoothDevice connectDevice = getConnectDevice();
            for (BluetoothDevice device : devices) {
                MyBluetoothDevice myDevice = new MyBluetoothDevice(device);
                int type = device.getBluetoothClass().getMajorDeviceClass();
                if (type == BluetoothClass.Device.Major.AUDIO_VIDEO) { // 音频设备
                    logUtil.i(TAG, "mA2dp=" + mA2dp + ",mHeadset=" + mHeadset + ",device=" + myDevice.getBluetoothDevice().getName() + ",isConnected=" + myDevice.isConnected());
                    if (mA2dp != null) {
                        logUtil.d(TAG, "mA2dp connect state=" + mA2dp.getConnectionState(myDevice.getBluetoothDevice()));
                        myDevice.setConnected(mA2dp.getConnectionState(myDevice.getBluetoothDevice()) == BluetoothProfile.STATE_CONNECTED);
                    }
                    if (mHeadset != null && !myDevice.isConnected()) {
                        logUtil.d(TAG, "mHeadset connect state=" + mHeadset.getConnectionState(myDevice.getBluetoothDevice()));
                        myDevice.setConnected(mHeadset.getConnectionState(myDevice.getBluetoothDevice()) == BluetoothProfile.STATE_CONNECTED);
                    }
                    if (connectDevice != null && device.getAddress().equals(connectDevice.getAddress())) {
                        myDevice.setConnected(true);
                    }
                    if (!mBluetoothAdapter.getAddress().equals(myDevice.getBluetoothDevice().getAddress()) && !mPairDevices.contains(myDevice))
                        mPairDevices.add(myDevice);
                } else if (type == BluetoothClass.Device.Major.PHONE && mIsSupportBlePhone) { // 电话
                    myDevice.setConnected(mConnectedPhoneAddr.equals(myDevice.getBluetoothDevice().getAddress()));
                    if (!mBluetoothAdapter.getAddress().equals(myDevice.getBluetoothDevice().getAddress()) && !mPairDevices.contains(myDevice))
                        mPairDevices.add(myDevice);
                }
            }
        }
        mBondAdapter.notifyDataSetChanged();
    }

    /**
     * 查找连接的手机
     */
    public void findCurPhone() {
        if (getActivity() == null) return;
        Intent intent = new Intent(FIND_PHONE);
        getActivity().sendBroadcast(intent);
        logUtil.w(TAG, "sendBroadcast " + intent.getAction());
    }

    /**
     * 改变手机连接状态
     *
     * @param bleAddr   手机蓝牙地址
     * @param connected true 为进行连接，false 为断开
     */
    public void switchConnectPhone(String bleAddr, boolean connected) {
        if (getActivity() == null) return;
        Intent intent = new Intent(CONNECT_TO_PHONE);
        intent.putExtra("bleAddr", bleAddr);
        intent.putExtra("status", connected ? "1" : "0");
        getActivity().sendBroadcast(intent);
        logUtil.w(TAG, "sendBroadcast " + intent.getAction() + " the bleAddr:" + bleAddr +
                " switch to:" + (connected ? "1" : "0"));
    }

    /**
     * 连接 A2DP 与 Headset
     */
    private void connectProfile() {
        closeProfile();
        mBluetoothAdapter.getProfileProxy(FridgeApplication.getContext(),
                mProfileServiceListener, BluetoothProfile.A2DP);
        mBluetoothAdapter.getProfileProxy(FridgeApplication.getContext(),
                mProfileServiceListener, BluetoothProfile.HEADSET);
    }


    private BluetoothDevice getConnectDevice() {
        Class<BluetoothAdapter> bluetoothAdapterClass = BluetoothAdapter.class;//得到BluetoothAdapter的Class对象
        try {//得到蓝牙状态的方法
            Method method = bluetoothAdapterClass.getDeclaredMethod("getConnectionState", (Class[]) null);
            //打开权限
            method.setAccessible(true);
            int state = (int) method.invoke(mBluetoothAdapter, (Object[]) null);

            if (state == BluetoothAdapter.STATE_CONNECTED) {

                logUtil.i(TAG, "BluetoothAdapter.STATE_CONNECTED");

                Set<BluetoothDevice> devices = mBluetoothAdapter.getBondedDevices();
                logUtil.i(TAG, "devices:" + devices.size());

                for (BluetoothDevice device : devices) {

                    Method isConnectedMethod = BluetoothDevice.class.getDeclaredMethod("isConnected", (Class[]) null);
                    method.setAccessible(true);
                    boolean isConnected = (boolean) isConnectedMethod.invoke(device, (Object[]) null);

                    if (isConnected) {
                        logUtil.i(TAG, "connected:" + device.getAddress());
                        return device;
                    }

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 断开当前 A2DP 与 Headset
     */
    private void closeProfile() {
        logUtil.e(TAG, "closeProfile");
        try {
            if (mA2dp != null) {
                mA2dp.getClass()
                        .getMethod("disconnect", BluetoothDevice.class)
                        .invoke(mA2dp, mConnectedSpeaker);
            }
            if (mHeadset != null) {
                mHeadset.getClass()
                        .getMethod("disconnect", BluetoothDevice.class)
                        .invoke(mHeadset, mConnectedSpeaker);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (mA2dp != null) {
            mBluetoothAdapter.closeProfileProxy(BluetoothProfile.A2DP, mA2dp);
        }
        if (mHeadset != null) {
            mBluetoothAdapter.closeProfileProxy(BluetoothProfile.HEADSET, mHeadset);
        }
    }

    /**
     * 清除已连接和待连接设备
     */
    public void clearConnectDevice() {
        mConnectedSpeaker = null;
        mPreConnectSpeaker = null;
        mConnectedPhoneAddr = "";
        mPreConnectPhoneAddr = "";
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_ENABLE_BT) {
            // 打开蓝牙
            if (resultCode == Activity.RESULT_OK) {
                logUtil.d(TAG, "__________1");
                bluetoothEnableChanged(true);
                setDiscoverableTimeout(300);
            } else {
                logUtil.d(TAG, "__________2");
                bluetoothEnableChanged(false);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //   closeProfile();
        if (getActivity() != null) {
            getActivity().unregisterReceiver(mReceiver);
        }
        if (mEditNameDialog != null && mEditNameDialog.isShowing()) {
            mEditNameDialog.cancel();
            mEditNameDialog = null;
        }
        if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
            mLoadingDialog.cancel();
            mLoadingDialog = null;
        }
        if (mBluetoothAdapter != null) mBluetoothAdapter = null;
        if (mBondAdapter != null) mBondAdapter = null;
        if (mScanAdapter != null) mScanAdapter = null;
        if (mPairDevices != null) {
            mPairDevices.clear();
            mPairDevices = null;
        }
        if (mScanDevices != null) {
            mScanDevices.clear();
            mScanDevices = null;
        }
        if (mProfileServiceListener != null) {
            mProfileServiceListener = null;
        }
    }

    /**
     * 连接蓝牙音箱
     */
    private BluetoothProfile.ServiceListener mProfileServiceListener = new BluetoothProfile.ServiceListener() {

        @Override
        public void onServiceConnected(int profile, BluetoothProfile proxy) {
            mPreConnectSpeaker = getConnectDevice();
            if (mPreConnectSpeaker != null) {
                logUtil.i(TAG, "mPreConnectSpeaker =" + mPreConnectSpeaker.getName());
            } else {
                logUtil.i(TAG, "mPreConnectSpeaker null");
            }
            logUtil.d(TAG, "-----------mProfileServiceListener profile=" + profile + ",mPreConnectSpeaker=" + mPreConnectSpeaker);
            if (mPreConnectSpeaker == null) return;
            try {
                switch (profile) {
                    case BluetoothProfile.A2DP:
                        logUtil.d(TAG, "A2DP onServiceConnected");
                        mA2dp = (BluetoothA2dp) proxy;
                        refreshBoundedDevices();
                        mPreConnectSpeaker = getConnectDevice();
                        if (mPreConnectSpeaker != null) {
                            logUtil.i(TAG, "mPreConnectSpeaker =" + mPreConnectSpeaker.getName());
                        } else {
                            logUtil.i(TAG, "mPreConnectSpeaker null");
                        }
                        if (mA2dp.getConnectionState(mPreConnectSpeaker) == BluetoothProfile.STATE_DISCONNECTED) {
                            mA2dp.getClass()
                                    .getMethod("connect", BluetoothDevice.class)
                                    .invoke(mA2dp, mPreConnectSpeaker);
                        }
                        break;
                    case BluetoothProfile.HEADSET:
                        logUtil.d(TAG, "Headset onServiceConnected");
                        mHeadset = (BluetoothHeadset) proxy;
                        if (mHeadset.getConnectionState(mPreConnectSpeaker) == BluetoothProfile.STATE_DISCONNECTED) {
                            mHeadset.getClass()
                                    .getMethod("connect", BluetoothDevice.class)
                                    .invoke(mHeadset, mPreConnectSpeaker);
                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(int profile) {
            switch (profile) {
                case BluetoothProfile.A2DP:
                    logUtil.d(TAG, "A2DP disconnected");
                    mA2dp = null;
                    break;
                case BluetoothProfile.HEADSET:
                    logUtil.d(TAG, "Headset disconnected");
                    mHeadset = null;
                    break;
            }
        }
    };

    private class BlueBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context paramAnonymousContext, Intent intent) {
            String action = intent.getAction();
            if (action == null) return;
            logUtil.w(TAG, "receive action: " + action);
            BluetoothDevice device;
            int state, connectState;
            switch (action) {
                case FIND_PHONE_RESULT: // 查询绑定结果
                    if (!intent.getStringExtra("bleAddr").isEmpty()) {
                        mConnectedPhoneAddr = intent.getStringExtra("bleAddr");
                        logUtil.w(TAG, "the action is:" + action + " the bleAddr is:" + mConnectedPhoneAddr);
                        refreshBoundedDevices();
                        hideLoading();
                    }
                    break;
                case BluetoothDevice.ACTION_FOUND: // 搜索到蓝牙设备，每搜到一个发送一条广播
                    device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    state = device.getBondState();
                    int type = device.getBluetoothClass().getMajorDeviceClass();
                    logUtil.d(TAG, "find bluetooth device: " + device.getName() + " the bound state: " + state + " the type is: " + type);
                    // 判断可连接设备（手机、音箱）
                    if (state == BluetoothDevice.BOND_NONE &&
                            ((type == BluetoothClass.Device.Major.PHONE && mIsSupportBlePhone) ||
                                    type == BluetoothClass.Device.Major.AUDIO_VIDEO) && device.getName() != null && !device.getName().equals("")) {
                        MyBluetoothDevice myBluetoothDevice = new MyBluetoothDevice(device);
                        if (!mScanDevices.contains(myBluetoothDevice) &&
                                !myBluetoothDevice.getBluetoothDevice().getAddress().equals(mBluetoothAdapter.getAddress())) { // 防止重复添加
                            mScanDevices.add(new MyBluetoothDevice(device));
                            mScanAdapter.notifyItemInserted(mScanDevices.size() - 1);
                        }
                    }
                    break;
                case android.bluetooth.BluetoothAdapter.ACTION_DISCOVERY_FINISHED: // 搜索结束
                    mRefreshTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.refresh));
                    break;
                case BluetoothDevice.ACTION_BOND_STATE_CHANGED: // 配对状态改变
                    device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    switch (device.getBondState()) {
                        case BluetoothDevice.BOND_BONDING:
                            logUtil.d(TAG, FridgeApplication.getContext().getResources().getString(R.string.management_bluetooth_matching));
                            break;
                        case BluetoothDevice.BOND_BONDED:
                            logUtil.d(TAG, FridgeApplication.getContext().getResources().getString(R.string.management_bluetooth_match_finish));
                            refreshBoundedDevices();
                            if (device.getBluetoothClass().getMajorDeviceClass() == BluetoothClass.Device.Major.PHONE && mIsSupportBlePhone) {
                                // 连接手机
                                closeProfile();
                                mPreConnectPhoneAddr = device.getAddress();
                                if (mConnectedPhoneAddr.length() > 0) {
                                    switchConnectPhone(mConnectedPhoneAddr, false);
                                } else {
                                    switchConnectPhone(mPreConnectPhoneAddr, true);
                                }
                            } else {
                                // 连接音箱
                                mPreConnectSpeaker = device;
                                connectProfile();
                            }
                            for (int i = mScanDevices.size() - 1; i >= 0; i--) {
                                MyBluetoothDevice myDevice = mScanDevices.get(i);
                                if (myDevice.getBluetoothDevice().getAddress().equals(device.getAddress())) {
                                    mScanDevices.remove(myDevice);
                                    break;
                                }
                            }
                            mScanAdapter.notifyDataSetChanged();
                            hideLoading();
                            break;
                        case BluetoothDevice.BOND_NONE:
                            logUtil.d(TAG, FridgeApplication.getContext().getResources().getString(R.string.management_bluetooth_match_cancel));
                            refreshBoundedDevices();
                            hideLoading();
                            break;
                    }
                    break;
                case BluetoothDevice.ACTION_ACL_CONNECTED: // 蓝牙设备连接
                    device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    logUtil.d(TAG, "蓝牙设备已连接" + " device:" + device.getName());
                    break;
                case BluetoothDevice.ACTION_ACL_DISCONNECTED: // 蓝牙设备断开连接
                    device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    logUtil.d(TAG, "蓝牙设备已断开" + " device:" + device.getName());
                    break;
                case BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED: // 蓝牙设备提出断开连接请求，即将断开连接
                    device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    logUtil.d(TAG, "蓝牙设备即将断开" + " device:" + device.getName());
                    break;
                case android.bluetooth.BluetoothAdapter.ACTION_STATE_CHANGED: // 打开、关闭蓝牙模块
                    state = intent.getIntExtra(android.bluetooth.BluetoothAdapter.EXTRA_STATE, -1);
                    switch (state) {
                        case android.bluetooth.BluetoothAdapter.STATE_ON: // 蓝牙开
                            bluetoothEnableChanged(true);
                            break;
                        case android.bluetooth.BluetoothAdapter.STATE_OFF: // 蓝牙关
                            bluetoothEnableChanged(false);
                            break;
                    }
                    break;
                // A2DP、Headset 连接状态改变
                case BluetoothA2dp.ACTION_CONNECTION_STATE_CHANGED:
                case BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED:
                    device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    connectState = intent.getIntExtra(BluetoothA2dp.EXTRA_STATE, -1);
                    logUtil.d(TAG, "connectState: " + connectState + " device: " + device.getName());
                    switch (connectState) {
                        case BluetoothProfile.STATE_CONNECTING: // 正在连接
                            logUtil.d(TAG, "connecting");
                            showLoading();
                            break;
                        case BluetoothProfile.STATE_CONNECTED: // 已连接
                            logUtil.d(TAG, "connected");
                            mConnectedSpeaker = device;
                            mPreConnectSpeaker = null;
                            refreshBoundedDevices();
                            // 连接成功，开始播放
                            hideLoading();
                            break;
                        case BluetoothProfile.STATE_DISCONNECTED: // 断开连接
                            logUtil.d(TAG, "disconnected");
                            // 已连接的设备切换为断开
                            if (mConnectedSpeaker != null) {
                                mConnectedSpeaker = null;
                                refreshBoundedDevices();
                            }
                            hideLoading();
                            break;
                    }
                    break;
                case BluetoothA2dp.ACTION_PLAYING_STATE_CHANGED: // A2DP 播放状态改变
                    device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    connectState = intent.getIntExtra(BluetoothA2dp.EXTRA_STATE, -1);
                    logUtil.d(TAG, "connected");
                    logUtil.d(TAG, "connectState: " + connectState + " device: " + device.getName());
                    mConnectedSpeaker = device;
                    refreshBoundedDevices();
                    // 连接成功，开始播放
                    hideLoading();
                    break;
                case PHONE_CONNECTION_STATE_CHANGED: // 手机连接状态改变
                    device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    connectState = intent.getIntExtra(BluetoothA2dp.EXTRA_STATE, -1);
                    logUtil.d(TAG, "connectState: " + connectState + " device: " + device.getName());
                    switch (connectState) {
                        case BluetoothProfile.STATE_CONNECTING: // 正在连接
                            logUtil.d(TAG, "connecting");
                            showLoading();
                            break;
                        case BluetoothProfile.STATE_CONNECTED: // 已连接
                            logUtil.d(TAG, "connected");
                            mConnectedPhoneAddr = device.getAddress();
                            mPreConnectPhoneAddr = "";
                            refreshBoundedDevices();
                            hideLoading();
                            break;
                        case BluetoothProfile.STATE_DISCONNECTING: // 正在断开连接
                            logUtil.d(TAG, "disconnecting");
                            break;
                        case BluetoothProfile.STATE_DISCONNECTED: // 已断开连接
                            logUtil.d(TAG, "disconnected");
                            if (mConnectedPhoneAddr.length() > 0 && mPreConnectPhoneAddr.length() == 0 &&
                                    mConnectedPhoneAddr.equals(device.getAddress())) {
                                // 已连接的设备切换为断开
                                mConnectedPhoneAddr = "";
                                refreshBoundedDevices();
                                hideLoading();
                            } else if (mConnectedPhoneAddr.length() > 0 &&
                                    (mPreConnectPhoneAddr.length() > 0 || mPreConnectSpeaker != null) &&
                                    mConnectedPhoneAddr.equals(device.getAddress())) {
                                // 连接其他设备
                                mConnectedPhoneAddr = "";
                                if (mPreConnectPhoneAddr.length() > 0) {
                                    switchConnectPhone(mPreConnectPhoneAddr, true);
                                }
                            } else if (mPreConnectPhoneAddr.length() > 0 &&
                                    mPreConnectPhoneAddr.equals(device.getAddress())) {
                                // 连接失败
                                mPreConnectPhoneAddr = "";
                                refreshBoundedDevices();
                                hideLoading();
                            } else hideLoading();
                            refreshBoundedDevices();
                            break;
                    }
                    break;
            }
        }
    }

    /**
     * 开启蓝牙可见性
     */
    public void setDiscoverableTimeout(int timeout) {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        try {
            Method setDiscoverableTimeout = BluetoothAdapter.class.getMethod("setDiscoverableTimeout", int.class);
            setDiscoverableTimeout.setAccessible(true);
            Method setScanMode = BluetoothAdapter.class.getMethod("setScanMode", int.class, int.class);
            setScanMode.setAccessible(true);
            setDiscoverableTimeout.invoke(adapter, timeout);
            setScanMode.invoke(adapter, BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE, timeout);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 关闭蓝牙可见性
     */
    private void closeBluetoothDiscoverable() {
        // 尝试关闭蓝牙可见性
        try {
            Method setDiscoverableTimeout = BluetoothAdapter.class.getMethod("setDiscoverableTimeout", int.class);
            setDiscoverableTimeout.setAccessible(true);
            Method setScanMode = BluetoothAdapter.class.getMethod("setScanMode", int.class, int.class);
            setScanMode.setAccessible(true);

            setDiscoverableTimeout.invoke(mBluetoothAdapter, 1);
            setScanMode.invoke(mBluetoothAdapter, BluetoothAdapter.SCAN_MODE_CONNECTABLE, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
