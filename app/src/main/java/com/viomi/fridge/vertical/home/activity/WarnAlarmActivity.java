package com.viomi.fridge.vertical.home.activity;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Window;
import android.widget.TextView;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.speech.util.WakeAndLock;

/**
 * 报警 Activity
 * Created by William on 2018/3/4.
 */
public class WarnAlarmActivity extends Activity {
    private MediaPlayer mMediaPlayer;
    private int mCurrentVolume;// 当前音量
    private AudioManager mAudioManager;// 音量管理器

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// 取消标题栏
        setContentView(R.layout.activity_warn_alarm);

        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        if (mAudioManager != null) {
            mCurrentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_SYSTEM);
            int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM);
            mAudioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, maxVolume, 0);
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume, 0);
        }

        WakeAndLock wakeAndLock = new WakeAndLock();
        wakeAndLock.screenOn();

        TextView textView = findViewById(R.id.warn_alarm_text);

        int rawId = 0;
        int type = getIntent().getIntExtra("type", 0);
        if (type == 0) { // 水浸报警
            rawId = R.raw.water_alarm;
            textView.setText(getResources().getString(R.string.alarm_water));
        } else if (type == 1) { // 入侵报警
            rawId = R.raw.people_alarm;
            textView.setText(getResources().getString(R.string.alarm_people));
        } else if (type == 2) { // 烟雾报警
            rawId = R.raw.smoke_alarm;
            textView.setText(getResources().getString(R.string.alarm_smoke));
        } else if (type == 3) { // 燃气泄漏报警
            rawId = R.raw.gas_alarm;
            textView.setText(getResources().getString(R.string.alarm_gas));
        }

        if (rawId != 0) {
            mMediaPlayer = MediaPlayer.create(this, R.raw.water_alarm);
            mMediaPlayer.setLooping(true);
            mMediaPlayer.start();
        }

        findViewById(R.id.warn_alarm_close).setOnClickListener(v -> finish());
    }

    @Override
    protected void onDestroy() {
        mAudioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, mCurrentVolume, 0);
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mCurrentVolume, 0);
        if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
        }
        super.onDestroy();
    }
}