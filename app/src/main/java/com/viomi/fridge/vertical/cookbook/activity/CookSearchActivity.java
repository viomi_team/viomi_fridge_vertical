package com.viomi.fridge.vertical.cookbook.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.view.dialog.LoadingDialog;
import com.viomi.fridge.vertical.common.base.BaseActivity;
import com.viomi.fridge.vertical.common.http.ApiClient;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.cookbook.model.CookMenuDetail;
import com.viomi.fridge.vertical.cookbook.model.CookMenuMethod;
import com.viomi.fridge.vertical.cookbook.model.CookMenuSearch;
import com.viomi.fridge.vertical.cookbook.view.adapter.CookMenuListAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * 菜谱搜索 Activity
 * Created by nanquan on 2018/2/26.
 */
public class CookSearchActivity extends BaseActivity {
    private static final String TAG = CookSearchActivity.class.getSimpleName();
    private CompositeSubscription mCompositeSubscription;
    private LoadingDialog mLoadingDialog;// 加载对话框
    private List<CookMenuDetail> mList;
    private CookMenuListAdapter mAdapter;
    private String mKeyword;// 关键字
    private int mCount = 0;// 无结果次数
    private boolean mIsRepeat = true;
    private int mCurrentPage = 1;// 当前页数

    @BindView(R.id.et_title_search)
    EditText mEditText;// 搜索框输入

    @BindView(R.id.cookbook_search_list)
    RecyclerView mRecyclerView;// 搜索列表

    @BindView(R.id.cookbook_search_no_content)
    LinearLayout mLinearLayout;// 无搜索结果时显示

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        layoutId = R.layout.activity_cook_search;
        super.onCreate(savedInstanceState);
        mLoadingDialog = new LoadingDialog(CookSearchActivity.this);
        mCompositeSubscription = new CompositeSubscription();
        initRecyclerView();
        initEditText();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
        if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
            mLoadingDialog.cancel();
            mLoadingDialog = null;
        }
        if (mList != null) {
            mList.clear();
            mList = null;
        }
        if (mAdapter != null) mAdapter = null;
    }

    private void initRecyclerView() {
        mList = new ArrayList<>();
        mAdapter = new CookMenuListAdapter(this, mList);
        mAdapter.setOnItemClickListener(position -> {
            Intent intent = new Intent(CookSearchActivity.this, CookDetailActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("cookMenuDetail", mList.get(position));
            intent.putExtras(bundle);
            startActivity(intent);
        });
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE: {
                        // 滑动到底部，加载更多
                        if (!mRecyclerView.canScrollVertically(1)) {
                            mCurrentPage++;
                            mIsRepeat = false;
                            getSearchResult();
                        }
                    }
                }
            }
        });
    }

    private void initEditText() {
        mEditText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                mKeyword = String.valueOf(mEditText.getText()).trim();
                if (!TextUtils.isEmpty(mKeyword)) {
                    mList.clear();
                    mAdapter.notifyDataSetChanged();
                    mCurrentPage = 1;
                    getSearchResult();
                }
                hideKeyboard();
            }
            return false;
        });
    }

    private void getSearchResult() {
        if (mLoadingDialog != null && !mLoadingDialog.isShowing()) mLoadingDialog.show();
        Subscription subscription = ApiClient.getInstance().getApiService().getCookSearch(null, mKeyword, mCurrentPage, 20)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(cookMenuSearchMobResult -> {
                    if (mLoadingDialog != null && mLoadingDialog.isShowing())
                        mLoadingDialog.dismiss();
                    CookMenuSearch cookMenuSearch = cookMenuSearchMobResult.getResult();
                    if (cookMenuSearch == null || cookMenuSearch.getList() == null || cookMenuSearch.getList().size() == 0) {
                        if (mList.size() == 0) {
                            mLinearLayout.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);
                        }
                    } else {
                        mLinearLayout.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                        mList.addAll(filterNoImage(cookMenuSearch.getList()));
                        mAdapter.notifyDataSetChanged();
                        if (mIsRepeat && mCount < 3 && mList.size() < 8) {
                            mCount++;
                            mCurrentPage++;
                            getSearchResult();
                        }
                    }
                }, throwable -> {
                    if (mLoadingDialog != null && mLoadingDialog.isShowing())
                        mLoadingDialog.dismiss();
                    logUtil.e(TAG, throwable.getMessage());
                });
        mCompositeSubscription.add(subscription);
    }

    /**
     * 过滤无图菜谱
     */
    private List<CookMenuDetail> filterNoImage(List<CookMenuDetail> list) {
        List<CookMenuDetail> list1 = new ArrayList<>();
        for (CookMenuDetail detail : list) {
            if (detail == null ||
                    detail.getRecipe() == null ||
                    detail.getRecipe().getImg() == null ||
                    TextUtils.isEmpty(detail.getRecipe().getImg()) ||
                    detail.getRecipe().getMethod() == null ||
                    TextUtils.isEmpty(detail.getRecipe().getMethod())) {
                logUtil.d(TAG, "detail is null");
            } else {
                Gson gson = new Gson();
                List<CookMenuMethod> methodList = gson.fromJson(detail.getRecipe().getMethod(),
                        new TypeToken<List<CookMenuMethod>>() {
                        }.getType());
                boolean isReturn = false;
                for (CookMenuMethod cookMenuMethod : methodList) {
                    if (cookMenuMethod.getImg() == null || cookMenuMethod.getImg().equals("")
                            || cookMenuMethod.getStep() == null || cookMenuMethod.getStep().equals("")) {
                        isReturn = true;
                        break;
                    }
                }
                if (!isReturn) list1.add(detail);
            }
        }
        return list1;
    }

    /**
     * 隐藏软键盘
     */
    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(mEditText.getWindowToken(), 0);
        }
    }
}