package com.viomi.fridge.vertical.cookbook.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.util.ImageLoadUtil;
import com.viomi.fridge.vertical.cookbook.model.CookMenuDetail;
import com.viomi.fridge.vertical.cookbook.model.CookMenuRecipe;

import java.util.List;

/**
 * 菜谱列表适配器
 * Created by nanquan on 2018/2/27.
 */
public class CookMenuListAdapter extends RecyclerView.Adapter<CookMenuListAdapter.CookMenuHolder> {
    private Context mContext;
    private List<CookMenuDetail> mList;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public CookMenuListAdapter(Context context, List<CookMenuDetail> list) {
        mContext = context;
        mList = list;
    }

    @Override
    public CookMenuHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(mContext)
                .inflate(R.layout.item_cook_menu, parent, false);
        return new CookMenuHolder(rootView);
    }

    @Override
    public void onBindViewHolder(CookMenuHolder holder, int position) {
        CookMenuDetail cookMenuDetail = mList.get(position);
        CookMenuRecipe cookMenuRecipe = cookMenuDetail.getRecipe();
        // 设置图片
        if (cookMenuRecipe != null &&
                !TextUtils.isEmpty(cookMenuRecipe.getImg())) {
            ImageLoadUtil.getInstance().loadUrl(cookMenuRecipe.getImg(), holder.ivImg);
        } else {
            ImageLoadUtil.getInstance().loadResource(R.drawable.cook_menu_default, holder.ivImg);
        }
        // 设置名字
        holder.tvName.setText(cookMenuDetail.getName());
        // 设置食材
        if (cookMenuRecipe != null &&
                !TextUtils.isEmpty(cookMenuRecipe.getIngredients())) {
            Gson gson = new Gson();
            List<String> ingredientList = gson.fromJson(cookMenuRecipe.getIngredients(),
                    new TypeToken<List<String>>() {
                    }.getType());
            holder.tvMaterial.setText(ingredientList.get(0));
        } else {
            holder.tvMaterial.setText("");
        }
        // 设置描述
        if (cookMenuRecipe != null &&
                !TextUtils.isEmpty(cookMenuRecipe.getSumary())) {
            holder.tvIntro.setText(cookMenuRecipe.getSumary());
        } else {
            holder.tvIntro.setText("");
        }
        if (mOnItemClickListener != null) {
            holder.itemView.setOnClickListener(v ->
                    mOnItemClickListener.onItemClick(position));
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    class CookMenuHolder extends RecyclerView.ViewHolder {
        ImageView ivImg;
        TextView tvName;
        TextView tvMaterial;
        TextView tvIntro;

        CookMenuHolder(View itemView) {
            super(itemView);
            ivImg = itemView.findViewById(R.id.iv_cook_menu_img);
            tvName = itemView.findViewById(R.id.tv_cook_menu_name);
            tvMaterial = itemView.findViewById(R.id.tv_cook_menu_material);
            tvIntro = itemView.findViewById(R.id.tv_cook_menu_intro);
        }
    }
}