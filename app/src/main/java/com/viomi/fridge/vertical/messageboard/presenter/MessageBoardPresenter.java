package com.viomi.fridge.vertical.messageboard.presenter;

import android.content.Context;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.ToastUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.messageboard.contract.MessageBoardContract;
import com.viomi.fridge.vertical.messageboard.model.MessageBoardRepository;
import com.viomi.widget.sketchview.SketchView;

import javax.annotation.Nullable;
import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * 留言版 Presenter
 * Created by William on 2018/4/10.
 */
public class MessageBoardPresenter implements MessageBoardContract.Presenter {
    private static final String TAG = MessageBoardPresenter.class.getSimpleName();
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;

    @Nullable
    private MessageBoardContract.View mView;

    @Inject
    MessageBoardPresenter(Context context) {
        mContext = context;
    }

    @Override
    public void subscribe(MessageBoardContract.View view) {
        this.mView = view;
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public void unSubscribe() {
        this.mView = null;
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
    }

    @Override
    public void save(SketchView sketchView) {
        Subscription subscription = MessageBoardRepository.getInstance().saveImage(sketchView.getResultBitmap(), 90)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(aBoolean -> {
                    if (aBoolean) {
                        if (mView != null) mView.saveFinish();
                    } else
                        ToastUtil.showCenter(mContext, mContext.getResources().getString(R.string.message_board_save_fail_tip));
                }, throwable -> {
                    ToastUtil.showCenter(mContext, mContext.getResources().getString(R.string.message_board_save_fail_tip));
                    logUtil.e(TAG, throwable.getMessage());
                });
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void load() {
        Subscription subscription = MessageBoardRepository.getInstance().getMessageBoardList()
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(list -> {
                    if (mView != null) mView.setTitleBarUi(list);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }
}