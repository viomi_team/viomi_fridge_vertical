package com.viomi.fridge.vertical.album.contract;


import com.viomi.fridge.vertical.album.model.entity.AlbumEntity;
import com.viomi.fridge.vertical.common.callback.BasePresenter;

import java.util.List;

/**
 * 电子相册 Contract
 * Created by William on 2018/1/19.
 */
public interface AlbumContract {
    interface View {
        void initWithCache(boolean enable, String time);// 根据缓存初始化

        void notifyDataChanged();// 更新数据

        void notifyDataRemoved(int size);// 删除数据

        void notifyDataInserted(int size);// 插入数据

        void updateAlbum();// 更新相册

        void showAlbum();// 显示相册

        void hideAlbum();// 隐藏相册
    }

    interface Presenter extends BasePresenter<View> {
        void loadCache();// 读取缓存配置

        void loadAlbum(List<AlbumEntity> list);// 读取相册

        void delete(List<AlbumEntity> list);// 删除相册

        void cacheSwitch(boolean enable);// 缓存屏保开关

        void cacheTime(String time);// 缓存屏保时间

        void cacheScreenSaver(List<AlbumEntity> list);// 缓存屏保相册

        boolean isChosen(List<AlbumEntity> list);// 是否有选中
    }
}
