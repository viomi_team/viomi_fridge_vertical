package com.viomi.fridge.vertical.fridgecontrol.defined.action;

import com.viomi.fridge.vertical.fridgecontrol.defined.ViomiDefined;
import com.viomi.fridge.vertical.fridgecontrol.defined.property.IndoorTemp;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetIndoorTemp extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setIndoorTemp.toActionType();

    public SetIndoorTemp() {
        super(TYPE);

        super.addArgument(IndoorTemp.TYPE.toString());
    }
}