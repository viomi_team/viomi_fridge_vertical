package com.viomi.fridge.vertical.iot.model.repository;

import com.miot.api.MiotManager;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.common.http.ApiClient;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.iot.model.http.entity.RPCResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import rx.Observable;

/**
 * 蒸烤箱相关 Api
 * Created by William on 2018/8/30.
 */
public class OvenRepository {
    private static final String TAG = OvenRepository.class.getSimpleName();

    /**
     * 设置开始，暂停或结束烹饪
     */
    public static Observable<RPCResult> setPauseStatus(int status, String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "setpauseStatus");
            jsonObject.put("did", did);
            jsonObject.put("id", 12);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(status);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
            logUtil.e(TAG, e.toString());
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }
}
