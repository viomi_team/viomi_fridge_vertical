package com.viomi.fridge.vertical.administration.module;

import com.viomi.fridge.vertical.administration.contract.ManageContract;
import com.viomi.fridge.vertical.administration.presenter.ManagePresenter;
import com.viomi.fridge.vertical.administration.view.fragment.AboutFragment;
import com.viomi.fridge.vertical.administration.view.fragment.FileFragment;
import com.viomi.fridge.vertical.administration.view.fragment.MediaFragment;
import com.viomi.fridge.vertical.administration.view.fragment.SettingFragment;
import com.viomi.fridge.vertical.common.scope.ActivityScoped;
import com.viomi.fridge.vertical.common.scope.FragmentScoped;
import com.viomi.fridge.vertical.speech.contract.SpeechManageContract;
import com.viomi.fridge.vertical.speech.presenter.SpeechManagePresenter;
import com.viomi.fridge.vertical.speech.view.fragment.SpeechFragment;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * 管理中心 Module
 */
@Module
public abstract class ManageModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract SettingFragment settingFragment();// 通用设置

    @FragmentScoped
    @ContributesAndroidInjector
    abstract MediaFragment mediaFragment();// 屏幕与声音

    @FragmentScoped
    @ContributesAndroidInjector
    abstract SpeechFragment voiceFragment();// 语音助手

    @FragmentScoped
    @ContributesAndroidInjector
    abstract AboutFragment aboutFragment();// 关于软件

    @FragmentScoped
    @ContributesAndroidInjector
    abstract FileFragment fileFragment();// 文件管理

    @ActivityScoped
    @Binds
    abstract ManageContract.Presenter managePresenter(ManagePresenter presenter);

    @ActivityScoped
    @Binds
    abstract SpeechManageContract.Presenter speechManagePresenter(SpeechManagePresenter presenter);
}
