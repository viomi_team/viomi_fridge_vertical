package com.viomi.fridge.vertical.iot.view.dialog;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.CommonDialog;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.iot.contract.HeatKettleContract;
import com.viomi.fridge.vertical.iot.model.http.entity.HeatKettleProp;
import com.viomi.fridge.vertical.iot.presenter.HeatKettlePresenter;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 即热饮水吧 Dialog
 * Created by William on 2018/2/7.
 */
public class HeatKettleDialog extends CommonDialog implements HeatKettleContract.View {
    private static final String TAG = HeatKettleDialog.class.getSimpleName();
    public final String PARAM_NAME = "name";// 设备名称
    public final String PARAM_DID = "did";// 设备 id
    private boolean mIsSetting;
    private String mDid;
    private int mMinTemp = 40;
    private HeatKettleContract.Presenter mPresenter;

    @BindView(R.id.heat_kettle_temp_setting)
    LinearLayout mSettingLinearLayout;// 设置温度布局

    @BindView(R.id.heat_kettle_name)
    TextView mNameTextView;// 设备名称
    @BindView(R.id.heat_kettle_temp_status)
    TextView mStatusTextView;// 工作模式
    @BindView(R.id.heat_kettle_temp)
    TextView mTempTextView;// 出水温度
    @BindView(R.id.heat_kettle_temp_unit)
    TextView mTempUnitTextView;// 出水温度单位
    @BindView(R.id.heat_kettle_tds_value)
    TextView mTDSTextView;// TDS 值
    @BindView(R.id.heat_kettle_store_value)
    TextView mStoreTextView;// 存水时间
    @BindView(R.id.heat_kettle_wash_value)
    TextView mCleanTextView;// 未清洗时间
    @BindView(R.id.heat_kettle_temp_value)
    TextView mTempSettingTextView;// 温水键设置温度
    @BindView(R.id.heat_kettle_temp_tip)
    TextView mTempTipTextView;// 温度提示文字
    @BindView(R.id.heat_kettle_min_temp)
    TextView mMinTempTextView;// 最低设置温度

    @BindView(R.id.heat_kettle_temp_seek_bar)
    SeekBar mSeekBar;// 温水键温度设置条

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.dialog_heat_kettle;
    }

    @Override
    protected void initWithOnCreateDialog(View view) {
        mPresenter = new HeatKettlePresenter();
        mNameTextView.setText(getArguments() == null ? "" : getArguments().getString(PARAM_NAME));
        mSettingLinearLayout.setVisibility(View.GONE);
        mTempSettingTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.iot_value_default));
        mDid = getArguments() == null ? "" : getArguments().getString(PARAM_DID);
        mSeekBar.setMax(90 - mMinTemp);
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mTempSettingTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_value), progress + mMinTemp));
                mTempTipTextView.setText(switchTempTip(progress + mMinTemp));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mIsSetting = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mPresenter.setTemp(mDid, seekBar.getProgress() + mMinTemp);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe(this);
        mPresenter.getProp(mDid);
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.unSubscribe();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mPresenter != null) mPresenter = null;
    }

    @Override
    public void refreshUi(HeatKettleProp prop) {
        if (prop.getWork_mode() == 0)
            mStatusTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.iot_heat_kettle_temp_normal));
        else if (prop.getWork_mode() == 1)
            mStatusTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.iot_heat_kettle_temp_warm));
        else if (prop.getWork_mode() == 2)
            mStatusTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.iot_heat_kettle_temp_hot));
        else
            mStatusTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.iot_heat_kettle_temp_no_select));
        if (mTempUnitTextView.getVisibility() != View.VISIBLE)
            mTempUnitTextView.setVisibility(View.VISIBLE);
        mTempTextView.setText(String.valueOf(prop.getSetup_tempe()));
        mTDSTextView.setText(String.valueOf(prop.getTds()));
        mStoreTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_heat_kettle_store_value), prop.getWater_remain_time()));
        mCleanTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_heat_kettle_wash_value), prop.getFlush_time() / 24));
        if (!mIsSetting) {
            if (90 - prop.getMin_set_tempe() <= 0) {
                mMinTempTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.iot_heat_kettle_error));
                mSeekBar.setEnabled(false);
            } else {
                mMinTemp = prop.getMin_set_tempe();
                mSeekBar.setMax(90 - mMinTemp);
                mSeekBar.setEnabled(true);
                mMinTempTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_value), mMinTemp));
                mSeekBar.setProgress(prop.getCustom_tempe1() - mMinTemp);
            }
        }
    }

    @Override
    public void setIsSetting() {
        mIsSetting = false;
    }

    @OnClick(R.id.heat_kettle_set_temp_layout)
    public void showSetting() {
        if (mSettingLinearLayout.getVisibility() == View.VISIBLE)
            mSettingLinearLayout.setVisibility(View.GONE);
        else mSettingLinearLayout.setVisibility(View.VISIBLE);
    }

    /**
     * 根据温度显示对应提示
     */
    private String switchTempTip(int temp) {
        String str;
        switch (temp) {
            case 40:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_tip_40);
                break;
            case 50:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_tip_50);
                break;
            case 60:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_tip_60);
                break;
            case 75:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_tip_75);
                break;
            case 80:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_tip_80);
                break;
            case 85:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_tip_85);
                break;
            case 90:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_tip_90);
                break;
            default:
                str = "";
                break;
        }
        logUtil.d(TAG, str);
        return str;
    }
}