package com.viomi.fridge.vertical.administration.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.contract.LoginQRCodeContract;
import com.viomi.fridge.vertical.administration.model.preference.LoginPreference;
import com.viomi.fridge.vertical.administration.model.repository.LoginRepository;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.ToastUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.fridgecontrol.model.repository.MiotRepository;

import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * 登录二维码生成 Presenter
 * Created by William on 2018/1/27.
 */
public class LoginQRCodePresenter implements LoginQRCodeContract.Presenter {
    private static final String TAG = LoginQRCodePresenter.class.getSimpleName();
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;

    @Nullable
    private LoginQRCodeContract.View mView;


    public LoginQRCodePresenter(Context context) {
        mContext = context;
    }

    @Override
    public void subscribe(LoginQRCodeContract.View view) {
        this.mView = view;
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public void unSubscribe() {
        this.mView = null;
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
    }

    @Override
    public void loadQRCode() {
        Subscription subscription = LoginRepository.getInstance().createQRCode(mContext)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .filter(result -> result != null && !TextUtils.isEmpty(result.getLoginQRCode().getResult()))
                .onTerminateDetach()
                .subscribe(result -> {
                    if (mView != null) mView.showQRCode(result.getLoginQRCode());
                }, throwable -> {
                    logUtil.e(TAG, throwable.getMessage());
                    if (mView != null) mView.showRetry();
                });
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void checkStatus() {
        String clientId = LoginPreference.getInstance().getClientId();
        Subscription subscription = Observable.interval(0, 3, TimeUnit.SECONDS)
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .flatMap(aLong -> LoginRepository.getInstance().getLoginStatus(clientId))
                .takeUntil(result -> result != null && result.getLoginQRCode().getToken() != null && result.getLoginQRCode().getAppendAttr() != null
                        && result.getLoginQRCode().getLoginResult() != null && result.getLoginQRCode().getLoginResult().getUserCode() != null)
                .filter(result -> result != null && result.getLoginQRCode().getToken() != null && result.getLoginQRCode().getAppendAttr() != null
                        && result.getLoginQRCode().getLoginResult() != null && result.getLoginQRCode().getLoginResult().getUserCode() != null)
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .map(qrCodeBase -> {
                    if (mView != null) mView.showLoading();
                    return qrCodeBase;
                })
                .observeOn(Schedulers.io())
                .onTerminateDetach()
                .flatMap(qrCodeBase -> MiotRepository.getInstance().bindDevice(qrCodeBase))
                .onTerminateDetach()
                .map(qrCodeBase -> qrCodeBase != null && LoginRepository.getInstance().saveUserInfo(mContext, qrCodeBase))
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(aBoolean -> {
                    if (aBoolean) {
                        if (mView != null) mView.loginSuccess();
                    } else if (mView != null)
                        mView.loginFail(mContext.getResources().getString(R.string.management_login_error_tip));
                }, throwable -> {
                    logUtil.e(TAG, throwable.getMessage());
                    if (mView != null)
                        mView.loginFail(mContext.getResources().getString(R.string.management_login_error_tip));
                });
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void loadMiQRCode() {
        Subscription subscription = LoginRepository.getInstance().createMiQRCode()
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(s -> {
                    if (s == null) {
                        if (mView != null) mView.showRetry();
                    } else if (s.equals("")) {
                        ToastUtil.showCenter(FridgeApplication.getContext(), mContext.getResources().getString(R.string.management_mi_has_bind));
                        LoginPreference.getInstance().saveBindStatus(true);
                        RxBus.getInstance().post(BusEvent.MSG_MI_BIND);
                    } else {
                        logUtil.d(TAG, s);
                        if (mView != null) {
                            mView.showMiQRCode(LoginRepository.getInstance().getMiQRCode(s, (int) mContext.getResources().getDimension(R.dimen.px_x_220), (int) mContext.getResources().getDimension(R.dimen.px_x_220)));
                        }
                    }
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }
}
