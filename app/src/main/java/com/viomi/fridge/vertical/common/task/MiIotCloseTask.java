package com.viomi.fridge.vertical.common.task;

import android.os.AsyncTask;

import com.miot.api.MiotManager;
import com.viomi.fridge.vertical.common.util.logUtil;

/**
 * MiIot 关闭
 * Created by William on 2018/1/30.
 */
public class MiIotCloseTask extends AsyncTask<Void, Void, Integer> {
    private static final String TAG = MiIotCloseTask.class.getSimpleName();

    @Override
    protected Integer doInBackground(Void... voids) {
        return MiotManager.getInstance().close();
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
        do {
            int result = integer;
            logUtil.d(TAG, "MiIotClose result: " + result);
        }
        while (false);
    }
}