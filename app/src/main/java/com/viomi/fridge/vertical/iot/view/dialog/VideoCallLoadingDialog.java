package com.viomi.fridge.vertical.iot.view.dialog;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseDialog;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hailang on 2018/6/4 0004.
 */

public class VideoCallLoadingDialog extends BaseDialog {


    @BindView(R.id.iv_videocall_loading)
    public ImageView ivVideocallLoading;
    @BindView(R.id.tv_videocall_dialog)
    public TextView tvVideocallDialog;
    @BindView(R.id.layout_content)
    public LinearLayout layoutContent;

    public VideoCallLoadingDialog(Context context) {
        super(context, R.style.Dialog);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
        setContentView(View.inflate(context, R.layout.layout_videocall_loading, null), layoutParams);
        ButterKnife.bind(this);

    }


    @Override
    public void initView() {

    }
}
