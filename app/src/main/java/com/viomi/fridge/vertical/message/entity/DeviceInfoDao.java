package com.viomi.fridge.vertical.message.entity;

import android.content.Context;

/**
 * Created by young2 on 2017/6/21.
 */

public class DeviceInfoDao extends BaseDao<DeviceInfoMessage,Integer>{
        public DeviceInfoDao(Context context){
            super(context, DeviceInfoMessage.class);
        }
}
