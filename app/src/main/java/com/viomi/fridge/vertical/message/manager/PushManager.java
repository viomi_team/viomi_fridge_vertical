package com.viomi.fridge.vertical.message.manager;

import android.content.Context;
import android.text.TextUtils;

import com.umeng.message.PushAgent;
import com.umeng.message.common.inter.ITagManager;
import com.umeng.message.entity.UMessage;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.administration.model.entity.QRCodeBase;
import com.viomi.fridge.vertical.common.http.ApiClient;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.FileUtil;
import com.viomi.fridge.vertical.common.util.PreferenceHelper;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;
import com.viomi.fridge.vertical.message.entity.PushMsg;
import com.viomi.fridge.vertical.message.entity.PushSetting;
import com.viomi.fridge.vertical.message.entity.UmengInfo;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;

/**
 * 推送管理
 * Created by nanquan on 2018/2/9.
 */
public class PushManager {
    private static final String TAG = PushManager.class.getSimpleName();
    private static final boolean DEBUG = false;
    private static PushManager INSTANCE = null;
    private PushAgent mPushAgent;
    private PreferenceHelper mPreferenceHelper;
    private PushSetting mPushSetting;
    private Subscription mSubscription;

    public static PushManager getInstance() {
        synchronized (PushManager.class) {
            if (INSTANCE == null) {
                synchronized (PushManager.class) {
                    if (INSTANCE == null) {
                        INSTANCE = new PushManager();
                    }
                }
            }
        }
        return INSTANCE;
    }

    public void init() {
        Context context = FridgeApplication.getContext();
        mPreferenceHelper = new PreferenceHelper(context);
        mPushAgent = PushAgent.getInstance(context);
        mPushSetting = mPreferenceHelper.getPushSetting();
    }

    public void initSetting() {
        if (!FridgePreference.getInstance().getModel().equals(AppConstants.MODEL_JD)) {
            setActivityPushEnable(mPushSetting.isActivityEnable());
            setMallPushEnable(mPushSetting.isMallEnable());
        }
        setPushEnable(mPushSetting.isPushEnable());
        setNoPushTime(mPushSetting.getNoPushStartTime(), mPushSetting.getNoPushEndTime());
        singleUpgradePushSet();
    }

    /***
     * 设置活动消息推送
     *
     * @param enable 是否允许
     */
    public void setActivityPushEnable(boolean enable) {
        String topic_release = "viomi_activities";
        String topic_test = "test_viomi_activities";

        if (DEBUG) {
            unSetTag(topic_release);
            if (enable) {
                setTag(topic_test);
            } else {
                unSetTag(topic_test);
            }
        } else {
            unSetTag(topic_test);
            if (enable) {
                setTag(topic_release);
            } else {
                unSetTag(topic_release);
            }
        }

        if (enable != mPushSetting.isActivityEnable()) {
            mPushSetting.setActivityEnable(enable);
            mPreferenceHelper.putPushSetting(mPushSetting);
        }
    }

    /***
     * 设置商城消息推送
     *
     * @param enable 是否允许
     */
    public void setMallPushEnable(boolean enable) {
        logUtil.d(TAG, enable + "");
        Context context = FridgeApplication.getContext();
        QRCodeBase userInfo = (QRCodeBase) FileUtil.getObject(context, AppConstants.USER_INFO_FILE);
        if (userInfo != null && enable) {
            String alias = "fridge21_" + ToolUtil.getMiIdentification().getDeviceId() +
                    "_yid_" + userInfo.getLoginQRCode().getUserInfo().getAccount();
            logUtil.d(TAG, "Alias:" + alias);
            setAlias("YID", alias);
            mPushSetting.setMallAlias(alias);
            mPreferenceHelper.putPushSetting(mPushSetting);

            ApiClient.getInstance().getApiService().reportAlias(alias)
                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                    .onTerminateDetach()
                    .subscribe(result -> logUtil.d(TAG, "reportUserAlias,response=" + result),
                            throwable -> {
                                retry();
                                logUtil.e(TAG, "reportUserAlias,fail,msg=" + throwable.getMessage());
                            });
        } else {
            String alias = mPushSetting.getMallAlias();
            logUtil.d(TAG, "unsetAlias:" + alias);
            if (alias != null) {
                unSetAlias("YID", alias);
            }
        }

        if (enable != mPushSetting.isMallEnable()) {
            mPushSetting.setMallEnable(enable);
            mPreferenceHelper.putPushSetting(mPushSetting);
        }
    }

    /***
     * 设置设备消息推送
     *
     * @param enable 是否允许
     */
    public void setDevicePushEnable(boolean enable) {
        if (enable != mPushSetting.isDeviceEnable()) {
            mPushSetting.setDeviceEnable(enable);
            mPreferenceHelper.putPushSetting(mPushSetting);
        }
    }

    /***
     * 设置接收推送
     *
     * @param pushEnable 是否允许推送
     */
    public void setPushEnable(boolean pushEnable) {
        if (pushEnable) {
            mPushAgent.setNoDisturbMode(mPushSetting.getNoPushStartTime(), 0,
                    mPushSetting.getNoPushEndTime(), 0);
        } else {
            mPushAgent.setNoDisturbMode(0, 0, 0, 0);
        }
        if (mPushSetting.isPushEnable() != pushEnable) {
            mPushSetting.setPushEnable(pushEnable);
            mPreferenceHelper.putPushSetting(mPushSetting);
        }
    }

    /***
     * 设置接收推送的时段，不在该时段的推送消息会被缓存起来
     * 到了合适的时段再向 app 推送缓存的消息
     *
     * @param startHour 免打扰开始时间
     * @param endHour 免打扰结束时间
     */
    public void setNoPushTime(int startHour, int endHour) {
        if (mPushSetting.isPushEnable()) {
            mPushAgent.setNoDisturbMode(startHour, 0, endHour, 0);
        }
        if (mPushSetting.getNoPushStartTime() != startHour ||
                mPushSetting.getNoPushEndTime() != endHour) {
            mPushSetting.setNoPushStartTime(startHour);
            mPushSetting.setNoPushEndTime(endHour);
            mPreferenceHelper.putPushSetting(mPushSetting);
        }
    }

    /***
     * 单台设备升级推送
     */
    public void singleUpgradePushSet() {
        String alias = ToolUtil.getMiIdentification().getDeviceId();
        setAlias("DID", alias);
    }

    /***
     * 所有设备升级推送
     */
    public void upgradePushSet() {
        String topic_release = "viomi_device";
        String topic_test = "test_viomi_device";

        if (DEBUG) {
            unSetTag(topic_release);
            setTag(topic_test);
        } else {
            unSetTag(topic_test);
            setTag(topic_release);
        }
    }

    public void dispatchMessage(UMessage msg) {
        if (FridgePreference.getInstance().getModel().equals(AppConstants.MODEL_JD)) return;
        String message = msg.getRaw().toString();
        if (message == null || TextUtils.isEmpty(message)) return;
        logUtil.d(TAG, "push message" + message);
        // 转换为 PushMsg
        UmengInfo umengInfo = new UmengInfo();
        umengInfo.body = new UmengInfo.Body();
        try {
            JSONObject jsonObject = new JSONObject(message).optJSONObject("body");
            umengInfo.body.title = jsonObject.optString("title");
            umengInfo.body.text = jsonObject.optString("text");
            umengInfo.body.custom = jsonObject.optString("custom");
            if (TextUtils.isEmpty(umengInfo.body.custom)) {
                umengInfo.body.custom = jsonObject.optJSONObject("custom").toString();
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        PushMsg pushMsg = new PushMsg();
        pushMsg.title = umengInfo.body.title;
        pushMsg.content = umengInfo.body.text;
        if (umengInfo.body.custom != null) {
            JSONObject jsonObject;
            try {
                pushMsg.custom = umengInfo.body.custom;
                jsonObject = new JSONObject(umengInfo.body.custom);
                pushMsg.type = jsonObject.optString("type");
                JSONObject data = jsonObject.optJSONObject("data");
                pushMsg.link = data.optString("link");
                pushMsg.data = data.toString();
            } catch (Throwable e) {
                logUtil.e(TAG, "passer custom error!custom=" +
                        umengInfo.body.custom + ",errmsg=" + e.getMessage());
                e.printStackTrace();
            }
        }
        pushNotificationToStatusBar(pushMsg);
//---------------------------------------------------------------------------------------------------------------
//        // 判断推送类型并做对应处理
//        if (pushMsg.type == null) {
//            return;
//        }
//        if (isInAcceptTime()) {
//            switch (pushMsg.type) {
//                case PushMsg.PUSH_MESSAGE_TYPE_ACTIVITY:
//                    logUtil.d(TAG, "PUSH_MESSAGE_TYPE_ACTIVITY");
//                    if (isActivityPushEnable()) {
//                        logUtil.d(TAG, "success");
//                        // 显示新消息弹窗及首页红点
//                        RxBus.getInstance().post(BusEvent.MSG_MESSAGE_PUSH);
//                    }
//                    break;
//                case PushMsg.PUSH_MESSAGE_TYPE_ABNORMAL:
//                case PushMsg.PUSH_MESSAGE_TYPE_FILTER:
//                case PushMsg.PUSH_MESSAGE_TYPE_FOOD:
//                    if (isDevicePushEnable()) {
//                        // 显示新消息弹窗及首页红点
//                        RxBus.getInstance().post(BusEvent.MSG_MESSAGE_PUSH);
//                    }
//                    break;
//                case PushMsg.PUSH_MESSAGE_TYPE_UPDATE:
//                    // 系统升级
//                    // TODO
//                    break;
//            }
//        }
    }

    private void setTag(String tag) {
        logUtil.d(TAG, "setTag:" + tag);
        mPushAgent.getTagManager().addTags((isSuccess, result) ->
                logResult("setTag", isSuccess, result), tag);
    }

    private void unSetTag(String tag) {
        logUtil.d(TAG, "unSetTag:" + tag);
        mPushAgent.getTagManager().deleteTags((isSuccess, result) ->
                logResult("unSetTag", isSuccess, result), tag);
    }

    private void logResult(String method, final boolean isSuccess, final ITagManager.Result result) {
        if (isSuccess) {
            logUtil.d(TAG, method + " success");
        } else {
            String errorStr = "";
            if (result != null) {
                errorStr = result.jsonString;
            }
            logUtil.e(TAG, method + "fail!msg=" + errorStr);
        }

    }

    private void setAlias(String type, String alias) {
        logUtil.d(TAG, "setAlias:" + "type=" + type + ",alias=" + alias);
        mPushAgent.addAlias(alias, type, (isSuccess, message) -> {
            if (isSuccess) {
                logUtil.d(TAG, "setAlias success");
            } else {
                logUtil.e(TAG, "setAlias fail!msg=" + message);
            }
        });
    }

    private void unSetAlias(String type, String alias) {
        logUtil.d(TAG, "unSetAlias:" + "type=" + type + ",alias=" + alias);
        mPushAgent.deleteAlias(alias, type, (isSuccess, message) -> {
            if (isSuccess) {
                logUtil.d(TAG, "unSetAlias success");
            } else {
                logUtil.e(TAG, "unSetAlias fail!msg=" + message);
            }
        });
    }

    /***
     * @return 是否允许活动消息推送
     */
    public boolean isActivityPushEnable() {
        return mPushSetting.isActivityEnable();
    }

    /***
     * @return 是否允许商城消息推送
     */
    public boolean isMallPushEnable() {
        return mPushSetting.isMallEnable();
    }

    /***
     * @return 是否允许设备消息推送
     */
    public boolean isDevicePushEnable() {
        return mPushSetting.isDeviceEnable();
    }

    /***
     * @return 是否允许消息推送
     */
    public boolean isPushEnable() {
        return mPushSetting.isPushEnable();
    }

    /***
     * @return 免打扰开始时间
     */
    public int getNoPushStartTime() {
        return mPushSetting.getNoPushStartTime();
    }

    /***
     * @return 免打扰结束时间
     */
    public int getNoPushEndTime() {
        return mPushSetting.getNoPushEndTime();
    }

    /***
     * @return 是否在可推送时间内
     */
    public boolean isInAcceptTime() {
        boolean enable;
        int startHour;
        int endHour;
        PushSetting pushSetting = mPreferenceHelper.getPushSetting();
        enable = pushSetting.isPushEnable();
        startHour = pushSetting.getNoPushStartTime();
        endHour = pushSetting.getNoPushEndTime();
        if (!enable) {
            return true;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH", Locale.getDefault());
        int curHour = 0;
        try {
            curHour = Integer.parseInt(simpleDateFormat.format(new Date()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (startHour < endHour) {
            return !(curHour >= startHour && curHour < endHour);
        } else if (startHour > endHour) {
            return !(curHour >= startHour || curHour < endHour);
        } else {
            return curHour != startHour;
        }
    }

    /**
     * 推送通知到状态栏
     *
     * @param pushMsg 推送数据
     */
    public void pushNotificationToStatusBar(PushMsg pushMsg) {
        logUtil.d(TAG, "pushNotificationToStatusBar");
        if (pushMsg == null) return;
        RxBus.getInstance().post(BusEvent.MSG_PUSH_MESSAGE, pushMsg);
    }

    public void close() {
        INSTANCE = null;
        mPushAgent = null;
        mPreferenceHelper = null;
        mPushSetting = null;
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
    }

    /**
     * 上报 Alias 失败重试
     */
    private void retry() {
        mSubscription = Observable.timer(10, TimeUnit.SECONDS)
                .compose(RxSchedulerUtil.SchedulersTransformer2())
                .onTerminateDetach()
                .subscribe(aLong -> setMallPushEnable(mPushSetting.isMallEnable()),
                        throwable -> logUtil.e(TAG, throwable.getMessage()));
    }
}
