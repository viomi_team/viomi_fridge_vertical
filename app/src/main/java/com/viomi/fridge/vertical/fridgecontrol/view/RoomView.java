package com.viomi.fridge.vertical.fridgecontrol.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;

import java.lang.ref.WeakReference;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 仓室 View
 * Created by William on 2018/2/2.
 */
public class RoomView extends RelativeLayout {
    private Unbinder unbinder;
    private static final int MSG_TEMP_ICON_GONE = 1;
    private static final int MSG_MODE_ICON_GONE = 2;
    private ViewHandler mHandler;
    private Context mContext;

    @Nullable
    @BindView(R.id.room_type)
    TextView mNameTextView;// 仓室名称

    @Nullable
    @BindView(R.id.room_temp)
    TextView mTempTextView;// 温度

    @Nullable
    @BindView(R.id.room_unit)
    TextView mUnitTextView;// 单位

    @Nullable
    @BindView(R.id.room_alpha_flash)
    ImageView mAlphaImageView;// 渐变动画

    @Nullable
    @BindView(R.id.room_rotate_flash)
    ImageView mRotateImageView;// 旋转动画

    public RoomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    public RoomView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoomView(Context context) {
        this(context, null);
    }

    private void initView(Context context, AttributeSet attrs) {
        mContext = context;
        View view = LayoutInflater.from(context).inflate(R.layout.fridge_room_view, this);
        unbinder = ButterKnife.bind(this, view);
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.RoomView);
        String name = typedArray.getString(R.styleable.RoomView_name);
        typedArray.recycle();

        mHandler = new ViewHandler(this);
        if (mNameTextView != null) mNameTextView.setText(name);// 仓室名称
        if (mTempTextView != null)
            mTempTextView.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/DINCond-Medium.otf"));// 字体
    }

    /**
     * 显示温度
     *
     * @param enable: 仓室开关
     * @param temp:   温度
     */
    public void setTemp(boolean enable, int temp) {
        if (mTempTextView == null) return;
        if (!enable)
            mTempTextView.setText(mContext.getResources().getString(R.string.fridge_temp_default));
        else {
            if (FridgePreference.getInstance().getModel().equals(AppConstants.MODEL_X3) || FridgePreference.getInstance().getModel().equals(AppConstants.MODEL_X5)) {
                if (temp <= -24) mTempTextView.setText(String.valueOf(-24));
                else mTempTextView.setText(String.valueOf(temp));
            } else mTempTextView.setText(String.valueOf(temp));
        }
    }

    /**
     * 显示模式设置动画图标
     */
    public void setModeIconVisible(int type) {
        if (mRotateImageView != null && mRotateImageView.getVisibility() == View.VISIBLE)
            setTempIconGone();
        if (mAlphaImageView != null) {
            mAlphaImageView.setVisibility(View.VISIBLE);
            switch (type) {
                case 1: // 智能模式对应图标
                    mAlphaImageView.setImageResource(R.drawable.icon_fridge_flash_intelligence);
                    break;
                case 2: // 假日模式对应图标
                    mAlphaImageView.setImageResource(R.drawable.icon_fridge_flash_holiday);
                    break;
            }
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.5f, 1.0f);
            alphaAnimation.setDuration(1000);
            alphaAnimation.setInterpolator(new LinearInterpolator());
            alphaAnimation.setRepeatCount(-1);
            alphaAnimation.setRepeatMode(AlphaAnimation.REVERSE);
            alphaAnimation.setFillAfter(true);
            mAlphaImageView.setAnimation(alphaAnimation);
            if (mHandler != null) mHandler.sendEmptyMessageDelayed(MSG_MODE_ICON_GONE, 5000);
        }
    }

    /**
     * 显示温度设置动画图标
     */
    public void setTempIconVisible(int type) {
        if (mAlphaImageView != null && mAlphaImageView.getVisibility() == View.VISIBLE)
            setModeIconGone();
        if (mRotateImageView != null) {
            mRotateImageView.setVisibility(View.VISIBLE);
            switch (type) {
                case 1:
                    mRotateImageView.setImageResource(R.drawable.icon_fridge_flash_fan);
                    break;
                case 2:
                    mRotateImageView.setImageResource(R.drawable.icon_fridge_flash_snow);
                    break;
            }
            RotateAnimation rotateAnimation = new RotateAnimation(-360f, 360f, Animation.RELATIVE_TO_SELF, 0.5f,
                    Animation.RELATIVE_TO_SELF, 0.5f);
            rotateAnimation.setDuration(1500);
            rotateAnimation.setInterpolator(new LinearInterpolator());
            rotateAnimation.setRepeatCount(-1);
            rotateAnimation.setFillAfter(true);
            mRotateImageView.setAnimation(rotateAnimation);
            if (mHandler != null) mHandler.sendEmptyMessageDelayed(MSG_TEMP_ICON_GONE, 5000);
        }
    }

    public void setTempIconGone() {
        if (mRotateImageView != null) {
            if (mRotateImageView.getAnimation() != null) mRotateImageView.clearAnimation();
            mRotateImageView.setVisibility(View.GONE);
        }
    }

    public void setModeIconGone() {
        if (mAlphaImageView != null) {
            if (mAlphaImageView.getAnimation() != null) mAlphaImageView.clearAnimation();
            mAlphaImageView.setVisibility(View.GONE);
        }
    }

    /**
     * 适配 4 门布局
     */
    public void setIconLayout() {
        if (mAlphaImageView != null) {
            RelativeLayout.LayoutParams alphaLayoutParams = (LayoutParams) mAlphaImageView.getLayoutParams();
            alphaLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            alphaLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_START);
            alphaLayoutParams.setMargins(0, 40, 0, 0);
            mAlphaImageView.setLayoutParams(alphaLayoutParams);
        }

        if (mRotateImageView != null) {
            RelativeLayout.LayoutParams rotateLayoutParams = (LayoutParams) mRotateImageView.getLayoutParams();
            rotateLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            rotateLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_START);
            rotateLayoutParams.setMargins(0, 40, 0, 0);
            mRotateImageView.setLayoutParams(rotateLayoutParams);
        }
    }

    /**
     * 适配对开门布局
     */
    public void setOppositeLayout() {
        // 温度
        if (mTempTextView != null) {
            RelativeLayout.LayoutParams tempLayoutParams = (LayoutParams) mTempTextView.getLayoutParams();
            tempLayoutParams.removeRule(RelativeLayout.ABOVE);
            tempLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
            tempLayoutParams.setMargins(0, 10, 0, 10);
            mTempTextView.setLayoutParams(tempLayoutParams);
        }
        // 仓室名称
        if (mNameTextView != null) {
            RelativeLayout.LayoutParams typeLayoutParams = (LayoutParams) mNameTextView.getLayoutParams();
            typeLayoutParams.removeRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            typeLayoutParams.addRule(RelativeLayout.BELOW, mTempTextView.getId());
            typeLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            mNameTextView.setLayoutParams(typeLayoutParams);
        }
        // 单位
        if (mUnitTextView != null) {
            RelativeLayout.LayoutParams unitLayoutParams = (LayoutParams) mUnitTextView.getLayoutParams();
            unitLayoutParams.removeRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            unitLayoutParams.addRule(RelativeLayout.BELOW, mTempTextView.getId());
            mUnitTextView.setLayoutParams(unitLayoutParams);
        }
        // 渐变动画
        if (mAlphaImageView != null) {
            RelativeLayout.LayoutParams alphaLayoutParams = (LayoutParams) mAlphaImageView.getLayoutParams();
            alphaLayoutParams.removeRule(RelativeLayout.CENTER_VERTICAL);
            alphaLayoutParams.removeRule(RelativeLayout.ALIGN_PARENT_END);
            alphaLayoutParams.addRule(RelativeLayout.ABOVE, R.id.room_temp);
            alphaLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            mAlphaImageView.setLayoutParams(alphaLayoutParams);
        }
        // 旋转动画
        if (mRotateImageView != null) {
            RelativeLayout.LayoutParams rotateLayoutParams = (LayoutParams) mRotateImageView.getLayoutParams();
            rotateLayoutParams.removeRule(RelativeLayout.CENTER_VERTICAL);
            rotateLayoutParams.removeRule(RelativeLayout.ALIGN_PARENT_END);
            rotateLayoutParams.addRule(RelativeLayout.ABOVE, R.id.room_temp);
            rotateLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            mRotateImageView.setLayoutParams(rotateLayoutParams);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        unbinder.unbind();
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
        if (mContext != null) {
            mContext = null;
        }
    }

    private static class ViewHandler extends Handler {
        WeakReference<RoomView> weakReference;

        private ViewHandler(RoomView view) {
            this.weakReference = new WeakReference<>(view);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (this.weakReference != null) {
                RoomView view = this.weakReference.get();
                if (view != null) {
                    switch (msg.what) {
                        case MSG_TEMP_ICON_GONE:
                            view.setTempIconGone();
                            break;
                        case MSG_MODE_ICON_GONE:
                            view.setModeIconGone();
                            break;
                    }
                }
            }
        }
    }
}