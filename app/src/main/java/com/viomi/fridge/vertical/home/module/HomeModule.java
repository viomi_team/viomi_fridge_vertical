package com.viomi.fridge.vertical.home.module;

import com.viomi.fridge.vertical.common.scope.ActivityScoped;
import com.viomi.fridge.vertical.common.scope.FragmentScoped;
import com.viomi.fridge.vertical.home.contract.MainContract;
import com.viomi.fridge.vertical.home.presenter.MainPresenter;
import com.viomi.fridge.vertical.home.view.fragment.HomeFragment;
import com.viomi.fridge.vertical.iot.contract.IotTypeContract;
import com.viomi.fridge.vertical.iot.presenter.IotTypePresenter;
import com.viomi.fridge.vertical.iot.view.fragment.IotFragment;
import com.viomi.fridge.vertical.web.fragment.MallFragment;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * 主页 Module
 * Created by William on 2018/3/5.
 */
@Module
public abstract class HomeModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract IotFragment iotFragment();// 互联网家

    @FragmentScoped
    @ContributesAndroidInjector
    abstract HomeFragment homeFragment();// 主页

    @FragmentScoped
    @ContributesAndroidInjector
    abstract MallFragment mallFragment();// 商城

    @ActivityScoped
    @Binds
    abstract MainContract.Presenter mainPresenter(MainPresenter presenter);

    @ActivityScoped
    @Binds
    abstract IotTypeContract.Presenter iotScenePresenter(IotTypePresenter presenter);
}