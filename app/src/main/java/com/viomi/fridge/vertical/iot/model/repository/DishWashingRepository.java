package com.viomi.fridge.vertical.iot.model.repository;

import com.miot.api.MiotManager;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.http.ApiClient;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.iot.model.http.entity.RPCResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import rx.Observable;

/**
 * 洗碗机相关 Api
 * Created by William on 2018/7/5.
 */
public class DishWashingRepository {
    private static final String TAG = DishWashingRepository.class.getSimpleName();

    /**
     * GetProp
     */
    public static Observable<RPCResult> getProp(String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put("program");
            jsonArray.put("ldj_state");
            jsonArray.put("salt_state");
            jsonArray.put("left_time");
            jsonArray.put("bespeak_h");
            jsonArray.put("bespeak_min");
            jsonArray.put("bespeak_status");
            jsonArray.put("wash_process");
            jsonArray.put("wash_status");
            jsonArray.put("custom_program");
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
            logUtil.e(TAG, e.toString());
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 设置洗涤自定义模式
     */
    public static Observable<RPCResult> setCustomProgram(int custom_program, String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_custom_program");
            jsonObject.put("did", did);
            jsonObject.put("id", 12);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(custom_program);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            logUtil.e(TAG, e.toString());
            e.printStackTrace();
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 设置洗涤程序
     */
    public static Observable<RPCResult> setProgram(int wash_program, String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_program");
            jsonObject.put("did", did);
            jsonObject.put("id", 12);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(wash_program);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            logUtil.e(TAG, e.toString());
            e.printStackTrace();
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 设置启动或暂停
     */
    public static Observable<RPCResult> setWashAction(int wash_action, String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_wash_action");
            jsonObject.put("did", did);
            jsonObject.put("id", 12);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(wash_action);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            logUtil.e(TAG, e.toString());
            e.printStackTrace();
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 洗涤过程
     */
    public static String switchWashProcess(int wash_process, int wash_status, int bespeak_status) {
        String str = "";
        switch (wash_process) {
            case 0:
                str = bespeak_status == 0 ? FridgeApplication.getContext().getResources().getString(R.string.iot_dish_washing_free) : FridgeApplication.getContext().getResources().getString(R.string.iot_dish_washing_booking);
                break;
            case 1:
                str = wash_status == 1 ? FridgeApplication.getContext().getResources().getString(R.string.iot_dish_washing_preparing) : FridgeApplication.getContext().getResources().getString(R.string.iot_dish_washing_prepare_pause);
                break;
            case 2:
                str = wash_status == 1 ? FridgeApplication.getContext().getResources().getString(R.string.iot_dish_washing_main) : FridgeApplication.getContext().getResources().getString(R.string.iot_dish_washing_main_pause);
                break;
            case 3:
                str = wash_status == 1 ? FridgeApplication.getContext().getResources().getString(R.string.iot_dish_washing_washing) : FridgeApplication.getContext().getResources().getString(R.string.iot_dish_washing_washing_pause);
                break;
            case 4:
                str = wash_status == 1 ? FridgeApplication.getContext().getResources().getString(R.string.iot_dish_washing_poaching) : FridgeApplication.getContext().getResources().getString(R.string.iot_dish_washing_poach_pause);
                break;
            case 5:
                str = wash_status == 1 ? FridgeApplication.getContext().getResources().getString(R.string.iot_dish_washing_drying) : FridgeApplication.getContext().getResources().getString(R.string.iot_dish_washing_drying_pause);
                break;
            case 6:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_dish_washing_finish);
                break;
        }
        return str;
    }

    /**
     * 洗涤程序
     */
    public static String switchProgram(int program, int custom_program) {
        String str = "";
        switch (program) {
            case 0:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_dish_washing_mode_0);
                break;
            case 1:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_dish_washing_mode_1);
                break;
            case 2:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_dish_washing_mode_2);
                break;
            case 3:
                str = switchDefine(custom_program, FridgeApplication.getContext().getResources().getString(R.string.iot_dish_washing_mode_3));
                break;
            case 249:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_dish_washing_mode_249);
                break;
        }
        return str;
    }

    /**
     * 洗涤程序自定义
     */
    private static String switchDefine(int custom_program, String str) {
        switch (custom_program) {
            case 3:
                str = str + "-" + FridgeApplication.getContext().getResources().getString(R.string.iot_dish_washing_strong);
                break;
            case 4:
                str = str + "-" + FridgeApplication.getContext().getResources().getString(R.string.iot_dish_washing_prepare);
                break;
            case 5:
                str = str + "-" + FridgeApplication.getContext().getResources().getString(R.string.iot_dish_washing_glass);
                break;
            case 6:
                str = str + "-" + FridgeApplication.getContext().getResources().getString(R.string.iot_dish_washing_disinfect);
                break;
            case 250:
                str = str + "-" + FridgeApplication.getContext().getResources().getString(R.string.iot_dish_washing_dry);
                break;
        }
        return str;
    }
}
