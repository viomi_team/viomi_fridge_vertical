package com.viomi.fridge.vertical.fridgecontrol.defined.action;

import com.viomi.fridge.vertical.fridgecontrol.defined.ViomiDefined;
import com.viomi.fridge.vertical.fridgecontrol.defined.property.ComReceiveData;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetComReceiveData extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setComReceiveData.toActionType();

    public SetComReceiveData() {
        super(TYPE);

        super.addArgument(ComReceiveData.TYPE.toString());
    }
}