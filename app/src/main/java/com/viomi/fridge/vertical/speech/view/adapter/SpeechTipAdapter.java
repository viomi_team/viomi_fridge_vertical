package com.viomi.fridge.vertical.speech.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.viomi.fridge.vertical.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 语音指令提示适配器
 * Created by William on 2018/1/31.
 */
public class SpeechTipAdapter extends BaseAdapter {
    private List<String> mList;
    private Context mContext;

    public SpeechTipAdapter(Context context, List<String> list) {
        this.mList = list;
        this.mContext = context;
        if (mList == null) mList = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.view_holder_speech_tip, parent, false);
            viewHolder.mTextView = convertView.findViewById(R.id.holder_speech_tip);
            convertView.setTag(viewHolder);
        } else viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.mTextView.setText(mList.get(position));
        return convertView;
    }

    private class ViewHolder {
        private TextView mTextView;
    }
}