package com.viomi.fridge.vertical.iot.contract;

import com.miot.common.abstractdevice.AbstractDevice;
import com.viomi.fridge.vertical.common.callback.BasePresenter;

import java.util.List;

/**
 */
public interface LockContract {
    interface View {
        void makecall();

        void close();

        void ondoorUnlock();

        void onError();

        void onLockCaneraFound( String GwID,  String type);

        void initRtc(String userName, String password, String endPoint);
    }

    interface Presenter extends BasePresenter<View> {
        void getWlinkDeviceChildren(String gwID);

        void turnOffCamera();

        void unlock(String devID, String GwID, String pass, String type);

        void getWlinkDevices(String deviceId);
    }
}