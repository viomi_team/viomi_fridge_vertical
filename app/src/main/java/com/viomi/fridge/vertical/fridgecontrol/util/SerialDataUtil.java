package com.viomi.fridge.vertical.fridgecontrol.util;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.fridgecontrol.model.serial.entity.DeviceParams;

/**
 * 串口数据处理工具类
 * Created by William on 2018/1/5.
 */
public class SerialDataUtil {
    private static final String TAG = SerialDataUtil.class.getSimpleName();

    /**
     * 检验设置结果
     *
     * @param isRecovery: 是否恢复原来数据
     * @param paramsGet:  获取数据
     * @param paramsSet:  设置数据
     */
    public static boolean checkCMDResult(boolean isRecovery, DeviceParams paramsGet, DeviceParams paramsSet, String model) {
        logUtil.d(TAG, "Get = " + paramsGet.toString(model) + "\n Set = " + paramsSet.toString(model));
        if (paramsGet.toString(model).equals(paramsSet.toString(model))) return true;// 设置成功
        else { // 设置不成功，恢复
            if (isRecovery) {
                logUtil.e(TAG, "write fail, restore success");
                switch (model) {
                    case AppConstants.MODEL_X2:  // 双鹿 446
                        paramsSet.setMode(paramsGet.getMode());
                        paramsSet.setCold_temp_set(paramsGet.getCold_temp_set());
                        paramsSet.setFreezing_temp_set(paramsGet.getFreezing_temp_set());
                        paramsSet.setCold_switch(paramsGet.isCold_switch());
                        paramsSet.setOne_key_clean(paramsGet.isOne_key_clean());
                        break;
                    case AppConstants.MODEL_X3:  // 美菱 462
                        paramsSet.setMode(paramsGet.getMode());
                        paramsSet.setCold_temp_set(paramsGet.getCold_temp_set());
                        paramsSet.setFreezing_temp_set(paramsGet.getFreezing_temp_set());
                        paramsSet.setCold_switch(paramsGet.isCold_switch());
                        paramsSet.setQuick_cold(paramsGet.isQuick_cold());
                        paramsSet.setQuick_freeze(paramsGet.isQuick_freeze());
                        break;
                    case AppConstants.MODEL_X5:  // 美菱 521
                        paramsSet.setMode(paramsGet.getMode());
                        paramsSet.setCold_temp_set(paramsGet.getCold_temp_set());
                        paramsSet.setChangeable_temp_set(paramsGet.getChangeable_temp_set());
                        paramsSet.setFreezing_temp_set(paramsGet.getFreezing_temp_set());
                        paramsSet.setCold_switch(paramsGet.isCold_switch());
                        paramsSet.setChangeable_switch(paramsGet.isChangeable_switch());
                        paramsSet.setQuick_freeze(paramsGet.isQuick_freeze());
                        paramsSet.setOne_key_clean(paramsGet.isOne_key_clean());
                        paramsSet.setFresh_fruit(paramsGet.isFresh_fruit());
                        paramsSet.setRetain_fresh(paramsGet.isRetain_fresh());
                        paramsSet.setIced(paramsGet.isIced());
                        break;
                }
            } else {
                logUtil.e(TAG, "write fail, retry");
            }
            return false;
        }
    }

    /**
     * 用于双鹿 446 发送数据
     */
    public static void copyParams(DeviceParams params1, DeviceParams params2) {
        params1.setMode(params2.getMode());
        params1.setCold_temp_set(params2.getCold_temp_set());
        params1.setFreezing_temp_set(params2.getFreezing_temp_set());
        params1.setCold_switch(params2.isCold_switch());
        params1.setRcf_forced(params2.isRcf_forced());
        params1.setRc_forced_frost(params2.isRc_forced_frost());
        params1.setFc_forced_frost(params2.isFc_forced_frost());
        params1.setCommodity_inspection(params2.isCommodity_inspection());
        params1.setTime_cut(params2.isTime_cut());
        params1.setQuick_cold(params2.isQuick_cold());
        params1.setQuick_freeze(params2.isQuick_freeze());
    }

    /**
     * 获取位校验和
     *
     * @param data:   解析后的数据
     * @param length: 一共需要发送的位数
     */
    public static int getCheckSum(int data[], int length) {
        if (data == null || data.length == 0 || data.length < length) return 0;
        int sum = 0;
        for (int i = 0; i < length; i++) {
            sum += data[i];
        }
        return 0xFF & sum;
    }

    /**
     * 获取前多少位校验和
     */
    public static int getCheckSum(int[] data, int start, int length) {
        if (data == null || data.length == 0 || data.length < length + start) {
            return 0;
        }
        int sum = 0;
        for (int i = 0; i < length; i++) {
            sum += data[start + i];
        }
        return 0xff & sum;
    }

    /**
     * 字节数组转换为 16 进制字符串
     *
     * @param byteArray: 字节数据
     */
    public static String bytesToHexString(int[] byteArray) {
        if (byteArray == null) return "";
        int size = byteArray.length;
        StringBuilder sb = new StringBuilder(size);
        String sTemp;
        for (int aByteArray : byteArray) {
            sTemp = Integer.toHexString(0xFF & aByteArray);
            if (sTemp.length() < 2)
                sb.append(0);
            sb.append(sTemp.toUpperCase());
            sb.append(" ");
        }
        return sb.toString();
    }

    /**
     * 字节数组转换成 16 进制字符串
     *
     * @param bArray: 字节数组
     */
    public static String bytesToHexString(byte[] bArray) {
        if (bArray == null) return null;
        int size = bArray.length;
        StringBuilder sb = new StringBuilder(size);
        String sTemp;
        for (byte aBArray : bArray) {
            sTemp = Integer.toHexString(0xFF & aBArray);
            if (sTemp.length() < 2)
                sb.append(0);
            sb.append(sTemp.toUpperCase());
        }
        return sb.toString();
    }

    /**
     * 根据给定长度把字节数组转换成 16 进制字符串
     *
     * @param bArray: 字节数据
     * @param length: 长度
     */
    public static String bytesToHexString(int[] bArray, int length) {
        if (bArray == null) return null;
        int size = bArray.length;
        if (size > length) size = length;
        StringBuilder sb = new StringBuilder(size);
        String sTemp;
        for (int i = 0; i < size; i++) {
            sTemp = Integer.toHexString(0xFF & bArray[i]);
            if (sTemp.length() < 2)
                sb.append(0);
            sb.append(sTemp.toUpperCase());
            sb.append(" ");
        }
        return sb.toString();
    }
}