package com.viomi.fridge.vertical.cookbook.model;

import java.io.Serializable;

/**
 * 菜谱方法
 * Created by nanquan on 2018/2/27.
 */
public class CookMenuMethod implements Serializable{
    private String img;
    private String step;
    private int position;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}