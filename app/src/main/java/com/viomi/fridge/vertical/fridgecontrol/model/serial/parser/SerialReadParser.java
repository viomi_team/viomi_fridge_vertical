package com.viomi.fridge.vertical.fridgecontrol.model.serial.parser;


import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;
import com.viomi.fridge.vertical.fridgecontrol.model.repository.FridgeRepository;
import com.viomi.fridge.vertical.fridgecontrol.model.serial.entity.DeviceParams;
import com.viomi.fridge.vertical.fridgecontrol.util.CRC16Util;
import com.viomi.fridge.vertical.fridgecontrol.util.SerialDataUtil;

/**
 * 串口接收数据解析
 * Created by William on 2018/1/5.
 */
public class SerialReadParser {
    private static final String TAG = SerialReadParser.class.getSimpleName();
    private static int mCount = 0;

    /**
     * 接收数据后进行解析
     *
     * @param data:      串口接收数据
     * @param paramsGet: 冰箱读取数据实例
     * @param paramsSet: 冰箱设置数据实例
     */
    public static boolean parse(int[] data, DeviceParams paramsGet, DeviceParams paramsSet, String deviceModel) {
        if (paramsGet == null) {
            logUtil.e(TAG, "receive params is null");
            return false;
        }
        if (data == null) {
            logUtil.e(TAG, "receive data is null");
            return false;
        }
        int receiveLength = data.length;// 接收字节长度
        // 协议定的接收字节长度
        int length;
        switch (deviceModel) {
            case AppConstants.MODEL_X2: // 双鹿 446
                length = AppConstants.X2_READ_BYTE_COUNT;
                break;
            case AppConstants.MODEL_X3: // 美菱 462
                length = AppConstants.X3_READ_BYTE_COUNT;
                break;
            case AppConstants.MODEL_X4: // 雪祺 450
            case AppConstants.MODEL_JD: // 雪祺 450 京东定制
                length = AppConstants.X4_READ_BYTE_COUNT;
                break;
            case AppConstants.MODEL_X5: // 美菱 521
                length = AppConstants.X5_READ_BYTE_COUNT;
                break;
            default:
                length = AppConstants.X1_READ_BYTE_COUNT;
                break;
        }
        if (receiveLength < length) {
            logUtil.d(TAG, "receive data length is not right");
            return false;
        }
        int i = 0;// 下标
        // 校验返回数据头部
        switch (deviceModel) {
            case AppConstants.MODEL_X1:
                if (data[0] != (0xFF & AppConstants.X1_SERIAL_DATA_START)) { // 开始位与协议不一致
                    logUtil.e(TAG, "receive data header is error, header = " + data[0]);
                    return false;
                }
                break;
            case AppConstants.MODEL_X2: // 双鹿 446
                if (data[0] == (0xFF & AppConstants.X2_SERIAL_DATA_START)
                        && data[1] == (0xFF & AppConstants.X2_SERIAL_DATA_START)
                        && data[AppConstants.X2_READ_BYTE_COUNT - 1] == (0xff & AppConstants.X2_SERIAL_DATA_END)
                        && data[AppConstants.X2_READ_BYTE_COUNT - 2] == (0xff & AppConstants.X2_SERIAL_DATA_END)) { // 校验包头和包尾各两位
                    logUtil.d(TAG, "receive data header is correct");
                } else {
                    logUtil.e(TAG, "receive data header is error, header =" + data[0]);
                    return false;
                }
                break;
            case AppConstants.MODEL_X3: // 美菱 462
                if (data[0] == (0xff & AppConstants.X3_SERIAL_DATA_START_0) && data[1] == (0xff & AppConstants.X3_SERIAL_DATA_START_1)) { // 校验开始位两位
                    logUtil.d(TAG, "receive data header is correct");
                } else {
                    logUtil.e(TAG, "receive data header is error! header0=" + data[0] + ",header1=" + data[1]);
                    return false;
                }
                break;
            case AppConstants.MODEL_X5: // 美菱 521
                if (data[0] == (0xff & AppConstants.X5_SERIAL_DATA_START_0) && data[1] == (0xff & AppConstants.X5_SERIAL_DATA_START_1)) { // 检验开始位两位
                    logUtil.d(TAG, "receive data header is correct");
                } else {
                    logUtil.e(TAG, "receive data header is error! header0=" + data[0] + ",header1=" + data[1]);
                    return false;
                }
                break;
        }
        // 开始解析
        int checkSum;
        switch (deviceModel) {
            case AppConstants.MODEL_X1: // 三门冰箱
                checkSum = SerialDataUtil.getCheckSum(data, AppConstants.X1_READ_BYTE_COUNT - 1);
                if (checkSum != (0xFF &data[AppConstants.X1_READ_BYTE_COUNT - 1])) {
                    logUtil.e(TAG, "receive data parse error, checkSum = " + (0xFF &data[AppConstants.X1_READ_BYTE_COUNT - 1]) + ",crc = " + checkSum);
                    return false;
                }
                i++;
                paramsGet.setMode(data[i]);// 冰箱工作模式

                if (paramsGet.getMode() == AppConstants.MODE_QUICK_COLD) { // 速冷
                    paramsGet.setQuick_cold(true);
                    paramsGet.setQuick_freeze(false);
                    paramsGet.setMode(AppConstants.MODE_NULL);
                } else if (paramsGet.getMode() == AppConstants.MODE_QUICK_FREEZE) {  // 速冻
                    paramsGet.setQuick_cold(false);
                    paramsGet.setQuick_freeze(true);
                    paramsGet.setMode(AppConstants.MODE_NULL);
                } else {
                    paramsGet.setQuick_cold(false);
                    paramsGet.setQuick_freeze(false);
                }
                i++;
                paramsGet.setCold_temp_set(data[i] - 50);// 冷藏室温度和开关

                if (paramsGet.getCold_temp_set() == AppConstants.ROOM_CLOSED_TEMP)
                    paramsGet.setCold_switch(false);
                else paramsGet.setCold_switch(true);
                i++;
                paramsGet.setChangeable_temp_set(data[i] - 50);// 变温室温度和开关

                if (paramsGet.getChangeable_temp_set() == AppConstants.ROOM_CLOSED_TEMP)
                    paramsGet.setChangeable_switch(false);
                else paramsGet.setChangeable_switch(true);
                i++;// 冷冻室温度

                paramsGet.setFreezing_temp_set(data[i] - 50);
                i++;
                int status1 = data[i];// 状态

                paramsGet.setOne_key_clean(isOneKeyCleanWorking(status1));// 一键净化

                paramsGet.setRcf_forced(isRCFForcedStart(status1));// 强制启动

                paramsGet.setFc_forced_frost(isForcedFrost(status1));// 强制化霜测试

                paramsGet.getDeviceError().setError_fc_fan(isFanAlarm(status1));// 风机报警

                paramsGet.getDeviceError().setRc_door_open_alarm(isRCDoorOpenAlarm(status1));// 冷藏室门开报警

                i++;
                if (data[i] == 0xA0)
                    paramsGet.getDeviceError().setError_communication(false);// 通讯接收正常
                else if (data[i] == 0x05)
                    paramsGet.getDeviceError().setError_communication(true);// 通讯故障
                i++;
                if (data[i] == 0xFF) paramsGet.getDeviceError().setError_rc_sensor(true);// 冷藏传感器故障
                else {  // 冷藏室实际温度
                    paramsGet.getDeviceError().setError_rc_sensor(false);
                    paramsGet.setCold_temp_real(data[i] - 50);
                }
                i++;
                if (data[i] == 0xFF) paramsGet.getDeviceError().setError_cc_sensor(true);// 变温传感器故障
                else {  // 变温室实际温度
                    paramsGet.getDeviceError().setError_cc_sensor(false);
                    paramsGet.setChangeable_temp_real(data[i] - 50);
                }
                i++;
                if (data[i] == 0xFF) paramsGet.getDeviceError().setError_fc_sensor(true);// 冷冻传感器故障
                else {  // 冷冻室实际温度
                    paramsGet.getDeviceError().setError_fc_sensor(false);
                    paramsGet.setFreezing_temp_real(data[i] - 50);
                }
                i++;
                if (data[i] == 0xFF)
                    paramsGet.getDeviceError().setError_indoor_sensor(true);// 环境温度传感器故障
                else {  // 环境（室内）温度
                    paramsGet.getDeviceError().setError_indoor_sensor(false);
                    paramsGet.setIndoor_temp(data[i] - 50);
                }
                i++;
                if (data[i] == 0xFF)
                    paramsGet.getDeviceError().setError_fc_sensor(true);// 冷冻化霜传感器故障
                else {
                    paramsGet.getDeviceError().setError_fc_sensor(false);
                    paramsGet.setFc_evaporator_temp(data[i] - 50);
                }
                i++;
//                paramsGet.setHumidity(data[i] & 0xFF);// 湿度

                i++;
                int check1 = data[i];// 查询数据 1

                paramsGet.setFc_evaporator_heat(isFCEvaporatorHeat(check1));// 冷冻化霜加热状态

                i++;
                int check2 = data[i];// 查询数据 2

                i++;
//                paramsGet.setVersion(((byte) data[i]) & 0x0F);// 版本

//                paramsGet.setModel((((byte) data[i]) & ((byte) 0xf0)) / 16);// 型号

                i++;
                paramsGet.setCrc(data[i]);// 检验

                paramsGet.setErrorCode();// 设置故障码

                return true;

            case AppConstants.MODEL_X2: // 双鹿 446
                checkSum = SerialDataUtil.getCheckSum(data, 2, AppConstants.X2_READ_BYTE_COUNT - 4 - 1);
                if (checkSum != (0xFF &data[AppConstants.X2_READ_BYTE_COUNT - 2 - 1])) {
                    logUtil.e(TAG, "parser,crc error !crc=" + (0xFF &data[AppConstants.X2_READ_BYTE_COUNT - 2 - 1]) + ",checkSum=" + checkSum);
                    return false;
                }
                // 帧头固定
                i++;
                i++;
                // 冷藏感温头温度值（冷藏室实际温度）
                i++;
                paramsGet.setCold_temp_real((data[i] - 80) / 2);
                // 冷藏化霜感温头温度值（冷藏化霜温度）
                i++;
                paramsGet.setRc_evaporator_temp((data[i] - 80) / 2);
                // 冷冻感温头温度值（冷冻室设置温度）
                i++;
                paramsGet.setFreezing_temp_real((data[i] - 80) / 2);
                // 变温感温头温度值（变温室设置温度）
                i++;
//                paramsGet.setChangeable_temp_set((data[i] - 80) / 2);
                // 环境感温头温度值（室内温度）
                i++;
                paramsGet.setIndoor_temp((data[i] - 80) / 2);
                // 冷冻化霜感温头温度值（冷冻化霜温度）
                i++;
                paramsGet.setFc_evaporator_temp((data[i] - 80) / 2);
                // 预留
                i++;
                // 预留
                i++;
                // 设备状态信息
                i++;
                paramsGet.setCommodity_inspection((data[i] & 0x02) == 0x02);// 设备状态，商检
                if ((paramsGet.getDeviceError().isRc_door_open_alarm() != ((data[i] & 0x20) == 0x20)) ||
                        paramsGet.getDeviceError().isFc_door_open_alarm() != ((data[i] & 0x80) == 0x80)) {
                    paramsGet.getDeviceError().setRc_door_open_alarm((data[i] & 0x20) == 0x20);// 冷藏室门开报警
//                paramsGet.getDeviceError().setCc_door_open_alarm((data[i] & 0x40) == 0x40);// 变温室门开报警
                    paramsGet.getDeviceError().setFc_door_open_alarm((data[i] & 0x80) == 0x80);// 冷冻室门开报警
                    if (paramsGet.getDeviceError().isRc_door_open_alarm() || paramsGet.getDeviceError().isFc_door_open_alarm()) { // 门开报警
                        RxBus.getInstance().post(BusEvent.MSG_OPEN_ALARM_DISPLAY);
                    } else RxBus.getInstance().post(BusEvent.MSG_OPEN_ALARM_DISMISS);
                }
                // 工作状态信息
                i++;
                paramsGet.setRc_forced_frost((data[i] & 0x10) == 0x10);// 冷藏化霜状态
                paramsGet.setFc_forced_frost((data[i] & 0x20) == 0x20);// 冷冻化霜状态
                // 故障信息
                i++;
//                paramsGet.getDeviceError().setError_cc_sensor((data[i] & 0x01) == 0x01);// 变温传感器故障
                paramsGet.getDeviceError().setError_rc_sensor((data[i] & 0x02) == 0x02);// 冷藏传感器故障
                paramsGet.getDeviceError().setError_rc_defrost_sensor((data[i] & 0x04) == 0x04);// 冷藏化霜传感器故障
                paramsGet.getDeviceError().setError_fc_defrost_sensor((data[i] & 0x08) == 0x08);// 冷冻化霜传感器故障
                paramsGet.getDeviceError().setError_indoor_sensor((data[i] & 0x10) == 0x10);// 环境传感器故障
                paramsGet.getDeviceError().setError_fc_sensor((data[i] & 0x40) == 0x40);// 冷冻传感器故障
                // 故障信息 2
                i++;
                paramsGet.getDeviceError().setError_fan_door((data[i] & 0x02) == 0x02);// 风门故障
                paramsGet.getDeviceError().setError_cc_fan((data[i] & 0x04) == 0x04);// 冷凝风扇故障故障
                paramsGet.getDeviceError().setError_fc_fan((data[i] & 0x10) == 0x10);// 冷冻风扇故障故障
                paramsGet.getDeviceError().setError_rc_fan((data[i] & 0x40) == 0x40);// 冷藏风扇故障

                paramsGet.setErrorCode();// 设置故障码
                // 预留
                i++;
                // 检验
                i++;
                paramsGet.setCrc(data[i]);

                return true;

            case AppConstants.MODEL_X3: // 美菱 462
                checkSum = SerialDataUtil.getCheckSum(data, 0, AppConstants.X3_READ_BYTE_COUNT - 1);
                if (checkSum != (0xFF &data[AppConstants.X3_READ_BYTE_COUNT - 1])) {
                    logUtil.e(TAG, "parser,crc error !crc=" +(0xFF &data[AppConstants.X3_READ_BYTE_COUNT - 1])+ ",checkSum=" + checkSum);
                    return false;
                }
                // 通信起始命令 data1
                i++;
                // 冰箱运行状态 1（运行模式类） data2
                i++;
                if ((data[i] & 0x01) == 0x01) { // 智能模式
                    paramsGet.setMode(AppConstants.MODE_SMART);
                } else if ((data[i] & 0x04) == 0x04) { // 假日模式
                    paramsGet.setMode(AppConstants.MODE_HOLIDAY);
                } else { // 无模式
                    paramsGet.setMode(AppConstants.MODE_NULL);
                }
                paramsGet.setQuick_freeze((data[i] & 0x10) == 0x10);// 速冻
                paramsGet.setQuick_cold((data[i] & 0x20) == 0x20);// 速冷
                paramsGet.setCold_switch(!((data[i] & 0x02) == 0x02));// 冷藏室开关
                // 冰箱运行状态 2  data3
                i++;
                paramsGet.setFlipping_beam_reset_mode((data[i] & 0x10) == 0x10);// 翻转梁加热丝关闭模式
                // 冰箱运行状态 3（开关状态类） data4
                i++;
                if ((paramsGet.getDeviceError().isRc_door_open_alarm() != ((data[i] & 0x01) == 0x01)) ||
                        (paramsGet.getDeviceError().isFc_door_open_alarm() == (((data[i] & 0x02) == 0) && ((data[i] & 0x04) == 0)))) {
                    paramsGet.getDeviceError().setRc_door_open_alarm((data[i] & 0x01) == 0x01);// 冷藏门开关状态位
                    paramsGet.getDeviceError().setFc_door_open_alarm(!(((data[i] & 0x02) == 0) && ((data[i] & 0x04) == 0)));// 冷冻门开关状态位

                    if (paramsGet.getDeviceError().isRc_door_open_alarm() || paramsGet.getDeviceError().isFc_door_open_alarm()) { // 门开报警
                        RxBus.getInstance().post(BusEvent.MSG_OPEN_ALARM_DISPLAY);
                    } else RxBus.getInstance().post(BusEvent.MSG_OPEN_ALARM_DISMISS);
                }

                // 冰箱冷藏室设定温度值（设定温度值整数位 + 100） data5
                i++;
                paramsGet.setCold_temp_set(data[i] - 100);
                // 冰箱变温室设定温度值（设定温度值整数位 + 100） data6
                i++;
                paramsGet.setChangeable_temp_set(0);
                // 冰箱冷冻室设定温度值（设定温度值整数位 + 100） data7
                i++;
                paramsGet.setFreezing_temp_set(data[i] - 100);
                // 冰箱冷藏室设定温度值小数位（设定温度值小数位 * 10 + 10） data8
                i++;
                // 冰箱变温室设定温度值小数位（设定温度值小数位 * 10 + 10） data9
                i++;
                // 冰箱冷冻室设定温度值小数位（设定温度值小数位 * 10 + 10） data10
                i++;
                // 冰箱冷藏室传感器温度值（温度值 + 100，此值若 = 255 表示该传感器故障） data11
                i++;
                paramsGet.setCold_temp_real(data[i] - 100);
                paramsGet.getDeviceError().setError_rc_sensor(data[i] == 0xff);
                // 冰箱变温室传感器温度值（温度值 + 100，此值若 = 255 表示该传感器故障） data12
                i++;
                paramsGet.setChangeable_temp_real(data[i] - 100);
//            if(data[i]==0xff){
//                deviceError.error_ccroom_sencor=true;
//            }
                // 冰箱冷冻室传感器温度值（温度值 + 100，此值若 = 255 表示该传感器故障） data13
                i++;
                paramsGet.setFreezing_temp_real(data[i] - 100);
                paramsGet.getDeviceError().setError_fc_sensor(data[i] == 0xff);
                // 冰箱环境温度传感器温度值（传送实际温度值（0~50），若环温低于 0 度，此值为 0。此值若 = 70 表示该传感器故障） data14
                i++;
                paramsGet.setIndoor_temp(data[i]);
                paramsGet.getDeviceError().setError_indoor_sensor(data[i] == 70);
                // 冰箱冷藏室传感器温度值小数位（温度值小数位 * 10 + 10） data15
                i++;
                // 冰箱变温室传感器温度值小数位（温度值小数位 * 10 + 10） data16
                i++;
                // 冰箱冷冻室传感器温度值小数位（温度值小数位 * 10 + 10） data17
                i++;
                // 冰箱环境温度传感器温度值小数位（温度值小数位 * 10 + 10） data18
                i++;
                // 冰箱运行状态 4 data19
                i++;
                paramsGet.setFlipping_beam_status((data[i] & 0x80) == 0x80);// 翻转梁加热丝
                // 冰箱运行状态 5 data20
                i++;
                // 保留 data21
                i++;
                // 保留 data22
                i++;
                // 变频档位（范围：0～99）data23
                i++;
                // 冰箱冷冻室蒸发传感器温度值（温度值 + 100，此值若 = 255 表示该传感器故障） data24
                i++;
                paramsGet.setFc_evaporator_temp(data[i] - 100);
                paramsGet.getDeviceError().setError_fc_defrost_sensor(data[i] == 0xff);
                // 冰箱运行状态 7（冰箱故障判断类 1） data25
                i++;
//            if((data[i]&0x01)==0x01) {
//                deviceError.error_traffic=true;
//            }else {
//                deviceError.error_traffic=false;
//            }
                paramsGet.getDeviceError().setError_fc_fan((data[i] & 0x02) == 0x02);
                // 冰箱运行状态 8（冰箱故障判断类） data26
                i++;

                i += 12;// 保留,data27~data38

                i++;// 冰箱型号预定义，值为零（DATA39 = 0）,data39

                i++;// 冰箱型号（冰箱产品代号，高位放在DATA40字节,低位放在DATA41字节）,data40
                paramsGet.setData40(data[i]);

                i++;//,data41
                paramsGet.setData41(data[i]);

                i += 5;// 保留,data42~data46

                i++;//,data47

                paramsGet.setErrorCode();// 设置故障码
//                PushMsg pushMsg = new PushMsg();
//                pushMsg.title = DataProcessUtil.bytesToHexString(data);
//                pushMsg.content =info.toShortString();
//                pushMsg.type=PushMsg.PUSH_MESSAGE_TYPE_ADVERT;
//                PushManager.getInstance().pushNotificationToStatusBar(pushMsg);
                return true;

            case AppConstants.MODEL_X4: // 雪祺 450
            case AppConstants.MODEL_JD: // 雪祺 450 京东定制
                if (mCount <= 6) mCount++;
                checkSum = SerialDataUtil.getCheckSum(data, AppConstants.X4_READ_BYTE_COUNT - 1);
                if (checkSum != (0xFF &data[AppConstants.X4_READ_BYTE_COUNT - 1])) {
                    logUtil.e(TAG, "receive data parse error, checkSum = " + (0xFF &data[AppConstants.X4_READ_BYTE_COUNT - 1]) + ",crc = " + checkSum);
                    return false;
                }
                // 冷冻室实际温度（传感器温度值 + 50）
                i++;
                paramsGet.setFreezing_temp_real(data[i] - 50);

                i++;
                if ((data[i] & 0x01) == 0x01) {
                    paramsGet.setMode(AppConstants.MODE_SMART);// 假日模式
                } else {
                    paramsGet.setMode(AppConstants.MODE_NULL);
                }
                paramsGet.setQuick_cold((data[i] & 0x02) == 0x02);// 速冷
                // 冷藏室实际温度（传感器温度值 + 50）
                i++;
                paramsGet.setCold_temp_real(data[i] - 50);
                // 压缩机累计运行时间（以小时为单位发送）
                i++;
                paramsGet.setCompressor_run_time(data[i]);
                if (mCount >= 6) FridgePreference.getInstance().saveRunTime(data[i]);

                // 化霜传感器温度（传感器温度值 + 50）
                i++;
                paramsGet.setFc_evaporator_temp(data[i] - 50);

                i++;
                int error = data[i] & 0x0F;
                // 先复位
                paramsGet.getDeviceError().setError_fc_sensor(false);
                paramsGet.getDeviceError().setError_indoor_sensor(false);
                paramsGet.getDeviceError().setError_rc_sensor(false);
                paramsGet.getDeviceError().setError_fc_defrost_sensor(false);
                paramsGet.getDeviceError().setError_defrost(false);
                paramsGet.getDeviceError().setError_fc_fan(false);
                paramsGet.getDeviceError().setError_cc_fan(false);
                if (((data[i] & 0x40) == 0x40) && error > 0) { // 有故障
                    switch (error) {
                        case 1:
                            paramsGet.getDeviceError().setError_fc_sensor(true);// 冷冻传感器故障
                            break;
                        case 2:
                            paramsGet.getDeviceError().setError_indoor_sensor(true);// 环温传感器故障
                            break;
                        case 3:
                            paramsGet.getDeviceError().setError_rc_sensor(true);// 冷藏传感器 1 故障
                            break;
                        case 4:
                            paramsGet.getDeviceError().setError_rc_sensor(true);// 冷藏传感器 2 故障
                            break;
                        case 5:
                            paramsGet.getDeviceError().setError_fc_defrost_sensor(true);// 化霜传感器故障
                            break;
                        case 6:
                            paramsGet.getDeviceError().setError_defrost(true);// 化霜故障
                            break;
                        case 7:
                            paramsGet.getDeviceError().setError_fc_fan(true);// 冷冻风机故障
                            break;
                        case 8:
                            paramsGet.getDeviceError().setError_cc_fan(true);// 冷凝风机故障
                            break;
//                    case 9:
//                        params.getDeviceError().setError_communication(true);// 通讯故障
//                        break;
                    }
                }
                if ((paramsGet.getDeviceError().isRc_door_open_alarm() != ((data[i] & 0x10) == 0x10)) ||
                        paramsGet.getDeviceError().isFc_door_open_alarm() != ((data[i] & 0x20) == 0x20)) {
                    paramsGet.getDeviceError().setRc_door_open_alarm((data[i] & 0x10) == 0x10);// 冷藏室门状态标志
                    paramsGet.getDeviceError().setFc_door_open_alarm((data[i] & 0x20) == 0x20);// 冷冻室门状态标志

                    if (paramsGet.getDeviceError().isRc_door_open_alarm() || paramsGet.getDeviceError().isFc_door_open_alarm()) { // 门开报警
                        RxBus.getInstance().post(BusEvent.MSG_OPEN_ALARM_DISPLAY);
                    } else RxBus.getInstance().post(BusEvent.MSG_OPEN_ALARM_DISMISS);
                }

                i++;
                if (mCount >= 6) {
                    if (((data[i] & 0x02) == 0x02) && paramsSet.isQuick_freeze()) { // 退出速冻
                        FridgeRepository.getInstance().setSmartFreeze(false, null);
                    }
                }

                i++;
                i++;
                i++;
                i++;
                paramsGet.setCrc(data[i]);// 检验

                paramsGet.setErrorCode();// 设置故障码

                // 存在故障，退出所有模式
//                if (paramsGet.getError() != 0) {
//                    FridgeRepository.getInstance().quitMode();// 退出模式
//                }
                return true;

            case AppConstants.MODEL_X5: // 美菱 521
                checkSum = SerialDataUtil.getCheckSum(data, 0, AppConstants.X5_READ_BYTE_COUNT - 1);
                if (checkSum != (0xFF &data[AppConstants.X5_READ_BYTE_COUNT - 1])) {
                    logUtil.e(TAG, "parser,crc error !crc=" +  (0xFF &data[AppConstants.X5_READ_BYTE_COUNT - 1]) + ",checkSum=" + checkSum);
                    return false;
                }
                // 通信起始命令 DATA1
                i++;
                // 冰箱运行状态1（运行模式类）DATA2
                i++;
                if ((data[i] & 0x01) == 0x01) { // 智能模式
                    paramsGet.setMode(AppConstants.MODE_SMART);
                } else if ((data[i] & 0x04) == 0x04) { // 假日模式
                    paramsGet.setMode(AppConstants.MODE_HOLIDAY);
                } else { // 无模式
                    paramsGet.setMode(AppConstants.MODE_NULL);
                }
                paramsGet.setQuick_freeze((data[i] & 0x10) == 0x10);// 速冻
                paramsGet.setCold_switch(!((data[i] & 0x02) == 0x02));// 冷藏室开关
                paramsGet.setChangeable_switch(!((data[i] & 0x08) == 0x08));// 变温室开关
                paramsGet.setOne_key_clean((data[i] & 0x80) == 0x80);// 一键净化
                // 冰箱运行状态 2  DATA3
                i++;
                paramsGet.setFresh_fruit((data[i] & 0x01) == 0x01);// 鲜果
                paramsGet.setRetain_fresh((data[i] & 0x02) == 0x02);// 0 度保鲜
                paramsGet.setIced((data[i] & 0x04) == 0x04);// 冰镇
                paramsGet.setFlipping_beam_reset_mode((data[i] & 0x10) == 0x10);// 翻转梁加热丝关闭模式
                // 冰箱运行状态 3（开关状态类） DATA4
                i++;
                if ((paramsGet.getDeviceError().isRc_door_open_alarm() != ((data[i] & 0x01) == 0x01)) ||
                        (paramsGet.getDeviceError().isFc_door_open_alarm() == (((data[i] & 0x02) == 0) && ((data[i] & 0x04) == 0))) ||
                        (paramsGet.getDeviceError().isCc_door_open_alarm() != ((data[i] & 0x08) == 0x08))) {
                    paramsGet.getDeviceError().setRc_door_open_alarm((data[i] & 0x01) == 0x01);// 冷藏门开关状态位
                    paramsGet.getDeviceError().setFc_door_open_alarm(!(((data[i] & 0x02) == 0) && ((data[i] & 0x04) == 0)));// 冷冻门开关
                    paramsGet.getDeviceError().setCc_door_open_alarm((data[i] & 0x08) == 0x08);// 变温门开关状态位

                    if (paramsGet.getDeviceError().isRc_door_open_alarm() || paramsGet.getDeviceError().isCc_door_open_alarm() ||
                            paramsGet.getDeviceError().isFc_door_open_alarm()) { // 门开报警
                        RxBus.getInstance().post(BusEvent.MSG_OPEN_ALARM_DISPLAY);
                    } else RxBus.getInstance().post(BusEvent.MSG_OPEN_ALARM_DISMISS);
                }

                // 冰箱冷藏室设定温度值（设定温度值整数位 + 100） DATA5
                i++;
                paramsGet.setCold_temp_set(data[i] - 100);
                // 冰箱变温室设定温度值（设定温度值整数位 + 100） DATA6
                i++;
                paramsGet.setChangeable_temp_set(data[i] - 100);
                // 冰箱冷冻室设定温度值（设定温度值整数位 + 100） DATA7
                i++;
                paramsGet.setFreezing_temp_set(data[i] - 100);
                // 冰箱冷藏室设定温度值小数位（设定温度值小数位 * 10 + 10） DATA8
                i++;
                // 冰箱变温室设定温度值小数位（设定温度值小数位 * 10 + 10） DATA9
                i++;
                // 冰箱冷冻室设定温度值小数位（设定温度值小数位 * 10 + 10） DATA10
                i++;
                // 冰箱冷藏室传感器温度值（温度值 + 100，此值若 = 255 表示该传感器故障） DATA11
                i++;
                paramsGet.setCold_temp_real(data[i] - 100);
                paramsGet.getDeviceError().setError_rc_sensor(data[i] == 0xff);
                // 冰箱变温室传感器温度值（温度值 + 100，此值若 = 255 表示该传感器故障） DATA12
                i++;
                paramsGet.setChangeable_temp_real(data[i] - 100);
                paramsGet.getDeviceError().setError_cc_sensor(data[i] == 0xff);
                // 冰箱冷冻室传感器温度值（温度值 + 100，此值若 = 255 表示该传感器故障） DATA13
                i++;
                paramsGet.setFreezing_temp_real(data[i] - 100);
                paramsGet.getDeviceError().setError_fc_sensor(data[i] == 0xff);
                // 冰箱环境温度传感器温度值（传送实际温度值（0~50），若环温低于 0 度，此值为 0。此值若 = 70 表示该传感器故障） DATA14
                i++;
                paramsGet.setIndoor_temp(data[i]);
                paramsGet.getDeviceError().setError_indoor_sensor(data[i] == 70);
                // 冰箱冷藏室传感器温度值小数位（温度值小数位 * 10 + 10） DATA15
                i++;
                // 冰箱变温室传感器温度值小数位（温度值小数位 * 10 + 10） DATA16
                i++;
                // 冰箱冷冻室传感器温度值小数位（温度值小数位 * 10 + 10） DATA17
                i++;
                // 冰箱环境温度传感器温度值小数位（温度值小数位 * 10 + 10） DATA18
                i++;
                // 冰箱运行状态 4 DATA19
                i++;
                paramsGet.setFlipping_beam_status((data[i] & 0x80) == 0x80);// 翻转梁加热丝
                // 冰箱运行状态 5 DATA20
                i++;
                // 冰箱冷藏室蒸发传感器温度值（温度值 + 100，此值若 = 255 表示该传感器故障）DATA21
                i++;
                paramsGet.setRc_evaporator_temp(data[i] - 100);
                paramsGet.getDeviceError().setError_rc_defrost_sensor(data[i] == 0xff);
                // 冰箱变温室蒸发传感器温度值（温度值 + 100，此值若 = 255 表示该传感器故障）DATA22
                i++;
                paramsGet.setCc_evaporator_temp(data[i] - 100);
                paramsGet.getDeviceError().setError_cc_defrost_sensor(data[i] == 0xff);
                // 变频档位（范围：0～99）DATA23
                i++;
                // 冰箱冷冻室蒸发传感器温度值（温度值 + 100，此值若 = 255 表示该传感器故障） DATA24
                i++;
                paramsGet.setFc_evaporator_temp(data[i] - 100);
                paramsGet.getDeviceError().setError_fc_defrost_sensor(data[i] == 0xff);
                // 冰箱运行状态 7（冰箱故障判断类 1） DATA25
                i++;
                paramsGet.getDeviceError().setError_fc_fan((data[i] & 0x02) == 0x02);
                // 冰箱运行状态 8（冰箱故障判断类） DATA26
                i++;
                // 冰箱湿度传感器温度值（温度值 + 100，此值若 = 255 表示该传感器故障）DATA27
                i++;
                paramsGet.getDeviceError().setError_humidity_temp_sensor(data[i] == 0xff);
                // 冰箱湿度传感器湿度值（湿度值 0~100，此值若 = 255 表示该传感器故障）DATA28
                i++;
                paramsGet.getDeviceError().setError_humidity_sensor(data[i] == 0xff);
                // 冰箱冷藏室变温区传感器温度值（温度值 + 100，此值若 = 255 表示该传感器故障）DATA29
                i++;
                paramsGet.getDeviceError().setError_rc_cc_sensor(data[i] == 0xff);
                i += 9;// 保留：DATA30~ DATA38

                i++;// 冰箱型号预定义，值为零（DATA39 = 0）,DATA39

                i++;// 冰箱型号（冰箱产品代号，高位放在 DATA40 字节,低位放在 DATA41 字节）,DATA40
                paramsGet.setData40(data[i]);

                i++;// DATA41
                paramsGet.setData41(data[i]);

                i += 5;// 保留 DATA42~DATA46

                i++;// DATA47

                paramsGet.setErrorCode();// 设置故障码

                return true;

            default:
                logUtil.e(TAG, "model error=" + deviceModel);
                return false;
        }
    }

    /**
     * 检测 MCU 升级状态
     *
     * @param data:   读取数据
     * @param length: 返回长度
     * @return MCU 是否正在升级
     */
    public static boolean checkMCUUpdate(int[] data, int length) {
        if (data == null || length != 4) {  // 升级返回的数据长度为 4
            return false;
        }
        if (((byte) data[0]) != AppConstants.MCU_UPDATE_HEADER) {
            logUtil.e(TAG, "MCU don't start with " + AppConstants.MCU_UPDATE_HEADER);
            return false;
        }
        if (data[1] != AppConstants.MCU_UPDATE_START && data[1] != AppConstants.MCU_UPDATE_SEND_SUCCESS
                && data[1] != AppConstants.MCU_UPDATE_SEND_FAIL && data[1] != AppConstants.MCU_UPDATE_SUCCESS) {
            logUtil.e(TAG, "MCU is not updating");
            return false;
        }
        int receiveRCR = data[2] * 256 + data[3];
        byte[] bytes = new byte[2];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) (data[i] & 0xff);
        }
        int crc = CRC16Util.calcCrc16(bytes, 2);
        if (crc != receiveRCR) {
            logUtil.e(TAG, "MCU CRC16 fail!");
            return false;
        }
//        mUpgradeStep = bytes[1];
        return true;
    }

    /**
     * 是否一键净化中
     *
     * @param status: 读取状态
     */
    private static boolean isOneKeyCleanWorking(int status) {
        return (status & 0x40) == 0x40;
    }

    /**
     * 是否强制启动
     *
     * @param status: 读取状态
     */
    private static boolean isRCFForcedStart(int status) {
        return (status & 0x02) == 0x02;
    }

    /**
     * 是否强制化霜测试
     *
     * @param status: 读取状态
     */
    private static boolean isForcedFrost(int status) {
        return (status & 0x04) == 0x04;
    }

    /**
     * 是否风机报警
     *
     * @param status: 读取状态
     */
    private static boolean isFanAlarm(int status) {
        return (status & 0x20) == 0x20;
    }

    /**
     * 是否冷藏室门开报警
     *
     * @param status: 读取状态
     */
    private static boolean isRCDoorOpenAlarm(int status) {
        return (status & 0x01) == 0x01;
    }

    /**
     * 冷冻化霜加热状态
     *
     * @param status: 读取状态
     */
    private static boolean isFCEvaporatorHeat(int status) {
        return (status & 0x20) == 0x20;
    }
}