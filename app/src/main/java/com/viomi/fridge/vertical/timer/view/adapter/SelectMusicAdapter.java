package com.viomi.fridge.vertical.timer.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.util.ImageLoadUtil;

/**
 * 选择定时结束铃声列表适配器
 * Created by nanquan on 2018/1/24.
 */
public class SelectMusicAdapter extends RecyclerView.Adapter<SelectMusicAdapter.MusicHolder> {
    private Context mContext;
    private int mCheckPosition;
    private String[] musicNames = new String[]{"铃声1", "铃声2", "铃声3", "铃声4", "铃声5"};
    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public SelectMusicAdapter(Context context, int checkPosition) {
        this.mContext = context;
        this.mCheckPosition = checkPosition;
    }

    @Override
    public MusicHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(mContext).inflate(R.layout.item_select_music, parent, false);
        return new MusicHolder(rootView);
    }

    @Override
    public void onBindViewHolder(MusicHolder holder, int position) {
        holder.tvMusic.setText(musicNames[position]);
        if (position == mCheckPosition) {
            ImageLoadUtil.getInstance().loadResource(R.drawable.icon_check, holder.ivCheck);
        } else {
            ImageLoadUtil.getInstance().loadResource(R.drawable.icon_uncheck, holder.ivCheck);
        }

        holder.itemView.setOnClickListener(v -> {
            mCheckPosition = holder.getAdapterPosition();
            notifyDataSetChanged();
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return musicNames.length;
    }

    public int getCheckPosition() {
        return mCheckPosition;
    }

    class MusicHolder extends RecyclerView.ViewHolder {
        TextView tvMusic;
        ImageView ivCheck;

        MusicHolder(View itemView) {
            super(itemView);
            tvMusic = itemView.findViewById(R.id.tv_music_name);
            ivCheck = itemView.findViewById(R.id.iv_music_check);
        }
    }
}