package com.viomi.fridge.vertical.home.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.List;

/**
 * 思必驰天气 Json
 * Created by William on 2018/8/13.
 */
public class WeatherDetailEntity implements Serializable {
    @JSONField(name = "aqi")
    private AirQualityEntity aqi;
    @JSONField(name = "daily_forecast")
    private List<DailyForecastEntity> daily_forecast;
    @JSONField(name = "now")
    private NowEntity now;

    public AirQualityEntity getAqi() {
        return aqi;
    }

    public void setAqi(AirQualityEntity aqi) {
        this.aqi = aqi;
    }

    public List<DailyForecastEntity> getDaily_forecast() {
        return daily_forecast;
    }

    public void setDaily_forecast(List<DailyForecastEntity> daily_forecast) {
        this.daily_forecast = daily_forecast;
    }

    public NowEntity getNow() {
        return now;
    }

    public void setNow(NowEntity now) {
        this.now = now;
    }
}
