package com.viomi.fridge.vertical.speech.model.repository.skill;

import android.content.Context;

import com.miot.common.abstractdevice.AbstractDevice;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.administration.model.entity.QRCodeBase;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.iot.model.repository.RangeHoodRepository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 云米油烟机技能
 * Created by William on 2018/8/30.
 */
public class ViomiHoodSkill {
    private static final String TAG = ViomiHoodSkill.class.getSimpleName();

    public static void handle(Context context, String data, AbstractDevice device) {
        String content;// 语音播报内容
        QRCodeBase qrCodeBase = (QRCodeBase) ToolUtil.getFileObject(context, AppConstants.USER_INFO_FILE);
        if (qrCodeBase == null) {
            content = "请先登录云米账号";
            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
            return;
        }
        if (device == null) {
            content = "没有连接到抽油烟机";
            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
            return;
        }

        try {
            JSONObject jsonObject = new JSONObject(data);
            JSONObject nlu = jsonObject.optJSONObject("nlu");
            if (nlu == null) return;
            JSONObject semantics = nlu.optJSONObject("semantics");
            if (semantics == null) return;
            JSONObject request = semantics.optJSONObject("request");
            if (request == null) return;
            JSONArray slots = request.optJSONArray("slots");
            if (slots == null || slots.length() <= 0) return;

            String value = "";// 意图
            for (int i = 0; i < slots.length(); i++) {
                JSONObject jsonItem = slots.optJSONObject(i);
                String name = jsonItem.optString("name");
                if (name.equals("intent")) {
                    value = jsonItem.optString("value");
                    break;
                }
            }

            switch (value) {
                case "打开油烟机":
                    RangeHoodRepository.setPower("2", device.getDeviceId())
                            .compose(RxSchedulerUtil.SchedulersTransformer1())
                            .subscribe(rpcResult -> {
                            }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                    content = "正在为你开启" + device.getName();
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "关闭油烟机":
                    RangeHoodRepository.setPower("0", device.getDeviceId())
                            .compose(RxSchedulerUtil.SchedulersTransformer1())
                            .subscribe(rpcResult -> {
                            }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                    content = "正在为你关闭" + device.getName();
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "设置档位":
                    String value_shift = "";
                    for (int i = 0; i < slots.length(); i++) {
                        JSONObject itemJson = slots.optJSONObject(i);
                        String name = itemJson.optString("name");
                        if ("档位变量".equals(name)) {
                            value_shift = itemJson.optString("value");
                            break;
                        }
                    }
                    switch (value_shift) {
                        case "高档":
                            RangeHoodRepository.setWind("16", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "设为高档模式";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "爆炒":
                            RangeHoodRepository.setWind("4", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "设为爆炒模式";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "低档":
                            RangeHoodRepository.setWind("1", device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "设为低挡模式";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                    }
                    break;
                case "打开灯光":
                    RangeHoodRepository.setLight("1", device.getDeviceId())
                            .compose(RxSchedulerUtil.SchedulersTransformer1())
                            .subscribe(rpcResult -> {
                            }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                    content = "正在为你开启" + device.getName() + "灯光";
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "关闭灯光":
                    RangeHoodRepository.setLight("0", device.getDeviceId())
                            .compose(RxSchedulerUtil.SchedulersTransformer1())
                            .subscribe(rpcResult -> {
                            }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                    content = "正在为你关闭" + device.getName() + "灯光";
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
            }
        } catch (JSONException e) {
            logUtil.e(TAG, e.toString());
            e.printStackTrace();
        }
    }
}
