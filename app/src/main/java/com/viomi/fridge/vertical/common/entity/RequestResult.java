package com.viomi.fridge.vertical.common.entity;

/**
 * 网络请求返回
 * Created by nanquan on 2018/2/6.
 */
public class RequestResult<T> {

    private MobBaseRes<T> mobBaseRes;

    public MobBaseRes<T> getMobBaseRes() {
        return mobBaseRes;
    }

    public void setMobBaseRes(MobBaseRes<T> mobBaseRes) {
        this.mobBaseRes = mobBaseRes;
    }
}
