package com.viomi.fridge.vertical.messageboard.contract;

import com.viomi.fridge.vertical.common.callback.BasePresenter;
import com.viomi.fridge.vertical.messageboard.model.entity.MessageBoardEntity;

import java.util.List;

/**
 * 留言板列表 Contract
 * Created by William on 2018/4/11.
 */
public interface MessageBoardListContract {
    interface View {
        void display();// 显示留言板列表

        void notifyDataChanged();// 数据更新

        void notifyDataRemoved(int size);// 删除数据

        void notifyDataInserted(int size);// 插入数据
    }

    interface Presenter extends BasePresenter<View> {
        void loadList(List<MessageBoardEntity> list);// 读取留言板列表

        void delete(List<MessageBoardEntity> list);// 删除留言板图片
    }
}
