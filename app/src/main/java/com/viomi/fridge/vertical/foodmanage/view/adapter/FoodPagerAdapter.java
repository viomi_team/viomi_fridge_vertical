package com.viomi.fridge.vertical.foodmanage.view.adapter;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.viomi.fridge.vertical.foodmanage.view.fragment.FoodManageFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * 食材管理 ViewPager 适配器
 * Created by William on 2018/2/25.
 */
public class FoodPagerAdapter extends FragmentPagerAdapter {
    private List<FoodManageFragment> mList;
    private String[] mTitles;

    public FoodPagerAdapter(FragmentManager fm, List<FoodManageFragment> list, String[] titles) {
        super(fm);
        mList = list;
        mTitles = titles;
        if (mList == null) mList = new ArrayList<>();
    }

    @Override
    public FoodManageFragment getItem(int position) {
        return mList.get(position);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    // 重写这个方法，将设置每个 Tab 的标题
    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles[position];
    }
}