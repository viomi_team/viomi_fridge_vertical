package com.viomi.fridge.vertical.fridgecontrol.model.serial.manager;

import com.mediatek.factorymode.serial.jniSERIAL;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.common.callback.AppCallback;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;
import com.viomi.fridge.vertical.fridgecontrol.model.repository.FridgeRepository;
import com.viomi.fridge.vertical.fridgecontrol.model.repository.MiotRepository;
import com.viomi.fridge.vertical.fridgecontrol.model.serial.entity.DeviceError;
import com.viomi.fridge.vertical.fridgecontrol.model.serial.entity.DeviceParams;
import com.viomi.fridge.vertical.fridgecontrol.model.serial.parser.SerialReadParser;
import com.viomi.fridge.vertical.fridgecontrol.model.serial.parser.SerialWriteParser;
import com.viomi.fridge.vertical.fridgecontrol.util.SerialDataUtil;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * 串口通讯管理
 * Created by William on 2018/1/5.
 */
public class SerialManager {
    private static final String TAG = SerialManager.class.getSimpleName();
    private jniSERIAL mJniSerial;// 串口实例
    private static SerialManager mInstance;// 实例
    private CompositeSubscription mCompositeSubscription;// 统一管理取消订阅
    private Subscription mCMDWriteSubscription;// 重新发送
    private Subscription mTimeSubscription;// 速冷关闭计时（适用雪祺 450）
    private Subscription mTempSubscription;// 每隔一分钟设置温度（适用美菱 462 和 521）
    private DeviceParams mDeviceParamsSet;// 发送数据
    private DeviceParams mDeviceParamsGet;// 接收数据
    private DeviceParams mDeviceParamCacheSet;// 暂存发送数据（适用双鹿 446）
    private boolean mIsWriting = false, mIsCommunicationError = false;// 是否正在发送串口数据，是否发生通讯故障
    private boolean mIsFirst = true;// 是否首次上电（450 通讯故障判断）
    private String mModel;// 冰箱 Model
    private int mCommunicationCount = 0;// 串口读取次数（通讯故障判断）
    private int[] mDataReceive;// 上一次读取数据
    private long mTimeReceive;// 上一次读取数据时间戳
    private boolean mIsModify = true, mIsReceiveData = false;// 是否查询状态（适用美菱 462 和 521）,是否接受到数据

    public static SerialManager getInstance() {
        if (mInstance == null) {
            synchronized (SerialManager.class) {
                if (mInstance == null) {
                    mInstance = new SerialManager();
                }
            }
        }
        return mInstance;
    }

    /**
     * 打开串口
     */
    public void open() {
        if (mModel == null || mModel.equals(""))
            mModel = FridgePreference.getInstance().getModel();// 读取冰箱 Model
        int writePeriod = 3;// 串口写入间隔
        int mReadPeriod = 500;// 串口读取间隔
        if (mJniSerial == null) mJniSerial = new jniSERIAL();
        if (jniSERIAL.no_serial_lib) { // 本地没有 so 库
            logUtil.e(TAG, "open fail, no serial lib");
            if (mModel.equals(AppConstants.MODEL_X2)) mDeviceParamCacheSet = new DeviceParams();
            mDeviceParamsSet = new DeviceParams();
            mDeviceParamsGet = new DeviceParams();
            return;
        }
        // 波特率
        int baudRate;
        if (mModel.equals(AppConstants.MODEL_X2)) baudRate = 4800;
        else baudRate = 9600;
        logUtil.d(TAG, "open serial, baudRate = " + baudRate);
        int result = mJniSerial.init(mJniSerial, baudRate, 8, 'n', 1);
        logUtil.d(TAG, "Serial initWithOnCreate result:" + result);

        if (mCompositeSubscription == null) mCompositeSubscription = new CompositeSubscription();
        else mCompositeSubscription.clear();

        // 读取缓存数据
        if (mModel.equals(AppConstants.MODEL_X2)) {
            if (mDeviceParamCacheSet == null) {
                mDeviceParamCacheSet = FridgePreference.getInstance().getFridgeSetData();
                if (mDeviceParamCacheSet == null) mDeviceParamCacheSet = new DeviceParams();
            }
        }
        if (mDeviceParamsSet == null) {
            mDeviceParamsSet = FridgePreference.getInstance().getFridgeSetData();
            if (mDeviceParamsSet == null) mDeviceParamsSet = new DeviceParams();
        }
        if (mDeviceParamsGet == null) {
            mDeviceParamsGet = FridgePreference.getInstance().getFridgeGetData();
            if (mDeviceParamsGet == null) mDeviceParamsGet = new DeviceParams();
        }

        // 串口写数据
        Subscription writeSubscription = Observable.interval(0, writePeriod, TimeUnit.SECONDS)
                .onBackpressureDrop() // 背压处理
                .compose(RxSchedulerUtil.SchedulersTransformer5())
                .onTerminateDetach()
                .subscribe(aLong -> {
                    logUtil.d(TAG, "serial write start");
                    try {
                        if (mIsWriting) return;// 正在发送，返回
                        if (mDeviceParamsSet == null) { // 被回收重新读取
                            mDeviceParamsSet = FridgePreference.getInstance().getFridgeSetData();
                            if (mDeviceParamsSet == null) mDeviceParamsSet = new DeviceParams();
                        }
                        if (mModel.equals(AppConstants.MODEL_X2)) {
                            if (mDeviceParamCacheSet == null) { // 被回收重新读取
                                mDeviceParamCacheSet = FridgePreference.getInstance().getFridgeSetData();
                                if (mDeviceParamCacheSet == null)
                                    mDeviceParamCacheSet = new DeviceParams();
                            }
                        }
                        sendToControl(false, null);
                    } catch (Exception e) {
                        e.printStackTrace();
                        logUtil.e(TAG, "writeSubscription error!e=" + e.getMessage());
                    }

                }, throwable -> {
                    close();
                    open();
                    logUtil.e(TAG, "writeSubscription error!");
                    logUtil.e(TAG, throwable.getMessage());
                });

        // 串口读数据
        Subscription readSubscription = Observable.interval(0, mReadPeriod, TimeUnit.MILLISECONDS)
                .onBackpressureDrop() // 背压处理
                .compose(RxSchedulerUtil.SchedulersTransformer5())
                .onTerminateDetach()
                .subscribe(aLong -> {
                    try {
                        if (mDeviceParamsGet == null) { // 被回收重新读取
                            mDeviceParamsGet = FridgePreference.getInstance().getFridgeGetData();
                            if (mDeviceParamsGet == null) mDeviceParamsGet = new DeviceParams();
                        }
                        int length = mJniSerial.serial_read(mJniSerial);// 读取返回字节长度
                        //                    logUtil.d(TAG,"serial_read ,length="+length);
                        serialRead(mJniSerial.rd_data, length);
                    } catch (Exception e) {
                        e.printStackTrace();
                        logUtil.e(TAG, "readSubscription error!e=" + e.getMessage());
                    }
                }, throwable -> {
                    close();
                    open();
                    logUtil.e(TAG, "readSubscription error!");
                    logUtil.e(TAG, throwable.getMessage());
                });

        mCompositeSubscription.add(writeSubscription);
        mCompositeSubscription.add(readSubscription);
    }

    /**
     * 显示板向主控板发送数据
     *
     * @param isSave:   是否保存到本地，冰箱重启可恢复
     * @param callback: 回调
     */
    public void sendToControl(boolean isSave, AppCallback<String> callback) {
        logUtil.i(TAG, "sendToControl");
        mIsReceiveData = false;
        switch (mModel) {
            case AppConstants.MODEL_X4: // 雪祺 450 对开门
            case AppConstants.MODEL_JD: // 雪祺 450 京东定制
                mIsWriting = true;// 写入开始
                if (serialWrite(mDeviceParamsSet)) { // 成功写入
                    mIsWriting = false;// 写入结束
                    logUtil.d(TAG, "serial write success");
                    if (isSave) { // 保存数据到本地
                        FridgePreference.getInstance().saveFridgeSetData(mDeviceParamsSet);
                    }
                    if (mDeviceParamsSet.isQuick_cold()) startQuickColdTiming();
                    else stopQuickColdTiming();
                    if (callback != null) callback.onSuccess(null);
                } else { // 写入失败
                    mIsWriting = false;// 写入结束
                    logUtil.e(TAG, "serial write fail");
                    if (callback != null) callback.onFail(-98, "serial parse fail");
                }
                break;
            case AppConstants.MODEL_X2: // 双鹿 446
                if (serialWrite(mDeviceParamsSet)) { // 成功发送命令
                    if (mCMDWriteSubscription != null) { // 先取消订阅
                        mCMDWriteSubscription.unsubscribe();
                        mCMDWriteSubscription = null;
                    }
                    mCMDWriteSubscription = Observable.interval(1500, 1500, TimeUnit.MILLISECONDS)
                            .onBackpressureDrop() // 背压处理
                            .compose(RxSchedulerUtil.SchedulersTransformer5())
                            .onTerminateDetach()
                            .subscribe(aLong -> {
                                if (mIsReceiveData) { // 有返回数据
                                    mIsWriting = false;// 写入结束
                                    mIsModify = false;// 修改为查询状态
                                    if (isSave) { // 保存数据到本地
                                        FridgePreference.getInstance().saveFridgeSetData(mDeviceParamsSet);
                                    }
                                    SerialDataUtil.copyParams(mDeviceParamCacheSet, mDeviceParamsSet);// 更新暂存数据
                                    logUtil.d(TAG, "serial write success");
                                    if (mCMDWriteSubscription != null) {
                                        mCMDWriteSubscription.unsubscribe();// 取消订阅
                                        mCMDWriteSubscription = null;
                                    }
                                    if (callback != null) callback.onSuccess(null);
                                } else { // 写入失败
                                    logUtil.e(TAG, "serial Write fail, time = " + aLong);
                                    if (aLong >= 2) { // 连续 3 次失败
                                        mIsWriting = false;// 写入结束
                                        SerialDataUtil.copyParams(mDeviceParamsSet, mDeviceParamCacheSet);// 恢复上次数据
                                        if (isSave) { // 保存数据到本地
                                            FridgePreference.getInstance().saveFridgeSetData(mDeviceParamsSet);
                                        }
                                        if (mCMDWriteSubscription != null) {
                                            mCMDWriteSubscription.unsubscribe();// 取消订阅
                                            mCMDWriteSubscription = null;
                                        }
                                        if (callback != null)
                                            callback.onFail(-99, "serial write fail");
                                    } else { // 重试
                                        serialWrite(mDeviceParamsSet);
                                    }
                                }
                            }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                } else if (callback != null) { // 写入失败
                    callback.onFail(-98, "serial parse fail");
                }
                break;
            default: // 美菱 462，521
                if (serialWrite(mDeviceParamsSet)) { // 成功发送命令
                    if (mCMDWriteSubscription != null) { // 先取消订阅
                        mCMDWriteSubscription.unsubscribe();
                        mCMDWriteSubscription = null;
                    }
                    mCMDWriteSubscription = Observable.interval(0, 1500, TimeUnit.MILLISECONDS)
                            .onBackpressureDrop() // 背压处理
                            .compose(RxSchedulerUtil.SchedulersTransformer5())
                            .onTerminateDetach()
                            .subscribe(aLong -> {
                                boolean isRecover = false;
                                if (aLong >= 2) isRecover = true;
                                if (SerialDataUtil.checkCMDResult(isRecover, mDeviceParamsGet, mDeviceParamsSet, mModel)) { // 写入成功
                                    mIsWriting = false;// 写入结束
                                    mIsModify = false;// 修改为查询状态
                                    if (isSave) { // 保存数据到本地
                                        FridgePreference.getInstance().saveFridgeSetData(mDeviceParamsSet);
                                    }
                                    if (mDeviceParamsSet.getMode() == AppConstants.MODE_SMART)
                                        setTempInSmart();
                                    else stopSetTempInSmart();
                                    logUtil.d(TAG, "serial write success");
                                    if (mCMDWriteSubscription != null) {
                                        mCMDWriteSubscription.unsubscribe();// 取消订阅
                                        mCMDWriteSubscription = null;
                                    }
                                    if (callback != null) callback.onSuccess(null);
                                } else { // 写入失败
                                    logUtil.e(TAG, "serial Write fail, time = " + aLong);
                                    if (aLong >= 2) { // 连续 3 次失败
                                        mIsWriting = false;// 写入结束
                                        if (isSave) { // 保存数据到本地
                                            FridgePreference.getInstance().saveFridgeSetData(mDeviceParamsSet);
                                        }
                                        if (mCMDWriteSubscription != null) {
                                            mCMDWriteSubscription.unsubscribe();// 取消订阅
                                            mCMDWriteSubscription = null;
                                        }
                                        if (callback != null)
                                            callback.onFail(-99, "serial write fail");
                                    } else { // 重试
                                        serialWrite(mDeviceParamsSet);
                                    }
                                }
                            }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                } else if (callback != null) {
                    callback.onFail(-98, "serial parse fail");
                }
                break;
        }
    }

    /**
     * 串口写入数据
     *
     * @param params: 冰箱相关数据
     * @return 是否发送成功
     */
    private boolean serialWrite(DeviceParams params) {
        logUtil.d(TAG, "serialWrite");
        if (jniSERIAL.no_serial_lib) { // 本地没有 so 库
            logUtil.e(TAG, "serial write fail, no serial lib");
            return false;
        }
        if (mJniSerial == null) {
            open();// 重新打开串口
            if (jniSERIAL.no_serial_lib) {
                logUtil.e(TAG, "restart fail, no serial lib");
                return false;
            }
        }
        mIsWriting = true;// 写入开始
        if (mModel == null || mModel.equals("")) mModel = FridgePreference.getInstance().getModel();
        int length;// 写入字节长度
        switch (mModel) {
            case AppConstants.MODEL_X2: // 双鹿 446
                length = AppConstants.X2_WRITE_BYTE_COUNT;
                break;
            case AppConstants.MODEL_X3: // 美菱 462
                length = AppConstants.X3_WRITE_BYTE_COUNT;
                break;
            case AppConstants.MODEL_X4: // 雪祺 450
            case AppConstants.MODEL_JD: // 雪祺 450 京东定制
                length = AppConstants.X4_WRITE_BYTE_COUNT;
                break;
            case AppConstants.MODEL_X5: // 美菱 521
                length = AppConstants.X5_WRITE_BYTE_COUNT;
                break;
            default:
                length = AppConstants.X1_WRITE_BYTE_COUNT;
                break;
        }
        int[] data = new int[length];// 封装需要写入的数据
        boolean result = SerialWriteParser.parse(params, data, mModel, mIsModify);// 写入前先进行格式化
        if (!result) {  // 解析失败
            logUtil.e(TAG, "serial write params parser fail");
            mIsWriting = false;// 写入结束
            return false;
        }
        mJniSerial.wr_data = data;
        logUtil.d(TAG, "serial write, data = " + SerialDataUtil.bytesToHexString(data));

        int size = mJniSerial.serial_write(mJniSerial, mJniSerial.wr_data.length);// 开始写入数据
        if (size != data.length) {  // 写入后返回长度与写入前做比较，长度不一样则写入失败
            logUtil.e(TAG, "serial write fail, size = " + size);
            mIsWriting = false;// 写入结束
            return false;
        }
        return true;
    }

    /**
     * 串口读取数据
     *
     * @param data:   返回数据
     * @param length: 返回数据长度
     */
    private void serialRead(int[] data, int length) {
        mCommunicationCount++;
        DeviceError mDeviceError = mDeviceParamsGet.getDeviceError();// 保存上次故障信息
        // 各型号通讯故障判断
        if (mModel.equals(AppConstants.MODEL_X4) || mModel.equals(AppConstants.MODEL_JD)) { // 雪祺 450
            if (!mIsCommunicationError && mIsFirst && mCommunicationCount > 10) { // 450 首次上电
                logUtil.d(TAG, "CommunicationError");
                mIsCommunicationError = true;
                mDeviceParamsGet.getDeviceError().setError_communication(true);
                mDeviceParamsGet.setErrorCode();
                FridgeRepository.getInstance().quitMode();// 退出模式
                RxBus.getInstance().post(BusEvent.MSG_OPEN_ALARM_DISMISS);// 关闭门开报警
                MiotRepository.getInstance().sendFaultHappen(AppConstants.ERROR_COMMUNICATION);// 上报
            } else if (!mIsCommunicationError && mCommunicationCount > 120) { // 读取失败超过 1 分钟上报通讯故障
                mIsCommunicationError = true;
                mDeviceParamsGet.getDeviceError().setError_communication(true);
                mDeviceParamsGet.setErrorCode();
                FridgeRepository.getInstance().quitMode();// 退出模式
                RxBus.getInstance().post(BusEvent.MSG_OPEN_ALARM_DISMISS);// 关闭门开报警
                MiotRepository.getInstance().sendFaultHappen(AppConstants.ERROR_COMMUNICATION);// 上报
            }
        } else {
            if (!mIsCommunicationError && mCommunicationCount > 120) { // 读取失败超过 1 分钟上报通讯故障
                mIsCommunicationError = true;
                mDeviceParamsGet.getDeviceError().setError_communication(true);
                mDeviceParamsGet.setErrorCode();
                RxBus.getInstance().post(BusEvent.MSG_OPEN_ALARM_DISMISS);// 关闭门开报警
                MiotRepository.getInstance().sendFaultHappen(AppConstants.ERROR_COMMUNICATION);// 上报
            }
        }

        if (data == null || length == 0) { // 读取失败
            logUtil.e(TAG, "serial Read fail, length = " + length);
            return;
        }
        mTimeReceive = System.currentTimeMillis();// 保存当前时间
        mDataReceive = data;// 保存此次读取数据
        logUtil.d(TAG, "serial Read，length = " + length);
        logUtil.d(TAG, "serial Read，data = " + SerialDataUtil.bytesToHexString(data, length));

        // 检测 MCU 是否正在更新
        if (SerialReadParser.checkMCUUpdate(data, length)) return;
        boolean result = SerialReadParser.parse(data, mDeviceParamsGet, mDeviceParamsSet, mModel);
        logUtil.d(TAG, "serial Read，result = " + result + ",data = " + mDeviceParamsGet.toString(mModel));
        if (result) { // 成功解析
            mIsReceiveData = true;
            mCommunicationCount = 0;// 复位
            mIsCommunicationError = false;
            mIsFirst = false;
            mDeviceParamsGet.getDeviceError().setError_communication(false);
            mDeviceParamsGet.setErrorCode();
            FridgePreference.getInstance().saveFridgeGetData(mDeviceParamsGet);
            FridgeRepository.getInstance().faultHappenOrFix(mDeviceError, mDeviceParamsGet.getDeviceError());// 故障上报
        }
    }

    /**
     * 速冷计时（雪祺 450 需开启速冷 2.5 小时后自动关闭）
     */
    private void startQuickColdTiming() {
        if (mTimeSubscription != null && !mTimeSubscription.isUnsubscribed()) return;
        logUtil.d(TAG, "startQuickColdTiming");
        mTimeSubscription = Observable.timer(150, TimeUnit.MINUTES)
                .compose(RxSchedulerUtil.SchedulersTransformer3())
                .onTerminateDetach()
                .subscribe(aLong -> {
                    FridgeRepository.getInstance().setSmartCool(false, null);// 退出速冷
                    if (mTimeSubscription != null) {
                        mTimeSubscription.unsubscribe();
                        mTimeSubscription = null;
                    }
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(mTimeSubscription);
    }

    /**
     * 速冷计时结束
     */
    private void stopQuickColdTiming() {
        if (mTimeSubscription != null) {
            logUtil.e(TAG, "stopQuickColdTiming");
            mTimeSubscription.unsubscribe();
            mTimeSubscription = null;
        }
    }

    /**
     * 美菱 462，521 在智能模式时需要每隔 1 分钟根据环温设定温度
     */
    private void setTempInSmart() {
        if (mTempSubscription != null && !mTempSubscription.isUnsubscribed()) return;
        logUtil.d(TAG, "setTempInSmart");
        mTempSubscription = Observable.interval(0, 1, TimeUnit.MINUTES)
                .onBackpressureDrop() // 背压处理
                .compose(RxSchedulerUtil.SchedulersTransformer3())
                .onTerminateDetach()
                .subscribe(aLong -> FridgeRepository.getInstance().setMode("smart", null),
                        throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(mTempSubscription);
    }

    /**
     * 停止在智能模式时每隔 1 分钟根据环温设定温度
     */
    private void stopSetTempInSmart() {
        if (mTempSubscription != null) {
            logUtil.e(TAG, "stopSetTempInSmart");
            mTempSubscription.unsubscribe();
            mTempSubscription = null;
        }
    }

    public DeviceParams getDeviceParamsSet() {
        return mDeviceParamsSet;
    }

    public DeviceParams getDeviceParamsGet() {
        return mDeviceParamsGet;
    }

    public void setIsModify(boolean isModify) {
        mIsModify = isModify;
    }

    /**
     * 返回上次读取串口数据
     */
    public String getLastDataReceive() {
        return mTimeReceive + ":" + SerialDataUtil.bytesToHexString(mDataReceive);
    }

    /**
     * 关闭串口
     */
    public void close() {
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
        if (mCMDWriteSubscription != null) {
            mCMDWriteSubscription.unsubscribe();
            mCMDWriteSubscription = null;
        }
        if (mJniSerial != null) {
            if (!jniSERIAL.no_serial_lib) {
                mJniSerial.exit();
            }
        }
        mIsReceiveData = false;
        mIsWriting = false;
    }
}