package com.viomi.fridge.vertical.administration.contract;

import com.viomi.fridge.vertical.administration.model.entity.AppUpdateResult;
import com.viomi.fridge.vertical.common.callback.BasePresenter;

/**
 * 版本升级 Contract
 * Created by William on 2018/2/22.
 */
public interface UpdateContract {
    interface View {
        void refreshUi(AppUpdateResult result);// app 更新刷新 Ui

        void refreshProgress(int progress);// 更新下载进度

        void install(String path);// 安装 App
    }

    interface Presenter extends BasePresenter<View> {
        void checkAppUpdate();// 检查 App 更新

        void downloadApp(String url);// 下载 App 文件
    }
}
