package com.viomi.fridge.vertical.foodmanage.contract;

import com.viomi.fridge.vertical.common.callback.BasePresenter;
import com.viomi.fridge.vertical.foodmanage.model.entity.FoodDetail;

import java.util.List;

/**
 * 食材管理列表 Contract
 * Created by William on 2018/2/28.
 */
public interface FoodFragmentContract {
    interface View {
        void refreshList(List<FoodDetail> list);// 刷新食材列表

        void notifyDataChanged();// 删除数据
    }

    interface Presenter extends BasePresenter<View> {
        void loadFood(List<FoodDetail> list, List<FoodDetail> list1, int type);// 读取食材信息

        void deleteFood(List<FoodDetail> list);// 删除食材
    }
}