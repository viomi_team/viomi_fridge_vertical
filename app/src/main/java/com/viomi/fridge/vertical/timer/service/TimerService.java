package com.viomi.fridge.vertical.timer.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.PreferenceHelper;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.functions.Func1;

/**
 * 计时器服务
 * Created by nanquan on 2018/1/24.
 */
public class TimerService extends Service {
    private SoundPool mSoundPool;
    private ArrayList<Integer> trackList = new ArrayList<>();
    private int currentStream = -1;
    private Subscription busSubscription;
    private Subscription timeSubscription;
    private PreferenceHelper mPreferenceHelper;
    private WindowManager mWindowManager;
    private FrameLayout mCompleteDialog;
    private boolean isCompleteDialogShow;
    public final static int WHAT_SHOW_COMPLETE_DIALOG = 0;
    public final static int WHAT_CANCEL_COMPLETE_DIALOG = 1;
    private Handler mHandler;
    private static boolean timerRunning = false;// 当前是否运行

    @Override
    public void onCreate() {
        super.onCreate();
        mPreferenceHelper = new PreferenceHelper(this);
        mWindowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        initSoundPool();
        initSubscription();
        initDialog();
        initHandler();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_REDELIVER_INTENT;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void initSoundPool() {
        if (Build.VERSION.SDK_INT > 21) {
            SoundPool.Builder builder = new SoundPool.Builder();
            builder.setMaxStreams(3);
            AudioAttributes.Builder attributesBuilder = new AudioAttributes.Builder();
            attributesBuilder.setLegacyStreamType(AudioManager.STREAM_MUSIC);
            builder.setAudioAttributes(attributesBuilder.build());
            mSoundPool = builder.build();
        } else {
            mSoundPool = new SoundPool(3, AudioManager.STREAM_MUSIC, 0);
        }

        int track1 = mSoundPool.load(this, R.raw.ok, 1);
        int track2 = mSoundPool.load(this, R.raw.ok2, 1);
        int track3 = mSoundPool.load(this, R.raw.ok3, 1);
        int track4 = mSoundPool.load(this, R.raw.ok4, 1);
        int track5 = mSoundPool.load(this, R.raw.ok5, 1);
        trackList.add(track1);
        trackList.add(track2);
        trackList.add(track3);
        trackList.add(track4);
        trackList.add(track5);
    }

    private void initSubscription() {
        busSubscription = RxBus.getInstance()
                .subscribe(event -> {
                    switch (event.getMsgId()) {
                        case BusEvent.MSG_TIMER_START: {
                            // 开始计时
                            int duration = (int) event.getMsgObject();
                            startTiming(duration);
                        }
                        break;
                        case BusEvent.MSG_TIMER_STOP: {
                            // 结束计时
                            stopTimer();
                        }
                        break;
                        case BusEvent.MSG_TIMER_SPEECH_STOP: {
                            // 结束计时
                            stopTimer();
                        }
                        break;
                        case BusEvent.MSG_TIMER_PLAY_MUSIC: {
                            // 播放音乐
                            int position = (int) event.getMsgObject();
                            playMusic(position);
                        }
                        break;
                        case BusEvent.MSG_TIMER_STOP_MUSIC: {
                            // 结束音乐
                            stopMusic();
                        }
                        break;
                    }
                });
    }

    @SuppressLint("InflateParams")
    private void initDialog() {
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater == null) {
            return;
        }
        mCompleteDialog = (FrameLayout) inflater
                .inflate(R.layout.dialog_timer_complete, null);
        TextView tvConfirm = mCompleteDialog.findViewById(R.id.tv_confirm);
        tvConfirm.setOnClickListener(view ->
                mHandler.sendEmptyMessage(WHAT_CANCEL_COMPLETE_DIALOG));
    }

    @SuppressLint("HandlerLeak")
    private void initHandler() {
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case WHAT_SHOW_COMPLETE_DIALOG: {
                        // 显示计时完成弹窗
                        if (!isCompleteDialogShow) {
                            mWindowManager.addView(mCompleteDialog,
                                    getWindowLayoutParams(Gravity.CENTER,
                                            WindowManager.LayoutParams.MATCH_PARENT,
                                            WindowManager.LayoutParams.MATCH_PARENT));
                            isCompleteDialogShow = true;
                        }
                    }
                    break;
                    case WHAT_CANCEL_COMPLETE_DIALOG: {
                        // 隐藏计时完成弹窗
                        if (isCompleteDialogShow) {
                            mWindowManager.removeView(mCompleteDialog);
                            isCompleteDialogShow = false;
                        }
                    }
                    break;
                }
            }
        };
    }

    private WindowManager.LayoutParams getWindowLayoutParams(int gravity, int width, int height) {
        WindowManager.LayoutParams wmParams = new WindowManager.LayoutParams();

        wmParams.format = PixelFormat.TRANSPARENT;

        wmParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM;

        wmParams.x = 0;
        wmParams.y = 0;

        wmParams.gravity = gravity;
        wmParams.width = width;
        wmParams.height = height;
        wmParams.type = WindowManager.LayoutParams.TYPE_PHONE;
        return wmParams;
    }

    /**
     * 点亮屏幕
     */
    @SuppressWarnings("deprecation")
    private void screenOn() {
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (powerManager != null && !powerManager.isScreenOn()) {
            PowerManager.WakeLock wakeLock = powerManager.newWakeLock(
                    PowerManager.FULL_WAKE_LOCK |
                            PowerManager.ACQUIRE_CAUSES_WAKEUP, "screen");
            wakeLock.acquire(1000);
            wakeLock.release();
        }
    }

    private void startTiming(int duration) {
        timerRunning = true;
        timeSubscription = Observable
                .interval(1, TimeUnit.SECONDS)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .map((Func1<Long, Object>) aLong -> duration - aLong.intValue())
                .take(duration + 1)
                .subscribe(aLong -> {
                    RxBus.getInstance().post(BusEvent.MSG_TIMER_TIMING, aLong);
                    if ((int) aLong == 0) {
                        timerRunning = false;
                        mHandler.sendEmptyMessage(WHAT_SHOW_COMPLETE_DIALOG);
                        screenOn();
                        playMusic(mPreferenceHelper.getSelectedTimeoutRing());
                    }
                });
    }

    private void stopTimer() {
        timerRunning = false;
        if (!timeSubscription.isUnsubscribed()) {
            timeSubscription.unsubscribe();
        }
    }

    private void playMusic(int position) {
        if (currentStream != -1) {
            mSoundPool.stop(currentStream);
        }
        currentStream = mSoundPool.play(trackList.get(position),
                1, 1, 1, 0, 1);
    }


    private void stopMusic() {
        if (currentStream != -1) {
            mSoundPool.stop(currentStream);
        }
    }

    public static boolean getTimerRunning() {
        return timerRunning;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mSoundPool != null) {
            mSoundPool.release();
        }
        if (busSubscription != null && !busSubscription.isUnsubscribed()) {
            busSubscription.unsubscribe();
            busSubscription = null;
        }
        if (timeSubscription != null && !timeSubscription.isUnsubscribed()) {
            timeSubscription.unsubscribe();
            timeSubscription = null;
        }
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
    }
}