package com.viomi.fridge.vertical.speech.model.repository.skill;

import android.content.Context;

import com.miot.common.abstractdevice.AbstractDevice;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.administration.model.entity.QRCodeBase;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.iot.model.http.entity.CSeriesPurifierProp;
import com.viomi.fridge.vertical.iot.model.http.entity.Mi1APurifierProp;
import com.viomi.fridge.vertical.iot.model.http.entity.MiPurifierProp;
import com.viomi.fridge.vertical.iot.model.http.entity.SSeriesPurifierProp;
import com.viomi.fridge.vertical.iot.model.http.entity.VSeriesPurifierProp;
import com.viomi.fridge.vertical.iot.model.http.entity.X3PurifierProp;
import com.viomi.fridge.vertical.iot.model.http.entity.X5PurifierProp;
import com.viomi.fridge.vertical.iot.model.repository.WaterPurifierRepository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 云米净水器技能
 * Created by William on 2018/8/29.
 */
public class ViomiWaterPurifierSkill {
    private static final String TAG = ViomiWaterPurifierSkill.class.getSimpleName();

    public static void handle(Context context, String data, AbstractDevice device) {
        String content;// 语音播报内容
        QRCodeBase qrCodeBase = (QRCodeBase) ToolUtil.getFileObject(context, AppConstants.USER_INFO_FILE);
        if (qrCodeBase == null) {
            content = "请先登录云米账号";
            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
            return;
        }
        if (device == null) {
            content = "没有连接到净水器";
            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
            return;
        }

        try {
            JSONObject jsonObject = new JSONObject(data);
            JSONObject nlu = jsonObject.optJSONObject("nlu");
            if (nlu == null) return;
            JSONObject semantics = nlu.optJSONObject("semantics");
            if (semantics == null) return;
            JSONObject request = semantics.optJSONObject("request");
            if (request == null) return;
            JSONArray slots = request.optJSONArray("slots");
            if (slots == null || slots.length() <= 0) return;

            String value = "";// 意图
            int value_temp = -1;// 设置水温
            for (int i = 0; i < slots.length(); i++) {
                JSONObject jsonItem = slots.optJSONObject(i);
                String name = jsonItem.optString("name");
                if (name.equals("intent")) {
                    value = jsonItem.optString("value");
                    break;
                }
            }

            switch (value) {
                case "设置温水键温度":
                    for (int i = 0; i < slots.length(); i++) {
                        JSONObject itemJson = slots.optJSONObject(i);
                        String name = itemJson.optString("name");
                        if ("整数".equals(name)) {
                            value_temp = itemJson.optInt("value");
                            break;
                        }
                    }
                    if (value_temp == -1) return;
                    switch (device.getDeviceModel()) {
                        case AppConstants.YUNMI_WATERPURI_X3:
                        case AppConstants.YUNMI_WATERPURI_X5:
                            if (value_temp < 40 || value_temp > 90) {
                                content = "净水器温度只能设置40到90度";
                                RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                                return;
                            }
                            WaterPurifierRepository.setTemp(device.getDeviceId(), value_temp)
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "出水温度设置为" + value_temp + "度";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        default:
                            content = device.getName() + "不能设置温度";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                    }
                    break;
                case "快捷设置温度":
                    value_temp = 50;
                    switch (device.getDeviceModel()) {
                        case AppConstants.YUNMI_WATERPURI_X3:
                        case AppConstants.YUNMI_WATERPURI_X5:
                            WaterPurifierRepository.setTemp(device.getDeviceId(), value_temp)
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "出水温度设置为" + value_temp + "度";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        default:
                            content = device.getName() + "不能设置温度";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                    }
                    break;
                case "查询水质":
                    switch (device.getDeviceModel()) {
                        case AppConstants.YUNMI_WATERPURI_V1:
                        case AppConstants.YUNMI_WATERPURI_V2:
                            WaterPurifierRepository.miGetProp(device.getDeviceId())
                                    .subscribeOn(Schedulers.io())
                                    .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                                    .onTerminateDetach()
                                    .map(rpcResult -> {
                                        VSeriesPurifierProp prop = new VSeriesPurifierProp();
                                        prop.initGetProp(rpcResult.getList());
                                        return prop;
                                    })
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .onTerminateDetach()
                                    .subscribe(vSeriesPurifierProp -> RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, device.getName()
                                                    + "当前的TDS值为" + vSeriesPurifierProp.getTds_out()),
                                            throwable -> logUtil.e(TAG, throwable.getMessage()));
                            break;
                        case AppConstants.YUNMI_WATERPURI_S1:
                        case AppConstants.YUNMI_WATERPURI_S2:
                            WaterPurifierRepository.miGetProp(device.getDeviceId())
                                    .subscribeOn(Schedulers.io())
                                    .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                                    .onTerminateDetach()
                                    .map(rpcResult -> new SSeriesPurifierProp(rpcResult.getList()))
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .onTerminateDetach()
                                    .subscribe(sSeriesPurifierProp -> RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, device.getName()
                                            + "当前的TDS值为" + sSeriesPurifierProp.getTds_out()), throwable -> logUtil.e(TAG, throwable.getMessage()));
                            break;
                        case AppConstants.YUNMI_WATERPURI_C1:
                        case AppConstants.YUNMI_WATERPURI_C2:
                            WaterPurifierRepository.miGetProp(device.getDeviceId())
                                    .subscribeOn(Schedulers.io())
                                    .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                                    .onTerminateDetach()
                                    .map(rpcResult -> {
                                        CSeriesPurifierProp prop = new CSeriesPurifierProp();
                                        prop.initGetProp(rpcResult.getList());
                                        return prop;
                                    })
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .onTerminateDetach()
                                    .subscribe(cSeriesPurifierProp -> RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, device.getName()
                                            + "当前的TDS值为" + cSeriesPurifierProp.getTds_out()), throwable -> logUtil.e(TAG, throwable.getMessage()));
                            break;
                        case AppConstants.YUNMI_WATERPURI_X3:
                            WaterPurifierRepository.miGetProp(device.getDeviceId())
                                    .subscribeOn(Schedulers.io())
                                    .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                                    .onTerminateDetach()
                                    .map(rpcResult -> {
                                        X3PurifierProp prop = new X3PurifierProp();
                                        prop.initGetProp(rpcResult.getList());
                                        return prop;
                                    })
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .onTerminateDetach()
                                    .subscribe(x3PurifierProp -> RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, device.getName()
                                            + "当前的TDS值为" + x3PurifierProp.getTds_out()), throwable -> logUtil.e(TAG, throwable.getMessage()));
                            break;
                        case AppConstants.YUNMI_WATERPURI_X5:
                            WaterPurifierRepository.miGetProp(device.getDeviceId())
                                    .subscribeOn(Schedulers.io())
                                    .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                                    .onTerminateDetach()
                                    .map(rpcResult -> {
                                        X5PurifierProp prop = new X5PurifierProp();
                                        prop.initGetExtraProp(rpcResult.getList());
                                        return prop;
                                    })
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .onTerminateDetach()
                                    .subscribe(x5PurifierProp -> RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, device.getName()
                                            + "当前的TDS值为" + x5PurifierProp.getTds_out()), throwable -> logUtil.e(TAG, throwable.getMessage()));
                            break;
                        case AppConstants.YUNMI_WATERPURIFIER_V1:
                        case AppConstants.YUNMI_WATERPURIFIER_V2:
                        case AppConstants.YUNMI_WATERPURIFIER_V3:
                        case AppConstants.YUNMI_WATERPURI_LX2:
                        case AppConstants.YUNMI_WATERPURI_LX3:
                            WaterPurifierRepository.miGetProp(device.getDeviceId())
                                    .subscribeOn(Schedulers.io())
                                    .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                                    .onTerminateDetach()
                                    .map(rpcResult -> new MiPurifierProp(rpcResult.getList()))
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .onTerminateDetach()
                                    .subscribe(miPurifierProp -> RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, device.getName()
                                            + "当前的TDS值为" + miPurifierProp.getTds_out()), throwable -> logUtil.e(TAG, throwable.getMessage()));
                            break;
                        case AppConstants.YUNMI_WATERPURI_LX5:
                            WaterPurifierRepository.mi1AGetProp(device.getDeviceId())
                                    .subscribeOn(Schedulers.io())
                                    .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                                    .onTerminateDetach()
                                    .map(rpcResult -> new Mi1APurifierProp(rpcResult.getList()))
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .onTerminateDetach()
                                    .subscribe(miPurifierProp -> RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, device.getName()
                                            + "当前的TDS值为" + miPurifierProp.getTds_out()), throwable -> logUtil.e(TAG, throwable.getMessage()));
                            break;
                    }
                    break;
                case "查询水温":
                    switch (device.getDeviceModel()) {
                        case AppConstants.YUNMI_WATERPURI_X3:
                            X3PurifierProp propX3 = new X3PurifierProp();
                            WaterPurifierRepository.x3GetProp(device.getDeviceId())
                                    .subscribeOn(Schedulers.io())
                                    .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                                    .onTerminateDetach()
                                    .map(rpcResult -> {
                                        propX3.initGetExtraProp(rpcResult.getList());
                                        return propX3;
                                    })
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .onTerminateDetach()
                                    .subscribe(x3PurifierProp -> RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, device.getName() +
                                            "当前的出水水温为" + x3PurifierProp.getSetup_tempe() + "度"), throwable -> logUtil.e(TAG, throwable.getMessage()));
                            break;
                        case AppConstants.YUNMI_WATERPURI_X5:
                            X5PurifierProp propX5 = new X5PurifierProp();
                            WaterPurifierRepository.x5GetProp(device.getDeviceId())
                                    .subscribeOn(Schedulers.io())
                                    .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                                    .onTerminateDetach()
                                    .map(rpcResult -> {
                                        propX5.initGetExtraProp(rpcResult.getList());
                                        return propX5;
                                    })
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .onTerminateDetach()
                                    .subscribe(x5PurifierProp -> RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, device.getName() +
                                            "当前的出水水温为" + x5PurifierProp.getSetup_tempe() + "度"), throwable -> logUtil.e(TAG, throwable.getMessage()));
                            break;
                        default:
                            content = device.getName() + "不支持出水温度查询";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                    }
                    break;

            }
        } catch (JSONException e) {
            logUtil.e(TAG, e.toString());
            e.printStackTrace();
        }
    }
}
