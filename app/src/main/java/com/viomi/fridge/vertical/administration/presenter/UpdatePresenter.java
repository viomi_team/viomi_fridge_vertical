package com.viomi.fridge.vertical.administration.presenter;

import android.content.Context;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.administration.contract.UpdateContract;
import com.viomi.fridge.vertical.administration.model.preference.ManagePreference;
import com.viomi.fridge.vertical.administration.model.repository.ManageRepository;
import com.viomi.fridge.vertical.common.http.ApiClient;
import com.viomi.fridge.vertical.common.http.ProgressListener;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;

import javax.annotation.Nullable;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * 版本升级 Presenter
 * Created by William on 2018/2/22.
 */
public class UpdatePresenter implements UpdateContract.Presenter {
    private static final String TAG = UpdatePresenter.class.getSimpleName();
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;

    @Nullable
    private UpdateContract.View mView;

    public UpdatePresenter(Context context) {
        mContext = context;
    }

    @Override
    public void subscribe(UpdateContract.View view) {
        mView = view;
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public void unSubscribe() {
        mView = null;
        ApiClient.getInstance().setProgressListener(null);
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
    }

    @Override
    public void checkAppUpdate() {
        String packageName = ManagePreference.getInstance().getDebug() ? mContext.getPackageName() + ".debug" : mContext.getPackageName();
        String model = FridgePreference.getInstance().getModel();// 读取冰箱 Model
        String channel = AppConstants.CHANNEL_VIOMI;
        if (AppConstants.MODEL_JD.equals(model)) {
            channel = AppConstants.CHANNEL_JINGDONG;
        }
        Subscription subscription = ApiClient.getInstance().getApiService().checkAppUpdate(packageName, channel)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(appUpdateResult -> {
                    if (mView != null) mView.refreshUi(appUpdateResult);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void downloadApp(String url) {
        ProgressListener progressListener = (currentBytes, contentLength, isFinish) -> {
            if (mView != null) mView.refreshProgress((int) (currentBytes * 100 / contentLength));
        };
        ApiClient.getInstance().setProgressListener(progressListener);
        Subscription subscription = ApiClient.getInstance().getApiService().download(url)
                .subscribeOn(Schedulers.newThread())
                .onTerminateDetach()
                .flatMap(responseBody -> ManageRepository.getInstance().saveApk(url, responseBody))
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(s -> {
                    if (mView != null) mView.install(s);
                }, throwable -> {
                    if (mView != null) mView.install(null);
                    logUtil.e(TAG, throwable.getMessage());
                });
        mCompositeSubscription.add(subscription);
    }
}
