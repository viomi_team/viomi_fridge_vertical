package com.viomi.fridge.vertical.speech.model.repository;

import android.content.Context;

import com.miot.common.abstractdevice.AbstractDevice;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.iot.model.http.entity.DeviceTypeEntity;
import com.viomi.fridge.vertical.speech.model.repository.skill.ChineseStudySkill;
import com.viomi.fridge.vertical.speech.model.repository.skill.ControlSkill;
import com.viomi.fridge.vertical.speech.model.repository.skill.CookBookSkill;
import com.viomi.fridge.vertical.speech.model.repository.skill.FridgeSkill;
import com.viomi.fridge.vertical.speech.model.repository.skill.JokeSkill;
import com.viomi.fridge.vertical.speech.model.repository.skill.MusicSkill;
import com.viomi.fridge.vertical.speech.model.repository.skill.NewsSkill;
import com.viomi.fridge.vertical.speech.model.repository.skill.StorySkill;
import com.viomi.fridge.vertical.speech.model.repository.skill.ViomiDishWasherSkill;
import com.viomi.fridge.vertical.speech.model.repository.skill.ViomiFanSkill;
import com.viomi.fridge.vertical.speech.model.repository.skill.ViomiHoodSkill;
import com.viomi.fridge.vertical.speech.model.repository.skill.ViomiHotDrinkerSkill;
import com.viomi.fridge.vertical.speech.model.repository.skill.ViomiVacuumSkill;
import com.viomi.fridge.vertical.speech.model.repository.skill.ViomiWasherSkill;
import com.viomi.fridge.vertical.speech.model.repository.skill.ViomiWaterPurifierSkill;
import com.viomi.fridge.vertical.speech.model.repository.skill.WuLianSkill;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * 技能处理管理
 * Created by William on 2018/8/14.
 */
public class SkillRepository {
    private static final String TAG = SkillRepository.class.getSimpleName();
    private static final String VIDEO_ID = "2018081400000052";// 影音娱乐
    private static final String MUSIC_ID = "2018080800000015";// 音乐
    private static final String NEWS_ID = "100001825";// 新闻
    private static final String CHINESE_STUDY_ID = "2018060500000011";// 国学
    private static final String JOKE_ID = "100001760";// 笑话
    private static final String STORY_ID = "2018040200000012";// 故事
    private static final String AITEK_COOKBOOK_ID = "2018080800000021";// AITEK 菜谱
    private static final String WULIAN_ID = "2018061300000038";// 物联设备控制
    private static final String CONTROL = "冰箱控制";
    private static final String FRIDGE = "云米冰箱";
    private static final String HOT_DRINKER = "云米饮水吧";
    private static final String WATER_PURIFIER = "云米净水器";
    private static final String RANGE_HOOD = "云米油烟机";
    private static final String DISH_WASHER = "云米洗碗机";
    private static final String WASHER = "云米洗衣机";
    private static final String OVEN = "云米蒸烤机";
    private static final String VACUUM = "云米扫地机";
    private static final String FAN = "云米风扇";
    private static SkillRepository mInstance;
    private List<DeviceTypeEntity> mList;// 设备列表

    public static SkillRepository getInstance() {
        if (mInstance == null) {
            synchronized (SkillRepository.class) {
                if (mInstance == null) {
                    mInstance = new SkillRepository();
                }
            }
        }
        return mInstance;
    }

    /**
     * 语义理解文本处理
     */
    public void handleNlu(Context context, String data) {
        try {
            JSONObject jsonObject = new JSONObject(data);
            if (jsonObject.optJSONObject("nlu") == null) return;
            JSONObject nlu = jsonObject.optJSONObject("nlu");
            if (nlu == null) return;
            String skill = nlu.optString("skill");
            AbstractDevice abstractDevice = null;
            switch (skill) {
                case CONTROL: // 通用控制
                    ControlSkill.handle(context, data);
                    break;
                case FRIDGE: // 冰箱控制
                    FridgeSkill.handle(context, data);
                    break;
                case HOT_DRINKER: // 云米饮水吧
                    if (mList != null && mList.get(4) != null) {
                        for (AbstractDevice device : mList.get(4).getList()) {
                            if (device.isOnline()) {
                                abstractDevice = device;
                                break;
                            }
                        }
                        ViomiHotDrinkerSkill.handle(FridgeApplication.getContext(), data, abstractDevice);
                    }
                    break;
                case WATER_PURIFIER: // 云米净水器
                    if (mList != null && mList.get(3) != null) {
                        for (AbstractDevice device : mList.get(3).getList()) {
                            if (device.isOnline()) {
                                abstractDevice = device;
                                break;
                            }
                        }
                        ViomiWaterPurifierSkill.handle(FridgeApplication.getContext(), data, abstractDevice);
                    }
                    break;
                case RANGE_HOOD: // 云米油烟机
                    if (mList != null && mList.get(2) != null) {
                        for (AbstractDevice device : mList.get(2).getList()) {
                            if (device.isOnline()) {
                                abstractDevice = device;
                                break;
                            }
                        }
                        ViomiHoodSkill.handle(FridgeApplication.getContext(), data, abstractDevice);
                    }
                    break;
                case DISH_WASHER: // 云米洗碗机
                    if (mList != null && mList.get(7) != null) {
                        for (AbstractDevice device : mList.get(7).getList()) {
                            if (device.isOnline()) {
                                abstractDevice = device;
                                break;
                            }
                        }
                        ViomiDishWasherSkill.handle(FridgeApplication.getContext(), data, abstractDevice);
                    }
                    break;
                case WASHER: // 云米洗衣机
                    if (mList != null && mList.get(8) != null) {
                        for (AbstractDevice device : mList.get(8).getList()) {
                            if (device.isOnline()) {
                                abstractDevice = device;
                                break;
                            }
                        }
                        ViomiWasherSkill.handle(FridgeApplication.getContext(), data, abstractDevice);
                    }
                    break;
                case OVEN: // 云米蒸烤箱
//                    if (mList != null && mList.get(1) != null) {
//                        for (AbstractDevice device : mList.get(1).getList()) {
//                            if (device.isOnline()) {
//                                abstractDevice = device;
//                                break;
//                            }
//                        }
//                        ViomiOvenSkill.handle(FridgeApplication.getContext(), data, abstractDevice);
//                    }
                    break;
                case VACUUM: // 云米扫地机
                    if (mList != null && mList.get(14) != null) {
                        for (AbstractDevice device : mList.get(14).getList()) {
                            if (device.isOnline()) {
                                abstractDevice = device;
                                break;
                            }
                        }
                        ViomiVacuumSkill.handle(FridgeApplication.getContext(), data, abstractDevice);
                    }
                    break;
                case FAN: // 云米风扇
                    if (mList != null && mList.get(11) != null) {
                        for (AbstractDevice device : mList.get(11).getList()) {
                            if (device.isOnline()) {
                                abstractDevice = device;
                                break;
                            }
                        }
                        ViomiFanSkill.handle(FridgeApplication.getContext(), data, abstractDevice);
                    }
                    break;
            }
        } catch (JSONException e) {
            logUtil.e(TAG, e.toString());
            e.printStackTrace();
        }
    }

    /**
     * 语义理解返回 Json 处理
     */
    public void handleNlp(Context context, String data) {
        try {
            JSONObject jsonObject = new JSONObject(data);
            String skillId = jsonObject.optString("skillId");
            logUtil.d(TAG, "handleNlp skillId：" + skillId);
            if (skillId == null || skillId.equals("")) return;
            switch (skillId) {
                case MUSIC_ID: // 音乐
                    MusicSkill.handle(context, data);
                    break;
                case NEWS_ID: // 新闻
                    NewsSkill.handle(context, data);
                    break;
                case CHINESE_STUDY_ID: // 国学
                    ChineseStudySkill.handle(context, data);
                    break;
                case JOKE_ID: // 笑话
                    JokeSkill.handle(context, data);
                    break;
                case STORY_ID: // 故事
                    StorySkill.handle(context, data);
                    break;
                case VIDEO_ID: // 影音娱乐
                    if (ToolUtil.checkApkExist(FridgeApplication.getContext(), "com.unilife.fridge.yunmi.media")) {
                        try {
                            ToolUtil.startOtherApp(FridgeApplication.getContext(), "com.unilife.fridge.yunmi.media", false);
                        } catch (Exception e) {
                            logUtil.e(TAG, e.toString());
                            e.printStackTrace();
                        }
                    }
                    break;
                case AITEK_COOKBOOK_ID: // AITEK 菜谱
                    CookBookSkill.handle(context, data);
                    break;
                case WULIAN_ID: // 物联设备控制
                    WuLianSkill.handle(context, data);
                    break;
            }
        } catch (JSONException e) {
            logUtil.e(TAG, e.toString());
            e.printStackTrace();
        }
    }

    public void setIotDevice(List<DeviceTypeEntity> list) {
        this.mList = list;
    }
}
