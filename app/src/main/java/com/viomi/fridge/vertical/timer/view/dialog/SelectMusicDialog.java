package com.viomi.fridge.vertical.timer.view.dialog;

import android.content.Context;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseDialog;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.PreferenceHelper;
import com.viomi.fridge.vertical.timer.view.adapter.SelectMusicAdapter;

/**
 * 选择铃声弹框
 * Created by nanquan on 2018/1/24.
 */
public class SelectMusicDialog extends BaseDialog implements View.OnClickListener, SelectMusicAdapter.OnItemClickListener {
    private Context mContext;
    private OnMusicClickListener mOnMusicClickListener;
    private OnConfirmClickListener mOnConfirmClickListener;
    private PreferenceHelper mPreferenceHelper;
    private SelectMusicAdapter mAdapter;

    public void setOnMusicClickListener(OnMusicClickListener onMusicClickListener) {
        this.mOnMusicClickListener = onMusicClickListener;
    }

    public void setOnConfirmClickListener(OnConfirmClickListener onConfirmClickListener) {
        mOnConfirmClickListener = onConfirmClickListener;
    }

    public interface OnMusicClickListener {
        void onMusicClick(int position);
    }

    public interface OnConfirmClickListener {
        void onConfirmClick(int position);
    }

    public SelectMusicDialog(Context context) {
        super(context, R.style.Dialog);
        setContentView(R.layout.dialog_select_music);
        this.mContext = context;
        this.mPreferenceHelper = new PreferenceHelper(mContext);
        setCancelable(false);
    }

    @Override
    public void initView() {
        TextView btnCancel = findViewById(R.id.tv_cancel);
        TextView btnConfirm = findViewById(R.id.tv_confirm);
        btnCancel.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);
        RecyclerView rvMusic = findViewById(R.id.rv_music);
        rvMusic.setLayoutManager(new LinearLayoutManager(mContext));
        rvMusic.addItemDecoration(new DividerItemDecoration(mContext, LinearLayout.VERTICAL));
        mAdapter = new SelectMusicAdapter(mContext,
                mPreferenceHelper.getSelectedTimeoutRing());
        mAdapter.setOnItemClickListener(this);
        rvMusic.setAdapter(mAdapter);
    }

    @Override
    public void onItemClick(int position) {
        if (mOnMusicClickListener != null) {
            mOnMusicClickListener.onMusicClick(position);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel: // 取消
                RxBus.getInstance().post(BusEvent.MSG_TIMER_STOP_MUSIC);
                cancel();
                break;
            case R.id.tv_confirm: // 确定
                RxBus.getInstance().post(BusEvent.MSG_TIMER_STOP_MUSIC);
                mPreferenceHelper.putSelectedTimeoutTing(mAdapter.getCheckPosition());
                if (mOnConfirmClickListener != null) {
                    mOnConfirmClickListener.onConfirmClick(mAdapter.getCheckPosition());
                }
                cancel();
                break;
        }
    }
}