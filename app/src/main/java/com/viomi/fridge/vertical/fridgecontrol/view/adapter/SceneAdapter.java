package com.viomi.fridge.vertical.fridgecontrol.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseRecyclerViewAdapter;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.entity.ChangeableScene;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 变温室场景选择适配器
 * Created by William on 2018/2/9.
 */
public class SceneAdapter extends BaseRecyclerViewAdapter<SceneAdapter.SceneHolder> {
    private List<ChangeableScene> mList;
    private int mType, mLocation = -1;// 0:普通模式；1:加号；2:减号

    public SceneAdapter(List<ChangeableScene> list, int type) {
        this.mList = list;
        this.mType = type;
        if (mList == null) mList = new ArrayList<>();
    }

    @Override
    public SceneHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_holder_changeable_scene, parent, false);
        return new SceneHolder(view, this);
    }

    @Override
    public void onBindViewHolder(SceneHolder holder, int position) {
        holder.textView.setText(mList.get(position).getScene());
        if (mType == 0) holder.imageView.setVisibility(View.GONE);
        else if (mType == 1)
            holder.imageView.setImageResource(R.drawable.icon_changeable_scene_add);
        else holder.imageView.setImageResource(R.drawable.icon_changeable_scene_delete);
        if (position == mLocation) holder.textView.setSelected(true);
        else holder.textView.setSelected(false);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class SceneHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.holder_scene_name)
        TextView textView;
        @BindView(R.id.holder_scene_icon)
        ImageView imageView;

        SceneHolder(View itemView, SceneAdapter adapter) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> adapter.onItemHolderClick(this, 500));
        }
    }

    public void setSelectedPosition(int location) {
        this.mLocation = location;
    }
}