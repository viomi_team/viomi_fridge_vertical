package com.viomi.fridge.vertical.home.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;

/**
 * 思必驰天气 Json
 * Created by William on 2018/8/15.
 */
public class NowEntity implements Serializable {
    @JSONField(name = "cond")
    private CondEntity cond;
    @JSONField(name = "fl")
    private String fl;
    @JSONField(name = "hum")
    private String hum;
    @JSONField(name = "pcpn")
    private String pcpn;
    @JSONField(name = "pres")
    private String pres;
    @JSONField(name = "tmp")
    private String tmp;
    @JSONField(name = "vis")
    private String vis;
    @JSONField(name = "wind")
    private WindEntity wind;

    public CondEntity getCond() {
        return cond;
    }

    public void setCond(CondEntity cond) {
        this.cond = cond;
    }

    public String getFl() {
        return fl;
    }

    public void setFl(String fl) {
        this.fl = fl;
    }

    public String getHum() {
        return hum;
    }

    public void setHum(String hum) {
        this.hum = hum;
    }

    public String getPcpn() {
        return pcpn;
    }

    public void setPcpn(String pcpn) {
        this.pcpn = pcpn;
    }

    public String getPres() {
        return pres;
    }

    public void setPres(String pres) {
        this.pres = pres;
    }

    public String getTmp() {
        return tmp;
    }

    public void setTmp(String tmp) {
        this.tmp = tmp;
    }

    public String getVis() {
        return vis;
    }

    public void setVis(String vis) {
        this.vis = vis;
    }

    public WindEntity getWind() {
        return wind;
    }

    public void setWind(WindEntity wind) {
        this.wind = wind;
    }
}
