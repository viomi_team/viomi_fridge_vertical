package com.viomi.fridge.vertical.administration.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 屏保时间适配器
 * Created by William on 2018/8/20.
 */
public class ScreenTimeAdapter extends BaseRecyclerViewAdapter<ScreenTimeAdapter.TimeHolder> {
    private List<String> mList;

    public ScreenTimeAdapter(List<String> list) {
        this.mList = list;
        if (mList == null) mList = new ArrayList<>();
    }

    @Override
    public TimeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_holder_screen_time, parent, false);
        return new TimeHolder(view, this);
    }

    @Override
    public void onBindViewHolder(TimeHolder holder, int position) {
        String time = mList.get(position);
        holder.textView.setText(time);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class TimeHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.screen_time_text)
        TextView textView;

        TimeHolder(View itemView, ScreenTimeAdapter adapter) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> adapter.onItemHolderClick(this, 1000));
        }
    }
}
