package com.viomi.fridge.vertical.message.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.viomi.fridge.vertical.message.view.dialog.MainMessageSettingFragment;
import com.viomi.fridge.vertical.message.view.dialog.TimeMessageSettingFragment;

/**
 * Created by nanquan on 2018/2/7.
 */
public class MessageSettingPagerAdapter extends FragmentPagerAdapter {
    private MainMessageSettingFragment mMainFragment;
    private TimeMessageSettingFragment mTimeFragment;

    public MessageSettingPagerAdapter(FragmentManager fm,
                                      MainMessageSettingFragment mainFragment,
                                      TimeMessageSettingFragment timeFragment) {
        super(fm);
        this.mMainFragment = mainFragment;
        this.mTimeFragment = timeFragment;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 1) {
            return mTimeFragment;
        }
        return mMainFragment;
    }

    @Override
    public int getCount() {
        return 2;
    }
}