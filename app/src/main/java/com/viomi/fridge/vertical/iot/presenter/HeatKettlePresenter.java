package com.viomi.fridge.vertical.iot.presenter;

import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.iot.contract.HeatKettleContract;
import com.viomi.fridge.vertical.iot.model.http.entity.HeatKettleProp;
import com.viomi.fridge.vertical.iot.model.repository.HeatKettleRepository;

import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * 即热饮水吧 Presenter
 * Created by William on 2018/2/8.
 */
public class HeatKettlePresenter implements HeatKettleContract.Presenter {
    private static final String TAG = HeatKettlePresenter.class.getSimpleName();
    private CompositeSubscription mCompositeSubscription;

    @Nullable
    private HeatKettleContract.View mView;

    @Override
    public void subscribe(HeatKettleContract.View view) {
        this.mView = view;
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public void unSubscribe() {
        mView = null;
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
    }

    @Override
    public void getProp(String did) {
        Subscription subscription = Observable.interval(0, 5, TimeUnit.SECONDS)
                .onBackpressureDrop() // 背压处理
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .flatMap(aLong -> HeatKettleRepository.getProp(did))
                .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                .onTerminateDetach()
                .map(rpcResult -> new HeatKettleProp(rpcResult.getList()))
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(heatKettleProp -> {
                    if (heatKettleProp != null && mView != null) mView.refreshUi(heatKettleProp);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void setTemp(String did, int temp) {
        Subscription subscription = HeatKettleRepository.setTemp(did, temp)
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (mView != null) mView.setIsSetting();
                }, throwable -> {
                    if (mView != null) mView.setIsSetting();
                    logUtil.e(TAG, throwable.getMessage());
                });
        mCompositeSubscription.add(subscription);
    }
}