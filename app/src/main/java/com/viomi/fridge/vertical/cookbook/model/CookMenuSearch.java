package com.viomi.fridge.vertical.cookbook.model;

import java.util.List;

/**
 * 菜谱搜索结果
 * Created by nanquan on 2018/2/12.
 */
public class CookMenuSearch {
    private int curPage;
    private int total;
    private List<CookMenuDetail> list;

    public int getCurPage() {
        return curPage;
    }

    public void setCurPage(int curPage) {
        this.curPage = curPage;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<CookMenuDetail> getList() {
        return list;
    }

    public void setList(List<CookMenuDetail> list) {
        this.list = list;
    }
}