package com.viomi.fridge.vertical.iot.model.repository;

import com.miot.api.MiotManager;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.http.ApiClient;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.iot.model.http.entity.RPCResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import rx.Observable;

/**
 * 即热饮水吧相关 Api
 * Created by William on 2018/2/8.
 */
public class HeatKettleRepository {
    private static final String TAG = HeatKettleRepository.class.getSimpleName();

    /**
     * miOpen
     */
    public static Observable<RPCResult> getProp(String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put("setup_tempe");
            jsonArray.put("tds");
            jsonArray.put("water_remain_time");
            jsonArray.put("flush_time");
            jsonArray.put("custom_tempe1");
            jsonArray.put("min_set_tempe");
            jsonArray.put("work_mode");
            jsonArray.put("run_status");
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
            logUtil.e(TAG, e.toString());
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 温水键温度设置
     */
    public static Observable<RPCResult> setTemp(String did, int temp) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_tempe_setup");
            jsonObject.put("did", did);
            jsonObject.put("id", 2);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(1);
            jsonArray.put(temp);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
            logUtil.e(TAG, e.toString());
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 工作模式
     */
    public static String switchMode(int work_mode) {
        String str = FridgeApplication.getContext().getResources().getString(R.string.iot_heat_kettle_temp);
        switch (work_mode) {
            case 0:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_heat_kettle_temp_normal);
                break;
            case 1:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_heat_kettle_temp_warm);
                break;
            case 2:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_heat_kettle_temp_hot);
                break;
        }
        return str;
    }

    /**
     * 根据温度显示对应提示
     */
    public static String switchTempTip(int temp) {
        String str = temp + FridgeApplication.getContext().getResources().getString(R.string.fridge_temp_unit);
        switch (temp) {
            case 40:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_tip_40);
                break;
            case 50:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_tip_50);
                break;
            case 60:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_tip_60);
                break;
            case 75:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_tip_75);
                break;
            case 80:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_tip_80);
                break;
            case 85:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_tip_85);
                break;
            case 90:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_temp_tip_90);
                break;
        }
        return str;
    }
}
