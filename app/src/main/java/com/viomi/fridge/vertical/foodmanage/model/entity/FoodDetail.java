package com.viomi.fridge.vertical.foodmanage.model.entity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * 食材相关信息
 * Created by William on 2018/1/10.
 */
public class FoodDetail implements Serializable {
    private String foodType;// 食材分类
    private String name;// 食材名称
    private String roomType;// 存放类型(0: 冷藏室；1: 变温室；2: 冷冻室)
    private String addTime;// 食材添加时间
    private String imgPath;// 食材绘图保存目录
    private String deadLine;// 食材到期时间
    private String isPush;// 是否已推送
    private boolean isSelected = false;

    @Override
    public String toString() {
        return "{name:" + name + ",roomType:" + roomType + ",addTime:" + addTime + ",imgPath:" + imgPath +
                ",deadLine" + deadLine + ",isPush" + isPush + "}";
    }

    public String toJson() {
        JSONObject object=new JSONObject();
        try {
            object.put("name",name);
            object.put("addTime",addTime);
            object.put("type",foodType);
            object.put("deadLine",deadLine);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object.toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getDeadLine() {
        return deadLine;
    }

    public void setDeadLine(String deadLine) {
        this.deadLine = deadLine;
    }

    public String getIsPush() {
        return isPush;
    }

    public void setIsPush(String isPush) {
        this.isPush = isPush;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getFoodType() {
        return foodType;
    }

    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }
}