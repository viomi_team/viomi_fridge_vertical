package com.viomi.fridge.vertical.messageboard.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.album.activity.AlbumPreviewActivity;
import com.viomi.fridge.vertical.common.base.BaseActivity;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.messageboard.contract.MessageBoardListContract;
import com.viomi.fridge.vertical.messageboard.model.entity.MessageBoardEntity;
import com.viomi.fridge.vertical.messageboard.view.adapter.MessageBoardAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 历史留言板 Activity
 * Created by William on 2018/4/11.
 */
public class MessageBoardListActivity extends BaseActivity implements MessageBoardListContract.View {
    private static final String TAG = MessageBoardListActivity.class.getSimpleName();
    private MessageBoardAdapter mAdapter;// 列表适配器
    private List<MessageBoardEntity> mList;// 留言板集合

    @Inject
    MessageBoardListContract.Presenter mPresenter;

    @BindView(R.id.title_bar_edit)
    LinearLayout mEditLinearLayout;// 编辑
    @BindView(R.id.title_bar_delete)
    LinearLayout mDeleteLinearLayout;// 删除

    @BindView(R.id.message_board_list)
    RecyclerView mRecyclerView;// 列表

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        layoutId = R.layout.activity_message_board_list;
        mTitle = getResources().getString(R.string.home_message_board);
        super.onCreate(savedInstanceState);
        mPresenter.subscribe(this);// 订阅
        mRecyclerView.setLayoutManager(new GridLayoutManager(MessageBoardListActivity.this, 3));// 设置布局管理器
        mList = new ArrayList<>();
        mAdapter = new MessageBoardAdapter(mList);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener((parent, view, position, id) -> {
            if (mAdapter.isEdit()) { // 编辑模式
                mList.get(position).setSelected(!mList.get(position).isSelected());
                mAdapter.notifyItemChanged(position);
            } else { // 预览
                ArrayList<String> list = new ArrayList<>();
                for (MessageBoardEntity entity : mList)
                    list.add(entity.getFile().getAbsolutePath());
                Intent intent = new Intent(MessageBoardListActivity.this, AlbumPreviewActivity.class);
                intent.putStringArrayListExtra(AppConstants.PHOTO_LIST, list);
                intent.putExtra(AppConstants.PHOTO_INDEX, position);
                startActivity(intent);
            }
        });
        if (mPresenter != null) mPresenter.loadList(mList);// 读取相册列表
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) {
            mPresenter.unSubscribe();// 取消订阅
            mPresenter = null;
        }
        if (mList != null) {
            mList.clear();
            mList = null;
        }
        if (mAdapter != null) mAdapter = null;
        RxBus.getInstance().post(BusEvent.MSG_MESSAGE_BOARD_UPDATE);// 留言板更新
    }

    @OnClick(R.id.title_bar_edit)
    public void edit() { // 编辑
        mEditLinearLayout.setVisibility(View.GONE);
        mDeleteLinearLayout.setVisibility(View.VISIBLE);
        mAdapter.setEdit(true);// 编辑模式
        mAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.title_bar_delete)
    public void delete() { // 删除
        mEditLinearLayout.setVisibility(View.VISIBLE);
        mDeleteLinearLayout.setVisibility(View.GONE);
        mAdapter.setEdit(false);
        if (mPresenter != null) mPresenter.delete(mList);
    }

    @Override
    public void display() {
        if (mList == null || mList.size() == 0) {
            finish();
            return;
        }
        logUtil.d(TAG, "size = " + mList.size());
    }

    @Override
    public void notifyDataChanged() {
        if (mAdapter != null) mAdapter.notifyDataSetChanged();
    }

    @Override
    public void notifyDataRemoved(int size) {
        if (mAdapter != null) mAdapter.notifyItemRangeRemoved(0, size);
    }

    @Override
    public void notifyDataInserted(int size) {
        if (mAdapter != null) mAdapter.notifyItemRangeInserted(0, size);
    }
}
