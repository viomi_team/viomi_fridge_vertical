package com.viomi.fridge.vertical.fridgecontrol.defined.action;

import com.viomi.fridge.vertical.fridgecontrol.defined.ViomiDefined;
import com.viomi.fridge.vertical.fridgecontrol.defined.property.OutdoorTemp;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetOutdoorTemp extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setOutdoorTemp.toActionType();

    public SetOutdoorTemp() {
        super(TYPE);

        super.addArgument(OutdoorTemp.TYPE.toString());
    }
}