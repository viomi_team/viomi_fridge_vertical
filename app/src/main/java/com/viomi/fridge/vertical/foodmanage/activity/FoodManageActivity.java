package com.viomi.fridge.vertical.foodmanage.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeController;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseActivity;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.StatisticsUtil;
import com.viomi.fridge.vertical.common.util.ToastUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.foodmanage.contract.FoodManageContract;
import com.viomi.fridge.vertical.foodmanage.model.entity.DefinedFood;
import com.viomi.fridge.vertical.foodmanage.model.entity.FoodDetail;
import com.viomi.fridge.vertical.foodmanage.view.adapter.FoodManageSubTypeAdapter;
import com.viomi.fridge.vertical.foodmanage.view.adapter.FoodManageTypeAdapter;
import com.viomi.fridge.vertical.foodmanage.view.adapter.FoodPagerAdapter;
import com.viomi.fridge.vertical.foodmanage.view.fragment.FoodManageFragment;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;
import com.viomi.fridge.vertical.fridgecontrol.model.repository.FridgeRepository;
import com.viomi.fridge.vertical.fridgecontrol.model.repository.MiotRepository;
import com.viomi.widget.popupwindow.CommonPopupWindow;
import com.viomi.widget.wheelview.adapter.ArrayWheelAdapter;
import com.viomi.widget.wheelview.widget.WheelView;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindArray;
import butterknife.BindView;
import butterknife.OnClick;
import rx.Subscription;

import static com.viomi.fridge.vertical.common.util.StatisticsUtil.EVENT_ENTER_FOODMANAGER;

/**
 * 食材管理 Activity
 * Created by William on 2018/1/19.
 */
public class FoodManageActivity extends BaseActivity implements CommonPopupWindow.ViewInterface,
        FoodManageContract.View, FoodManageFragment.OnFoodEditListener, ViewPager.OnPageChangeListener {
    private static final String TAG = FoodManageActivity.class.getSimpleName();
    private static final String Path = Environment.getExternalStorageDirectory().toString() + "/" + AppConstants.PATH + AppConstants.FOOD_MANAGE_PATH;// 保存路径
    private static final String Cache_Path = Environment.getExternalStorageDirectory().toString() + "/" + AppConstants.PATH + AppConstants.CAMERA_CACHE;// 拍照缓存路径
    private String mModel;// 设备 model
    private Subscription mSubscription;// 统一管理消息订阅
    private int mType, mFoodType = -1, mPosition;// 仓室类型，食材分类，当前仓室位置
    private CommonPopupWindow mFoodTypePopupWindow, mFoodSubTypePopupWindow;// 分类，子类 PopupWindow
    private String mImagePath = "null";// 拍照文件路径
    private FoodDetail mFoodDetail;// 食材信息
    private DefinedFood mDefineFood;// 推荐保质期信息
    private File mCameraFile, mCropFile;// 拍照，裁剪保存文件
    private int mSelectedYear, mSelectedMonth, mSelectedDay;// 选择年份，月份，日
    private int mColdFoodSize, mCCFoodSize, mChangeableFoodSize, mFreezingFoodSize;// 各仓室食材数量

    @Inject
    FoodManageContract.Presenter mPresenter;

    @BindView(R.id.food_manage_tab)
    TabLayout mTabLayout;// 仓室 Tab

    @BindView(R.id.food_manage_viewpager)
    ViewPager mViewPager;

    @BindView(R.id.food_manage_layout)
    RelativeLayout mFoodRelativeLayout;// 食材编辑或添加布局

    @BindView(R.id.food_manage_img)
    SimpleDraweeView mSimpleDraweeView;// 食材图片

    @BindView(R.id.food_manage_type_layout)
    FrameLayout mTypeFrameLayout;// 食材分类布局
    @BindView(R.id.food_manage_sub_type_layout)
    FrameLayout mSubTypeFrameLayout;// 食材子类布局
    @BindView(R.id.food_manage_time_layout)
    FrameLayout mTimeFrameLayout;// 保质期布局

    @BindView(R.id.title_bar_edit)
    LinearLayout mEditLinearLayout;// 编辑
    @BindView(R.id.title_bar_delete)
    LinearLayout mDeleteLinearLayout;// 删除

    @BindView(R.id.food_manage_cold)
    TextView mColdTextView;// 冷藏室
    @BindView(R.id.food_manage_cc)
    TextView mCCTextView;// 冷藏变温区
    @BindView(R.id.food_manage_changeable)
    TextView mChangeableTextView;// 变温室
    @BindView(R.id.food_manage_freezing)
    TextView mFreezingTextView;// 冷冻室
    @BindView(R.id.food_manage_type)
    TextView mTypeTextView;// 食材分类
    @BindView(R.id.food_manage_sub_type)
    TextView mSubTypeTextView;// 食材子类
    @BindView(R.id.food_manage_time)
    TextView mFoodTimeTextView;// 保质期

    @BindView(R.id.food_manage_name_arrow)
    ImageView mTypeArrowImageView;// 食材分类箭头
    @BindView(R.id.food_manage_time_arrow)
    ImageView mTimeArrowImageView;// 保质期箭头
    @BindView(R.id.food_manage_sub_type_arrow)
    ImageView mSubTypeArrowImageView;// 食材子类箭头
    @BindView(R.id.food_manage_add)
    ImageView mAddImageView;// 添加食材

    @BindArray(R.array.food_manage_meat)
    String[] mMeatArray;// 肉荤类
    @BindArray(R.array.food_manage_egg)
    String[] mEggArray;// 豆蛋奶
    @BindArray(R.array.food_manage_fruits)
    String[] mFruitsArray;// 水果类
    @BindArray(R.array.food_manage_fungus)
    String[] mFungusArray;// 菌类
    @BindArray(R.array.food_manage_vegetables)
    String[] mVegetablesArray;// 蔬菜类

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        layoutId = R.layout.activity_food_manage;
        mTitle = getResources().getString(R.string.food_management_title);
        super.onCreate(savedInstanceState);
        mPresenter.subscribe(this);// 订阅
        StatisticsUtil.writeStatistics(FoodManageActivity.this, EVENT_ENTER_FOODMANAGER, null);
        // 根据 Model 匹配 Ui
        mModel = FridgePreference.getInstance().getModel();
        mPosition = 0;
        String titles[];
        if (mModel.equals(AppConstants.MODEL_X2) || mModel.equals(AppConstants.MODEL_X3)
                || mModel.equals(AppConstants.MODEL_X4) || mModel.equals(AppConstants.MODEL_JD)) {
            mChangeableTextView.setVisibility(View.GONE);
            mCCTextView.setVisibility(View.GONE);
            titles = new String[]{getResources().getString(R.string.food_management_room_cold), getResources().getString(R.string.food_management_room_freezing)};
        } else {
            titles = new String[]{getResources().getString(R.string.food_management_room_cold), getResources().getString(R.string.food_management_room_cc), getResources().getString(R.string.food_management_room_changeable), getResources().getString(R.string.food_management_room_freezing)};
        }

        FoodManageFragment coldFragment = FoodManageFragment.newInstance(0);// 冷藏室
        FoodManageFragment changeableFragment = FoodManageFragment.newInstance(1);// 变温室
        FoodManageFragment freezingFragment = FoodManageFragment.newInstance(2);// 冷冻室
        FoodManageFragment ccFragment = FoodManageFragment.newInstance(3);// 冷藏变温区

        coldFragment.setOnFoodEditListener(this);
        changeableFragment.setOnFoodEditListener(this);
        freezingFragment.setOnFoodEditListener(this);
        ccFragment.setOnFoodEditListener(this);

        List<FoodManageFragment> list = new ArrayList<>();
        if (mModel.equals(AppConstants.MODEL_X2) || mModel.equals(AppConstants.MODEL_X3)
                || mModel.equals(AppConstants.MODEL_X4) || mModel.equals(AppConstants.MODEL_JD)) {
            list.add(coldFragment);
            list.add(freezingFragment);
        } else {
            list.add(coldFragment);
            list.add(ccFragment);
            list.add(changeableFragment);
            list.add(freezingFragment);
        }

        FoodPagerAdapter adapter = new FoodPagerAdapter(getSupportFragmentManager(), list, titles);
        mViewPager.setAdapter(adapter);
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.setOffscreenPageLimit(list.size());
        mViewPager.addOnPageChangeListener(this);

        mSubscription = RxBus.getInstance().subscribe(busEvent -> {
            switch (busEvent.getMsgId()) {
                case BusEvent.MSG_FOOD_UPDATE: // 食材更新
                    runOnUiThread(() -> {
                        if (busEvent.getMsgObject() == null) return;
                        List<FoodDetail> foodList = (List<FoodDetail>) busEvent.getMsgObject();
                        logUtil.d(TAG, foodList.size() + "");
                        if (foodList.size() >= 50)
                            mAddImageView.setVisibility(View.GONE);
                        else mAddImageView.setVisibility(View.VISIBLE);
                        mColdFoodSize = 0;
                        mCCFoodSize = 0;
                        mChangeableFoodSize = 0;
                        mFreezingFoodSize = 0;
                        for (FoodDetail detail : foodList) {
                            switch (detail.getRoomType()) {
                                case "0": // 冷藏室
                                    mColdFoodSize++;
                                    break;
                                case "1": // 变温室
                                    mChangeableFoodSize++;
                                    break;
                                case "2": // 冷冻室
                                    mFreezingFoodSize++;
                                    break;
                                case "3": // 冷藏变温区
                                    mCCFoodSize++;
                                    break;
                            }
                        }
                        if (mPosition == 0) {
                            if (mColdFoodSize > 0 && mEditLinearLayout.getVisibility() != View.VISIBLE && mDeleteLinearLayout.getVisibility() != View.VISIBLE) {
                                mEditLinearLayout.setVisibility(View.VISIBLE);
                            } else if (mColdFoodSize <= 0)
                                mEditLinearLayout.setVisibility(View.GONE);
                        } else if (mPosition == 1) {
                            if (mModel.equals(AppConstants.MODEL_X2) || mModel.equals(AppConstants.MODEL_X3) || mModel.equals(AppConstants.MODEL_X4) ||
                                    mModel.equals(AppConstants.MODEL_JD)) {
                                if (mFreezingFoodSize > 0 && mEditLinearLayout.getVisibility() != View.VISIBLE && mDeleteLinearLayout.getVisibility() != View.VISIBLE) {
                                    mEditLinearLayout.setVisibility(View.VISIBLE);
                                } else if (mFreezingFoodSize <= 0)
                                    mEditLinearLayout.setVisibility(View.GONE);
                            } else if (mCCFoodSize > 0 && mEditLinearLayout.getVisibility() != View.VISIBLE && mDeleteLinearLayout.getVisibility() != View.VISIBLE)
                                mEditLinearLayout.setVisibility(View.VISIBLE);
                            else if (mCCFoodSize <= 0) mEditLinearLayout.setVisibility(View.GONE);
                        } else if (mPosition == 2) {
                            if (mChangeableFoodSize > 0 && mEditLinearLayout.getVisibility() != View.VISIBLE && mDeleteLinearLayout.getVisibility() != View.VISIBLE)
                                mEditLinearLayout.setVisibility(View.VISIBLE);
                            else if (mChangeableFoodSize <= 0)
                                mEditLinearLayout.setVisibility(View.GONE);
                        } else if (mPosition == 3) {
                            if (mFreezingFoodSize > 0 && mEditLinearLayout.getVisibility() != View.VISIBLE && mDeleteLinearLayout.getVisibility() != View.VISIBLE) {
                                mEditLinearLayout.setVisibility(View.VISIBLE);
                            } else if (mFreezingFoodSize <= 0)
                                mEditLinearLayout.setVisibility(View.GONE);
                        }
                    });
                    break;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstants.CODE_CAMERA && resultCode == RESULT_OK) { // 拍照回调
            Intent intent = new Intent("com.android.camera.action.CROP");
            File path = new File(Path);
            if (!path.exists()) logUtil.d(TAG, "create FoodManage path:" + path.mkdirs());
            Uri inputUri, cropUri;
            mCropFile = new File(Path, System.currentTimeMillis() + ".jpg");// 裁剪保存图片
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) { // Android N 以上
                inputUri = FileProvider.getUriForFile(FridgeApplication.getContext(), "com.viomi.fridge.vertical.fileprovider", mCameraFile);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            } else {
                inputUri = Uri.fromFile(mCameraFile);
            }
            cropUri = Uri.fromFile(mCropFile);
            intent.setDataAndType(inputUri, "image/*");
            intent.putExtra("crop", "true");// 可裁剪
            intent.putExtra("aspectX", 1);// 裁剪的宽比例
            intent.putExtra("aspectY", 1);// 裁剪的高比例
            intent.putExtra("outputX", 268);// 裁剪的宽度
            intent.putExtra("outputY", 268);// 裁剪的高度
            intent.putExtra("scale", true);// 支持缩放
            intent.putExtra(MediaStore.EXTRA_OUTPUT, cropUri);// 将裁剪的结果输出到指定的 Uri
            intent.putExtra("return-data", false);// 若为 true 则表示返回数据
            intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());// 裁剪成的图片的格式
            intent.putExtra("noFaceDetection", false);// 去除默认的人脸识别，否则和剪裁匡重叠
            startActivityForResult(intent, AppConstants.CODE_CAMERA_CROP);
        } else if (requestCode == AppConstants.CODE_CAMERA_CROP && resultCode == RESULT_OK) { // 裁剪回调
            mImagePath = mCropFile.getAbsolutePath();
            loadFoodImage(Uri.fromFile(mCropFile));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) {
            mPresenter.unSubscribe();// 取消订阅
            mPresenter = null;
        }
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
        if (mFoodTypePopupWindow != null && mFoodTypePopupWindow.isShowing()) {
            mFoodTypePopupWindow.dismiss();
            mFoodTypePopupWindow = null;
        }
        if (mFoodSubTypePopupWindow != null && mFoodSubTypePopupWindow.isShowing()) {
            mFoodSubTypePopupWindow.dismiss();
            mFoodSubTypePopupWindow = null;
        }
        StatisticsUtil.writeStatistics(FoodManageActivity.this, StatisticsUtil.EVENT_EXIT_FOODMANAGER, null);
        RxBus.getInstance().post(BusEvent.MSG_DELETE_CAMERA_CACHE);// 删除拍照缓存
    }

    @OnClick(R.id.food_manage_add)
    public void addFood() { // 添加食材
        if (isRepeatedClick()) return;
        showFoodAddDialog();
        RxBus.getInstance().post(BusEvent.MSG_FOOD_MANAGE_EDIT_CHANGE);
        mDeleteLinearLayout.setVisibility(View.GONE);
        if (mPosition == 0) {
            if (mColdFoodSize > 0 && mEditLinearLayout.getVisibility() != View.VISIBLE && mDeleteLinearLayout.getVisibility() != View.VISIBLE) {
                mEditLinearLayout.setVisibility(View.VISIBLE);
            } else if (mColdFoodSize <= 0) mEditLinearLayout.setVisibility(View.GONE);
        } else if (mPosition == 1) {
            if (mModel.equals(AppConstants.MODEL_X2) || mModel.equals(AppConstants.MODEL_X3) || mModel.equals(AppConstants.MODEL_X4) ||
                    mModel.equals(AppConstants.MODEL_JD)) {
                if (mFreezingFoodSize > 0 && mEditLinearLayout.getVisibility() != View.VISIBLE && mDeleteLinearLayout.getVisibility() != View.VISIBLE) {
                    mEditLinearLayout.setVisibility(View.VISIBLE);
                } else if (mFreezingFoodSize <= 0)
                    mEditLinearLayout.setVisibility(View.GONE);
            } else if (mCCFoodSize > 0 && mEditLinearLayout.getVisibility() != View.VISIBLE && mDeleteLinearLayout.getVisibility() != View.VISIBLE)
                mEditLinearLayout.setVisibility(View.VISIBLE);
            else if (mCCFoodSize <= 0) mEditLinearLayout.setVisibility(View.GONE);
        } else if (mPosition == 2) {
            if (mChangeableFoodSize > 0 && mEditLinearLayout.getVisibility() != View.VISIBLE && mDeleteLinearLayout.getVisibility() != View.VISIBLE)
                mEditLinearLayout.setVisibility(View.VISIBLE);
            else if (mChangeableFoodSize <= 0)
                mEditLinearLayout.setVisibility(View.GONE);
        } else if (mPosition == 3) {
            if (mFreezingFoodSize > 0 && mEditLinearLayout.getVisibility() != View.VISIBLE && mDeleteLinearLayout.getVisibility() != View.VISIBLE) {
                mEditLinearLayout.setVisibility(View.VISIBLE);
            } else if (mFreezingFoodSize <= 0)
                mEditLinearLayout.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.food_manage_img)
    public void editPhoto() { // 编辑食材照片
        if (isRepeatedClick()) return;
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);// 启动系统相机
        if (intent.resolveActivity(getPackageManager()) != null) { // 判断是否有相机应用
            File path = new File(Cache_Path);
            if (!path.exists()) logUtil.d(TAG, "create cache path:" + path.mkdirs());
            mCameraFile = new File(Cache_Path, System.currentTimeMillis() + ".jpg");// 拍照保存文件
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) { // 7.0及以上
                Uri uri = FileProvider.getUriForFile(FridgeApplication.getContext(), "com.viomi.fridge.vertical.fileprovider", mCameraFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            } else {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mCameraFile));
            }
            startActivityForResult(intent, AppConstants.CODE_CAMERA);
        }
    }

    @OnClick({R.id.food_manage_cold, R.id.food_manage_changeable, R.id.food_manage_freezing, R.id.food_manage_cc})
    public void choseRoom(TextView textView) { // 仓室选择
        switch (textView.getId()) {
            case R.id.food_manage_cold: // 冷藏室
                mType = AppConstants.ROOM_TYPE_COLD;
                mViewPager.setCurrentItem(0, true);
                break;
            case R.id.food_manage_changeable: // 变温室
                mType = AppConstants.ROOM_TYPE_CHANGEABLE;
                mViewPager.setCurrentItem(2, true);
                break;
            case R.id.food_manage_freezing: // 冷冻室
                mType = AppConstants.ROOM_TYPE_FREEZING;
                if (mModel.equals(AppConstants.MODEL_X2) || mModel.equals(AppConstants.MODEL_X3)
                        || mModel.equals(AppConstants.MODEL_X4) || mModel.equals(AppConstants.MODEL_JD))
                    mViewPager.setCurrentItem(1, true);
                else
                    mViewPager.setCurrentItem(3, true);
                break;
            case R.id.food_manage_cc: // 冷藏变温区
                mType = AppConstants.ROOM_TYPE_CC;
                mViewPager.setCurrentItem(1, true);
                break;
        }
        setTabUi(mType);// 设置 Tab Ui
        if (mDefineFood != null) matchPeriod(mDefineFood);
    }

    @OnClick(R.id.food_manage_type_layout)
    public void choseFoodType(FrameLayout frameLayout) { // 食材分类选择
        if (isRepeatedClick()) return;
        Animation animation = new RotateAnimation(0, 90);
        animation.setDuration(200);
        animation.setFillAfter(true);
        mTypeArrowImageView.startAnimation(animation);
        mTimeFrameLayout.setVisibility(View.GONE);
        mSubTypeFrameLayout.setVisibility(View.GONE);
        mTypeFrameLayout.setBackgroundResource(R.drawable.bg_food_manage_type_top_corner);
        mFoodTypePopupWindow = new CommonPopupWindow.Builder(FoodManageActivity.this)
                .setView(R.layout.popup_window_food_type)
                .setOutsideTouchable(true)
                .setWidthAndHeight(frameLayout.getMeasuredWidth(), ViewGroup.LayoutParams.WRAP_CONTENT)
                .setViewOnclickListener(this)
                .create();
        mFoodTypePopupWindow.showAsDropDown(frameLayout, 0, 0);
        mFoodTypePopupWindow.setOnDismissListener(() -> {
            if (!mSubTypeTextView.getText().toString().equals(""))
                mSubTypeFrameLayout.setVisibility(View.VISIBLE);
            Animation aim = new RotateAnimation(90, 0);
            aim.setDuration(200);
            aim.setFillAfter(true);
            mTypeArrowImageView.startAnimation(aim);
            mTimeFrameLayout.setVisibility(View.VISIBLE);
            mTypeFrameLayout.setBackgroundResource(R.drawable.bg_common_white_black_10_corner);
        });
    }

    @OnClick(R.id.food_manage_sub_type_layout)
    public void choseFoodSubType(FrameLayout frameLayout) { // 食材分类子类选择
        if (isRepeatedClick()) return;
        Animation animation = new RotateAnimation(0, 90);
        animation.setDuration(200);
        animation.setFillAfter(true);
        mSubTypeArrowImageView.startAnimation(animation);
        mTimeFrameLayout.setVisibility(View.GONE);
        mSubTypeFrameLayout.setBackgroundResource(R.drawable.bg_food_manage_type_top_corner);
        mFoodSubTypePopupWindow = new CommonPopupWindow.Builder(FoodManageActivity.this)
                .setView(R.layout.popup_window_food_sub_type)
                .setOutsideTouchable(true)
                .setWidthAndHeight(frameLayout.getMeasuredWidth(), ViewGroup.LayoutParams.WRAP_CONTENT)
                .setViewOnclickListener(this)
                .create();
        mFoodSubTypePopupWindow.showAsDropDown(frameLayout, 0, 0);
        mFoodSubTypePopupWindow.setOnDismissListener(() -> {
            Animation aim = new RotateAnimation(90, 0);
            aim.setDuration(200);
            aim.setFillAfter(true);
            mSubTypeArrowImageView.startAnimation(aim);
            mTimeFrameLayout.setVisibility(View.VISIBLE);
            mSubTypeFrameLayout.setBackgroundResource(R.drawable.bg_common_white_black_10_corner);
        });
    }

    @OnClick(R.id.food_manage_time_layout)
    public void choseFoodTime(FrameLayout frameLayout) { // 保质期选择
        if (isRepeatedClick()) return;
        Animation animation = new RotateAnimation(0, 90);
        animation.setDuration(200);
        animation.setFillAfter(true);
        mTimeArrowImageView.startAnimation(animation);
        mTimeFrameLayout.setBackgroundResource(R.drawable.bg_food_manage_type_top_corner);
        CommonPopupWindow foodTimePopupWindow = new CommonPopupWindow.Builder(FoodManageActivity.this)
                .setView(R.layout.popup_window_food_manage_time)
                .setOutsideTouchable(true)
                .setWidthAndHeight(frameLayout.getMeasuredWidth(), ViewGroup.LayoutParams.WRAP_CONTENT)
                .setViewOnclickListener(this)
                .create();
        foodTimePopupWindow.showAsDropDown(frameLayout, 0, 0);
        foodTimePopupWindow.setOnDismissListener(() -> {
            Animation aim = new RotateAnimation(90, 0);
            aim.setDuration(200);
            aim.setFillAfter(true);
            mTimeArrowImageView.startAnimation(aim);
            mTimeFrameLayout.setBackgroundResource(R.drawable.bg_common_white_black_10_corner);
            long time = System.currentTimeMillis() + 24L * 60L * 60L * 1000L;
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(time);
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH) + 1;
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            if (mSelectedYear == 0) mSelectedYear = year;
            if (mSelectedMonth == 0) mSelectedMonth = month;
            if (mSelectedDay == 0) mSelectedDay = day;
            String str = getResources().getString(R.string.food_management_time) +
                    mSelectedYear + getResources().getString(R.string.year) +
                    (mSelectedMonth < 10 ? "0" + mSelectedMonth : mSelectedMonth) + getResources().getString(R.string.month) +
                    (mSelectedDay < 10 ? "0" + mSelectedDay : mSelectedDay) + getResources().getString(R.string.day);
            mFoodTimeTextView.setText(str);
        });
    }

    @OnClick(R.id.food_manage_cancel)
    public void cancel() { // 取消
        if (mPresenter != null && !mImagePath.equals("null")) mPresenter.deleteImage(mImagePath);
        dismissFoodDialog();
    }

    @OnClick(R.id.food_manage_confirm)
    public void confirm() { // 确定
//        if (mFoodDetail == null && mImagePath == null) { // 食材图片
//            ToastUtil.showCenter(FridgeApplication.getContext(), getResources().getString(R.string.food_management_no_photo_tip));
//            return;
//        }
        if (mFoodType == -1) { // 食材分类未选
            ToastUtil.showCenter(FridgeApplication.getContext(), getResources().getString(R.string.food_management_no_add_type_tip));
            return;
        }
        if (mSelectedYear == 0 && mSelectedMonth == 0 && mSelectedDay == 0) { // 保质期未选择
            ToastUtil.showCenter(FridgeApplication.getContext(), getResources().getString(R.string.food_management_no_time_tip));
            return;
        }
        boolean isEdit;
        String id = null;
        if (mFoodDetail != null) { // 编辑
            isEdit = true;
            id = mFoodDetail.getAddTime();
            // 删除原先图片
            if (!mImagePath.equals("null"))
                logUtil.d(TAG, "delete result " + new File(mFoodDetail.getImgPath()).delete());
        } else {
            isEdit = false;
            mFoodDetail = new FoodDetail();
        }
        mFoodDetail.setRoomType(String.valueOf(mType));// 仓室类型
        mFoodDetail.setFoodType(mTypeTextView.getText().toString());// 食材分类
        mFoodDetail.setName(mSubTypeTextView.getText().toString());// 食材名称
        mFoodDetail.setAddTime(String.valueOf(System.currentTimeMillis()));// 添加时间
        // 食材图片
        if (isEdit) {
            if (!mImagePath.equals("null")) mFoodDetail.setImgPath(mImagePath);
        } else {
            mFoodDetail.setImgPath(mImagePath);
        }
        mFoodDetail.setIsPush("false");// 是否已推送过期
        mFoodDetail.setSelected(false);
        Calendar calendar = Calendar.getInstance();
        calendar.set(mSelectedYear, mSelectedMonth - 1, mSelectedDay);// 过期时间
        mFoodDetail.setDeadLine(String.valueOf(calendar.getTimeInMillis()));
        if (!isEdit) {
            mPresenter.saveFood(mFoodDetail);
            StatisticsUtil.writeStatistics(FoodManageActivity.this, StatisticsUtil.EVENT_ADD_FOOD, mFoodDetail.toJson());
        } else mPresenter.editFood(mFoodDetail, id);
    }

    @OnClick(R.id.title_bar_edit)
    public void editFood() { // 编辑
        RxBus.getInstance().post(BusEvent.MSG_FOOD_MANAGE_EDIT_CHANGE, mType);
        mEditLinearLayout.setVisibility(View.GONE);
        mDeleteLinearLayout.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.title_bar_delete)
    public void deleteFood() { // 删除食材
        RxBus.getInstance().post(BusEvent.MSG_FOOD_MANAGE_DELETE, mType);
        mDeleteLinearLayout.setVisibility(View.GONE);
    }

    @Override
    public void getChildView(View view, int layoutResId) {
        if (layoutResId == R.layout.popup_window_food_type) { // 食材分类
            RecyclerView recyclerView = view.findViewById(R.id.popup_food_type_list);
            recyclerView.setLayoutManager(new GridLayoutManager(FoodManageActivity.this, 2));
            String[] types = getResources().getStringArray(R.array.food_manage_type);
            List<String> list = Arrays.asList(types);
            FoodManageTypeAdapter adapter = new FoodManageTypeAdapter(list, mTypeTextView.getText().toString());
            adapter.setOnItemClickListener((parent, view1, position, id) -> {
                mTypeTextView.setText(list.get(position));
                mFoodType = position;// 食材分类位置
                mDefineFood = new DefinedFood();
                switch (mFoodType) {
                    case 0: // 肉荤类
                        mDefineFood.setName(mMeatArray[0]);
                        mDefineFood.setColdNoFilterTime(getResources().getIntArray(R.array.food_manage_meat_cold_no_filter)[0]);
                        mDefineFood.setColdFilterTime(getResources().getIntArray(R.array.food_manage_meat_cold_filter)[0]);
                        mDefineFood.setFreezingNoFilterTime(getResources().getIntArray(R.array.food_manage_meat_freezing_no_filter)[0]);
                        mDefineFood.setFreezingFilterTime(getResources().getIntArray(R.array.food_manage_meat_freezing_filter)[0]);
                        break;
                    case 1: // 豆蛋奶
                        mDefineFood.setName(mEggArray[0]);
                        mDefineFood.setColdNoFilterTime(getResources().getIntArray(R.array.food_manage_egg_cold_no_filter)[0]);
                        mDefineFood.setColdFilterTime(getResources().getIntArray(R.array.food_manage_egg_cold_filter)[0]);
                        break;
                    case 2: // 水果类
                        mDefineFood.setName(mFruitsArray[0]);
                        mDefineFood.setColdNoFilterTime(getResources().getIntArray(R.array.food_manage_fruits_cold_no_filter)[0]);
                        mDefineFood.setColdFilterTime(getResources().getIntArray(R.array.food_manage_fruits_cold_filter)[0]);
                        break;
                    case 3: // 菌类
                        mDefineFood.setName(mFungusArray[0]);
                        mDefineFood.setColdNoFilterTime(getResources().getIntArray(R.array.food_manage_fungus_cold_no_filter)[0]);
                        mDefineFood.setColdFilterTime(getResources().getIntArray(R.array.food_manage_fungus_cold_filter)[0]);
                        break;
                    case 4: // 蔬菜类
                        mDefineFood.setName(mVegetablesArray[0]);
                        mDefineFood.setColdNoFilterTime(getResources().getIntArray(R.array.food_manage_vegetables_cold_no_filter)[0]);
                        mDefineFood.setColdFilterTime(getResources().getIntArray(R.array.food_manage_vegetables_cold_filter)[0]);
                        break;
                }
                setSubType(mFoodType);// 显示子类
                mFoodTypePopupWindow.dismiss();
            });
            recyclerView.setAdapter(adapter);
        } else if (layoutResId == R.layout.popup_window_food_sub_type) { // 食材子类
            RecyclerView recyclerView = view.findViewById(R.id.popup_food_sub_type_list);
            recyclerView.setLayoutManager(new GridLayoutManager(FoodManageActivity.this, 2));
            String[] name = mMeatArray;
            List<DefinedFood> list = new ArrayList<>();
            int[] coldNoFilter = getResources().getIntArray(R.array.food_manage_meat_cold_no_filter),
                    coldFilter = getResources().getIntArray(R.array.food_manage_meat_cold_filter),
                    freezingNoFilter = new int[]{},
                    freezingFilter = new int[]{};
            // 根据分类匹配子类
            switch (mFoodType) {
                case 0: // 肉荤类
                    name = mMeatArray;
                    coldNoFilter = getResources().getIntArray(R.array.food_manage_meat_cold_no_filter);
                    coldFilter = getResources().getIntArray(R.array.food_manage_meat_cold_filter);
                    freezingNoFilter = getResources().getIntArray(R.array.food_manage_meat_freezing_no_filter);
                    freezingFilter = getResources().getIntArray(R.array.food_manage_meat_freezing_filter);
                    break;
                case 1: // 豆蛋奶
                    name = mEggArray;
                    coldNoFilter = getResources().getIntArray(R.array.food_manage_egg_cold_no_filter);
                    coldFilter = getResources().getIntArray(R.array.food_manage_egg_cold_filter);
                    break;
                case 2: // 水果类
                    name = mFruitsArray;
                    coldNoFilter = getResources().getIntArray(R.array.food_manage_fruits_cold_no_filter);
                    coldFilter = getResources().getIntArray(R.array.food_manage_fruits_cold_filter);
                    break;
                case 3: // 菌类
                    name = mFungusArray;
                    coldNoFilter = getResources().getIntArray(R.array.food_manage_fungus_cold_no_filter);
                    coldFilter = getResources().getIntArray(R.array.food_manage_fungus_cold_filter);
                    break;
                case 4: // 蔬菜类
                    name = mVegetablesArray;
                    coldNoFilter = getResources().getIntArray(R.array.food_manage_vegetables_cold_no_filter);
                    coldFilter = getResources().getIntArray(R.array.food_manage_vegetables_cold_filter);
                    break;
            }
            for (int i = 0; i < name.length; i++) {
                DefinedFood definedFood = new DefinedFood();
                definedFood.setName(name[i]);
                definedFood.setColdFilterTime(coldFilter[i]);
                definedFood.setColdNoFilterTime(coldNoFilter[i]);
                if (freezingNoFilter.length > 0)
                    definedFood.setFreezingNoFilterTime(freezingNoFilter[i]);
                if (freezingFilter.length > 0)
                    definedFood.setFreezingFilterTime(freezingFilter[i]);
                list.add(definedFood);
            }
            FoodManageSubTypeAdapter adapter = new FoodManageSubTypeAdapter(list, mSubTypeTextView.getText().toString());
            adapter.setOnItemClickListener((parent, view1, position, id) -> {
                mSubTypeTextView.setText(list.get(position).getName());
                mDefineFood = list.get(position);
                matchPeriod(mDefineFood);
                mFoodSubTypePopupWindow.dismiss();
            });
            recyclerView.setAdapter(adapter);
            recyclerView.smoothScrollToPosition(Arrays.asList(name).indexOf(mSubTypeTextView.getText().toString()));
        } else if (layoutResId == R.layout.popup_window_food_manage_time) { // 保质期
            WheelView<Integer> yearWheelView = view.findViewById(R.id.food_manage_time_year);
            WheelView<Integer> monthWheelView = view.findViewById(R.id.food_manage_time_month);
            WheelView<Integer> dayWheelView = view.findViewById(R.id.food_manage_time_day);
            // 获取当前日期
            long time = System.currentTimeMillis() + 24L * 60L * 60L * 1000L;
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(time);
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH) + 1;
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            // WheelView Style
            WheelView.WheelViewStyle wheelStyle = new WheelView.WheelViewStyle();
            wheelStyle.selectedTextColor = getResources().getColor(R.color.color_25);
            wheelStyle.textColor = getResources().getColor(R.color.color_99);
            wheelStyle.textSize = 30;
            wheelStyle.selectedTextSize = 30;
            wheelStyle.itemPadding = 0;
            wheelStyle.itemMargin = 0;
            wheelStyle.itemHeight = 50;
            wheelStyle.holoBorderColor = getResources().getColor(android.R.color.white);
            wheelStyle.holoBorderWidth = 0;

            List<Integer> yearLList = new ArrayList<>();// 年份集合
            List<Integer> monthList = new ArrayList<>(1);// 月份集合
            List<Integer> dayList = new ArrayList<>();// 日集合
            for (int i = year; i < 2101; i++) yearLList.add(i);
            for (int i = 1; i < 13; i++) monthList.add(i);
            // 年
            yearWheelView.setWheelData(yearLList);
            yearWheelView.setWheelAdapter(new ArrayWheelAdapter<>(FoodManageActivity.this));
            yearWheelView.setSkin(WheelView.Skin.None);// 皮肤
            yearWheelView.setLoop(true);// 循环滚动
            yearWheelView.setWheelSize(5);
            yearWheelView.setStyle(wheelStyle);
            yearWheelView.setExtraText(getResources().getString(R.string.year), 0xFF252525, 30, 40);
            // 月
            monthWheelView.setWheelData(monthList);
            monthWheelView.setWheelAdapter(new ArrayWheelAdapter<>(FoodManageActivity.this));
            monthWheelView.setSkin(WheelView.Skin.None);
            monthWheelView.setLoop(true);// 循环滚动
            monthWheelView.setWheelSize(5);
            monthWheelView.setStyle(wheelStyle);
            monthWheelView.setExtraText(getResources().getString(R.string.month), 0xFF252525, 30, 40);
            // 日
            dayWheelView.setWheelData(setDays(mSelectedYear == 0 ? year : mSelectedYear,
                    mSelectedMonth == 0 ? month : mSelectedMonth, dayList));
            dayWheelView.setWheelAdapter(new ArrayWheelAdapter<>(FoodManageActivity.this));
            dayWheelView.setSkin(WheelView.Skin.None);
            dayWheelView.setLoop(true);// 循环滚动
            dayWheelView.setWheelSize(5);
            dayWheelView.setStyle(wheelStyle);
            dayWheelView.setExtraText(getResources().getString(R.string.day), 0xFF252525, 30, 40);
            // 初始化
            yearWheelView.setSelection(yearLList.indexOf(mSelectedYear == 0 ? year : mSelectedYear));
            monthWheelView.setSelection(monthList.indexOf(mSelectedMonth == 0 ? month : mSelectedMonth));
            dayWheelView.setSelection(dayList.indexOf(mSelectedDay == 0 ? day : mSelectedDay));
            yearWheelView.setOnWheelItemSelectedListener((position, o) -> {
                if (o != null) mSelectedYear = o;
                monthWheelView.setWheelData(setMonthData(monthList, year, mSelectedYear, month));
                dayWheelView.setWheelData(setDays(mSelectedYear, mSelectedMonth, dayList));
            });

            monthWheelView.setOnWheelItemSelectedListener((position, o) -> {
                if (o != null) mSelectedMonth = o;
                dayWheelView.setWheelData(setDays(mSelectedYear, mSelectedMonth, dayList));
            });

            dayWheelView.setOnWheelItemSelectedListener((position, o) -> {
                if (o != null) mSelectedDay = o;
            });
        }
    }

    @Override
    public void dismissFoodDialog() {
        mFoodRelativeLayout.setVisibility(View.GONE);
        mImagePath = "null";
        mDefineFood = null;
        mFoodDetail = null;
    }

    // 显示食材添加 Ui
    private void showFoodAddDialog() {
        mFoodRelativeLayout.setVisibility(View.VISIBLE);
        setTabUi(mType);// 选择仓室
        mTypeTextView.setText(getResources().getString(R.string.food_management_type_default));
        mSubTypeFrameLayout.setVisibility(View.GONE);
        mSubTypeTextView.setText("");
        mFoodType = -1;
        mSelectedYear = 0;
        mSelectedMonth = 0;
        mSelectedDay = 0;
        mFoodTimeTextView.setText(getResources().getString(R.string.food_management_time));
        loadFoodImage(Uri.parse("android.resource://" + FridgeApplication.getContext().getPackageName() + "/" + R.drawable.icon_food_manage_camera));
    }

    // 加载食材图片
    private void loadFoodImage(Uri uri) {
        int width = 268, height = 268;
        ImageRequest request = ImageRequestBuilder
                .newBuilderWithSource(uri)
                .setResizeOptions(new ResizeOptions(width, height))
                .build();
        PipelineDraweeController controller = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                .setOldController(mSimpleDraweeView.getController())
                .setImageRequest(request)
                .build();
        RoundingParams roundingParams = RoundingParams.fromCornersRadius(20f);
        roundingParams.setBorder(getResources().getColor(R.color.color_e8), 1.0f);
        mSimpleDraweeView.getHierarchy().setRoundingParams(roundingParams);
        mSimpleDraweeView.setController(controller);
    }

    // 根据分类显示默认的子类
    private void setSubType(int type) {
        mFoodType = type;
        int per = MiotRepository.getInstance().FILTER_LIFE_BASE - FridgeRepository.getInstance().getFilterLifeUsedTime();
        int[] period = new int[0];
        switch (mFoodType) {
            case 0: // 肉荤类
                mSubTypeTextView.setText(mMeatArray[0]);
                if (per == 0) { // 无滤芯
                    if (mType == 0) { // 冷藏室
                        period = getResources().getIntArray(R.array.food_manage_meat_cold_no_filter);
                    } else if (mType == 2) { // 冷冻室
                        period = getResources().getIntArray(R.array.food_manage_meat_freezing_no_filter);
                    }
                } else {
                    if (mType == 0) { // 冷藏室
                        period = getResources().getIntArray(R.array.food_manage_meat_cold_filter);
                    } else if (mType == 2) { // 冷冻室
                        period = getResources().getIntArray(R.array.food_manage_meat_freezing_filter);
                    }
                }
                break;
            case 1: // 豆蛋奶
                mSubTypeTextView.setText(mEggArray[0]);
                if (mType == 0)
                    if (per == 0) { // 无滤芯
                        period = getResources().getIntArray(R.array.food_manage_egg_cold_no_filter);
                    } else {
                        period = getResources().getIntArray(R.array.food_manage_egg_cold_filter);
                    }
                break;
            case 2: // 水果类
                mSubTypeTextView.setText(mFruitsArray[0]);
                if (mType == 0)
                    if (per == 0) { // 无滤芯
                        period = getResources().getIntArray(R.array.food_manage_fruits_cold_no_filter);
                    } else {
                        period = getResources().getIntArray(R.array.food_manage_fruits_cold_filter);
                    }
                break;
            case 3: // 菌类
                mSubTypeTextView.setText(mFungusArray[0]);
                if (mType == 0)
                    if (per == 0) { // 无滤芯
                        period = getResources().getIntArray(R.array.food_manage_fungus_cold_no_filter);
                    } else {
                        period = getResources().getIntArray(R.array.food_manage_fungus_cold_filter);
                    }
                break;
            case 4: // 蔬菜类
                mSubTypeTextView.setText(mVegetablesArray[0]);
                if (mType == 0)
                    if (per == 0) { // 无滤芯
                        period = getResources().getIntArray(R.array.food_manage_vegetables_cold_no_filter);
                    } else {
                        period = getResources().getIntArray(R.array.food_manage_vegetables_cold_filter);
                    }
                break;
        }
        if (period.length == 0) {
            mSelectedYear = 0;
            mSelectedMonth = 0;
            mSelectedDay = 0;
            mFoodTimeTextView.setText(getResources().getString(R.string.food_management_time));
        } else {
            Calendar calendar = Calendar.getInstance();
            long time = period[0] * 24L * 60L * 60L * 1000L + System.currentTimeMillis();
            calendar.setTime(new Date(time));
            mSelectedYear = calendar.get(Calendar.YEAR);
            mSelectedMonth = calendar.get(Calendar.MONTH) + 1;
            mSelectedDay = calendar.get(Calendar.DAY_OF_MONTH);
            String str = getResources().getString(R.string.food_management_time) +
                    mSelectedYear + getResources().getString(R.string.year) +
                    (mSelectedMonth < 10 ? "0" + mSelectedMonth : mSelectedMonth) + getResources().getString(R.string.month) +
                    (mSelectedDay < 10 ? "0" + mSelectedDay : mSelectedDay) + getResources().getString(R.string.day);
            mFoodTimeTextView.setText(str);
        }
    }

    // 根据食材名称和仓室类型匹配保质期
    private void matchPeriod(DefinedFood definedFood) {
        int per = MiotRepository.getInstance().FILTER_LIFE_BASE - FridgeRepository.getInstance().getFilterLifeUsedTime();
        long period = 0;
        switch (mType) {
            case 0: // 冷藏室
                if (per == 0) { // 无滤芯
                    period = System.currentTimeMillis() + definedFood.getColdNoFilterTime() * 24L * 60L * 60L * 1000L;
                } else { // 有滤芯
                    period = System.currentTimeMillis() + definedFood.getColdFilterTime() * 24L * 60L * 60L * 1000L;
                }
                break;
            case 2: // 冷冻室
                if (per == 0 && definedFood.getFreezingNoFilterTime() != 0) {
                    period = System.currentTimeMillis() + definedFood.getFreezingNoFilterTime() * 24L * 60L * 60L * 1000L;
                } else if (definedFood.getFreezingFilterTime() != 0) {
                    period = System.currentTimeMillis() + definedFood.getFreezingFilterTime() * 24L * 60L * 60L * 1000L;
                }
                break;
        }
        if (period == 0) {
            mSelectedYear = 0;
            mSelectedMonth = 0;
            mSelectedDay = 0;
            mFoodTimeTextView.setText(getResources().getString(R.string.food_management_time));
        } else {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date(period));
            mSelectedYear = calendar.get(Calendar.YEAR);
            mSelectedMonth = calendar.get(Calendar.MONTH) + 1;
            mSelectedDay = calendar.get(Calendar.DAY_OF_MONTH);
            String str = getResources().getString(R.string.food_management_time) +
                    mSelectedYear + getResources().getString(R.string.year) +
                    (mSelectedMonth < 10 ? "0" + mSelectedMonth : mSelectedMonth) + getResources().getString(R.string.month) +
                    (mSelectedDay < 10 ? "0" + mSelectedDay : mSelectedDay) + getResources().getString(R.string.day);
            mFoodTimeTextView.setText(str);
        }
    }

    /**
     * 设置仓室 Tab Ui
     */
    private void setTabUi(int type) {
        mColdTextView.setSelected(false);
        mCCTextView.setSelected(false);
        mChangeableTextView.setSelected(false);
        mFreezingTextView.setSelected(false);
        switch (type) {
            case AppConstants.ROOM_TYPE_COLD: // 冷藏室
                mColdTextView.setSelected(true);
                break;
            case AppConstants.ROOM_TYPE_CHANGEABLE: // 变温室
                mChangeableTextView.setSelected(true);
                break;
            case AppConstants.ROOM_TYPE_FREEZING: // 冷冻室
                mFreezingTextView.setSelected(true);
                break;
            case AppConstants.ROOM_TYPE_CC: // 冷藏变温区
                mCCTextView.setSelected(true);
                break;
        }
    }

    /**
     * 根据年 月 获取对应的月份 天数
     */
    private List<Integer> setDays(int selectedYear, int selectedMonth, List<Integer> dayList) {
        dayList.clear();
        long time = System.currentTimeMillis() + 24L * 60L * 60L * 1000L;
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        int curYear = calendar.get(Calendar.YEAR);
        int curMonth = calendar.get(Calendar.MONTH) + 1;
        int curDay = calendar.get(Calendar.DAY_OF_MONTH);
        calendar.set(Calendar.YEAR, selectedYear);
        calendar.set(Calendar.MONTH, selectedMonth - 1);
        calendar.set(Calendar.DATE, 1);
        calendar.roll(Calendar.DATE, -1);
        int maxDate = calendar.get(Calendar.DATE);
        if ((curYear == selectedYear) && (curMonth == selectedMonth)) {
            for (int i = curDay; i <= maxDate; i++) dayList.add(i);
        } else {
            for (int i = 1; i <= maxDate; i++) dayList.add(i);
        }
        return dayList;
    }

    /**
     * 根据年获取对应月份（排除过去时间）
     */
    private List<Integer> setMonthData(List<Integer> list, int currentYear, int selectedYear, int month) {
        list.clear();
        if (selectedYear == currentYear)
            for (int i = month; i < 13; i++) list.add(i);
        else for (int i = 1; i < 13; i++) list.add(i);
        return list;
    }

    @Override
    public void onFoodEdit(FoodDetail detail) {
        if (isRepeatedClick()) return;
        mFoodDetail = detail;
        mFoodRelativeLayout.setVisibility(View.VISIBLE);
        mType = Integer.valueOf(mFoodDetail.getRoomType());
        setTabUi(mType);// 选择仓室
        mTypeTextView.setText(detail.getFoodType());
        mSubTypeFrameLayout.setVisibility(View.VISIBLE);
        mSubTypeTextView.setText(detail.getName());
        List<String> list = Arrays.asList(getResources().getStringArray(R.array.food_manage_type));
        mFoodType = list.indexOf(detail.getFoodType());
        mDefineFood = new DefinedFood();
        switch (mFoodType) {
            case 0: // 肉荤类
                mDefineFood.setName(mMeatArray[0]);
                mDefineFood.setColdNoFilterTime(getResources().getIntArray(R.array.food_manage_meat_cold_no_filter)[0]);
                mDefineFood.setColdFilterTime(getResources().getIntArray(R.array.food_manage_meat_cold_filter)[0]);
                mDefineFood.setFreezingNoFilterTime(getResources().getIntArray(R.array.food_manage_meat_freezing_no_filter)[0]);
                mDefineFood.setFreezingFilterTime(getResources().getIntArray(R.array.food_manage_meat_freezing_filter)[0]);
                break;
            case 1: // 豆蛋奶
                mDefineFood.setName(mEggArray[0]);
                mDefineFood.setColdNoFilterTime(getResources().getIntArray(R.array.food_manage_egg_cold_no_filter)[0]);
                mDefineFood.setColdFilterTime(getResources().getIntArray(R.array.food_manage_egg_cold_filter)[0]);
                break;
            case 2: // 水果类
                mDefineFood.setName(mFruitsArray[0]);
                mDefineFood.setColdNoFilterTime(getResources().getIntArray(R.array.food_manage_fruits_cold_no_filter)[0]);
                mDefineFood.setColdFilterTime(getResources().getIntArray(R.array.food_manage_fruits_cold_filter)[0]);
                break;
            case 3: // 菌类
                mDefineFood.setName(mFungusArray[0]);
                mDefineFood.setColdNoFilterTime(getResources().getIntArray(R.array.food_manage_fungus_cold_no_filter)[0]);
                mDefineFood.setColdFilterTime(getResources().getIntArray(R.array.food_manage_fungus_cold_filter)[0]);
                break;
            case 4: // 蔬菜类
                mDefineFood.setName(mVegetablesArray[0]);
                mDefineFood.setColdNoFilterTime(getResources().getIntArray(R.array.food_manage_vegetables_cold_no_filter)[0]);
                mDefineFood.setColdFilterTime(getResources().getIntArray(R.array.food_manage_vegetables_cold_filter)[0]);
                break;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(Long.valueOf(mFoodDetail.getDeadLine())));
        mSelectedYear = calendar.get(Calendar.YEAR);
        mSelectedMonth = calendar.get(Calendar.MONTH) + 1;
        mSelectedDay = calendar.get(Calendar.DAY_OF_MONTH);
        String str = getResources().getString(R.string.food_management_time) +
                mSelectedYear + getResources().getString(R.string.year) +
                (mSelectedMonth < 10 ? "0" + mSelectedMonth : mSelectedMonth) + getResources().getString(R.string.month) +
                (mSelectedDay < 10 ? "0" + mSelectedDay : mSelectedDay) + getResources().getString(R.string.day);
        mFoodTimeTextView.setText(str);
        Uri uri;
        if (detail.getImgPath() == null || detail.getImgPath().equals("null")) {
            uri = Uri.parse("android.resource://" + FridgeApplication.getContext().getPackageName() + "/" + R.drawable.icon_food_manage_camera);
        } else {
            uri = Uri.fromFile(new File(detail.getImgPath()));
        }
        loadFoodImage(uri);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == 0) {
            mType = AppConstants.ROOM_TYPE_COLD;
        } else if (position == 1) {
            if (mModel.equals(AppConstants.MODEL_X2) || mModel.equals(AppConstants.MODEL_X3)
                    || mModel.equals(AppConstants.MODEL_X4) || mModel.equals(AppConstants.MODEL_JD)) {
                mType = AppConstants.ROOM_TYPE_FREEZING;
            } else mType = AppConstants.ROOM_TYPE_CC;
        } else if (position == 2) {
            mType = AppConstants.ROOM_TYPE_CHANGEABLE;
        } else {
            mType = AppConstants.ROOM_TYPE_FREEZING;
        }
        if (mPosition != position) {
            mPosition = position;
            mDeleteLinearLayout.setVisibility(View.GONE);
            RxBus.getInstance().post(BusEvent.MSG_FOOD_MANAGE_EDIT_CHANGE);
            switch (position) {
                case 0:
                    if (mColdFoodSize > 0) mEditLinearLayout.setVisibility(View.VISIBLE);
                    else mEditLinearLayout.setVisibility(View.GONE);
                    break;
                case 1:
                    if (mModel.equals(AppConstants.MODEL_X2) || mModel.equals(AppConstants.MODEL_X3) || mModel.equals(AppConstants.MODEL_X4) ||
                            mModel.equals(AppConstants.MODEL_JD)) {
                        if (mFreezingFoodSize > 0) mEditLinearLayout.setVisibility(View.VISIBLE);
                        else mEditLinearLayout.setVisibility(View.GONE);
                    } else if (mCCFoodSize > 0) {
                        mEditLinearLayout.setVisibility(View.VISIBLE);
                    } else mEditLinearLayout.setVisibility(View.GONE);
                    break;
                case 2:
                    if (mChangeableFoodSize > 0) mEditLinearLayout.setVisibility(View.VISIBLE);
                    else mEditLinearLayout.setVisibility(View.GONE);
                    break;
                case 3:
                    if (mFreezingFoodSize > 0) mEditLinearLayout.setVisibility(View.VISIBLE);
                    else mEditLinearLayout.setVisibility(View.GONE);
                    break;
            }
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}