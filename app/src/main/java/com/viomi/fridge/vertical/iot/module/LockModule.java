package com.viomi.fridge.vertical.iot.module;

import com.viomi.fridge.vertical.common.scope.ActivityScoped;
import com.viomi.fridge.vertical.iot.contract.LockContract;
import com.viomi.fridge.vertical.iot.presenter.LockPressenter;
import com.viomi.fridge.vertical.iot.presenter.RangeHoodPresenter;

import dagger.Binds;
import dagger.Module;

/**
 * Created by hailang on 2018/5/15 0015.
 */
@Module
public abstract class LockModule {
    @ActivityScoped
    @Binds
    abstract LockContract.Presenter lockPresenter(LockPressenter presenter);
}
