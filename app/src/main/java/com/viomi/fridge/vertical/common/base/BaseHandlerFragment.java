package com.viomi.fridge.vertical.common.base;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;

import java.lang.ref.WeakReference;

/**
 * 带 Handler 全局 Fragment
 * Created by William on 2017/12/29.
 */
public class BaseHandlerFragment extends BaseFragment {
    protected Handler mHandler;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new BaseHandler(this);
    }

    @Override
    protected void initWithOnCreate() {

    }

    @Override
    protected void initWithOnCreateView() {

    }

    protected void handleMessage(Message msg) {

    }

    private static class BaseHandler extends Handler {
        WeakReference<BaseHandlerFragment> weakReference;

        private BaseHandler(BaseHandlerFragment fragment) {
            this.weakReference = new WeakReference<>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (this.weakReference != null) {
                BaseHandlerFragment fragment = this.weakReference.get();
                if (fragment != null && !fragment.isRemoving()) {
                    fragment.handleMessage(msg);
                }
            }
        }
    }
}
