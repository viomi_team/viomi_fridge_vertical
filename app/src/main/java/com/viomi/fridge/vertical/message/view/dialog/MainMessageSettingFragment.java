package com.viomi.fridge.vertical.message.view.dialog;

import android.widget.TextView;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseFragment;
import com.viomi.fridge.vertical.common.scope.ActivityScoped;
import com.viomi.fridge.vertical.message.manager.PushManager;
import com.viomi.widget.switchbutton.SwitchButton;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 消息设置 Dialog
 * Created by nanquan on 2018/2/7.
 */
@ActivityScoped
public class MainMessageSettingFragment extends BaseFragment {
    private OnClickListener mOnClickListener;
    private PushManager mPushManager;

    @BindView(R.id.sb_message_setting_activity)
    SwitchButton mActivitySwitchButton;// 活动促销开关
    @BindView(R.id.sb_message_setting_mall)
    SwitchButton mMallSwitchButton;// 商城消息开关
    @BindView(R.id.sb_message_setting_device)
    SwitchButton mDeviceSwitchButton;// 设备消息开关
    @BindView(R.id.sb_message_setting_disturb)
    SwitchButton mNoDisturbSwitchButton;// 勿扰开关

    @BindView(R.id.tv_message_setting_time)
    TextView mTimeTextView;// 勿扰时间

    @Inject
    public MainMessageSettingFragment() {

    }

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.dialog_message_setting_main;
    }

    @Override
    protected void initWithOnCreateView() {
        initState();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mPushManager != null) mPushManager = null;
        if (mOnClickListener != null) mOnClickListener = null;
    }

    private void initState() {
        mPushManager = PushManager.getInstance();
        mActivitySwitchButton.setOn(mPushManager.isActivityPushEnable());
        mMallSwitchButton.setOn(mPushManager.isMallPushEnable());
        mDeviceSwitchButton.setOn(mPushManager.isDevicePushEnable());
        mNoDisturbSwitchButton.setOn(mPushManager.isPushEnable());
        setTimeText(mPushManager.getNoPushStartTime(), mPushManager.getNoPushEndTime());

        mActivitySwitchButton.setOnSwitchStateChangeListener(isOn -> mPushManager.setActivityPushEnable(isOn));
        mMallSwitchButton.setOnSwitchStateChangeListener(isOn -> mPushManager.setMallPushEnable(isOn));
        mDeviceSwitchButton.setOnSwitchStateChangeListener(isOn -> mPushManager.setDevicePushEnable(isOn));
        mNoDisturbSwitchButton.setOnSwitchStateChangeListener(isOn -> mPushManager.setPushEnable(isOn));
    }

    public void setTimeText(int startTime, int endTime) {
        mTimeTextView.setText(String.valueOf(startTime + ":00~" + endTime + ":00"));
    }

    @OnClick(R.id.fl_message_setting_container)
    public void OnBackgroundClick() { // 背景点击
        if (mOnClickListener != null) {
            mOnClickListener.onBackgroundClick();
        }
    }

    @OnClick(R.id.ll_message_setting_time)
    public void OnTimeItemClick() { // 选择时间
        if (mOnClickListener != null) {
            mOnClickListener.onTimeItemClick();
        }
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.mOnClickListener = onClickListener;
    }

    public interface OnClickListener {

        void onBackgroundClick();

        void onTimeItemClick();
    }
}