package com.viomi.fridge.vertical;

import android.app.ActivityManager;
import android.app.Notification;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.os.Process;
import android.support.multidex.MultiDex;
import android.text.TextUtils;

import com.facebook.cache.disk.DiskCacheConfig;
import com.facebook.common.internal.Supplier;
import com.facebook.common.util.ByteConstants;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.cache.MemoryCacheParams;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.facebook.imagepipeline.decoder.SimpleProgressiveJpegConfig;
import com.miot.api.MiotManager;
import com.umeng.commonsdk.UMConfigure;
import com.umeng.message.IUmengRegisterCallback;
import com.umeng.message.PushAgent;
import com.umeng.message.UmengMessageHandler;
import com.umeng.message.entity.UMessage;
import com.viomi.fridge.vertical.administration.model.repository.ManageRepository;
import com.viomi.fridge.vertical.common.component.DaggerApplicationComponent;
import com.viomi.fridge.vertical.common.http.ApiClient;
import com.viomi.fridge.vertical.common.task.MiIotCloseTask;
import com.viomi.fridge.vertical.common.task.MiIotOpenTask;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;
import com.viomi.fridge.vertical.fridgecontrol.model.repository.MiotRepository;
import com.viomi.fridge.vertical.message.entity.DaoMaster;
import com.viomi.fridge.vertical.message.entity.DaoSession;
import com.viomi.fridge.vertical.message.manager.PushManager;

import java.io.File;
import java.util.concurrent.TimeUnit;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import rx.Observable;

/**
 * Application 类
 * Created by William on 2018/1/2.
 */
public class FridgeApplication extends DaggerApplication {
    private static final String TAG = FridgeApplication.class.getSimpleName();
    private static FridgeApplication mFridgeApplication;
    //    private RefWatcher mRefWatcher;// LeakCanary 工具（正式发版隐藏）
    private DaoSession mDaoSession;

    public static FridgeApplication getInstance() {
        return mFridgeApplication;
    }

    @Override
    public void onCreate() {
        // StrictMode
//        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
//                .detectDiskReads()
//                .detectDiskWrites()
//                .detectNetwork()
//                .penaltyLog()
//                .build());
//        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
//                .detectLeakedSqlLiteObjects()
//                .detectLeakedClosableObjects()
//                .penaltyLog()
//                .penaltyDeath()
//                .build());
        super.onCreate();
        logUtil.d(TAG, "onCreate");
        mFridgeApplication = this;
        initUmeng();// 友盟初始化
        if (isMainProcess()) {
            FridgePreference.getInstance().saveModel(ToolUtil.getDeviceModel());// 获取型号
            initMiIot();// Miot 初始化
            initDevice();// 设备初始化
            initFresco();// Fresco 初始化
            initGreenDao();
            ApiClient.getInstance().init();// Retrofit 初始化
//            mRefWatcher = setupLeakCanary();
        }
    }

    @Override
    public void onTerminate() {
        logUtil.e(TAG, "onTerminate");
        if (isMainProcess()) {
            new MiIotCloseTask().execute();
        }
        super.onTerminate();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }

    /**
     * Fresco 初始化
     */
    private void initFresco() {
        final MemoryCacheParams memoryCacheParams = new MemoryCacheParams(
                30 * ByteConstants.MB,// 内存缓存中总图片的最大大小,以字节为单位
                Integer.MAX_VALUE,// 内存缓存中图片的最大数量
                30 * ByteConstants.MB,// 内存缓存中准备清除但尚未被删除的总图片的最大大小,以字节为单位
                Integer.MAX_VALUE,// 内存缓存中准备清除的总图片的最大数量
                Integer.MAX_VALUE);// 内存缓存中单个图片的最大大小
        Supplier<MemoryCacheParams> memoryCacheParamsSupplier = () -> memoryCacheParams;

        DiskCacheConfig diskCacheConfig = DiskCacheConfig.newBuilder(this)
                .setBaseDirectoryPath(new File(Environment.getExternalStorageDirectory(), AppConstants.PATH))
                .setBaseDirectoryName(AppConstants.CACHE_SAVE_PATH)
                .setMaxCacheSize(50 * ByteConstants.MB)
                .setMaxCacheSizeOnLowDiskSpace(30 * ByteConstants.MB)
                .setMaxCacheSizeOnVeryLowDiskSpace(10 * ByteConstants.MB)
                .build();

        // 日志
//        Set<RequestListener> requestListeners = new HashSet<>();
//        requestListeners.add(new RequestLoggingListener());

        ImagePipelineConfig config = ImagePipelineConfig.newBuilder(this)
                // 三级缓存中已解码图片的内存缓存配置
                .setBitmapMemoryCacheParamsSupplier(memoryCacheParamsSupplier)
                // 自定义缓存键值对
                //.setCacheKeyFactory(cacheKeyFactory)
                // 是否支持自动缩放（缩放必须要设置ResizeOptions）
                .setDownsampleEnabled(true)
                // 对网络图进行 Resize 处理，减少内存开销
                .setResizeAndRotateEnabledForNetwork(true)
                // 是否支持 WebP 图片
                //.setWebpSupportEnabled(true)
                // 三级缓存中编码图片的内存缓存
                .setEncodedMemoryCacheParamsSupplier(memoryCacheParamsSupplier)
                // 各种线程池
                //.setExecutorSupplier(executorSupplier)
                // 缓存的统计数据追踪器。它是一个接口，提供了各个缓存中图片 Hit 与 Miss 的回调方法，通常可以使用它来统计缓存命中率
                //.setImageCacheStatsTracker(imageCacheStatsTracker)
                // 三级缓存中硬盘缓存的配置，默认缓存目录在 app 自身 CacheDir 的 image_cache 目录下
                .setMainDiskCacheConfig(diskCacheConfig)
                // 注册一个内存调节器，它将根据不同的 MemoryTrimType 回收类型在需要降低内存使用时候进行回收一些内存缓存资源(Bitmap和Encode)。数值越大，表示要回收的资源越多
                //.setMemoryTrimmableRegistry(memoryTrimmableRegistry)
                // 网络图片下载请求类
                //.setNetworkFetchProducer(networkFetchProducer)
                //.setPoolFactory(poolFactory)
                // 渐进式显示网络的 JPEG 图的配置，不过要使用渐进式显示图片，需要在 ImageRequest 中显示的设置是否支持渐进式显示：setProgressiveRenderingEnabled(true)
                .setProgressiveJpegConfig(new SimpleProgressiveJpegConfig())
                //.setRequestListeners(requestListeners)
                // 小图的硬盘缓存配置，默认是和主硬盘缓存目录是共用的。如果需要把小图和普通图片分开，则需重新配置
                .setSmallImageDiskCacheConfig(diskCacheConfig)
                // 打印日志
                // .setRequestListeners(requestListeners)
                // 对透明度无要求
                .setBitmapsConfig(Bitmap.Config.RGB_565)
                .build();
        Fresco.initialize(this, config);
//        FLog.setMinimumLoggingLevel(FLog.VERBOSE);// 日志级别
    }

    /**
     * MiIot 初始化
     */
    private void initMiIot() {
        if (isMainProcess()) {
            logUtil.d(TAG, "MiIotManager init");
            MiotManager.getInstance().initialize(this);
            new MiIotOpenTask().execute();
        }
    }

    /**
     * 初始化 MiIot Device
     */
    private void initDevice() {
        if (isMainProcess()) {
            ManageRepository.getInstance().getUser(this)
                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                    .onTerminateDetach()
                    .subscribe(qrCodeBase -> MiotRepository.getInstance().initDevice(this, qrCodeBase),
                            throwable -> logUtil.e(TAG, throwable.getMessage()));
        }
    }

    /**
     * 初始化 GreenDao
     */
    private void initGreenDao() {
        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(this, "viomi.db", null);
        DaoMaster daoMaster = new DaoMaster(devOpenHelper.getWritableDb());
        mDaoSession = daoMaster.newSession();
    }

//    /**
//     * 初始化 LeakCanary
//     */
//    private RefWatcher setupLeakCanary() {
//        if (LeakCanary.isInAnalyzerProcess(this)) {
//            return RefWatcher.DISABLED;
//        }
//        return LeakCanary.install(this);
//    }
//
//    public static RefWatcher getRefWatcher(Context context) {
//        FridgeApplication leakApplication = (FridgeApplication) context.getApplicationContext();
//        return leakApplication.mRefWatcher;
//    }

    /**
     * 初始化友盟
     */
    private void initUmeng() {
        UMConfigure.init(this, null, "Umeng", UMConfigure.DEVICE_TYPE_PHONE, null);
        PushAgent mPushAgent = PushAgent.getInstance(this);
        // 注册推送服务，每次调用 register 方法都会回调该接口
        mPushAgent.register(new IUmengRegisterCallback() {

            @Override
            public void onSuccess(String deviceToken) {
                // 注册成功会返回 device token
                logUtil.d(TAG, "deviceToken=" + deviceToken);
                PushManager.getInstance().initSetting();// 友盟推送初始化
            }

            @Override
            public void onFailure(String s, String s1) {
                logUtil.e(TAG, "push register fail!s=" + s + ",s1=" + s1);
                // 注册失败重连
                Observable.timer(10, TimeUnit.SECONDS)
                        .compose(RxSchedulerUtil.SchedulersTransformer1())
                        .subscribe(aLong -> initUmeng(), throwable -> logUtil.e(TAG, throwable.getMessage()));
            }
        });
        // 自定义消息推送
        UmengMessageHandler messageHandler = new UmengMessageHandler() {
            /**
             * 自定义消息的回调方法
             * */
            @Override
            public void dealWithCustomMessage(final Context context, final UMessage msg) {
                logUtil.e(TAG, "dealWithCustomMessage=" + msg.getRaw().toString());
            }

            @Override
            public Notification getNotification(Context context, UMessage msg) {
                // 消息送达
                PushManager.getInstance().dispatchMessage(msg);
                return super.getNotification(context, msg);
            }
        };
        mPushAgent.setMessageHandler(messageHandler);
    }

    public DaoSession getDaoSession() {
        return mDaoSession;
    }

    public static Context getContext() {
        return getInstance().getApplicationContext();
    }

    private boolean isMainProcess() {
        String mainProcessName = getPackageName();
        String processName = getProcessName();
        // logUtil.d(TAG, "isMainProcess = " + result + ",mainProcessName = " + mainProcessName + ",processName = " + processName);
        return TextUtils.equals(processName, mainProcessName);
    }

    private String getProcessName() {
        int pid = Process.myPid();
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        if (activityManager != null) {
            for (ActivityManager.RunningAppProcessInfo processInfo : activityManager.getRunningAppProcesses()) {
                if (processInfo.pid == pid) {
                    return processInfo.processName;
                }
            }
        }
        return null;
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerApplicationComponent.builder().application(this).build();
    }
}
