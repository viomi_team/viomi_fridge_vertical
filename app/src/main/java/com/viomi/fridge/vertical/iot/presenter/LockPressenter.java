package com.viomi.fridge.vertical.iot.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.viomi.fridge.vertical.administration.model.preference.LoginPreference;
import com.viomi.fridge.vertical.common.http.ApiClient;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.iot.contract.IotContract;
import com.viomi.fridge.vertical.iot.contract.LockContract;
import com.viomi.fridge.vertical.iot.model.http.MiDeviceApi;
import com.viomi.fridge.vertical.iot.model.http.entity.CameraStatus;
import com.viomi.fridge.vertical.iot.model.repository.LockReposity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import javax.annotation.Nullable;
import javax.inject.Inject;

import rx.Observable;
import rx.Scheduler;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by hailang on 2018/5/15 0015.
 */

public class LockPressenter implements LockContract.Presenter {
    final String TAG = LockPressenter.class.getSimpleName();
    LockContract.View mView;
    //"FFD4600F004B1200", "50294D10509B" //棕色
    //9BE3600F004B1200 50294D10509B//银色

    String devID = "9BE3600F004B1200";
    String type = "Bc";
//    String GwID="50294D10509B";

    //    String devID = "9BE3600F004B1200";
    String GwID = "50294D10509B";
    private CompositeSubscription mCompositeSubscription;
    Context mContext;

    @Override
    public void subscribe(LockContract.View view) {
        mView = view;
        mCompositeSubscription = new CompositeSubscription();
    }

    @Inject
    LockPressenter(Context context) {
        mContext = context;
    }

    @Override
    public void unSubscribe() {
        mView = null;
        mCompositeSubscription.clear();
    }

    @Override
    public void getWlinkDeviceChildren(final String devIDT) {
        devID = devIDT;
        Subscription subscription = LockReposity.getWlinkDeviceChildren(LoginPreference.getInstance().getUserToken())
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    try {
                        logUtil.d(TAG, "turnOnCamera_________" + "getWlinkDeviceChildren  " + result);
                        JSONArray jsonArray = new JSONObject(result).optJSONObject("mobBaseRes")
                                .optJSONArray("result");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.optJSONObject(i);
                            if (devID.equals(jsonObject.optString("devID"))) {
                                GwID = jsonObject.optString("gwID");
                                type = jsonObject.optString("type");
                                mView.onLockCaneraFound(GwID, type);
                                break;
                            }
                        }
                        turnOnCamera(devID, GwID, type);
                    } catch (Throwable e) {
                        mView.onError();
                        e.printStackTrace();
                    }
                }, throwable -> {
                    mView.onError();
                    logUtil.d(TAG, throwable.getMessage());
                });
        mCompositeSubscription.add(subscription);
    }

    void turnOnCamera(final String devID, final String GwID, final String type) {

        Subscription subscription = Observable
                .interval(0, 1, TimeUnit.SECONDS).
                        subscribeOn(Schedulers.io()).
                        takeUntil(Observable.timer(12, TimeUnit.SECONDS)).
                        subscribe(num -> {
                            logUtil.d(TAG, "turnOnCamera num:" + num);
                            if (num == 11) {
                                getCamerStatus();
                            } else if (num == 0) {
                                requestTurnOnCamera(devID, GwID, type);
                            }
                        });

        mCompositeSubscription.add(subscription);
    }

    public void getWlinkDevices(final String targetDeviceId) {
        if (TextUtils.isEmpty(targetDeviceId)) {
            logUtil.d(TAG, "getWlinkDevices: error targetDeviceId==null");
            return;
        }
        Subscription subscription = LockReposity.getWlinkDevices(LoginPreference.getInstance().getUserToken())
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    logUtil.d(TAG, "getWlinkDevices:" + result);
                    try {
                        JSONArray jsonArray = new JSONObject(result).optJSONObject("mobBaseRes")
                                .optJSONObject("result").optJSONObject("data")
                                .optJSONArray("boundDevices");
                        boolean got = false;
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            JSONArray deviceSips = null;
                            if (jsonObject.has("sipInfo"))
                                deviceSips = jsonObject.optJSONObject("sipInfo").optJSONArray("deviceSips");
                            if (deviceSips != null)
                                for (int j = 0; j < deviceSips.length(); j++) {
                                    JSONObject jsonObjectJ = deviceSips.optJSONObject(j);
                                    if (targetDeviceId.equals(jsonObjectJ.optString("deviceId"))) {
                                        mView.initRtc(jsonObject.optJSONObject("sipInfo").optString("suid")
                                                , jsonObject.optJSONObject("sipInfo").optString("spassword")
                                                , jsonObjectJ.optString("sdomain"));
                                        got = true;
                                        break;
                                    }
                                }
                            if (got)
                                break;
                        }
                        if (!got) {
                            jsonArray = new JSONObject(result).optJSONObject("mobBaseRes")
                                    .optJSONObject("result").optJSONObject("data")
                                    .optJSONArray("gainDevices");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                JSONArray deviceSips = null;
                                if (jsonObject.has("sipInfo"))
                                    deviceSips = jsonObject.optJSONObject("sipInfo").optJSONArray("deviceSips");
                                if (deviceSips != null)
                                    for (int j = 0; j < deviceSips.length(); j++) {
                                        JSONObject jsonObjectJ = deviceSips.optJSONObject(j);
                                        if (targetDeviceId.equals(jsonObjectJ.optString("deviceId"))) {
                                            mView.initRtc(jsonObject.optJSONObject("sipInfo").optString("suid")
                                                    , jsonObject.optJSONObject("sipInfo").optString("spassword")
                                                    , jsonObjectJ.optString("sdomain"));
                                            got = true;
                                            break;
                                        }
                                    }
                                if (got)
                                    break;
                            }
                        }
                    } catch (Throwable e) {
                        mView.onError();
                        e.printStackTrace();
                    }
                }, throwable -> {
                    mView.onError();
                    logUtil.d(TAG, throwable.getMessage());
                });
        mCompositeSubscription.add(subscription);
    }


    void getCamerStatus() {
        Subscription subscription = Observable
                .interval(0, 1, TimeUnit.SECONDS)
                .flatMap(aLong -> LockReposity.getCameraStatus(devID, type))
                .takeUntil(cameraStatus -> {
                    try {
                        for (CameraStatus.MobBaseResBean.ResultBean bean : cameraStatus.getMobBaseRes().getResult()) {
                            if (bean.getAttributeId() == 32776)
                                if (bean.getAttributeValue().equals("11") || bean.getAttributeValue().equals("04"))
                                    return true;
                        }
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                    return false;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cameraStatus -> {
                    try {
                        String result = cameraStatus.getMobBaseRes().getResult().get(0).getAttributeValue();
                        logUtil.d(TAG, "turnOnCamera_________" + result);
                        if (("11").equals(result) || ("04").equals(result)) {
                            if (mView != null) mView.makecall();
                            logUtil.d(TAG, "turnOnCamera_________" + "makecall");
                        }
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }

                }, throwable -> {
                    logUtil.d(TAG, throwable.getMessage());
                });
        mCompositeSubscription.add(subscription);
    }

    public void requestTurnOnCamera(String devID, String GwID, String type) {
        Subscription subscription = LockReposity.turnOnCamera(devID, GwID, type)
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(string -> {
                    logUtil.d(TAG, string);
                }, throwable -> {
                    logUtil.d(TAG, throwable.getMessage());
                });
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void turnOffCamera() {
        Subscription subscription = LockReposity.turnOffCamera(devID, GwID, type)
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(string -> {
//                    if (mView != null) mView.close();
                }, throwable -> {
                    logUtil.d(TAG, throwable.getMessage());
                });
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void unlock(String devID, String GwID, String pass, String type) {
        Subscription subscription = LockReposity.doorUnlock(devID, GwID, pass, type)
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(string -> {
                    logUtil.d(TAG, string);
                    if (mView != null) mView.ondoorUnlock();
                }, throwable -> {
                    logUtil.d(TAG, throwable.getMessage());
                });
        mCompositeSubscription.add(subscription);
    }
}