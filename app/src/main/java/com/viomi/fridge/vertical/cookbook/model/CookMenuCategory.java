package com.viomi.fridge.vertical.cookbook.model;

import java.io.Serializable;
import java.util.List;

/**
 * 菜谱分类
 * Created by nanquan on 2018/2/12.
 */
public class CookMenuCategory implements Serializable {
    private CategoryInfo categoryInfo;
    private List<CookMenuCategory> childs;

    public static class CategoryInfo implements Serializable {
        private String ctgId;
        private String name;

        public String getCtgId() {
            return ctgId;
        }

        public void setCtgId(String ctgId) {
            this.ctgId = ctgId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public CategoryInfo getCategoryInfo() {
        return categoryInfo;
    }

    public void setCategoryInfo(CategoryInfo categoryInfo) {
        this.categoryInfo = categoryInfo;
    }

    public List<CookMenuCategory> getChilds() {
        return childs;
    }

    public void setChilds(List<CookMenuCategory> childs) {
        this.childs = childs;
    }
}