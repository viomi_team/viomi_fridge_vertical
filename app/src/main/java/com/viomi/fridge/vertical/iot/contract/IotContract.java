package com.viomi.fridge.vertical.iot.contract;

import com.miot.common.abstractdevice.AbstractDevice;
import com.viomi.fridge.vertical.common.callback.BasePresenter;

import java.util.List;

/**
 * 互联网家 Contract
 * Created by William on 2018/2/3.
 */
public interface IotContract {
    interface View {
        void showMiDeviceList(List<AbstractDevice> list);// 展示小米设备列表

        void showRefreshFail();// 刷新失败
    }

    interface Presenter extends BasePresenter<View> {
        void loadMiDeviceList();// 加载小米设备列表
    }
}
