package com.viomi.fridge.vertical.foodmanage.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 食材管理种类适配器
 * Created by William on 2018/2/25.
 */
public class FoodManageTypeAdapter extends BaseRecyclerViewAdapter<FoodManageTypeAdapter.TypeViewHolder> {
    private List<String> mList;
    private String mSelected;

    public FoodManageTypeAdapter(List<String> list, String selected) {
        mList = list;
        mSelected = selected;
        if (mList == null) mList = new ArrayList<>();
    }

    @Override
    public TypeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_holder_food_manage_type, parent, false);
        return new TypeViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(TypeViewHolder holder, int position) {
        String text = mList.get(position);
        holder.textView.setText(text);
        if (text.equals(mSelected)) holder.textView.setSelected(true);
        else holder.textView.setSelected(false);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class TypeViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.holder_food_manage_type)
        TextView textView;

        TypeViewHolder(View itemView, FoodManageTypeAdapter adapter) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> adapter.onItemHolderClick(this, 1000));
        }
    }
}