package com.viomi.fridge.vertical.album.activity;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.album.model.entity.AlbumEntity;
import com.viomi.fridge.vertical.album.model.preference.AlbumPreference;
import com.viomi.fridge.vertical.common.base.BaseActivity;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.widget.AutoScrollPageView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 屏保 Activity
 * Created by William on 2018/3/17.
 */
public class ScreenSaverActivity extends BaseActivity {
    private static final String TAG = ScreenSaverActivity.class.getSimpleName();
    private Subscription mSubscription;

    @BindView(R.id.screen_saver_album)
    AutoScrollPageView mAutoScrollPageView;// 轮播

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        layoutId = R.layout.activity_screen_saver;
        super.onCreate(savedInstanceState);

        mSubscription = Observable.just(AlbumPreference.getInstance().getAlbumEntityList())
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .map(list -> {
                    List<Uri> list1 = new ArrayList<>();
                    for (AlbumEntity entity : list) {
                        if (entity.getFile().exists())
                            list1.add(Uri.fromFile(entity.getFile()));
                    }
                    return list1;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(uris -> {
                    if (uris.size() == 0) { // 无屏保图
                        finish();
                        return;
                    }
                    mAutoScrollPageView.setIsScreenSaver(true);
                    mAutoScrollPageView.setCustomAdapterDatas(uris);
                    if (uris.size() > 1) mAutoScrollPageView.startAutoScroll();
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));

        mAutoScrollPageView.setOnTouchListener((view, motionEvent) -> {
            finish();
            return false;
        });
        mAutoScrollPageView.setOnViewPageClickListener(this::finish);
        findViewById(R.id.screen_saver_layout).setOnClickListener(view -> finish());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
    }
}
