package com.viomi.fridge.vertical.fridgecontrol.defined.action;

import com.viomi.fridge.vertical.fridgecontrol.defined.ViomiDefined;
import com.viomi.fridge.vertical.fridgecontrol.defined.property.SmartCool;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetSmartCool extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setSmartCool.toActionType();

    public SetSmartCool() {
        super(TYPE);

        super.addArgument(SmartCool.TYPE.toString());
    }
}