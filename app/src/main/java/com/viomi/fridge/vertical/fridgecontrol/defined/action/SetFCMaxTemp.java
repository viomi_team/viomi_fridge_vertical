package com.viomi.fridge.vertical.fridgecontrol.defined.action;

import com.viomi.fridge.vertical.fridgecontrol.defined.ViomiDefined;
import com.viomi.fridge.vertical.fridgecontrol.defined.property.FCMaxTemp;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetFCMaxTemp extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setFCMaxTemp.toActionType();

    public SetFCMaxTemp() {
        super(TYPE);

        super.addArgument(FCMaxTemp.TYPE.toString());
    }
}