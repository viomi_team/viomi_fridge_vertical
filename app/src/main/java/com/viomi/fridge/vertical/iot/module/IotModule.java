package com.viomi.fridge.vertical.iot.module;

import com.viomi.fridge.vertical.common.scope.ActivityScoped;
import com.viomi.fridge.vertical.iot.contract.IotContract;
import com.viomi.fridge.vertical.iot.presenter.IotPresenter;

import dagger.Binds;
import dagger.Module;

/**
 * 互联网家 Module
 * Created by William on 2018/2/3.
 */
@Module
public abstract class IotModule {

    @ActivityScoped
    @Binds
    abstract IotContract.Presenter iotPresenter(IotPresenter presenter);
}