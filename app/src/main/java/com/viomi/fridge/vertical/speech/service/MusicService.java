package com.viomi.fridge.vertical.speech.service;

import android.animation.ObjectAnimator;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.media.audiofx.Visualizer;
import android.net.Uri;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.BounceInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;

import com.alibaba.fastjson.JSON;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.entertainment.ICallBack;
import com.viomi.fridge.vertical.entertainment.IPlayback;
import com.viomi.fridge.vertical.entertainment.MusicPlayActivity;
import com.viomi.fridge.vertical.entertainment.MusicPlayer;
import com.viomi.fridge.vertical.speech.model.entity.MediaContentEntity;
import com.viomi.fridge.vertical.speech.model.entity.MediaEntity;
import com.viomi.widget.floatwindow.FloatWindow;
import com.viomi.widget.floatwindow.MoveType;
import com.viomi.widget.floatwindow.Screen;
import com.viomi.widget.floatwindow.ViewStateListener;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import rx.Subscription;

/**
 * 音乐播放 Service
 * Created by William on 2018/8/14.
 */
public class MusicService extends Service implements IPlayback, AudioManager.OnAudioFocusChangeListener {
    private static final String TAG = MusicService.class.getSimpleName();
    private static final int MSG_SHOW_FLOAT_WINDOW = 1;// 显示悬浮窗
    private static final int MSG_HIDE_FLOAT_WINDOW = 2;// 隐藏悬浮窗
    private static final int MSG_UPDATE_FLOAT_WINDOW = 3;// 更新悬浮窗
    private static final int MSG_START_ANIMATION = 4;// 开始动画
    private static final int MSG_STOP_ANIMATION = 5;// 停止动画
    private WindowManager mWindowManager;
    private SimpleDraweeView mSimpleDraweeView;// 悬浮窗图片
    private View mView, mDeleteView;// 悬浮窗布局，移除悬浮窗布局
    private ObjectAnimator mObjectAnimator;
    private String mType;// 播放类型
    private IPlayback mCurrentPlayer, mPreparePlayer;
    private HandlerThread mThread, mPrepareThread;
    private Handler mHandler, mPrepareHandler;
    private MusicHandler mMusicHandler;
    private ICallBack mICallBack, mCustomCallBack;
    private AudioManager mAudioManager;
    private Subscription mSubscription;
    private int mCurrentPosition, mPosition_x, mPosition_y;
    private List<MediaContentEntity> mList;// 播放资源集合
    private MusicBinder mBinder;
    private int mTotalTime = 0;// 歌曲总时间
    private boolean mIsAudioFocusGain, mIsPause = false, mIsFirst = true;// 音频是否获取焦点，是否暂停

    public enum State {
        IDLE, INITIALIZED, PREPARING, PREPARED, STARTED, PAUSED, STOPPED, PLAYBACK_COMPLETED, END, ERROR
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mCurrentPlayer = getPreparedPlayer();
        initBackgroundThread();
        initPrepareThread();
        requestAudioFocus();
        setCallBack(playCallBack);
        mMusicHandler = new MusicHandler(this);
        mBinder = new MusicBinder();
        mList = new ArrayList<>();

        mSubscription = RxBus.getInstance().subscribe(busEvent -> {
            switch (busEvent.getMsgId()) {
                case BusEvent.MSG_PLAY_CONTINUE: // 继续播放
                    mIsPause = false;
                    resume();
                    break;
                case BusEvent.MSG_PLAY_PAUSE: // 暂停播放
                    mIsPause = true;
                    pause();
                    break;
                case BusEvent.MSG_PLAY_NEXT: // 下一首
                    mIsPause = false;
                    playNext();
                    break;
                case BusEvent.MSG_PLAY_PREVIOUS: // 上一首
                    mIsPause = false;
                    playPrevious();
                    break;
                case BusEvent.MSG_PLAY_STOP: // 停止播放
                    stopSelf();
                    break;
                case BusEvent.MSG_STOP_SPEAK: // 语音停止说
                    if (!mIsPause) resume();
                    break;
                case BusEvent.MSG_START_SPEAK: // 语音开始说
                    if (!mIsPause) pause();
                    break;
                case BusEvent.MSG_SHOW_MUSIC_FLOAT: // 显示音乐悬浮按钮
                    if (mMusicHandler != null)
                        Message.obtain(mMusicHandler, MSG_SHOW_FLOAT_WINDOW).sendToTarget();
                    break;
            }
        });
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            mType = intent.getStringExtra(AppConstants.SPEECH_MEDIA_TYPE);
            String data = intent.getStringExtra(AppConstants.SPEECH_MEDIA_DATA);
            Intent activityIntent = new Intent(this, MusicPlayActivity.class);
            mList.clear();
            MediaEntity mediaEntity = JSON.parseObject(data, MediaEntity.class);
            mList.addAll(mediaEntity.getContent());
            mCurrentPosition = 0;
            String url = mList.get(mCurrentPosition).getLinkUrl();
            if (url != null && !TextUtils.isEmpty(url)) {
                play(url);
                if (mIsFirst || ToolUtil.isActivityRunning(FridgeApplication.getContext(), "com.viomi.fridge.vertical.entertainment.MusicPlayActivity")) {
                    mIsFirst = false;
                    activityIntent.putExtra(AppConstants.SPEECH_MEDIA_DATA, mList.get(mCurrentPosition));
                    activityIntent.putExtra(AppConstants.SPEECH_MEDIA_TYPE, mType);
                    activityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(activityIntent);
                } else if (mMusicHandler != null) {
                    Message.obtain(mMusicHandler, MSG_UPDATE_FLOAT_WINDOW).sendToTarget();
                }
            } else stopSelf();
        }
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        destroy();
        hideFloatWindow();
        removeDeleteLayout();
        if (mThread != null && mThread.isAlive()) {
            mThread.quit();
            mHandler.removeCallbacksAndMessages(null);
        }
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
        if (mMusicHandler != null) {
            mMusicHandler.removeCallbacksAndMessages(null);
            mMusicHandler = null;
        }
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_LOSS: // 长时间音频失去焦点
                logUtil.e(TAG, "music loss audio focus");
                mIsAudioFocusGain = false;
                if (mCurrentPlayer != null && mCurrentPlayer.isPlaying()) {
                    mCurrentPlayer.stop();
                    mCurrentPlayer.destroy();
                }
                if (mPreparePlayer != null) {
                    mPreparePlayer.stop();
                    mPreparePlayer.destroy();
                }
                break;
            case AudioManager.AUDIOFOCUS_GAIN:
                mIsAudioFocusGain = true;
                break;
        }
    }

    @Override
    public void play(String url) {
        logUtil.d(TAG, "play：" + url);
        mHandler.removeCallbacksAndMessages(null);
        mCurrentPlayer.stop();
        checkBackgroundThread();
        mHandler.post(() -> {
            if (TextUtils.isEmpty(url)) {
                mICallBack.onComplete(false);
                return;
            }
            if (!mIsAudioFocusGain) requestAudioFocus();
            initPlayer();
            mCurrentPlayer.play(url);
        });
    }

    @Override
    public void replay() {

    }

    @Override
    public void pause() {
        checkBackgroundThread();
        mHandler.post(() -> mCurrentPlayer.pause());
    }

    @Override
    public void resume() {
        checkBackgroundThread();
        mHandler.post(() -> mCurrentPlayer.resume());
    }

    @Override
    public void stop() {
        checkBackgroundThread();
        mHandler.post(() -> mCurrentPlayer.stop());
    }

    @Override
    public void seekTo(int position) {
        checkBackgroundThread();
        mHandler.post(() -> {
            if (!mCurrentPlayer.isPlaying()) {
                mCurrentPlayer.replay();
            }
            mCurrentPlayer.seekTo(position);
        });
    }

    @Override
    public State getState() {
        return mCurrentPlayer.getState();
    }

    @Override
    public boolean isPlaying() {
        return mCurrentPlayer != null && mCurrentPlayer.isPlaying();
    }

    @Override
    public void destroy() {
        if (mCurrentPlayer != null) mCurrentPlayer.destroy();
        if (mPreparePlayer != null) mPreparePlayer.destroy();
    }

    @Override
    public void setCallBack(ICallBack callBack) {
        mICallBack = callBack;
    }

    @Override
    public int getPosition() {
        if (getState() == State.STARTED || getState() == State.PAUSED) {
            return mCurrentPlayer.getPosition();
        }
        return 0;
    }

    @Override
    public boolean isPrepared() {
        return false;
    }

    @Override
    public void setIsPrepared(boolean flag) {

    }

    @Override
    public void prepareNext(String url, String cp) {
        mPrepareHandler.removeCallbacksAndMessages(null);
        checkPrepareThread();
        mPrepareHandler.post(() -> {
            mPreparePlayer = getUnPreparedPlayer();
            if (mPreparePlayer == null) return;
            if (!TextUtils.isEmpty(url)) {
                mPreparePlayer.prepareNext(url, cp);
            }
        });
    }

    private void initBackgroundThread() {
        mThread = new HandlerThread("background");
        mThread.start();
        mHandler = new Handler(mThread.getLooper());
    }

    private void initPrepareThread() {
        mPrepareThread = new HandlerThread("prepareThread");
        mPrepareThread.start();
        mPrepareHandler = new Handler(mPrepareThread.getLooper());
    }

    private void checkBackgroundThread() {
        if (!mThread.isAlive()) {
            try {
                mThread.start();
                mHandler = new Handler(mThread.getLooper());
            } catch (IllegalThreadStateException e) {
                e.printStackTrace();
            }
        }
    }

    private void checkPrepareThread() {
        if (!mPrepareThread.isAlive()) {
            try {
                mPrepareThread.start();
                mPrepareHandler = new Handler(mPrepareThread.getLooper());
            } catch (IllegalThreadStateException e) {
                e.printStackTrace();
            }
        }
    }

    public void setCustomCallback(ICallBack callback) {
        this.mCustomCallBack = callback;
    }

    public void removeCustomCallback() {
        mCustomCallBack = null;
    }

    /**
     * 获取 Audio 焦点
     */
    private void requestAudioFocus() {
        if (mAudioManager == null) {
            mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        }
        if (mAudioManager != null) {
            int result = mAudioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
            if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                mIsAudioFocusGain = true;
            }
        }
    }

    private void initPlayer() {
        logUtil.e(TAG, "initPlayer: ");
        mCurrentPlayer = getPreparedPlayer();
        mCurrentPlayer.setCallBack(mICallBack);
    }

    private MusicPlayer getPreparedPlayer() {
        if (MusicPlayer.getFirstPlayer(this).isPrepared()) {
            return MusicPlayer.getFirstPlayer(this);
        } else if (MusicPlayer.getSecondPlayer(this).isPrepared()) {
            return MusicPlayer.getSecondPlayer(this);
        }
        return MusicPlayer.getFirstPlayer(this);
    }

    private MusicPlayer getUnPreparedPlayer() {
        // 避免快速切换时获取正在播放的播放器
        MusicPlayer firstPlayer = MusicPlayer.getFirstPlayer(this);
        MusicPlayer secondPlayer = MusicPlayer.getSecondPlayer(this);
        if (firstPlayer.getState() != State.STARTED && !firstPlayer.isPrepared()) {
            return firstPlayer;
        } else if (secondPlayer.getState() != State.STARTED && !secondPlayer.isPrepared()) {
            return secondPlayer;
        }
        return null;
    }

    public void playNext() {
        if (mList != null && mList.size() > 0) {
            mCurrentPosition = (++mCurrentPosition) % mList.size();
            play(mList.get(mCurrentPosition).getLinkUrl());
        }
        if (ToolUtil.isActivityRunning(FridgeApplication.getContext(), "com.viomi.fridge.vertical.entertainment.MusicPlayActivity")) {
            Intent intent = new Intent(this, MusicPlayActivity.class);
            intent.putExtra(AppConstants.SPEECH_MEDIA_DATA, mList.get(mCurrentPosition));
            intent.putExtra(AppConstants.SPEECH_MEDIA_TYPE, mType);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            if (mMusicHandler != null)
                Message.obtain(mMusicHandler, MSG_UPDATE_FLOAT_WINDOW).sendToTarget();
        }
    }

    public void playPrevious() {
        if (mList != null && mList.size() > 0) {
            mCurrentPosition = (--mCurrentPosition) % mList.size();
            if (mCurrentPosition < 0) {
                mCurrentPosition = mList.size() + mCurrentPosition;
            }
            play(mList.get(mCurrentPosition).getLinkUrl());
        }
        if (ToolUtil.isActivityRunning(FridgeApplication.getContext(), "com.viomi.fridge.vertical.entertainment.MusicPlayActivity")) {
            Intent intent = new Intent(this, MusicPlayActivity.class);
            intent.putExtra(AppConstants.SPEECH_MEDIA_DATA, mList.get(mCurrentPosition));
            intent.putExtra(AppConstants.SPEECH_MEDIA_TYPE, mType);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            if (mMusicHandler != null)
                Message.obtain(mMusicHandler, MSG_UPDATE_FLOAT_WINDOW).sendToTarget();
        }
    }

    public void getProgress() {
        if (mTotalTime == 0) return;
        if (isPlaying()) {
            if (mCustomCallBack != null) mCustomCallBack.onPlay();
        } else {
            if (mCustomCallBack != null) mCustomCallBack.onPause();
        }
        if (mCustomCallBack != null) {
            mCustomCallBack.onSetTotalTime(mTotalTime);
            mCustomCallBack.onUpdateSeekBar(0);
        }
    }

    private void showFloatWindow() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater == null) return;
        mView = inflater.inflate(R.layout.view_music_float_window, null);
        mSimpleDraweeView = mView.findViewById(R.id.music_float_cover);
        Uri uri;
        if (mType.equals(AppConstants.SPEECH_MEDIA_MUSIC) && mList.get(mCurrentPosition).getImageUrl() != null
                && !mList.get(mCurrentPosition).getImageUrl().isEmpty()) {
            uri = Uri.parse(mList.get(mCurrentPosition).getImageUrl());
        } else {
            uri = Uri.parse("android.resource://" + FridgeApplication.getContext().getPackageName() + "/" + R.drawable.icon_music_float_window_default);
        }
        ImageRequest request = ImageRequestBuilder
                .newBuilderWithSource(uri)
                .setResizeOptions(new ResizeOptions((int) getResources().getDimension(R.dimen.px_x_100), (int) getResources().getDimension(R.dimen.px_x_100)))
                .build();
        PipelineDraweeController controller = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                .setOldController(mSimpleDraweeView.getController())
                .setImageRequest(request)
                .build();
        mSimpleDraweeView.setController(controller);
        FloatWindow
                .with(getApplicationContext())
                .setView(mView)
                .setX(Screen.width, 0.8f)
                .setY(Screen.height, 0.3f)
                .setMoveType(MoveType.slide, 10, 10)
                .setMoveStyle(500, new BounceInterpolator())
                .setViewStateListener(new ViewStateListener() {
                    @Override
                    public void onPositionUpdate(int x, int y) {
                        mPosition_x = x;
                        mPosition_y = y;
                        if (mDeleteView != null && mDeleteView.getVisibility() != View.VISIBLE)
                            mDeleteView.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onShow() {
                        startAnimation();
                    }

                    @Override
                    public void onHide() {

                    }

                    @Override
                    public void onDismiss() {

                    }

                    @Override
                    public void onMoveAnimStart() {

                    }

                    @Override
                    public void onMoveAnimEnd() {
                        if (mPosition_x >= 820 && mPosition_y >= 1720) {
                            stopSelf();
                        } else if (mDeleteView != null && mDeleteView.getVisibility() == View.VISIBLE)
                            mDeleteView.setVisibility(View.GONE);
                    }

                    @Override
                    public void onBackToDesktop() {

                    }
                })
                .setDesktopShow(true)
                .build();
        mView.setOnClickListener(v -> {
            Intent intent = new Intent(this, MusicPlayActivity.class);
            intent.putExtra(AppConstants.SPEECH_MEDIA_DATA, mList.get(mCurrentPosition));
            intent.putExtra(AppConstants.SPEECH_MEDIA_TYPE, mType);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            hideFloatWindow();
        });
    }

    private void hideFloatWindow() {
        FloatWindow.destroy();
        mObjectAnimator = null;
        mView = null;
        mSimpleDraweeView = null;
    }

    private void updateFloatWindow() {
        if (FloatWindow.get() == null || !FloatWindow.get().isShowing() || mView == null || mSimpleDraweeView == null)
            return;
        Uri uri;
        if (mType.equals(AppConstants.SPEECH_MEDIA_MUSIC) && mList.get(mCurrentPosition).getImageUrl() != null
                && !mList.get(mCurrentPosition).getImageUrl().isEmpty()) {
            uri = Uri.parse(mList.get(mCurrentPosition).getImageUrl());
        } else {
            uri = Uri.parse("android.resource://" + FridgeApplication.getContext().getPackageName() + "/" + R.drawable.icon_music_float_window_default);
        }
        ImageRequest request = ImageRequestBuilder
                .newBuilderWithSource(uri)
                .setResizeOptions(new ResizeOptions((int) getResources().getDimension(R.dimen.px_x_100), (int) getResources().getDimension(R.dimen.px_x_100)))
                .build();
        PipelineDraweeController controller = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                .setOldController(mSimpleDraweeView.getController())
                .setImageRequest(request)
                .build();
        mSimpleDraweeView.setController(controller);
    }

    private void startAnimation() {
        if (FloatWindow.get() == null || !FloatWindow.get().isShowing() || mView == null || mSimpleDraweeView == null)
            return;
        if (mObjectAnimator == null) {
            mObjectAnimator = ObjectAnimator.ofFloat(mView, "rotation", 0, 359);
            mObjectAnimator.setRepeatCount(ObjectAnimator.INFINITE);
            mObjectAnimator.setRepeatMode(ObjectAnimator.RESTART);
            // 线型插值器，动画匀速执行
            mObjectAnimator.setInterpolator(new LinearInterpolator());
            mObjectAnimator.setDuration(10 * 1000);
        }
        if (isPlaying()) mObjectAnimator.start();
    }

    private void stopAnimation() {
        if (FloatWindow.get() == null || !FloatWindow.get().isShowing() || mView == null || mSimpleDraweeView == null)
            return;
        if (mObjectAnimator != null) {
            mObjectAnimator.cancel();
        }
    }

    private void addDeleteLayout() {
        if (mWindowManager == null)
            mWindowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater == null) return;
        mDeleteView = inflater.inflate(R.layout.float_window_delete_layout, null);
        FrameLayout frameLayout = mDeleteView.findViewById(R.id.delete_layout_bg);
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) frameLayout.getLayoutParams();
        layoutParams.setMarginEnd(-200);
        frameLayout.setLayoutParams(layoutParams);
        mWindowManager.addView(mDeleteView, getWindowLayoutParams());
        mDeleteView.setVisibility(View.GONE);
    }

    private void removeDeleteLayout() {
        if (mWindowManager != null) mWindowManager.removeView(mDeleteView);
    }

    private WindowManager.LayoutParams getWindowLayoutParams() {
        WindowManager.LayoutParams wmParams = new WindowManager.LayoutParams();
        wmParams.format = PixelFormat.TRANSPARENT;
        wmParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM;
        wmParams.x = ToolUtil.getScreenWidth(FridgeApplication.getContext());
        wmParams.y = ToolUtil.getScreenHeight(FridgeApplication.getContext()) / 2;
        wmParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        wmParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        wmParams.type = WindowManager.LayoutParams.TYPE_PHONE;
        return wmParams;
    }

    private ICallBack playCallBack = new ICallBack() {
        @Override
        public void onPlay() {
            if (mCustomCallBack != null) mCustomCallBack.onPlay();
            if (mMusicHandler != null)
                Message.obtain(mMusicHandler, MSG_START_ANIMATION).sendToTarget();
        }

        @Override
        public void onPause() {
            if (mCustomCallBack != null) mCustomCallBack.onPause();
            if (mMusicHandler != null)
                Message.obtain(mMusicHandler, MSG_STOP_ANIMATION).sendToTarget();
        }

        @Override
        public void onResume() {
            if (mCustomCallBack != null) mCustomCallBack.onResume();
            if (mMusicHandler != null)
                Message.obtain(mMusicHandler, MSG_START_ANIMATION).sendToTarget();
        }

        @Override
        public void onStop() {
            if (mCustomCallBack != null) mCustomCallBack.onStop();
            if (mMusicHandler != null)
                Message.obtain(mMusicHandler, MSG_STOP_ANIMATION).sendToTarget();
        }

        @Override
        public void onBuffering() {
            if (mCustomCallBack != null) mCustomCallBack.onBuffering();
        }

        @Override
        public void onBufferComplete() {
            if (mCustomCallBack != null) mCustomCallBack.onBufferComplete();
        }

        @Override
        public void onBufferingUpdate(int percent) {
            if (mCustomCallBack != null) mCustomCallBack.onBufferingUpdate(percent);
        }

        @Override
        public void onUpdateSeekBar(int time) {
            if (mCustomCallBack != null) mCustomCallBack.onUpdateSeekBar(time);
        }

        @Override
        public void onComplete(boolean isFinish) {
            if (mCustomCallBack != null) mCustomCallBack.onComplete(isFinish);
            if (mMusicHandler != null)
                Message.obtain(mMusicHandler, MSG_STOP_ANIMATION).sendToTarget();
            playNext();
        }

        @Override
        public void onPlayError(int errorCode, String errorMsg) {

        }

        @Override
        public void onPrepareNext() {

        }

        @Override
        public void onPrepare() {

        }

        @Override
        public void onSetTotalTime(int time) {
            mTotalTime = time;
            if (mCustomCallBack != null) mCustomCallBack.onSetTotalTime(time);
        }

        @Override
        public void onSetVisualizer(Visualizer visualizer) {

        }

        @Override
        public void onUpdateUIState(UIState uiState) {

        }
    };

    public class MusicBinder extends Binder {
        public MusicService getService() {
            return MusicService.this;
        }
    }

    private static class MusicHandler extends Handler {
        WeakReference<MusicService> weakReference;

        MusicHandler(MusicService service) {
            this.weakReference = new WeakReference<>(service);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (this.weakReference != null) {
                MusicService service = this.weakReference.get();
                if (service != null) {
                    switch (msg.what) {
                        case MSG_SHOW_FLOAT_WINDOW: // 显示悬浮窗
                            service.addDeleteLayout();
                            service.showFloatWindow();
                            break;
                        case MSG_HIDE_FLOAT_WINDOW: // 隐藏悬浮窗
                            service.removeDeleteLayout();
                            service.hideFloatWindow();
                            break;
                        case MSG_UPDATE_FLOAT_WINDOW: // 更新悬浮窗
                            service.updateFloatWindow();
                            break;
                        case MSG_START_ANIMATION: // 开始动画
                            service.startAnimation();
                            break;
                        case MSG_STOP_ANIMATION: // 停止动画
                            service.stopAnimation();
                            break;
                    }
                }
            }
        }
    }
}
