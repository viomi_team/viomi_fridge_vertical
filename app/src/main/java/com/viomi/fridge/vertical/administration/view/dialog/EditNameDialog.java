package com.viomi.fridge.vertical.administration.view.dialog;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseDialog;
import com.viomi.fridge.vertical.common.util.ToastUtil;

public class EditNameDialog extends BaseDialog implements View.OnClickListener {
    private Context mContext;
    private EditText edInfo;

    private EditNameDialogListener mEditNameDialogListener;

    public void setEditNameDialogListener(EditNameDialogListener mEditNameDialogListener) {
        this.mEditNameDialogListener = mEditNameDialogListener;
    }

    public EditNameDialog(Context context) {
        super(context, R.style.DialogGrey);
        setContentView(R.layout.dialog_edit_name);
        this.mContext = context;
        setCancelable(false);
    }

    @Override
    public void initView() {
        TextView tvBtnLeft = findViewById(R.id.tvBtnLeft);
        TextView tvBtnRight = findViewById(R.id.tvBtnRight);
        tvBtnLeft.setOnClickListener(this);
        tvBtnRight.setOnClickListener(this);
        edInfo = findViewById(R.id.edInfo);
    }

    public void show(String name) {
        show();
        edInfo.setText(name);
        GradientDrawable drawable = (GradientDrawable) edInfo.getBackground();
        drawable.setColor(Color.parseColor("#f0f0f0"));
        edInfo.setBackground(drawable);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvBtnLeft: {
                cancel();
            }
            break;
            case R.id.tvBtnRight: {
                if (mEditNameDialogListener == null) {
                    return;
                }
                if (TextUtils.isEmpty(edInfo.getText())) {
                    ToastUtil.show(mContext, "请输入蓝牙名称");
                } else if (mEditNameDialogListener != null) {
                    mEditNameDialogListener.btnRightClick(edInfo.getText().toString().trim());
                    cancel();
                }
            }
            break;
        }
    }

    public interface EditNameDialogListener {
        void btnRightClick(String name);
    }
}