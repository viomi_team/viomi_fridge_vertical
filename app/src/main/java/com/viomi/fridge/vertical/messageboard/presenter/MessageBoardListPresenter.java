package com.viomi.fridge.vertical.messageboard.presenter;

import android.content.Context;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.ToastUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.messageboard.contract.MessageBoardListContract;
import com.viomi.fridge.vertical.messageboard.model.MessageBoardRepository;
import com.viomi.fridge.vertical.messageboard.model.entity.MessageBoardEntity;

import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * 留言板列表 Presenter
 * Created by William on 2018/4/11.
 */
public class MessageBoardListPresenter implements MessageBoardListContract.Presenter {
    private static final String TAG = MessageBoardListPresenter.class.getSimpleName();
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;

    @Nullable
    private MessageBoardListContract.View mView;

    @Inject
    MessageBoardListPresenter(Context context) {
        mContext = context;
    }

    @Override
    public void subscribe(MessageBoardListContract.View view) {
        mView = view;
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public void unSubscribe() {
        mView = null;
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
    }

    @Override
    public void loadList(List<MessageBoardEntity> list) {
        Subscription subscription = MessageBoardRepository.getInstance().getMessageBoardList()
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(messageBoardEntities -> {
                    if (messageBoardEntities != null && mView != null) {
                        int size = list.size();
                        list.clear();
                        mView.notifyDataRemoved(size);
                        list.addAll(messageBoardEntities);
                        mView.notifyDataInserted(list.size());
                        mView.display();
                    }
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void delete(List<MessageBoardEntity> list) {
        Subscription subscription = MessageBoardRepository.getInstance().deleteList(list)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(aBoolean -> {
                    if (aBoolean && mView != null) {
                        mView.notifyDataChanged();
                        mView.display();
                    } else
                        ToastUtil.showCenter(mContext, mContext.getResources().getString(R.string.message_board_delete_fail_tip));
                }, throwable -> {
                    logUtil.e(TAG, throwable.getMessage());
                    ToastUtil.showCenter(mContext, mContext.getResources().getString(R.string.message_board_delete_fail_tip));
                });
        mCompositeSubscription.add(subscription);
    }
}
