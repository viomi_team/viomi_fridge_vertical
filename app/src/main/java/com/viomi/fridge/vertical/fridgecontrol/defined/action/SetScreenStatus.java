package com.viomi.fridge.vertical.fridgecontrol.defined.action;

import com.viomi.fridge.vertical.fridgecontrol.defined.ViomiDefined;
import com.viomi.fridge.vertical.fridgecontrol.defined.property.ScreenStatus;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetScreenStatus extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setScreenStatus.toActionType();

    public SetScreenStatus() {
        super(TYPE);

        super.addArgument(ScreenStatus.TYPE.toString());
    }
}