package com.viomi.fridge.vertical.iot.model.http.entity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 烟机统计数据
 * Created by William on 2018/2/22.
 */

public class RangeHoodData {
    private int mCount;// 累计运行次数
    private int mTime;// 累计运行时间
    private int mPurify;// 累计净化量

    public RangeHoodData(List<Object> list) {
        try {
            JSONObject data = new JSONObject(list.get(0).toString());
            String value = data.optString("value");
            if (value.equals("")) return;
            Pattern pattern = Pattern.compile("\\d+");
            Matcher matcher = pattern.matcher(value);
            if (matcher.find()) mCount = Integer.parseInt(matcher.group(0));
            if (matcher.find()) mTime = Integer.parseInt(matcher.group(0));
            if (matcher.find()) mPurify = Integer.parseInt(matcher.group(0));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getmCount() {
        return mCount;
    }

    public int getmTime() {
        return mTime;
    }

    public int getmPurify() {
        return mPurify;
    }
}
