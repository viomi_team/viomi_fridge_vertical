package com.viomi.fridge.vertical.speech.model.repository.skill;

import android.content.Context;

import com.miot.common.abstractdevice.AbstractDevice;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.administration.model.entity.QRCodeBase;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.iot.model.repository.FanRepository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 云米风扇技能
 * Created by William on 2018/8/30.
 */
public class ViomiFanSkill {
    private static final String TAG = ViomiFanSkill.class.getSimpleName();

    public static void handle(Context context, String data, AbstractDevice device) {
        String content;// 语音播报内容
        QRCodeBase qrCodeBase = (QRCodeBase) ToolUtil.getFileObject(context, AppConstants.USER_INFO_FILE);
        if (qrCodeBase == null) {
            content = "请先登录云米账号";
            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
            return;
        }
        if (device == null) {
            content = "没有连接到电风扇";
            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
            return;
        }

        try {
            JSONObject jsonObject = new JSONObject(data);
            JSONObject nlu = jsonObject.optJSONObject("nlu");
            if (nlu == null) return;
            JSONObject semantics = nlu.optJSONObject("semantics");
            if (semantics == null) return;
            JSONObject request = semantics.optJSONObject("request");
            if (request == null) return;
            JSONArray slots = semantics.optJSONObject("request").optJSONArray("slots");
            if (slots == null || slots.length() <= 0) return;

            String value = "";// 意图
            for (int i = 0; i < slots.length(); i++) {
                JSONObject jsonItem = slots.optJSONObject(i);
                String name = jsonItem.optString("name");
                if (name.equals("intent")) {
                    value = jsonItem.optString("value");
                    break;
                }
            }

            switch (value) {
                case "打开风扇":
                    FanRepository.setPower("1", device.getDeviceId())
                            .compose(RxSchedulerUtil.SchedulersTransformer1())
                            .subscribe(rpcResult -> {
                            }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                    content = "正在为你开启" + device.getName();
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "关闭风扇":
                    FanRepository.setPower("0", device.getDeviceId())
                            .compose(RxSchedulerUtil.SchedulersTransformer1())
                            .subscribe(rpcResult -> {
                            }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                    content = "正在为你关闭" + device.getName();
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "加大风速":
                    FanRepository.setWindVolume("1", device.getDeviceId())
                            .compose(RxSchedulerUtil.SchedulersTransformer1())
                            .subscribe(rpcResult -> {
                            }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                    content = "正在为你加大" + device.getName() + "风速";
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "减小风速":
                    FanRepository.setWindVolume("0", device.getDeviceId())
                            .compose(RxSchedulerUtil.SchedulersTransformer1())
                            .subscribe(rpcResult -> {
                            }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                    content = "正在为你降低" + device.getName() + "风速";
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "开启摆风":
                    FanRepository.setShake("1", device.getDeviceId())
                            .compose(RxSchedulerUtil.SchedulersTransformer1())
                            .subscribe(rpcResult -> {
                            }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                    content = "正在为你开启" + device.getName() + "摇头";
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "关闭摆风":
                    FanRepository.setShake("0", device.getDeviceId())
                            .compose(RxSchedulerUtil.SchedulersTransformer1())
                            .subscribe(rpcResult -> {
                            }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                    content = "正在为你关闭" + device.getName() + "摇头";
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "设置风速":
                    String value_shift = "";
                    for (int i = 0; i < slots.length(); i++) {
                        JSONObject itemJson = slots.optJSONObject(i);
                        String name = itemJson.optString("name");
                        if ("档位".equals(name)) {
                            value_shift = itemJson.optString("value");
                            break;
                        }
                    }
                    switch (value_shift) {
                        case "最小档":
                        case "一档":
                            FanRepository.setWindSpeed(35, device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "设为" + value_shift;
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "二档":
                            FanRepository.setWindSpeed(69, device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "设为" + value_shift;
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "中档":
                        case "三档":
                            FanRepository.setWindSpeed(103, device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "设为" + value_shift;
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "四档":
                            FanRepository.setWindSpeed(137, device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "设为" + value_shift;
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "五档":
                            FanRepository.setWindSpeed(171, device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "设为" + value_shift;
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "最高档":
                        case "六档":
                            FanRepository.setWindSpeed(206, device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "设为" + value_shift;
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                    }
                    break;
                case "设置风扇模式":
                    String mode = "";
                    for (int i = 0; i < slots.length(); i++) {
                        JSONObject itemJson = slots.optJSONObject(i);
                        String name = itemJson.optString("name");
                        if ("风扇模式".equals(name)) {
                            mode = itemJson.optString("value");
                            break;
                        }
                    }
                    switch (mode) {
                        case "标准风模式":
                            FanRepository.setWindMode(0, device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "设为" + mode;
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "自然风模式":
                            FanRepository.setWindMode(1, device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "设为" + mode;
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "AI智能风模式":
                            FanRepository.setWindMode(2, device.getDeviceId())
                                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                                    .subscribe(rpcResult -> {
                                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                            content = "正在为你把" + device.getName() + "设为" + mode;
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                    }
                    break;
            }
        } catch (JSONException e) {
            logUtil.e(TAG, e.toString());
            e.printStackTrace();
        }
    }
}
