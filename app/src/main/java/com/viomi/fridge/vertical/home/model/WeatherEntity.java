package com.viomi.fridge.vertical.home.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;

/**
 * 思必驰天气 Json
 * Created by William on 2018/8/13.
 */
public class WeatherEntity implements Serializable {
    @JSONField(name = "success")
    private boolean success;
    @JSONField(name = "message")
    private String message;
    @JSONField(name = "data")
    private WeatherDetailEntity data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public WeatherDetailEntity getData() {
        return data;
    }

    public void setData(WeatherDetailEntity data) {
        this.data = data;
    }
}
