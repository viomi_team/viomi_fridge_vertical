package com.viomi.fridge.vertical.message.view.fragment;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseFragment;
import com.viomi.fridge.vertical.common.entity.RequestResult;
import com.viomi.fridge.vertical.common.http.ApiClient;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.scope.ActivityScoped;
import com.viomi.fridge.vertical.common.util.PreferenceHelper;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.ToastUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;
import com.viomi.fridge.vertical.message.entity.ActivityMessage;
import com.viomi.fridge.vertical.message.manager.MessageDaoManager;
import com.viomi.fridge.vertical.message.manager.PushManager;
import com.viomi.fridge.vertical.message.view.adapter.ActivityMessageAdapter;
import com.viomi.fridge.vertical.web.activity.BrowserActivity;
import com.viomi.widget.dialog.BaseAlertDialog;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * 消息中心 - 活动促销
 * Created by nanquan on 2018/2/6.
 */
@ActivityScoped
public class ActivityMessageFragment extends BaseFragment {
    private static final String TAG = ActivityMessageFragment.class.getSimpleName();
    private List<ActivityMessage> mList;
    private PreferenceHelper mPreferenceHelper;
    private MessageDaoManager mMessageDaoManager;
    private CompositeSubscription mCompositeSubscription;

    @BindView(R.id.rv_message)
    RecyclerView mRecyclerView;// 消息列表

    @BindView(R.id.ll_message_empty)
    LinearLayout mLinearLayout;// 消息为空布局

    @Inject
    public ActivityMessageFragment() {

    }

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.fragment_message;
    }

    @Override
    protected void initWithOnCreateView() {
        mPreferenceHelper = new PreferenceHelper(FridgeApplication.getContext());
        mMessageDaoManager = new MessageDaoManager();
        mCompositeSubscription = new CompositeSubscription();
        initRecyclerView();
        if (!FridgePreference.getInstance().getModel().equals(AppConstants.MODEL_JD)) {
            initCacheData();
            initNetData();

            Subscription subscription = RxBus.getInstance().subscribe(busEvent -> {
                switch (busEvent.getMsgId()) {
                    case BusEvent.MSG_PUSH_MESSAGE: // 接收到新消息
                        if (PushManager.getInstance().isInAcceptTime() && PushManager.getInstance().isActivityPushEnable()) {
                            if (getActivity() != null) {
                                getActivity().runOnUiThread(() -> {
                                    mList.clear();
                                    mRecyclerView.getAdapter().notifyDataSetChanged();
                                    initCacheData();
                                    initNetData();
                                });
                            }
                        }
                        break;
                }
            });
            mCompositeSubscription.add(subscription);
        }
    }

    private void initRecyclerView() {
        mList = new ArrayList<>();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        ActivityMessageAdapter messageAdapter = new ActivityMessageAdapter(getActivity(), mList);
        mRecyclerView.setAdapter(messageAdapter);
        messageAdapter.setOnItemClickListener(new ActivityMessageAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                ActivityMessage message = mList.get(position);
                if (message != null && !TextUtils.isEmpty(message.getAccessurl())) {
                    Intent intent = new Intent(getActivity(), BrowserActivity.class);
                    intent.putExtra("url", message.getAccessurl());
                    startActivity(intent);
                }
            }

            @Override
            public void onItemDeleteClick(int position) {
                if (getActivity() != null) {
                    BaseAlertDialog dialog = new BaseAlertDialog(getActivity(), FridgeApplication.getContext().getResources().getString(R.string.message_delete_tip), FridgeApplication.getContext().getResources().getString(R.string.cancel), FridgeApplication.getContext().getResources().getString(R.string.confirm));
                    dialog.setOnLeftClickListener(dialog::dismiss);
                    dialog.setOnRightClickListener(() -> {
                        ActivityMessage message = mList.get(position);
                        if (message != null && !TextUtils.isEmpty(message.getId())) {
                            mMessageDaoManager.deleteActivityMessage(message.getId());
                            mList.remove(position);
                            notifyData();
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
            }
        });
    }

    private void initCacheData() {
        List<ActivityMessage> cacheList = mMessageDaoManager.getActivityMessages();
        if (cacheList != null && cacheList.size() > 0) {
            mList.addAll(cacheList);
            notifyData();
        }
    }

    private void initNetData() {
        String lastId = mPreferenceHelper.getLastActivityMessageId();
        Subscription subscription = ApiClient.getInstance().getApiService().getActivityMessages(lastId)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .subscribe(new Subscriber<RequestResult<List<ActivityMessage>>>() {
                    @Override
                    public void onNext(RequestResult<List<ActivityMessage>> listRequestResult) {
                        if (listRequestResult == null ||
                                listRequestResult.getMobBaseRes() == null ||
                                listRequestResult.getMobBaseRes().getResult() == null ||
                                listRequestResult.getMobBaseRes().getResult().size() == 0) {
                            notifyData();
                            return;
                        }
                        List<ActivityMessage> resultList = listRequestResult.getMobBaseRes().getResult();
                        mList.addAll(resultList);
                        notifyData();
                        mPreferenceHelper.putLastActivityMessageId(resultList.get(0).getId());
                        mMessageDaoManager.saveActivityMessages(resultList);
                    }

                    @Override
                    public void onError(Throwable e) {
                        logUtil.d(TAG, e.toString());
                        notifyData();
                        ToastUtil.show(FridgeApplication.getContext(), FridgeApplication.getContext().getResources().getString(R.string.net_error));
                    }

                    @Override
                    public void onCompleted() {

                    }
                });
        mCompositeSubscription.add(subscription);
    }

    private void notifyData() {
        mRecyclerView.getAdapter().notifyDataSetChanged();
        if (mList.size() == 0) {
            mRecyclerView.setVisibility(View.GONE);
            mLinearLayout.setVisibility(View.VISIBLE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            mLinearLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
        if (mList != null) {
            mList.clear();
            mList = null;
        }
        if (mPreferenceHelper != null) mPreferenceHelper = null;
        if (mMessageDaoManager != null) mMessageDaoManager = null;
    }
}