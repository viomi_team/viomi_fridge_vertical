package com.viomi.fridge.vertical.common.entity;

/**
 * 网络请求返回实体
 * Created by nanquan on 2018/2/6.
 */
public class MobBaseRes<T> {

    private int code;       // 返回码

    private String desc;    // 返回信息

    private T result;       // 请求内容

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }
}
