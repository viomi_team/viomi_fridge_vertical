package com.viomi.fridge.vertical.speech.model.repository.skill;

import android.content.Context;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.administration.model.entity.QRCodeBase;
import com.viomi.fridge.vertical.administration.model.preference.LoginPreference;
import com.viomi.fridge.vertical.common.http.ApiClient;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.iot.model.http.entity.WLinkDeviceBean;
import com.viomi.fridge.vertical.iot.model.repository.LockReposity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import rx.schedulers.Schedulers;

/**
 * 物联设备技能
 * Created by William on 2018/8/24.
 */
public class WuLianSkill {
    private static final String TAG = WuLianSkill.class.getSimpleName();

    public static void handle(Context context, String data) {
        String content;// 语音播报内容
        QRCodeBase qrCodeBase = (QRCodeBase) ToolUtil.getFileObject(context, AppConstants.USER_INFO_FILE);
        if (qrCodeBase == null) {
            content = "请先登录云米账号";
            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
            return;
        }

        try {
            JSONObject jsonObject = new JSONObject(data);
            String intentName = jsonObject.optString("intentName");
            if (intentName == null || intentName.equals("")) return;

            LockReposity.getWlinkDeviceChildren(LoginPreference.getInstance().getUserToken())
                    .subscribeOn(Schedulers.io())
                    .onTerminateDetach()
                    .map(WuLianSkill::parseDevices)
                    .filter(linkDeviceBeans -> linkDeviceBeans != null && linkDeviceBeans.size() > 0)
                    .onTerminateDetach()
                    .subscribe(wLinkDeviceBeans -> control(wLinkDeviceBeans, intentName),
                            throwable -> logUtil.e(TAG, throwable.getMessage()));
        } catch (JSONException e) {
            logUtil.e(TAG, e.toString());
            e.printStackTrace();
        }
    }

    private static void control(List<WLinkDeviceBean> list, String intent) {
        JSONObject jsonObject = new JSONObject();
        switch (intent) {
            case "打开窗帘":
                try {
                    jsonObject.put("method", "open");
                    jsonObject.put("parameter", "0");
                    boolean hasDevice = filter(list, jsonObject, new String[]{"80"});
                    controlResult(hasDevice, "打开窗帘", "窗帘");
                } catch (JSONException e) {
                    logUtil.e(TAG, e.toString());
                    e.printStackTrace();
                }
                break;
            case "关闭窗帘":
                try {
                    jsonObject.put("method", "close");
                    jsonObject.put("parameter", "100");
                    boolean hasDevice = filter(list, jsonObject, new String[]{"80"});
                    controlResult(hasDevice, "关闭窗帘", "窗帘");
                } catch (JSONException e) {
                    logUtil.e(TAG, e.toString());
                    e.printStackTrace();
                }
                break;
            case "打开门锁":
                try {
                    jsonObject.put("method", "unLock");
                    jsonObject.put("parameter", "000000");
                    boolean hasDevice = filter(list, jsonObject, new String[]{"Bc"});
                    controlResult(hasDevice, "打开门锁", "门锁");
                } catch (JSONException e) {
                    logUtil.e(TAG, e.toString());
                    e.printStackTrace();
                }
                break;
            case "开关开启":
                try {
                    jsonObject.put("method", "turnOn");
                    jsonObject.put("endpointNumber", "1");
                    filter(list, jsonObject, new String[]{"An", "62", "Ao"});
                    jsonObject.put("endpointNumber", "2");
                    filter(list, jsonObject, new String[]{"An", "62", "Ao"});
                    jsonObject.put("endpointNumber", "3");
                    boolean hasDevice = filter(list, jsonObject, new String[]{"An", "62", "Ao"});
                    controlResult(hasDevice, "打开开关", "开关");
                } catch (JSONException e) {
                    logUtil.e(TAG, e.toString());
                    e.printStackTrace();
                }
                break;
            case "开关关闭":
                try {
                    jsonObject.put("method", "turnOff");
                    jsonObject.put("endpointNumber", "1");
                    filter(list, jsonObject, new String[]{"An", "62", "Ao"});
                    jsonObject.put("endpointNumber", "2");
                    filter(list, jsonObject, new String[]{"An", "62", "Ao"});
                    jsonObject.put("endpointNumber", "3");
                    boolean hasDevice = filter(list, jsonObject, new String[]{"An", "62", "Ao"});
                    controlResult(hasDevice, "关闭开关", "开关");
                } catch (JSONException e) {
                    logUtil.e(TAG, e.toString());
                    e.printStackTrace();
                }
                break;
            case "打开插座":
                try {
                    jsonObject.put("method", "turnOn");
                    jsonObject.put("endpointNumber", "1");
                    filter(list, jsonObject, new String[]{"An", "62", "Ao"});
                    jsonObject.put("endpointNumber", "2");
                    filter(list, jsonObject, new String[]{"An", "62", "Ao"});
                    jsonObject.put("endpointNumber", "3");
                    boolean hasDevice = filter(list, jsonObject, new String[]{"An", "62", "Ao"});
                    controlResult(hasDevice, "打开插座", "插座");
                } catch (JSONException e) {
                    logUtil.e(TAG, e.toString());
                    e.printStackTrace();
                }
                break;
            case "关闭插座":
                try {
                    jsonObject.put("method", "turnOff");
                    jsonObject.put("endpointNumber", "1");
                    filter(list, jsonObject, new String[]{"An", "62", "Ao"});
                    jsonObject.put("endpointNumber", "2");
                    filter(list, jsonObject, new String[]{"An", "62", "Ao"});
                    jsonObject.put("endpointNumber", "3");
                    boolean hasDevice = filter(list, jsonObject, new String[]{"An", "62", "Ao"});
                    controlResult(hasDevice, "关闭插座", "插座");
                } catch (JSONException e) {
                    logUtil.e(TAG, e.toString());
                    e.printStackTrace();
                }
                break;
            // 上线时屏蔽
            case "开灯":
                try {
                    jsonObject.put("method", "turnOn");
                    jsonObject.put("endpointNumber", "1");
                    filter(list, jsonObject, new String[]{"An", "62", "Ao"});
                    jsonObject.put("endpointNumber", "2");
                    filter(list, jsonObject, new String[]{"An", "62", "Ao"});
                    jsonObject.put("endpointNumber", "3");
                    boolean hasDevice = filter(list, jsonObject, new String[]{"An", "62", "Ao"});
                    controlResult(hasDevice, "开灯", "灯");
                } catch (JSONException e) {
                    logUtil.e(TAG, e.toString());
                    e.printStackTrace();
                }
                break;
            //上线时屏蔽
            case "关灯":
                try {
                    jsonObject.put("method", "turnOff");
                    jsonObject.put("endpointNumber", "1");
                    filter(list, jsonObject, new String[]{"An", "62", "Ao"});
                    jsonObject.put("endpointNumber", "2");
                    filter(list, jsonObject, new String[]{"An", "62", "Ao"});
                    jsonObject.put("endpointNumber", "3");
                    boolean hasDevice = filter(list, jsonObject, new String[]{"An", "62", "Ao"});
                    controlResult(hasDevice, "关灯", "灯");
                } catch (JSONException e) {
                    logUtil.e(TAG, e.toString());
                    e.printStackTrace();
                }
                break;
        }
    }

    private static boolean filter(List<WLinkDeviceBean> linkDeviceBeans, JSONObject jsonObject, String[] dtypes) throws JSONException {
        boolean hasDevice = false;
        for (int i = 0; i < linkDeviceBeans.size(); i++) {
            WLinkDeviceBean wLinkDeviceBean = linkDeviceBeans.get(i);
            for (String dtype : dtypes) {
                if (dtype.equals(wLinkDeviceBean.getType())) {
                    hasDevice = true;
                    jsonObject.put("devID", wLinkDeviceBean.getDevID());
                    jsonObject.put("gwID", wLinkDeviceBean.getGwID());
                    jsonObject.put("type", dtype);
                    controlWLink(jsonObject);
                }
            }
        }
        return hasDevice;
    }

    private static void controlWLink(JSONObject jsonObject) {
        ApiClient.getInstance().getApiService().wulianOperate(LoginPreference.getInstance().getUserToken(),
                ApiClient.getInstance().getRequestBody(jsonObject))
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .subscribe(s -> {
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
    }

    private static void controlResult(boolean hasDevice, String action, String device) {
        String content;
        if (hasDevice) content = "正在为你" + action;
        else content = "没有连接到" + device;
        RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
    }

    private static List<WLinkDeviceBean> parseDevices(String result) {
        JSONObject rawjson;
        List<WLinkDeviceBean> linkDeviceBeans = new ArrayList<>();
        try {
            rawjson = new JSONObject(result);
            JSONObject mobBaseRes = rawjson.optJSONObject("mobBaseRes");
            if (mobBaseRes == null) return null;
            JSONArray resultArray = mobBaseRes.optJSONArray("result");
            if (resultArray == null || resultArray.length() <= 0) return null;
            for (int i = 0; i < resultArray.length(); i++) {
                JSONObject item = resultArray.optJSONObject(i);
                String gwID = item.optString("gwID");
                String devID = item.optString("devID");
                String devName = item.optString("devName");
                String type = item.optString("type");
                String mode = item.optString("mode");
                linkDeviceBeans.add(new WLinkDeviceBean(gwID, devID, devName, type, mode));
            }
        } catch (JSONException e) {
            logUtil.e(TAG, e.toString());
            e.printStackTrace();
        }
        return linkDeviceBeans;
    }
}
