package com.viomi.fridge.vertical.iot.presenter;

import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.iot.contract.PLMachineContract;
import com.viomi.fridge.vertical.iot.model.http.entity.PLMachineProp;
import com.viomi.fridge.vertical.iot.model.repository.PLMachineRepository;

import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * 管线机 Presenter
 * Created by William on 2018/2/8.
 */
public class PLMachinePresenter implements PLMachineContract.Presenter {
    private static final String TAG = PLMachinePresenter.class.getSimpleName();
    private CompositeSubscription mCompositeSubscription;

    @Nullable
    private PLMachineContract.View mView;

    @Override
    public void subscribe(PLMachineContract.View view) {
        this.mView = view;
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public void unSubscribe() {
        mView = null;
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
    }

    @Override
    public void getProp(String did) {
        Subscription subscription = Observable.interval(0, 5, TimeUnit.SECONDS)
                .onBackpressureDrop() // 背压处理
                .subscribeOn(Schedulers.io())
                .flatMap(aLong -> PLMachineRepository.getProp(did))
                .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                .map(rpcResult -> new PLMachineProp(rpcResult.getList()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(plMachineProp -> {
                    if (plMachineProp != null && mView != null) mView.refreshUi(plMachineProp);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void setTemp(String did, int temp) {
        Subscription subscription = PLMachineRepository.setTemp(did, temp)
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (mView != null) mView.setIsSetting();
                }, throwable -> {
                    if (mView != null) mView.setIsSetting();
                    logUtil.e(TAG, throwable.getMessage());
                });
        mCompositeSubscription.add(subscription);
    }
}