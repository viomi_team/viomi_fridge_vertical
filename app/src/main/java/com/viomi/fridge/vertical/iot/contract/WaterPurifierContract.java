package com.viomi.fridge.vertical.iot.contract;

import com.viomi.fridge.vertical.common.callback.BasePresenter;
import com.viomi.fridge.vertical.iot.model.http.entity.CSeriesPurifierProp;
import com.viomi.fridge.vertical.iot.model.http.entity.MiPurifierProp;
import com.viomi.fridge.vertical.iot.model.http.entity.SSeriesPurifierProp;
import com.viomi.fridge.vertical.iot.model.http.entity.VSeriesPurifierProp;
import com.viomi.fridge.vertical.iot.model.http.entity.WaterPurifierFilter;
import com.viomi.fridge.vertical.iot.model.http.entity.X3PurifierProp;
import com.viomi.fridge.vertical.iot.model.http.entity.X5PurifierProp;

import java.util.List;

/**
 * 净水器 Contract
 * Created by William on 2018/2/5.
 */
public interface WaterPurifierContract {
    interface View {
        void showMiUi();// 显示小米净水器系列 ui

        void showVSeriesUi();// 显示云米 V 系列 ui

        void showSSeriesUi();// 显示云米 S 系列 ui

        void showCSeriesUi();// 显示云米 C 系列 ui

        void showX3Ui();// 显示 X3 ui

        void showX5Ui();// 显示 X5 ui

        void refreshMiUi(MiPurifierProp prop);// 刷新小米净水器系列 ui

        void refreshVSeriesUi(VSeriesPurifierProp prop);// 刷新云米 V 系列 ui

        void refreshSSeriesUi(SSeriesPurifierProp prop);// 刷新云米 S 系列 ui

        void refreshCSeriesUi(CSeriesPurifierProp prop);// 刷新云米 C 系列 ui

        void refreshX3Ui(X3PurifierProp prop);// 刷新 X3 ui

        void refreshX5Ui(X5PurifierProp prop);// 刷新 X5 ui

        void setIsTempSetting();

        void setIsSmallSetting();

        void setIsMiddleSetting();

        void setIsBigSetting();
    }

    interface Presenter extends BasePresenter<View> {
        void switchModel(String model, String did);// 根据 Model 显示 Ui

        void MiGetProp();// 小米净水器系列 getProp 请求

        void VSeriesGetProp();// 云米 V 系列 getProp 请求

        void SSeriesGetProp();// 云米 S 系列 getProp 请求

        void CSeriesGetProp();// 云米 C 系列 getProp 请求

        void X3SeriesGetProp();// X3 getProp 请求

        void X5SeriesGetProp();// X5 getProp 请求

        void setWaterTemp(int temp);// 设定出水温度

        void setWaterFlow(int flow, int index);// 设定出水流量

        void initFourFilters(List<WaterPurifierFilter> list);// 初始化滤芯

        void initThreeFilters(List<WaterPurifierFilter> list);// 初始化滤芯
    }
}
