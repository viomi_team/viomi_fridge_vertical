package com.viomi.fridge.vertical.message.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseActivity;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.PreferenceHelper;
import com.viomi.fridge.vertical.message.view.dialog.MessageSettingDialog;
import com.viomi.fridge.vertical.message.view.fragment.ActivityMessageFragment;
import com.viomi.fridge.vertical.message.view.fragment.DeviceMessageFragment;
import com.viomi.fridge.vertical.message.view.fragment.MallMessageFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 消息中心 Activity
 * Created by nanquan on 2018/2/6.
 */
public class MessageCenterActivity extends BaseActivity {
    private Fragment mCurrentFragment;// 当前 Fragment
    private MessageSettingDialog mSettingDialog;// 设置 Dialog

    @BindView(R.id.title_bar_right_image)
    ImageView mSettingImageView;// 设置

    @BindView(R.id.tv_message_activity)
    TextView mActivityTextView;// 活动促销
    @BindView(R.id.tv_message_mall)
    TextView mMallTextView;// 商城消息
    @BindView(R.id.tv_message_device)
    TextView mDeviceTextView;// 设备消息

    @Inject
    ActivityMessageFragment mActivityMessageFragment;// 活动消息 Fragment

    @Inject
    MallMessageFragment mMallMessageFragment;// 商城消息 Fragment

    @Inject
    DeviceMessageFragment mDeviceMessageFragment;// 设备消息 Fragment

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        layoutId = R.layout.activity_message_center;
        mTitle = getResources().getString(R.string.message_title);
        super.onCreate(savedInstanceState);
        mSettingImageView.setImageResource(R.drawable.icon_title_setting);
        mSettingImageView.setVisibility(View.VISIBLE);
        initFragment();
        initDialog();
        // 通知关闭消息弹窗
        RxBus.getInstance().post(BusEvent.MSG_OPEN_MESSAGE_CENTER);
        // 清除未读消息数量
        PreferenceHelper preferenceHelper = new PreferenceHelper(this);
        preferenceHelper.putUnreadMessageNum(0);
        handleIntent(getIntent());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSettingDialog != null && mSettingDialog.isAdded()) {
            mSettingDialog.dismiss();
            mSettingDialog = null;
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void initFragment() {
        mActivityTextView.setTextColor(getResources().getColor(R.color.color_light_green));
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fl_message_content, mActivityMessageFragment).commit();
        mCurrentFragment = mActivityMessageFragment;
    }

    private void initDialog() {
        mSettingDialog = new MessageSettingDialog();
    }

    @OnClick(R.id.title_bar_right_image)
    public void onSettingClick() {
        if (mSettingDialog == null) mSettingDialog = new MessageSettingDialog();
        if (mSettingDialog.isAdded()) return;
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("dialog");
        if (fragment != null) {
            fragmentTransaction.remove(fragment);
        }
        fragmentTransaction.addToBackStack(null);
        mSettingDialog.show(fragmentTransaction, "dialog");
    }

    @OnClick({R.id.fl_message_activity, R.id.fl_message_mall, R.id.fl_message_device})
    public void onTabClick(View view) {
        TextView selectTextView = null;
        Fragment selectFragment = null;
        switch (view.getId()) {
            case R.id.fl_message_activity: // 活动促销
                selectTextView = mActivityTextView;
                selectFragment = mActivityMessageFragment;
                break;
            case R.id.fl_message_mall: // 商城消息
                selectTextView = mMallTextView;
                selectFragment = mMallMessageFragment;
                break;
            case R.id.fl_message_device: // 设备消息
                selectTextView = mDeviceTextView;
                selectFragment = mDeviceMessageFragment;
                break;
        }
        if (mCurrentFragment == selectFragment) return;
        mActivityTextView.setTextColor(getResources().getColor(R.color.color_99));
        mMallTextView.setTextColor(getResources().getColor(R.color.color_99));
        mDeviceTextView.setTextColor(getResources().getColor(R.color.color_99));
        if (selectTextView != null && selectFragment != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            selectTextView.setTextColor(getResources().getColor(R.color.color_light_green));
            if (selectFragment.isAdded()) {
                transaction.hide(mCurrentFragment).show(selectFragment).commit();
            } else {
                transaction.hide(mCurrentFragment).add(R.id.fl_message_content, selectFragment).commit();
            }
            mCurrentFragment = selectFragment;
        }
    }

    private void handleIntent(Intent intent) {
        String type;
        if (intent == null) return;
        if (intent.getExtras() == null) return;
        type = intent.getExtras().getString(AppConstants.MESSAGE_CENTER_TYPE, "");
        TextView selectTextView = null;
        Fragment selectFragment = null;
        switch (type) {
            case "活动促销":
                selectTextView = mActivityTextView;
                selectFragment = mActivityMessageFragment;
                break;
            case "商城消息":
                selectTextView = mMallTextView;
                selectFragment = mMallMessageFragment;
                break;
            case "设备消息":
                selectTextView = mDeviceTextView;
                selectFragment = mDeviceMessageFragment;
                break;
        }
        if (mCurrentFragment == selectFragment) return;
        mActivityTextView.setTextColor(getResources().getColor(R.color.color_99));
        mMallTextView.setTextColor(getResources().getColor(R.color.color_99));
        mDeviceTextView.setTextColor(getResources().getColor(R.color.color_99));
        if (selectTextView != null && selectFragment != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            selectTextView.setTextColor(getResources().getColor(R.color.color_light_green));
            if (selectFragment.isAdded()) {
                transaction.hide(mCurrentFragment).show(selectFragment).commit();
            } else {
                transaction.hide(mCurrentFragment).add(R.id.fl_message_content, selectFragment).commit();
            }
            mCurrentFragment = selectFragment;
        }
    }
}
