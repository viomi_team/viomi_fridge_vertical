package com.viomi.fridge.vertical.administration.model.entity;

import android.bluetooth.BluetoothDevice;

/**
 * Created by Ljh on 2017/10/24.
 */
public class MyBluetoothDevice {
    private boolean isConnected = false;
    private BluetoothDevice bluetoothDevice;

    public MyBluetoothDevice(BluetoothDevice device) {
        this.bluetoothDevice = device;
    }

    @Override
    public boolean equals(Object obj) {
        return !(obj instanceof MyBluetoothDevice) || bluetoothDevice.getAddress().equals(((MyBluetoothDevice) obj).getBluetoothDevice().getAddress());
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void setConnected(boolean connected) {
        isConnected = connected;
    }

    public BluetoothDevice getBluetoothDevice() {
        return bluetoothDevice;
    }

    public void setBluetoothDevice(BluetoothDevice bluetoothDevice) {
        this.bluetoothDevice = bluetoothDevice;
    }
}