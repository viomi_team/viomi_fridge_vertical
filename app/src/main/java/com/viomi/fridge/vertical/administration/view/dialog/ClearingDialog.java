package com.viomi.fridge.vertical.administration.view.dialog;

import android.content.Context;
import android.widget.ImageView;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseDialog;
import com.viomi.fridge.vertical.common.util.ImageLoadUtil;

/**
 * 清理缓存提示
 * Created by nanquan on 2018/1/15.
 */
public class ClearingDialog extends BaseDialog {
    private ImageView ivCleaning;

    public ClearingDialog(Context context) {
        super(context, R.style.Dialog);
        setContentView(R.layout.dialog_loading);
        setCancelable(true);
    }

    @Override
    public void initView() {
        ivCleaning = findViewById(R.id.iv_loading);
    }

    @Override
    public void show() {
        super.show();
        ImageLoadUtil
                .getInstance()
                .loadResource(R.drawable.cleaning, ivCleaning);
    }
}