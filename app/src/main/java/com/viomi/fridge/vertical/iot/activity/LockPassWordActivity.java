package com.viomi.fridge.vertical.iot.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseActivity;
import com.viomi.fridge.vertical.iot.contract.LockContract;
import com.viomi.fridge.vertical.iot.model.repository.LockReposity;
import com.wulian.sdk.android.ipc.rtcv2.IPCController;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by hailang on 2018/6/5 0005.
 */

public class LockPassWordActivity extends BaseActivity implements View.OnClickListener, LockContract.View {


    @BindView(R.id.tv_pass)
    TextView tvPass;
    @BindView(R.id.grid)
    GridView grid;
    @BindView(R.id.item_0)
    FrameLayout item0;
    @BindView(R.id.v_close)
    View vClose;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @Inject
    LockContract.Presenter presenter;
    String devID, GwID, type;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        layoutId = R.layout.activity_lock_password;
        super.onCreate(savedInstanceState);
        devID = getIntent().getStringExtra("devID");
        GwID = getIntent().getStringExtra("GwID");
        type = getIntent().getStringExtra("type");
        presenter.subscribe(this);
        vClose.setOnClickListener(this);
        item0.setOnClickListener(this);
        ivBack.setOnClickListener(this);
        grid.setAdapter(new PassKeyboardAdapter());
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tvPass.setText(tvPass.getText().toString() + String.valueOf(position + 1));
                checkInput();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.unSubscribe();
    }

    String pass;
    String lastPass;

    void checkInput() {
        pass = tvPass.getText().toString();
        if (pass.equals(lastPass))
            return;
        if (pass.length() >= 6) {
            presenter.unlock(devID, GwID, tvPass.getText().toString(), type);
        }
        lastPass = pass;
    }

    @Override
    public void makecall() {

    }

    @Override
    public void close() {

    }

    @Override
    public void ondoorUnlock() {
        Toast.makeText(getBaseContext(), getString(R.string.lock_door_unlock), Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onError() {

    }

    @Override
    public void onLockCaneraFound(String GwID, String type) {

    }

    @Override
    public void initRtc(String userName, String password, String endPoint) {

    }

    class PassKeyboardAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return 9;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(getBaseContext(), R.layout.item_lock_password, null);
            }
            ((TextView) convertView.findViewById(R.id.tv_number)).setText(position + 1 + "");
            return convertView;
        }
    }

    @Override
    public void onClick(View v) {
        if (v == vClose) {
            finish();
        } else if (v == item0) {
            tvPass.setText(tvPass.getText().toString() + 0);
            checkInput();
        } else if (v == ivBack) {
            String s = tvPass.getText().toString();
            if (s.length() > 0)
                tvPass.setText(s.substring(0, s.length() - 1));
        }
    }
}
