package com.viomi.fridge.vertical.album.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.album.contract.AlbumContract;
import com.viomi.fridge.vertical.album.model.entity.AlbumEntity;
import com.viomi.fridge.vertical.album.view.AlbumAdapter;
import com.viomi.fridge.vertical.album.view.AlbumTimeAdapter;
import com.viomi.fridge.vertical.common.base.BaseActivity;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.ToastUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.widget.popupwindow.CommonPopupWindow;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import rx.Subscription;

/**
 * 电子相册 Activity
 * Created by William on 2018/1/19.
 */
public class AlbumActivity extends BaseActivity implements AlbumContract.View, AdapterView.OnItemClickListener, CommonPopupWindow.ViewInterface {
    private static final String TAG = AlbumActivity.class.getSimpleName();
    private Subscription mSubscription;// 消息订阅
    private List<AlbumEntity> mList;// 相册集合
    private AlbumAdapter mAdapter;// 相册适配器
    private CommonPopupWindow mPopupWindow;// 时间选择 PopupWindow
    private boolean mIsChoseAlbum = false;// 是否选择相册

    @Inject
    AlbumContract.Presenter mPresenter;

    @BindView(R.id.album_list)
    RecyclerView mRecyclerView;// 相册列表

    @BindView(R.id.album_layout)
    RelativeLayout mRelativeLayout;// 背景

    @BindView(R.id.album_guide)
    ImageView mGuideImageView;// 指引图

    @BindView(R.id.album_all_chose)
    TextView mAllChoseTextView;// 全选
    @BindView(R.id.album_num)
    TextView mNumTextView;// 图片数量
    @BindView(R.id.album_open)
    TextView mOpenTextView;// 开
    @BindView(R.id.album_close)
    TextView mCloseTextView;// 关
    @BindView(R.id.album_time)
    TextView mTimeTextView;// 屏保时间

    @BindView(R.id.album_operation_layout)
    LinearLayout mOperationLinearLayout;// 相册相关操作布局

    @BindView(R.id.album_edit_layout)
    FrameLayout mEditFrameLayout;// 相册编辑按钮布局

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        layoutId = R.layout.activity_album;
        mTitle = getResources().getString(R.string.album_title);
        super.onCreate(savedInstanceState);
        mPresenter.subscribe(this);// 订阅
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 4);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mList = new ArrayList<>();
        mAdapter = new AlbumAdapter(mList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(this);

        mSubscription = RxBus.getInstance().subscribe(busEvent -> {
            switch (busEvent.getMsgId()) {
                case BusEvent.MSG_ALBUM_UPDATE: // 接收到新相册
                    if (mPresenter != null) mPresenter.loadAlbum(mList);
                    break;
                case BusEvent.MSG_ALBUM_DELETE: // 语音删除图片
                    runOnUiThread(() -> {
                        for (AlbumEntity entity : mList) entity.setSelected(false);
                        int position = (int) busEvent.getMsgObject();
                        if (position > mList.size() || position - 1 < 0) return;
                        AlbumEntity entity = mList.get(position - 1);
                        entity.setSelected(true);
                        delete();
                        RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, "已为您删除第" + position + "张图片");
                    });
                    break;
            }
        });
        if (mPresenter != null) mPresenter.loadAlbum(mList);// 读取相册

        mIsChoseAlbum = getIntent().getBooleanExtra(AppConstants.CHOSE_ALBUM, false);
        if (mIsChoseAlbum) { // 仅选择相册
            mEditFrameLayout.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RxBus.getInstance().post(BusEvent.MSG_SCREEN_SAVER_UPDATE);// 保存屏保
        if (mPresenter != null) {
            mPresenter.unSubscribe();// 取消订阅
            mPresenter = null;
        }
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
        if (mList != null) {
            mList.clear();
            mList = null;
        }
        if (mAdapter != null) mAdapter = null;
        if (mPopupWindow != null && mPopupWindow.isShowing()) {
            mPopupWindow.dismiss();
            mPopupWindow = null;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        if (mAdapter.isEditMode()) { // 编辑模式
            AlbumEntity entity = mList.get(position);
            entity.setSelected(!entity.isSelected());
            if (entity.isSelected()) mAdapter.setSelectCount(mAdapter.getSelectCount() + 1);
            else mAdapter.setSelectCount(mAdapter.getSelectCount() - 1);
            mAdapter.notifyItemChanged(position);
            if (mAdapter.getSelectCount() == mList.size()) mAllChoseTextView.setSelected(true);
            else mAllChoseTextView.setSelected(false);
        } else if (mIsChoseAlbum) { // 选择相册
            Intent intent = new Intent();
            intent.putExtra(AppConstants.CHOSE_ALBUM_RESULT, mList.get(position).getFile().getAbsolutePath());
            setResult(AppConstants.CODE_CHOSE_ALBUM_SUCCESS, intent);
            finish();
        } else { // 预览模式
            ArrayList<String> list = new ArrayList<>();
            for (AlbumEntity entity : mList) list.add(entity.getFile().getAbsolutePath());
            Intent intent = new Intent(AlbumActivity.this, AlbumPreviewActivity.class);
            intent.putStringArrayListExtra(AppConstants.PHOTO_LIST, list);
            intent.putExtra(AppConstants.PHOTO_INDEX, position);
            startActivity(intent);
        }
    }

    @Override
    public void initWithCache(boolean enable, String time) { // 根据缓存显示 UI
        // 屏保开关
        if (enable) mOpenTextView.setSelected(true);
        else mCloseTextView.setSelected(true);
        mTimeTextView.setText(time);// 屏保时间
    }

    @Override
    public void notifyDataChanged() {
        if (mAdapter != null) mAdapter.notifyDataSetChanged();
    }

    @Override
    public void notifyDataRemoved(int size) {
        if (mAdapter != null) mAdapter.notifyItemRangeRemoved(0, size);
    }

    @Override
    public void notifyDataInserted(int size) {
        if (mAdapter != null) mAdapter.notifyItemRangeInserted(0, size);
    }

    @Override
    public void updateAlbum() { // 更新相册
        mAllChoseTextView.setSelected(false);
        if (mList == null || mList.size() == 0) hideAlbum();
        else {
            mNumTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.album_num), mList.size()));
            showAlbum();
        }
    }

    @Override
    public void showAlbum() { // 有相册
        if (mRecyclerView.getVisibility() != View.VISIBLE)
            mRecyclerView.setVisibility(View.VISIBLE);
        if (mOperationLinearLayout.getVisibility() == View.GONE && mEditFrameLayout.getVisibility() == View.GONE && !mIsChoseAlbum) {
            mEditFrameLayout.setVisibility(View.VISIBLE);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mRecyclerView.getLayoutParams();
            layoutParams.addRule(RelativeLayout.ABOVE, R.id.album_edit_layout);
            mRecyclerView.setLayoutParams(layoutParams);
        }
        if (mGuideImageView.getVisibility() == View.VISIBLE)
            mGuideImageView.setVisibility(View.GONE);
        mRelativeLayout.setBackgroundResource(R.color.color_theme);
    }

    @Override
    public void hideAlbum() { // 无相册
        if (mRecyclerView.getVisibility() == View.VISIBLE) mRecyclerView.setVisibility(View.GONE);
        if (mEditFrameLayout.getVisibility() == View.VISIBLE)
            mEditFrameLayout.setVisibility(View.GONE);
        if (mOperationLinearLayout.getVisibility() == View.VISIBLE)
            mOperationLinearLayout.setVisibility(View.GONE);
        if (mGuideImageView.getVisibility() != View.VISIBLE)
            mGuideImageView.setVisibility(View.VISIBLE);
        mRelativeLayout.setBackgroundResource(R.color.color_f2);
    }

    @Override
    public void getChildView(View view, int layoutResId) {
        RecyclerView recyclerView = view.findViewById(R.id.album_time_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        List<String> list = new ArrayList<>();
        list.add(getResources().getString(R.string.album_15_second));
        list.add(getResources().getString(R.string.album_30_second));
        list.add(getResources().getString(R.string.album_1_minute));
        list.add(getResources().getString(R.string.album_2_minute));
        list.add(getResources().getString(R.string.album_5_minute));
        list.add(getResources().getString(R.string.album_10_minute));
        list.add(getResources().getString(R.string.album_30_minute));
        AlbumTimeAdapter adapter = new AlbumTimeAdapter(list);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener((parent, view1, position, id) -> {
            mTimeTextView.setText(list.get(position));
            mPresenter.cacheTime(mTimeTextView.getText().toString());// 缓存屏保时间
            mPopupWindow.dismiss();
        });
    }

    @OnClick(R.id.album_all_chose)
    public void allChose() { // 全选
        mAllChoseTextView.setSelected(!mAllChoseTextView.isSelected());
        boolean isSelect = mAllChoseTextView.isSelected();
        if (!isSelect) mAdapter.setSelectCount(0);
        for (int i = 0; i < mList.size(); i++) mList.get(i).setSelected(isSelect);
        mAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.album_delete)
    public void delete() { // 删除相册
        if (mPresenter.isChosen(mList)) {
            if (mPresenter != null) mPresenter.delete(mList);
            if (mEditFrameLayout.getVisibility() == View.GONE)
                mEditFrameLayout.setVisibility(View.VISIBLE);
            if (mOperationLinearLayout.getVisibility() == View.VISIBLE)
                mOperationLinearLayout.setVisibility(View.GONE);
            mAdapter.setEditMode(false);
            mAdapter.setSelectCount(0);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mRecyclerView.getLayoutParams();
            layoutParams.addRule(RelativeLayout.ABOVE, R.id.album_edit_layout);
            mRecyclerView.setLayoutParams(layoutParams);
        } else {
            ToastUtil.showDefinedCenter(FridgeApplication.getContext(), getResources().getString(R.string.album_delete_tip));
        }
    }

    @OnClick(R.id.album_edit)
    public void edit() { // 编辑
        mEditFrameLayout.setVisibility(View.GONE);
        mOperationLinearLayout.setVisibility(View.VISIBLE);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mRecyclerView.getLayoutParams();
        layoutParams.addRule(RelativeLayout.ABOVE, R.id.album_operation_layout);
        mRecyclerView.setLayoutParams(layoutParams);
        mAdapter.setEditMode(true);
        mAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.album_time)
    public void choseTime() { // 选择屏保时间
        if (mPopupWindow != null && mPopupWindow.isShowing()) return;
        mPopupWindow = new CommonPopupWindow.Builder(this)
                .setView(R.layout.popup_window_album_time)
                .setOutsideTouchable(true)
                .setWidthAndHeight(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                .setViewOnclickListener(this)
                .create();
        mPopupWindow.showAsDropDown(mTimeTextView, Math.abs(mTimeTextView.getMeasuredWidth() / 2 - mPopupWindow.getWidth() / 2),
                -(mPopupWindow.getHeight() + mTimeTextView.getMeasuredHeight()));
    }

    @OnClick(R.id.album_set)
    public void setScreenSaver() { // 设置屏保
        if (mPresenter.isChosen(mList) && mPresenter != null) {
            mPresenter.cacheScreenSaver(mList);// 缓存选择相册
            logUtil.d(TAG, "set screen saver success");
            finish();
        } else {
            ToastUtil.showDefinedCenter(FridgeApplication.getContext(), getResources().getString(R.string.album_set_tip));
        }
    }

    @OnClick({R.id.album_open, R.id.album_close})
    public void setSwitch(TextView textView) { // 屏保开关
        switch (textView.getId()) {
            case R.id.album_open: // 开
                if (!mOpenTextView.isSelected()) {
                    mOpenTextView.setSelected(true);
                    mPresenter.cacheSwitch(true);// 缓存屏保开关
                }
                if (mCloseTextView.isSelected()) mCloseTextView.setSelected(false);
                break;
            case R.id.album_close: // 关
                if (mOpenTextView.isSelected()) mOpenTextView.setSelected(false);
                if (!mCloseTextView.isSelected()) {
                    mCloseTextView.setSelected(true);
                    mPresenter.cacheSwitch(false);// 缓存屏保开关
                }
                break;
        }
    }

    @OnClick(R.id.album_back_arrow)
    public void cancelEdit() { // 取消编辑
        mAdapter.setEditMode(false);
        mAdapter.setSelectCount(0);
        mAllChoseTextView.setSelected(false);
        for (AlbumEntity entity : mList) entity.setSelected(false);
        mAdapter.notifyDataSetChanged();
        mEditFrameLayout.setVisibility(View.VISIBLE);
        mOperationLinearLayout.setVisibility(View.GONE);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mRecyclerView.getLayoutParams();
        layoutParams.addRule(RelativeLayout.ABOVE, R.id.album_edit_layout);
        mRecyclerView.setLayoutParams(layoutParams);
    }
}
