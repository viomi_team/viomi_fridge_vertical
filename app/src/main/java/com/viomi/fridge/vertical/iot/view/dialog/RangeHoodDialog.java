package com.viomi.fridge.vertical.iot.view.dialog;

import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.CommonDialog;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.iot.contract.RangeHoodContract;
import com.viomi.fridge.vertical.iot.model.http.entity.RangeHoodData;
import com.viomi.fridge.vertical.iot.model.http.entity.RangeHoodProp;
import com.viomi.fridge.vertical.iot.presenter.RangeHoodPresenter;
import com.viomi.widget.BubbleView;
import com.viomi.widget.CircleProgressView;

import java.util.concurrent.TimeUnit;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;
import rx.Observable;
import rx.Subscription;

/**
 * 烟机 Dialog
 * Created by William on 2018/2/21.
 */
public class RangeHoodDialog extends CommonDialog implements RangeHoodContract.View {
    private static final String TAG = RangeHoodDialog.class.getSimpleName();
    public final String PARAM_NAME = "name";// 设备名称
    public final String PARAM_DID = "did";// 设备 id
    private String mDid;
    private ClipDrawable mClipDrawable;
    private RotateAnimation mRotateAnimation;// 旋转动画
    private boolean mIsSetting = false, mIsDelay;// 是否正在设置，是否延时关机
    private int mDelayTime;// 延时关机时间
    private Subscription mSubscription;
    private RangeHoodContract.Presenter mPresenter;

    @BindString(R.string.iot_range_hood_stove_no_running)
    String mStoveNoRunning;
    @BindString(R.string.iot_range_hood_stove_no_link)
    String mStoveNoLink;
    @BindString(R.string.iot_range_hood_stove_closed)
    String mStoveClosed;
    @BindString(R.string.iot_range_hood_stove_fire_low)
    String mStoveLow;
    @BindString(R.string.iot_range_hood_stove_fire_heavy)
    String mStoveHeavy;

    @BindView(R.id.range_hood_pm_layout)
    LinearLayout mPMLinearLayout;// 净化量和空气质量布局

    @BindView(R.id.range_hood_delay_progress)
    CircleProgressView mCircleProgressView;// 延时关机进度

    @BindView(R.id.range_hood_bubble)
    BubbleView mBubbleView;// 气泡 View

    @BindView(R.id.range_hood_pm_line)
    View mLineView;// 分割线

    @BindView(R.id.range_hood_name)
    TextView mNameTextView;// 设备名称
    @BindView(R.id.range_hood_stove_status)
    TextView mStoveStatusTextView;// 烟灶状态
    @BindView(R.id.range_hood_stove_per)
    TextView mBatteryPerTextView;// 电量百分比
    @BindView(R.id.range_hood_stove1_using)
    TextView mStove1UsingTextView;// 燃气灶 1 使用中文字
    @BindView(R.id.range_hood_stove2_using)
    TextView mStove2UsingTextView;// 燃气灶 2 使用中文字
    @BindView(R.id.range_hood_stove1_status)
    TextView mStove1StatusTextView;// 燃气灶 1 状态
    @BindView(R.id.range_hood_stove2_status)
    TextView mStove2StatusTextView;// 燃气灶 2 状态
    @BindView(R.id.range_hood_status)
    TextView mHoodStatusTextView;// 烟机状态
    @BindView(R.id.range_hood_power)
    TextView mPowerTextView;// 电源开关
    @BindView(R.id.range_hood_low)
    TextView mLowTextView;// 低档
    @BindView(R.id.range_hood_high)
    TextView mHighTextView;// 高档
    @BindView(R.id.range_hood_fry)
    TextView mFryTextView;// 爆炒
    @BindView(R.id.range_hood_light_switch)
    TextView mLightTextView;// 灯光
    @BindView(R.id.range_hood_air_quality)
    TextView mAriQualityTextView;// 空气质量
    @BindView(R.id.range_hood_pm_setting)
    TextView mPMSettingTextView;// PM2.5 设置
    @BindView(R.id.range_hood_running_count)
    TextView mRunningCountTextView;// 运行次数
    @BindView(R.id.range_hood_running_time)
    TextView mRunningTimeTextView;// 运行时间
    @BindView(R.id.range_hood_purify_value)
    TextView mPurifyTextView;// 净化量

    @BindView(R.id.range_hood_stove1_icon)
    ImageView mStove1ImageView;// 燃气灶 1 状态图
    @BindView(R.id.range_hood_stove2_icon)
    ImageView mStove2ImageView;// 燃气灶 2 状态图
    @BindView(R.id.range_hood_battery)
    ImageView mBatteryImageView;// 电量图标
    @BindView(R.id.range_hood_fan_out)
    ImageView mFanOutImageView;// 风扇外层
    @BindView(R.id.range_hood_fan)
    ImageView mFanImageView;// 风扇
    @BindView(R.id.range_hood_off)
    ImageView mClosedImageView;// 烟机关闭图标
    @BindView(R.id.range_hood_light)
    ImageView mLightImageView;// 灯光

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.dialog_range_hood;
    }

    @Override
    protected void initWithOnCreateDialog(View view) {
        mPresenter = new RangeHoodPresenter();
        mDid = getArguments() != null ? getArguments().getString(PARAM_DID) : "";
        mNameTextView.setText(getArguments() != null ? getArguments().getString(PARAM_NAME) : "");

        LayerDrawable layerDrawable = (LayerDrawable) mBatteryImageView.getDrawable();
        mClipDrawable = (ClipDrawable) layerDrawable.findDrawableByLayerId(R.id.battery_full);

        mCircleProgressView.setRoundColor(ContextCompat.getColor(FridgeApplication.getContext(), R.color.range_hood_fade_green));
        mCircleProgressView.setStrokeWidth(4);
        mCircleProgressView.setText("");
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe(this);
        mPresenter.getProp(mDid);
        mPresenter.getUserData(mDid);
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.unSubscribe();
        mBubbleView.setStop(true);
        if (mSubscription != null) mSubscription.unsubscribe();
        mFanImageView.clearAnimation();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mPresenter != null) mPresenter = null;
    }

    @Override
    public void refreshUi(RangeHoodProp prop) {
        // 烟灶联动与火力
        switch (prop.getLink_state()) {
            case "0":
            case "1":
                mStoveStatusTextView.setText(mStoveNoRunning);
                mBatteryImageView.setVisibility(View.GONE);
                mBatteryPerTextView.setVisibility(View.GONE);
                mStove1UsingTextView.setVisibility(View.GONE);
                mStove2UsingTextView.setVisibility(View.GONE);
                mStove1StatusTextView.setText(mStoveNoLink);
                mStove2StatusTextView.setText(mStoveNoLink);
                mStove1ImageView.setImageResource(R.drawable.icon_range_hood_non_link);
                mStove2ImageView.setImageResource(R.drawable.icon_range_hood_non_link);
                break;
            default:
                mStoveStatusTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.iot_range_hood_stove_running));
                mBatteryImageView.setVisibility(View.VISIBLE);
                mBatteryPerTextView.setVisibility(View.VISIBLE);
                switch (prop.getStove1_data()) { // 燃气灶 1
                    case "0":
                        mStove1StatusTextView.setText(mStoveClosed);
                        mStove1UsingTextView.setVisibility(View.GONE);
                        mStove1ImageView.setImageResource(R.drawable.icon_range_hood_off);
                        break;
                    case "1":
                        mStove1StatusTextView.setText(mStoveLow);
                        mStove1UsingTextView.setVisibility(View.VISIBLE);
                        mStove1ImageView.setImageResource(R.drawable.icon_range_hood_low_fire);
                        break;
                    case "2":
                        mStove1StatusTextView.setText(mStoveHeavy);
                        mStove1UsingTextView.setVisibility(View.VISIBLE);
                        mStove1ImageView.setImageResource(R.drawable.icon_range_hood_high_fire);
                        break;
                }
                switch (prop.getStove2_data()) { // 燃气灶 2
                    case "0":
                        mStove2StatusTextView.setText(mStoveClosed);
                        mStove2UsingTextView.setVisibility(View.GONE);
                        mStove2ImageView.setImageResource(R.drawable.icon_range_hood_off);
                        break;
                    case "1":
                        mStove2StatusTextView.setText(mStoveLow);
                        mStove2UsingTextView.setVisibility(View.VISIBLE);
                        mStove2ImageView.setImageResource(R.drawable.icon_range_hood_low_fire);
                        break;
                    case "2":
                        mStove2StatusTextView.setText(mStoveHeavy);
                        mStove2UsingTextView.setVisibility(View.VISIBLE);
                        mStove2ImageView.setImageResource(R.drawable.icon_range_hood_high_fire);
                        break;
                }
                break;
        }
        // 电量
        String battery = prop.getBattary_life() + "%";
        mBatteryPerTextView.setText(battery);
        mClipDrawable.setLevel(calculateLevel(Integer.valueOf(prop.getBattary_life())));
        if (mIsSetting) return;
        // 电源状态
        if (prop.getPower_state().equals("0")) {
            mHoodStatusTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.iot_range_hood_closed));
            mFanOutImageView.setVisibility(View.VISIBLE);
            mFanImageView.clearAnimation();
            mBubbleView.setStop(true);
            mClosedImageView.setVisibility(View.VISIBLE);
            mFanImageView.setVisibility(View.GONE);
            mCircleProgressView.setVisibility(View.GONE);
            mCircleProgressView.cancelAnimate();
            if (mSubscription != null) mSubscription.unsubscribe();
            mIsDelay = false;
            mPowerTextView.setSelected(false);
            mLowTextView.setSelected(false);
            mHighTextView.setSelected(false);
            mFryTextView.setSelected(false);
            mLowTextView.setEnabled(false);
            mHighTextView.setEnabled(false);
            mHighTextView.setEnabled(false);
        } else {
            mClosedImageView.setVisibility(View.GONE);
            if (prop.getPower_state().equals("1")) { // 待机
                mFanImageView.setVisibility(View.GONE);
                mFanOutImageView.setVisibility(View.GONE);
                mFanImageView.clearAnimation();
                if (mCircleProgressView.getVisibility() == View.GONE) {
                    mCircleProgressView.setVisibility(View.VISIBLE);
                    if (prop.getPoweroff_delaytime().equals("error")) mDelayTime = 0;
                    else mDelayTime = Integer.valueOf(prop.getPoweroff_delaytime());
                    if (mDelayTime == 0) mDelayTime = 120;
                    mCircleProgressView.setPeriod(mDelayTime * 10);
                    mCircleProgressView.runAnimate(100);
                    mHoodStatusTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.iot_range_hood_closing_delay));
                    mIsDelay = true;
                    mSubscription = Observable.interval(0, 500, TimeUnit.MILLISECONDS)
                            .onBackpressureDrop() // 背压处理
                            .compose(RxSchedulerUtil.SchedulersTransformer1())
                            .onTerminateDetach()
                            .subscribe(aLong -> mPowerTextView.setSelected(!mPowerTextView.isSelected()), throwable -> logUtil.e(TAG, throwable.getMessage()));
                }
            } else {
                mIsDelay = false;
                if (mSubscription != null) mSubscription.unsubscribe();
                mFanOutImageView.setVisibility(View.VISIBLE);
                mCircleProgressView.setVisibility(View.GONE);
                mFanImageView.setVisibility(View.VISIBLE);
                mCircleProgressView.cancelAnimate();
                mPowerTextView.setSelected(true);
            }
            mLowTextView.setEnabled(true);
            mHighTextView.setEnabled(true);
            mFryTextView.setEnabled(true);
            // 风挡
            switch (prop.getWind_state()) {
                case "0": // 关闭
                    if (mCircleProgressView.getVisibility() == View.GONE)
                        mHoodStatusTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.iot_range_hood_free));
                    mFanImageView.clearAnimation();
                    mBubbleView.setStop(true);
                    mLowTextView.setSelected(false);
                    mHighTextView.setSelected(false);
                    mFryTextView.setSelected(false);
                    break;
                case "1": // 低档
                    if (mCircleProgressView.getVisibility() == View.GONE)
                        mHoodStatusTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.iot_range_hood_low_desc));
                    startAnim(3000, 30);
                    mLowTextView.setSelected(true);
                    mHighTextView.setSelected(false);
                    mFryTextView.setSelected(false);
                    break;
                case "4": // 爆炒
                    if (mCircleProgressView.getVisibility() == View.GONE)
                        mHoodStatusTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.iot_range_hood_fry_desc));
                    startAnim(1000, 10);
                    mLowTextView.setSelected(false);
                    mHighTextView.setSelected(false);
                    mFryTextView.setSelected(true);
                    break;
                case "16": // 高档
                    if (mCircleProgressView.getVisibility() == View.GONE)
                        mHoodStatusTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.iot_range_hood_high_desc));
                    startAnim(2000, 20);
                    mLowTextView.setSelected(false);
                    mHighTextView.setSelected(true);
                    mFryTextView.setSelected(false);
                    break;
            }
        }
        // 灯光
        switch (prop.getLight_state()) {
            case "0":
                mLightTextView.setSelected(false);
                mLightImageView.setVisibility(View.GONE);
                break;
            case "1":
                mLightTextView.setSelected(true);
                mLightImageView.setVisibility(View.VISIBLE);
                break;
        }
        // PM 2.5
//        if (prop.getPm2_5() != 0) {
//            mLineView.setVisibility(View.VISIBLE);
//            mPMLinearLayout.setVisibility(View.VISIBLE);
//            mAriQualityTextView.setText(setAirQuality(prop.getPm2_5()));
//        } else {
//            mLineView.setVisibility(View.GONE);
//            mPMLinearLayout.setVisibility(View.GONE);
//            mPMSettingTextView.setVisibility(View.GONE);
//        }
    }

    @Override
    public void refreshUi(RangeHoodData data) {
        mRunningCountTextView.setText(String.valueOf(data.getmCount()));
        mRunningTimeTextView.setText(Html.fromHtml(ToolUtil.timeFormat(data.getmTime())));
        mPurifyTextView.setText(String.valueOf(data.getmPurify()));
    }

    @Override
    public void setIsSetting() {
        mIsSetting = false;
    }

    @OnClick(R.id.range_hood_power)
    public void setPower() {
        mIsSetting = true;
        if (mPowerTextView.isSelected() && !mIsDelay && mDelayTime != 0)
            mPresenter.setPower("1", mDid);
        else if (mIsDelay || mPowerTextView.isSelected()) mPresenter.setPower("0", mDid);
        else mPresenter.setPower("2", mDid);
        if (mIsDelay) {
            if (mSubscription != null) mSubscription.unsubscribe();
            mPowerTextView.setSelected(false);
        } else {
            mPowerTextView.setSelected(!mPowerTextView.isSelected());
        }
    }

    @OnClick({R.id.range_hood_low, R.id.range_hood_high, R.id.range_hood_fry})
    public void setWind(TextView textView) {
        mIsSetting = true;
        switch (textView.getId()) {
            case R.id.range_hood_low:
                if (mPowerTextView.isSelected()) {
                    if (mLowTextView.isSelected()) mPresenter.setWind("0", mDid);
                    else mPresenter.setWind("1", mDid);
                    mLowTextView.setSelected(!mLowTextView.isSelected());
                    mHighTextView.setSelected(false);
                    mFryTextView.setSelected(false);
                }
                break;
            case R.id.range_hood_high:
                if (mPowerTextView.isSelected()) {
                    if (mHighTextView.isSelected()) mPresenter.setWind("0", mDid);
                    else mPresenter.setWind("16", mDid);
                    mLowTextView.setSelected(false);
                    mHighTextView.setSelected(!mHighTextView.isSelected());
                    mFryTextView.setSelected(false);
                }
                break;
            case R.id.range_hood_fry:
                if (mPowerTextView.isSelected()) {
                    if (mFryTextView.isSelected()) mPresenter.setWind("0", mDid);
                    else mPresenter.setWind("4", mDid);
                    mLowTextView.setSelected(false);
                    mHighTextView.setSelected(false);
                    mFryTextView.setSelected(!mFryTextView.isSelected());
                }
                break;
        }
    }

    @OnClick(R.id.range_hood_light_switch)
    public void setLight() {
        mIsSetting = true;
        if (mLightTextView.isSelected()) mPresenter.setLight("0", mDid);
        else mPresenter.setLight("1", mDid);
        mLightTextView.setSelected(!mLightTextView.isSelected());
    }

    private int calculateLevel(int progress) {
        int leftOffset = ToolUtil.dpToPx(getActivity(), 2);
        int powerLength = ToolUtil.dpToPx(getActivity(), 26.5f);// 40 px in hdpi
        int totalLength = ToolUtil.dpToPx(getActivity(), 32.5f);// 49 px in hdpi
        return (leftOffset + powerLength * progress / 100) * 10000 / totalLength;
    }

    private void startAnim(int d, int time) {
        if (mCircleProgressView.getVisibility() == View.GONE) {
            if (mFanImageView.getAnimation() == null) {
                mRotateAnimation = new RotateAnimation(0, -359, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
                mRotateAnimation.setRepeatCount(RotateAnimation.INFINITE);// 循环
                mRotateAnimation.setInterpolator(new LinearInterpolator());// 匀速
                mFanImageView.startAnimation(mRotateAnimation);
            }
            mRotateAnimation.setDuration(d); // 持续时间
        }
        mBubbleView.setStop(false);
        mBubbleView.setTime(time);
    }

    private String setAirQuality(int pm2_5) {
        String str = FridgeApplication.getContext().getResources().getString(R.string.iot_value_default);
        switch (pm2_5) {
            case 1:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_range_hood_air_good);
                break;
            case 2:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_range_hood_air_better);
                break;
            case 3:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_range_hood_air_poor_1);
                break;
            case 4:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_range_hood_air_poor_2);
                break;
            case 5:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_range_hood_air_poor_3);
                break;
            case 6:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_range_hood_air_poor_4);
                break;
        }
        return str;
    }
}