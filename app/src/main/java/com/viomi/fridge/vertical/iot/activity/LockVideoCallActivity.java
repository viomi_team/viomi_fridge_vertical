package com.viomi.fridge.vertical.iot.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseHandlerActivity;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.iot.contract.LockContract;
import com.viomi.fridge.vertical.iot.view.dialog.VideoCallLoadingDialog;
import com.wulian.sdk.android.ipc.rtcv2.IPCController;
import com.wulian.sdk.android.ipc.rtcv2.IPCResultCallBack;
import com.wulian.sdk.android.ipc.rtcv2.message.IPCCallStateMsgEvent;
import com.wulian.sdk.android.ipc.rtcv2.message.IPCOnReceivedMsgEvent;
import com.wulian.sdk.android.ipc.rtcv2.message.IPCVideoFrameMsgEvent;
import com.wulian.sdk.android.ipc.rtcv2.message.IPCcameraXmlMsgEvent;
import com.wulian.sdk.android.ipc.rtcv2.message.messagestate.MsgCallState;
import com.wulian.sdk.android.ipc.rtcv2.message.messagestate.MsgDPIType;
import com.wulian.sdk.android.ipc.rtcv2.message.messagestate.MsgReceivedType;
import com.wulian.webrtc.ViEAndroidGLES20;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by hailang on 2018/5/15 0015.
 */

public class LockVideoCallActivity extends BaseHandlerActivity implements View.OnClickListener, LockContract.View {
    private static final String TAG=LockVideoCallActivity.class.getSimpleName()+"LOG";
    @BindView(R.id.view_video)
    ViEAndroidGLES20 viewVideo;
    @Inject
    LockContract.Presenter presenter;
    @BindView(R.id.iv_unlock)
    ImageView ivUnlock;
    @BindView(R.id.tv_unlock)
    TextView tvUnlock;
    @BindView(R.id.layout_bottom)
    LinearLayout layoutBottom;
    @BindView(R.id.title_bar_right)
    TextView titleBarRight;
    @BindView(R.id.title_bar_right_progress)
    ProgressBar titleBarRightProgress;
    @BindView(R.id.title_bar_right_image)
    ImageView titleBarRightImage;
    @BindView(R.id.title_bar_back)
    ImageView titleBarBack;
    @BindView(R.id.layout_title)
    View layoutTitle;
    String sdomain;
    String deviceIdCar;
    String devID;
    String GwID, type;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        layoutId = R.layout.activity_lockvideocall;
        super.onCreate(savedInstanceState);
        loading();
        presenter.subscribe(this);
        ivUnlock.setOnClickListener(this);
        tvUnlock.setOnClickListener(this);
        titleBarBack.setOnClickListener(this);
        ivUnlock.setClickable(false);
        ivUnlock.setAlpha(0.6f);
        String info = getIntent().getStringExtra("info");
        layoutTitle.setBackgroundColor(Color.TRANSPARENT);
        Log.v(TAG, "LockVideoCallActivity  did:" + ToolUtil.getMiIdentification().getDeviceId());
        devID = "9BE3600F004B1200";
        deviceIdCar = "cmic10a850294d417c56";
        try {
            //{
//            "parameter": {
//                "camId": "cmic10a850294d417c56"
//            },
//            "event": "bell",
//            "did": "9BE3600F004B1200"
//        }
            JSONObject jsonObject = new JSONObject(info);
            devID = jsonObject.optString("did");
            deviceIdCar = jsonObject.optJSONObject("parameter").optString("camId");
            Log.i(TAG, "camId:" + deviceIdCar);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        presenter.getWlinkDevices(deviceIdCar);
//        mHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                Toast.makeText(LockVideoCallActivity.this,"门锁摄像头进入休眠",Toast.LENGTH_SHORT).show();
//                onBackPressed();
//            }
//        }, 120 * 1000);
    }

    @Override
    public void initRtc(String userName, String password, String endPoint) {
        sdomain = endPoint;
        IPCController.initRTCAsync(new IPCResultCallBack() {
            @Override
            public void getResult(int result) {
                Log.d(TAG, "initRTC result is:" + result);
                IPCResultCallBack registerAccountAsyncCallback = new IPCResultCallBack() {
                    @Override
                    public void getResult(int result) {
                        Log.d(TAG, "registerAccountAsyncCallback result is:"
                                + (result == 0 ? "TRUE" : "FALSE"));
                        IPCController.closeAllVideoAsync(null);
                        presenter.getWlinkDeviceChildren(devID);
                    }
                };
//                IPCController.setLog(true);
                //"us433160g4bbb74d2aa2", "64c942927ab673bf", "shsp.wuliangroup.cn"
                IPCController.registerAccountAsync(registerAccountAsyncCallback
                        , userName
                        , password
                        , endPoint);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        IPCController.setRender("", null);
        IPCController.closeAllVideoAsync(null);
        presenter.unSubscribe();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(IPCcameraXmlMsgEvent event) {
        Log.d(TAG, "End time is:" + System.currentTimeMillis() + " event.getApiType:" + event.getApiType().name());
//        Toast.makeText(this,
//                event.getApiType().name() + "\n" + event.getMessage(),
//                Toast.LENGTH_SHORT).show();
    }

    VideoCallLoadingDialog dialog;
    Animation rotateAnimation;

    void loading() {
        dialog = new VideoCallLoadingDialog(this);
        rotateAnimation = AnimationUtils.loadAnimation(this, R.anim.video_call_loading);
        dialog.ivVideocallLoading.setAnimation(rotateAnimation);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                rotateAnimation.cancel();
            }
        });
        dialog.layoutContent.setOnClickListener(null);
        dialog.show();
    }

    void showError() {
        dialog = new VideoCallLoadingDialog(this);
        dialog.tvVideocallDialog.setText(getString(R.string.lock_door_error));
        dialog.ivVideocallLoading.setImageResource(R.drawable.icon_shibai);
        dialog.layoutContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                loading();
                closeVideo();
//                presenter.turnOffCamera();
                presenter.getWlinkDevices(deviceIdCar);
            }
        });
        dialog.show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(IPCVideoFrameMsgEvent event) {
        Log.d(TAG, "End time is:" + System.currentTimeMillis() + " event:" + event.getType());
        switch (event.getType()) {
            case FRAME_MAIN_THUNBNAIL:
                if (event.getmVideoBitmap() == null) {
                    Log.d(TAG, "抓拍图片为空");
                } else {
                    Log.d(TAG, "抓拍图片不为空");
                }
                break;

            default:
                break;
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(IPCCallStateMsgEvent event) {
        MsgCallState callState = MsgCallState
                .getMsgCallState(event.getCallState());
        Log.d(TAG, "End time is:" + System.currentTimeMillis() + " callState:" + callState);
        switch (callState) {
            case STATE_ESTABLISHED:
                Log.d(TAG, "STATE_ESTABLISHED P2P established");
                break;
            case STATE_TERMINATED:
                Log.d(TAG, "STATE_TERMINATED close");
                showError();
                break;
            case STATE_VIDEO_INCOMING:
                IPCController.playAudio();
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(LockVideoCallActivity.this,"门锁摄像头初始化中...",Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }, 1000);
                Log.d(TAG, "STATE_VIDEO_INCOMING video picture incoming");
                break;
            default:
                Log.d(TAG, callState.name());
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(IPCOnReceivedMsgEvent event) {
        MsgReceivedType type = MsgReceivedType.getMsgReceivedTypeByID(event.getRtcType());
        Log.d(TAG, "End time is:" + System.currentTimeMillis() + " MsgReceivedType:" + type);
        switch (type) {
            case HANDLE_RTC_CALL_DQ_TYPE:
                Log.d(TAG, "DQ info is:" + event.getMessage());
//                Toast.makeText(this, "DQ info is:" + event.getMessage(),
//                        Toast.LENGTH_SHORT).show();
                break;
            case HANDLE_RTC_CALL_SPEED_TYPE:
                Log.d(TAG, "SPEED info is:" + event.getMessage());
//                Toast.makeText(this, "SPEED info is:" + event.getMessage(),
//                        Toast.LENGTH_SHORT).show();
                break;
            case HANDLE_RTC_AUDIO_PLAYER_EXCEPTION_TYPE:
                Log.d(TAG, "RTC Audio Player info is:" + event.getMessage());
                break;
            case HANDLE_RTC_AUDIO_RECORDER_EXCEPTION_TYPE:
                Log.d(TAG, "RTC Audio Recorder info is:" + event.getMessage());
                break;
            case HANDLE_RTC_VIDEO_DPI_TYPE:
                int DPITYPE = -1;
                try {
                    DPITYPE = Integer.parseInt(event.getMessage());
                } catch (NumberFormatException e) {
                    DPITYPE = -1;
                }
                String DPI_INFO = "UNKNOWN";
                MsgDPIType dpi = MsgDPIType.getMsgDPIType(DPITYPE);
                switch (dpi) {
                    case IPC_RTC_LOW_DPI:
                        DPI_INFO = "320x240";
                        break;
                    case IPC_RTC_MIDDLE_DPI:
                        DPI_INFO = "640x480";
                        break;
                    case IPC_RTC_HIGH_DPI:
                        DPI_INFO = "1280x720";
                        break;
                    case IPC_RTC_SUPER_DPI:
                        DPI_INFO = "1920x1080";
                        break;
                    case IPC_RTC_UNKNOWN_DPI:
                        DPI_INFO = "UNKNOWN";
                        break;
                    default:
                        break;
                }
                Log.v(TAG, "VIDEO info is:" + DPI_INFO);
//                Toast.makeText(this, "VIDEO info is:" + DPI_INFO,
//                        Toast.LENGTH_SHORT).show();
                break;
            case HANDLE_RTC_NULL_TYPE:
                Log.d(TAG, "NULL info is:" + event.getMessage());
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        IPCController.closeAllVideoAsync(null);
//        presenter.turnOffCamera();
        try {
//            IPCController.unRegisterAccount();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == ivUnlock) {
//            Intent intent = new Intent(this, LockPassWordActivity.class);
//            intent.putExtra("devID", devID);
//            intent.putExtra("GwID", GwID);
//            intent.putExtra("type", type);
//            startActivity(intent);
            presenter.unlock(devID, GwID, "000000", type);
        } else if (v == titleBarBack) {
            onBackPressed();
        }
    }

    @Override
    public void makecall() {
        if (dialog != null)
            dialog.dismiss();
        if (viewVideo == null)
            return;
        viewVideo.setVisibility(View.VISIBLE);
        IPCResultCallBack makeCallAsyncCallback = new IPCResultCallBack() {
            @Override
            public void getResult(int result) {
                Log.d(TAG, "makeCallAsyncCallback result is:"
                        + (result == 0 ? "TRUE" : "FALSE"));
            }
        };
        IPCController.setRender("", viewVideo);
        IPCController.makeCallAsync(makeCallAsyncCallback, deviceIdCar,
                sdomain);

    }

    public void closeVideo() {
        IPCController.closeAllVideoAsync(null);
    }

    @Override
    public void close() {

    }

    @Override
    public void ondoorUnlock() {
        Toast.makeText(getBaseContext(), getString(R.string.lock_door_unlock), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError() {
        dialog.dismiss();
        showError();
    }


    @Override
    public void onLockCaneraFound(String GwID, String type) {
        this.GwID = GwID;
        this.type = type;
        ivUnlock.setClickable(true);
        ivUnlock.setAlpha(1.0f);
    }


}
