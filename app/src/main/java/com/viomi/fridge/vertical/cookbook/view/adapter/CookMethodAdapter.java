package com.viomi.fridge.vertical.cookbook.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.util.ImageLoadUtil;
import com.viomi.fridge.vertical.cookbook.model.CookMenuMethod;

import java.util.List;

/**
 * 烹饪方法适配器
 * Created by nanquan on 2018/2/27.
 */
public class CookMethodAdapter extends RecyclerView.Adapter<CookMethodAdapter.MethodHolder> {
    private Context mContext;
    private List<CookMenuMethod> mList;

    public CookMethodAdapter(Context context, List<CookMenuMethod> list) {
        mContext = context;
        mList = list;
    }

    @Override
    public MethodHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.item_cook_method, parent, false);
        return new MethodHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MethodHolder holder, int position) {
        CookMenuMethod method = mList.get(position);
        if (!TextUtils.isEmpty(method.getImg())) {
            ImageLoadUtil.getInstance().loadUrl(method.getImg(), holder.ivMethod);
            holder.ivMethod.setVisibility(View.VISIBLE);
        } else {
            holder.ivMethod.setVisibility(View.INVISIBLE);
        }
        if (method.getPosition() != 0) {
            String str = method.getPosition() + "." + method.getStep();
            holder.tvMethod.setText(str);
        } else holder.tvMethod.setText(method.getStep());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MethodHolder extends RecyclerView.ViewHolder {
        ImageView ivMethod;
        TextView tvMethod;

        MethodHolder(View itemView) {
            super(itemView);
            ivMethod = itemView.findViewById(R.id.iv_cook_method);
            tvMethod = itemView.findViewById(R.id.tv_cook_method);
        }
    }
}