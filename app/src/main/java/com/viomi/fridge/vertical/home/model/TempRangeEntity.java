package com.viomi.fridge.vertical.home.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;

/**
 * 思必驰天气 Json
 * Created by William on 2018/8/15.
 */
public class TempRangeEntity implements Serializable {
    @JSONField(name = "max")
    private String max;
    @JSONField(name = "min")
    private String min;

    public String getMax() {
        return max;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }
}
