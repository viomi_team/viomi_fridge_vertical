package com.viomi.fridge.vertical.iot.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.miot.common.abstractdevice.AbstractDevice;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.model.preference.ManagePreference;
import com.viomi.fridge.vertical.common.base.BaseActivity;
import com.viomi.fridge.vertical.common.base.BaseRecyclerViewAdapter;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.iot.view.adapter.DishWashingAdapter;
import com.viomi.fridge.vertical.iot.view.adapter.FanAdapter;
import com.viomi.fridge.vertical.iot.view.adapter.HeatKettleAdapter;
import com.viomi.fridge.vertical.iot.view.adapter.RangeHoodAdapter;
import com.viomi.fridge.vertical.iot.view.adapter.SweepingRobotAdapter;
import com.viomi.fridge.vertical.iot.view.adapter.WashingMachineAdapter;
import com.viomi.fridge.vertical.iot.view.adapter.WaterPurifierAdapter;
import com.viomi.widget.gallery.AnimManager;
import com.viomi.widget.gallery.GalleryRecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 设备卡片 Activity
 * Created by William on 2018/7/4.
 */
public class DeviceGalleryActivity extends BaseActivity {
    private static final String TAG = DeviceGalleryActivity.class.getSimpleName();

    @BindView(R.id.device_gallery_list)
    GalleryRecyclerView mGalleryRecyclerView;

    @BindView(R.id.device_gallery_bg)
    RelativeLayout mRelativeLayout;

    @BindView(R.id.device_gallery_did)
    TextView mTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        layoutId = R.layout.activity_device_gallery;
        super.onCreate(savedInstanceState);
        initView();
    }

    private void initView() {
        int type = getIntent().getIntExtra(AppConstants.DEVICE_GALLERY_TYPE, 0);// 设备类型
        int firstPosition = getIntent().getIntExtra(AppConstants.DEVICE_GALLERY_FIRST, 0);// 初始位置
        if (getIntent().getExtras() == null) return;
        List<AbstractDevice> list = getIntent().getExtras().getParcelableArrayList(AppConstants.DEVICE_GALLERY_LIST);// 设备集合
        LinearLayoutManager layoutManager = new LinearLayoutManager(FridgeApplication.getContext(), LinearLayoutManager.HORIZONTAL, false);
        mGalleryRecyclerView.setFirstPosition(firstPosition * 2);
        mGalleryRecyclerView.setLayoutManager(layoutManager);
        mGalleryRecyclerView.setAdapter(switchAdapter(type, list));
        mGalleryRecyclerView.initFlingSpeed(9000)                           // 设置滑动速度（像素 / s）
                .initPageParams(40, 230) // 设置页边距和左右 item 的可见宽度，单位 dp
                .setAnimFactor(0.15f)                                      // 设置切换动画的参数因子
                .setAnimType(AnimManager.ANIM_BOTTOM_TO_TOP);             // 设置切换动画类型，目前有 AnimManager.ANIM_BOTTOM_TO_TOP 和 AnimManager.ANIM_TOP_TO_BOTTOM
        mGalleryRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    logUtil.d(TAG, "current:" + mGalleryRecyclerView.getScrolledPosition());
                    if (list != null) {
                        String did = "Did：" + list.get(mGalleryRecyclerView.getScrolledPosition()).getDeviceId();
                        mTextView.setText(did);
                    }
                }
            }
        });
        // 设备 Did
        if (ManagePreference.getInstance().getDebug()) mTextView.setVisibility(View.VISIBLE);
        else mTextView.setVisibility(View.GONE);
    }

    @OnClick(R.id.device_gallery_close)
    public void close() {
        mGalleryRecyclerView.setAdapter(new RecyclerView.Adapter() {
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                return null;
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            }

            @Override
            public int getItemCount() {
                return 0;
            }
        });
        finish();
    }

    /**
     * 根据设备类型选择适配器
     */
    private BaseRecyclerViewAdapter switchAdapter(int type, List<AbstractDevice> list) {
        BaseRecyclerViewAdapter adapter = null;
        switch (type) {
            case 1: // 蒸烤箱
                finish();
                break;
            case 2: // 烟灶
                adapter = new RangeHoodAdapter(list);
                break;
            case 3: // 净水器
                adapter = new WaterPurifierAdapter(list);
                break;
            case 4: // 即热饮水吧
                adapter = new HeatKettleAdapter(list);
                break;
            case 5: // 中央净水器
                break;
            case 6: // 中央软水机
                break;
            case 7: // 洗碗机
                adapter = new DishWashingAdapter(list);
                break;
            case 8: // 洗衣机
                adapter = new WashingMachineAdapter(list);
                break;
            case 9: // 热水器
                break;
            case 10: // 空气净化器
                break;
            case 11: // 风扇
                adapter = new FanAdapter(list);
                break;
            case 12: // 香熏机
                break;
            case 13: // 养生壶
                break;
            case 14: // 扫地机器人
                adapter = new SweepingRobotAdapter(list);
                break;
        }
        return adapter;
    }
}
