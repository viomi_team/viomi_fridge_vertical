package com.viomi.fridge.vertical.iot.view.fragment;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.model.entity.QRCodeBase;
import com.viomi.fridge.vertical.common.base.BaseFragment;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.scope.ActivityScoped;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;
import com.viomi.fridge.vertical.fridgecontrol.view.dialog.FridgeDialog;
import com.viomi.fridge.vertical.iot.activity.DeviceGalleryActivity;
import com.viomi.fridge.vertical.iot.contract.IotTypeContract;
import com.viomi.fridge.vertical.iot.model.http.entity.DeviceTypeEntity;
import com.viomi.fridge.vertical.iot.service.IotService;
import com.viomi.fridge.vertical.iot.util.DeviceIconUtil;
import com.viomi.fridge.vertical.iot.view.adapter.DeviceTypeAdapter;
import com.viomi.fridge.vertical.web.activity.BrowserActivity;
import com.viomi.widget.sphere3d.Tag;
import com.viomi.widget.sphere3d.TagCloudView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import rx.Subscription;

import static android.content.Context.BIND_AUTO_CREATE;

/**
 * 互联网家 Fragment
 * Created by William on 2018/3/5.
 */
@ActivityScoped
public class IotFragment extends BaseFragment implements IotTypeContract.View, TagCloudView.OnTagClickListener {
    private static final String TAG = IotFragment.class.getSimpleName();
    private DeviceTypeAdapter mAdapter;// 球形适配器
    private List<DeviceTypeEntity> mList;// 设备分类集合
    private FridgeDialog mFridgeDialog;// 冰箱 Dialog
    private Subscription mSubscription;// 统一消息管理
    private boolean mIsVisibleToUser = false;// 是否可见
    private Context mContext;

    @BindView(R.id.iot_device_sphere)
    TagCloudView mTagCloudView;// 设备场景

    @BindView(R.id.iot_user_icon)
    SimpleDraweeView mSimpleDraweeView;// 用户头像

    @BindView(R.id.iot_user_name)
    TextView mNameTextView;// 用户名称
    @BindView(R.id.iot_device_num)
    TextView mNumTextView;// 智能设备数量
    @BindView(R.id.iot_indoor_air)
    TextView mAirTextView;// 空气质量
    @BindView(R.id.iot_indoor_pm_25)
    TextView mPMTextView;// PM 2.5

    @Inject
    IotTypeContract.Presenter mPresenter;

    @Inject
    public IotFragment() {

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (mContext != null) {
            mIsVisibleToUser = isVisibleToUser;
            if (!isVisibleToUser) { // 不可见
                RxBus.getInstance().post(BusEvent.MSG_IOT_HIDE);
                mContext.unbindService(mServiceConnection);
                if (mPresenter != null) mPresenter.unSubscribe();// 取消订阅
                if (mAdapter != null) mAdapter = null;
                mTagCloudView.stopRevolve();// 停止旋转
                mTagCloudView.removeAllViews();
                mTagCloudView.setIsDrawLine(false);
                mTagCloudView.postInvalidate();
            } else { // 可见
                RxBus.getInstance().post(BusEvent.MSG_IOT_SHOW);
                if (mPresenter != null) {
                    mPresenter.subscribe(this);// 订阅
                    mContext.bindService(new Intent(mContext, IotService.class), mServiceConnection, BIND_AUTO_CREATE);
                }
                mTagCloudView.startRevolve();// 开始旋转
                mTagCloudView.setIsDrawLine(true);
                mAirTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_air_quality), FridgePreference.getInstance().getAirQuality()));
                mPMTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_pm_25), FridgePreference.getInstance().getPM25()));
            }
        }
    }

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.fragment_iot;
    }

    @Override
    protected void initWithOnCreateView() {
        mContext = getActivity();
        mNameTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_user_home), FridgeApplication.getContext().getResources().getString(R.string.me)));
        mNumTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_device_num), 0));
        ImageRequest request = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse("android.resource://" + FridgeApplication.getContext().getPackageName() + "/" + R.drawable.icon_user_head_default))
                .setResizeOptions(new ResizeOptions((int) FridgeApplication.getContext().getResources().getDimension(R.dimen.px_x_120), (int) FridgeApplication.getContext().getResources().getDimension(R.dimen.px_y_120)))
                .build();
        PipelineDraweeController controller = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                .setOldController(mSimpleDraweeView.getController())
                .setImageRequest(request)
                .build();
        mSimpleDraweeView.setController(controller);
        // 消息订阅
        mSubscription = RxBus.getInstance().subscribe(busEvent -> {
            switch (busEvent.getMsgId()) {
                case BusEvent.MSG_HOME_START_SCROLL: // 主页 ViewPager 开始滑动
                    if (mIsVisibleToUser) mTagCloudView.stopRevolve();
                    break;
                case BusEvent.MSG_HOME_STOP_SCROLL: // 主页 ViewPager 停止滑动
                    if (mIsVisibleToUser) mTagCloudView.startRevolve();
                    break;
                case BusEvent.MSG_START_STROLL:
                    if (mIsVisibleToUser) mTagCloudView.startRevolve();
                    break;
                case BusEvent.MSG_IOT_REFRESH: // 刷新 Ui
                    if (getActivity() != null) {
                        getActivity().runOnUiThread(() -> {
                            int count = 0;
                            for (int i = 0; i < mList.size(); i++) {
                                DeviceTypeEntity entity = mList.get(i);
                                count = count + entity.getList().size();
                            }
                            mNumTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_device_num), count));
                            if (mAdapter == null) {
                                mAdapter = new DeviceTypeAdapter(getActivity(), mList);
                                mTagCloudView.setAdapter(mAdapter);
                            }
                        });
                    }
                    break;
                case BusEvent.MSG_IOT_ITEM_REFRESH: // 刷新 Item
                    int position = (int) busEvent.getMsgObject();
                    notifyItemView(position);
                    break;
            }
        });
        mTagCloudView.setOnTagClickListener(this);
    }

    @Override
    public void onDestroyView() {
        if (mPresenter != null) {
            mPresenter.unSubscribe();
            mPresenter = null;
        }
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
        if (mFridgeDialog != null && mFridgeDialog.isAdded()) {
            mFridgeDialog.dismiss();
            mFridgeDialog = null;
        }
        if (mAdapter != null) mAdapter = null;
        if (mList != null) {
            mList.clear();
            mList = null;
        }
        mContext = null;
        super.onDestroyView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstants.CODE_ENTER_DEVICE_CARD) {
            if (mIsVisibleToUser) mTagCloudView.startRevolve();
            RxBus.getInstance().post(BusEvent.MSG_IOT_SHOW);
        }
    }

    @Override
    public void showUserInfo(QRCodeBase qrCodeBase) {
        Uri uri;
        if (qrCodeBase == null) {
            mNameTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_user_home), FridgeApplication.getContext().getResources().getString(R.string.me)));
        } else {
            mNameTextView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_user_home), qrCodeBase.getLoginQRCode().getUserInfo().getNickname()));
        }
        if (qrCodeBase == null || qrCodeBase.getLoginQRCode().getUserInfo().getHeadImg() == null ||
                qrCodeBase.getLoginQRCode().getUserInfo().getHeadImg().equals("null") || qrCodeBase.getLoginQRCode().getUserInfo().getHeadImg().equals("")) {
            uri = Uri.parse("android.resource://" + FridgeApplication.getContext().getPackageName() + "/" + R.drawable.icon_user_head_default);
        } else {
            uri = Uri.parse(qrCodeBase.getLoginQRCode().getUserInfo().getHeadImg());
        }
        ImageRequest request = ImageRequestBuilder
                .newBuilderWithSource(uri)
                .setResizeOptions(new ResizeOptions((int) FridgeApplication.getContext().getResources().getDimension(R.dimen.px_x_120), (int) FridgeApplication.getContext().getResources().getDimension(R.dimen.px_y_120)))
                .build();
        PipelineDraweeController controller = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                .setOldController(mSimpleDraweeView.getController())
                .setImageRequest(request)
                .build();
        mSimpleDraweeView.setController(controller);
    }

    private void notifyItemView(int position) {
        for (Tag tag : mTagCloudView.getTagCloud().getTagList()) {
            if (tag.getIdentification() == position) {
                View view;
                DeviceTypeEntity entity = mList.get(position);
                if (entity.getType().equals(FridgeApplication.getContext().getResources().getString(R.string.iot_device_fridge))) { // 冰箱
                    view = LayoutInflater.from(getActivity()).inflate(R.layout.view_holder_device_type_2, mTagCloudView, false);
                    // 图标
                    ImageView imageView = view.findViewById(R.id.holder_device_type_2_icon);
                    imageView.setImageResource(DeviceIconUtil.switchIconWithPosition(position));
                    // 设备分类
                    TextView textView = view.findViewById(R.id.holder_device_type_2_name);
                    textView.setText(entity.getType());
                    // 背景
                    LinearLayout linearLayout = view.findViewById(R.id.holder_device_type_2_bg);
                    linearLayout.setBackgroundResource(DeviceIconUtil.switchDeviceBg(position));
                    // 数据
                    TextView coldTextView = view.findViewById(R.id.holder_device_type_fridge_cold);
                    TextView changeableTextView = view.findViewById(R.id.holder_device_type_fridge_changeable);
                    TextView freezingTextView = view.findViewById(R.id.holder_device_type_fridge_freezing);
                    LinearLayout linearLayout1 = view.findViewById(R.id.holder_device_type_changeable_layout);
                    View lineView = view.findViewById(R.id.holder_device_type_line);
                    Typeface typeface = Typeface.createFromAsset(FridgeApplication.getContext().getAssets(), "fonts/DINCond-Medium.otf");
                    coldTextView.setTypeface(typeface);
                    changeableTextView.setTypeface(typeface);
                    freezingTextView.setTypeface(typeface);
                    if (entity.getData() != null) { // 数据不为空
                        String[] data = entity.getData().split(",");
                        if (data.length >= 2) {
                            if (data.length == 2) { // 两室
                                linearLayout1.setVisibility(View.GONE);
                                lineView.setVisibility(View.GONE);
                                coldTextView.setText(data[0]);
                                freezingTextView.setText(data[1]);
                            } else {
                                coldTextView.setText(data[0]);
                                changeableTextView.setText(data[1]);
                                freezingTextView.setText(data[2]);
                            }
                        }
                    }
                } else {
                    view = LayoutInflater.from(getActivity()).inflate(R.layout.view_holder_device_type_1, mTagCloudView, false);
                    // 图标
                    ImageView imageView = view.findViewById(R.id.holder_device_type_1_icon);
                    imageView.setImageResource(DeviceIconUtil.switchIconWithPosition(position));
                    // 设备分类
                    TextView typeTextView = view.findViewById(R.id.holder_device_type_1_name);
                    typeTextView.setText(entity.getType());
                    // 背景
                    LinearLayout linearLayout = view.findViewById(R.id.holder_device_type_1_bg);
                    if (entity.getList().size() > 0 && !entity.isExist()) { // 离线
                        linearLayout.setBackgroundResource(R.drawable.icon_device_circle_gray);
                    } else {
                        linearLayout.setBackgroundResource(DeviceIconUtil.switchDeviceBg(position));
                    }
                    // 显示数据
                    TextView dataTextView = view.findViewById(R.id.holder_device_type_1_data);
                    if (entity.getList().size() > 0 && entity.isExist()) { // 在线
                        dataTextView.setText(entity.getData() == null ? FridgeApplication.getContext().getResources().getString(R.string.iot_load_data_fail) : entity.getData());
                    } else if (entity.getList().size() > 0 && !entity.isExist()) { // 离线
                        dataTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.iot_device_type_offline));
                    } else if (entity.getList().size() == 0 && entity.isOnSale()) { // 已上架
                        dataTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.iot_buy_confirm));
                    } else {
                        dataTextView.setText(FridgeApplication.getContext().getResources().getString(R.string.iot_please_look_forward));
                    }
                }
                tag.setView(view);
                view.setOnClickListener(v -> {
                    if (mTagCloudView.getOnTagClickListener() != null) {
                        mTagCloudView.getOnTagClickListener().onItemClick(mTagCloudView, view, position);
                    }
                });
                break;
            }
        }
    }

    @Override
    public void onItemClick(ViewGroup parent, View view, int position) {
        if (isRepeatedClick()) return;
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        DeviceTypeEntity entity = mList.get(position);
        if (entity.getType().equals(FridgeApplication.getContext().getResources().getString(R.string.iot_device_fridge))) { // 冰箱
            if (mIsVisibleToUser) mTagCloudView.stopRevolve();
            if (mFridgeDialog == null) mFridgeDialog = new FridgeDialog();
            if (!mFridgeDialog.isAdded()) mFridgeDialog.show(transaction, TAG);
        } else if (entity.getList().size() > 0) {
            if (mIsVisibleToUser) mTagCloudView.stopRevolve();
            RxBus.getInstance().post(BusEvent.MSG_IOT_STOP);
            Intent intent = new Intent(getActivity(), DeviceGalleryActivity.class);
            intent.putExtra(AppConstants.DEVICE_GALLERY_TYPE, position);
            intent.putExtra(AppConstants.DEVICE_GALLERY_FIRST, entity.getPosition());
            intent.putExtra(AppConstants.DEVICE_GALLERY_LIST, entity.getList());
            startActivityForResult(intent, AppConstants.CODE_ENTER_DEVICE_CARD);
        } else if (entity.isOnSale()) { // 立即购买跳转
            if (mIsVisibleToUser) mTagCloudView.stopRevolve();
            Intent intent = new Intent(getActivity(), BrowserActivity.class);
            intent.putExtra(AppConstants.WEB_URL, mPresenter.switchBuyUrl(entity.getType()));
            startActivity(intent);
        }
    }

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            logUtil.d(TAG, "onServiceConnected");
            IotService iotService = ((IotService.IotBinder) service).getService();
            mList = iotService.getDeviceList();
            RxBus.getInstance().post(BusEvent.MSG_IOT_REFRESH);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            logUtil.d(TAG, "onServiceDisconnected");
        }
    };
}
