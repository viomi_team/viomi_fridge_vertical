package com.viomi.fridge.vertical.home.view.fragment;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeController;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.activity.ManageActivity;
import com.viomi.fridge.vertical.album.activity.AlbumActivity;
import com.viomi.fridge.vertical.common.base.BaseFragment;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.scope.ActivityScoped;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.StatisticsUtil;
import com.viomi.fridge.vertical.common.util.ToastUtil;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.cookbook.activity.CookDetailActivity;
import com.viomi.fridge.vertical.cookbook.activity.CookMainActivity;
import com.viomi.fridge.vertical.cookbook.model.CookMenuDetail;
import com.viomi.fridge.vertical.foodmanage.activity.FoodManageActivity;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;
import com.viomi.fridge.vertical.fridgecontrol.model.serial.entity.DeviceParams;
import com.viomi.fridge.vertical.fridgecontrol.view.dialog.FridgeDialog;
import com.viomi.fridge.vertical.home.contract.MainContract;
import com.viomi.fridge.vertical.home.model.WeatherDetailEntity;
import com.viomi.fridge.vertical.message.view.activity.MessageCenterActivity;
import com.viomi.fridge.vertical.messageboard.activity.MessageBoardActivity;
import com.viomi.fridge.vertical.messageboard.model.entity.MessageBoardEntity;
import com.viomi.fridge.vertical.speech.util.WeatherIconUtil;
import com.viomi.fridge.vertical.timer.activity.TimerActivity;
import com.viomi.fridge.vertical.web.activity.BrowserActivity;
import com.viomi.widget.AutoScrollPageView;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import rx.Observable;
import rx.Subscription;

/**
 * 主页 Fragment
 * Created by William on 2018/3/5.
 */
@ActivityScoped
public class HomeFragment extends BaseFragment implements MainContract.View, AutoScrollPageView.OnViewPageClickListener {
    private static final String TAG = HomeFragment.class.getSimpleName();
    private Subscription mSubscription;// 消息订阅
    private Subscription mWeatherSubscription;// 每隔 30 分钟刷新一次天气
    private String mModel;// 冰箱 Model
    private CookMenuDetail mCookMenuDetail;// 推荐菜谱
    private FridgeDialog mFridgeDialog;// 冰箱控制 Dialog

    @Inject
    MainContract.Presenter mPresenter;

    @BindView(R.id.home_weather_icon)
    ImageView mWeatherImageView;// 天气图标
    @BindView(R.id.home_city_icon)
    ImageView mCityImageView;// 定位图标

    @BindView(R.id.home_line)
    View mView;// 分割线

    @BindView(R.id.home_album_banner)
    AutoScrollPageView mAutoScrollPageView;// 电子相册轮播

    @BindView(R.id.home_message_board)
    SimpleDraweeView mBoardSimpleDraweeView;// 留言板图片
    @BindView(R.id.home_cookbook_img)
    SimpleDraweeView mCookbookSimpleDraweeView;// 菜谱图片

    @BindView(R.id.home_weather_desc)
    TextView mWeatherDescTextView;// 天气描述
    @BindView(R.id.home_weather_temp_range)
    TextView mWeatherTempRangeTextView;// 气温范围
    @BindView(R.id.home_weather_temp)
    TextView mWeatherTempTextView;// 气温
    @BindView(R.id.home_weather_temp_unit)
    TextView mWeatherTempUnitTextView;// 气温单位
    @BindView(R.id.home_city)
    TextView mCityTextView;// 城市
    @BindView(R.id.home_date)
    TextView mDateTextView;// 日期
    @BindView(R.id.home_time)
    TextView mTimeTextView;// 时间
    @BindView(R.id.home_cold_temp)
    TextView mColdTempTextView;// 冷藏室设置温度
    @BindView(R.id.home_cc_temp)
    TextView mCCTempTextView;// 冷藏变温区设置温度
    @BindView(R.id.home_changeable_temp)
    TextView mChangeableTempTextView;// 变温室设置温度
    @BindView(R.id.home_freezing_temp)
    TextView mFreezingTempTextView;// 冷冻室设置温度
    @BindView(R.id.home_cookbook_name)
    TextView mCookbookTextView;// 菜谱名称

    @BindView(R.id.home_changeable_layout)
    LinearLayout mChangeableLinearLayout;// 变温室布局
    @BindView(R.id.home_cc_layout)
    LinearLayout mCCLinearLayout;// 冷藏变温区布局

    @BindView(R.id.home_tool_layout)
    RelativeLayout mModule6RelativeLayout;// 6 图标布局
    @BindView(R.id.home_module_layout)
    RelativeLayout mModule5RelativeLayout;// 5 图标布局
    @BindView(R.id.home_message_board_layout)
    RelativeLayout mBoardRelativeLayout;// 留言板背景

    @Inject
    public HomeFragment() {

    }

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.fragment_home;
    }

    @Override
    protected void initWithOnCreateView() {
        logUtil.d(TAG, "onCreateView");
        mPresenter.subscribe(this);// 订阅
        mAutoScrollPageView.setOnViewPageClickListener(this);
        // 字体设置
        Typeface typeface = Typeface.createFromAsset(FridgeApplication.getContext().getAssets(), "fonts/DINCond-Medium.otf");
        mWeatherTempRangeTextView.setTypeface(typeface);
        mWeatherTempTextView.setTypeface(typeface);
        mTimeTextView.setTypeface(typeface);
        mColdTempTextView.setTypeface(typeface);
        mCCTempTextView.setTypeface(typeface);
        mChangeableTempTextView.setTypeface(typeface);
        mFreezingTempTextView.setTypeface(typeface);
        // 根据 Model 匹配 Ui
        mModel = FridgePreference.getInstance().getModel();
        if (mModel.equals(AppConstants.MODEL_X2) || mModel.equals(AppConstants.MODEL_X3)
                || mModel.equals(AppConstants.MODEL_X4) || mModel.equals(AppConstants.MODEL_JD)) {
            mChangeableLinearLayout.setVisibility(View.GONE);
            mCCLinearLayout.setVisibility(View.GONE);
            mView.setVisibility(View.VISIBLE);
        }
        refreshTime();// 刷新时间
        // 消息订阅
        mSubscription = RxBus.getInstance().subscribe(busEvent -> {
            switch (busEvent.getMsgId()) {
                case BusEvent.MSG_TIME_MINUTE: // 分钟监听
                    refreshTime();
                    break;
                case BusEvent.MSG_ALBUM_UPDATE: // 电子相册更新
                    if (mPresenter != null) mPresenter.loadAlbum();
                    break;
                case BusEvent.MSG_MESSAGE_BOARD_UPDATE: // 留言板更新
                    if (mPresenter != null) mPresenter.loadMessageBoard();
                    break;
                case BusEvent.MSG_START_STROLL: // 电子相册开始滚动
                    if (mAutoScrollPageView != null) mAutoScrollPageView.startAutoScroll();
                    break;
                case BusEvent.MSG_ALBUM_STOP_STROLL: // 电子相册停止滚动
                    if (mAutoScrollPageView != null) mAutoScrollPageView.stopAutoScroll();
                    break;
                case BusEvent.MSG_HOME_START_SCROLL: // ViewPager 开始滚动
                    if (mAutoScrollPageView != null) mAutoScrollPageView.stopAutoScroll();
                    break;
                case BusEvent.MSG_HOME_STOP_SCROLL: // ViewPager 结束滚动
                    if (mAutoScrollPageView != null) mAutoScrollPageView.startAutoScroll();
                    break;
            }
        });
        refreshCookbook(null);
    }

    @Override
    public void onResume() {
        super.onResume();
        setIconLayout();
    }

    @Override
    public void onDestroyView() {
        logUtil.e(TAG, "onDestroyView");
        mPresenter.unSubscribe();// 取消订阅
        mPresenter = null;
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
        if (mWeatherSubscription != null) {
            mWeatherSubscription.unsubscribe();
            mWeatherSubscription = null;
        }
        if (mFridgeDialog != null && mFridgeDialog.isAdded()) {
            mFridgeDialog.dismiss();
            mFridgeDialog = null;
        }
        super.onDestroyView();
    }

    @OnClick(R.id.home_fridge_layout)
    public void enterFridgeControl() { // 显示冰箱控制
        if (mFridgeDialog == null) mFridgeDialog = new FridgeDialog();
        if (mFridgeDialog.isAdded()) return;
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        mFridgeDialog.show(ft, TAG);
    }

    @OnClick(R.id.home_food_manage)
    public void enterFoodManagement() { // 食材管理跳转
        Intent intent = new Intent(getActivity(), FoodManageActivity.class);
        startActivity(intent);
    }

    @OnClick({R.id.home_video, R.id.home_new_entertainment})
    public void enterVideo() { // 视频跳转
        StatisticsUtil.writeStatistics(getActivity(), StatisticsUtil.EVENT_ENTER_VIDEO, null);
        if (ToolUtil.checkApkExist(FridgeApplication.getContext(), "com.unilife.fridge.yunmi.media")) {
            try {
                ToolUtil.startOtherApp(FridgeApplication.getContext(), "com.unilife.fridge.yunmi.media", false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (ToolUtil.checkApkExist(FridgeApplication.getContext(), "com.tencent.qqlive")) {
            try {
                ToolUtil.startOtherApp(FridgeApplication.getContext(), "com.tencent.qqlive", false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (ToolUtil.checkApkExist(FridgeApplication.getContext(), "com.youku.phone")) {
            try {
                ToolUtil.startOtherApp(FridgeApplication.getContext(), "com.youku.phone", false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else
            ToastUtil.showCenter(FridgeApplication.getContext(), FridgeApplication.getContext().getResources().getString(R.string.no_app_installed));
    }

    @OnClick(R.id.home_music)
    public void enterMusic() { // 音乐跳转
        if (ToolUtil.checkApkExist(FridgeApplication.getContext(), "fm.xiami.main")) {
            try {
                ToolUtil.startOtherApp(FridgeApplication.getContext(), "fm.xiami.main", false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else
            ToastUtil.showCenter(FridgeApplication.getContext(), FridgeApplication.getContext().getResources().getString(R.string.no_app_installed));
    }

    @OnClick({R.id.home_water_map, R.id.home_new_water})
    public void enterMap() { // 水质地图跳转
        Intent intent = new Intent(getActivity(), BrowserActivity.class);
        intent.putExtra(AppConstants.WEB_URL, AppConstants.URL_WATER_MAP);
        startActivity(intent);
    }

    @OnClick({R.id.home_message, R.id.home_new_message})
    public void enterMessage() { // 消息中心跳转
        Intent intent = new Intent(getActivity(), MessageCenterActivity.class);
        startActivity(intent);
    }

    @OnClick({R.id.home_timer, R.id.home_new_timer})
    public void enterTimer() { // 定时器跳转
        Intent intent = new Intent(getActivity(), TimerActivity.class);
        startActivity(intent);
    }

    @OnClick({R.id.home_setting, R.id.home_new_management})
    public void enterSetting() { // 管理中心跳转
        Intent intent = new Intent(getActivity(), ManageActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.home_weather_icon)
    public void refreshWeather() { // 手动刷新天气
        if (mPresenter != null) mPresenter.loadWeather(FridgePreference.getInstance().getCity());
    }

    @OnClick(R.id.home_cookbook_layout)
    public void enterCookbook() { // 菜谱跳转
        if (ToolUtil.checkApkExist(FridgeApplication.getContext(), "com.unilife.fridge.yunmi.recipe")) {
//            if (mCookMenuDetail != null) {
//                Intent intent = new Intent(getActivity(), CookDetailActivity.class);
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("cookMenuDetail", mCookMenuDetail);
//                bundle.putBoolean("isMain", true);
//                intent.putExtras(bundle);
//                startActivity(intent);
//            } else {
            try {
                ToolUtil.startOtherApp(FridgeApplication.getContext(), "com.unilife.fridge.yunmi.recipe", false);
            } catch (Exception e) {
                e.printStackTrace();
            }
//            }
        } else {
            startActivity(new Intent(getActivity(), CookMainActivity.class));
            if (mCookMenuDetail != null) {
                Intent intent = new Intent(getActivity(), CookDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("cookMenuDetail", mCookMenuDetail);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        }
    }

    @OnClick(R.id.home_message_board)
    public void enterMessageBoard() { // 留言板跳转
        Intent intent = new Intent(getActivity(), MessageBoardActivity.class);
        startActivity(intent);
    }

    @Override
    public void displayAlbum(List<Uri> list) {
        mAutoScrollPageView.setCustomAdapterDatas(list);
        mAutoScrollPageView.startAutoScroll();
    }

    @Override
    public void refreshFridgeUi(DeviceParams params) {
        mColdTempTextView.setText(params.isCold_switch() ? String.valueOf(params.getCold_temp_set()) : getResources().getString(R.string.fridge_temp_default));
        mChangeableTempTextView.setText(params.isChangeable_switch() ? String.valueOf(params.getChangeable_temp_set()) : getResources().getString(R.string.fridge_temp_default));
        if (params.isFresh_fruit()) {
            mCCTempTextView.setText(String.valueOf(5));
        } else if (params.isRetain_fresh()) {
            mCCTempTextView.setText(String.valueOf(0));
        } else if (params.isIced()) {
            mCCTempTextView.setText(String.valueOf(-1));
        } else mCCTempTextView.setText(getResources().getString(R.string.fridge_temp_default));
        if (mModel.equals(AppConstants.MODEL_X3) || mModel.equals(AppConstants.MODEL_X5)) {
            if (params.getFreezing_temp_set() < -24)
                mFreezingTempTextView.setText(String.valueOf(-24));
            else mFreezingTempTextView.setText(String.valueOf(params.getFreezing_temp_set()));
        } else mFreezingTempTextView.setText(String.valueOf(params.getFreezing_temp_set()));
    }

    @Override
    public void refreshCity(String city) {
        if (mCityTextView.getVisibility() != View.VISIBLE)
            mCityTextView.setVisibility(View.VISIBLE);
        if (mCityImageView.getVisibility() != View.VISIBLE)
            mCityImageView.setVisibility(View.VISIBLE);
        mCityTextView.setText(city);
    }

    @Override
    public void refreshWeather(WeatherDetailEntity entity) {
        String weather = entity.getDaily_forecast().get(0).getCond().getTxt_d().equals(entity.getDaily_forecast().get(0).getCond().getTxt_n()) ?
                entity.getDaily_forecast().get(0).getCond().getTxt_d() :
                entity.getDaily_forecast().get(0).getCond().getTxt_d() + "转" + entity.getDaily_forecast().get(0).getCond().getTxt_n();
        mWeatherImageView.setImageResource(WeatherIconUtil.setWeatherIcon(weather));// 天气图标
        mWeatherTempTextView.setText(entity.getNow().getTmp().replace("℃", ""));// 气温
        if (mWeatherTempUnitTextView.getVisibility() != View.VISIBLE)
            mWeatherTempUnitTextView.setVisibility(View.VISIBLE);// 气温单位
        mWeatherDescTextView.setText(weather);// 天气
        // 气温范围
        String range = entity.getDaily_forecast().get(0).getTmp().getMin().replace("℃", "") + "℃~" +
                entity.getDaily_forecast().get(0).getTmp().getMax().replace("℃", "") + "℃";
        mWeatherTempRangeTextView.setText(range);
        if (mWeatherSubscription != null) {
            mWeatherSubscription.unsubscribe();
            mWeatherSubscription = null;
        }
        mWeatherSubscription = Observable.timer(30, TimeUnit.MINUTES)
                .compose(RxSchedulerUtil.SchedulersTransformer2())
                .onTerminateDetach()
                .subscribe(aLong -> {
                    if (!FridgePreference.getInstance().getCity().equals("") && mPresenter != null) {
                        mPresenter.loadWeather(FridgePreference.getInstance().getCity());
                    }
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
    }

    @Override
    public void refreshMessageBoard(List<MessageBoardEntity> list) {
        Uri uri;
        ImageRequest request;
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mBoardSimpleDraweeView.getLayoutParams();
        if (list == null || list.size() == 0) {
            mBoardRelativeLayout.setBackgroundResource(0);
            uri = Uri.parse("android.resource://" + FridgeApplication.getContext().getPackageName() + "/" + R.drawable.img_message_board_default);
            GenericDraweeHierarchy hierarchy = mBoardSimpleDraweeView.getHierarchy();
            hierarchy.setActualImageScaleType(ScalingUtils.ScaleType.CENTER_CROP);
            hierarchy.setRoundingParams(RoundingParams.fromCornersRadius((int) FridgeApplication.getContext().getResources().getDimension(R.dimen.px_x_5)));
            mBoardSimpleDraweeView.setHierarchy(hierarchy);
            mBoardSimpleDraweeView.setBackgroundResource(0);
            request = ImageRequestBuilder
                    .newBuilderWithSource(uri)
                    .setResizeOptions(new ResizeOptions((int) getResources().getDimension(R.dimen.px_x_408), (int) getResources().getDimension(R.dimen.px_y_593)))
                    .build();
            layoutParams.width = (int) FridgeApplication.getContext().getResources().getDimension(R.dimen.px_x_510);
            layoutParams.height = (int) FridgeApplication.getContext().getResources().getDimension(R.dimen.px_y_786);
        } else {
            mBoardRelativeLayout.setBackgroundResource(android.R.color.white);
            uri = Uri.fromFile(list.get(0).getFile());
            mBoardSimpleDraweeView.setBackgroundResource(android.R.color.white);
            request = ImageRequestBuilder
                    .newBuilderWithSource(uri)
                    .setResizeOptions(new ResizeOptions((int) getResources().getDimension(R.dimen.px_x_342), (int) getResources().getDimension(R.dimen.px_y_593)))
                    .build();
            layoutParams.width = (int) FridgeApplication.getContext().getResources().getDimension(R.dimen.px_x_454);
            layoutParams.height = (int) FridgeApplication.getContext().getResources().getDimension(R.dimen.px_y_786);
        }
        mBoardSimpleDraweeView.setLayoutParams(layoutParams);
        PipelineDraweeController controller = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                .setOldController(mBoardSimpleDraweeView.getController())
                .setImageRequest(request)
                .build();
        mBoardSimpleDraweeView.setController(controller);
    }

    @Override
    public void refreshCookbook(CookMenuDetail cookMenuDetail) {
        mCookMenuDetail = cookMenuDetail;
        Uri uri;
        if (cookMenuDetail == null) {
            uri = Uri.parse("android.resource://" + FridgeApplication.getContext().getPackageName() + "/" + R.drawable.img_cookbook_default);
            mCookbookTextView.setText(getResources().getString(R.string.home_cookbook));
        } else {
            uri = Uri.parse(cookMenuDetail.getRecipe().getImg());
            mCookbookTextView.setText(cookMenuDetail.getName());
        }
        GenericDraweeHierarchy hierarchy = mCookbookSimpleDraweeView.getHierarchy();
        hierarchy.setRoundingParams(RoundingParams.fromCornersRadius((int) FridgeApplication.getContext().getResources().getDimension(R.dimen.px_x_5)));
        mCookbookSimpleDraweeView.setHierarchy(hierarchy);
        ImageRequest request = ImageRequestBuilder
                .newBuilderWithSource(uri)
                .setResizeOptions(new ResizeOptions((int) getResources().getDimension(R.dimen.px_x_408), (int) getResources().getDimension(R.dimen.px_y_340)))
                .build();
        PipelineDraweeController controller = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                .setOldController(mCookbookSimpleDraweeView.getController())
                .setImageRequest(request)
                .build();
        mCookbookSimpleDraweeView.setController(controller);
    }

    /**
     * 刷新时间
     */
    private void refreshTime() {
        Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        String hour = calendar.get(Calendar.HOUR_OF_DAY) < 10 ? "0" + calendar.get(Calendar.HOUR_OF_DAY) : calendar.get(Calendar.HOUR_OF_DAY) + "";
        String minute = calendar.get(Calendar.MINUTE) < 10 ? "0" + calendar.get(Calendar.MINUTE) : calendar.get(Calendar.MINUTE) + "";
        String date = month + getResources().getString(R.string.month) + day + getResources().getString(R.string.day);
        String time = hour + ":" + minute;
        if (getActivity() != null) {
            getActivity().runOnUiThread(() -> {
                if (mDateTextView != null) mDateTextView.setText(date);
                if (mTimeTextView != null) mTimeTextView.setText(time);
            });
        }
    }

    @Override
    public void onPageClick() {
        Intent intent = new Intent(getActivity(), AlbumActivity.class);
        startActivity(intent);
    }

    /**
     * 动态设置图标位置
     */
    private void setIconLayout() {
        if (ToolUtil.checkApkExist(FridgeApplication.getContext(), "com.unilife.fridge.yunmi.media")) {
            mModule6RelativeLayout.setVisibility(View.GONE);
            mModule5RelativeLayout.setVisibility(View.VISIBLE);

            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mBoardRelativeLayout.getLayoutParams();
            layoutParams.addRule(RelativeLayout.BELOW, R.id.home_module_layout);
            mBoardRelativeLayout.setLayoutParams(layoutParams);
        } else {
            mModule6RelativeLayout.setVisibility(View.VISIBLE);
            mModule5RelativeLayout.setVisibility(View.GONE);

            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mBoardRelativeLayout.getLayoutParams();
            layoutParams.addRule(RelativeLayout.BELOW, R.id.home_tool_layout);
            mBoardRelativeLayout.setLayoutParams(layoutParams);
        }
    }
}
