package com.viomi.fridge.vertical.administration.contract;

import android.graphics.Bitmap;

import com.viomi.fridge.vertical.administration.model.entity.QRCodeLogin;
import com.viomi.fridge.vertical.common.callback.BasePresenter;

/**
 * 登录二维码生成 Contract
 * Created by William on 2018/1/27.
 */
public interface LoginQRCodeContract {
    interface View {
        void showQRCode(QRCodeLogin loginQRCode);// 显示二维码

        void showMiQRCode(Bitmap bitmap);// 显示米家绑定二维码

        void showRetry();// 显示重试 Ui

        void showLoading();// 显示加载 Dialog

        void loginFail(String message);// 登录失败

        void loginSuccess();// 登录成功
    }

    interface Presenter extends BasePresenter<View> {
        void loadQRCode();// 获取二维码链接

        void checkStatus();// 检查登录状态

        void loadMiQRCode();// 米家绑定二维码
    }
}
