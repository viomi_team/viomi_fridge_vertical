package com.viomi.fridge.vertical.common.task;

import android.os.AsyncTask;

import com.miot.api.MiotManager;
import com.miot.common.config.AppConfiguration;
import com.miot.common.model.DeviceModel;
import com.miot.common.model.DeviceModelException;
import com.miot.common.model.DeviceModelFactory;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.administration.model.preference.LoginPreference;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.iot.model.http.entity.WaterPurifierBase;

/**
 * MiIot 配置初始化任务
 * Created by William on 2018/1/30.
 */
public class MiIotOpenTask extends AsyncTask<Void, Void, Integer> {
    private static final String TAG = MiIotOpenTask.class.getSimpleName();

    @Override
    protected Integer doInBackground(Void... voids) {
        AppConfiguration appConfig = new AppConfiguration();
        logUtil.d(TAG, LoginPreference.getInstance().getPhoneType());
        if (LoginPreference.getInstance().getPhoneType().equals("android")) {
            appConfig.setAppId(AppConstants.OAUTH_ANDROID_APP_ID);
            appConfig.setAppKey(AppConstants.OAUTH_ANDROID_APP_KEY);
        } else {
            appConfig.setAppId(AppConstants.OAUTH_IOS_APP_ID);
            appConfig.setAppKey(AppConstants.OAUTH_IOS_APP_KEY);
        }

        MiotManager.getInstance().setAppConfig(appConfig);

        try {
            // 净水器
            DeviceModel water1 = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.YUNMI_WATERPURIFIER_V1,
                    "device/ddd_WaterPurifier.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(water1);
            DeviceModel water2 = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.YUNMI_WATERPURIFIER_V2,
                    "device/ddd_WaterPurifier.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(water2);
            DeviceModel water3 = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.YUNMI_WATERPURIFIER_V3,
                    "device/ddd_WaterPurifier.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(water3);
            DeviceModel water4 = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.YUNMI_WATERPURI_LX2,
                    "device/ddd_WaterPurifier.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(water4);
            DeviceModel water5 = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.YUNMI_WATERPURI_LX3,
                    "device/ddd_WaterPurifier.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(water5);
            DeviceModel water6 = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.YUNMI_WATERPURI_V1,
                    "device/ddd_WaterPuri_V1.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(water6);
            DeviceModel water7 = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.YUNMI_WATERPURI_V2,
                    "device/ddd_WaterPuri_V2.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(water7);
            DeviceModel water8 = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.YUNMI_WATERPURI_S1,
                    "device/ddd_WaterPuri_S1.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(water8);
            DeviceModel water9 = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.YUNMI_WATERPURI_S2,
                    "device/ddd_WaterPuri_S2.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(water9);
            DeviceModel water10 = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.YUNMI_WATERPURI_C1,
                    "device/ddd_WaterPuri_C1.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(water10);
            DeviceModel water11 = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.YUNMI_WATERPURI_C2,
                    "device/ddd_WaterPuri_C2.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(water11);
            DeviceModel water12 = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.YUNMI_WATERPURI_X3,
                    "device/ddd_WaterPuri_X3.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(water12);
            DeviceModel water13 = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.YUNMI_WATERPURI_X5,
                    "device/ddd_WaterPuri_X5.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(water13);
            DeviceModel water14 = DeviceModelFactory.createDeviceModel(FridgeApplication.getInstance(), AppConstants.YUNMI_WATERPURI_LX5,
                    "device/ddd_WaterPurifier.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(water14);

            // 烟机
            DeviceModel hoodA6Model = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.VIOMI_HOOD_A6,
                    "device/Viomi_Hood_A6.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(hoodA6Model);
            DeviceModel hoodA7Model = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.VIOMI_HOOD_A7,
                    "device/Viomi_Hood_A7.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(hoodA7Model);
            DeviceModel hoodA4Model = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.VIOMI_HOOD_A4,
                    "device/Viomi_Hood_A4.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(hoodA4Model);
            DeviceModel hoodA5Model = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.VIOMI_HOOD_A5,
                    "device/Viomi_Hood_A5.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(hoodA5Model);
            DeviceModel hoodC1Model = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.VIOMI_HOOD_C1,
                    "device/Viomi_Hood_C1.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(hoodC1Model);
            DeviceModel hoodH1Model = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.VIOMI_HOOD_H1,
                    "device/Viomi_Hood_H1.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(hoodH1Model);
            DeviceModel hoodH2Model = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.VIOMI_HOOD_H2,
                    "device/Viomi_Hood_H2.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(hoodH2Model);
            DeviceModel hoodX2Model = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.VIOMI_HOOD_X2,
                    "device/Viomi_Hood_X2.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(hoodX2Model);

            // 即热饮水吧
            DeviceModel KettleR1Model = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.YUNMI_KETTLE_R1,
                    "device/Viomi_Kettle_R1.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(KettleR1Model);
            DeviceModel KettleR2Model = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.YUNMI_KETTLE_R2,
                    "device/Viomi_Kettle_R2.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(KettleR2Model);
            DeviceModel KettleR3Model = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.YUNMI_KETTLE_R3,
                    "device/Viomi_Kettle_R3.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(KettleR3Model);

            // 管线机
            DeviceModel PLmg2Model = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.YUNMI_PLMACHINE_MG2,
                    "device/Viomi_Plmachine_Mg2.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(PLmg2Model);

            // 洗碗机
            DeviceModel DishWasherV01Model = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.VIOMI_DISH_WASHER_V01,
                    "device/Viomi_Dish_Washer_V01.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(DishWasherV01Model);
            DeviceModel DishWasherV02Model = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.VIOMI_DISH_WASHER_V02,
                    "device/Viomi_Dish_Washer_V02.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(DishWasherV02Model);

            // 洗衣机
            DeviceModel WasherU1Model = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.VIOMI_WASHER_U1,
                    "device/Viomi_Washer_U1.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(WasherU1Model);

            // 风扇
            DeviceModel FanV1Model = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.VIOMI_FAN_V1,
                    "device/Viomi_Fan_V1.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(FanV1Model);

            // 扫地机器人
            DeviceModel VacuumV1Model = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.VIOMI_VACUUM_V1,
                    "device/Viomi_Vacuum_V1.xml", WaterPurifierBase.class);
            MiotManager.getInstance().addModel(VacuumV1Model);

            // 蒸烤箱
//            DeviceModel OvenV1Model = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.VIOMI_OVEN_V1,
//                    "device/Viomi_Oven_V1.xml", WaterPurifierBase.class);
//            MiotManager.getInstance().addModel(OvenV1Model);
//            DeviceModel OvenV2Model = DeviceModelFactory.createDeviceModel(FridgeApplication.getContext(), AppConstants.VIOMI_OVEN_V2,
//                    "device/Viomi_Oven_V2.xml", WaterPurifierBase.class);
//            MiotManager.getInstance().addModel(OvenV2Model);
        } catch (DeviceModelException e) {
            e.printStackTrace();
            logUtil.e(TAG, e.getMessage());
        }
        return MiotManager.getInstance().open();
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
        do {
            int result = integer;
            logUtil.d(TAG, "MiIotOpen result: " + result);
        }
        while (false);
    }
}
