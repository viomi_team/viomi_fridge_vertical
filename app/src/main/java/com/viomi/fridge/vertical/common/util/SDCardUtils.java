package com.viomi.fridge.vertical.common.util;

import android.os.Environment;
import android.os.StatFs;

public class SDCardUtils {

    /**
     * 判断 SD 卡是否挂载
     */
    private static boolean isSDCardMounted() {
        // 判断 SD 卡是否挂载
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    /**
     * 获得 SD 卡根目录
     */
    private static String getSDCardRootDir() {
        if (isSDCardMounted()) {
            return Environment.getExternalStorageDirectory().getAbsolutePath();
        }
        return null;
    }

    /**
     * 获得 SD 卡总大小
     *
     * @return 返回MB
     */
    public static long getSDCardTotalSize() {
        if (isSDCardMounted()) {
            StatFs fs = new StatFs(getSDCardRootDir());
            long blockCountLong = fs.getBlockCount();
            long blockSizeLong = fs.getBlockSize();
            return blockCountLong * blockSizeLong / 1024 / 1024;
        }
        return 0;
    }

    /**
     * 获得 SD 卡剩余空间大小，返回 MB
     */
    public static long getSDCardFreeSize() {
        return getSDCardFreeSizeBit() / 1024 / 1024;
    }

    public static long getSDCardFreeSizeBit() {
        if (isSDCardMounted()) {
            StatFs fs = new StatFs(getSDCardRootDir());
            long freeBlocks = fs.getFreeBlocks();
            long blockSize = fs.getBlockSize();
            return freeBlocks * blockSize;
        }
        return 0;
    }

    /**
     * bit 转换为 Kb/M/GB 显示
     */
    public static String bit2KbMGB(long bit) {
        String temp;
        if (bit > 1024L * 1024 * 1024)
            temp = String.format("%.2f", bit / 1024f / 1024 / 1024) + "GB";
        else if (bit > 1024L * 1024)
            temp = String.format("%.2f", bit / 1024f / 1024) + "MB";
        else if (bit > 1024)
            temp = String.format("%.2f", bit / 1024f) + "KB";
        else
            temp = "0Kb";
        return temp;
    }
}