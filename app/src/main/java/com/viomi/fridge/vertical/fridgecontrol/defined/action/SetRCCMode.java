package com.viomi.fridge.vertical.fridgecontrol.defined.action;

import com.viomi.fridge.vertical.fridgecontrol.defined.ViomiDefined;
import com.viomi.fridge.vertical.fridgecontrol.defined.property.RCCMode;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetRCCMode extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setRCCMode.toActionType();

    public SetRCCMode() {
        super(TYPE);

        super.addArgument(RCCMode.TYPE.toString());
    }
}