package com.viomi.fridge.vertical.home.contract;

import android.net.Uri;

import com.viomi.fridge.vertical.common.callback.BasePresenter;
import com.viomi.fridge.vertical.cookbook.model.CookMenuDetail;
import com.viomi.fridge.vertical.fridgecontrol.model.serial.entity.DeviceParams;
import com.viomi.fridge.vertical.home.model.WeatherDetailEntity;
import com.viomi.fridge.vertical.messageboard.model.entity.MessageBoardEntity;

import java.util.List;

/**
 * 首页 Contract
 */
public interface MainContract {
    interface View {
        void displayAlbum(List<Uri> list);// 显示相册

        void refreshFridgeUi(DeviceParams params);// 刷新冰箱 Ui

        void refreshCity(String city);// 刷新定位城市

        void refreshWeather(WeatherDetailEntity entity);// 刷新天气

        void refreshMessageBoard(List<MessageBoardEntity> list);// 留言板刷新

        void refreshCookbook(CookMenuDetail cookMenuDetail);// 菜谱刷新
    }

    interface Presenter extends BasePresenter<View> {
        void loadAlbum();// 读取相册

        void loadMessageBoard();// 读取留言板

        void loadCookBook();// 读取菜谱

        void loadCookbookCategory();// 读取菜谱分类

        void loadCookbookRecommend();// 读取推荐菜谱

        void loadFridge();// 读取冰箱设置温度

        void location();// 定位城市

        void loadWeather(String cityName);// 获取天气
    }
}
