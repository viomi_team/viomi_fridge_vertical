package com.viomi.fridge.vertical.fridgecontrol.model.preference.entity;

import java.io.Serializable;

/**
 * 变温室场景信息
 * Created by William on 2018/2/3.
 */
public class ChangeableScene implements Serializable {
    private String scene;// 场景
    private String tip;// 提示
    private int temp;// 场景对应温度

    public String getScene() {
        return scene;
    }

    public void setScene(String scene) {
        this.scene = scene;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public int getTemp() {
        return temp;
    }

    public void setTemp(int temp) {
        this.temp = temp;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ChangeableScene) {
            ChangeableScene scene = (ChangeableScene) obj;
            return this.scene.equals(scene.scene)
                    && this.tip.equals(scene.tip)
                    && this.temp == scene.temp;
        }
        return false;
    }
}