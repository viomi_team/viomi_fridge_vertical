package com.viomi.fridge.vertical.fridgecontrol.defined.action;

import com.viomi.fridge.vertical.fridgecontrol.defined.ViomiDefined;
import com.viomi.fridge.vertical.fridgecontrol.defined.property.LightUpScreen;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetLightUpScreen extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setLightUpScreen.toActionType();

    public SetLightUpScreen() {
        super(TYPE);

        super.addArgument(LightUpScreen.TYPE.toString());
    }
}