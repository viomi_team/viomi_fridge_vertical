package com.mediatek.factorymode.microphone;

public class JniSpiAudio {

    static {
        System.loadLibrary("spiaudiojni");
    }

    public static native void native_release();

    public static native int native_setup(int samplerate, int channelnum, int audioformat, int periodsize, int periodcount);

    public static native int native_read(byte[] audiodata,int offsetinbytes,int sizeinbytes);

    public static native int native_set_channels_gain(byte[] channelgainsize, int channelnum);
}
