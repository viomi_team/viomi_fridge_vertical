package com.viomi.fridge.vertical.album.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 电子相册时间选择适配器
 * Created by William on 2018/1/23.
 */
public class AlbumTimeAdapter extends BaseRecyclerViewAdapter<AlbumTimeAdapter.TimeHolder> {
    private List<String> mList;

    public AlbumTimeAdapter(List<String> list) {
        this.mList = list;
        if (mList == null) mList = new ArrayList<>();
    }

    @Override
    public TimeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_holder_album_time, parent, false);
        return new TimeHolder(view, this);
    }

    @Override
    public void onBindViewHolder(TimeHolder holder, int position) {
        String time = mList.get(position);
        holder.textView.setText(time);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class TimeHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.album_time_text)
        TextView textView;

        TimeHolder(View itemView, AlbumTimeAdapter adapter) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> adapter.onItemHolderClick(this, 1000));
        }
    }
}