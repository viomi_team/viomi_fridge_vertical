package com.viomi.fridge.vertical.speech.model.repository.skill;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.administration.activity.ManageActivity;
import com.viomi.fridge.vertical.administration.model.preference.ManagePreference;
import com.viomi.fridge.vertical.album.activity.AlbumActivity;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.home.activity.HomeActivity;
import com.viomi.fridge.vertical.message.view.activity.MessageCenterActivity;
import com.viomi.fridge.vertical.messageboard.activity.MessageBoardActivity;
import com.viomi.fridge.vertical.speech.model.preference.SpeechPreference;
import com.viomi.fridge.vertical.timer.TimerApi;
import com.viomi.fridge.vertical.timer.activity.TimerActivity;
import com.viomi.fridge.vertical.timer.service.TimerService;
import com.viomi.fridge.vertical.web.activity.BrowserActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 通用控制技能
 * Created by William on 2018/8/17.
 */
public class ControlSkill {
    private static final String TAG = ControlSkill.class.getSimpleName();

    public static void handle(Context context, String data) {
        try {
            JSONObject jsonObject = new JSONObject(data);
            JSONObject nlu = jsonObject.optJSONObject("nlu");
            if (nlu == null) return;
            JSONObject semantics = nlu.optJSONObject("semantics");
            if (semantics == null) return;
            JSONObject request = semantics.optJSONObject("request");
            if (request == null) return;
            JSONArray slots = request.optJSONArray("slots");
            String value = "", unitValue = "", integerValue = "0", modeValue = "", msgTypeValue = "";
            int mode = 0;
            for (int i = 0; i < slots.length(); i++) {
                JSONObject jsonItem = slots.optJSONObject(i);
                String name = jsonItem.optString("name");
                switch (name) {
                    case "intent":
                        value = jsonItem.optString("value");
                        break;
                    case "时间单位":
                        unitValue = jsonItem.optString("value");
                        break;
                    case "整数":
                        integerValue = jsonItem.optString("value");
                        break;
                    case "定时模式":
                        modeValue = jsonItem.optString("value");
                        break;
                    case "消息类型":
                        msgTypeValue = jsonItem.optString("value");
                        break;
                }
            }

            String content = null;
            Intent intent;

            switch (value) {
                // 音量控制
                case "soundUp": // 增大
                    soundUp();
                    break;
                case "soundDown": // 减小
                    soundDown();
                    break;
                case "soundSet": // 设置百分比
                    int value_set_int = -1;
                    for (int i = 0; i < slots.length(); i++) {
                        JSONObject jsonItem = slots.optJSONObject(i);
                        String name = jsonItem.optString("name");
                        if ("整数".equals(name)) {
                            value_set_int = jsonItem.optInt("value");
                            break;
                        }
                    }
                    if (value_set_int != -1) setSoundPercent(value_set_int);
                    break;
                case "soundMax": // 最大
                    setSoundPercent(100);
                    break;
                case "soundMin": // 最小
                    setSoundPercent(0);
                    break;
                // 跳转管理中心
                case "openset":
                    intent = new Intent(context, ManageActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    content = "已为您打开管理中心";
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                // 返回桌面
                case "BackToTheDesktop":
                    intent = new Intent(context, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(AppConstants.HOME_POSITION, 1);
                    context.startActivity(intent);
                    content = "已为您返回桌面";
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                // 跳转云米商城
                case "打开云米商城":
                    intent = new Intent(context, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(AppConstants.HOME_POSITION, 2);
                    context.startActivity(intent);
                    content = "已为您打开云米商城";
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                // 跳转互联网家
                case "打开互联界面":
                    intent = new Intent(context, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(AppConstants.HOME_POSITION, 0);
                    context.startActivity(intent);
                    content = "已为您打开互联界面";
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                // 打开定时器
                case "打开定时":
                    intent = new Intent(context, TimerActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    content = "已为您打开定时器";
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                // 设置定时器时间
                case "设置定时":
                    switch (unitValue) {
                        case "秒":
                            content = "定时器设置不可低于1分钟";
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            break;
                        case "分钟":
                            if (Integer.parseInt(integerValue) < 1) {
                                content = "定时器设置不可低于1分钟";
                                RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            } else if (Integer.parseInt(integerValue) >= 1440) {
                                content = "定时器设置不可超过24小时";
                                RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            } else if (TimerService.getTimerRunning()) {
                                content = "定时器已启动，请先关闭定时器";
                                RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            } else {
                                content = "已为您把定时器设置为" + integerValue + "分钟";
                                TimerApi.startTime(context, Integer.parseInt(integerValue));
                                RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            }
                            break;
                        case "小时":
                            if (Integer.parseInt(integerValue) >= 24) {
                                content = "定时器设置不可超过24小时";
                                RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            } else if (TimerService.getTimerRunning()) {
                                content = "定时器已启动，请先关闭定时器";
                                RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            } else {
                                content = "已为您把定时器设置为" + integerValue + "小时";
                                TimerApi.startTime(context, Integer.parseInt(integerValue) * 60);
                                RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            }
                            break;
                    }
                    break;
                // 设置定时模式
                case "设置定时模式":
                    if (TimerService.getTimerRunning()) {
                        content = "定时器已启动，请先关闭定时器";
                        RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    } else {
                        switch (modeValue) {
                            case "蒸蛋":
                                mode = 1;
                                content = "已为您把定时器设置为蒸蛋模式";
                                break;
                            case "煮粥":
                                mode = 2;
                                content = "已为您把定时器设置为煮粥模式";
                                break;
                            case "煲汤":
                                mode = 3;
                                content = "已为您把定时器设置为煲汤模式";
                                break;
                            case "焖饭":
                                mode = 4;
                                content = "已为您把定时器设置为焖饭模式";
                                break;
                            case "炖鱼":
                                mode = 5;
                                content = "已为您把定时器设置为炖鱼模式";
                                break;
                            case "煎药":
                                mode = 6;
                                content = "已为您把定时器设置为煎药模式";
                                break;
                        }
                        if (content != null) {
                            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                            TimerApi.startMode(context, mode);
                        }
                    }
                    break;
                // 关闭定时器
                case "关闭定时":
                    if (!TimerService.getTimerRunning()) content = "定时器已关闭";
                    else {
                        content = "已为您关闭定时器";
                        TimerApi.cancel();
                    }
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "查看消息":
                    if (msgTypeValue.equals("")) content = "已为您打开消息中心";
                    else content = "已为您打开" + msgTypeValue;
                    intent = new Intent(context, MessageCenterActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    if (!msgTypeValue.equals(""))
                        intent.putExtra(AppConstants.MESSAGE_CENTER_TYPE, msgTypeValue);
                    context.startActivity(intent);
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "打开相册":
                    intent = new Intent(context, AlbumActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    content = "已为您打开电子相册";
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "删除相片":
                    if (!ToolUtil.isActivityRunning(FridgeApplication.getContext(), "com.viomi.fridge.vertical.album.activity.AlbumActivity"))
                        return;
                    RxBus.getInstance().post(BusEvent.MSG_ALBUM_DELETE, Integer.parseInt(integerValue));
                    break;
                case "打开留言板":
                    intent = new Intent(context, MessageBoardActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    content = "已为您打开留言板";
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "打开菜谱":
                    if (ToolUtil.checkApkExist(FridgeApplication.getContext(), "com.unilife.fridge.yunmi.recipe")) {
                        content = "已为您打开菜谱";
                        RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                        try {
                            ToolUtil.startOtherApp(FridgeApplication.getContext(), "com.unilife.fridge.yunmi.recipe", false);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case "打开肯德基外卖":
                    if (SpeechPreference.getInstance().isKFCFirst()) {
                        SpeechPreference.getInstance().saveKFCFirst(false);
                        content = "首次订餐，需要您先填写送餐地址哦。";
                    } else content = "欢迎您来点餐!";
                    intent = new Intent(context, BrowserActivity.class);
                    if (ManagePreference.getInstance().getDebug())
                        intent.putExtra(AppConstants.WEB_URL, AppConstants.URL_KFC_DEBUG);
                    else intent.putExtra(AppConstants.WEB_URL, AppConstants.URL_KFC_RELEASE);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                // 亮度控制
                case "亮度调节":
                    break;
                case "最低亮度":
                    break;
                case "最高亮度":
                    break;
                // 播放控制
                case "next": // 下一首
                    RxBus.getInstance().post(BusEvent.MSG_PLAY_NEXT);
                    break;
                case "prev": // 上一首
                    RxBus.getInstance().post(BusEvent.MSG_PLAY_PREVIOUS);
                    break;
                case "play": // 播放
                    RxBus.getInstance().post(BusEvent.MSG_PLAY_CONTINUE);
                    break;
                case "pause": // 暂停播放
                    RxBus.getInstance().post(BusEvent.MSG_PLAY_PAUSE);
                    break;
                case "exit": // 停止播放
                    RxBus.getInstance().post(BusEvent.MSG_PLAY_STOP);
                    break;
                default:
                    break;
            }
        } catch (JSONException e) {
            logUtil.e(TAG, e.toString());
            e.printStackTrace();
        }
    }

    private static void soundDown() {
        AudioManager audioManager = (AudioManager) FridgeApplication.getContext().getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            int streamMaxMusicVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            int streamMaxSystemVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM);
            int musicVolume = (int) Math.ceil(5 / 100.0 * streamMaxMusicVolume);
            int curMusicVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            int systemVolume = (int) Math.ceil(5 / 100.0 * streamMaxSystemVolume);
            int curSystemVolume = audioManager.getStreamVolume(AudioManager.STREAM_SYSTEM);
            setSound(curMusicVolume - musicVolume, curSystemVolume - systemVolume);
        }
    }

    private static void soundUp() {
        AudioManager audioManager = (AudioManager) FridgeApplication.getContext().getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            int streamMaxMusicVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            int streamMaxSystemVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM);
            int musicVolume = (int) Math.ceil(5 / 100.0 * streamMaxMusicVolume);
            int curMusicVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            int systemVolume = (int) Math.ceil(5 / 100.0 * streamMaxSystemVolume);
            int curSystemVolume = audioManager.getStreamVolume(AudioManager.STREAM_SYSTEM);
            setSound(curMusicVolume + musicVolume, curSystemVolume + systemVolume);
        }
    }

    private static void setSoundPercent(int soundValue) {
        AudioManager audioManager = (AudioManager) FridgeApplication.getContext().getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            int streamMaxMusicVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            int streamMaxSystemVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM);
            int musicVolume = (int) Math.ceil(soundValue / 100.0 * streamMaxMusicVolume);
            int systemVolume = (int) Math.ceil(soundValue / 100.0 * streamMaxSystemVolume);
            setSound(musicVolume, systemVolume);
        }
    }

    private static void setSound(int musicValue, int systemValue) {
        AudioManager audioManager = (AudioManager) FridgeApplication.getContext().getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, musicValue, 0);
            audioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, systemValue, 0);
        }
    }
}
