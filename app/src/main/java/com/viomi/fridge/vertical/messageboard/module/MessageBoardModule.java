package com.viomi.fridge.vertical.messageboard.module;

import com.viomi.fridge.vertical.common.scope.ActivityScoped;
import com.viomi.fridge.vertical.messageboard.contract.MessageBoardContract;
import com.viomi.fridge.vertical.messageboard.presenter.MessageBoardPresenter;

import dagger.Binds;
import dagger.Module;

/**
 * 留言板 Module
 * Created by William on 2018/4/2.
 */
@Module
public abstract class MessageBoardModule {

    @ActivityScoped
    @Binds
    abstract MessageBoardContract.Presenter messageBoardPresenter(MessageBoardPresenter presenter);
}