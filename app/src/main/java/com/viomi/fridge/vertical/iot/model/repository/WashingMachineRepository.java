package com.viomi.fridge.vertical.iot.model.repository;

import android.view.View;
import android.widget.TextView;

import com.miot.api.MiotManager;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.http.ApiClient;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.iot.model.http.entity.RPCResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import rx.Observable;

/**
 * 洗衣机相关 Api
 * Created by William on 2018/7/5.
 */
public class WashingMachineRepository {
    private static final String TAG = WashingMachineRepository.class.getSimpleName();

    /**
     * GetProp
     */
    public static Observable<RPCResult> getProp(String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put("program");
            jsonArray.put("wash_process");
            jsonArray.put("wash_status");
            jsonArray.put("water_temp");
            jsonArray.put("rinse_time");
            jsonArray.put("remain_time");
            jsonArray.put("spin_level");
            jsonArray.put("appoint_time");
            jsonArray.put("be_status");
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
            logUtil.e(TAG, e.toString());
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 设置启动，暂停和停止
     */
    public static Observable<RPCResult> setWashAction(int wash_action, String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_wash_action");
            jsonObject.put("did", did);
            jsonObject.put("id", 12);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(wash_action);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
            logUtil.e(TAG, e.toString());
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 设置水温
     */
    public static Observable<RPCResult> setWashTemp(int water_temp, String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_wash_temp");
            jsonObject.put("did", did);
            jsonObject.put("id", 12);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(water_temp);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
            logUtil.e(TAG, e.toString());
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 设置漂洗次数
     */
    public static Observable<RPCResult> setRinseTime(int rinse_time, String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_rinse_time");
            jsonObject.put("did", did);
            jsonObject.put("id", 12);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(rinse_time);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
            logUtil.e(TAG, e.toString());
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 设定脱水强度
     */
    public static Observable<RPCResult> setSpinLevel(String spin_level, String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_spin_level");
            jsonObject.put("did", did);
            jsonObject.put("id", 12);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(spin_level);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
            logUtil.e(TAG, e.toString());
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 设定洗涤程序
     */
    public static Observable<RPCResult> setWashProgram(String program, String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_wash_program");
            jsonObject.put("did", did);
            jsonObject.put("id", 12);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(program);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
            logUtil.e(TAG, e.toString());
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 洗涤过程
     */
    public static String switchWashProcess(int wash_process, int wash_status) {
        String str = "";
        switch (wash_process) {
            case 0:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_free);
                break;
            case 1:
                str = wash_status == 1 ? FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_weighing) : FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_pause);
                break;
            case 2:
                str = wash_status == 1 ? FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_wash) : FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_pause);
                break;
            case 3:
                str = wash_status == 1 ? FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_poaching) : FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_pause);
                break;
            case 4:
                str = wash_status == 1 ? FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_dry) : FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_pause);
                break;
            case 5:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_finish);
                break;
            case 6:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_stop);
                break;
            case 7:
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_power_off);
                break;
        }
        return str;
    }

    /**
     * 洗涤程序
     */
    public static String switchProgram(String program) {
        String str = "";
        switch (program) {
            case "goldenwash":
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_mode_golden);
                break;
            case "drumclean":
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_mode_drum);
                break;
            case "spin":
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_mode_spin);
                break;
            case "antibacterial":
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_mode_antibacterial);
                break;
            case "super_quick":
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_mode_quick);
                break;
            case "cottons":
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_mode_cottons);
                break;
            case "shirts":
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_mode_shirts);
                break;
            case "child_cloth":
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_mode_child_cloth);
                break;
            case "jeans":
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_mode_jeans);
                break;
            case "wool":
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_mode_wool);
                break;
            case "down":
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_mode_down);
                break;
            case "chiffon":
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_mode_chiffon);
                break;
            case "outdoor":
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_mode_outdoor);
                break;
            case "delicates":
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_mode_delicates);
                break;
            case "underwears":
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_mode_underwear);
                break;
            case "rinse_spin":
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_mode_rinse_spin);
                break;
            case "My Time":
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_mode_define);
                break;
            case "cotton Eco":
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_mode_cotton_eco);
                break;
        }
        return str;
    }

    /**
     * 脱水速度
     */
    public static String switchLevel(String spin_level) {
        String str = "";
        switch (spin_level) {
            case "none":
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_dry_rate_not);
                break;
            case "gentle":
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_dry_rate_gentle);
                break;
            case "mild":
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_dry_rate_mild);
                break;
            case "middle":
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_dry_rate_middle);
                break;
            case "strong":
                str = FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_dry_rate_strong);
                break;
        }
        return str;
    }

    /**
     * 完成时间
     */
    public static void remainTime(TextView textView, int wash_process, int wash_status, int remain_time) {
        if ((wash_process == 1 || wash_process == 2 || wash_process == 3 || wash_process == 4) && wash_status == 1) {
            textView.setVisibility(View.VISIBLE);
            long time = System.currentTimeMillis() + remain_time * 60 * 1000;
            SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.getDefault());
            Date d1 = new Date(time);
            textView.setText(String.format(FridgeApplication.getContext().getResources().getString(R.string.iot_washing_machine_time), format.format(d1)));
        } else {
            textView.setVisibility(View.GONE);
        }
    }
}
