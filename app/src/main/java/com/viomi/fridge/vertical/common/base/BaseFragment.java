package com.viomi.fridge.vertical.common.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.leakcanary.RefWatcher;
import com.viomi.fridge.vertical.FridgeApplication;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;


/**
 * 全局 Fragment
 * Created by William on 2017/12/29.
 */
public abstract class BaseFragment extends DaggerFragment {
    private static final int MIN_CLICK_DELAY_TIME = 1000;
    private long lastClickTime;
    protected int layoutId;
    protected View mRootView;
    protected Unbinder unbinder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initWithOnCreate();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(layoutId, container, false);
        unbinder = ButterKnife.bind(this, mRootView);
        initWithOnCreateView();
        return mRootView;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
//        if (getActivity() != null) {
//            RefWatcher refWatcher = FridgeApplication.getRefWatcher(getActivity());
//            refWatcher.watch(this);
//        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    protected abstract void initWithOnCreate();

    protected abstract void initWithOnCreateView();

    /**
     * 两次点击间隔不能少于 1000 毫秒（防止重复点击）
     */
    protected boolean isRepeatedClick() {
        boolean flag = true;
        long curClickTime = System.currentTimeMillis();
        if ((curClickTime - lastClickTime) >= MIN_CLICK_DELAY_TIME) {
            flag = false;
        }
        lastClickTime = curClickTime;
        return flag;
    }
}
