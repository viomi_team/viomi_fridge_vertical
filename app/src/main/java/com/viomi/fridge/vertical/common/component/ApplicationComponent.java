package com.viomi.fridge.vertical.common.component;

import android.app.Application;

import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.common.module.ActivityBindingModule;
import com.viomi.fridge.vertical.common.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;


@Singleton
@Component(modules = {ApplicationModule.class,
        ActivityBindingModule.class,
        AndroidSupportInjectionModule.class})
public interface ApplicationComponent extends AndroidInjector<FridgeApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        ApplicationComponent.Builder application(Application application);

        ApplicationComponent build();
    }
}
