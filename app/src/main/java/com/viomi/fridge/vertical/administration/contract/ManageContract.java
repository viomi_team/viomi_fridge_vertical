package com.viomi.fridge.vertical.administration.contract;

import com.viomi.fridge.vertical.administration.model.entity.QRCodeBase;
import com.viomi.fridge.vertical.common.callback.BasePresenter;

/**
 * 管理中心 Contract
 * Created by William on 2018/1/29.
 */
public interface ManageContract {
    interface View {
        void showUserInfo(QRCodeBase qrCodeBase);// 显示用户信息
    }

    interface Presenter extends BasePresenter<View> {
        void loadUserInfo();// 读取用户信息

        void logout(int type);// 注销登录或解绑（1：注销登录；2：解绑）
    }
}
