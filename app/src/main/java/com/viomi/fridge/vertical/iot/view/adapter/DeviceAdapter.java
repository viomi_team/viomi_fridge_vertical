package com.viomi.fridge.vertical.iot.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.miot.api.MiotManager;
import com.miot.common.abstractdevice.AbstractDevice;

import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseRecyclerViewAdapter;
import com.viomi.fridge.vertical.iot.model.http.entity.DeviceInfo;
import com.viomi.fridge.vertical.iot.util.DeviceIconUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 设备列表适配器
 * Created by William on 2018/2/3.
 */
public class DeviceAdapter extends BaseRecyclerViewAdapter<DeviceAdapter.DeviceHolder> {
    private List<DeviceInfo> mList;

    public DeviceAdapter(List<DeviceInfo> list) {
        this.mList = list;
        if (mList == null) mList = new ArrayList<>();
    }

    @Override
    public DeviceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_holder_device, parent, false);
        return new DeviceHolder(view, this);
    }

    @Override
    public void onBindViewHolder(DeviceHolder holder, int position) {
        DeviceInfo info = mList.get(position);
        if (info.getAbstractDevice() != null) { // 小米 Iot
            AbstractDevice abstractDevice = info.getAbstractDevice();
            holder.imageView.setImageResource(DeviceIconUtil.switchIconWithModel(abstractDevice.getDeviceModel()));
            if (abstractDevice.isOnline()) {
                holder.nameTextView.setTextColor(0xFF37D58E);
                holder.statusTextView.setTextColor(0xFF37D58E);
            } else {
                holder.nameTextView.setTextColor(0xFF999999);
                holder.statusTextView.setTextColor(0xFF999999);
            }
            String str;
            if (abstractDevice.getOwnerInfo().getUserId().equals(MiotManager.getPeopleManager().getPeople().getUserId())) {
                str = abstractDevice.getDeviceId();
            } else {
                str = abstractDevice.getDeviceId() + FridgeApplication.getContext().getResources().getString(R.string.iot_from) +
                        abstractDevice.getOwnerInfo().getUserName();
            }
            holder.nameTextView.setText(abstractDevice.getName());
            holder.descTextView.setText(str);
            holder.statusTextView.setText(abstractDevice.isOnline() ? holder.online : holder.offline);
        }
//        else if (info.getCameraInfo() != null) { // 绿联摄像头
//            CameraInfo cameraInfo = info.getCameraInfo();
//            holder.imageView.setImageResource(R.drawable.icon_device_camera);
//            if (cameraInfo.isOnline()) {
//                holder.nameTextView.setTextColor(0xFF37D58E);
//                holder.statusTextView.setTextColor(0xFF37D58E);
//            } else {
//                holder.nameTextView.setTextColor(0xFF999999);
//                holder.statusTextView.setTextColor(0xFF999999);
//            }
//            holder.nameTextView.setText(cameraInfo.getName());
//            holder.descTextView.setText(cameraInfo.getSrcId());
//            holder.statusTextView.setText(cameraInfo.isOnline() ? holder.online : holder.offline);
//        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class DeviceHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.holder_device_icon)
        ImageView imageView;// 设备图标
        @BindView(R.id.holder_device_name)
        TextView nameTextView;// 设备名称
        @BindView(R.id.holder_device_detail)
        TextView descTextView;// 描述
        @BindView(R.id.holder_device_status)
        TextView statusTextView;// 设备状态

        @BindString(R.string.iot_device_online)
        String online;
        @BindString(R.string.iot_device_offline)
        String offline;

        DeviceHolder(View itemView, DeviceAdapter adapter) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> adapter.onItemHolderClick(this, 1000));
        }
    }
}