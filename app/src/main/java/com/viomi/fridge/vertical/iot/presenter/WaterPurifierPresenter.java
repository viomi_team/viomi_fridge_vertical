package com.viomi.fridge.vertical.iot.presenter;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.iot.contract.WaterPurifierContract;
import com.viomi.fridge.vertical.iot.model.http.entity.CSeriesPurifierProp;
import com.viomi.fridge.vertical.iot.model.http.entity.MiPurifierProp;
import com.viomi.fridge.vertical.iot.model.http.entity.SSeriesPurifierProp;
import com.viomi.fridge.vertical.iot.model.http.entity.VSeriesPurifierProp;
import com.viomi.fridge.vertical.iot.model.http.entity.WaterPurifierFilter;
import com.viomi.fridge.vertical.iot.model.http.entity.X3PurifierProp;
import com.viomi.fridge.vertical.iot.model.http.entity.X5PurifierProp;
import com.viomi.fridge.vertical.iot.model.repository.WaterPurifierRepository;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * 净水器 Presenter
 * Created by William on 2018/2/5.
 */
public class WaterPurifierPresenter implements WaterPurifierContract.Presenter {
    private static final String TAG = WaterPurifierPresenter.class.getSimpleName();
    private CompositeSubscription mCompositeSubscription;
    private String mDid;

    @Nullable
    private WaterPurifierContract.View mView;

    @Override
    public void subscribe(WaterPurifierContract.View view) {
        this.mView = view;
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public void unSubscribe() {
        this.mView = null;
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
    }

    @Override
    public void switchModel(String model, String did) {
        mDid = did;
        if (mView != null)
            switch (model) {
                case AppConstants.YUNMI_WATERPURI_V1:
                case AppConstants.YUNMI_WATERPURI_V2:
                    mView.showVSeriesUi();
                    VSeriesGetProp();
                    break;
                case AppConstants.YUNMI_WATERPURI_S1:
                case AppConstants.YUNMI_WATERPURI_S2:
                    mView.showSSeriesUi();
                    SSeriesGetProp();
                    break;
                case AppConstants.YUNMI_WATERPURI_C1:
                case AppConstants.YUNMI_WATERPURI_C2:
                    mView.showCSeriesUi();
                    CSeriesGetProp();
                    break;
                case AppConstants.YUNMI_WATERPURI_X3:
                    mView.showX3Ui();
                    X3SeriesGetProp();
                    break;
                case AppConstants.YUNMI_WATERPURI_X5:
                    mView.showX5Ui();
                    X5SeriesGetProp();
                    break;
                case AppConstants.YUNMI_WATERPURIFIER_V1:
                case AppConstants.YUNMI_WATERPURIFIER_V2:
                case AppConstants.YUNMI_WATERPURIFIER_V3:
                case AppConstants.YUNMI_WATERPURI_LX2:
                case AppConstants.YUNMI_WATERPURI_LX3:
                    mView.showMiUi();
                    MiGetProp();
                    break;
            }
    }

    @Override
    public void MiGetProp() {
        Subscription subscription = Observable.interval(0, 5, TimeUnit.SECONDS)
                .onBackpressureDrop() // 背压处理
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .flatMap(aLong -> WaterPurifierRepository.miGetProp(mDid))
                .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                .onTerminateDetach()
                .map(rpcResult -> new MiPurifierProp(rpcResult.getList()))
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(result -> {
                    if (result != null && mView != null) {
                        mView.refreshMiUi(result);
                    }
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void VSeriesGetProp() {
        VSeriesPurifierProp prop = new VSeriesPurifierProp();
        Subscription subscription = Observable.interval(0, 5, TimeUnit.SECONDS)
                .onBackpressureDrop() // 背压处理
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .flatMap(aLong -> WaterPurifierRepository.miGetProp(mDid))
                .filter(rpcResult -> rpcResult.getCode() == 0)
                .onTerminateDetach()
                .flatMap(rpcResult -> {
                    prop.initGetProp(rpcResult.getList());
                    return WaterPurifierRepository.vGetProp(mDid);
                })
                .filter(rpcResult -> rpcResult.getCode() == 0)
                .onTerminateDetach()
                .map(rpcResult -> {
                    prop.initGetExtraProp(rpcResult.getList());
                    return prop;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (rpcResult != null && mView != null) {
                        mView.refreshVSeriesUi(rpcResult);
                    }
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void SSeriesGetProp() {
        Subscription subscription = Observable.interval(0, 5, TimeUnit.SECONDS)
                .onBackpressureDrop() // 背压处理
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .flatMap(aLong -> WaterPurifierRepository.miGetProp(mDid))
                .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                .onTerminateDetach()
                .map(rpcResult -> new SSeriesPurifierProp(rpcResult.getList()))
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(sSeriesPurifierProp -> {
                    if (sSeriesPurifierProp != null && mView != null) {
                        mView.refreshSSeriesUi(sSeriesPurifierProp);
                    }
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void CSeriesGetProp() {
        CSeriesPurifierProp prop = new CSeriesPurifierProp();
        Subscription subscription = Observable.interval(0, 5, TimeUnit.SECONDS)
                .onBackpressureDrop() // 背压处理
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .flatMap(aLong -> WaterPurifierRepository.miGetProp(mDid))
                .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                .onTerminateDetach()
                .flatMap(rpcResult -> {
                    prop.initGetProp(rpcResult.getList());
                    return WaterPurifierRepository.cGetProp(mDid);
                })
                .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                .onTerminateDetach()
                .map(rpcResult -> {
                    prop.initGetExtraProp(rpcResult.getList());
                    return prop;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(cSeriesPurifierProp -> {
                    if (cSeriesPurifierProp != null && mView != null) {
                        mView.refreshCSeriesUi(cSeriesPurifierProp);
                    }
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void X3SeriesGetProp() {
        X3PurifierProp prop = new X3PurifierProp();
        Subscription subscription = Observable.interval(0, 5, TimeUnit.SECONDS)
                .onBackpressureDrop() // 背压处理
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .flatMap(aLong -> WaterPurifierRepository.miGetProp(mDid))
                .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                .onTerminateDetach()
                .flatMap(rpcResult -> {
                    prop.initGetProp(rpcResult.getList());
                    return WaterPurifierRepository.x3GetProp(mDid);
                })
                .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                .onTerminateDetach()
                .map(rpcResult -> {
                    prop.initGetExtraProp(rpcResult.getList());
                    return prop;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(x3PurifierProp -> {
                    if (x3PurifierProp != null && mView != null) {
                        mView.refreshX3Ui(x3PurifierProp);
                    }
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void X5SeriesGetProp() {
        X5PurifierProp prop = new X5PurifierProp();
        Subscription subscription = Observable.interval(0, 5, TimeUnit.SECONDS)
                .onBackpressureDrop() // 背压处理
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .flatMap(aLong -> WaterPurifierRepository.miGetProp(mDid))
                .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                .onTerminateDetach()
                .flatMap(rpcResult -> {
                    prop.initGetProp(rpcResult.getList());
                    return WaterPurifierRepository.x5GetProp(mDid);
                })
                .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                .onTerminateDetach()
                .map(rpcResult -> {
                    prop.initGetExtraProp(rpcResult.getList());
                    return prop;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(x5PurifierProp -> {
                    if (x5PurifierProp != null && mView != null) {
                        mView.refreshX5Ui(x5PurifierProp);
                    }
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void setWaterTemp(int temp) {
        Subscription subscription = WaterPurifierRepository.setTemp(mDid, temp)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (mView != null) mView.setIsTempSetting();
                }, throwable -> {
                    logUtil.e(TAG, throwable.getMessage());
                    if (mView != null) mView.setIsTempSetting();
                });
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void setWaterFlow(int flow, int index) {
        Subscription subscription = WaterPurifierRepository.setFlow(mDid, flow, index)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (mView != null) {
                        if (index == 0) mView.setIsSmallSetting();
                        else if (index == 1) mView.setIsMiddleSetting();
                        else if (index == 2) mView.setIsBigSetting();
                    }
                }, throwable -> {
                    if (mView != null) {
                        if (index == 0) mView.setIsSmallSetting();
                        else if (index == 1) mView.setIsMiddleSetting();
                        else if (index == 2) mView.setIsBigSetting();
                    }
                    logUtil.e(TAG, throwable.getMessage());
                });
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void initFourFilters(List<WaterPurifierFilter> list) {
        list.clear();
        WaterPurifierFilter filter1 = new WaterPurifierFilter();
        filter1.setPercent(-100);
        filter1.setName(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_filter_pp));
        filter1.setDesc(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_filter_pp_desc));
        WaterPurifierFilter filter2 = new WaterPurifierFilter();
        filter2.setPercent(-100);
        filter2.setName(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_filter_front));
        filter2.setDesc(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_filter_front_desc));
        WaterPurifierFilter filter3 = new WaterPurifierFilter();
        filter3.setPercent(-100);
        filter3.setName(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_filter_ro));
        filter3.setDesc(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_filter_ro_desc));
        WaterPurifierFilter filter4 = new WaterPurifierFilter();
        filter4.setPercent(-100);
        filter4.setName(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_filter_behind));
        filter4.setDesc(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_filter_behind_desc));
        list.add(filter1);
        list.add(filter2);
        list.add(filter3);
        list.add(filter4);
    }

    @Override
    public void initThreeFilters(List<WaterPurifierFilter> list) {
        list.clear();
        WaterPurifierFilter filter1 = new WaterPurifierFilter();
        filter1.setPercent(-100);
        filter1.setName(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_filter_intelligence));
        filter1.setDesc(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_filter_intelligence_desc));
        WaterPurifierFilter filter2 = new WaterPurifierFilter();
        filter2.setPercent(-100);
        filter2.setName(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_filter_ro));
        filter2.setDesc(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_filter_ro_desc));
        WaterPurifierFilter filter3 = new WaterPurifierFilter();
        filter3.setPercent(-100);
        filter3.setName(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_filter_pure_tank));
        filter3.setDesc(FridgeApplication.getContext().getResources().getString(R.string.iot_water_purifier_filter_pure_tank_desc));
        list.add(filter1);
        list.add(filter2);
        list.add(filter3);
    }
}