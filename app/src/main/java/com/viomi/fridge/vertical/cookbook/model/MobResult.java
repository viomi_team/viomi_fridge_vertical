package com.viomi.fridge.vertical.cookbook.model;

/**
 * Mob 接口返回
 * Created by nanquan on 2018/2/12.
 */

public class MobResult<T> {

    private String msg;

    private T result;

    private String retCode;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public String getRetCode() {
        return retCode;
    }

    public void setRetCode(String retCode) {
        this.retCode = retCode;
    }
}
