package com.viomi.fridge.vertical.fridgecontrol.defined.action;

import com.viomi.fridge.vertical.fridgecontrol.defined.ViomiDefined;
import com.viomi.fridge.vertical.fridgecontrol.defined.property.SmokeAlarm;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetSmokeAlarm extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setSmokeAlarm.toActionType();

    public SetSmokeAlarm() {
        super(TYPE);

        super.addArgument(SmokeAlarm.TYPE.toString());
    }
}