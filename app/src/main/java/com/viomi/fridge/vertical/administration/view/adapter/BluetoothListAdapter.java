package com.viomi.fridge.vertical.administration.view.adapter;

import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.model.entity.MyBluetoothDevice;

import java.util.List;

/**
 * 蓝牙设置页面适配器
 * Created by nanquan on 2018/1/11.
 */
public class BluetoothListAdapter extends RecyclerView.Adapter<BluetoothListAdapter.BluetoothViewHolder> {
    private Context mContext;
    private List<MyBluetoothDevice> mList;
    private OnItemClickListener onItemClickListener;
    private OnItemLongClickListener onItemLongClickListener;

    public void setOnItemClickListener(BluetoothListAdapter.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setOnItemLongClickListener(BluetoothListAdapter.OnItemLongClickListener onItemLongClickListener) {
        this.onItemLongClickListener = onItemLongClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(MyBluetoothDevice myBluetoothDevice);
    }

    public interface OnItemLongClickListener {
        void onItemLongClick(MyBluetoothDevice myBluetoothDevice);
    }

    public BluetoothListAdapter(Context context, List<MyBluetoothDevice> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public BluetoothViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rooView = LayoutInflater.from(mContext).inflate(R.layout.item_bluetooth, parent, false);
        return new BluetoothViewHolder(rooView);
    }

    @Override
    public void onBindViewHolder(BluetoothViewHolder holder, int position) {
        MyBluetoothDevice myDevice = mList.get(position);
        BluetoothDevice device = myDevice.getBluetoothDevice();
        holder.tvName.setText(device.getName());
        if (device.getBondState() == BluetoothDevice.BOND_BONDED) {
            boolean isConnected = myDevice.isConnected();
            holder.tvStatus.setText(isConnected ? FridgeApplication.getContext().getResources().getString(R.string.status_connected) : FridgeApplication.getContext().getResources().getString(R.string.status_unconnected));
        } else {
            holder.tvStatus.setText("");
        }
        if (device.getBluetoothClass().getMajorDeviceClass() == BluetoothClass.Device.Major.PHONE) {
            holder.imgType.setImageResource(R.drawable.phone);
        } else {
            holder.imgType.setImageResource(R.drawable.earphone);
        }

        if (onItemClickListener != null) {
            holder.itemView.setOnClickListener(view -> onItemClickListener.onItemClick(myDevice));
        }
        if (onItemLongClickListener != null) {
            holder.itemView.setOnLongClickListener(v -> {
                onItemLongClickListener.onItemLongClick(myDevice);
                return true;
            });
        }
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    class BluetoothViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        TextView tvStatus;
        ImageView imgType;

        BluetoothViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            imgType = itemView.findViewById(R.id.imgType);
        }
    }
}