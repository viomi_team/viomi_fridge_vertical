package com.viomi.fridge.vertical.fridgecontrol.model.repository;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.callback.AppCallback;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;
import com.viomi.fridge.vertical.fridgecontrol.model.serial.entity.DeviceError;
import com.viomi.fridge.vertical.fridgecontrol.model.serial.entity.DeviceParams;
import com.viomi.fridge.vertical.fridgecontrol.model.serial.manager.SerialManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;

/**
 * 冰箱相关操作 Api
 * Created by William on 2018/1/8.
 */
public class FridgeRepository {
    private static final String TAG = FridgeRepository.class.getSimpleName();
    private static FridgeRepository mInstance;// 实例
    public final String MODE_SMART = "smart";// 智能模式
    final String MODE_HOLIDAY = "holiday";// 假日模式
    private final String SWITCH_ON = "on";// 开
    private final String SWITCH_OFF = "off";// 关
    private boolean mIsOneKeyCleanRunning = false;// 一键净化是否在运行
    private Subscription mSubscription;// 一键净化计时
    private List<String> mList;// 随机提示文字集合

    public static FridgeRepository getInstance() {
        if (mInstance == null) {
            synchronized (FridgeRepository.class) {
                if (mInstance == null) {
                    mInstance = new FridgeRepository();
                }
            }
        }
        return mInstance;
    }

    /**
     * 设置工作模式
     */
    public void setMode(String mode, AppCallback<String> callback) {
        String model = FridgePreference.getInstance().getModel();
        if (isInspectionRunning() || getComReceiveData().getDeviceError().isError_communication()) {
            if (callback != null) callback.onFail(-99, "setMode fail");
            return;
        }
//        if ((model.equals(AppConstants.MODEL_X4) || model.equals(AppConstants.MODEL_JD)) && getComReceiveData().getError() != 0) {
//            if (callback != null) callback.onFail(-99, "setMode fail");
//            return;
//        }
        DeviceParams params = getComSendData();
        SerialManager.getInstance().setIsModify(true);
        switch (mode) {
            case MODE_SMART: // 智能模式
                params.setMode(AppConstants.MODE_SMART);
                params.setQuick_cold(false);
                params.setQuick_freeze(false);
//                if (!model.equals(AppConstants.MODEL_X4) && !model.equals(AppConstants.MODEL_JD))
                params.setCold_switch(true);
                if (model.equals(AppConstants.MODEL_X4) || model.equals(AppConstants.MODEL_JD)) {
                    FridgePreference.getInstance().saveColdTemp(params.getCold_temp_set());
                    FridgePreference.getInstance().saveFreezingTemp(params.getFreezing_temp_set());
                    params.setCold_temp_set(4);
                    params.setFreezing_temp_set(-18);
                } else {
                    // 根据室内温度设置冷藏室和冷冻室温度
                    if (getComReceiveData().getIndoor_temp() < 23) {
                        params.setCold_temp_set(4);
                        params.setFreezing_temp_set(-18);
                    } else if (getComReceiveData().getIndoor_temp() >= 23 && getComReceiveData().getIndoor_temp() < 27) {
                        params.setCold_temp_set(5);
                        params.setFreezing_temp_set(-18);
                    } else if (getComReceiveData().getIndoor_temp() >= 27 && getComReceiveData().getIndoor_temp() <= 34) {
                        params.setCold_temp_set(6);
                        params.setFreezing_temp_set(-18);
                    } else {
                        params.setCold_temp_set(7);
                        params.setFreezing_temp_set(-17);
                    }
                }
                SerialManager.getInstance().sendToControl(true, callback);
                break;
            case MODE_HOLIDAY: // 假日模式
                params.setMode(AppConstants.MODE_HOLIDAY);
                params.setQuick_cold(false);
                params.setQuick_freeze(false);
//                if (!model.equals(AppConstants.MODEL_X4) && !model.equals(AppConstants.MODEL_JD))
                params.setCold_switch(true);
                params.setChangeable_switch(true);
                if (model.equals(AppConstants.MODEL_X4) || model.equals(AppConstants.MODEL_JD)) {
                    FridgePreference.getInstance().saveColdTemp(params.getCold_temp_set());
                    FridgePreference.getInstance().saveFreezingTemp(params.getFreezing_temp_set());
                }
                params.setCold_temp_set(8);
                if (model.equals(AppConstants.MODEL_X4) || model.equals(AppConstants.MODEL_JD)) {
                    params.setFreezing_temp_set(-15);
                } else params.setFreezing_temp_set(-18);
                SerialManager.getInstance().sendToControl(true, callback);
                break;
            default: // 无模式
                params.setMode(AppConstants.MODE_NULL);
                params.setQuick_cold(false);
                params.setQuick_freeze(false);
                params.setCold_temp_set(FridgePreference.getInstance().getColdTemp());
                params.setFreezing_temp_set(FridgePreference.getInstance().getFreezingTemp());
                SerialManager.getInstance().sendToControl(true, callback);
                break;
        }
        // 属性上报
        MiotRepository.getInstance().sendPropertyMode(params.getMode());
        MiotRepository.getInstance().sendPropertyRCRoomEnable(params.isCold_switch());
        MiotRepository.getInstance().sendPropertyRCSetTemp(params.getCold_temp_set());
        if (FridgePreference.getInstance().getModel().equals(AppConstants.MODEL_X5)) {
            MiotRepository.getInstance().sendPropertyCCRoomEnable(params.isChangeable_switch());
            MiotRepository.getInstance().sendPropertyCCSetTemp(params.getChangeable_temp_set());
        }
        MiotRepository.getInstance().sendPropertyFZSetTemp(params.getFreezing_temp_set());
    }

    /**
     * 设置冷藏室温度
     */
    public void setRCRoomTemp(int temp, AppCallback<String> callback) {
        if (isInspectionRunning() || getComReceiveData().getDeviceError().isError_communication()) {
            if (callback != null) callback.onFail(-99, "setRCRoomTemp fail");
            return;
        }
        DeviceParams params = getComSendData();
        SerialManager.getInstance().setIsModify(true);
        params.setQuick_cold(false);
        params.setCold_temp_set(temp);
        params.setMode(AppConstants.MODE_NULL);
        SerialManager.getInstance().sendToControl(true, callback);
        MiotRepository.getInstance().sendPropertyRCSetTemp(params.getCold_temp_set());
    }

    /**
     * 设置变温室温度
     */
    public boolean setCCRoomTemp(int temp, AppCallback<String> callback) {
        if (isInspectionRunning() || getComReceiveData().getDeviceError().isError_communication()) {
            if (callback != null) callback.onFail(-99, "setCCRoomTemp fail");
            return false;
        }
        DeviceParams params = getComSendData();
        SerialManager.getInstance().setIsModify(true);
        params.setChangeable_temp_set(temp);
        params.setMode(AppConstants.MODE_NULL);
        SerialManager.getInstance().sendToControl(true, callback);
        MiotRepository.getInstance().sendPropertyCCRoomEnable(params.isChangeable_switch());
        return true;
    }

    /**
     * 设置冷冻室温度
     */
    public void setFCRoomTemp(int temp, AppCallback<String> callback) {
        if (isInspectionRunning() || getComReceiveData().getDeviceError().isError_communication()) {
            if (callback != null) callback.onFail(-99, "setFCRoomTemp fail");
            return;
        }
        DeviceParams params = getComSendData();
        SerialManager.getInstance().setIsModify(true);
        params.setQuick_freeze(false);
        params.setFreezing_temp_set(temp);
        params.setMode(AppConstants.MODE_NULL);
        SerialManager.getInstance().sendToControl(true, callback);
        MiotRepository.getInstance().sendPropertyFZSetTemp(params.getFreezing_temp_set());
    }

    /**
     * 设置冷藏室开关
     */
    public void setRCSet(boolean enable, AppCallback<String> callback) {
        if (isInspectionRunning() || getComReceiveData().getDeviceError().isError_communication()) {
            if (callback != null) callback.onFail(-99, "setRCSet fail");
            return;
        }
        DeviceParams params = getComSendData();
        SerialManager.getInstance().setIsModify(true);
        String model = FridgePreference.getInstance().getModel();
        if (enable) {
            params.setQuick_cold(false);
//            if (!model.equals(AppConstants.MODEL_X4) && !model.equals(AppConstants.MODEL_JD))
            params.setMode(AppConstants.MODE_NULL);
            if ((model.equals(AppConstants.MODEL_X4) || model.equals(AppConstants.MODEL_JD)) && params.getMode() == AppConstants.MODE_HOLIDAY) {
                params.setCold_temp_set(8);
            } else params.setCold_temp_set(FridgePreference.getInstance().getColdTemp());
            params.setCold_switch(true);
        } else {
            params.setQuick_cold(false);
            params.setFresh_fruit(false);
            params.setRetain_fresh(false);
            params.setIced(false);
//            if (!model.equals(AppConstants.MODEL_X4) && !model.equals(AppConstants.MODEL_JD))
            params.setMode(AppConstants.MODE_NULL);
            FridgePreference.getInstance().saveColdTemp(params.getCold_temp_set());
            params.setCold_switch(false);
        }
        SerialManager.getInstance().sendToControl(true, callback);
        MiotRepository.getInstance().sendPropertyRCRoomEnable(params.isCold_switch());
        MiotRepository.getInstance().sendPropertyRCSetTemp(params.getCold_temp_set());
    }

    /**
     * 设置变温室开关
     */
    public void setCCSet(boolean enable, AppCallback<String> callback) {
        if (isInspectionRunning() || getComReceiveData().getDeviceError().isError_communication()) {
            if (callback != null) callback.onFail(-99, "setCCSet fail");
            return;
        }
        DeviceParams params = getComSendData();
        SerialManager.getInstance().setIsModify(true);
        if (enable) {
            params.setChangeable_temp_set(FridgePreference.getInstance().getChangeableTemp());
            params.setChangeable_switch(true);
            params.setMode(AppConstants.MODE_NULL);
        } else {
            FridgePreference.getInstance().saveChangeableColdTemp(params.getChangeable_temp_set());
            params.setChangeable_switch(false);
            params.setMode(AppConstants.MODE_NULL);
        }
        SerialManager.getInstance().sendToControl(true, callback);
        MiotRepository.getInstance().sendPropertyCCRoomEnable(params.isChangeable_switch());
        MiotRepository.getInstance().sendPropertyCCSetTemp(params.getChangeable_temp_set());
    }

    /**
     * 速冷设置
     */
    public void setSmartCool(boolean enable, AppCallback<String> callback) {
        String model = FridgePreference.getInstance().getModel();
        if (isInspectionRunning() || getComReceiveData().getDeviceError().isError_communication()) {
            if (callback != null) callback.onFail(-99, "setSmartCool fail");
            return;
        }
//        if ((model.equals(AppConstants.MODEL_X4) || model.equals(AppConstants.MODEL_JD)) && getComReceiveData().getError() != 0) {
//            if (callback != null) callback.onFail(-99, "setSmartCool fail");
//            return;
//        }
        DeviceParams params = getComSendData();
        SerialManager.getInstance().setIsModify(true);
        if (enable) {
            params.setCold_switch(true);
            params.setQuick_cold(true);
            params.setMode(AppConstants.MODE_NULL);// 关闭模式
            int temp = params.getCold_temp_set();
            if (!params.isCold_switch()) temp = AppConstants.COLD_TEMP_DEFAULT;
            FridgePreference.getInstance().saveColdTemp(temp);
            params.setCold_temp_set(2);
        } else {
            params.setQuick_cold(false);
            params.setMode(AppConstants.MODE_NULL);
            params.setCold_temp_set(FridgePreference.getInstance().getColdTemp());
        }
        SerialManager.getInstance().sendToControl(true, callback);
        MiotRepository.getInstance().sendPropertyRCRoomEnable(params.isCold_switch());
        MiotRepository.getInstance().sendPropertyRCSetTemp(params.getCold_temp_set());
    }

    /**
     * 速冻设置
     */
    public void setSmartFreeze(boolean enable, AppCallback<String> callback) {
        String model = FridgePreference.getInstance().getModel();
        if (isInspectionRunning() || getComReceiveData().getDeviceError().isError_communication()) {
            if (callback != null) callback.onFail(-99, "setSmartFreeze fail");
            return;
        }
//        if ((model.equals(AppConstants.MODEL_X4) || model.equals(AppConstants.MODEL_JD)) && getComReceiveData().getError() != 0) {
//            if (callback != null) callback.onFail(-99, "setSmartFreeze fail");
//            return;
//        }
        DeviceParams params = getComSendData();
        SerialManager.getInstance().setIsModify(true);
        if (enable) {
            params.setQuick_freeze(true);
            params.setMode(AppConstants.MODE_NULL);
            FridgePreference.getInstance().saveFreezingTemp(params.getFreezing_temp_set());
            switch (model) {
                case AppConstants.MODEL_X3: // 美菱 462
                case AppConstants.MODEL_X5: // 美菱 521
                    params.setFreezing_temp_set(-32);
                    break;
                case AppConstants.MODEL_X4: // 雪祺 450
                case AppConstants.MODEL_JD: // 雪祺 450 京东定制
                    params.setFreezing_temp_set(-23);
                    break;
                default:
                    params.setFreezing_temp_set(-24);
                    break;
            }
        } else {
            params.setQuick_freeze(false);
            params.setMode(AppConstants.MODE_NULL);
            params.setFreezing_temp_set(FridgePreference.getInstance().getFreezingTemp());
        }
        SerialManager.getInstance().sendToControl(true, callback);
        MiotRepository.getInstance().sendPropertyFZSetTemp(params.getFreezing_temp_set());
    }

    /**
     * 冰饮设置
     */
    public void setCoolBeverage(boolean enable, AppCallback<String> callback) {
        if (isInspectionRunning() || getComReceiveData().getDeviceError().isError_communication()) {
            if (callback != null) callback.onFail(-99, "setCoolBeverage fail");
            return;
        }
        DeviceParams params = getComSendData();
        SerialManager.getInstance().setIsModify(true);
        params.setIced_drink(enable);
        SerialManager.getInstance().sendToControl(true, callback);
    }

    /**
     * 鲜果
     */
    public void setFreshFruit(boolean enable, AppCallback<String> callback) {
        if (isInspectionRunning() || getComReceiveData().getDeviceError().isError_communication()) {
            if (callback != null) callback.onFail(-99, "setFreshFruit fail");
            return;
        }
        DeviceParams params = getComSendData();
        SerialManager.getInstance().setIsModify(true);
        params.setFresh_fruit(enable);
        if (enable) {
            params.setRetain_fresh(false);
            params.setIced(false);
        }
        SerialManager.getInstance().sendToControl(true, callback);
    }

    /**
     * 0 ℃保鲜
     */
    public void setRetainFresh(boolean enable, AppCallback<String> callback) {
        if (isInspectionRunning() || getComReceiveData().getDeviceError().isError_communication()) {
            if (callback != null) callback.onFail(-99, "setRetainFresh fail");
            return;
        }
        DeviceParams params = getComSendData();
        SerialManager.getInstance().setIsModify(true);
        params.setRetain_fresh(enable);
        if (enable) {
            params.setFresh_fruit(false);
            params.setIced(false);
        }
        SerialManager.getInstance().sendToControl(true, callback);
    }

    /**
     * 冰镇
     */
    public void setIced(boolean enable, AppCallback<String> callback) {
        if (isInspectionRunning() || getComReceiveData().getDeviceError().isError_communication()) {
            if (callback != null) callback.onFail(-99, "setIced fail");
            return;
        }
        DeviceParams params = getComSendData();
        SerialManager.getInstance().setIsModify(true);
        params.setIced(enable);
        if (enable) {
            params.setFresh_fruit(false);
            params.setRetain_fresh(false);
        }
        SerialManager.getInstance().sendToControl(true, callback);
    }

    /**
     * 冷藏变温区关闭
     */
    public void closeCC(AppCallback<String> callback) {
        if (isInspectionRunning() || getComReceiveData().getDeviceError().isError_communication()) {
            if (callback != null) callback.onFail(-99, "setIced fail");
            return;
        }
        DeviceParams params = getComSendData();
        SerialManager.getInstance().setIsModify(true);
        params.setFresh_fruit(false);
        params.setRetain_fresh(false);
        params.setIced(false);
        SerialManager.getInstance().sendToControl(true, callback);
    }

    /**
     * 一键净化设置
     */
    public void setOneKeyClean(boolean enable) {
        if (isInspectionRunning() || getComReceiveData().getDeviceError().isError_communication())
            return;
        DeviceParams params = getComSendData();
        SerialManager.getInstance().setIsModify(true);
        params.setOne_key_clean(enable);
        AppCallback<String> callback = new AppCallback<String>() {
            @Override
            public void onSuccess(String data) {
                if (enable) {
                    mIsOneKeyCleanRunning = true;
                    mSubscription = Observable.timer(60, TimeUnit.SECONDS)
                            .compose(RxSchedulerUtil.SchedulersTransformer6())
                            .onTerminateDetach()
                            .subscribe(aLong -> mIsOneKeyCleanRunning = false, throwable -> logUtil.e(TAG, throwable.getMessage()));
                } else {
                    mIsOneKeyCleanRunning = false;
                    if (mSubscription != null) mSubscription.unsubscribe();
                }
            }

            @Override
            public void onFail(int errorCode, String msg) {
                mIsOneKeyCleanRunning = !enable;
            }
        };
        SerialManager.getInstance().sendToControl(true, callback);
    }

    /**
     * 是否一键净化中
     */
    public boolean isOneKeyCleanRunning() {
        return mIsOneKeyCleanRunning && getComSendData().isOne_key_clean();
    }

    /**
     * 滤芯已用寿命（单位：小时）
     */
    public int getFilterLifeUsedTime() {
        return FridgePreference.getInstance().getUsedFilterLife();
    }

    /**
     * 获取模式
     */
    public String getMode() {
        String str;
        int mode = getComSendData().getMode();
        switch (mode) {
            case AppConstants.MODE_SMART:
                str = MODE_SMART;
                break;
            case AppConstants.MODE_HOLIDAY:
                str = MODE_HOLIDAY;
                break;
            default:
                str = MiotRepository.getInstance().MODE_NONE;
                break;
        }
        return str;
    }

    /**
     * 获取冷藏室设置温度
     */
    public int getRCSetTemp() {
        return getComSendData().getCold_temp_set();
    }

    /**
     * 获取变温室设置温度
     */
    public int getCCSetTemp() {
        return getComSendData().getChangeable_temp_set();
    }

    /**
     * 获取冷冻室设置温度
     */
    public int getFCSetTemp() {
        return getComSendData().getFreezing_temp_set();
    }

    /**
     * 获取冷藏室实际温度
     */
    int getRCRealTemp() {
        return getComReceiveData().getCold_temp_real();
    }

    /**
     * 获取变温室实际温度
     */
    int getCCRealTemp() {
        return getComReceiveData().getChangeable_temp_real();
    }

    /**
     * 获取冷冻室实际温度
     */
    int getFCRealTemp() {
        return getComReceiveData().getFreezing_temp_real();
    }

    /**
     * 获取冷藏室开关
     */
    public String getRCSet() {
        return getComSendData().isCold_switch() ? SWITCH_ON : SWITCH_OFF;
    }

    /**
     * 获取变温室开关
     */
    public String getCCSet() {
        return getComSendData().isChangeable_switch() ? SWITCH_ON : SWITCH_OFF;
    }

    /**
     * 获取鲜果状态
     */
    public boolean isFreshFruit() {
        return getComSendData().isFresh_fruit();
    }

    /**
     * 获取 0 ℃保鲜状态
     */
    public boolean isRetainFresh() {
        return getComSendData().isRetain_fresh();
    }

    /**
     * 获取冰镇状态
     */
    public boolean isIced() {
        return getComSendData().isIced();
    }

    /**
     * 获取一键净化
     */
    String getOneKeyClean() {
        return isOneKeyCleanRunning() ? SWITCH_ON : SWITCH_OFF;
    }

    /**
     * 获取室内温度
     */
    int getIndoorTemp() {
        return getComReceiveData().getIndoor_temp();
    }

    /**
     * 获取错误码
     */
    public int getError() {
        return getComReceiveData().getError();
    }

    /**
     * 获取速冷状态
     */
    public String getSmartCool() {
        return getComSendData().isQuick_cold() ? SWITCH_ON : SWITCH_OFF;
    }

    /**
     * 获取速冻状态
     */
    public String getSmartFreeze() {
        return getComSendData().isQuick_freeze() ? SWITCH_ON : SWITCH_OFF;
    }

    /**
     * 获取冰饮状态
     */
    String getCoolBeverage() {
        return getComSendData().isIced_drink() ? SWITCH_ON : SWITCH_OFF;
    }

    /**
     * 获取串口读取的数据
     */
    private DeviceParams getComReceiveData() {
        return SerialManager.getInstance().getDeviceParamsGet();
    }

    /**
     * 获取串口设置的数据
     */
    public DeviceParams getComSendData() {
        return SerialManager.getInstance().getDeviceParamsSet();
    }

    /**
     * 强制不停机
     *
     * @param enable: 开启 / 关闭
     */
    public void setRCFForce(boolean enable, AppCallback<String> callback) {
        DeviceParams params = getComSendData();
        SerialManager.getInstance().setIsModify(true);
        params.setRcf_forced(enable);
        SerialManager.getInstance().sendToControl(true, callback);
    }

    /**
     * 强制化霜设置
     *
     * @param enable: 开启 / 关闭
     */
    public void setFrostForce(boolean enable, AppCallback<String> callback) {
        DeviceParams params = getComSendData();
        SerialManager.getInstance().setIsModify(true);
        params.setFc_forced_frost(enable);
        SerialManager.getInstance().sendToControl(true, callback);
    }

    /**
     * 翻转梁加热器复位模式
     *
     * @param enable: 开启 / 关闭
     */
    public void setFlippingBeamMode(boolean enable, AppCallback<String> callback) {
        DeviceParams params = getComSendData();
        SerialManager.getInstance().setIsModify(true);
        params.setFlipping_beam_reset_mode(enable);
        SerialManager.getInstance().sendToControl(true, callback);
    }

    /**
     * 缩时
     */
    public void setTimeCut(boolean enable, AppCallback<String> callback) {
        DeviceParams params = getComSendData();
        SerialManager.getInstance().setIsModify(true);
        params.setTime_cut(enable);
        SerialManager.getInstance().sendToControl(true, callback);
    }

    /**
     * 商检操作
     *
     * @param enable: 开始 / 停止
     */
    public boolean setInspection(boolean enable) {
//        if (enable) { // 开始商检
//            mIsInspection = true;
//            FridgePreference.saveInspectionFlag(true);
//            SpeechPreference.saveSwitch(false);
//            VoiceManager.getInstance().enableVoice(false);
//            byte[] bytes = new byte[]{0x55, 0x55, (byte) 0xFC, 0x00, 0x00, 0x00, 0x00, 0x5A, 0x2C, 0x46, 0x00, 0x7A, 0x04
//                    , 0x00, 0x00, 0x00, 0x46, (byte) 0xAA, (byte) 0xAA};
//            return SerialManager.getInstance().serialWrite(bytes);
//        } else { // 停止商检
//            mIsInspection = false;
//            FridgePreference.saveInspectionFlag(false);
//            SpeechPreference.saveSwitch(true);
//            VoiceManager.getInstance().enableVoice(true);
//            return false;
//        }
        return false;
    }

    /**
     * 雪祺 450 通信故障时需退出所有模式
     */
    public void quitMode() {
        DeviceParams params = getComSendData();
        if (params.isQuick_cold()) {
            params.setQuick_cold(false);
            params.setMode(AppConstants.MODE_NULL);
            params.setCold_temp_set(FridgePreference.getInstance().getColdTemp());
        }
        if (params.isQuick_freeze()) {
            params.setQuick_freeze(false);
            params.setMode(AppConstants.MODE_NULL);
            params.setFreezing_temp_set(FridgePreference.getInstance().getFreezingTemp());
        }
        if (params.getMode() != AppConstants.MODE_NULL) {
            params.setMode(AppConstants.MODE_NULL);
            params.setQuick_cold(false);
            params.setQuick_freeze(false);
            params.setCold_temp_set(FridgePreference.getInstance().getColdTemp());
            params.setFreezing_temp_set(FridgePreference.getInstance().getFreezingTemp());
        }
        SerialManager.getInstance().sendToControl(true, null);
    }

    /**
     * 是否正在商检
     */
    public boolean isInspectionRunning() {
        return false;
    }

    /**
     * 随机生成冰箱提示文字
     */
    public String randomTip() {
        int mode = SerialManager.getInstance().getDeviceParamsSet().getMode();
        if (mList == null) mList = new ArrayList<>();
        mList.clear();
        if (mode == AppConstants.MODE_SMART) {
            mList.add(FridgeApplication.getContext().getResources().getString(R.string.fridge_tip_9));
        } else if (mode == AppConstants.MODE_HOLIDAY) {
            mList.add(FridgeApplication.getContext().getResources().getString(R.string.fridge_tip_10));
        } else if (getSmartCool().equals(MiotRepository.getInstance().SWITCH_ON)) {
            mList.add(FridgeApplication.getContext().getResources().getString(R.string.fridge_tip_4));
        } else if (getSmartFreeze().equals(MiotRepository.getInstance().SWITCH_ON)) {
            mList.add(FridgeApplication.getContext().getResources().getString(R.string.fridge_tip_5));
        }
        if (isOneKeyCleanRunning()) {
            mList.add(FridgeApplication.getContext().getResources().getString(R.string.fridge_tip_8));
        }
        String report = FridgePreference.getInstance().getWeatherReport();
        if (!report.equals("")) mList.add(report);
        mList.add(FridgeApplication.getContext().getResources().getString(R.string.fridge_tip_1));
        if (FridgePreference.getInstance().getModel().equals(AppConstants.MODEL_X5))
            mList.add(FridgeApplication.getContext().getResources().getString(R.string.fridge_tip_2));
        mList.add(FridgeApplication.getContext().getResources().getString(R.string.fridge_tip_3));
        mList.add(FridgeApplication.getContext().getResources().getString(R.string.fridge_tip_6));
        mList.add(FridgeApplication.getContext().getResources().getString(R.string.fridge_tip_7));

        int count = mList.size();
        int index = new Random().nextInt(count - 1);
        return mList.get(index);
    }

    /**
     * 冰箱故障发生或修复事件上报
     *
     * @param lastError:    上次发生故障信息
     * @param currentError: 本次获取冰箱故障信息
     */
    public void faultHappenOrFix(DeviceError lastError, DeviceError currentError) {
        if (lastError.isError_communication() != currentError.isError_communication()) { // 通信故障
            if (currentError.isError_communication())
                MiotRepository.getInstance().sendFaultHappen(AppConstants.ERROR_COMMUNICATION);
            else
                MiotRepository.getInstance().sendFaultFix(AppConstants.ERROR_COMMUNICATION);
        }
        if (lastError.isError_rc_sensor() != currentError.isError_rc_sensor()) { // 冷藏室传感器故障
            if (currentError.isError_rc_sensor())
                MiotRepository.getInstance().sendFaultHappen(AppConstants.ERROR_RC_SENSOR);
            else
                MiotRepository.getInstance().sendFaultFix(AppConstants.ERROR_RC_SENSOR);
        }
        if (lastError.isError_cc_sensor() != currentError.isError_cc_sensor()) { // 变温室传感器故障
            if (currentError.isError_cc_sensor())
                MiotRepository.getInstance().sendFaultHappen(AppConstants.ERROR_CC_SENSOR);
            else
                MiotRepository.getInstance().sendFaultFix(AppConstants.ERROR_CC_SENSOR);
        }
        if (lastError.isError_fc_sensor() != currentError.isError_fc_sensor()) { // 冷冻室传感器故障
            if (currentError.isError_fc_sensor())
                MiotRepository.getInstance().sendFaultHappen(AppConstants.ERROR_FC_SENSOR);
            else
                MiotRepository.getInstance().sendFaultFix(AppConstants.ERROR_FC_SENSOR);
        }
        if (lastError.isError_rc_defrost_sensor() != currentError.isError_rc_defrost_sensor()) { // 冷藏化霜传感器故障
            if (currentError.isError_rc_defrost_sensor())
                MiotRepository.getInstance().sendFaultHappen(AppConstants.ERROR_RC_DEFROST_SENSOR);
            else
                MiotRepository.getInstance().sendFaultFix(AppConstants.ERROR_RC_DEFROST_SENSOR);
        }
        if (lastError.isError_fc_defrost_sensor() != currentError.isError_fc_defrost_sensor()) { // 冷冻化霜传感器故障
            if (currentError.isError_fc_defrost_sensor())
                MiotRepository.getInstance().sendFaultHappen(AppConstants.ERROR_FC_DEFROST_SENSOR);
            else
                MiotRepository.getInstance().sendFaultFix(AppConstants.ERROR_FC_DEFROST_SENSOR);
        }
        if (lastError.isError_indoor_sensor() != currentError.isError_indoor_sensor()) { // 环境温度传感器故障
            if (currentError.isError_indoor_sensor())
                MiotRepository.getInstance().sendFaultHappen(AppConstants.ERROR_INDOOR_SENSOR);
            else
                MiotRepository.getInstance().sendFaultFix(AppConstants.ERROR_INDOOR_SENSOR);
        }
        if (lastError.isError_fan_door() != currentError.isError_fan_door()) { // 风门故障
            if (currentError.isError_fan_door())
                MiotRepository.getInstance().sendFaultHappen(AppConstants.ERROR_FAN_DOOR);
            else
                MiotRepository.getInstance().sendFaultFix(AppConstants.ERROR_FAN_DOOR);
        }
        if (lastError.isError_rc_fan() != currentError.isError_rc_fan()) { // 冷藏风扇故障
            if (currentError.isError_rc_fan())
                MiotRepository.getInstance().sendFaultHappen(AppConstants.ERROR_RC_FAN);
            else
                MiotRepository.getInstance().sendFaultFix(AppConstants.ERROR_RC_FAN);
        }
        if (lastError.isError_cc_fan() != currentError.isError_cc_fan()) { // 冷凝风扇故障
            if (currentError.isError_cc_fan())
                MiotRepository.getInstance().sendFaultHappen(AppConstants.ERROR_CC_FAN);
            else
                MiotRepository.getInstance().sendFaultFix(AppConstants.ERROR_CC_FAN);
        }
        if (lastError.isError_fc_fan() != currentError.isError_fc_fan()) { // 冷冻风扇故障
            if (currentError.isError_fc_fan())
                MiotRepository.getInstance().sendFaultHappen(AppConstants.ERROR_FC_FAN);
            else
                MiotRepository.getInstance().sendFaultFix(AppConstants.ERROR_FC_FAN);
        }
        if (lastError.isError_defrost() != currentError.isError_defrost()) { // 化霜不良报警
            if (currentError.isError_defrost())
                MiotRepository.getInstance().sendFaultHappen(AppConstants.ERROR_DEFROST);
            else
                MiotRepository.getInstance().sendFaultFix(AppConstants.ERROR_DEFROST);
        }
        if (lastError.isError_cc_defrost_sensor() != currentError.isError_cc_defrost_sensor()) { // 变温化霜传感器故障
            if (currentError.isError_cc_defrost_sensor())
                MiotRepository.getInstance().sendFaultHappen(AppConstants.ERROR_CC_DEFROST_SENSOR);
            else
                MiotRepository.getInstance().sendFaultFix(AppConstants.ERROR_CC_DEFROST_SENSOR);
        }
        if (lastError.isError_humidity_sensor() != currentError.isError_humidity_sensor()) { // 湿度传感器湿度故障
            if (currentError.isError_humidity_sensor())
                MiotRepository.getInstance().sendFaultHappen(AppConstants.ERROR_HUMIDITY_SENSOR);
            else
                MiotRepository.getInstance().sendFaultFix(AppConstants.ERROR_HUMIDITY_SENSOR);
        }
        if (lastError.isError_humidity_temp_sensor() != currentError.isError_humidity_temp_sensor()) { // 湿度传感器温度故障
            if (currentError.isError_humidity_temp_sensor())
                MiotRepository.getInstance().sendFaultHappen(AppConstants.ERROR_HUMIDITY_TEMP_SENSOR);
            else
                MiotRepository.getInstance().sendFaultFix(AppConstants.ERROR_HUMIDITY_TEMP_SENSOR);
        }
        if (lastError.isError_rc_cc_sensor() != currentError.isError_rc_cc_sensor()) { // 冷藏变温传感器故障
            if (currentError.isError_rc_cc_sensor())
                MiotRepository.getInstance().sendFaultHappen(AppConstants.ERROR_RC_CC_SENSOR);
            else
                MiotRepository.getInstance().sendFaultFix(AppConstants.ERROR_RC_CC_SENSOR);
        }
    }
}
