package com.viomi.fridge.vertical.fridgecontrol.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseHandlerActivity;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;
import com.viomi.fridge.vertical.fridgecontrol.model.repository.FridgeRepository;
import com.viomi.fridge.vertical.fridgecontrol.model.serial.manager.SerialManager;
import com.viomi.widget.switchbutton.SwitchButton;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * 工厂测试 Activity
 * Created by William on 2018/1/17.
 */
public class FactoryTestActivity extends BaseHandlerActivity {
    private static final String TAG = FactoryTestActivity.class.getSimpleName();
    private Subscription mSerialSubscription;// 串口压力测试
    private CompositeSubscription mCompositeSubscription;
    private boolean mIsIgnore = false;

    @BindView(R.id.factory_switch)
    SwitchButton mSwitchButton;// 测试缩时开关

    @BindView(R.id.factory_test_frost_force)
    Button mFrostForceButton;// 强制化霜
    @BindView(R.id.factory_test_quick_freezing)
    Button mQuickFreezingButton;// 速冻
    @BindView(R.id.factory_test_rcf_force)
    Button mRCFForceButton;// 强制不停机
    @BindView(R.id.factory_test_inspection)
    Button mInspectionButton;// 商检
    @BindView(R.id.factory_test_closing_mode)
    Button mClosingModeButton;// 防凝露关闭模式
    @BindView(R.id.factory_test_serial)
    Button mSerialPressureButton;// 串口压力测试

    @BindView(R.id.factory_test_model)
    TextView mModelTextView;// 设备型号
    @BindView(R.id.factory_test_data)
    TextView mFridgeDataTextView;// 冰箱相关数据
    @BindView(R.id.factory_test_inspection_desc)
    TextView mInspectionDescTextView;// 商检过程描述
    @BindView(R.id.factory_test_status)
    TextView mFlippingBeamTextView;// 翻转梁加热丝状态
    @BindView(R.id.factory_test_error)
    TextView mErrorTextView;// 转接板型号错误

    @BindView(R.id.factory_time_cut_layout)
    LinearLayout mLinearLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        mTitle = getResources().getString(R.string.factory_test_title);
        layoutId = R.layout.activity_factory_test;
        super.onCreate(savedInstanceState);
        mCompositeSubscription = new CompositeSubscription();
        switch (FridgePreference.getInstance().getModel()) {
            case AppConstants.MODEL_X2: // 双鹿 446
                mModelTextView.setText(String.format(getResources().getString(R.string.factory_test_model), getResources().getString(R.string.model_x2)));
                initWithX2();
                break;
            case AppConstants.MODEL_X3: // 美菱 462
                mModelTextView.setText(String.format(getResources().getString(R.string.factory_test_model), getResources().getString(R.string.model_x3)));
                if (SerialManager.getInstance().getDeviceParamsGet().getData40()
                        != 17 && SerialManager.getInstance().getDeviceParamsGet().getData41() != 12) {
                    mErrorTextView.setVisibility(View.VISIBLE);
                    mErrorTextView.setText(String.format(getResources().getString(R.string.factory_test_model_error),
                            SerialManager.getInstance().getDeviceParamsGet().getData40(),
                            SerialManager.getInstance().getDeviceParamsGet().getData41()));
                }
                initWithX3();
                break;
            case AppConstants.MODEL_X4: // 雪祺 450
                mModelTextView.setText(String.format(getResources().getString(R.string.factory_test_model), getResources().getString(R.string.model_x4)));
                initWithX4();
                break;
            case AppConstants.MODEL_X5: // 美菱 521
                mModelTextView.setText(String.format(getResources().getString(R.string.factory_test_model), getResources().getString(R.string.model_x5)));
                if (SerialManager.getInstance().getDeviceParamsGet().getData40()
                        != 18 && SerialManager.getInstance().getDeviceParamsGet().getData41() != 73) {
                    mErrorTextView.setVisibility(View.VISIBLE);
                    mErrorTextView.setText(String.format(getResources().getString(R.string.factory_test_model_error),
                            SerialManager.getInstance().getDeviceParamsGet().getData40(),
                            SerialManager.getInstance().getDeviceParamsGet().getData41()));
                }
                initWithX5();
                break;
            case AppConstants.MODEL_JD: // 雪祺 450 京东定制
                mModelTextView.setText(String.format(getResources().getString(R.string.factory_test_model), getResources().getString(R.string.model_x4_jd)));
                initWithX4();
                break;
        }
        // 测试缩时
        mSwitchButton.setOnSwitchStateChangeListener(isOn -> {
            if (!mIsIgnore) {
                FridgeRepository.getInstance().setTimeCut(isOn, null);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstants.CODE_SELF_CHECKING) {
            logUtil.d(TAG, "self checking return");
            SerialManager.getInstance().open();// 重新打开串口
        }
    }

    /**
     * 双鹿 446
     */
    private void initWithX2() {
        Subscription subscription = Observable.interval(0, 3, TimeUnit.SECONDS)
                .onBackpressureDrop() // 背压处理
                .subscribeOn(Schedulers.computation())
                .onTerminateDetach()
                .map(aLong -> SerialManager.getInstance().getDeviceParamsGet())
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(deviceParams -> {
                    // 冰箱数据
                    String str = getResources().getString(R.string.factory_test_temp) + "\u2000\u2000" +
                            getResources().getString(R.string.factory_test_cold) + deviceParams.getCold_temp_real() + "\u2000\u2000" +
                            getResources().getString(R.string.factory_test_freezing) + deviceParams.getFreezing_temp_real() + "\u2000\u2000" +
                            getResources().getString(R.string.factory_test_indoor) + deviceParams.getIndoor_temp() + "\u2000\u2000" +
                            getResources().getString(R.string.factory_test_frost) + deviceParams.getFc_evaporator_temp();
                    mFridgeDataTextView.setText(str);
                    // 强制化霜
                    if (deviceParams.isFc_forced_frost()) {
                        mFrostForceButton.setText(getResources().getString(R.string.factory_test_frost_force_cancel));
                    } else {
                        mFrostForceButton.setText(getResources().getString(R.string.factory_test_frost_force));
                    }
                    // 速冻
                    if (deviceParams.isQuick_freeze()) {
                        mQuickFreezingButton.setText(getResources().getString(R.string.factory_test_quick_freezing_cancel));
                    } else {
                        mQuickFreezingButton.setText(getResources().getString(R.string.factory_test_quick_freezing));
                    }
                    // 强制不停机
                    if (deviceParams.isRcf_forced()) {
                        mRCFForceButton.setText(getResources().getString(R.string.factory_test_rcf_force_cancel));
                    } else {
                        mRCFForceButton.setText(getResources().getString(R.string.factory_test_rcf_force));
                    }
                    // 测试缩时功能
                    mIsIgnore = true;
                    mSwitchButton.setOn(deviceParams.isTime_cut());
                    mIsIgnore = false;
                    // 防凝露关闭模式
                    if (deviceParams.isFlipping_beam_reset_mode()) {
                        mClosingModeButton.setText(getResources().getString(R.string.factory_test_closing_mode_exit));
                    } else {
                        mClosingModeButton.setText(getResources().getString(R.string.factory_test_closing_mode_enter));
                    }
                    // 翻转梁加热丝状态
                    if (deviceParams.isFlipping_beam_status()) {
                        mFlippingBeamTextView.setText(String.format(getResources().getString(R.string.factory_test_heating_status), getResources().getString(R.string.factory_test_working)));
                    } else {
                        mFlippingBeamTextView.setText(String.format(getResources().getString(R.string.factory_test_heating_status), getResources().getString(R.string.factory_test_heating_closed)));
                    }
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    /**
     * 美菱 462
     */
    private void initWithX3() {
        mInspectionButton.setVisibility(View.GONE);
        mInspectionDescTextView.setVisibility(View.GONE);
        Subscription subscription = Observable.interval(0, 3, TimeUnit.SECONDS)
                .onBackpressureDrop() // 背压处理
                .subscribeOn(Schedulers.computation())
                .onTerminateDetach()
                .map(aLong -> SerialManager.getInstance().getDeviceParamsGet())
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(deviceParams -> {
                    // 冰箱数据
                    String str = getResources().getString(R.string.factory_test_temp) + "\u2000\u2000" +
                            getResources().getString(R.string.factory_test_cold) + deviceParams.getCold_temp_real() + "\u2000\u2000" +
                            getResources().getString(R.string.factory_test_freezing) + deviceParams.getFreezing_temp_real() + "\u2000\u2000" +
                            getResources().getString(R.string.factory_test_indoor) + deviceParams.getIndoor_temp() + "\u2000\u2000" +
                            getResources().getString(R.string.factory_test_frost) + deviceParams.getFc_evaporator_temp();
                    mFridgeDataTextView.setText(str);
                    // 强制化霜
                    if (deviceParams.isFc_forced_frost()) {
                        mFrostForceButton.setText(getResources().getString(R.string.factory_test_frost_force_cancel));
                    } else {
                        mFrostForceButton.setText(getResources().getString(R.string.factory_test_frost_force));
                    }
                    // 速冻
                    if (deviceParams.isQuick_freeze()) {
                        mQuickFreezingButton.setText(getResources().getString(R.string.factory_test_quick_freezing_cancel));
                    } else {
                        mQuickFreezingButton.setText(getResources().getString(R.string.factory_test_quick_freezing));
                    }
                    // 强制不停机
                    if (deviceParams.isRcf_forced()) {
                        mRCFForceButton.setText(getResources().getString(R.string.factory_test_rcf_force_cancel));
                    } else {
                        mRCFForceButton.setText(getResources().getString(R.string.factory_test_rcf_force));
                    }
                    // 测试缩时功能
                    mIsIgnore = true;
                    mSwitchButton.setOn(deviceParams.isTime_cut());
                    mIsIgnore = false;
                    // 防凝露关闭模式
                    if (deviceParams.isFlipping_beam_reset_mode()) {
                        mClosingModeButton.setText(getResources().getString(R.string.factory_test_closing_mode_exit));
                    } else {
                        mClosingModeButton.setText(getResources().getString(R.string.factory_test_closing_mode_enter));
                    }
                    // 翻转梁加热丝状态
                    if (deviceParams.isFlipping_beam_status()) {
                        mFlippingBeamTextView.setText(String.format(getResources().getString(R.string.factory_test_heating_status), getResources().getString(R.string.factory_test_working)));
                    } else {
                        mFlippingBeamTextView.setText(String.format(getResources().getString(R.string.factory_test_heating_status), getResources().getString(R.string.factory_test_heating_closed)));
                    }
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    /**
     * 雪祺 450
     */
    private void initWithX4() {
        mFridgeDataTextView.setVisibility(View.GONE);
        mSerialPressureButton.setVisibility(View.GONE);
        mFrostForceButton.setVisibility(View.GONE);
        mQuickFreezingButton.setVisibility(View.GONE);
        mRCFForceButton.setVisibility(View.GONE);
        mSerialPressureButton.setVisibility(View.GONE);
        mClosingModeButton.setVisibility(View.GONE);
        mInspectionButton.setVisibility(View.GONE);
        mLinearLayout.setVisibility(View.GONE);
        mFridgeDataTextView.setVisibility(View.GONE);
        mFlippingBeamTextView.setVisibility(View.GONE);
        mInspectionDescTextView.setVisibility(View.GONE);
    }

    /**
     * 美菱 521
     */
    private void initWithX5() {
        mInspectionButton.setVisibility(View.GONE);
        mInspectionDescTextView.setVisibility(View.GONE);
        Subscription subscription = Observable.interval(0, 3, TimeUnit.SECONDS)
                .onBackpressureDrop() // 背压处理
                .subscribeOn(Schedulers.computation())
                .onTerminateDetach()
                .map(aLong -> SerialManager.getInstance().getDeviceParamsGet())
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(deviceParams -> {
                    // 冰箱数据
                    String str = getResources().getString(R.string.factory_test_temp) + "\u2000\u2000" +
                            getResources().getString(R.string.factory_test_cold) + deviceParams.getCold_temp_real() + "\u2000\u2000" +
                            getResources().getString(R.string.factory_test_changeable) + deviceParams.getChangeable_temp_real() + "\u2000\u2000" +
                            getResources().getString(R.string.factory_test_freezing) + deviceParams.getFreezing_temp_real() + "\u2000\u2000" +
                            getResources().getString(R.string.factory_test_indoor) + deviceParams.getIndoor_temp() + "\u2000\u2000" +
                            getResources().getString(R.string.factory_test_frost) + deviceParams.getFc_evaporator_temp();
                    mFridgeDataTextView.setText(str);
                    // 强制化霜
                    if (deviceParams.isFc_forced_frost()) {
                        mFrostForceButton.setText(getResources().getString(R.string.factory_test_frost_force_cancel));
                    } else {
                        mFrostForceButton.setText(getResources().getString(R.string.factory_test_frost_force));
                    }
                    // 速冻
                    if (deviceParams.isQuick_freeze()) {
                        mQuickFreezingButton.setText(getResources().getString(R.string.factory_test_quick_freezing_cancel));
                    } else {
                        mQuickFreezingButton.setText(getResources().getString(R.string.factory_test_quick_freezing));
                    }
                    // 强制不停机
                    if (deviceParams.isRcf_forced()) {
                        mRCFForceButton.setText(getResources().getString(R.string.factory_test_rcf_force_cancel));
                    } else {
                        mRCFForceButton.setText(getResources().getString(R.string.factory_test_rcf_force));
                    }
                    // 测试缩时功能
                    mIsIgnore = true;
                    mSwitchButton.setOn(deviceParams.isTime_cut());
                    mIsIgnore = false;
                    // 防凝露关闭模式
                    if (deviceParams.isFlipping_beam_reset_mode()) {
                        mClosingModeButton.setText(getResources().getString(R.string.factory_test_closing_mode_exit));
                    } else {
                        mClosingModeButton.setText(getResources().getString(R.string.factory_test_closing_mode_enter));
                    }
                    // 翻转梁加热丝状态
                    if (deviceParams.isFlipping_beam_status()) {
                        mFlippingBeamTextView.setText(String.format(getResources().getString(R.string.factory_test_heating_status), getResources().getString(R.string.factory_test_working)));
                    } else {
                        mFlippingBeamTextView.setText(String.format(getResources().getString(R.string.factory_test_heating_status), getResources().getString(R.string.factory_test_heating_closed)));
                    }
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @OnClick(R.id.factory_test_self_checking)
    public void selfChecking() { // 自检
        Intent intent = new Intent(Intent.ACTION_VIEW);
        String factory = ToolUtil.getFridgeFactory();
        if (AppConstants.FACTORY_CND.equals(factory)) {
            intent.setClassName("com.cndlcd.factory", "com.cndlcd.factory.DeviceTest");
        } else {
            intent.setClassName("com.mediatek.factorymode", "com.mediatek.factorymode.FactoryMode");
        }
        startActivityForResult(intent, AppConstants.CODE_SELF_CHECKING);
        SerialManager.getInstance().close();// 关闭串口
    }

    @OnClick(R.id.factory_test_frost_force)
    public void frostForce() { // 强制化霜
        if (mFrostForceButton.getText().toString().equals(getResources().getString(R.string.factory_test_frost_force))) {
            mFrostForceButton.setText(getResources().getString(R.string.factory_test_frost_force_cancel));
            FridgeRepository.getInstance().setFrostForce(true, null);
        } else {
            mFrostForceButton.setText(getResources().getString(R.string.factory_test_frost_force));
            FridgeRepository.getInstance().setFrostForce(false, null);
        }
    }

    @OnClick(R.id.factory_test_quick_freezing)
    public void quickFreezing() { // 速冻
        if (mQuickFreezingButton.getText().toString().equals(getResources().getString(R.string.factory_test_quick_freezing))) {
            mQuickFreezingButton.setText(getResources().getString(R.string.factory_test_quick_freezing_cancel));
            FridgeRepository.getInstance().setSmartFreeze(true, null);
        } else {
            mQuickFreezingButton.setText(getResources().getString(R.string.factory_test_quick_freezing));
            FridgeRepository.getInstance().setSmartFreeze(false, null);
        }
    }

    @OnClick(R.id.factory_test_rcf_force)
    public void rcfForce() { // 强制不停机
        if (mRCFForceButton.getText().toString().equals(getResources().getString(R.string.factory_test_rcf_force))) {
            mRCFForceButton.setText(getResources().getString(R.string.factory_test_rcf_force_cancel));
            FridgeRepository.getInstance().setRCFForce(true, null);
        } else {
            mRCFForceButton.setText(getResources().getString(R.string.factory_test_rcf_force));
            FridgeRepository.getInstance().setRCFForce(false, null);
        }
    }

    @OnClick(R.id.factory_test_closing_mode)
    public void closingMode() { // 防凝露关闭模式
        if (mClosingModeButton.getText().toString().equals(getResources().getString(R.string.factory_test_closing_mode_enter))) {
            mClosingModeButton.setText(getResources().getString(R.string.factory_test_closing_mode_exit));
            FridgeRepository.getInstance().setFlippingBeamMode(true, null);
        } else {
            mClosingModeButton.setText(getResources().getString(R.string.factory_test_closing_mode_enter));
            FridgeRepository.getInstance().setFlippingBeamMode(false, null);
        }
    }

    @OnClick(R.id.factory_test_serial)
    public void serialPressureTest() { // 压力串口测试
        if (mSerialPressureButton.getText().toString().equals(getResources().getString(R.string.factory_test_serial_pressure))) {
            mSerialPressureButton.setText(getResources().getString(R.string.factory_test_serial_pressure_cancel));
            if (mSerialSubscription != null) mSerialSubscription.unsubscribe();
            mSerialSubscription = Observable.interval(1000, 500, TimeUnit.MILLISECONDS)
                    .onBackpressureDrop() // 背压处理
                    .compose(RxSchedulerUtil.SchedulersTransformer2())
                    .onTerminateDetach()
                    .subscribe(aLong -> FridgeRepository.getInstance().setRCRoomTemp(3, null),
                            throwable -> logUtil.e(TAG, throwable.getMessage()));
        } else {
            mSerialPressureButton.setText(getResources().getString(R.string.factory_test_serial_pressure));
            if (mSerialSubscription != null) mSerialSubscription.unsubscribe();
            mSerialSubscription = null;
        }
        if (mSerialSubscription != null) {
            mCompositeSubscription.add(mSerialSubscription);
        }
    }
}