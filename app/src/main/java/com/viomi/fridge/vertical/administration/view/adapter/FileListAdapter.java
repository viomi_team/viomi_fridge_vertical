package com.viomi.fridge.vertical.administration.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.model.entity.FileInfo;
import com.viomi.fridge.vertical.common.util.FileUtil;
import com.viomi.fridge.vertical.common.util.SDCardUtils;

import java.util.List;


/**
 * 文件列表适配器
 * Created by nanquan on 2018/2/5.
 */
public class FileListAdapter extends RecyclerView.Adapter<FileListAdapter.FileViewHolder> {
    private Context mContext;
    private List<FileInfo> mList;
    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public FileListAdapter(Context mContext, List<FileInfo> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public FileViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(mContext).inflate(R.layout.item_file_manage, parent, false);
        return new FileViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(FileViewHolder holder, int position) {
        FileInfo file = mList.get(position);

        if (file.getBitmap(mContext) != null) {
            holder.imgLogo.setImageBitmap(file.getBitmap(mContext));
        } else {
            holder.imgLogo.setImageResource(R.drawable.icon_file_music);
        }

        if (file.getFileType() == FileUtil.FileType.FILE_MUSIC) {
            holder.tvName.setText(file.getFileName());
            holder.tvName.setVisibility(View.VISIBLE);
        } else {
            holder.tvName.setVisibility(View.GONE);
        }
        holder.imgSelect.setSelected(file.isSelected());
        holder.tvSize.setText(SDCardUtils.bit2KbMGB(file.getFileSize()));

        if (onItemClickListener != null) {
            holder.itemView.setOnClickListener(view -> onItemClickListener.onItemClick(position));
        }
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    class FileViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgLogo;
        private ImageView imgSelect;
        private TextView tvName;
        private TextView tvSize;

        FileViewHolder(View view) {
            super(view);
            imgLogo = view.findViewById(R.id.imgLogo);
            imgSelect = view.findViewById(R.id.imgSelect);
            tvName = view.findViewById(R.id.tvName);
            tvSize = view.findViewById(R.id.tvSize);
        }
    }
}