package com.viomi.fridge.vertical.message.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

/**
 * 商城消息实体
 * Created by nanquan on 2018/2/6.
 */

@Entity
public class DeviceMessage {
    @Id
    private String id;
    private long time;
    private String topic;
    private String title;
    private String content;
    private String imgurl;
    private String accessurl;
    private String attrs;

    @Generated(hash = 874401727)
    public DeviceMessage(String id, long time, String topic, String title,
            String content, String imgurl, String accessurl, String attrs) {
        this.id = id;
        this.time = time;
        this.topic = topic;
        this.title = title;
        this.content = content;
        this.imgurl = imgurl;
        this.accessurl = accessurl;
        this.attrs = attrs;
    }

    @Generated(hash = 177518416)
    public DeviceMessage() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public String getAccessurl() {
        return accessurl;
    }

    public void setAccessurl(String accessurl) {
        this.accessurl = accessurl;
    }

    public String getAttrs() {
        return attrs;
    }

    public void setAttrs(String attrs) {
        this.attrs = attrs;
    }
}
