package com.viomi.fridge.vertical.speech.model.entity;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.List;

/**
 * 思必驰媒体资源 Json
 * Created by William on 2018/8/14.
 */
public class MediaEntity implements Serializable {
    @JSONField(name = "widgetName")
    private String widgetName;
    @JSONField(name = "itemsPerPage")
    private int itemsPerPage;
    @JSONField(name = "skillId")
    private String skillId;
    @JSONField(name = "currentPage")
    private int currentPage;
    @JSONField(name = "intentName")
    private String intentName;
    @JSONField(name = "totalPages")
    private int totalPages;
    @JSONField(name = "dataSource")
    private String dataSource;
    @JSONField(name = "content")
    private List<MediaContentEntity> content;

    public String getWidgetName() {
        return widgetName;
    }

    public void setWidgetName(String widgetName) {
        this.widgetName = widgetName;
    }

    public int getItemsPerPage() {
        return itemsPerPage;
    }

    public void setItemsPerPage(int itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

    public String getSkillId() {
        return skillId;
    }

    public void setSkillId(String skillId) {
        this.skillId = skillId;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public String getIntentName() {
        return intentName;
    }

    public void setIntentName(String intentName) {
        this.intentName = intentName;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public String getDataSource() {
        return dataSource;
    }

    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    public List<MediaContentEntity> getContent() {
        return content;
    }

    public void setContent(List<MediaContentEntity> content) {
        this.content = content;
    }
}
