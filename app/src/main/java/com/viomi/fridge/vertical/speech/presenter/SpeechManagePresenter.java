package com.viomi.fridge.vertical.speech.presenter;

import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.speech.contract.SpeechManageContract;
import com.viomi.fridge.vertical.speech.model.preference.SpeechPreference;

import javax.annotation.Nullable;

import rx.Observable;
import rx.Subscription;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * 语音助手设置 Presenter
 * Created by William on 2018/1/31.
 */
public class SpeechManagePresenter implements SpeechManageContract.Presenter {
    private static final String TAG = SpeechManagePresenter.class.getSimpleName();
    private CompositeSubscription mCompositeSubscription;

    @Nullable
    private SpeechManageContract.View mView;

    @Override
    public void subscribe(SpeechManageContract.View view) {
        this.mView = view;
        mCompositeSubscription = new CompositeSubscription();
        loadSetting();
    }

    @Override
    public void unSubscribe() {
        this.mView = null;
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
    }

    @Override
    public void loadSetting() {
        Subscription subscription = Observable.just(SpeechPreference.getInstance().getSwitch())
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(aBoolean -> {
                    if (mView != null) mView.showSetting(aBoolean);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void cacheSwitch(boolean enable) {
        Subscription subscription = Observable.just(enable)
                .observeOn(Schedulers.io())
                .onTerminateDetach()
                .subscribe(aBoolean -> SpeechPreference.getInstance().saveSwitch(aBoolean),
                        throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }
}