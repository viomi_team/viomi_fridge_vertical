package com.viomi.fridge.vertical.album.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.album.view.AlbumPreviewAdapter;
import com.viomi.fridge.vertical.common.base.BaseActivity;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * 电子相册预览 Activity
 * Created by William on 2018/1/26.
 */
public class AlbumPreviewActivity extends BaseActivity {
    private static final String TAG = AlbumPreviewActivity.class.getSimpleName();

    @BindView(R.id.album_preview_viewpager)
    ViewPager mViewPager;// 左右切换

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        layoutId = R.layout.activity_album_preview;
        mTitle = "";
        super.onCreate(savedInstanceState);
        ArrayList<String> paths;
        int index;
        paths = getIntent().getStringArrayListExtra(AppConstants.PHOTO_LIST);// 相册集合
        index = getIntent().getIntExtra(AppConstants.PHOTO_INDEX, 0);// 相册开始下标
        logUtil.d(TAG, index + "");
        AlbumPreviewAdapter adapter = new AlbumPreviewAdapter(this, paths, ToolUtil.getScreenWidth(AlbumPreviewActivity.this),
                ToolUtil.getScreenHeight(AlbumPreviewActivity.this));
        mViewPager.setAdapter(adapter);
        mViewPager.setCurrentItem(index);
    }
}
