package com.viomi.fridge.vertical.messageboard.view.dialog;

import android.view.View;
import android.widget.SeekBar;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.CommonDialog;
import com.viomi.fridge.vertical.common.util.logUtil;

import butterknife.BindView;

/**
 * 橡皮擦编辑 Dialog
 * Created by William on 2018/2/27.
 */
public class EraserEditDialog extends CommonDialog implements SeekBar.OnSeekBarChangeListener {
    private static final String TAG = EraserEditDialog.class.getSimpleName();
    private OnEraserEditDismissListener onEraserEditDismissListener;
    private int mSize = 20, mProgress = 20;// 橡皮擦大小

    @BindView(R.id.eraser_edit)
    SeekBar mSeekBar;// 调整橡皮擦大小

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.dialog_eraser_edit;
    }

    @Override
    protected void initWithOnCreateDialog(View view) {
        mSeekBar.setOnSeekBarChangeListener(this);
        mSeekBar.setProgress(mProgress);
    }

    @Override
    public void onDestroyView() {
        if (onEraserEditDismissListener != null) {
            onEraserEditDismissListener.onEraserEditDismiss(mSize);
            logUtil.d(TAG, "onEraserEditDismissListener");
        }
        super.onDestroyView();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        mProgress = progress;
        int calcProgress = progress > 1 ? progress : 1;
        mSize = Math.round((100 / 100f) * calcProgress);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    public interface OnEraserEditDismissListener {
        void onEraserEditDismiss(int size);
    }

    public void setOnEraserEditDismissListener(OnEraserEditDismissListener onEraserEditDismissListener) {
        this.onEraserEditDismissListener = onEraserEditDismissListener;
    }
}