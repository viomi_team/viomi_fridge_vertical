package com.viomi.fridge.vertical.foodmanage.module;

import com.viomi.fridge.vertical.common.scope.ActivityScoped;
import com.viomi.fridge.vertical.foodmanage.contract.FoodManageContract;
import com.viomi.fridge.vertical.foodmanage.presenter.FoodManagePresenter;

import dagger.Binds;
import dagger.Module;

/**
 * 食材管理 Module
 * Created by William on 2018/2/25.
 */
@Module
public abstract class FoodManageModule {

    @ActivityScoped
    @Binds
    abstract FoodManageContract.Presenter foodManagePresenter(FoodManagePresenter presenter);
}