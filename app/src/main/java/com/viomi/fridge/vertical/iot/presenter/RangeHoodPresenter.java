package com.viomi.fridge.vertical.iot.presenter;

import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.iot.contract.RangeHoodContract;
import com.viomi.fridge.vertical.iot.model.http.entity.RangeHoodData;
import com.viomi.fridge.vertical.iot.model.http.entity.RangeHoodProp;
import com.viomi.fridge.vertical.iot.model.repository.RangeHoodRepository;

import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * 烟机 Presenter
 * Created by William on 2018/2/21.
 */
public class RangeHoodPresenter implements RangeHoodContract.Presenter {
    private static final String TAG = RangeHoodPresenter.class.getSimpleName();
    private CompositeSubscription mCompositeSubscription;

    @Nullable
    private RangeHoodContract.View mView;

    @Override
    public void subscribe(RangeHoodContract.View view) {
        this.mView = view;
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public void unSubscribe() {
        mView = null;
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
    }

    @Override
    public void getProp(String did) {
        Subscription subscription = Observable.interval(0, 5, TimeUnit.SECONDS)
                .onBackpressureDrop() // 背压处理
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .flatMap(aLong -> RangeHoodRepository.getProp(did))
                .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                .onTerminateDetach()
                .map(rpcResult -> new RangeHoodProp(rpcResult.getList()))
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(rangeHoodProp -> {
                    if (rangeHoodProp != null && mView != null) mView.refreshUi(rangeHoodProp);
                });
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void getUserData(String did) {
        Subscription subscription = Observable.interval(0, 5, TimeUnit.SECONDS)
                .onBackpressureDrop() // 背压处理
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .flatMap(aLong -> RangeHoodRepository.getUserData(did))
                .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                .onTerminateDetach()
                .map(rpcResult -> new RangeHoodData(rpcResult.getList()))
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(rangeHoodData -> {
                    if (rangeHoodData != null && mView != null) mView.refreshUi(rangeHoodData);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void setPower(String param, String did) {
        Subscription subscription = RangeHoodRepository.setPower(param, did)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (mView != null) mView.setIsSetting();
                }, throwable -> {
                    if (mView != null) mView.setIsSetting();
                    logUtil.e(TAG, throwable.getMessage());
                });
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void setWind(String param, String did) {
        Subscription subscription = RangeHoodRepository.setWind(param, did)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (mView != null) mView.setIsSetting();
                }, throwable -> {
                    if (mView != null) mView.setIsSetting();
                    logUtil.e(TAG, throwable.getMessage());
                });
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void setLight(String param, String did) {
        Subscription subscription = RangeHoodRepository.setLight(param, did)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (mView != null) mView.setIsSetting();
                }, throwable -> {
                    if (mView != null) mView.setIsSetting();
                    logUtil.e(TAG, throwable.getMessage());
                });
        mCompositeSubscription.add(subscription);
    }
}