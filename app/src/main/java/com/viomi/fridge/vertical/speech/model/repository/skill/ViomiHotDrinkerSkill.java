package com.viomi.fridge.vertical.speech.model.repository.skill;

import android.content.Context;

import com.miot.common.abstractdevice.AbstractDevice;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.administration.model.entity.QRCodeBase;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.iot.model.http.entity.HeatKettleProp;
import com.viomi.fridge.vertical.iot.model.repository.HeatKettleRepository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 云米饮水吧技能
 * Created by William on 2018/8/29.
 */
public class ViomiHotDrinkerSkill {
    private static final String TAG = ViomiHotDrinkerSkill.class.getSimpleName();

    public static void handle(Context context, String data, AbstractDevice device) {
        String content;// 语音播报内容
        QRCodeBase qrCodeBase = (QRCodeBase) ToolUtil.getFileObject(context, AppConstants.USER_INFO_FILE);
        if (qrCodeBase == null) {
            content = "请先登录云米账号";
            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
            return;
        }
        if (device == null) {
            content = "没有连接到饮水吧";
            RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
            return;
        }

        try {
            JSONObject jsonObject = new JSONObject(data);
            JSONObject nlu = jsonObject.optJSONObject("nlu");
            if (nlu == null) return;
            JSONObject semantics = nlu.optJSONObject("semantics");
            if (semantics == null) return;
            JSONObject request = semantics.optJSONObject("request");
            if (request == null) return;
            JSONArray slots = request.optJSONArray("slots");
            if (slots == null || slots.length() <= 0) return;

            String value = "";// 意图
            int value_temp = -1;// 设置水温
            for (int i = 0; i < slots.length(); i++) {
                JSONObject jsonItem = slots.optJSONObject(i);
                String name = jsonItem.optString("name");
                if (name.equals("intent")) {
                    value = jsonItem.optString("value");
                    break;
                }
            }

            switch (value) {
                case "设置温水键温度":
                    for (int i = 0; i < slots.length(); i++) {
                        JSONObject itemJson = slots.optJSONObject(i);
                        String name = itemJson.optString("name");
                        if ("整数".equals(name)) {
                            value_temp = itemJson.optInt("value");
                            break;
                        }
                    }
                    if (value_temp == -1) return;
                    if (value_temp < 40 || value_temp > 90) {
                        content = "饮水吧温度只能设为40至90摄氏度";
                        RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                        return;
                    }
                    HeatKettleRepository.setTemp(device.getDeviceId(), value_temp)
                            .compose(RxSchedulerUtil.SchedulersTransformer1())
                            .subscribe(rpcResult -> {
                            }, throwable -> logUtil.e(TAG, throwable.getMessage()));
                    content = "正在为你把" + device.getName() + "温度调整为" + value_temp + "度";
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "快捷设置温度":
                    value_temp = 50;
                    content = "正在为你把" + device.getName() + "温度调整为" + value_temp + "度";
                    RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, content);
                    break;
                case "查询水温":
                    HeatKettleRepository.getProp(device.getDeviceId())
                            .subscribeOn(Schedulers.io())
                            .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                            .onTerminateDetach()
                            .map(rpcResult -> new HeatKettleProp(rpcResult.getList()))
                            .observeOn(AndroidSchedulers.mainThread())
                            .onTerminateDetach()
                            .subscribe(heatKettleProp -> RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, device.getName() + "当前出水温度为"
                                    + heatKettleProp.getSetup_tempe() + "度"), throwable -> logUtil.e(TAG, throwable.getMessage()));
                    break;
                case "查询水质":
                    HeatKettleRepository.getProp(device.getDeviceId())
                            .subscribeOn(Schedulers.io())
                            .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                            .onTerminateDetach()
                            .map(rpcResult -> new HeatKettleProp(rpcResult.getList()))
                            .observeOn(AndroidSchedulers.mainThread())
                            .onTerminateDetach()
                            .subscribe(heatKettleProp -> RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, device.getName() + "当前TDS值为"
                                    + heatKettleProp.getTds()), throwable -> logUtil.e(TAG, throwable.getMessage()));
                    break;
                case "查询存水时间":
                    HeatKettleRepository.getProp(device.getDeviceId())
                            .subscribeOn(Schedulers.io())
                            .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                            .onTerminateDetach()
                            .map(rpcResult -> new HeatKettleProp(rpcResult.getList()))
                            .observeOn(AndroidSchedulers.mainThread())
                            .onTerminateDetach()
                            .subscribe(heatKettleProp -> RxBus.getInstance().post(BusEvent.MSG_SPEAK_CONTENT, device.getName() + "存水时间为"
                                    + heatKettleProp.getWater_remain_time() + "小时"), throwable -> logUtil.e(TAG, throwable.getMessage()));
                    break;
            }
        } catch (JSONException e) {
            logUtil.e(TAG, e.toString());
            e.printStackTrace();
        }
    }
}
