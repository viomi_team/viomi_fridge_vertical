package com.viomi.fridge.vertical.administration.view.dialog;

import android.view.View;
import android.widget.EditText;

import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.CommonDialog;
import com.viomi.fridge.vertical.common.http.ApiClient;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.ToastUtil;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.OnClick;
import rx.Subscription;

/**
 * 意见反馈 Dialog
 * Created by William on 2018/3/15.
 */
public class FeedbackDialog extends CommonDialog {
    private static final String TAG = FeedbackDialog.class.getSimpleName();
    private LoadingDialog mLoadingDialog;// 加载对话框
    private Subscription mSubscription;

    @BindView(R.id.feedback_phone)
    EditText mPhoneEditText;// 电话输入
    @BindView(R.id.feedback_content)
    EditText mContentEditText;// 内容输入

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.dialog_feedback;
    }

    @Override
    protected void initWithOnCreateDialog(View view) {
        mLoadingDialog = new LoadingDialog(getActivity());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
        if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
            mLoadingDialog.cancel();
            mLoadingDialog = null;
        }
    }

    @OnClick(R.id.feedback_submit)
    public void submit() { // 提交
        String phone = mPhoneEditText.getText().toString().trim();
        String content = mContentEditText.getText().toString().trim();
        if (phone.equals("")) {
            ToastUtil.showCenter(FridgeApplication.getContext(), FridgeApplication.getContext().getResources().getString(R.string.management_feedback_no_phone_tip));
            return;
        }
        if (phone.length() != 11) {
            ToastUtil.showCenter(FridgeApplication.getContext(), FridgeApplication.getContext().getResources().getString(R.string.management_feedback_wrong_phone_tip));
            return;
        }
        if (content.length() < 6) {
            ToastUtil.showCenter(FridgeApplication.getContext(), FridgeApplication.getContext().getResources().getString(R.string.management_feedback_content_tip));
            return;
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("contactWay", phone);
            jsonObject.put("feedback", content);
            jsonObject.put("sourceChannel", 9);
            jsonObject.put("appVersion", ToolUtil.getVersion());

            if (mLoadingDialog != null) mLoadingDialog.show();
            mSubscription = ApiClient.getInstance().getApiService().feedback(ApiClient.getInstance().getRequestBody(jsonObject))
                    .compose(RxSchedulerUtil.SchedulersTransformer1())
                    .onTerminateDetach()
                    .subscribe(result -> {
                        logUtil.d(TAG, result);
                        if (mLoadingDialog != null) mLoadingDialog.dismiss();
                        ToastUtil.showCenter(FridgeApplication.getContext(), FridgeApplication.getContext().getResources().getString(R.string.management_feedback_submit_success));
                        dismiss();
                    }, throwable -> {
                        logUtil.d(TAG, throwable.getMessage());
                        if (mLoadingDialog != null) mLoadingDialog.dismiss();
                        ToastUtil.showCenter(FridgeApplication.getContext(), FridgeApplication.getContext().getResources().getString(R.string.management_feedback_submit_fail));
                    });
        } catch (JSONException e) {
            e.printStackTrace();
            ToastUtil.showCenter(FridgeApplication.getContext(), FridgeApplication.getContext().getResources().getString(R.string.management_feedback_submit_fail));
        }
    }
}