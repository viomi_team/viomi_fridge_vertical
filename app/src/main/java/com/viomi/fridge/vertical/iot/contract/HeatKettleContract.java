package com.viomi.fridge.vertical.iot.contract;

import com.viomi.fridge.vertical.common.callback.BasePresenter;
import com.viomi.fridge.vertical.iot.model.http.entity.HeatKettleProp;

/**
 * 即热饮水吧 Contract
 * Created by William on 2018/2/8.
 */
public interface HeatKettleContract {
    interface View {
        void refreshUi(HeatKettleProp prop);// 刷新 Ui

        void setIsSetting();
    }

    interface Presenter extends BasePresenter<View> {
        void getProp(String did);// getProp 请求

        void setTemp(String did, int temp);// 温水键温度设置
    }
}
