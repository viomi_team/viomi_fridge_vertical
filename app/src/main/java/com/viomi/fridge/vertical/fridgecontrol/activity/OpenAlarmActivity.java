package com.viomi.fridge.vertical.fridgecontrol.activity;

import android.content.Context;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseActivity;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.logUtil;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;
import rx.Observable;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * 门开报警 Dialog
 * Created by William on 2018/3/22.
 */
public class OpenAlarmActivity extends BaseActivity {
    private static final String TAG = OpenAlarmActivity.class.getSimpleName();
    private CompositeSubscription mCompositeSubscription;// 统一管理消息订阅
    private int minute, second;// 分钟，秒
    private AudioManager mAudioManager;
    private MediaPlayer mMediaPlayer;

    @BindView(R.id.open_alarm_time)
    TextView mTextView;// 倒计时

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        layoutId = R.layout.activity_open_alarm;
        super.onCreate(savedInstanceState);
        mCompositeSubscription = new CompositeSubscription();
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        mTextView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/DINCond-Medium.otf"));

        minute = 1;
        startPlay();
        Subscription timeSubscription = Observable.interval(0, 1, TimeUnit.SECONDS)
                .onBackpressureDrop() // 背压处理
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(aLong -> {
                    second++;
                    if (second >= 60) {
                        second = 0;
                        minute++;
                    }
                    String min = minute < 10 ? "0" + minute : minute + "";
                    String sec = second < 10 ? "0" + second : second + "";
                    String str = min + ":" + sec;
                    mTextView.setText(str);
                }, throwable -> logUtil.e(TAG, throwable.getMessage()));

        Subscription msgSubscription = RxBus.getInstance().subscribe(busEvent -> {
            switch (busEvent.getMsgId()) {
                case BusEvent.MSG_OPEN_ALARM_DISMISS: // 关闭门开报警
                    finish();
                    break;
            }
        });

        mCompositeSubscription.add(timeSubscription);
        mCompositeSubscription.add(msgSubscription);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
        stopPlay();
    }

    @OnClick(R.id.open_alarm_confirm)
    public void close() { // 关闭
        finish();
    }

    private void startPlay() {
        mMediaPlayer = MediaPlayer.create(this, R.raw.open_alarm);
        if (mAudioManager != null) {
            if (mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC) != 0) {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                try {
                    mMediaPlayer.prepare();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mMediaPlayer.setLooping(false);
                mMediaPlayer.start();
            }
        }
    }

    private void stopPlay() {
        if (mMediaPlayer != null) {
            if (mMediaPlayer.isPlaying()) mMediaPlayer.stop();
            mMediaPlayer.release();
        }
    }
}