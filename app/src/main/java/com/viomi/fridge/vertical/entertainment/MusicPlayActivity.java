package com.viomi.fridge.vertical.entertainment;

import android.animation.ObjectAnimator;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.audiofx.Visualizer;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeController;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.postprocessors.IterativeBoxBlurPostProcessor;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseHandlerActivity;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.speech.model.entity.MediaContentEntity;
import com.viomi.fridge.vertical.speech.service.MusicService;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;
import rx.Observable;
import rx.Subscription;

/**
 * 音乐播放器 Activity
 * Created by William on 2018/8/14.
 */
public class MusicPlayActivity extends BaseHandlerActivity implements SeekBar.OnSeekBarChangeListener {
    private final static String TAG = MusicPlayActivity.class.getSimpleName();
    private boolean mIsSetting = false;
    private Subscription mSubscription;
    private Subscription mRxBusSubscription;
    private MusicService mService;
    private ObjectAnimator mObjectAnimator;

    @BindView(R.id.music_play_cover_layout)
    RelativeLayout mCoverRelativeLayout;// 封面布局

    @BindView(R.id.music_play_cover)
    SimpleDraweeView mSimpleDraweeView;// 封面
    @BindView(R.id.music_play_bg)
    SimpleDraweeView mBgSimpleDraweeView;// 背景

    @BindView(R.id.music_play_title)
    TextView mNameTextView;// 音乐标题
    @BindView(R.id.music_play_detail)
    TextView mDetailTextView;// 详情
    @BindView(R.id.music_play_end_time)
    TextView mEndTimeTextView;// 结束时间
    @BindView(R.id.music_play_start_time)
    TextView mProgressTextView;// 进度时间

    @BindView(R.id.music_play)
    ImageView mPlayImageView;// 播放或暂停
    @BindView(R.id.music_previous)
    ImageView mPreviousImageView;// 上一首
    @BindView(R.id.music_next)
    ImageView mNextImageView;// 下一首

    @BindView(R.id.music_play_seek_bar)
    SeekBar mSeekBar;// 播放进度

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        layoutId = R.layout.activity_music_play;
        super.onCreate(savedInstanceState);
        RxBus.getInstance().post(BusEvent.MSG_ALBUM_STOP_STROLL);// 首页相册停止滚动
        bindService(new Intent(this, MusicService.class), mServiceConnection, BIND_AUTO_CREATE);
        mSeekBar.setOnSeekBarChangeListener(this);

        mRxBusSubscription = RxBus.getInstance().subscribe(busEvent -> {
            switch (busEvent.getMsgId()) {
                case BusEvent.MSG_PLAY_STOP: // 停止播放
                    finish();
                    break;
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RxBus.getInstance().post(BusEvent.MSG_START_STROLL);// 首页相册开始滚动
        if (mService != null) mService.removeCustomCallback();
        unbindService(mServiceConnection);
        stopAnimation();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
        if (mRxBusSubscription != null) {
            mRxBusSubscription.unsubscribe();
            mRxBusSubscription = null;
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        initView(intent);
    }

    @OnClick(R.id.music_previous)
    public void previous() { // 上一首
        RxBus.getInstance().post(BusEvent.MSG_PLAY_PREVIOUS);
    }

    @OnClick(R.id.music_next)
    public void next() { // 下一首
        RxBus.getInstance().post(BusEvent.MSG_PLAY_NEXT);
    }

    @OnClick(R.id.music_play)
    public void playOrPause() { // 播放或暂停
        if (mService != null) {
            if (mService.isPlaying()) RxBus.getInstance().post(BusEvent.MSG_PLAY_PAUSE);
            else RxBus.getInstance().post(BusEvent.MSG_PLAY_CONTINUE);
        }
    }

    @OnClick(R.id.music_play_layout)
    public void close() {
        finish();
        RxBus.getInstance().post(BusEvent.MSG_SHOW_MUSIC_FLOAT);
    }

    private void initView(Intent intent) {
        String type = intent.getStringExtra(AppConstants.SPEECH_MEDIA_TYPE);
        MediaContentEntity entity = (MediaContentEntity) intent.getSerializableExtra(AppConstants.SPEECH_MEDIA_DATA);
        mNameTextView.setText(entity.getTitle());
        if (type.equals(AppConstants.SPEECH_MEDIA_MUSIC)) {
            mDetailTextView.setVisibility(View.VISIBLE);
            mDetailTextView.setText(String.format(getResources().getString(R.string.entertainment_music_detail),
                    TextUtils.isEmpty(entity.getLabel().trim()) ? getResources().getString(R.string.entertainment_unknown) : entity.getLabel(),
                    TextUtils.isEmpty(entity.getSubTitle().trim()) ? getResources().getString(R.string.entertainment_unknown) : entity.getSubTitle()));
        } else mDetailTextView.setVisibility(View.INVISIBLE);
        // 封面图片
        ImageRequest request = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(type.equals(AppConstants.SPEECH_MEDIA_MUSIC) ? entity.getImageUrl() : ""))
                .setResizeOptions(new ResizeOptions((int) getResources().getDimension(R.dimen.px_x_150), (int) getResources().getDimension(R.dimen.px_y_150)))
                .build();
        PipelineDraweeController controller = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                .setOldController(mSimpleDraweeView.getController())
                .setImageRequest(request)
                .build();
        mSimpleDraweeView.setController(controller);
        mSimpleDraweeView.setVisibility(View.VISIBLE);
        loadBackground(type.equals(AppConstants.SPEECH_MEDIA_MUSIC) ? entity.getImageUrl() : "");
    }

    private void startAnimation() {
        if (mObjectAnimator == null) {
            mObjectAnimator = ObjectAnimator.ofFloat(mCoverRelativeLayout, "rotation", 0, 359);
            mObjectAnimator.setRepeatCount(ObjectAnimator.INFINITE);
            mObjectAnimator.setRepeatMode(ObjectAnimator.RESTART);
            // 线型插值器，动画匀速执行
            mObjectAnimator.setInterpolator(new LinearInterpolator());
            mObjectAnimator.setDuration(10 * 1000);
        }
        mObjectAnimator.start();
    }

    private void stopAnimation() {
        if (mObjectAnimator != null) mObjectAnimator.cancel();
    }

    private void loadBackground(String url) {
        Uri uri = Uri.parse(url);
        GenericDraweeHierarchy hierarchy = mBgSimpleDraweeView.getHierarchy();
        hierarchy.setActualImageScaleType(ScalingUtils.ScaleType.CENTER_CROP);
        mBgSimpleDraweeView.setHierarchy(hierarchy);
        ImageRequest request = ImageRequestBuilder
                .newBuilderWithSource(uri)
                .setResizeOptions(new ResizeOptions((int) getResources().getDimension(R.dimen.px_x_820), (int) getResources().getDimension(R.dimen.px_y_565)))
                .setPostprocessor(new IterativeBoxBlurPostProcessor(6, 5))
                .build();
        PipelineDraweeController controller = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                .setOldController(mBgSimpleDraweeView.getController())
                .setImageRequest(request)
                .build();
        mBgSimpleDraweeView.setController(controller);
    }

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            logUtil.d(TAG, "onServiceConnected");
            mService = ((MusicService.MusicBinder) service).getService();
            mService.setCustomCallback(iCallBack);
            mService.getProgress();
            initView(getIntent());
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            logUtil.d(TAG, "onServiceDisconnected");
        }
    };

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        mProgressTextView.setText(ToolUtil.formatTimeString(progress));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        mIsSetting = true;
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mIsSetting = false;
        if (mService != null)
            mService.seekTo(seekBar.getProgress() * 1000);
    }

    private ICallBack iCallBack = new ICallBack() {
        @Override
        public void onPlay() {
            if (mHandler != null)
                mHandler.post(() -> {
                    mPlayImageView.setImageResource(R.drawable.btn_media_pause);
                    startAnimation();
                });
        }

        @Override
        public void onPause() {
            if (mHandler != null)
                mHandler.post(() -> {
                    mPlayImageView.setImageResource(R.drawable.btn_media_play);
                    stopAnimation();
                });
        }

        @Override
        public void onResume() {
            if (mHandler != null)
                mHandler.post(() -> {
                    mPlayImageView.setImageResource(R.drawable.btn_media_pause);
                    startAnimation();
                });
        }

        @Override
        public void onStop() {
            if (mHandler != null)
                mHandler.post(() -> {
                    mPlayImageView.setImageResource(R.drawable.btn_media_play);
                    stopAnimation();
                });
        }

        @Override
        public void onBuffering() {

        }

        @Override
        public void onBufferComplete() {

        }

        @Override
        public void onBufferingUpdate(int percent) {

        }

        @Override
        public void onUpdateSeekBar(int time) {
            if (mHandler != null) {
                mHandler.post(() -> {
                    mSeekBar.setProgress(0);
                    mProgressTextView.setText(ToolUtil.formatTimeString(0));
                });
            }
            if (mSubscription != null) {
                mSubscription.unsubscribe();
                mSubscription = null;
            }
            mSubscription = Observable.interval(0, 500, TimeUnit.MILLISECONDS)
                    .onBackpressureDrop()
                    .compose(RxSchedulerUtil.SchedulersTransformer4())
                    .onTerminateDetach()
                    .subscribe(aLong -> {
                        if (!mIsSetting) mSeekBar.setProgress(mService.getPosition() / 1000);
                    }, throwable -> logUtil.e(TAG, throwable.getMessage()));
        }

        @Override
        public void onComplete(boolean isFinish) {
            if (mHandler != null) {
                mHandler.post(() -> {
                    mPlayImageView.setImageResource(R.drawable.btn_media_play);
                    stopAnimation();
                });
            }
        }

        @Override
        public void onPlayError(int errorCode, String errorMsg) {

        }

        @Override
        public void onPrepareNext() {

        }

        @Override
        public void onPrepare() {

        }

        @Override
        public void onSetTotalTime(int time) {
            if (mHandler != null) {
                mHandler.post(() -> {
                    mSeekBar.setMax(time);
                    mEndTimeTextView.setText(ToolUtil.formatTimeString(time));
                });
            }
        }

        @Override
        public void onSetVisualizer(Visualizer visualizer) {

        }

        @Override
        public void onUpdateUIState(UIState uiState) {

        }
    };
}
