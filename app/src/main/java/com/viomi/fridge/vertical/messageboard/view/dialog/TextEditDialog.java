package com.viomi.fridge.vertical.messageboard.view.dialog;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.CommonDialog;
import com.viomi.fridge.vertical.common.util.logUtil;

import butterknife.BindView;
import butterknife.OnTextChanged;

/**
 * 留言板文字编辑 Dialog
 * Created by William on 2018/4/11.
 */
public class TextEditDialog extends CommonDialog {
    private static final String TAG = TextEditDialog.class.getSimpleName();
    private OnDismissListener onDismissListener;
    private String mContent = "";// 输入内容

    @BindView(R.id.text_edit_input)
    EditText mEditText;// 文字输入

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.dialog_text_edit;
    }

    @Override
    protected void initWithOnCreateDialog(View view) {
        mContent = "";
        if (getArguments() != null) {
            mContent = getArguments().getString("text", "");
            mEditText.setText(mContent);
        }
        // 显示输入法
        InputMethodManager imm = (InputMethodManager) FridgeApplication.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null)
            imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @Override
    public void onDestroy() {
        if (onDismissListener != null) {
            logUtil.d(TAG, mContent);
            onDismissListener.onDismiss(mContent);
        }
        super.onDestroy();
    }

    @OnTextChanged(value = R.id.text_edit_input, callback = OnTextChanged.Callback.TEXT_CHANGED)
    public void onTextChanged(CharSequence s, int start, int before, int count) { // 文字改变监听
        mContent = mEditText.getText().toString();
    }

    public interface OnDismissListener {
        void onDismiss(String content);
    }

    public void setOnDismissListener(OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }
}