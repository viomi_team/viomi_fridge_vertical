package com.viomi.fridge.vertical.album.model.entity;

import java.io.File;

/**
 * 相册相关信息
 * Created by William on 2018/1/20.
 */
public class AlbumEntity {
    private File file;// 图片文件
    private boolean isSelected;// 是否被选中
    private long lastModified;// 修改时间

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public long getLastModified() {
        return lastModified;
    }

    public void setLastModified(long lastModified) {
        this.lastModified = lastModified;
    }
}