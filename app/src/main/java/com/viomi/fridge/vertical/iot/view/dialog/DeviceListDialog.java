package com.viomi.fridge.vertical.iot.view.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.CommonDialog;
import com.viomi.fridge.vertical.common.base.RecyclerViewLinearDivider;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.util.ToastUtil;
import com.viomi.fridge.vertical.iot.model.http.entity.DeviceInfo;
import com.viomi.fridge.vertical.iot.view.adapter.DeviceAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 设备列表 Dialog
 * Created by William on 2018/3/3.
 */
public class DeviceListDialog extends CommonDialog implements AdapterView.OnItemClickListener {
    private static final String TAG = DeviceListDialog.class.getSimpleName();
    private List<DeviceInfo> mList;
    private WaterPurifierDialog mWaterPurifierDialog;// 净水器 Dialog
    private HeatKettleDialog mHeatKettleDialog;// 即热饮水吧 Dialog
    private PLMachineDialog mPLMachineDialog;// 管线机 Dialog
    private RangeHoodDialog mRangeHoodDialog;// 烟机 Dialog
//    private CameraDialog mCameraDialog;// 摄像头 Dialog

    public DeviceListDialog() {
        mList = new ArrayList<>();
        RxBus.getInstance().subscribe(busEvent -> {
            switch (busEvent.getMsgId()) {
                case BusEvent.MSG_IOT_DEVICES:
                    mList = (List<DeviceInfo>) busEvent.getMsgObject();
                    break;
            }
        });
    }

    @BindView(R.id.device_list)
    RecyclerView mRecyclerView;

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.dialog_device_list;
    }

    @Override
    protected void initWithOnCreateDialog(View view) {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.addItemDecoration(new RecyclerViewLinearDivider(getActivity(), LinearLayoutManager.VERTICAL, 1,
                getResources().getColor(R.color.color_e8)));
        DeviceAdapter adapter = new DeviceAdapter(mList);
        mRecyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null && dialog.getWindow() != null) {
            dialog.getWindow().setLayout(820, 820);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mList != null) {
            mList.clear();
            mList = null;
        }
        if (mWaterPurifierDialog != null && mWaterPurifierDialog.isAdded()) {
            mWaterPurifierDialog.dismiss();
            mWaterPurifierDialog = null;
        }
        if (mHeatKettleDialog != null && mHeatKettleDialog.isAdded()) {
            mHeatKettleDialog.dismiss();
            mHeatKettleDialog = null;
        }
        if (mPLMachineDialog != null && mPLMachineDialog.isAdded()) {
            mPLMachineDialog.dismiss();
            mPLMachineDialog = null;
        }
        if (mRangeHoodDialog != null && mRangeHoodDialog.isAdded()) {
            mRangeHoodDialog.dismiss();
            mRangeHoodDialog = null;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        DeviceInfo deviceInfo = mList.get(position);
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        Bundle bundle = new Bundle();
        if (deviceInfo.getAbstractDevice() != null) { // 小米 IOT 设备
            if (deviceInfo.getAbstractDevice().isOnline()) { // 在线
                switch (deviceInfo.getAbstractDevice().getDeviceModel()) {
                    case AppConstants.YUNMI_WATERPURI_V1:
                    case AppConstants.YUNMI_WATERPURI_V2:
                    case AppConstants.YUNMI_WATERPURI_S1:
                    case AppConstants.YUNMI_WATERPURI_S2:
                    case AppConstants.YUNMI_WATERPURI_C1:
                    case AppConstants.YUNMI_WATERPURI_C2:
                    case AppConstants.YUNMI_WATERPURI_X3:
                    case AppConstants.YUNMI_WATERPURI_X5:
                    case AppConstants.YUNMI_WATERPURIFIER_V1:
                    case AppConstants.YUNMI_WATERPURIFIER_V2:
                    case AppConstants.YUNMI_WATERPURIFIER_V3:
                    case AppConstants.YUNMI_WATERPURI_LX2:
                    case AppConstants.YUNMI_WATERPURI_LX3:
                        if (mWaterPurifierDialog == null)
                            mWaterPurifierDialog = new WaterPurifierDialog();
                        if (!mWaterPurifierDialog.isAdded()) {
                            bundle.putString(mWaterPurifierDialog.PARAM_NAME, deviceInfo.getAbstractDevice().getName());
                            bundle.putString(mWaterPurifierDialog.PARAM_DID, deviceInfo.getAbstractDevice().getDeviceId());
                            bundle.putString(mWaterPurifierDialog.PARAM_MODEL, deviceInfo.getAbstractDevice().getDeviceModel());
                            mWaterPurifierDialog.setArguments(bundle);
                            mWaterPurifierDialog.show(ft, TAG);
                        }
                        break;
                    case AppConstants.YUNMI_KETTLE_R1:
                        if (mHeatKettleDialog == null) mHeatKettleDialog = new HeatKettleDialog();
                        if (!mHeatKettleDialog.isAdded()) {
                            bundle.putString(mHeatKettleDialog.PARAM_NAME, deviceInfo.getAbstractDevice().getName());
                            bundle.putString(mHeatKettleDialog.PARAM_DID, deviceInfo.getAbstractDevice().getDeviceId());
                            mHeatKettleDialog.setArguments(bundle);
                            mHeatKettleDialog.show(ft, TAG);
                        }
                        break;
                    case AppConstants.YUNMI_PLMACHINE_MG2:
                        if (mPLMachineDialog == null) mPLMachineDialog = new PLMachineDialog();
                        if (!mPLMachineDialog.isAdded()) {
                            bundle.putString(mPLMachineDialog.PARAM_NAME, deviceInfo.getAbstractDevice().getName());
                            bundle.putString(mPLMachineDialog.PARAM_DID, deviceInfo.getAbstractDevice().getDeviceId());
                            mPLMachineDialog.setArguments(bundle);
                            mPLMachineDialog.show(ft, TAG);
                        }
                        break;
                    case AppConstants.VIOMI_HOOD_A5:
                    case AppConstants.VIOMI_HOOD_A6:
                    case AppConstants.VIOMI_HOOD_A4:
                    case AppConstants.VIOMI_HOOD_A7:
                    case AppConstants.VIOMI_HOOD_C1:
                    case AppConstants.VIOMI_HOOD_H1:
                    case AppConstants.VIOMI_HOOD_H2:
                        if (mRangeHoodDialog == null) mRangeHoodDialog = new RangeHoodDialog();
                        if (!mRangeHoodDialog.isAdded()) {
                            bundle.putString(mRangeHoodDialog.PARAM_NAME, deviceInfo.getAbstractDevice().getName());
                            bundle.putString(mRangeHoodDialog.PARAM_DID, deviceInfo.getAbstractDevice().getDeviceId());
                            mRangeHoodDialog.setArguments(bundle);
                            mRangeHoodDialog.show(ft, TAG);
                        }
                        break;
                }
            } else {
                ToastUtil.showCenter(FridgeApplication.getContext(), FridgeApplication.getContext().getResources().getString(R.string.iot_device_offline_tip));
            }
        }
//        else if (deviceInfo.getCameraInfo() != null) { // 绿联摄像头
//            if (deviceInfo.getCameraInfo().isOnline()) {
//                bundle.putString(mCameraDialog.PARAM_DID, deviceInfo.getCameraInfo().getSrcId());
//                mCameraDialog.setArguments(bundle);
//                mCameraDialog.show(ft, TAG);
//            } else {
//                ToastUtil.showCenter(mContext, getResources().getString(R.string.iot_device_offline_tip));
//            }
//        }
    }
}