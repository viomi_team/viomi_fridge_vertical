package com.viomi.fridge.vertical.administration.view.dialog;

import android.content.Context;
import android.widget.ImageView;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.common.base.BaseDialog;
import com.viomi.fridge.vertical.common.util.ImageLoadUtil;
import com.viomi.fridge.vertical.common.util.RxSchedulerUtil;
import com.viomi.fridge.vertical.common.util.logUtil;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;

/**
 * 等待提示
 * Created by nanquan on 2018/1/15.
 */
public class LoadingDialog extends BaseDialog {
    private static final String TAG = LoadingDialog.class.getSimpleName();
    private ImageView ivLoading;
    private Subscription timeSubscription;

    public LoadingDialog(Context context) {
        super(context, R.style.Dialog);
        setContentView(R.layout.dialog_loading);
        // 不可点击取消
        setCancelable(false);
    }

    @Override
    public void initView() {
        ivLoading = findViewById(R.id.iv_loading);
    }

    @Override
    public void show() {
        super.show();
        ImageLoadUtil
                .getInstance()
                .loadResource(R.drawable.loading, ivLoading);
        // 最多显示 10 秒
        timeSubscription = Observable.interval(10, TimeUnit.SECONDS)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .take(1)
                .onTerminateDetach()
                .subscribe(aLong -> cancel(), throwable -> logUtil.e(TAG, throwable.getMessage()));
    }

    @Override
    public void cancel() {
        super.cancel();
        if (timeSubscription != null && !timeSubscription.isUnsubscribed()) {
            timeSubscription.unsubscribe();
            timeSubscription = null;
        }
    }
}