package com.viomi.fridge.vertical.home.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewStub;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.model.preference.ManagePreference;
import com.viomi.fridge.vertical.album.AlbumService;
import com.viomi.fridge.vertical.common.base.BaseActivity;
import com.viomi.fridge.vertical.common.base.BaseFragment;
import com.viomi.fridge.vertical.common.entity.MiIdentification;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.service.BackgroundService;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;
import com.viomi.fridge.vertical.home.view.adapter.HomePageAdapter;
import com.viomi.fridge.vertical.home.view.fragment.HomeFragment;
import com.viomi.fridge.vertical.iot.service.IotService;
import com.viomi.fridge.vertical.iot.view.fragment.IotFragment;
import com.viomi.fridge.vertical.speech.service.SpeechService;
import com.viomi.fridge.vertical.timer.service.TimerService;
import com.viomi.fridge.vertical.web.fragment.MallFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import rx.Subscription;

/**
 * 首页 Activity
 * Created by William on 2018/3/5.
 */
public class HomeActivity extends BaseActivity implements ViewPager.OnPageChangeListener {
    private static final String TAG = HomeActivity.class.getSimpleName();
    private Subscription mSubscription;

    @Inject
    IotFragment mIotFragment;// 互联网家 Fragment

    @Inject
    HomeFragment mHomeFragment;// 主页 Fragment

    @Inject
    MallFragment mMallFragment;// 云米商城 Fragment

    @BindView(R.id.home_viewpager)
    ViewPager mViewPager;// ViewPager

    @BindView(R.id.home_guide_layout)
    ViewStub mViewStub;// 控制指引布局显示

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        layoutId = R.layout.activity_home;
        super.onCreate(savedInstanceState);
        if (ManagePreference.getInstance().getGuide()) { // 首次使用
            View view = mViewStub.inflate();// 指引
            view.findViewById(R.id.guide_known).setOnClickListener(v -> {
                view.setVisibility(View.GONE);
                ManagePreference.getInstance().saveGuide(false);
            });
        }
        List<BaseFragment> list = new ArrayList<>();
        list.add(mIotFragment);
        list.add(mHomeFragment);
        list.add(mMallFragment);
        HomePageAdapter homePageAdapter = new HomePageAdapter(getSupportFragmentManager(), list);
        mViewPager.setAdapter(homePageAdapter);
        mViewPager.setCurrentItem(1);// 初始显示页
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.addOnPageChangeListener(this);
        startService();// 启动服务

        mSubscription = RxBus.getInstance().subscribe(busEvent -> {
            switch (busEvent.getMsgId()) {
                case BusEvent.MSG_TIME_MINUTE: // 每分钟监听
                    runOnUiThread(() -> {
                        if (!ToolUtil.isServiceWork(HomeActivity.this,
                                "com.viomi.fridge.vertical.common.service.BackgroundService")) {
                            logUtil.d(TAG, "restart BackgroundService success");
                            startService(new Intent(HomeActivity.this, BackgroundService.class));// 后台服务
                        }
                        if (!ToolUtil.isServiceWork(HomeActivity.this,
                                "com.viomi.fridge.vertical.album.AlbumService")) {
                            logUtil.d(TAG, "restart AlbumService success");
                            startService(new Intent(HomeActivity.this, AlbumService.class));// 电子相册服务
                        }
                        if (!ToolUtil.isServiceWork(HomeActivity.this,
                                "com.viomi.fridge.vertical.timer.service.TimerService")) {
                            logUtil.d(TAG, "restart TimerService success");
                            startService(new Intent(HomeActivity.this, TimerService.class));// 定时器服务
                        }
                        if (!ToolUtil.isServiceWork(HomeActivity.this,
                                "com.viomi.fridge.vertical.speech.service.SpeechService")) {
                            MiIdentification miIdentification = ToolUtil.getMiIdentification();
                            String mac = miIdentification.getMac();
                            String did = miIdentification.getDeviceId();
                            if (!mac.equals("7C:49:EB:0F:42:82") && !did.equals("85396802")) { // 没有烧写 did，不初始化语音
                                logUtil.d(TAG, "restart SpeechService success");
                                startService(new Intent(this, SpeechService.class));// 语音服务
                            }
                        }
                        if (!ToolUtil.isServiceWork(HomeActivity.this,
                                "com.viomi.fridge.vertical.iot.service.IotService")) {
                            logUtil.d(TAG, "restart IotService success");
                            startService(new Intent(this, IotService.class));// 互联网家后台服务
                        }
                    });
                    break;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (FridgePreference.getInstance().getModel().equals(AppConstants.MODEL_JD)) { // 京东冰箱
            Intent intent = new Intent("com.jd.smart.fridge.launcher.onresume.call");
            sendBroadcast(intent);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        RxBus.getInstance().post(BusEvent.MSG_REMOVE_HOME_MALL);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        logUtil.d(TAG, "onDestroy");
        stopAllService();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        int position = intent.getIntExtra(AppConstants.HOME_POSITION, 1);
        mViewPager.setCurrentItem(position, true);
    }

    private void startService() {
        MiIdentification miIdentification = ToolUtil.getMiIdentification();
        String mac = miIdentification.getMac();
        String did = miIdentification.getDeviceId();
        if (!mac.equals("7C:49:EB:0F:42:82") && !did.equals("85396802")) { // 没有烧写 did，不初始化语音
            startService(new Intent(this, SpeechService.class));// 语音服务
        }
        startService(new Intent(this, BackgroundService.class));// 后台服务
        startService(new Intent(this, AlbumService.class));// 电子相册后台服务
        startService(new Intent(this, TimerService.class));// 定时器服务
        startService(new Intent(this, IotService.class));// 互联网家后台服务
    }

    private void stopAllService() {
        stopService(new Intent(this, BackgroundService.class));
        stopService(new Intent(this, AlbumService.class));
        stopService(new Intent(this, TimerService.class));
        stopService(new Intent(this, SpeechService.class));
        stopService(new Intent(this, IotService.class));
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onBackPressed() {
        // 拦截返回键监听
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        if (state == 1) { // 开始滑动
            RxBus.getInstance().post(BusEvent.MSG_HOME_START_SCROLL);
        } else if (state == 2) { // 滑动结束
            RxBus.getInstance().post(BusEvent.MSG_HOME_STOP_SCROLL);
        }
    }
}
