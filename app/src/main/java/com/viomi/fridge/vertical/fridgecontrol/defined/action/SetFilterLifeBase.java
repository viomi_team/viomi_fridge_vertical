package com.viomi.fridge.vertical.fridgecontrol.defined.action;

import com.viomi.fridge.vertical.fridgecontrol.defined.ViomiDefined;
import com.viomi.fridge.vertical.fridgecontrol.defined.property.FilterLifeBase;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetFilterLifeBase extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setFilterLifeBase.toActionType();

    public SetFilterLifeBase() {
        super(TYPE);

        super.addArgument(FilterLifeBase.TYPE.toString());
    }
}