package com.viomi.fridge.vertical.home.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.viomi.fridge.vertical.common.base.BaseFragment;

import java.util.List;

/**
 * 主页面 ViewPager 适配器
 * Created by William on 2018/3/5.
 */
public class HomePageAdapter extends FragmentPagerAdapter {
    private FragmentManager mFragmentManager;
    private List<BaseFragment> mList;

    public HomePageAdapter(FragmentManager fm, List<BaseFragment> list) {
        super(fm);
        mList = list;
    }

    @Override
    public Fragment getItem(int position) {
        return mList.get(position);
    }

    @Override
    public int getCount() {
        return mList.size();
    }
}