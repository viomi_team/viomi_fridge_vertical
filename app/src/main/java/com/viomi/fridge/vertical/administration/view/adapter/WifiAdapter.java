package com.viomi.fridge.vertical.administration.view.adapter;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.view.dialog.WifiSettingDialog;
import com.viomi.fridge.vertical.common.util.logUtil;

import java.util.List;

/**
 * WIFI 设置页面适配器
 * Created by nanquan on 2018/1/9.
 */
public class WifiAdapter extends RecyclerView.Adapter<WifiAdapter.WifiViewHolder> {
    private static final String TAG = WifiAdapter.class.getName();
    private Context mContext;
    private List<ScanResult> mList;
    private WifiManager mWifiManager;
    private int[] wifiIcons = new int[]{R.drawable.wifi_1, R.drawable.wifi_2, R.drawable.wifi_3};
    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(ScanResult scanResult);
    }

    public WifiAdapter(Context context, List<ScanResult> list, WifiManager wifiManager) {
        this.mContext = context;
        this.mList = list;
        this.mWifiManager = wifiManager;
    }

    @Override
    public WifiViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rooView = LayoutInflater.from(mContext).inflate(R.layout.item_wifi, parent, false);
        return new WifiViewHolder(rooView);
    }

    @Override
    public void onBindViewHolder(WifiViewHolder holder, int position) {
        ScanResult scanResult = mList.get(position);

        holder.wifi_name.setText(scanResult.SSID);
        holder.wifi_icon.setImageResource(wifiIcons[WifiManager.calculateSignalLevel(scanResult.level, wifiIcons.length)]);

        final WifiInfo info = mWifiManager.getConnectionInfo();
        final boolean isCurrentNetwork_WifiInfo = info != null
                && android.text.TextUtils.equals(info.getSSID(), "\"" + scanResult.SSID + "\"")
                && android.text.TextUtils.equals(info.getBSSID(), scanResult.BSSID);
        if (isCurrentNetwork_WifiInfo) {
            if (WifiSettingDialog.isWiFiActive(mContext)) {
                // 已连接连接
                holder.connect_status.setText(R.string.status_connected);
                holder.wifi_name.setTextColor(mContext.getResources().getColor(R.color.color_light_green));
                holder.connect_status.setTextColor(mContext.getResources().getColor(R.color.color_light_green));
                logUtil.e(TAG, "BSSID = " + scanResult.BSSID);
            } else {
                // 正在连接
                holder.connect_status.setText(R.string.status_connecting);
                holder.wifi_name.setTextColor(mContext.getResources().getColor(R.color.color_25));
                holder.connect_status.setTextColor(mContext.getResources().getColor(R.color.color_99));
            }
        } else {
            // 未连接
            holder.connect_status.setText(R.string.status_unconnected);
            holder.wifi_name.setTextColor(mContext.getResources().getColor(R.color.color_25));
            holder.connect_status.setTextColor(mContext.getResources().getColor(R.color.color_99));
        }

        if (onItemClickListener != null) {
            holder.itemView.setOnClickListener(view -> onItemClickListener.onItemClick(scanResult));
        }
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    class WifiViewHolder extends RecyclerView.ViewHolder {
        TextView wifi_name;
        TextView connect_status;
        ImageView wifi_icon;

        WifiViewHolder(View itemView) {
            super(itemView);
            connect_status = itemView.findViewById(R.id.connect_status);
            wifi_name = itemView.findViewById(R.id.wifi_name);
            wifi_icon = itemView.findViewById(R.id.wifi_icon);
        }
    }
}