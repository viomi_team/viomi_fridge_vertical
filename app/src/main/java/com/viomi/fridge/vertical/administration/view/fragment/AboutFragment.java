package com.viomi.fridge.vertical.administration.view.fragment;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.fridge.vertical.AppConstants;
import com.viomi.fridge.vertical.FridgeApplication;
import com.viomi.fridge.vertical.R;
import com.viomi.fridge.vertical.administration.model.preference.ManagePreference;
import com.viomi.fridge.vertical.administration.model.repository.ManageRepository;
import com.viomi.fridge.vertical.administration.view.dialog.FeedbackDialog;
import com.viomi.fridge.vertical.administration.view.dialog.UpdateDialog;
import com.viomi.fridge.vertical.common.base.BaseFragment;
import com.viomi.fridge.vertical.common.entity.MiIdentification;
import com.viomi.fridge.vertical.common.http.ApiClient;
import com.viomi.fridge.vertical.common.rxbus.BusEvent;
import com.viomi.fridge.vertical.common.rxbus.RxBus;
import com.viomi.fridge.vertical.common.scope.ActivityScoped;
import com.viomi.fridge.vertical.common.util.ToolUtil;
import com.viomi.fridge.vertical.common.util.logUtil;
import com.viomi.fridge.vertical.fridgecontrol.activity.FactoryTestActivity;
import com.viomi.fridge.vertical.fridgecontrol.model.preference.FridgePreference;
import com.viomi.fridge.vertical.web.activity.BrowserActivity;
import com.viomi.widget.switchbutton.SwitchButton;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * 关于软件 Fragment
 * Created by William on 2018/1/31.
 */
@ActivityScoped
public class AboutFragment extends BaseFragment {
    private static final String TAG = AboutFragment.class.getSimpleName();
    private int mVersion;// App 版本
    private int mClickCount;// 调试点击次数
    private Subscription mSubscription;// 消息订阅
    private FeedbackDialog mFeedbackDialog;// 意见反馈 Dialog
    private UpdateDialog mUpdateDialog;// 检查更新 Dialog

    @BindView(R.id.manage_about_app_version)
    TextView mAppVersionTextView;// app 版本
    @BindView(R.id.manage_about_system_version)
    TextView mSystemVersionTextView;// 系统版本
    @BindView(R.id.manage_about_app_version_new)
    TextView mAppNewVersionTextView;// app 更新红点
    @BindView(R.id.manage_about_mac)
    TextView mMacTextView;// Mac 地址

    @BindView(R.id.manage_about_system_setting_layout)
    RelativeLayout mSystemRelativeLayout;// 系统设置
    @BindView(R.id.manage_about_factory_layout)
    RelativeLayout mFactoryRelativeLayout;// 工厂模式
    @BindView(R.id.manage_about_debug_layout)
    RelativeLayout mDebugRelativeLayout;// 测试环境

    @BindView(R.id.manage_about_line_1)
    View mLineView1;// 分割线
    @BindView(R.id.manage_about_line_2)
    View mLineView2;// 分割线
    @BindView(R.id.manage_about_line_3)
    View mLineView3;// 分割线

    @BindView(R.id.manage_about_version_arrow)
    ImageView mImageView;// 软件版本箭头

    @BindView(R.id.manage_about_debug_switch)
    SwitchButton mSwitchButton;// 测试环境开关

    private CompositeSubscription mCompositeSubscription;

    @Inject
    public AboutFragment() {

    }

    @Override
    protected void initWithOnCreate() {
        layoutId = R.layout.fragment_about;
    }

    @Override
    protected void initWithOnCreateView() {
        mSwitchButton.setOn(ManagePreference.getInstance().getDebug());
        mCompositeSubscription = new CompositeSubscription();
        initVersion();
        initData();
        mClickCount = 0;
        mSubscription = RxBus.getInstance().subscribe(busEvent -> {
            switch (busEvent.getMsgId()) {
                case BusEvent.MSG_VERSION_UPDATE: // 是否有新版本
                    if (ManagePreference.getInstance().getAppUpdate())
                        mAppNewVersionTextView.setVisibility(View.VISIBLE);
                    else mAppNewVersionTextView.setVisibility(View.GONE);
                    break;
            }
        });

        mSwitchButton.setOnSwitchStateChangeListener(isOn -> ManageRepository.getInstance().logout()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                    if (aBoolean) {
                        ToolUtil.saveObject(FridgeApplication.getContext(), AppConstants.USER_INFO_FILE, null);// 删除本地云米账号信息
                        ManagePreference.getInstance().saveDebug(isOn);
                        RxBus.getInstance().post(BusEvent.MSG_SERVER_CHANGE);// 服务器环境改变
                        ApiClient.getInstance().changeServer();// 改变服务器环境
                        if (getActivity() != null) getActivity().finish();
                    }
                }, throwable -> logUtil.e(TAG, throwable.getMessage())));
        if (FridgePreference.getInstance().getModel().equals(AppConstants.MODEL_JD))
            mImageView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
        if (mFeedbackDialog != null && mFeedbackDialog.isAdded()) {
            mFeedbackDialog.dismiss();
            mFeedbackDialog = null;
        }
        if (mUpdateDialog != null && mUpdateDialog.isAdded()) {
            mUpdateDialog.dismiss();
            mUpdateDialog = null;
        }
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
    }

    private void initVersion() {
        String version = "V1.0";
        try {
            PackageManager manager = FridgeApplication.getContext().getPackageManager();
            PackageInfo info = manager.getPackageInfo(FridgeApplication.getContext().getPackageName(), 0);
            mVersion = info.versionCode;
            version = "V" + info.versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        mAppVersionTextView.setText(version);
        mSystemVersionTextView.setText(android.os.Build.DISPLAY);
        if (ManagePreference.getInstance().getAppUpdate())
            mAppNewVersionTextView.setVisibility(View.VISIBLE);
        else mAppNewVersionTextView.setVisibility(View.GONE);
    }

    private void initData() {
        MiIdentification miIdentification = ToolUtil.getMiIdentification();
        String mac = miIdentification.getMac();
        String did = miIdentification.getDeviceId();
        if (mac.equals("7C:49:EB:0F:42:82") && did.equals("85396802")) {
            String str = "MAC: " + mac + ",  DID: " + did + "(Error)";
            mMacTextView.setText(str);
            mMacTextView.setTextColor(Color.RED);
        } else mMacTextView.setText(String.valueOf("MAC: " + mac + ",  DID: " + did));
    }

    @OnClick(R.id.manage_about_app_version_layout)
    public void onVersionClick() { // 软件版本点击
        if (FridgePreference.getInstance().getModel().equals(AppConstants.MODEL_JD)) return;
        if (mUpdateDialog == null) mUpdateDialog = new UpdateDialog();
        if (mUpdateDialog.isAdded()) return;
        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putInt(mUpdateDialog.VERSION, mVersion);
        mUpdateDialog.setArguments(bundle);
        mUpdateDialog.show(fragmentTransaction, TAG);
    }

    @OnClick(R.id.manage_about_instructions)
    public void onInstructionClick() { // 进入使用说明
        String url = AppConstants.URL_GUIDE_X1;
        switch (FridgePreference.getInstance().getModel()) {
            case AppConstants.MODEL_X1:
                url = AppConstants.URL_GUIDE_X1;
                break;
            case AppConstants.MODEL_X2: // 双鹿 446
                url = AppConstants.URL_GUIDE_X2;
                break;
            case AppConstants.MODEL_X3: // 美菱 462
                url = AppConstants.URL_GUIDE_X3;
                break;
            case AppConstants.MODEL_X4: // 雪祺 450
            case AppConstants.MODEL_JD: // 京东冰箱
                url = AppConstants.URL_GUIDE_X4;
                break;
            case AppConstants.MODEL_X5: // 美菱 521
                url = AppConstants.URL_GUIDE_X5;
                break;
        }
        Intent intent = new Intent(getActivity(), BrowserActivity.class);
        intent.putExtra(AppConstants.WEB_URL, url);
        startActivity(intent);
    }

    @OnClick(R.id.manage_about_feedback)
    public void onFeedBackClick() { // 意见反馈
        if (mFeedbackDialog == null) mFeedbackDialog = new FeedbackDialog();
        if (mFeedbackDialog.isAdded()) return;
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        mFeedbackDialog.show(transaction, TAG);
    }

    @OnClick(R.id.system_layout)
    public void enterDebug() { // 进入调试模式
        if (mClickCount == 5) return;
        mClickCount++;
        if (mClickCount == 5) {
            mSystemRelativeLayout.setVisibility(View.VISIBLE);
            mFactoryRelativeLayout.setVisibility(View.VISIBLE);
            mDebugRelativeLayout.setVisibility(View.VISIBLE);
            mLineView1.setVisibility(View.VISIBLE);
            mLineView2.setVisibility(View.VISIBLE);
            mLineView3.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.manage_about_system_setting_layout)
    public void enterSystemSetting() { // 进入系统设置
        Intent intent = new Intent(Settings.ACTION_SETTINGS);
        startActivity(intent);
    }

    @OnClick(R.id.manage_about_factory_layout)
    public void enterFactory() { // 进入工厂测试
        Intent intent = new Intent(getActivity(), FactoryTestActivity.class);
        startActivity(intent);
    }
}
